<style media="screen">
  .icon-timer::before{
    content : "\f017";
  }
</style>
<div class="panel col-md-6">
  <div class="panel-heading">
    <i class="icon-car"></i> AutoPermis.re - Calendrier / Points
  </div>
    {if !$npc->id_np_customer_type}
    N'a pas encore envoyé ses documents
    {else}
    <div class="panel panel-sm">
      <div class="panel-heading">
        <i class="icon-file"></i> Documents
      </div>
      <form class="form-horizontal" method="post" >
        <p>Type : {$npc->np_customer_type_obj->name}</p>
        <ul>
          {foreach from=$npc->files item=file}
          <li>{$file->np_customer_file_to_upload_obj->name} <a href="{$file->file}" target="_blank">Voir le fichier</a> </li>
          {/foreach}
        </ul>
        {if !$npc->verified}
        <div class="row">
          <div class="col-lg-12">
            <button onclick="checkDelete(event)" type="submit" name="cancelDemande" class="btn btn-default ">
              <i class="icon-times"></i>
              Supprimer les fichiers
            </button>
            <button onclick="checkConfirm(event)" type="submit" name="verifyUser" class="btn btn-default">
              <i class="icon-check"></i>
              Valider l'utilisateur
            </button>
          </div>
        </div>
        {else}
        <div class="row">
          <div class="col-lg-12">
            <span style="font-size : 1.4em;" class="pull-right badge badge-success"> Validé ! </span>
          </div>
        </div>
        {/if}
      </form>
    </div>
    <div class="panel panel-sm">
      <div class="panel-heading">
        <i class="icon-timer"></i> Heures de conduites
      </div>
      <ul>
        {foreach from=$calendar_slots item=slot}
        <li>Le {$slot->date_slot|date_format:"%A %d %B %Y"} de {$slot->hour}:00 à {$slot->hour+1}:00</li>
        {/foreach}
      </ul>
    </div>
    {/if}

</div>
<script type="text/javascript">
  function checkConfirm(e) {
    if (confirm('Êtes-vous sur de vouloir validé cette utilisateur ? Celui-ci pourra accéder à l\'agenda.')){
      return true;
    }
    e.stopPropagation();
    e.preventDefault();
    return false;
  }

  function checkDelete(e) {
    if (confirm('Êtes-vous sur de vouloir annulé la demande de l\'utilisateur ? Celui-ci devra réenvoyer les fichiers.')){
      return true;
    }
    e.stopPropagation();
    e.preventDefault();
    return false;
  }
</script>
