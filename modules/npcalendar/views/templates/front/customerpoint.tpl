<div class="box">
    <h3>Mes forfaits :</h3>
    <table class="table">
        <tr>
            <th>Produit</th>
            <th>Date de commande</th>
            <th>Date de fin de validité</th>
            <th>Week End</th>
            <th>Fractionnable</th>
            <th>Heure initials</th>
            <th>Heure restantes</th>
        </tr>
        {foreach from=$points item=point}
        <tr>
            <td>{$point->product_obj->name}</td>
            <td>{$point->date_add}</td>
            <td>{$point->date_end}</td>
            <td>
                {if !$point->weekend}
                <i class="icon icon-remove" style="color:red;"></i>
                {else}
                <i class="icon icon-check" style="color:green;"></i>
                {/if}
            </td>
            <td>
                {if !$point->splittable}
                <i class="icon icon-remove" style="color:red;"></i>
                {else}
                <i class="icon icon-check" style="color:green;"></i>
                {/if}
            </td>
            <td>{$point->nb_point}</td>
            <td>{$point->nb_point - $point->np_calendar_slots|count}</td>
        </tr>
        {/foreach}
    </table>
</div>
