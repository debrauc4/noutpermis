{if isset($error)}
<div class="alert alert-{$error.type}">
    <a>
        <h3>{$error.title}</h3>
    </a>
    <p>{$error.txt}</p>
</div>
{/if}

{if $np_customer->id_np_customer_type > 0 && $np_customer->verified == 1}
<div class="box">
    <div class="row">
        <div class="col-lg-12">
            <h3>1. Reservez votre créneau :</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h5>Heures en semaine : {$resume_points.week}</h5>
            <h5>Heures en weekend : {$resume_points.weekend}</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form class="" action="{$link->getModuleLink('npcalendar', 'calendarpage')}" method="post">
                <table>
                    <tr>
                        <td>a. Date</td>
                        <td>b. Heure</td>
                        <td>c. Forfait</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input class="btn btn-default" type="date" name="nDate" value="" onchange="fillHourSelect(event, true);fillWeekIndication(event)"> <br />
                            <span id="weekIndication"></span>
                        </td>
                        <td>
                            <select class="btn btn-default" name="nHour" id="selectHour">
                                <option value="">Choississez une heure</option>
                            </select>
                        </td>
                        <td>
                            <select class="btn btn-default" name="nIdCustomerPoint">
                                <option value="">Choississez un forfait</option>
                                {foreach from=$points item=point}
                                {if $point->nb_point - {$point->np_calendar_slots|count} > 0}
                                <option class="point-class" data-weekend="{$point->weekend}" value="{$point->id}">{$point->product_obj->name} ({$point->nb_point - $point->np_calendar_slots|count} restant(s))</option>
                                {/if}
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <input type="submit" class="btn btn-default" name="submitAddCalendarSlot" value="Reserver mon créneau">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<div class="box">
    <div class="row">
        <div class="col-lg-6">
            <h3>2. Liste des créneaux réservé :</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <h4>À venir</h4>
            <table class="table">
                <tr>
                    <th>Date et heure</th>
                    <th>Forfait</th>
                    <th>Actions</th>
                </tr>
                {foreach from=$calendar_slots.future item=slot}
                <tr>
                    <td>{$slot->date_slot} {$slot->hour}:00</td>
                    <td>{$slot->forfaitName}</td>
                    <td>
                        <button type="button" name="button">
                            <i class="icon icon-trash"></i>
                        </button>
                    </td>
                </tr>
                {/foreach}
            </table>
        </div>
        <div class="col-lg-6">
            <h4>Passé</h4>
            <table class="table">
                <tr>
                    <th>Date</th>
                    <th>Heure</th>
                    <th>Forfait</th>
                    <th>Actions</th>
                </tr>
                {foreach from=$calendar_slots.past item=slot}
                <tr>
                    <td>{$slot->date_slot}</td>
                    <td>{$slot->hour}</td>
                    <td>{$slot->forfaitName}</td>
                    <td>
                        <button type="button" name="button">
                            <i class="icon icon-trash"></i>
                        </button>
                    </td>
                </tr>
                {/foreach}
            </table>
        </div>
    </div>
</div>
{else}
<div class="alert alert-warning">
    <h3>Documents</h3>
    <p>Vous devez nous envoyer vos documents avant d'accéder aux réservations. <a href="{$link->getModuleLink('npcalendar', 'customerdoc')}">Cliquez ici</a></p>
</div>
{/if}

<script type="text/javascript" src="./modules/npcalendar/views/js/classes/NPCalendar.js"></script>
<script type="text/javascript">
var holidays = {$holidays|@json_encode};

function hideWeekPoints() {
    var forfaits = document.getElementsByClassName("point-class");
    for (one of forfaits) {
        console.log('Weekend ::' + one.dataset.weekend)
        if (one.dataset.weekend == 0){
            one.style.display = "none";
        }
    }
}

function viewAllPoints() {
    var forfaits = document.getElementsByClassName("point-class");
    for (one of forfaits) {
        one.style.display = "inline";
    }
}

function fillWeekIndication(evt) {
    var wi = document.getElementById('weekIndication');
    var nDate = new Date(evt.target.value)
    var day = nDate.getDay();
    if (day === 6){
        wi.innerHTML = "Forfait week-end seul (Samedi)"
        hideWeekPoints();
        return ;
    } else if (day === 0) {
        wi.innerHTML = "Forfait week-end seul (Dimanche)"
        hideWeekPoints();
        return ;
    } else {
        for (let h of holidays) {
            console.log(nDate.getDate(), h.day, nDate.getMonth(), h.month, nDate.getFullYear(), h.year)
            if (nDate.getDate() == h.day && nDate.getMonth()+1 == h.month){
                if (h.year == 0 || h.year == nDate.getFullYear()){
                    wi.innerHTML = "Forfait week-end seul (jours fériés)"
                    hideWeekPoints();
                    return ;
                }
            }
        }
        wi.innerHTML = "Forfait semaine ou week end"
        viewAllPoints();
    }
}
</script>
