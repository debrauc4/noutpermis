
<div class="box">
    {if $np_customer->id_np_customer_type == 0}
    <h3>Documents à fournir</h3>
    <p>Merci de bien vouloir nous fournir la liste des documents suivants :</p>

    {if isset($np_customer) && $np_customer->id_np_customer_type != 0}
    <article class="alert alert-success">
        <p>Vos documents ont été transmis à nos services. Un administrateur doit les valider.</p>
    </article>
    {else}
    {if isset($error_upload)}
    <article class="alert alert-warning">
        <p>Impossible d'ajouter vos documents. Merci de tous les renseigner en une seule fois. Et de ne transmettre des documents qu'au format PDF, PNG, JPEG ou JPG.</p>
    </article>
    {/if}
    <form action="{$link->getModuleLink('npcalendar', 'customerdoc')}" method="post" enctype="multipart/form-data">
        <select class="button btn btn-default" name="id_np_customer_type" onchange="changeCustomerType(event)">
            <option value="">-- Choississez un type de profil --</option>
            {foreach from=$np_customer_types item=ct}
            <option value="{$ct->id}">{$ct->name}</option>
            {/foreach}
        </select>
        <div class="" >
            <ul class="" id="file_list">

            </ul>
            <div id="file_list_submit">

            </div>
        </form>
        {/if}
    </div>
    {else}
    <h3>Vos documents :</h3>
        {if $np_customer->verified == 0}
            <p>Etat : En cours de vérification.</p>
        {else}
            <p>Etat : Documents vérifiés.</p>
        {/if}
        <ul>
            {foreach from=$np_customer->files item=file}
            <li><b>{$file->np_customer_file_to_upload_obj->name} : </b> <a href="{$file->file}" target="_blank">Voir le fichier</a> </li>
           {/foreach}
        </ul>
    {/if}
</div>
<script type="text/javascript">
var np_customer_types = {$np_customer_types|@json_encode nofilter};
  function fillCustomerType(customer_type) {
    if (customer_type == undefined){
      document.getElementById('file_list_submit').innerHTML = '';
      document.getElementById('file_list').innerHTML = '';
      return ;
    }
    var fl = document.getElementById('file_list')
    fl.innerHTML = "";
    var o = 0;
    for (i of customer_type.np_file_to_uploads) {
      var li ='<li class="onefile">';
      li += '<div class="name">' + i.name;
      if (i.file_example.length > 1)
        li += ' ( <a style="color:blue;"href="'+ i.file_example+'" target="_blank">Exemple</a> )';
      li += '</div>'
      li += '<div class="description">'+ i.description +'</div>'
      li += '<div class="uploader"><input type="file" class="btn btn-default" name="doc_'+ o +'" ></div>'
      li += '</li>';
      fl.innerHTML += li;
      o++;
    }
    document.getElementById('file_list_submit').innerHTML = '<input class="btn btn-default" type="submit" name="addNPCustomerFile" value="Envoyer">';
  }

  function changeCustomerType(evt) {
    var val = evt.target.value
    if (val == "") fillCustomerType(undefined);
    for (let v of np_customer_types){
      if (v.id == val){
        fillCustomerType(v)
        break;
      }
    }
  }
</script>
