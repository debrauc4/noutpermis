<style media="screen">
.icon-timer::before{ content : "\f017";}
</style>
{if isset($error)}
<article class="alert alert-danger">
    {$error.text}
</article>
{/if}
<div class="panel">
    {if $id_product > 0}
    <h3 class="tab"><i class="icon-timer"></i> Heures de conduites</h3>
    <article class="alert alert-info">
        Ce panneau permet de gerer le nombre d'heure crédité pour l'achat de "{$current_product->name}"
    </article>
    <div class="form-group">
        <label class="control-label col-lg-3" for="np_hide">N'est pas un forfait.</label>
        <div class="input-group col-lg-2">
            {if $pg->hide}
            <input type="checkbox" name="np_hide" value="1" checked>
            {else}
            <input type="checkbox" name="np_hide" value="1">
            {/if}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3" for="np_point_nb_point">Heures créditées</label>
        <div class="input-group col-lg-2">
            <span class="input-group-addon"><i class="icon-timer"></i></span>
            <input name="np_point_nb_point" id="np_point_nb_point" type="text" value="{$pg->nb_point}" >
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3" for="np_point_nb_day">Durée de validité (jours) :</label>
        <div class="input-group col-lg-2">
            <span class="input-group-addon">jours</span>
            <input name="np_point_nb_day" id="np_point_nb_day" type="text" value="{$pg->nb_day}" >
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3" for="np_point_nb_day">Forfait week-end :</label>
        <div class="input-group col-lg-2">
            {if $pg->weekend}
            <input type="checkbox" name="np_point_weekend" value="1" checked>
            {else}
            <input type="checkbox" name="np_point_weekend" value="1">
            {/if}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3" for="np_point_nb_day">Fractionnable :</label>
        <div class="input-group col-lg-2">
            {html_radios name='np_point_splittable' values=array(1, 0) output=array('Oui', 'Non') selected=$pg->splittable separator='<br />'}
            <!--
            <input type="radio" name="np_point_splittable" value="1" checked="{if $pg->splittable}true{else}false{/if}"> Oui <br />
            <input type="radio" name="np_point_splittable" value="0" checked="{if !$pg->splittable}true{else}false{/if}"> Non <br />
        -->
    </div>
</div>
<div class="panel-footer">
    <a href="{$link->getAdminLink('AdminProducts')|escape:'htmlall':'utf-8'}" class="btn btn-default"><i class="process-icon-cancel"></i> Annuler</a>
    <button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer</button>
    <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer et rester</button>
</div>
{else}
<article class="alert alert-danger">
    Veuillez enregistrer le produit avant de personnaliser le nombre d'heure
</article>
{/if}
</div>
