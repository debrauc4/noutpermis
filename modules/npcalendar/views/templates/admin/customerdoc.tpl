<div class="panel">
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Customer</th>
                <th>Type</th>
                <th>Verifié</th>
                <th>Voir la fiche</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$customers item=c}
            <tr>
                <td>{$c->id_customer}</td>
                <td>{$c->files[0]->date_add}</td>
                <td>{$c->customer_obj->firstname} {$c->customer_obj->lastname}</td>
                <td>{$c->np_customer_type_obj->name}</td>
                <td>
                    {if $c->verified}
                    <i class="icon-check"></i>
                    {else}
                    <i class="icon-remove"></i>
                    {/if}
                </td>
                <td><a href="{$link->getAdminLink('AdminCustomers')}&amp;id_customer={$c->id_customer}&amp;viewcustomer">Lien</a></td>
            </tr>
            {/foreach}
        </tbody>

    </table>
</div>
