{if isset($error)}
<div class="alert alert-{$error.type}">
    <h4>{$error.title}</h4>
    <p>{$error.txt}</p>
</div>
{/if}

<div class="row">
    <form class="" action="" method="post">
        <div class="panel col-lg-6">
            <h3><i class="icon icon-add"></i> Ajouter un jour férié</h3>
            <div class="form-group row">
                <label class="control-label col-lg-3" for="ean13">
                    <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.">
                        Jour
                    </span>
                </label>
                <div class="col-lg-3">
                    <input maxlength="13" type="text" name="nDay" value="0">
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-lg-3" for="ean13">
                    <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.">
                        Mois
                    </span>
                </label>
                <div class="col-lg-3">
                    <select class="" name="nMonth">
                        {foreach from=$months item=m key=i}
                        <option value="{$i+1}">{$m}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-lg-3" for="ean13">
                    <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Mettre 0 pour tout les ans.">
                        Tous les ans
                    </span>
                </label>
                <div class="col-lg-3">
                    <input type="checkbox" name="changeYearForm" value="0" onclick="setEveryYear(event)">
                </div>
            </div>
            <div class="form-group row" id="form-group-nyear">
                <label class="control-label col-lg-3" for="ean13">
                    <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Mettre 0 pour tout les ans.">
                        Année
                    </span>
                </label>
                <div class="col-lg-3">
                    <input maxlength="13" type="text"  name="nYear" value="0" id="nYear"><br />
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" name="submitAddHoliday" class="btn btn-default pull-right"><i class="process-icon-save"></i> Save and stay</button>
            </div>
        </div>
    </form>

    <div class="panel col-lg-6">
        <h3>Liste des jours fériés</h3>
        <table class="table">
            <tr>
                <td>Jour</td>
                <td>Mois</td>
                <td>Année</td>
                <td>Actions</td>
            </tr>
            {foreach from=$holidays item=h}
            <tr>
                <td>{$h->day}</td>
                <td>
                    {$months[$h->month-1]}
                </td>
                <td>
                    {if !$h->year}
                    Tous les ans
                    {else}
                    {$h->year}
                    {/if}
                </td>
                <td>
                    <form class="" action="" method="post">
                        <input type="hidden" name="nIdHoliday" value="{$h->id_np_calendar_holiday}">
                        <button type="submit" name="submitDeleteCalendarSlot"><i class="icon icon-trash"></i> </button>
                    </form>
                </td>
            </tr>
            {/foreach}
        </table>
    </div>
</div>
<script type="text/javascript">
    function setEveryYear(evt){
        var x = evt;
        console.log(evt)
        var e = document.getElementById('nYear');
        var fg = document.getElementById('form-group-nyear');
        if (evt.target.checked){
            e.readonly = true;
            e.value = 0;
            fg.style.display = 'none';
        } else {
            e.readonly = false;
            fg.style.display = 'block';
            e.value = new Date().getFullYear();
        }
    }
</script>
