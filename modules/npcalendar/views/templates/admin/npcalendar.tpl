<style media="screen">
#np-calendar td{
    height: 20px;
}
</style>


<form class="" action="" method="post">
    <div class="panel" id="currentCalendarSlot" style="display:none;">
        <h3 id="currentCalendarSlotTitle"></h3>
        <input type="hidden" id="idCurrentCalendarSlot" name="idCalendarSlot" value="">
        <div class="row">
            <div class="col-lg-4" id="currentCalendarSlotFirstBlock">

            </div>
            <div class="col-lg-4" id="currentCalendarSlotSecondBlock">

            </div>
            <div class="col-lg-4" id="currentCalendarSlotThirdBlock">
                <button class="btn btn-default" type="submit" onclick="confirmDeleteCalendarSlot()" name="submitDeleteCalendarSlot"><i class="icon icon-trash"></i> Supprimer le créneau</button>
            </div>
        </div>
    </div>
</form>


<div class="row">
    <div  class="col-lg-8 panel">
        <h3>Calendrier :</h3>
        {if isset($error)}
        <div class="alert alert-{$error.type}">
            {$error.title}<br />{$error.txt}
        </div>
        {/if}

        <div class="row">
            <div class="col-lg-1">
                <button type="button" name="button" onclick="goPastPage()"><</button>
            </div>
            <div class="col-lg-1">
                <button type="button" name="button" onclick="goNextPage()">></button>
            </div>
        </div>

        <div id="np-calendar">

        </div>
    </div>
    <form class="" action="" method="post">
        <div class="col-lg-4 panel">
            <div class="row">
                <h3 class="col-lg-12">Ajouter un créneau :</h3>
            </div>
            <div class="row">
                <div class="form-group redirect_product_options" style="">
                    <label class="control-label col-lg-4" for="redirect_type">
                        Utilisateur:
                    </label>
                    <div class="col-lg-8">
                        <select name="nIdCustomer" value="" onchange="loadCustomerPoint(event)">
                            <option value="">Choississez un utilisateur</option>
                            {foreach from=$customers item=customer}
                            <option value="{$customer.id_customer}">{$customer.firstname} {$customer.lastname}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group redirect_product_options" style="">
                    <label class="control-label col-lg-4" for="redirect_type">
                        Date:
                    </label>
                    <div class="col-lg-8">
                        <input type="date" name="nDate" value="" onchange="fillHourSelect(event)">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group redirect_product_options" style="">
                    <label class="control-label col-lg-4" for="redirect_type">
                        Hour:
                    </label>
                    <div class="col-lg-8">
                        <select class="" name="nHour" id="selectHour">
                            <option value="">Choississez une heure</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group redirect_product_options" style="">
                    <label class="control-label col-lg-4" for="redirect_type">
                        Forfait :
                    </label>
                    <div class="col-lg-8">
                        <select class="" name="nIdPoint" id="selectPoint">
                            <option value="0">Gratuit</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="panel-footer">
                <button type="submit" name="submitAddCalendarSlot" class="btn btn-default pull-right"><i class="process-icon-save"></i>Save</button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="../modules/npcalendar/views/js/classes/NPCalendar.js"></script>
<script type="text/javascript">
function loadCustomerPoint(event) {
    $.ajax({
        url : '../modules/npcalendar/ajax/getAvailableUserForfait.php',
        type : 'POST',
        cache : false,
        data : {
            ajax : true,
            id_customer : event.target.value,
            action : 'getCalendarSlot'
        },
        datatype : "json",
        success : function (res) {
            res = JSON.parse(res);
            console.log(res)
            var sp = document.getElementById('selectPoint');
            if (!res.error){
                for (var one of res.data){
                    var available = one.nb_point - one.np_calendar_slots.length;
                    if (available > 0){
                        var n = document.createElement('option');
                        n.value = one.id_np_customer_point;
                        n.text = one.product_obj.name + ' (' + available + ' restant(s))';
                        sp.appendChild(n)
                    }
                }
            } else {

            }
        }
    });
}

var date_start = null;
var date_end = null;
var c = null;

$(function() {
    c = new NPCalendar('np-calendar');
    c.createCalendar();
    getCalendarSlots();
});
function getCalendarSlots() {
    getCalendarSlotByInterval(c.date_start, c.date_end, (res) => {
        if (!res.error){
            for (var i = 0; i < res.data.length; i++){
                var html_id = res.data[i].date_slot + '-' + pad(res.data[i].hour);
                var td = document.getElementById(html_id)
                td.style.background = 'red';
                var btn = document.createElement("button");   // Create a <button> element
                btn.innerHTML = "<i class=\"icon icon-eye\"></i>";
                btn.value = "submitViewCalendarSlot"
                btn.name = "submitViewCalendarSlot"
                btn.type="submit"
                btn.className = "btn btn-default"
                btn.slot_data = res.data[i]
                btn.addEventListener('click', function() {
                    var id = this.slot_data.id_np_calendar_slot;
                    getCalendarSlotById(id, function(res) {
                        if (!res.error){
                            genViewCalendarSlot(res);
                            //document.getElementById('currentCalendarSlotThirdBlock').innerHTML = res.data.customer_obj.firstname + ' ' + res.data.customer_obj.lastname
                        } else {

                        }
                    })
                })
                td.appendChild(btn);
            }
        } else {

        }
    });
}
function goNextPage() {
    c.goNextPage()
    getCalendarSlots();

}
function goPastPage() {
    c.goPastPage()
    getCalendarSlots();
}

function genViewCalendarSlot(res) {
    console.log(res)
    var slot = res.data.slot;
    var point = res.data.point;
    var box = document.getElementById('currentCalendarSlot');
    var blocks = [document.getElementById('currentCalendarSlotFirstBlock'), document.getElementById('currentCalendarSlotSecondBlock')]
    var nIdCalendarSlot = document.getElementById('idCurrentCalendarSlot').value = slot.id_np_calendar_slot;
    for (block of blocks) {
        block.innerHTML = "";
    }
    box.style.display = 'block';
    document.getElementById('currentCalendarSlotTitle').innerHTML = "Créneau du " + slot.date_slot + " à " + slot.hour + ":00"
    blocks[0].innerHTML = 'Client : ' + slot.customer_obj.firstname + ' ' + slot.customer_obj.lastname
    blocks[0].innerHTML += '<br />Date : ' + slot.date_slot + ' à ' + slot.hour
    if (!point){
        blocks[1].innerHTML = 'Forfait : Gratuit'
    } else {
        blocks[1].innerHTML = 'Forfait : ' + point.product_obj.name
    }
}

function getCalendarSlotById(id, callback) {
    $.ajax({
        url : '../modules/npcalendar/ajax/getCalendarSlotById.php',
        type : 'POST',
        cache : false,
        data : {
            ajax : true,
            id_np_calendar_slot : id,
            action : 'getCalendarSlot'
        },
        datatype : "json",
        success : function (res) {
            callback(JSON.parse(res));
        }
    });
}
function confirmDeleteCalendarSlot() {
    if (confirm('Êtes-vous sûr(e) de vouloir supprimer ce creneau ?')){
        return true;
    }
    e.preventDefault();
}
</script>
