<div class="row">
  <div class="col-md-6" id="customerTypeForm" style="display:none;">
    {$nCustomerTypeForm}
  </div>
</div>
<div class="row">
  <div class="panel col-md-6">
    <div class="panel-heading">
      Clients
    </div>
    <div class="panel-body">
      <button type="button" name="button" class="btn btn-default" onclick="openCustomerTypeForm()"><i>+</i> Ajouter un type de client</button><br /><br />
      <div class="form-group">
        <label class="control-label col-lg-3" for="np_point_nb_day">Type de client :</label>
        <div class="input-group col-lg-6">
          <form class=""  action="" method="post">
            <select class="" name="id_type" onchange="changetypejs()">
              <option value="">Choississez un type de client</option>
              {html_options values=$customer_type_select.ids output=$customer_type_select.names selected=$current_id_np_customer_type}
            </select>
            <!--
            <select class="" name="id_type" value="{}">
              <option value="0">Choississez un type de client</option>
              {foreach from=$customer_types item=ct}
              <option value="{$ct->id}">{$ct->name}</option>
              {/foreach}
            </select>
          -->
            <input id="form_type"  style="display : none;" type="submit" name="changeType" value="changeType">
          </form>
        </div><br />
      </div>
      {if $current_id_np_customer_type}
      <h3 class="tab">Type : "{$current_np_customer_type->name}"</h3>
      <h4>Fichiers à envoyer</h4>
      <article class="alert alert-info">
        <p>
          Les "fichiers à envoyer" sont les fichiers que les "{$current_np_customer_type->name}s" doivent fournir afin de pouvoir commander des forfaits et acceder a l'agenda.
        </p>
      </article>
      <form class="" action="" method="post">
        <input type="hidden" name="id_type" value="{$current_np_customer_type->id}">
        <button class="btn btn-default" type="submit" name="addFtu">Aouter un fichier a télécharger</button>
      </form>
      <br />
      {if count($current_np_customer_type->np_file_to_uploads) == 0}
        <h4>Aucun fichier parametré</h4>

      {else}
      <table class="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Description</th>
            <th style="min-width:100px;">Actions</th>
          </tr>
          {foreach from=$current_np_customer_type->np_file_to_uploads item=ftu}
          <tr>
            <td>{$ftu->id}</td>
            <td>{$ftu->name}</td>
            <td>{$ftu->description}</td>
            <td>
              <form class="" action="" method="post">
                <input type="hidden" name="id_ftu" value="{$ftu->id}">
                <input type="hidden" name="id_type" value="{$ftu->id_np_customer_type}">
                <button class="btn btn-default" type="submit" name="viewFtuDetails"><i class="icon-eye"></i></button>
                <button class="btn btn-default" type="submit" name="deleteFtu" onclick="verifyUser(event)"><i class="icon-trash"></i></button>
              </form>
            </td>
          </tr>
          {/foreach}
        </thead>
      </table>
      {/if}
      {else}
      <article class="alert alert-info">
        Veuillez choisir un type dans le menu déroulant.
      </article>
      {/if}
    </div>
  </div>
  {if isset($ftuForm)}
  <div class="col-md-6">
    {$ftuForm}
  </div>

  {/if}
</div>
<script type="text/javascript">
  function changetypejs() {
    document.getElementById('form_type').click();
  }
  function openCustomerTypeForm(){
    var s = document.getElementById('customerTypeForm').style;
    s.display = (s.display == 'block')? 'none': 'block';
  }
  function verifyUser(e) {
    if (confirm('Êtes-vous sur de vouloir supprimer ce fichier a télécharger ?')){
      return true;
    }
    e.stopPropagation();
    e.preventDefault();
  }
</script>
