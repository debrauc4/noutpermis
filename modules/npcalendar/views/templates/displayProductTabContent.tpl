<style media="screen">
.whiteblock{
  border: 1px solid #d6d4d4;
  padding-top: 1px !important;
  background-color: white !important;
  padding: 10px;
  border-radius : 4px;
  margin-bottom: 7px;
}
</style>
<!-- full description -->
{if !$pg->hide && $pg->nb_point > 0}
<div class="whiteblock">
  <h3>Heures de conduite</h3>
  <p>
    <b>{$pg->nb_point}</b> heure(s) de conduite vous seront ajoutée(s). Pour une durée de validité de <b>{$pg->nb_day}</b> jour(s).<br />
    Vous aurez la possibilité de les utiliser
    {if $pg->weekend}
    la semaine, le weekend et les jours fériés.
    {else}
    en semaine uniquement.
    {/if}
    <br />
    Ces heures sont {if !$pg->splittable}non {/if}fractionnables.
  </p>
</div>
{/if}
