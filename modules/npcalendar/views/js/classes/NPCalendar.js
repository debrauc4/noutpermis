/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

function pad(x){
    return (x < 10) ? '0' + x : x;
}

class NPCalendar {
    constructor(div_id, hour_start, hour_end) {
        this.div = document.getElementById(div_id);
        this.hour_start = (hour_start)? hour_start : 7;
        this.hour_end = (hour_end)? hour_end : 21;
    }
    static getWeek(date_start){
        var monday = NPCalendar.getMonday(date_start);
        var next_monday = new Date(monday);
        next_monday.setDate(next_monday.getDate() + 7);
        return {
            start : monday,
            end : next_monday,
        }
    }
    static getMonday(date_start){
        var d = (date_start) ? new Date(date_start) : new Date();
        var day = d.getDay();
        var diff = d.getDate() - day + (day == 0 ? -6:1)
        return new Date(d.setDate(diff));
    }
    getFrDate(d){
        return ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'][d.getDay()];
    }
    static pad(x){
        return (x < 10) ? '0' + x : x;
    }
    getDateLabel(d, nodate=false){
        var txt = "";
        if (!nodate){
            txt = this.getFrDate(d);
        }
        txt += pad(d.getDate());
        txt += '-';
        txt += pad(d.getMonth() + 1) + '-' + d.getFullYear();
        return txt;
    }
    getTdHtmlId(d){
        var txt = d.getFullYear() + '-'
        txt += pad(d.getMonth() + 1) + '-'
        txt += pad(d.getDate());
        return txt;
    }
    createCalendar(date_start){
        this.div.innerHTML = "";// clean up
        var d = NPCalendar.getMonday(date_start);
        this.date_start = new Date(d);
        var nb_hours = this.hour_end - this.hour_start;
        var tbl = document.createElement('table');
        tbl.style.width = '100%';
        tbl.setAttribute('border', '1');
        var tbdy = document.createElement('tbody');
        var tr = document.createElement('tr');
        //Date
        tr.appendChild(document.createElement('th'));
        for (var j = 0; j < 7; j++) {
            var th = document.createElement('th');
            th.appendChild(document.createTextNode(this.getDateLabel(d)))
            tr.appendChild(th);
            d.setDate(d.getDate() + 1);
        }
        this.date_end = new Date(d);
        tbdy.appendChild(tr);
        for (var i = 0; i < nb_hours; i++) {
            var tr = document.createElement('tr');
            //slot
            var td = document.createElement('td');
            td.innerHTML = this.hour_start + i + ":00"
            tr.appendChild(td);
            var d = NPCalendar.getMonday(date_start);
            for (var j = 0; j < 7; j++) {
                var td = document.createElement('td');
                td.id = this.getTdHtmlId(d) + '-' + pad((this.hour_start + i));
                td.appendChild(document.createTextNode('\u0020'))
                tr.appendChild(td)
                d.setDate(d.getDate() + 1);
            }
            tbdy.appendChild(tr);
        }
        tbl.appendChild(tbdy);
        this.div.appendChild(tbl);
    }
    goNextPage(){
        var nDate = new Date(this.date_end);
        nDate.setDate(nDate.getDate() + 1);
        this.createCalendar(nDate);
    }
    goPastPage(){
        var nDate = new Date(this.date_start);
        nDate.setDate(nDate.getDate() - 1);
        this.createCalendar(nDate);
    }
}

function fillHourSelect(evt, front=undefined){
    var d = new Date(evt.target.value);
    var sh = document.getElementById('selectHour');
    var url = (front)? './' : '../';
    url += 'modules/npcalendar/ajax/getHourUnavailable.php'
    $.ajax({
        url : url,
        type : 'POST',
        cache : false,
        data : {
            ajax : true,
            day : d.toISOString().slice(0, 19).replace('T', ' '),
            action : 'getHourUnavailable'
        },
        datatype : "json",
        success : function (result) {
            var packet = JSON.parse(result);
            var start = 7;
            var end = 21;
            var htmlOptions = [];
            for (var i = start; i < end; i++){
                htmlOptions.push(i);
            }
            packet.result.forEach((one) => {
                var index = htmlOptions.indexOf(parseInt(one.hour));
                if (index > -1) {
                    htmlOptions.splice(index, 1);
                }
            });
            sh.options.length = 1;
            htmlOptions.forEach((one) => {
                var n = document.createElement('option');
                n.value = one;
                n.text = one + ":00";
                sh.appendChild(n);
            })
        }
    });
}

function getAvailableUserForfait(id_customer, callback) {
    $.ajax({
        url : '../modules/npcalendar/ajax/getAvailableUserForfait.php',
        type : 'POST',
        cache : false,
        data : {
            ajax : true,
            id_customer : id_customer,
            action : 'getCalendarSlot'
        },
        datatype : "json",
        success : function (result) {
            callback(JSON.parse(result));
        }
    });
}

function getCalendarSlotByInterval(date_start, date_end, callback) {
    var d1 = new Date(date_start);
    d1 = [pad(d1.getFullYear()), pad(d1.getMonth()+1), pad(d1.getDate())].join('-');
    var d2 = new Date(date_end);
    d2 = [pad(d2.getFullYear()), pad(d2.getMonth()+1), pad(d2.getDate())].join('-');
    $.ajax({
        url : '../modules/npcalendar/ajax/getCalendarSlotByInterval.php',
        type : 'POST',
        cache : false,
        data : {
            ajax : true,
            date_start : d1,
            date_end : d2,
            action : 'getCalendarSlot'
        },
        datatype : "json",
        success : function (result) {
            callback(JSON.parse(result));
        }
    });
}
