<?php

require_once('../../../config/config.inc.php');
require_once('../../../init.php');
require_once('../classes/NPCustomerPoint.php');
require_once('../classes/NPCalendarSlot.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (isset($_POST['id_customer'])){
    $idc = $_POST['id_customer'];
    if ($idc > 0){
        $slots = NPCustomerPoint::getAvailableByIdCustomer($idc);
    } else {
        echo json_encode(array('error' => true));
    }
    echo json_encode(array('data' => $slots, 'error' => false));
} else {
    echo json_encode(array('error' => true));
}

exit;
