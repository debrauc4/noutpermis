<?php
require_once('../../../config/config.inc.php');
require_once('../../../init.php');
require_once('../classes/NPCalendarSlot.php');
if (isset($_POST['day'])){
    $day = $_POST['day'];
    $cs = NPCalendarSlot::getByDay($day);
    echo json_encode(array('result' => $cs));
} else {
    echo json_encode(array('error' => true));
}

exit;
