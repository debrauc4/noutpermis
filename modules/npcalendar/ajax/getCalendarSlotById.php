<?php
require_once('../../../config/config.inc.php');
require_once('../../../init.php');
require_once('../classes/NPCalendarSlot.php');
require_once('../classes/NPCustomerPoint.php');
if (isset($_POST['id_np_calendar_slot'])){
    $id = $_POST['id_np_calendar_slot'];
    $slot = new NPCalendarSlot($id);
    $point = ($slot->id_np_customer_point == 0) ? null : new NPCustomerPoint($slot->id_np_customer_point);
    echo json_encode(array(
        'data' => array('slot' => $slot, 'point' => $point),
        'error' => false
    ));
} else {
    echo json_encode(array('error' => true));
}

exit;
