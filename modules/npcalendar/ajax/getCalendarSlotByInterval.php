<?php
require_once('../../../config/config.inc.php');
require_once('../../../init.php');
require_once('../classes/NPCalendarSlot.php');
if (isset($_POST['date_start']) && isset($_POST['date_end'])){
    $d1 = $_POST['date_start'];
    $d2 = $_POST['date_end'];
    $slots = NPCalendarSlot::getByInterval($d1, $d2);
    echo json_encode(array('data' => $slots, 'error' => false));
} else {
    echo json_encode(array('error' => true));
}

exit;
