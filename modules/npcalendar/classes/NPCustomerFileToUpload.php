<?php
/*
** Created for nout'permis, all right reserved
**
*/
class NPCustomerFileToUpload extends ObjectModel
{
    public $id;
    public $id_np_customer_type;
    public $description;
    public $name;
    public $file_example;
    public static $definition = array(
        'table' => 'np_customer_file_to_upload',
        'primary' => 'id_np_customer_file_to_upload',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          'id_np_customer_type' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'description' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
          'name' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
          'file_example' => array('type' => self::TYPE_STRING),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
      parent::__construct($id, $id_lang, $id_shop);

    }

    public function add($autodate = true, $null_values = false)
    {
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
    }

    public function delete(){
      parent::delete();
    }

    private static function _genByDb($arr){
        $primary = self::$definition['primary'];
      $ret = array();
      foreach($arr as &$one){
        $ret[] = new NPCustomerFileToUpload($one[$primary]);
      }
      return $ret;
    }

    public static function getByIdNpCustomerType($id_np_customer_type){
        $primary = self::$definition['primary'];
        $sql = 'SELECT '. $primary .' FROM '._DB_PREFIX_.NPCustomerFileToUpload::$definition['table'] .' WHERE id_np_customer_type = ' . $id_np_customer_type;
        $rq = Db::getInstance()->ExecuteS($sql);
        return NPCustomerFileToUpload::_genByDb($rq);
    }

    //install / uninstall
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. self::$definition['table'];
        $primary = self::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `id_np_customer_type` int(11) NOT NULL,
            `description` varchar(250) NOT NULL,
            `name` varchar(75) NOT NULL,
            `file_example` varchar(200) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. self::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
