<?php
/*
** Created for nout'permis, all right reserved
**
*/
class NPCustomerPoint extends ObjectModel
{
    public $id;
    public $id_order;
    public $id_customer;
    public $id_product;
    public $nb_point;
    public $date_add;
    public $date_end;
    public $debug;
    public $np_calendar_slots;
    public $product_obj;
    public $weekend;
    public $splittable;
    public $id_np_customer_point;
    public static $definition = array(
        'table' => 'np_customer_point',
        'primary' => 'id_np_customer_point',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'id_order' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'nb_point' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'weekend' => array('type' => self::TYPE_INT, 'validate' => 'isBool'),
          'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
          'date_end' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
          'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
          'splittable' => array('type' => self::TYPE_INT, 'validate' => 'isBool'),

        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
      parent::__construct($id, $id_lang, $id_shop);
      if ($this->id){
        $this->initObjects();
      }
    }

    private function initObjects(){
        $this->np_calendar_slots = NPCalendarSlot::getByIdNpCustomerPoint($this->id);
        if ($this->id_product){
          $this->product_obj = new Product($this->id_product, false, Context::getContext()->language->id);
        }
    }

    public function add($autodate = true, $null_values = false)
    {
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
    }

    public function delete(){
      parent::delete();
    }

    private static function _genByDb($arr){
        $primary = self::$definition['primary'];
      $ret = array();
      foreach($arr as &$one){
        $ret[] = new NPCustomerPoint($one[$primary]);
      }
      return $ret;
    }

    public static function getByIdCustomer($id_customer){
        $primary = self::$definition['primary'];
      $sql = 'SELECT '.$primary.' FROM '._DB_PREFIX_.NPCustomerPoint::$definition['table'] . ' WHERE id_customer = ' . $id_customer;
      $rq = Db::getInstance()->ExecuteS($sql);
      return NPCustomerPoint::_genByDb($rq);
    }

    public static function getForfaitName($id_np_customer_point){
        $sql = 'SELECT id_product FROM '._DB_PREFIX_.NPCustomerPoint::$definition['table'] . ' WHERE id_np_customer_point = ' . $id_np_customer_point;
        $rq = Db::getInstance()->getRow($sql);
        $p = new Product($rq['id_product'], false, Context::getContext()->language->id);
        return $p->name;
    }

    public static function getAvailableByIdCustomer($id_customer){
        $primary = self::$definition['primary'];
        $tbl = _DB_PREFIX_.NPCustomerPoint::$definition['table'];
        $sql = 'SELECT '.$primary.' FROM '. $tbl;
        $sql .= ' WHERE id_customer = ' . $id_customer;
        $sql .= ' AND date_end < \'NOW()\' ';
        $rq = Db::getInstance()->ExecuteS($sql);
        return NPCustomerPoint::_genByDb($rq);
    }

    public function alreadyExist(){
        $primary = self::$definition['primary'];
      $sql = 'SELECT '.$primary.' FROM '._DB_PREFIX_.NPCustomerPoint::$definition['table'] . ' WHERE id_customer = ' . $this->id_customer . ' AND id_order = ' . $this->id_order . ' AND id_product = ' . $this->id_product;
      $rq = Db::getInstance()->ExecuteS($sql);
      return $rq && count($rq) > 0;
    }

    public static function addFromNPPointGestion($np_point_gestion, $id_order, $id_customer, $quantity){
      $r = new NPCustomerPoint();
      $r->id_order = $id_order;
      $r->id_product = $np_point_gestion->id_product;
      $r->id_customer = $id_customer;
      $r->nb_point = $np_point_gestion->nb_point * $quantity;
      $r->weekend = $np_point_gestion->weekend;
      $r->splittable = $np_point_gestion->splittable;
      $r->date_end = date('Y-m-d H:i:s', strtotime(date('Y-m-d'). ' + '. $np_point_gestion->nb_day .' days'));
      //if (!$r->alreadyExist())
        $r->add();
    }

    /*
    ** For handle form to add customer calendar slot
    **
    */
    private function addSplittableSlot($id_customer, $nDate, $nHour){
        $n = new NPCalendarSlot();
        $n->id_customer = $id_customer;
        $n->id_np_customer_point = $this->id_np_customer_point;
        $n->date_slot = $nDate;
        $n->hour = $nHour;
        return $n->add();
    }

    private function addUnsplittableSlot($id_customer, $nDate, $nHour){
        $slots = array();
        $exist = false;
        for ($i=0; $i < $this->nb_point; $i++) {
            $n = new NPCalendarSlot();
            $n->date_slot = $nDate;
            $n->hour = $nHour++;
            $n->id_customer = $id_customer;
            $n->id_np_customer_point = $this->id_np_customer_point;
            if ($n->alreadyExist()){
                $exist = true;
                break;
            }
            $slots[] = $n;
        }
        if (!$exist) {
            foreach ($slots as $slot) {
                $slot->add();
            }
        } else {
        }
    }

    public function addSlotForCustomer($id_customer, $nDate, $nHour){
        if ($this->splittable){
            $this->addSplittableSlot($id_customer, $nDate, $nHour);
        } else {
            $this->addUnsplittableSlot($id_customer, $nDate, $nHour);
        }
    }




    //install / uninstall
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. self::$definition['table'];
        $primary = self::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `id_order` int(11) NOT NULL,
            `id_product` int(11) NOT NULL,
            `id_customer` int(11) NOT NULL,
            `nb_point` int(11) NOT NULL,
            `weekend` tinyint(4) NOT NULL,
            `date_add` date NOT NULL,
            `date_upd` date NOT NULL,
            `date_end` date NOT NULL,
            `splittable` tinyint(4) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. self::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
