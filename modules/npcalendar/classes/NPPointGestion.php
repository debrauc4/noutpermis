<?php
/*
** Created for nout'permis, all right reserved
**
*/
class NPPointGestion extends ObjectModel
{
    public $id;
    public $id_np_point_gestion;
    public $id_product;
    public $nb_point;
    public $nb_day;
    public $weekend;
    public $hide;
    public $splittable;

    public static $definition = array(
        'table' => 'np_point_gestion',
        'primary' => 'id_np_point_gestion',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'nb_point' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'nb_day' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'weekend' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'splittable' => array('type' => self::TYPE_INT, 'validate' => 'isBool'),
          'hide' => array('type' => self::TYPE_INT, 'validate' => 'isBool'),
        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
        parent::__construct($id, $id_lang, $id_shop);
    }

    public function add($autodate = true, $null_values = false)
    {
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
    }

    public function delete(){
      parent::delete();
    }

    public static function getByIdProduct($id_product){
        $primary = self::$definition['primary'];

      $sql = 'SELECT '. $primary .' FROM '._DB_PREFIX_.NPPointGestion::$definition['table'].' WHERE id_product = '. $id_product;
      $rq = Db::getInstance()->getRow($sql);
      return new NPPointGestion($rq[$primary]);
    }

    private static function _genByDb($arr){
        $primary = self::$definition['primary'];
      $ret = array();
      foreach($arr as &$one){
        $ret[] = new NPPointGestion($one[$primary]);
      }
      return $ret;
    }

    public static function getByIdCustomer($id_customer){
        $primary = self::$definition['primary'];
      $sql = 'SELECT '. $primary .' FROM '._DB_PREFIX_.NPPointGestion::$definition['table'].' WHERE id_customer = '. $id_customer;
      $rq = Db::getInstance()->ExecuteS($sql);
      return NPPointGestion::_genByDb($rq);
    }

    public static function alreadyExist($id_product){
        $primary = self::$definition['primary'];
      $sql = 'SELECT '. $primary .' FROM '._DB_PREFIX_.NPPointGestion::$definition['table'].' WHERE id_product = '. $id_product;
      $rq = Db::getInstance()->getRow($sql);
      return ($rq && $rq[$primary] > 0);
    }

    //install / uninstall
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. self::$definition['table'];
        $primary = self::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `id_product` int(11) NOT NULL,
            `nb_day` int(11) NOT NULL,
            `weekend` int(11) NOT NULL,
            `splittable` int(11) NOT NULL,
            `hide` tinyint(4) NOT NULL,
            `nb_point` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
        }

        public static function uninstallDb()
        {
            $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. self::$definition['table'];
            $drop = "DROP TABLE " . $tbl;
            $truncate = "TRUNCATE ". $tbl;
            Db::getInstance()->execute($truncate);
            Db::getInstance()->execute($drop);
        }

}
