<?php
/*
** Created for nout'permis, all right reserved
**
*/
class NPAppCms extends ObjectModel
{
    public $id_app_cms;
    public $id_cms;
    public $title;
    public $subtitle;
    public $img_link;
    public $id_app_cms_category;
    public $cms_obj;
    public static $definition = array(
        'table' => 'app_cms',
        'primary' => 'id_app_cms',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          'id_cms' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
          'title' => array('type' => self::TYPE_STRING),
          'subtitle' => array('type' => self::TYPE_STRING),
          'img_link' => array('type' => self::TYPE_STRING),
          'id_app_cms_category' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
      parent::__construct($id, $id_lang, $id_shop);
      if ($this->id && $this->id_cms){
        $sql = 'SELECT * FROM '._DB_PREFIX_ .'cms_lang WHERE id_cms = ' . $this->id_cms ;
        $this->cms_obj = Db::getInstance()->ExecuteS($sql);
      }
    }

    public function add($autodate = true, $null_values = false)
    {
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
    }

    public function delete(){
      parent::delete();
    }

    private static function _genByDb($arr){
      $ret = array();
      $id = NPAppCms::$definition['primary'];
      foreach($arr as &$one){
        $ret[] = new NPAppCms($one[$id]);
      }
      return $ret;
    }

    public static function getAll(){
      $sql = 'SELECT '. NPAppCms::$definition['primary'] .' FROM '._DB_PREFIX_.NPAppCms::$definition['table'];
      $rq = Db::getInstance()->ExecuteS($sql);
      return NPAppCms::_genByDb($rq);
    }

    public static function getByIdCategory($id_category){
      $sql = 'SELECT '. NPAppCms::$definition['primary'] .' FROM '._DB_PREFIX_.NPAppCms::$definition['table'] . ' WHERE id_app_cms_category = ' . $id_category;
      $rq = Db::getInstance()->ExecuteS($sql);
      return NPAppCms::_genByDb($rq);
    }

}
