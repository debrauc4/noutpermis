<?php
/*
** Created for nout'permis, all right reserved
**
*/
class NPCustomerFile extends ObjectModel
{
    public $id;
    public $id_np_customer_file;
    public $id_np_customer_file_to_upload;
    public $id_np_customer;
    public $date_add;
    public $file;
    public $np_customer_file_to_upload_obj;
    public $np_customer_obj;
    public static $definition = array(
        'table' => 'np_customer_file',
        'primary' => 'id_np_customer_file',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          'id_np_customer_file_to_upload' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'id_np_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'file' => array('type' => self::TYPE_STRING),
          'date_add' => array('type' => self::TYPE_DATE),
        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
      parent::__construct($id, $id_lang, $id_shop);
      if ($this->id && $this->id_np_customer_file_to_upload > 0){
        $this->np_customer_file_to_upload_obj = new NPCustomerFileToUpload($this->id_np_customer_file_to_upload);
      }
    }
    public function add($autodate = true, $null_values = false)
    {
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
    }

    public function delete(){
      $pos = strrpos($this->file, '/');
      $id = $pos === false ? $this->file : substr($this->file, $pos + 1);
      $url = _PS_UPLOAD_DIR_ . $id;
      unlink(realpath($url));
      parent::delete();
    }

    public static function getByIdCustomer($id_customer){
        $npc = NPCustomer::getByIdCustomer($id_customer);
        $id = $npc->id_np_customer;
        return self::getByIdNpCustomer($id);
    }

    private static function _genByDb($arr){
        if (!$arr){
            return null;
        }
        $primary = self::$definition['primary'];
        $ret = array();
        foreach($arr as &$one){
            $ret[] = new NPCustomerFile($one[$primary]);
        }
        return $ret;
    }

    public static function getByIdNpCustomer($id_np_customer){
        $primary = self::$definition['primary'];
      $sql = 'SELECT '. $primary .' FROM '._DB_PREFIX_.NPCustomerFile::$definition['table'].' WHERE id_np_customer = ' . $id_np_customer;
      $rq = Db::getInstance()->ExecuteS($sql);
      return NPCustomerFile::_genByDb($rq);
    }

    public static function getAll(){
        $primary = self::$definition['primary'];
        $sql = 'SELECT '. $primary .' FROM '._DB_PREFIX_.NPCustomerFile::$definition['table'] . ' GROUP BY id_np_customer ORDER BY date_add DESC ';
        $rq = Db::getInstance()->ExecuteS($sql);
        return NPCustomerFile::_genByDb($rq);
    }

    //install / uninstall
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. self::$definition['table'];
        $primary = self::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `id_np_customer_file_to_upload` int(11) NOT NULL,
            `id_np_customer` int(11) NOT NULL,
            `date_add` date NOT NULL,
            `file` varchar(100) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. self::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
