<?php
/*
** Created for nout'permis, all right reserved
**
*/
class NPAppCmsCategoryCore extends ObjectModel
{
    public $id_app_cms_category;
    public $title;
    public $subtitle;
    public $img_link;
    public $app_cmss;
    public static $definition = array(
        'table' => 'app_cms_category',
        'primary' => 'id_app_cms_category',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          'title' => array('type' => self::TYPE_STRING),
          'subtitle' => array('type' => self::TYPE_STRING),
          'img_link' => array('type' => self::TYPE_STRING),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
      parent::__construct($id, $id_lang, $id_shop);
      if ($this->id_app_cms_category){
        $this->app_cmss = NPAppCms::getByIdCategory($this->id_app_cms_category);
      }
    }

    public function add($autodate = true, $null_values = false)
    {
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
    }

    public function delete(){
      parent::delete();
    }

    private static function _genByDb($arr){
      $ret = array();
      $id = NPAppCmsCategory::$definition['primary'];
      foreach($arr as &$one){
        $ret[] = new NPAppCmsCategory($one[$id]);
      }
      return $ret;
    }

    public static function getAll(){
      $sql = 'SELECT '. NPAppCmsCategory::$definition['primary'] .' FROM '._DB_PREFIX_.NPAppCmsCategory::$definition['table'];
      $rq = Db::getInstance()->ExecuteS($sql);
      return NPAppCmsCategory::_genByDb($rq);
    }

}
