<?php
/*
** Created for nout'permis, all right reserved
**
*/
class NPCalendarHoliday extends ObjectModel
{
    public $id_np_calendar_holiday;
    public $day;
    public $month;
    public $year;
    public static $definition = array(
        'table' => 'np_calendar_holiday',
        'primary' => 'id_np_calendar_holiday',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'day' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'month' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'year' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
      parent::__construct($id, $id_lang, $id_shop);
    }

    public function add($autodate = true, $null_values = false)
    {
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
      return true;
    }

    public function delete(){
      parent::delete();
    }

    private static function _genByDb($arr){
        $primary = self::$definition['primary'];
      $ret = array();
      foreach($arr as &$one){
        $ret[] = new NPCalendarHoliday($one[$primary]);
      }
      return $ret;
    }

    public static function getAll(){
        $primary = self::$definition['primary'];
        $sql = 'SELECT '.$primary.' FROM '._DB_PREFIX_.self::$definition['table'];
        $rq = Db::getInstance()->ExecuteS($sql);
        return NPCalendarHoliday::_genByDb($rq);
    }

    public function exist(){
        $primary = self::$definition['primary'];
        $sql = 'SELECT '.$primary.' FROM '._DB_PREFIX_.self::$definition['table'];
        $sql .= ' WHERE day = ' . $this->day;
        $sql .= ' AND month = ' . $this->month;
        $sql .= ' AND year = ' . $this->year;
        $rq = Db::getInstance()->getRow($sql);
        return $rq && $rq[$primary] > 0;
    }

    //install / uninstall
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. self::$definition['table'];
        $primary = self::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `day` int(11) NOT NULL,
            `month` int(11) NOT NULL,
            `year` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. self::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
