<?php
/*
** Created for nout'permis, all right reserved
**
*/
require_once(dirname(__FILE__).'/NPCustomer.php');
require_once(dirname(__FILE__).'/NPCustomerFile.php');
require_once(dirname(__FILE__).'/NPCustomerPoint.php');

class NPCustomer extends ObjectModel
{
    public $id;
    public $id_np_customer;
    public $verified;
    public $id_customer;
    public $id_np_customer_type;
    public $customer_obj;
    public $np_customer_type_obj;
    public $files;
    public $np_customer_point;
    public $dispo_point;
    public static $definition = array(
        'table' => 'np_customer',
        'primary' => 'id_np_customer',//Change in mam website (id -> id_np_customer)
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          'verified' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'id_np_customer_type' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
      parent::__construct($id, $id_lang, $id_shop);
      if ($id){
        $this->initObject();

      }
    }
    public function initObject(){
      $this->files = NPCustomerFile::getByIdNpCustomer($this->id_np_customer);
      $this->customer_obj = new Customer($this->id_customer);
      if ($this->verified){
        $this->np_customer_point = NPCustomerPoint::getByIdCustomer($this->id_customer);
        $weekend = 0;
        $week = 0;
        foreach ($this->np_customer_point as $point){
          $pg = NPPointGestion::getByIdProduct($point->id_product);
          $d = date('Y-m-d');
          if ($d < $point->date_end){
            if ($pg->weekend){
              $weekend += ($point->nb_point - count($point->np_calendar_slots));
            } else {
              $week += ($point->nb_point) - count($point->np_calendar_slots);
            }
          }
        }
        $this->dispo_point = array(
          'weekend' => $weekend,
          'week' => $week,
        );
      }
      if ($this->id_np_customer_type){
        $this->np_customer_type_obj = new NPCustomerType($this->id_np_customer_type);
      }
    }
    public function add($autodate = true, $null_values = false)
    {
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
    }

    public function delete(){
      parent::delete();
    }

    public function cancelDemande(){
      $this->id_np_customer_type = 0;
      foreach($this->files as $file){
        $file->delete();
      }
      $this->save();
    }

    private static function _genByDb($arr){
        $primary = self::$definition['primary'];

      $ret = array();
      foreach($arr as &$one){
        $ret[] = new NPCustomer($one[$primary]);
      }
      return $ret;
    }

    public static function getCustomers(){
        $primary = self::$definition['primary'];

      $sql = 'SELECT '. $primary .' FROM '._DB_PREFIX_.NPCustomer::$definition['table'];
      $rq = Db::getInstance()->executeS($sql);
      return NPCustomer::_genByDb($rq);
    }

    public static function getByIdCustomer($id_customer){
        $primary = self::$definition['primary'];

      $sql = 'SELECT '.$primary.' FROM '._DB_PREFIX_.NPCustomer::$definition['table'].' WHERE id_customer = ' . $id_customer;
      $rq = Db::getInstance()->getRow($sql);
      if ($rq){
        return new NPCustomer($rq[$primary]);
      }
      $n = new NPCustomer();
      $n->id_customer = $id_customer;
      $n->save();
      $n->initObject();
      return $n;
    }

    //install / uninstall
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. self::$definition['table'];
        $primary = self::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `verified` tinyint(1) NOT NULL,
            `id_np_customer_type` int(11) NOT NULL,
            `id_customer` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
        NPCustomer::installDbSetCustomer();
    }

    public static function installDbSetCustomer(){
        $customers = Customer::getCustomers();
        foreach ($customers as $c) {
            $n = new NPCustomer();
            $n->id_customer = $c['id_customer'];
            $n->verified = 0;
            $n->id_np_customer_type = 0;
            $n->save();
        }
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. self::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
