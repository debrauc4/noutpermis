<?php

/*
** Created for nout'permis, all right reserved
**
*/
class NPCustomerType extends ObjectModel
{
    public $id;
    public $name;
    public $np_file_to_uploads;
    public static $definition = array(
        'table' => 'np_customer_type',
        'primary' => 'id_np_customer_type',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'name' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
        parent::__construct($id, $id_lang, $id_shop);
        if ($id){
            $this->np_file_to_uploads = NPCustomerFileToUpload::getByIdNpCustomerType($id);
        }
    }
    public function add($autodate = true, $null_values = false)
    {
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete(){
        parent::delete();
    }

    private static function _genByDb($arr){
        $primary = self::$definition['primary'];
        $ret = array();
        foreach($arr as &$one){
            $ret[] = new NPCustomerType($one[$primary]);
        }
        return $ret;
    }

    public static function getCustomerTypes(){
        $primary = self::$definition['primary'];
        $sql = 'SELECT '. $primary .' FROM '._DB_PREFIX_.NPCustomerType::$definition['table'];
        $rq = Db::getInstance()->ExecuteS($sql);
        return NPCustomerType::_genByDb($rq);
    }

    //install / uninstall
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. self::$definition['table'];
        $primary = self::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `name` varchar(50) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. self::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
