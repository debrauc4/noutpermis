<?php
/*
** Created for nout'permis, all right reserved
**
*/
require_once('NPCustomerPoint.php');

class NPCalendarSlot extends ObjectModel
{
    public $id_np_calendar_slot;
    public $id_customer;
    public $hour;
    public $date_slot;
    public $customer_obj;
    public $id_np_customer_point;
    public $forfaitName;

    public static $definition = array(
        'table' => 'np_calendar_slot',
        'primary' => 'id_np_calendar_slot',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          'date_slot' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
          'hour' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
          'id_np_customer_point' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null, Context $context = null){
      parent::__construct($id, $id_lang, $id_shop);
      if ($id){
        $this->customer_obj = new Customer($this->id_customer);
        if ($this->id_np_customer_point){
            $this->forfaitName = NPCustomerPoint::getForfaitName($this->id_np_customer_point);
        } else {
            $this->forfaitName = 'Gratuit';
        }
      }
    }

    public function add($autodate = true, $null_values = false)
    {
      if ($this->alreadyExist())
        return ;
      if (!parent::add($autodate, $null_values)) {
        return false;
      }
    }

    public function delete(){
      return parent::delete();
    }

    private static function _genByDb($arr){
      $ret = array();
      foreach($arr as &$one){
        $ret[] = new NPCalendarSlot($one['id_np_calendar_slot']);
      }
      return $ret;
    }

    public function alreadyExist(){
      $sql = 'SELECT '. self::$definition['primary'] .' FROM '._DB_PREFIX_.self::$definition['table'].' WHERE date_slot = \''. $this->date_slot .'\' AND hour = \'' . $this->hour . '\'';
      $rq = Db::getInstance()->executeS($sql);
      return (count($rq) != 0);
    }

    public static function getByInterval($date1, $date2){
      $date1 = date('Y-m-d', strtotime($date1));
      $date2 = date('Y-m-d', strtotime($date2));
      $sql = 'SELECT '. self::$definition['primary'] .' FROM '._DB_PREFIX_.self::$definition['table'].' WHERE date_slot >= \''. $date1 .'\' AND date_slot <= \''. $date2 . '\'';
      $rq = Db::getInstance()->executeS($sql);
      return self::_genByDb($rq);
    }

    public static function getByDay($date){
      $date = date('Y-m-d', strtotime($date));
      $sql = 'SELECT '. self::$definition['primary'] .' FROM '._DB_PREFIX_.self::$definition['table'].' WHERE date_slot = \''. $date .'\'';
      $rq = Db::getInstance()->executeS($sql);
      return self::_genByDb($rq);
    }

    public static function genByCustomValue($hour, $day, $month, $year, $id_customer, $id_np_customer_point){
      $n = new NPCalendarSlot();
      $n->date_slot = date("Y-m-d 00:00:00", strtotime($day . '-' . $month . '-' . $year));
      $n->hour = $hour;
      $n->id_customer = $id_customer;
      $n->id_np_customer_point = $id_np_customer_point;
      return $n;
    }

    public static function getByIdNpCustomerPoint($id_np_customer_point){
      $sql = 'SELECT '. self::$definition['primary'] .' FROM '._DB_PREFIX_.self::$definition['table'] . ' WHERE id_np_customer_point = ' . $id_np_customer_point;
      $rq = Db::getInstance()->executeS($sql);
      return self::_genByDb($rq);
    }

    public static function getByIdCustomer($id_customer){
      $sql = 'SELECT '. self::$definition['primary'] .' FROM '._DB_PREFIX_.self::$definition['table'] . ' WHERE id_customer = ' . $id_customer . ' ORDER BY date_slot ASC, hour ASC';
      $rq = Db::getInstance()->executeS($sql);
      return self::_genByDb($rq);
    }

    public static function addCalendarSlot($id_customer, $date, $hour, $id_np_customer_point = 0){
        if (!$id_customer || $id_customer < 1){
            return null;
        }
        $n = new NPCalendarSlot();
        $n->date_slot = date('Y-m-d', strtotime($date));
        $n->hour = $hour;
        $n->id_np_customer_point = $id_np_customer_point;
        $n->id_customer = $id_customer;
        $n->add();
        return $n;
    }

    //install / uninstall
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. self::$definition['table'];
        $primary = self::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `date_slot` date NOT NULL,
            `hour` int(11) NOT NULL,
            `id_customer` int(11) NOT NULL,
            `id_np_customer_point` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. self::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
