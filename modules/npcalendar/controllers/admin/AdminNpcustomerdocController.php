<?php

class AdminNpcustomerdocController extends AdminModulesController
{

    /**
    * Instanciation de la classe
    * Définition des paramètres basiques obligatoires
    */
    public function __construct()
    {
        $this->bootstrap = true; //Gestion de l'affichage en mode bootstrap
        $this->lang = true; //Flag pour dire si utilisation de langues ou non

        //Appel de la fonction parente pour pouvoir utiliser la traduction ensuite
        parent::__construct();

        //Liste des champs de l'objet à afficher dans la liste

        //Ajout d'actions sur chaque ligne
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

      public function init(){
        parent::init();
      }

      public function initContent(){
        parent::initContent();
        $files = NPCustomerFile::getAll();
        $np_customers = array();
        foreach ($files as $file) {
            $np_customers[] = new NPCustomer($file->id_np_customer);
        }
        $this->context->smarty->assign(array('customers' => $np_customers));
        $this->setTemplate('../../../../modules/npcalendar/views/templates/admin/customerdoc.tpl');
      }

      public function postProcess(){

      }

}
