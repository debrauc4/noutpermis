<?php

class AdminNpholidayController extends AdminModulesController
{

    /**
    * Instanciation de la classe
    * Définition des paramètres basiques obligatoires
    */
    public function __construct()
    {
        $this->bootstrap = true; //Gestion de l'affichage en mode bootstrap
        $this->lang = true; //Flag pour dire si utilisation de langues ou non

        //Appel de la fonction parente pour pouvoir utiliser la traduction ensuite
        parent::__construct();

        //Liste des champs de l'objet à afficher dans la liste

        //Ajout d'actions sur chaque ligne
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

    /**
    * Affichage du formulaire d'ajout / création de l'objet
    * @return string
    * @throws SmartyException
    */
    public function renderForm()
    {
        //Définition du formulaire d'édition
        $this->fields_form = [
        //Entête
        'legend' => [
        'title' => $this->module->l('Edit Sample'),
        'icon' => 'icon-cog'
        ],
        //Champs
        'input' => [
        [
        'type' => 'text', //Type de champ
        'label' => $this->module->l('name'), //Label
        'name' => 'name', //Nom
        'class' => 'input fixed-width-sm', //classes css
        'size' => 50, //longueur maximale du champ
        'required' => true, //Requis ou non
        'empty_message' => $this->l('Please fill the postcode'), //Message d'erreur si vide
        'hint' => $this->module->l('Enter sample name') //Indication complémentaires de saisie
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('code'),
        'name' => 'code',
        'class' => 'input fixed-width-sm',
        'size' => 5,
        'required' => true,
        'empty_message' => $this->module->l('Please fill the code'),
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('email'),
        'name' => 'email',
        'class' => 'input fixed-width-sm',
        'size' => 5,
        'required' => true,
        'empty_message' => $this->module->l('Please fill email'),
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('Title'),
        'name' => 'title',
        'class' => 'input fixed-width-sm',
        'lang' => true, //Flag pour utilisation des langues
        'required' => true,
        'empty_message' => $this->l('Please fill the title'),
        ],
        [
        'type' => 'textarea',
        'label' => $this->module->l('Description'),
        'name' => 'description',
        'lang' => true,
        'autoload_rte' => true, //Flag pour éditeur Wysiwyg
        ],
        ],
        //Boutton de soumission
        'submit' => [
        'title' => $this->l('Save'), //On garde volontairement la traduction de l'admin par défaut
        ]
        ];
        return parent::renderForm();
    }

    /*
    **
    */
    public function init(){
      parent::init();

    }

    private function genError($type, $title, $txt){
        $this->context->smarty->assign(array(
            'error' => array(
                'type' => $type,
                'title' => $title,
                'txt' => $txt,
            )
        ));
    }

    public function postProcess(){
    
        if (Tools::isSubmit('submitAddHoliday')){
            $nDay = Tools::getValue('nDay');
            $nMonth = Tools::getValue('nMonth');
            $nYear = Tools::getValue('nYear');
            $n = new NPCalendarHoliday();
            $n->day = $nDay;
            $n->month = $nMonth;
            $n->year = $nYear;
            if ($n->exist()){
                return $this->genError('warning' , 'Erreur !', 'Le jour férié n\'a pas été ajouté. Il existe déjà');
            }
            if ($n->add()){
                $this->genError('success' , 'Ajouté !', 'Le jour férié a été ajouté avec succés !');
            } else {
                $this->genError('warning' , 'Erreur !', 'Le jour férié n\'a pas été ajouté.' . $x);
            }
        } else if (Tools::isSubmit('submitDeleteCalendarSlot')){
            $nId = Tools::getValue('nIdHoliday');
            if ($nId > 0){
                $n = new NPCalendarHoliday($nId);
                $n->delete();
            }
        }
    }

    public function initContent(){
        parent::initContent();
        $months = array('Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre');
        $this->context->smarty->assign(array('months' => $months));
        $this->context->smarty->assign(array('holidays' => NPCalendarHoliday::getAll()));
        $this->setTemplate('../../../../modules/npcalendar/views/templates/admin/holiday.tpl');
    }
}
