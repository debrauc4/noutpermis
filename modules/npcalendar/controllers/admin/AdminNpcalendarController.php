<?php

class AdminNpcalendarController extends AdminModulesController
{

    /**
    * Instanciation de la classe
    * Définition des paramètres basiques obligatoires
    */
    public function __construct()
    {
        $this->bootstrap = true; //Gestion de l'affichage en mode bootstrap
        $this->lang = true; //Flag pour dire si utilisation de langues ou non

        //Appel de la fonction parente pour pouvoir utiliser la traduction ensuite
        parent::__construct();

        //Liste des champs de l'objet à afficher dans la liste

        //Ajout d'actions sur chaque ligne
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

    /**
    * Affichage du formulaire d'ajout / création de l'objet
    * @return string
    * @throws SmartyException
    */
    public function renderForm()
    {
        //Définition du formulaire d'édition
        $this->fields_form = [
        //Entête
        'legend' => [
        'title' => $this->module->l('Edit Sample'),
        'icon' => 'icon-cog'
        ],
        //Champs
        'input' => [
        [
        'type' => 'text', //Type de champ
        'label' => $this->module->l('name'), //Label
        'name' => 'name', //Nom
        'class' => 'input fixed-width-sm', //classes css
        'size' => 50, //longueur maximale du champ
        'required' => true, //Requis ou non
        'empty_message' => $this->l('Please fill the postcode'), //Message d'erreur si vide
        'hint' => $this->module->l('Enter sample name') //Indication complémentaires de saisie
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('code'),
        'name' => 'code',
        'class' => 'input fixed-width-sm',
        'size' => 5,
        'required' => true,
        'empty_message' => $this->module->l('Please fill the code'),
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('email'),
        'name' => 'email',
        'class' => 'input fixed-width-sm',
        'size' => 5,
        'required' => true,
        'empty_message' => $this->module->l('Please fill email'),
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('Title'),
        'name' => 'title',
        'class' => 'input fixed-width-sm',
        'lang' => true, //Flag pour utilisation des langues
        'required' => true,
        'empty_message' => $this->l('Please fill the title'),
        ],
        [
        'type' => 'textarea',
        'label' => $this->module->l('Description'),
        'name' => 'description',
        'lang' => true,
        'autoload_rte' => true, //Flag pour éditeur Wysiwyg
        ],
        ],
        //Boutton de soumission
        'submit' => [
        'title' => $this->l('Save'), //On garde volontairement la traduction de l'admin par défaut
        ]
        ];
        return parent::renderForm();
    }

    /*
    **
    */
    public function init(){
      parent::init();

    }

    private function genError($type, $title, $txt){
        $this->context->smarty->assign(array(
            'type' => $type,
            'title' => $title,
            'txt' => $txt,
        ));
    }

    public function postProcess(){
        
        if (Tools::isSubmit('submitAddCalendarSlot')){
            $n_date = Tools::getValue('nDate');
            $n_hour = Tools::getValue('nHour');
            $n_id_point = Tools::getValue('nIdPoint');
            $n_id_customer = Tools::getValue('nIdCustomer');
            $n = NPCalendarSlot::addCalendarSlot($n_id_customer, $n_date, $n_hour, $n_id_point);
            if ($n == null){
                $err = 'Le créneau est deja reservé !';
                $this->genError('warning', 'Erreur', 'Une erreur est survenue : ' . $err);
            } else {
                $this->genError('success', 'Succés', 'Le créneau à été reservé !');
            }
        } else if (Tools::isSubmit('submitDeleteCalendarSlot')){
            $id = Tools::getValue('idCalendarSlot');
            $n = new NPCalendarSlot($id);
            if (!$n->delete()){
                $this->context->smarty->assign(array('error' => array(
                    'type' => 'success',
                    'title' => 'Succés',
                    'txt' => 'Le creneau a été supprimer avec succés !',
                )));
            } else {
                $this->context->smarty->assign(array('error' => array(
                    'type' => 'warning',
                    'title' => 'Erreur',
                    'txt' => 'Le creneau n\'a pu être supprimé.',
                )));
            }
        }
    }

    public function initContent(){
        parent::initContent();
        $this->context->smarty->assign(array('customers' => Customer::getCustomers()));
        $this->setTemplate('../../../../modules/npcalendar/views/templates/admin/npcalendar.tpl');
    }
}
