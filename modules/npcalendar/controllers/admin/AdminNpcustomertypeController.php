<?php

class AdminNpcustomertypeController extends AdminModulesController
{

    /**
    * Instanciation de la classe
    * Définition des paramètres basiques obligatoires
    */
    public function __construct()
    {
        $this->bootstrap = true; //Gestion de l'affichage en mode bootstrap
        $this->lang = true; //Flag pour dire si utilisation de langues ou non

        //Appel de la fonction parente pour pouvoir utiliser la traduction ensuite
        parent::__construct();

        //Liste des champs de l'objet à afficher dans la liste

        //Ajout d'actions sur chaque ligne
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

      public function init(){
        parent::init();
      }

      public function initContent(){
        parent::initContent();
        $this->setTemplate('../../../../modules/npcalendar/views/templates/admin/customertype.tpl');
        $this->initCustomers();
        $this->initNewCustomerTypeForm();
      }

      public function postProcess(){
        if (Tools::isSubmit('viewFtuDetails')){
          $id_ftu = Tools::getValue('id_ftu');
          $c = new NPCustomerFileToUpload($id_ftu);
          $this->context->smarty->assign(array('ftuForm' => $this->initFtuForm(Tools::getValue('id_type'), $c)));
        }
        else if (Tools::isSubmit('addFtu')){
          $this->context->smarty->assign(array('ftuForm' => $this->initFtuForm(Tools::getValue('id_type'))));
        }
        else if (Tools::isSubmit('submitEditFtu')){
          $this->_submitEditFtu();
        }
        else if (Tools::isSubmit('deleteFtu')){
          $this->_submitEditFtu();
          $id_ftu = Tools::getValue('id_ftu');
          $n = new NpCustomerFileToUpload($id_ftu);
          $n->delete();
        }
        else if (Tools::isSubmit('submitNewCustomerType')){
          $n = new NPCustomerType();
          $n->name = Tools::getValue('nTypeName');
          $n->save();
        }
        if (($n_id_type = Tools::getValue('id_type'))){
          $this->context->smarty->assign(array('current_id_np_customer_type' => $n_id_type));
          $this->context->smarty->assign(array('current_np_customer_type' => new NPCustomerType($n_id_type)));
        }
      }

      private function _submitEditFtu(){
        if (Tools::getValue('id_ftu')){
          $n = new NPCustomerFileToUpload(Tools::getValue('id_ftu'));
        } else{
          $n = new NPCustomerFileToUpload();
        }
        if (isset($_FILES['file_example']['name']) && !empty($_FILES['file_example']['tmp_name']))
        {
          $extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.png', '.jpeg', '.jpg');
          $filename = uniqid(). '.' .pathinfo($_FILES['file_example']['name'], PATHINFO_EXTENSION);
          $filename = str_replace(' ', '-', $filename);
          $filename = strtolower($filename);
          $filename = filter_var($filename, FILTER_SANITIZE_STRING);
          $_FILES['file_example']['name'] = $filename;
          $uploader = new UploaderCore();
          $uploader->upload($_FILES['file_example']);
          $n->file_example = _PS_BASE_URL_ . __PS_BASE_URI__. 'upload/' .$filename;
        }
        $n->name = Tools::getValue('nFtuName');
        $n->description = Tools::getValue('nFtuDescription');
        $n->id_np_customer_type = Tools::getValue('id_type');
        $n->save();
        //
        $this->context->smarty->assign(array('ftuForm' => $this->initFtuForm($n->id_np_customer_type, $n)));
      }

      public function initCustomers(){
        $cts = NPCustomerType::getCustomerTypes();
        $ids = array();
        $names = array();
        foreach($cts as &$ct){
          $ids[] = $ct->id;
          $names[] = $ct->name;
        }
        $this->context->smarty->assign(array('customer_type_select' => array(
          'ids' => $ids,
          'names' => $names,
        )));
      }

      public function initFtuForm($id_type, $ftu_obj = null){
        $fields_form = array(
          'form' => array(
            'legend' => array(
              'title' => ($ftu_obj) ? 'Modifier ' . $ftu_obj->name : 'Ajouter un fichier',
              'icon' => 'icon-file',
            ),
            'input' => array(
              array(
                'type' => 'hidden',
                'label' => 'id',
                'name' => 'id_ftu',
                'required' => false,
              ),
              array(
                'type' => 'hidden',
                'label' => 'id',
                'name' => 'id_type',
                'required' => false,
              ),
              array(
                'type' => 'text',
                'label' => 'Nom',
                'name' => 'nFtuName',
                'required' => false,
              ),
              array(
                'type' => 'textarea',
                'label' => 'Description',
                'name' => 'nFtuDescription',
                'required' => false,
                'value' => 'Coucou'
              ),
              array(
                'type' => 'file',
                'label' => 'Fichier d\'exemple / à remplir',
                'name' => 'file_example',
                'required' => false,
                'file' => ($ftu_obj)? $ftu_obj->file_example : null,
              ),
            ),
            'submit' => array('title' => 'Enregistrer'),
          ),
        );

        $helper = new HelperForm();
        $helper->table = 'wineoheaderimg';
        $helper->default_form_language = (int) Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = (int) Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
        $helper->submit_action = 'submitEditFtu';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminNpcustomertype', false);
        $helper->token = Tools::getAdminTokenLite('AdminNpcustomertype');
        $helper->tpl_vars = array(
          'fields_value' => array(
            'nFtuDescription' => ($ftu_obj)? $ftu_obj->description : '',
            'nFtuName' => ($ftu_obj)? $ftu_obj->name : '',
            'id_ftu' => ($ftu_obj)? $ftu_obj->id : '',
            'id_type' => $id_type,
          ),
          'languages' => $this->context->controller->getLanguages(),
        );

            return $helper->generateForm(array($fields_form));
      }

      private function initNewCustomerTypeForm(){
        $fields_form = array(
          'form' => array(
            'legend' => array(
              'title' => 'Ajouter un type de client',
              'icon' => 'icon-people',
            ),
            'input' => array(
              array(
                'type' => 'text',
                'label' => 'Nom',
                'name' => 'nTypeName',
                'required' => true,
              ),
            ),
            'submit' => array('title' => 'Enregistrer'),
          ),
        );

        $helper = new HelperForm();
        $helper->table = 'wineoheaderimg';
        $helper->default_form_language = (int) Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = (int) Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
        $helper->submit_action = 'submitNewCustomerType';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminNpcustomertype', false);
        $helper->token = Tools::getAdminTokenLite('AdminNpcustomertype');
        $helper->tpl_vars = array(
          'fields_value' => array(
            'nFtuDescription' => ($ftu_obj)? $ftu_obj->description : '',
            'nFtuName' => ($ftu_obj)? $ftu_obj->name : 'ok',
            'id_ftu' => ($ftu_obj)? $ftu_obj->id : '',
            'id_type' => $id_type,
          ),
          'languages' => $this->context->controller->getLanguages(),
        );

        $this->context->smarty->assign(array('nCustomerTypeForm' => $helper->generateForm(array($fields_form))));
      }
}
