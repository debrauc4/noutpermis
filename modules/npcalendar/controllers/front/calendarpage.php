<?php

class NpcalendarCalendarpageModuleFrontController extends ModuleFrontController
{
    public $auth = true;

    public function __construct()
    {
        parent::__construct();
    }

    private function genAddSlotSuccessAlert(){
        $this->context->smarty->assign(array('error' => array(
            'type' => 'success',
            'title' => 'Succés',
            'txt' => 'Votre créneau à été reservé avec succés !',
        )));
    }

    private function genAddSlotErrorAlert($reasons){
        $str = '<ul>';
        foreach ($reasons as $r) {
            $str .= '<li>'. $r .'</li>';
        }
        $str .= '</ul>';
        $this->context->smarty->assign(array('error' => array(
            'type' => 'error',
            'title' => 'Erreur',
            'txt' => 'Impossible d\'ajouter ce créneaux : <br />' . $str,
        )));
    }

    public function postProcess(){
        if (Tools::isSubmit('submitAddCalendarSlot')){
            if (($nDate = Tools::getValue('nDate')) &&
            ($nHour = Tools::getValue('nHour')) &&
            ($nIdCustomerPoint = Tools::getValue('nIdCustomerPoint'))) {
                $point = new NPCustomerPoint($nIdCustomerPoint);
                $point->addSlotForCustomer($this->context->customer->id, $nDate, $nHour);
            }
        }
    }

    public function setMedia() {
        parent::setMedia();
    }

    public function parseCalendarSlots($slots){
        $ret = array(
        'past' => array(),
        'future' => array(),
        );
        $today = date('Y-m-d');
        foreach ($slots as $one) {
            $d = date('Y-m-d', strtotime($one->date_slot));
            if ($d > $today){
                $ret['future'][] = $one;
            } else {
                $ret['past'][] = $one;
            }
        }
        return $ret;
    }

    public function initResumeNbPoint($points){
        $week = 0;
        $weekend = 0;
        foreach ($points as $p) {
            if ($p->weekend){
                $weekend += ($p->nb_point - count($p->np_calendar_slots));
            } else {
                $week += ($p->nb_point - count($p->np_calendar_slots));
            }
        }
        $this->context->smarty->assign(array('resume_points' => array(
            'weekend' => $weekend,
            'week' => $week
        )));
    }

    public function initContent()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->display_column_right = false;
        parent::initContent();
        $id_customer = $this->context->customer->id;
        $points = NPCustomerPoint::getAvailableByIdCustomer($id_customer);
        $cs = NPCalendarSlot::getByIdCustomer($id_customer);
        $np_customer = NPCustomer::getByIdCustomer($this->context->customer->id);
        $this->context->smarty->assign(array('np_customer' => $np_customer));
        $this->context->smarty->assign(array('points' => $points));
        $this->context->smarty->assign(array('holidays' => NPCalendarHoliday::getAll()));
        $this->context->smarty->assign(array('calendar_slots' => $this->parseCalendarSlots($cs)));
        $this->initResumeNbPoint($points);
        $this->setTemplate('calendarpage.tpl');
    }
}
