<?php


class NpcalendarCustomerpointModuleFrontController extends ModuleFrontController
{
    public $auth = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function postProcess(){

    }



    public function setMedia() {
        parent::setMedia();
    }


    public function initContent()
    {
        $this->display_column_right = false;
        parent::initContent();
        $pts = NPCustomerPoint::getByIdCustomer($this->context->customer->id);
        $this->context->smarty->assign(array('points' => $pts));
        $this->setTemplate('customerpoint.tpl');
    }
}
