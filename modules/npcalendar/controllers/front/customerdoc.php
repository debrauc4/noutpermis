<?php


class NpcalendarCustomerdocModuleFrontController extends ModuleFrontController
{
    public $auth = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function postProcess(){

        if (Tools::isSubmit('addNPCustomerFile')){
            $error_upload = 0;
            $id_type = Tools::getValue('id_np_customer_type');
            $n = new NPCustomerType($id_type);
            $c = NPCustomer::getByIdCustomer($this->context->customer->id);
            $c->id_np_customer_type = $id_type;
            $i = 0;
            $extension_allowed = array('pdf', 'png', 'jpeg', 'jpg');
            $tabfiles = array();
            foreach($n->np_file_to_uploads as $ftu){
                if (isset($_FILES['doc_' . $i])){
                    $ext = pathinfo($_FILES['doc_' . $i]['name'], PATHINFO_EXTENSION);
                    if (in_array($ext, $extension_allowed)){
                        $tabfiles[] = $_FILES['doc_' . $i];
                    }
                    else{
                        $error_upload = 1;
                        break;
                    }
                } else {
                    $error_upload = 1;
                    break;
                }
                $i++;
            }
            //No error, we can add
            if (!$error_upload){
                $j = 0;
                foreach($tabfiles as $one_file){
                    $this->addNPCustomerFile($one_file, $n->np_file_to_uploads[$j++], $c->id_np_customer);
                }
                $c->save();
            } else{
                //show error
                $this->context->smarty->assign(array('error_upload' => 1));
            }
        }
    }

    public function addNPCustomerFile($file, $ftu, $id_np_customer){
        $n = new NPCustomerFile();
        if (isset($file['name']) && !empty($file['name']) && !empty($file['tmp_name']))
        {
            $filename = uniqid(). '.' .pathinfo($file['name'], PATHINFO_EXTENSION);
            $filename = str_replace(' ', '-', $filename);
            $filename = strtolower($filename);
            $filename = filter_var($filename, FILTER_SANITIZE_STRING);
            $file['name'] = $filename;
            $uploader = new UploaderCore();
            $uploader->upload($file);
            $n->file = _PS_BASE_URL_ . __PS_BASE_URI__. 'upload/' .$filename;
        }
        $n->id_np_customer_file_to_upload = $ftu->id;
        $n->id_np_customer = $id_np_customer;
        $n->add();
    }


    public function setMedia() {
        parent::setMedia();
    }


    public function initContent()
    {
        $this->display_column_right = false;
        parent::initContent();
        $this->context->smarty->assign(array('np_customer_types' => NPCustomerType::getCustomerTypes()));
        $this->context->smarty->assign(array('np_customer' => NPCustomer::getByIdCustomer($this->context->customer->id)));
        $this->setTemplate('customerdoc.tpl');
    }
}
