<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__).'/classes/NPCustomer.php');
require_once(dirname(__FILE__).'/classes/NPCustomerFile.php');
require_once(dirname(__FILE__).'/classes/NPCustomerFileToUpload.php');
require_once(dirname(__FILE__).'/classes/NPCustomerPoint.php');
require_once(dirname(__FILE__).'/classes/NPCustomerType.php');
require_once(dirname(__FILE__).'/classes/NPPointGestion.php');
require_once(dirname(__FILE__).'/classes/NPCalendarSlot.php');
require_once(dirname(__FILE__).'/classes/NPCalendarHoliday.php');


class Npcalendar extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'npcalendar';
        $this->tab = 'administration';
        $this->version = '0.0.1';
        $this->author = 'AutoPermis.re - Dev';
        $this->need_instance = 0;

        /**
        * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
        */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('AutoPermis.re - Calendrier / Points');
        $this->description = $this->l('Module essentiel pour la gestion du site : AutoPermis.Re');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

    }


    public function install()
    {
        Configuration::updateValue('NPCALENDAR_HOUR_START', 7);
        Configuration::updateValue('NPCALENDAR_HOUR_END', 21);
        Configuration::updateValue('NPCALENDAR_NB_CAR', 1);
        Configuration::updateValue('NPCALENDAR_LIVE_MODE', false);

        //Db install
        NPCustomer::installDb();
        NPCustomerFile::installDb();
        NPCustomerFileToUpload::installDb();
        NPCustomerPoint::installDb();
        NPCustomerType::installDb();
        NPPointGestion::installDb();
        NPCalendarSlot::installDb();
        NPCalendarHoliday::installDb();
        //!Db install

        return parent::install() &&
        $this->installTab() &&
        $this->installTabCustomertype() &&
        $this->installTabHoliday() &&
        $this->installTabCustomerdoc() &&
        $this->registerHook('header') &&
        $this->registerHook('backOfficeHeader') &&
        $this->registerHook('displayHomeTab') &&
        $this->registerHook('displayAdminCustomers') &&
        $this->registerHook('displayProductExtraContent') &&
        $this->registerHook('displayProductTab') &&
        $this->registerHook('displayProductTabContent') &&
        $this->registerHook('displayRightColumnProduct') &&
        $this->registerHook('actionProductUpdate') &&
        $this->registerHook('displayShoppingCart') &&
        $this->registerHook('displayCustomerAccount') &&
        $this->registerHook('actionOrderStatusUpdate') &&
        $this->registerHook('actionProductDelete') &&
        $this->registerHook('displayAdminProductsExtra') &&
        $this->registerHook('displayHomeTabContent');
    }

    public function uninstall()
    {
        Configuration::deleteByName('NPCALENDAR_LIVE_MODE');
        Configuration::deleteByName('NPCALENDAR_HOUR_START');
        Configuration::deleteByName('NPCALENDAR_HOUR_END');
        Configuration::deleteByName('NPCALENDAR_NB_CAR');
        //Db uninstall
        NPCustomer::uninstallDb();
        NPCustomerFile::uninstallDb();
        NPCustomerFileToUpload::uninstallDb();
        NPCustomerPoint::uninstallDb();
        NPCustomerType::uninstallDb();
        NPPointGestion::uninstallDb();
        NPCalendarSlot::uninstallDb();
        NPCalendarHoliday::uninstallDb();
        //db
        return parent::uninstall() &&
        $this->uninstallTab() &&
        $this->uninstallTabCustomertype() &&
        $this->uninstallTabHoliday() &&
        $this->uninstallTabCustomerdoc();
    }

    /**
    * Load the configuration form
    */
    public function getContent()
    {
        /**
        * If values have been submitted in the form, process.
        */
        if (((bool)Tools::isSubmit('submitNpcalendarModule')) == true) {
            $this->postProcess();
        }
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        return $output.$this->renderForm();
    }

    /**
    * Create the form that will be displayed in the configuration of your module.
    */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitNpcalendarModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
        .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
        'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
        'languages' => $this->context->controller->getLanguages(),
        'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
    * Create the structure of your form.
    */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-calendar"></i>',
                        'desc' => $this->l('Please enter the start hour of calendar'),
                        'name' => 'NPCALENDAR_HOUR_START',
                        'label' => $this->l('Heure debut (agenda) :'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-calendar"></i>',
                        'desc' => $this->l('Please enter the final hour of calendar'),
                        'name' => 'NPCALENDAR_HOUR_END',
                        'label' => $this->l('Heure de fin (agenda) :'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-car"></i>',
                        'desc' => $this->l('Please enter the number of car for agenda picking'),
                        'name' => 'NPCALENDAR_NB_CAR',
                        'label' => $this->l('Nombre de voiture :'),
                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
    * Set values for the inputs.
    */
    protected function getConfigFormValues()
    {
        return array(
            'NPCALENDAR_HOUR_START' => Configuration::get('NPCALENDAR_HOUR_START', 7),
            'NPCALENDAR_HOUR_END' => Configuration::get('NPCALENDAR_HOUR_END', 21),
            'NPCALENDAR_NB_CAR' => Configuration::get('NPCALENDAR_NB_CAR'),
        );
    }

    /**
    * Save form data.
    */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
        $this->context->smarty->assign(array('mytest' => Tools::getValue('mytest')));

    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be added on the FO.
    */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }


    public function addNPCustomerFile($file, $ftu, $id_np_customer){
        $n = new NPCustomerFile();
        if (isset($file['name']) && !empty($file['name']) && !empty($file['tmp_name']))
        {
            $filename = uniqid(). '.' .pathinfo($file['name'], PATHINFO_EXTENSION);
            $filename = str_replace(' ', '-', $filename);
            $filename = strtolower($filename);
            $filename = filter_var($filename, FILTER_SANITIZE_STRING);
            $file['name'] = $filename;
            $uploader = new UploaderCore();
            $uploader->upload($file);
            $n->file = _PS_BASE_URL_ . __PS_BASE_URI__. 'upload/' .$filename;
        }
        $n->id_np_customer_file_to_upload = $ftu->id;
        $n->id_np_customer = $id_np_customer;
        $n->add();
    }

    public function getFront($params){

        $cleanSaveSlot = false;
        //submit
        if (Tools::isSubmit('addSlots')){

        }
        else if (Tools::isSubmit('addNPCustomerFile')){
            $error_upload = 0;
            $id_type = Tools::getValue('id_np_customer_type');
            $n = new NPCustomerType($id_type);
            $c = NPCustomer::getByIdCustomer($this->context->customer->id);
            $c->id_np_customer_type = $id_type;
            $i = 0;
            $extension_allowed = array('pdf', 'png', 'jpeg', 'jpg');
            $tabfiles = array();
            foreach($n->np_file_to_uploads as $ftu){
                if (isset($_FILES['doc_' . $i])){
                    $ext = pathinfo($_FILES['doc_' . $i]['name'], PATHINFO_EXTENSION);
                    if (in_array($ext, $extension_allowed)){
                        $tabfiles[] = $_FILES['doc_' . $i];
                    }
                    else{
                        $error_upload = 1;
                        break;
                    }
                } else {
                    $error_upload = 1;
                    break;
                }
                $i++;
            }
            //No error, we can add
            if (!$error_upload){
                $j = 0;
                foreach($tabfiles as $one_file){
                    $this->addNPCustomerFile($one_file, $n->np_file_to_uploads[$j++], $c->id);
                }
                $c->save();
            } else{
                //show error
                $this->context->smarty->assign(array('error_upload' => 1));
            }
        }
        //init
        $this->context->smarty->assign(array('id_customer' => $this->context->customer->id));
        if ($this->context->customer->id > 0){
            $npc = NPCustomer::getByIdCustomer($this->context->customer->id);
            $this->context->smarty->assign(array('np_customer' => $npc));
        }
        //input

        $this->context->smarty->assign(array('np_customer_types' => NPCustomerType::getCustomerTypes()));
        return $this->display(__FILE__, 'npcalendar.tpl');
    }



    public function hookDisplayHomeTab($params)
    {
        //return $this->display(__FILE__, 'views/templates/displayHomeTab.tpl');
    }



    public function hookDisplayHomeTabContent($params)
    {
        return $this->getFront($params);
    }

    public function createTableLink(){
        $tab = new Tab();
        $tab->class_name = 'AdminNpcalendar';
        $tab->module = $this->name;
        $tab->id_parent = (int)Tab::getIdFromClassName('DEFAULT');
        $tab->icon = 'settings_applications';
        $languages = Language::getLanguages();
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Calendrier');
        }
        try {
            $tab->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }

        return true;
    }

    public function hookActionProductUpdate($params)
    {
        if (Tools::getValue('id_product')){
            $n = NPPointGestion::getByIdProduct(Tools::getValue('id_product'));
            $n->nb_day = (int)Tools::getValue('np_point_nb_day');
            $n->nb_point = (int)Tools::getValue('np_point_nb_point');
            $n->weekend = Tools::getValue('np_point_weekend');
            $n->splittable = Tools::getValue('np_point_splittable');
            $n->id_product = Tools::getValue('id_product');
            $n->hide = Tools::getValue('np_hide');
            $n->save();
        }
    }

    public function hookActionProductDelete($params)
    {
        $n = NPPointGestion::getByIdProduct(Tools::getValue('id_product'));
        $n->delete();
        return true;
    }

    public function hookDisplayProductExtraContent($params)
    {
        $pg = NPPointGestion::getByIdProduct(Tools::getValue('id_product'));
        if ($pg->hide == '0'){
            $array = array();
            $array[] = (new PrestaShop\PrestaShop\Core\Product\ProductExtraContent())
            ->setTitle('Points')
            ->setContent('<b>'. $pg->nb_point .'</b> heures de conduites vous seront crédités, ces heures seront valides <b>'. $pg->nb_day .'</b> jours');
            return $array;
        }
    }

    public function getFrontProductRender(){
        $id_product = Tools::getValue('id_product');
        if ($id_product > 0){
            if (NPPointGestion::alreadyExist($id_product)){
                $pg = NPPointGestion::getByIdProduct($id_product);
            }
            else {
                $n = new NPPointGestion();
                $n->id_product = $id_product;
                $n->save();
                $pg = $n;
            }
            $this->context->smarty->assign(array('pg' => $pg));
        }
        return $this->display(__FILE__, 'views/templates/displayProductTabContent.tpl');
    }

    public function hookDisplayProductTabContent($params)
    {
        return $this->getFrontProductRender();
    }

    public function hookDisplayRightColumnProduct(){
        return $this->getFrontProductRender();
    }


    public function hookDisplayAdminProductsExtra($params)
    {
        //update
        $id_product = Tools::getValue('id_product');
        if ($id_product){
            if (NPPointGestion::alreadyExist($id_product)){
                $pg = NPPointGestion::getByIdProduct($id_product);
            }
            else {
                $n = new NPPointGestion();
                $n->id_product = $id_product;
                $n->nb_point = 1;
                $n->nb_day = 1;
                $n->weekend = 0;
                $n->save();
                $pg = $n;
            }
            $this->context->smarty->assign(array('pg' => $pg));
            $this->context->smarty->assign(array('current_product' => new Product($id_product, false, $this->context->language->id)));
        }
        $this->context->smarty->assign(array('id_product' => $id_product));
        $this->context->smarty->assign(array('radio_options', array('Oui' => 1, 'Non' => 0)));
        return $this->display(__FILE__, 'views/templates/displayAdminProductsExtra.tpl');
    }

    public function hookDisplayAdminCustomers($params){
        if (Tools::isSubmit('verifyUser')){
            $npc = NPCustomer::getByIdCustomer($params['id_customer']);
            $npc->verified = 1;
            $npc->save();
        }
        else if (Tools::isSubmit('cancelDemande')){
            $npc = NPCustomer::getByIdCustomer($params['id_customer']);
            $npc->cancelDemande();
        }
        $npc = NPCustomer::getByIdCustomer($params['id_customer']);
        $this->context->smarty->assign(array('npc' => $npc));
        $this->context->smarty->assign(array('calendar_slots' => NPCalendarSlot::getByIdCustomer($params['id_customer'])));
        return $this->display(__FILE__, 'views/templates/displayAdminCustomers.tpl');
    }

    public function hookDisplayCustomerAccount($params)
    {
        return $this->display(__FILE__, 'views/templates/displayCustomerAccount.tpl');
    }

    public function hookActionOrderStatusUpdate($params){

        if ($params['newOrderStatus']->paid == 1){
            $id_order = $params['id_order'];
            $order=new Order($id_order);
            $products=$order->getProducts();
            foreach($products as $pdt){
                $np_pg = NPPointGestion::getByIdProduct($pdt['product_id']);
                NPCustomerPoint::addFromNPPointGestion($np_pg, $id_order, $order->id_customer, $pdt['product_quantity']);
                /*
                if ($pg->hide == '0' && $pg->nb_point > 0){
                    if ($np_pg->id_np_point_gestion > 0){
                    }
                }
                */
            }
        }
        return false;
    }

    //quotation
    public function hookDisplayShoppingCart($params){
        if (Tools::isSubmit('getQuotation')){
            $this->_genCartQuotation($params);
        }
        return '<form action="" method="post"><input style="padding : 5px 10px; background=" class="pull-right button btn btn-primary standard-checkout button-small" type="submit" name="getQuotation" value="Devis gratuit"/></form>';
    }

    public function _genCartQuotationProducts(&$pdf){
        $pdf->Cell('10', '10', 'Qté', 1);
        $pdf->Cell('110', '10', 'Désignation', 1);
        $pdf->Cell('35', '10', 'Prix u HT', 1);
        $pdf->Cell('35', '10', 'Prix total HT', 1);
        $pdf->Ln();

        $products = $this->context->cart->getProducts();
        $total_ht = 0;
        $total_ttc = 0;
        foreach ($products as &$pdt){
            $pdf->Cell('10', '10', $pdt['cart_quantity'], 1);
            $pdf->Cell('110', '10', $pdt['name'] . "\r\n - " . $pdt['attributes_small'], 1);
            $price_ht = Product::getPriceStatic($pdt['id_product'], false, $pdt['id_product_attribute'], 2);
            $price_ttc = Product::getPriceStatic($pdt['id_product'], true, $pdt['id_product_attribute'], 2);
            $total_ht += $price_ht;
            $total_ttc += $price_ttc;
            $pdf->Cell('35', '10', $price_ht . '€', 1);
            $pdf->Cell('35', '10', ($price_ht * $pdt['cart_quantity']) . '€', 1);
            $pdf->Ln();
        }
        $pdf->Cell('10', '10', '', 0);
        $pdf->Cell('110', '10', '', 0);
        $pdf->Cell('35', '10', 'Total HT', 1);
        $pdf->Cell('35', '10', $total_ht . '€', 1);
        $pdf->Ln();
        $pdf->Cell('10', '10', '', 0);
        $pdf->Cell('110', '10', '', 0);
        $pdf->Cell('35', '10', 'T.V.A. 8,5% ', 1);
        $pdf->Cell('35', '10', ($total_ttc - $total_ht) . '€', 1);
        $pdf->Ln();
        $pdf->Cell('10', '10', '', 0);
        $pdf->Cell('110', '10', '', 0);
        $pdf->Cell('35', '10', 'Total TTC', 1);
        $pdf->Cell('35', '10', $total_ttc . '€', 1);
        //var_dump();
    }

    public function _genCartQuotation($params){

        require_once _PS_MODULE_DIR_ . '../tools/tcpdf/tcpdf.php';
        $shop_logo = _PS_BASE_URL_.__PS_BASE_URI__.'/img/'.Configuration::get('PS_LOGO');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->AddPage();
        //$pdf->Image($shop_logo, 10, 10, 20, 20);
        $pdf->Cell('20', '20', '');
        $pdf->Ln();
        $pdf->Cell('95', '0', 'Auto Permis');
        $pdf->Cell('95', '0', 'DEVIS');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell('95', '0', "68 rue de Suffren");
        $pdf->Ln();
        $pdf->Cell('95', '0', "97410 - Saint-Pierre");
        $pdf->Ln();
        $pdf->Cell('95', '0', "0262.65.82.20");
        $pdf->Ln();
        $devis_nb = Configuration::get('NP_CALENDAR_DEVIS_NB');
        $pdf->Cell('95', '0', "Réf.: DV" . $devis_nb);
        Configuration::updateValue('NP_CALENDAR_DEVIS_NB', $devis_nb + 1);
        $c = new Customer($this->context->customer->id);
        $pdf->Cell('95', '0', "Client : " . $c->lastname . ' ' .$c->firstname);
        $pdf->Ln();
        $pdf->Cell('95', '0', "Date.: " . date("d/m/y à H:i"));
        $pdf->Ln();
        $pdf->Ln();
        $this->_genCartQuotationProducts($pdf);
        $pdf->Output('Autopermis_dv'. $devis_nb .'.pdf', 'D');
    }

    //install
    protected function installTab()
    {
        //Install Tab
        $tab = new Tab();
        $tab->class_name = 'AdminNpcalendarmaintab';
        $tab->module = $this->name;
        $tab->id_parent = (int)Tab::getIdFromClassName('DEFAULT');
        $tab->icon = 'icon-calendar';
        $languages = Language::getLanguages();
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('AutoPermis');
        }
        try {
            $tab->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        //Install main tab (calendar)
        $tab = new Tab();
        $tab->class_name = 'AdminNpcalendar';
        $tab->module = $this->name;
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminNpcalendarmaintab');
        $languages = Language::getLanguages();
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Calendrier');
        }
        try {
            $tab->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    protected function installTabCustomertype()
    {
        $tab = new Tab();
        $tab->class_name = 'AdminNpcustomertype';
        $tab->module = $this->name;
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminNpcalendarmaintab');
        $tab->icon = 'car';
        $languages = Language::getLanguages();
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Type de clients');
        }
        try {
            $tab->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }

        return true;
    }

    protected function installTabCustomerdoc()
    {
        $tab = new Tab();
        $tab->class_name = 'AdminNpcustomerdoc';
        $tab->module = $this->name;
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminNpcalendarmaintab');
        $tab->icon = 'car';
        $languages = Language::getLanguages();
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Documents envoyés');
        }
        try {
            $tab->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }

        return true;
    }

    protected function installTabHoliday()
    {
        $tab = new Tab();
        $tab->class_name = 'AdminNpholiday';
        $tab->module = $this->name;
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminNpcalendarmaintab');
        $tab->icon = 'car';
        $languages = Language::getLanguages();
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Jours fériés');
        }
        try {
            $tab->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }

        return true;
    }


    /**
    * Désinstallation du controller admin
    * @return boolean
    */
    protected function uninstallTab()
    {
        $idTab = (int)Tab::getIdFromClassName('AdminNpcalendarmaintab');
        if ($idTab) {
            $tab = new Tab($idTab);
            try {
                $tab->delete();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        $idTab = (int)Tab::getIdFromClassName('AdminNpcalendar');
        if ($idTab) {
            $tab = new Tab($idTab);
            try {
                $tab->delete();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        return true;
    }

    protected function uninstallTabCustomertype()
    {
        $idTab = (int)Tab::getIdFromClassName('AdminNpcustomertype');
        if ($idTab) {
            $tab = new Tab($idTab);
            try {
                $tab->delete();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        return true;
    }

    protected function uninstallTabCustomerdoc()
    {
        $idTab = (int)Tab::getIdFromClassName('AdminNpcustomerdoc');
        if ($idTab) {
            $tab = new Tab($idTab);
            try {
                $tab->delete();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        return true;
    }

    protected function uninstallTabHoliday()
    {
        $idTab = (int)Tab::getIdFromClassName('AdminNpholiday');
        if ($idTab) {
            $tab = new Tab($idTab);
            try {
                $tab->delete();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        return true;
    }
}
