<?php

require_once(dirname(__FILE__).'/../../classes/SMPWishlist.php');

class SocialmoneypotManagewishlistModuleFrontController extends ModuleFrontController
{
    public $auth = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function postProcess(){
        if (Tools::isSubmit('deleteWish')){
            $id = Tools::getValue('id_smp_wishlist');
            $n = new SMPWishlist($id);
            $n->deleted = 1;
            $n->save();
        }
    }

    public function initContent()
    {
        parent::initContent();
        $wishes = SMPWishlist::getWishes($this->context->customer->id);
        $this->context->smarty->assign(array('wish_group' => $wishes));
        $nb = Configuration::get('SOCIALMONEYPOT_NB_WISHLIST');
        $imgs = array();
        for ($i = 1; $i <= $nb; $i++){
            $imgs[] = Configuration::get('SOCIALMONEYPOT_ICON_LIST_' . $i);
        }
        $this->context->smarty->assign(array('imgs' => $imgs));
        $this->context->smarty->assign(array('nb_wish_list' => $nb));
        $this->setTemplate('managewishlist.tpl');
    }
}
