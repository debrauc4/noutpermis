<?php

require_once(dirname(__FILE__).'/../../classes/SMPFriend.php');

class SocialmoneypotManagefriendModuleFrontController extends ModuleFrontController
{
    public $auth = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function postProcess(){
        if (Tools::isSubmit('add_friend')){
            $this->context->smarty->assign(array('add_friend' => true));
        }
        else if (Tools::isSubmit('submitAddFriend')){
            $mail = Tools::getValue('n_mail');
            if (Validate::isEmail($mail)
                && $this->context->customer->email != $mail
                && !SMPFriend::exist($this->context->customer->id, $mail)) {
                $n = new SMPFriend();
                $n->id_customer_a = $this->context->customer->id;
                $n->mail = $mail;
                $n->add();
                $this->context->smarty->assign(array('error' => array(
                    'title' => 'Succès',
                    'type' => 'success',
                    'txt' => 'Votre ami(e) à été ajouté avec succés !'
                )));
            } else {
                $this->context->smarty->assign(array('error' => array(
                    'title' => 'Erreur',
                    'type' => 'warning',
                    'txt' => 'Impossible d\'ajouter cette adresse mail. Cette adresse mail est deja dans votre liste d\'ami'
                )));
            }
        } else if (Tools::isSubmit('deleteFriend')){
            $d = new SMPFriend(Tools::getValue('id_smp_friend'));
            if ($this->context->customer->id == $d->id_customer_a){
                $d->delete();
            }
        } else if (Tools::isSubmit('sendMail')){
            //TODO
            $id_smp_friend = Tools::getValue('id_smp_friend');
            $n = new SMPFriend($id_smp_friend);

            $corpse = "Un ami vous suggère de vous inscrire sur notre site et être son ami sur notre plateforme : " .  _PS_BASE_URL_.__PS_BASE_URI__ ;
            Mail::Send(
                $this->context->language->id,
                'newsletter',
                'Un ami vous suggère de vous inscrire',
                array(
                    '{firstname}' => '', // sender email address
                    '{lastname}' => '', // sender email address
                    '{message}' => nl2br($corpse) // email content
                ),
                $n->mail, // receiver email address
                NULL, //receiver name
                Configuration::get('PS_SHOP_EMAIL'),
                Configuration::get('PS_SHOP_NAME')
            );
        }
    }

    public function initContent()
    {
        parent::initContent();
        $id_customer = $this->context->customer->id;
        $this->context->smarty->assign(array('friends' => SMPFriend::getFriends($id_customer)));
        $this->setTemplate('module:socialmoneypot/views/templates/front/managefriend.tpl');
    }
}
