<?php

require_once(dirname(__FILE__).'/../../classes/SMPMoneyPot.php');
require_once(dirname(__FILE__).'/../../classes/SMPFriend.php');
//require_once(dirname(__FILE__).'../../classes/SMPMoneyPotContributor.php');


class SocialmoneypotManagemoneypotModuleFrontController extends ModuleFrontController
{
  public $auth = true;

  public function __construct()
  {
    parent::__construct();
  }

  public function addFriendsToMoneyPot($id_money_pot, $friend_add_type){
      $friends = array();
      $mp = new SMPMoneyPot($id_money_pot);
      if ($friend_add_type == 0 || $friend_add_type == 2) { //Receiver friend
          $fs = SMPFriend::getFriends($mp->id_receiver, true);
          $friends = $fs;
      }
      if ($friend_add_type == 1 || $friend_add_type == 2) {
          $fs = SMPFriend::getFriends($mp->id_creator, true);
          $friends = array_merge($friends, $fs);
      }
      //clean double
      $ret = array();
      foreach ($friends as $f){
          $ret[] = $f->id_customer_b;
          $ret[] = $f->id_customer_a;
      }
      $ret = array_unique($ret);
      foreach ($ret as $r){
          if ($r != $mp->id_receiver){
              $contrib = new SMPMoneyPotContributor();
              $contrib->id_smp_money_pot = $id_money_pot;
              $contrib->id_customer = $r;
              $contrib->save();
          }
      }
      return $ret;
  }

  public function postProcess(){

      if (Tools::isSubmit('add_pot')){
          $this->context->smarty->assign(array('adder' => true));
      } else if (Tools::isSubmit('submitAddPot')) {
          $n = new SMPMoneyPot();
          $n->id_receiver = Tools::getValue('n_id_receiver');
          $date_e = Tools::getValue('n_date');
          $date_e = date('Y-m-d', strtotime($date_e));
          $n->date_end = $date_e;
          $n->name = Tools::getValue('n_name');
          $n->id_creator = $this->context->customer->id;
          $n->add();
          if ($n->id && $n->id > 0){
              $this->addFriendsToMoneyPot($n->id, Tools::getValue('friend'));
              $this->initMoneyPot(new SMPMoneyPot($n->id));
          }
          else {
              $this->context->smarty->assign(array('error' => array(
                  'title' => 'Erreur',
                  'type' => 'warning',
                  'txt' => 'Un problème est survenu lors de la creation.'
              )));
          }
      } else if (Tools::isSubmit('setCurrentMoneyPot')){
          $id = Tools::getValue('id_smp_money_pot');
          $current = new SMPMoneyPot($id);
          $this->initMoneyPot($current);
      } else if (Tools::isSubmit('submiAddProductToSmp')){
          $id = Tools::getValue('id_smp_money_pot');
          $n_id_product = Tools::getValue('n_id_product');
          $n_id_product_attribute = Tools::getValue('n_id_product_attribute');
          if ($n_id_product){
              $n = new SMPMoneyPotProduct();
              $n->id_product = $n_id_product;
              $n->n_id_product_attribute = $n_id_product_attribute;
              $n->id_smp_money_pot = $id;
              $n->add();
          }
          $current = new SMPMoneyPot($id);
          $this->initMoneyPot($current);
      } else if (Tools::isSubmit('addContribution')){
          $nPrice = Tools::getValue('n_contribution');
          $id_pot = Tools::getValue('id_smp_money_pot');
          $id_n_product = $this->addProduct($nPrice, $id_pot);
          $this->context->smarty->assign(array('id_product_contribution' => $id_n_product));
          $current = new SMPMoneyPot($id_pot);
          $this->initMoneyPot($current);
      } else if (Tools::isSubmit('deleteSMPProduct')) {
          $id_smp_money_pot_product = Tools::getValue('id_smp_money_pot_product');
          $id_smp_money_pot = Tools::getValue('id_smp_money_pot');
          $n = new SMPMoneyPotProduct($id_smp_money_pot_product);
          $n->delete();
          $this->initMoneyPot(new SMPMoneyPot($id_smp_money_pot));
      }
  }

  public function productCleanCat($products, $id_category) {
      $clean = false;
      while (!$clean){
          $clean = true;
          for ($i = 0; $i < count($products); $i++){
                if ($products[$i]['id_category_default'] == $id_category){
                    array_splice($products, $i, 1);
                    $clean = false;
                    break;
                }
          }
      }
      return $products;
  }

  public function addProduct($ttc, $id_pot){
      $default_lang = Configuration::get('PS_LANG_DEFAULT');
      // id de la catégorie par défaut du produit
      $category_id = Configuration::get('SOCIALMONEYPOT_PRODUCT_CATEGORY');
      // caractéristiques du produit
      $cc = Configuration::get('SOCIALMONEYPOT_CONTRIBUTION_COUNTER');
      $product_name = "Contrib " . $cc;
      Configuration::updateValue('SOCIALMONEYPOT_CONTRIBUTION_COUNTER', $cc +1);
      $product_description = "Desription du produit";
      $product_description_short = "Desription courte du produit";

      // début de l'ajout
      $product = new Product();
      $product->name = array((int)$default_lang => $product_name);
      $product->id_category = $category_id;
      $product->id_category_default = $category_id;
      $product->link_rewrite = array((int)$default_lang => Tools::link_rewrite($product_name));
      $product->id_tax_rules_group = Configuration::get('SOCIALMONEYPOT_TAX_RULE');
      $product->description = array($default_lang => $product_description);
      $product->description_short = array($default_lang => $product_description_short);
      $product->quantity = 1;
      $product->reference = $id_pot;
      $product->add();
      $product = new Product($product->id, $this->context->language->id);
      $ht = (($product->tax_rate / 100) + 1.0);
      $ht = $ttc / $ht;
      $product->price = number_format($ht, 6);
      $product->new = NULL;
      $product->date_add = date('Y-m-d', strtotime('-1 year'));
      StockAvailable::setQuantity($product->id, 0, 1);
      $product->updateCategories(array(12));
      $product->save();
      return $product->id;
  }

  public function initMoneyPot($current){
      $this->context->smarty->assign(array('current' => $current));
      $this->context->smarty->assign(array('customer_id' => $this->context->customer->id));
      $product_list = $this->productCleanCat(Product::getProducts($this->context->language->id, 0, 5000, 'name', 'ASC', false, true), 12);
      $pdts = array();
      foreach ($product_list as $pdt){
          $p = new Product($pdt['id_product']);
          $attribute_list = $p->getAttributeCombinaisons($this->context->language->id);
          $attributes = array();
          foreach ($attribute_list as $attr) {
              if (!isset($attributes[$attr['id_product_attribute']])) {
                  $attributes[$attr['id_product_attribute']] = array();
              }
              $attributes[$attr['id_product_attribute']][] = $attr;
          }
          $clean_attributes = array();
          foreach ($attributes as $attr) {
              $clean_attributes[] = $attr;
          }
          $pdts[] = array(
              'obj' => $pdt,
              'attributes' => $clean_attributes
          );
      }
      $this->context->smarty->assign(array('product_list' => $pdts ));
      if (count($current->smp_products) == 0) {
          $this->context->smarty->assign(array('style_width' => 0 ));
          $this->context->smarty->assign(array('bar_width' => 0 ));
      } else {
          $this->context->smarty->assign(array('style_width' => 100 / count($current->smp_products) ));
          $this->context->smarty->assign(array('bar_width' => $current->amount * 100 / $current->amount_total ));
      }
      $style_pdt_widths = array();
      foreach ($current->smp_products as $pdt){
          $style_pdt_widths[] = 100 * $pdt->product_price_static / $current->amount_total;
      }
      $this->context->smarty->assign(array('style_pdt_widths' => $style_pdt_widths));
  }

  public function initContent()
  {
    parent::initContent();

    $id_customer = $this->context->customer->id;
    $this->context->smarty->assign(array('pots' => SMPMoneyPot::getMoneyPots($id_customer)));
    $this->context->smarty->assign(array('friends' => SMPFriend::getFriends($id_customer)));
    $this->setTemplate('module:socialmoneypot/views/templates/front/managemoneypot.tpl');

  }
}
