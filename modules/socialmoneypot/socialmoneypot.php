<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once(dirname(__FILE__).'/classes/SMPMoneyPot.php');
require_once(dirname(__FILE__).'/classes/SMPMoneyPotProduct.php');
require_once(dirname(__FILE__).'/classes/SMPMoneyPotContributor.php');
require_once(dirname(__FILE__).'/classes/SMPFriend.php');
require_once(dirname(__FILE__).'/classes/SMPWishlist.php');

class Socialmoneypot extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'socialmoneypot';
        $this->tab = 'administration';
        $this->version = '0.0.1';
        $this->author = 'FranceNovatech';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Social Money Pot');
        $this->description = $this->l('Creer une cagnotte solidaire pour acheter une liste de courses à un ami.');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('SOCIALMONEYPOT_CONTRIBUTION_COUNTER', $cc +1);
        Configuration::updateValue('SOCIALMONEYPOT_TAX_RULE', 1);
        Configuration::updateValue('SOCIALMONEYPOT_NB_WISHLIST', 1);
        SMPMoneyPot::installDb();
        SMPMoneyPotContributor::installDb();
        SMPFriend::installDb();
        SMPMoneyPotProduct::installDb();
        SMPWishlist::installDb();
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionOrderStatusUpdate') &&
            $this->registerHook('actionCustomerAccountAdd') &&
            $this->registerHook('displayRightColumnProduct') &&
            $this->registerHook('displayHome') &&
            $this->registerHook('displayCustomerAccount');
    }

    public function uninstall()
    {
        Configuration::deleteByName('SOCIALMONEYPOT_TAX_RULE');
        Configuration::deleteByName('SOCIALMONEYPOT_NB_WISHLIST');
        Configuration::deleteByName('SOCIALMONEYPOT_CONTRIBUTION_COUNTER');
        SMPMoneyPot::uninstallDb();
        SMPMoneyPotContributor::uninstallDb();
        SMPFriend::uninstallDb();
        SMPMoneyPotProduct::uninstallDb();
        SMPWishlist::uninstallDb();
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        /*
        if (((bool)Tools::isSubmit('submitSocialmoneypotModule')) == true) {
            $this->postProcess();
        }
        */
        if (Tools::isSubmit('submitSocialmoneypotNbList')){
            $x = Tools::getValue('nNumberList');
            $this->context->smarty->assign(array('nblist' => Tools::getValue('nNumberList')));

            if ($x && $x > 0){
                Configuration::updateValue('SOCIALMONEYPOT_NB_WISHLIST', $x);
            }
        }
        else if (Tools::isSubmit('submitSocialmoneypotFilesList')){
            $nb = Configuration::get('SOCIALMONEYPOT_NB_WISHLIST');
            $res = array();
            for ($i = 1; $i <= $nb; $i++){
                $name = 'nFileList'.$i;
                if (isset($_FILES[$name]['name']) && !empty($_FILES[$name]['tmp_name']))
                {
                    $res[] = $_FILES[$name];
                  $extension = array('.png', '.jpeg', '.jpg');
                  $filename = uniqid(). '.' .pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);
                  $filename = str_replace(' ', '-', $filename);
                  $filename = strtolower($filename);
                  $filename = filter_var($filename, FILTER_SANITIZE_STRING);
                  $_FILES[$name]['name'] = $filename;
                  $uploader = new UploaderCore();
                  $uploader->upload($_FILES[$name]);
                  $path = _PS_BASE_URL_ . __PS_BASE_URI__. 'upload/' .$filename;
                  Configuration::updateValue('SOCIALMONEYPOT_ICON_LIST_' . $i, $path);
                }
            }
        }
        $nb = Configuration::get('SOCIALMONEYPOT_NB_WISHLIST');
        $imgs = array();
        $cfg = array(
            'nb_wishlist' => $nb,
            'img' => array()
        );
        for ($i = 1; $i <= $nb; $i++){
            $cfg['img'][$i] = Configuration::get('SOCIALMONEYPOT_ICON_LIST_'.$i);
        }
        $this->context->smarty->assign(array(
            'cfg' => $cfg
        ));
        $this->context->smarty->assign('module_dir', $this->_path);
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSocialmoneypotModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
     public function getTaxRules()
     {
         return Db::getInstance()->ExecuteS('
         SELECT * FROM `'._DB_PREFIX_.'tax_rules_group`
         WHERE active = 1');
     }


    protected function getConfigForm()
    {
        //Tax rule select init
        $trs = $this->getTaxRules();
        $opts_trs = array();
        foreach ($trs as $tr) {
            $opts_trs[] = array(
                'id_option' => $tr['id_tax_rules_group'],
                'name' => $tr['name']
            );
        }
        //Category select init
        $cs = Category::getCategories(Context::getContext()->language->id, true, false);
        $opts_cs = array();
        foreach ($cs as $c) {
            $opts_cs[] = array(
                'id_option' => $c['id_category'],
                'name' => $c['name']
            );
        }
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'lang' => true,
                        'label' => $this->l('Tax rule to apply for contribution'),
                        'name' => 'SOCIALMONEYPOT_TAX_RULE',
                        'desc' => $this->l('Please choose the right tax rules.'),
                        'options' => array(
                            'query' => $opts_trs,
                            'id' => 'id_option',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'lang' => true,
                        'label' => $this->l('Product category'),
                        'name' => 'SOCIALMONEYPOT_PRODUCT_CATEGORY',
                        'desc' => $this->l('Select the category for contribution product (guests, customers, don\'t have to show this categories).'),
                        'options' => array(
                            'query' => $opts_cs,
                            'id' => 'id_option',
                            'name' => 'name'
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'SOCIALMONEYPOT_PRODUCT_CATEGORY' => Configuration::get('SOCIALMONEYPOT_PRODUCT_CATEGORY'),
            'SOCIALMONEYPOT_TAX_RULE' => Configuration::get('SOCIALMONEYPOT_TAX_RULE'),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }

    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayCustomerAccount()
    {
        return $this->display(__FILE__, 'views/templates/front/displayCustomerAccount.tpl');
    }

    public function hookActionOrderStatusUpdate($params){
      if ($params['newOrderStatus']->paid == 1){
        $id_order = $params['id_order'];
        $order=new Order($id_order);
        $products=$order->getProducts();
        foreach($products as $pdt){
            $product = new Product($pdt['product_id']);
            if ($product->id_category_default == 12){
                $id_customer = $order->id_customer;
                $id_smp_money_pot = $product->reference;
                $c = SMPMoneyPotContributor::getContributor($id_smp_money_pot, $id_customer);
                $c->amount += Product::getPriceStatic((int)$pdt['product_id']);
                $c->save();
            }
        }
      }
      return false;
    }

    public function hookActionCustomerAccountAdd($params)
    {
        $c = $params['newCustomer'];
        SMPFriend::setSignedIn($c->id, $c->email);
    }

    public function hookDisplayRightColumnProduct(){

        $id_product = Tools::getValue('id_product');
        $id_customer = $this->context->customer->id;
        if (Tools::isSubmit('submitOnOffWishlist')){
            $nListNumber = Tools::getValue('nListNumber');
            if (!($wl = SMPWishlist::exist($id_customer, $id_product, $nListNumber))){
                //modify
                $n = new SMPWishlist();
                $n->id_customer = $id_customer;
                $n->id_product = $id_product;
                $n->list_number = $nListNumber;
                $n->deleted = 0;
                $n->add();
            } else {
                //add
                $wl->deleted = ($wl->deleted == '0') ? 1 : 0;
                $wl->save();
            }
        }
        $wishes = SMPWishlist::getByIdProduct($id_customer, $id_product);
        $this->context->smarty->assign(array('wishes' => $wishes));
        $nb = Configuration::get('SOCIALMONEYPOT_NB_WISHLIST');
        $imgs = array();
        for ($i = 1; $i <= $nb; $i++){
            $imgs[] = Configuration::get('SOCIALMONEYPOT_ICON_LIST_' . $i);
        }
        $this->context->smarty->assign(array('nb_wishlist' => $nb));
        $this->context->smarty->assign(array('imgs' => $imgs));
        return $this->display(__FILE__, 'views/templates/front/displayRightColumnProduct.tpl');
    }

    public function hookDisplayHome(){
        $friends = SMPFriend::getFriends($this->context->customer->id);
        usort($friends, "SMPFriend::cmpBirthday");
        $this->context->smarty->assign(array('friends' => $friends));
        return $this->display(__FILE__, 'views/templates/front/displayHome.tpl');

    }
}
