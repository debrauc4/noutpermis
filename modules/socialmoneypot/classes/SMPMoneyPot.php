<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/
require_once(dirname(__FILE__).'/SMPFriend.php');
require_once(dirname(__FILE__).'/SMPMoneyPotContributor.php');


class SMPMoneyPot extends ObjectModel
{
    public $id_smp_money_pot;
    public $id_creator;
    public $id_receiver;
    public $date_end;
    public $date_add;
    public $name;
    //
    public $contributors;
    public $receiver;
    public $creator;
    public $amount;//Calculatede by contributros
    public $smp_products;
    public $amount_total;//Calculated by product prices
    public static $definition = array(
      'table' => 'smp_money_pot',
      'primary' => 'id_smp_money_pot',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_creator' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'id_receiver' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        'date_end' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        'name' => array('type' => self::TYPE_STRING),
      ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);
        $this->initContributors();
        if ($this->id_receiver && $this->id_receiver > 0){
            $this->receiver = new Customer($this->id_receiver);
        }
        if ($this->id_creator && $this->id_creator > 0){
            $this->creator = new Customer($this->id_creator);
        }
        if ($this->id && $this->id > 0){
            $this->smp_products = SMPMoneyPotProduct::getProducts($this->id);
            $total = 0;
            foreach ($this->smp_products as $p){
                $total += $p->product_price_static;
            }
            $this->amount_total = $total;
        }
    }

    public function add($autodate = true, $null_values = false)
    {
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
        if ($this->id_receiver){
            $friends = SMPFriend::getFriends($this->id_receiver);
            foreach ($friends as $friend) {
                $n = new SMPMoneyPotContributor();
                $n->id_smp_money_pot = $this->id;
                $n->id_customer = $friend->id_customer_b;
                $n->add();
            }
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new SMPMoneyPot($one[SMPMoneyPot::$definition['primary']]);
        }
        return $ret;
    }

    public static function getMoneyPots($id_customer)
    {
        $tbl = _DB_PREFIX_ . 'smp_money_pot';
        $primary =  $tbl . '.' . SMPMoneyPot::$definition['primary'];
        $tbl_c = _DB_PREFIX_ . 'smp_money_pot_contributor';
        $primary_c =  $tbl_c . '.' . SMPMoneyPotContributor::$definition['primary'];
        $sql = 'SELECT ' . $primary;
        $sql .= ' FROM '. $tbl;
        $sql .= ' LEFT JOIN '. $tbl_c;
        $sql .= ' ON '. $primary . ' = ' . $primary;
        $sql .= ' WHERE '. $tbl_c.'.id_customer = ' . $id_customer;
        $rq = Db::getInstance()->ExecuteS($sql);
        return SMPMoneyPot::genByDb($rq);
    }

    public function initContributors()
    {
        if ($this->id > 0){
            $this->contributors = SMPMoneyPotContributor::getContributors($this->id);
            $this->amount = 0;
            foreach ($this->contributors as $c) {
                $this->amount += $c->amount;
            }
        }
    }

    public static function installDb()
    {
        $tbl = _DB_PREFIX_. SMPMoneyPot::$definition['table'];
        $primary = SMPMoneyPot::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `id_smp_money_pot` int(11) NOT NULL,
            `id_creator` int(11) NOT NULL,
            `id_receiver` int(11) NOT NULL,
            `date_add` date NOT NULL,
            `name` varchar(80) NOT NULL,
            `date_end` date NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. SMPMoneyPot::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
