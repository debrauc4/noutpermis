<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/


class SMPWishlist extends ObjectModel
{
    public $id_smp_wishlist;
    public $id_customer;
    public $id_product;
    public $deleted;
    public $list_number;
    public $date_add;
    public $date_upd;

    public $product;
    public $customer;
    public $cover;
    public static $definition = array(
        'table' => 'smp_wishlist',
        'primary' => 'id_smp_wishlist',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'deleted' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'list_number' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);
        if ($id > 0 && $this->id_customer > 0){
            $this->customer = new Customer($this->id_customer);
        }
        if ($id > 0 && $this->id_product > 0){
            $this->product = new Product($this->id_product, false, Context::getContext()->language->id);
            $image = Image::getCover($this->id_product);
            $link = new Link;//because getImageLInk is not static function
            $this->cover = $link->getImageLink($this->product->link_rewrite, $image['id_image'], 'home_default');
        }
    }
    public function add($autodate = true, $null_values = false)
    {
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new SMPWishlist($one[SMPWishlist::$definition['primary']]);
        }
        return $ret;
    }

    public static function exist($id_customer, $id_product, $list_number)
    {
        $sql = 'SELECT '. SMPWishlist::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . SMPWishlist::$definition['table'];
        $sql .= ' WHERE id_customer = ' . $id_customer;
        $sql .= ' AND id_product = ' . $id_product;
        $sql .= ' AND list_number = ' . $list_number;
        $rq = Db::getInstance()->ExecuteS($sql);
        if (count($rq) > 0)
            return new SMPWishlist($rq[0][SMPWishlist::$definition['primary']]);
        return null;
    }

    public static function getWishes($id_customer)
    {
        $nb = Configuration::get('SOCIALMONEYPOT_NB_WISHLIST');
        $ret = array();
        for ($i = 1; $i <= $nb; $i++){
            $sql = 'SELECT '. SMPWishlist::$definition['primary'];
            $sql .= ' FROM '._DB_PREFIX_ . SMPWishlist::$definition['table'];
            $sql .= ' WHERE id_customer = ' . $id_customer;
            $sql .= ' AND deleted = 0';
            $sql .= ' AND list_number = ' . $i;
            $rq = Db::getInstance()->ExecuteS($sql);
            $ret[$i] = SMPWishlist::genByDb($rq);
        }
        return $ret;
    }

    //SELECT `ps_smp_wishlist`.* FROM `ps_smp_wishlist` LEFT JOIN `ps_smp_friend` ON `ps_smp_wishlist`.`id_customer` = `ps_smp_friend`.`id_customer_b` WHERE `ps_smp_friend`.id_customer_a = 2 AND `ps_smp_wishlist`.deleted = 0
    /*
    public static function getFriendWishes($id_customer)
    {
        $w_tbl = _DB_PREFIX_. SMPWishlist::$definition['table'];
        $f_tbl = _DB_PREFIX_. SMPFriend::$definition['table'];
        $primary = SMPWishlist::$definition['primary'];
        $sql = `SELECT {$w_tbl}.{$primary}`;
        return $ret;
    }
    */
    public static function getByIdProduct($id_customer, $id_product)
    {
        $sql = 'SELECT '. SMPWishlist::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . SMPWishlist::$definition['table'];
        $sql .= ' WHERE id_customer = ' . $id_customer;
        $sql .= ' AND id_product = ' . $id_product;
        $rq = Db::getInstance()->ExecuteS($sql);
        return SMPWishlist::genByDb($rq);
    }

    //install
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. SMPWishlist::$definition['table'];
        $primary = SMPWishlist::$definition['primary'];
        Db::getInstance()->execute(
        'CREATE TABLE `'. $tbl .'` (
        `'. $primary .'` int(11) NOT NULL,
        `id_customer` int(11) NOT NULL,
        `id_product` int(11) NOT NULL,
        `list_number` int(11) NOT NULL,
        `date_add` date NOT NULL,
        `date_upd` date NOT NULL,
        `deleted` tinyint(4) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
        'ALTER TABLE '. $tbl .'
        ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
        'ALTER TABLE `'. $tbl .'`
        MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
        COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. SMPWishlist::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
