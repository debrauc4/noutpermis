<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/

class SMPMoneyPotContributor extends ObjectModel
{
    public $id_smp_money_pot_contributor;
    public $id_smp_money_pot;
    public $id_customer;
    public $customer;
    public $amount;
    public static $definition = array(
      'table' => 'smp_money_pot_contributor',
      'primary' => 'id_smp_money_pot_contributor',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_smp_money_pot' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'amount' => array('type' => self::TYPE_STRING, 'validate' => 'isPrice'),
      ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null/*, Context $context = null*/)
    {
        parent::__construct($id, $id_lang, $id_shop);
        if ($this->id_customer && $this->id_customer > 0){
            $this->customer = new Customer($this->id_customer);
        }
    }

    public function add($autodate = true, $null_values = false)
    {
        if (!$this->amount){
            $this->amount = '0';
        }
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new SMPMoneyPotContributor($one[SMPMoneyPotContributor::$definition['primary']]);
        }
        return $ret;
    }

    public static function getContributor($id_smp_money_pot, $id_customer){
        $sql = 'SELECT '. SMPMoneyPotContributor::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . SMPMoneyPotContributor::$definition['table'];
        $sql .= ' WHERE id_smp_money_pot = ' . $id_smp_money_pot;
        $sql .= ' AND id_customer = ' . $id_customer;
        $rq = Db::getInstance()->ExecuteS($sql);
        $arr = SMPMoneyPotContributor::genByDb($rq);
        return $arr[0];
    }

    public static function getContributors($id_smp_money_pot = null){
        $sql = 'SELECT '. SMPMoneyPotContributor::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . SMPMoneyPotContributor::$definition['table'];
        if ($id_smp_money_pot){
            $sql .= ' WHERE id_smp_money_pot = ' . $id_smp_money_pot;
        }
        $sql .= ' ORDER BY amount';
        $rq = Db::getInstance()->ExecuteS($sql);
        return SMPMoneyPotContributor::genByDb($rq);
    }



    public static function installDb()
    {
        $tbl = _DB_PREFIX_. SMPMoneyPotContributor::$definition['table'];
        $primary = SMPMoneyPotContributor::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `id_smp_money_pot_contributor` int(11) NOT NULL,
            `id_smp_money_pot` int(11) NOT NULL,
            `id_customer` int(11) NOT NULL,
            `amount` varchar(12) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. SMPMoneyPotContributor::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
