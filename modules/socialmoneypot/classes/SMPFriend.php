<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/


class SMPFriend extends ObjectModel
{
    public $id_smp_friend;
    public $id_customer_a;
    public $id_customer_b;
    public $mail;
    public $date_add;
    public $customer_b;
    public $nb_day_birthday;
    public static $definition = array(
      'table' => 'smp_friend',
      'primary' => 'id_smp_friend',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_customer_a' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'id_customer_b' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'mail' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail'),
        'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
      ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);
        if ($this->id_customer_b > 0){
            $this->customer_b = new Customer($this->id_customer_b);
            $cur_day = date('Y-m-d');
            $cur_time_arr = explode('-',$cur_day);
            $birthday_arr = explode('-',$this->customer_b->birthday);
            $cur_year_b_day = $cur_time_arr[0]."-".$birthday_arr[1]."-".$birthday_arr[2];
            $diff=strtotime($cur_year_b_day)-time();
            $days = floor($diff/(60*60*24));
            $this->nb_day_birthday = ($days < 0) ? 365 + $days: $days;
        }
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->mail){
            $c = Customer::getCustomersByEmail($this->mail);
            if ($c && $c[0] && $c[0]['id_customer'] > 0){
                $this->id_customer_b = $c[0]['id_customer'];
            }
        }
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new SMPFriend($one[SMPFriend::$definition['primary']]);
        }
        return $ret;
    }

    public static function setSignedIn($id_customer, $mail){
        $tbl = _DB_PREFIX_ . SMPFriend::$definition['table'];
        $sql = "UPDATE `". $tbl ."` SET id_customer_b = " . $id_customer;
        $sql .= " WHERE mail = '" . $mail ."'";
        Db::getInstance()->Execute($sql);
    }

    public static function getFriends($id_customer_a, $active = false)
    {
        $sql = 'SELECT '. SMPFriend::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . SMPFriend::$definition['table'];
        $sql .= ' WHERE id_customer_a = ' . $id_customer_a;
        if ($active){
            $sql .= ' AND id_customer_b > 0 ';
        }
        $rq = Db::getInstance()->ExecuteS($sql);
        return SMPFriend::genByDb($rq);
    }

    public static function exist($id_customer_a, $mail){
        $sql = 'SELECT '. SMPFriend::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . SMPFriend::$definition['table'];
        $sql .= ' WHERE id_customer_a = ' . $id_customer_a;
        $sql .= ' AND mail LIKE \'' . $mail . '\'';
        return count(Db::getInstance()->ExecuteS($sql));
    }

    public static function cmpBirthday($a, $b){
        return $a->nb_day_birthday > $b->nb_day_birthday;
    }
    //install
    public static function installDb()
    {
        $tbl = _DB_PREFIX_. SMPFriend::$definition['table'];
        $primary = SMPFriend::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `id_smp_friend` int(11) NOT NULL,
            `id_customer_a` int(11) NOT NULL,
            `id_customer_b` int(11) NOT NULL,
            `mail` varchar(80) NOT NULL,
            `date_add` date NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. SMPFriend::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
