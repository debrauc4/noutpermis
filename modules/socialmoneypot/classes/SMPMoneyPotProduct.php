<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/


class SMPMoneyPotProduct extends ObjectModel
{
    public $id_smp_money_pot_product;
    public $id_smp_money_pot;
    public $id_product;
    public $id_product_attribute;
    //
    public $product;
    public $product_price_static;
    public $cover;
    public static $definition = array(
      'table' => 'smp_money_pot_product',
      'primary' => 'id_smp_money_pot_product',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'id_smp_money_pot' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'id_product_attribute' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
      ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);
        if ($this->id_product > 0){
            $this->product = new Product($this->id_product, false, Context::getContext()->language->id);
            $this->product_price_static = Product::getPriceStatic($this->id_product, true, NULL, 2);
            $image = Image::getCover($this->id_product);
            $link = new Link;//because getImageLInk is not static function
            $this->cover = $link->getImageLink($this->product->link_rewrite, $image['id_image'], 'home_default');
        }
    }
    public function add($autodate = true, $null_values = false)
    {
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new SMPMoneyPotProduct($one[SMPMoneyPotProduct::$definition['primary']]);
        }
        return $ret;
    }

    public static function getProducts($id_smp_money_pot)
    {
        $sql = 'SELECT '. SMPMoneyPotProduct::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . SMPMoneyPotProduct::$definition['table'];
        $sql .= ' WHERE id_smp_money_pot = ' . $id_smp_money_pot;
        $rq = Db::getInstance()->ExecuteS($sql);
        return SMPMoneyPotProduct::genByDb($rq);
    }

    public static function installDb()
    {
        $tbl = _DB_PREFIX_. SMPMoneyPotProduct::$definition['table'];
        $primary = SMPMoneyPotProduct::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `id_smp_money_pot_product` int(11) NOT NULL,
            `id_smp_money_pot` int(11) NOT NULL,
            `id_product` int(11) NOT NULL,
            `id_product_attribute` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. SMPMoneyPotProduct::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
