<style media="screen">
    #socialmoneypot-home{
        position: fixed;
        left: 0px;
        top: 10vh;
        background: #777;

        z-index: 10000;
    }
    #pan{
        padding: 10px;
        height: 80vh;
        width: 300px;
        border-top-right-radius: 12px;
        border-bottom-right-radius: 12px;
        border : 3px solid black;

    }
    #socialmoneypot-home p,
    #socialmoneypot-home a,
    #socialmoneypot-home h2,
    #socialmoneypot-home h3,
    #socialmoneypot-home li{
        color : white;
    }
    #closePanButton{
        position: absolute;
        top: 26px;
        right: -26px;
        height: 100px;
        background: black;
        color: white;
        font-size: 24px;
        border: none;
    }
    #addFriendInput{
        width: 78%;
    }
    #addFriendButton{
        width: 20%;
        background-color: var($btn-primary-color);
    }
    #socialmoneypot-home li{

    }
</style>
<div class="" id="socialmoneypot-home">
    <div class="content" id="pan">
        <h2>Vos amis ont aimé</h2>
        <form class="" action="" method="post">
            <input id="addFriendInput" type="text" name="nFriend" value="" placeholder="Mail d'un ami">
            <button id="addFriendButton" type="button" name="button"><i class="icon icon-plus"></i></button>
        </form>
        <div class="row">
            <ul class="col-lg-12">
                <li>Printed Dress</li>
                <li><a href="#">Voir tout</a></li>
            </ul>
        </div>
        <h2>Anniversaire proche</h2>
        <div class="row">
            <ul class="col-lg-12">
                {foreach from=$friends item=friend}
                {if $friend->id_customer_b > 0}
                <li>{$friend->customer_b->firstname} {$friend->customer_b->lastname} ({$friend->nb_day_birthday} jours)</li>
                {/if}
                {/foreach}
                {if $friends|count > 5}
                <li>Voir tout...</li>
                {/if}
            </ul>
        </div>
    </div>
    <!--
    <div class="hide-button-container">
        <button type="button" name="button" id="closePanButton" onclick="hidePan()"><</button>
    </div>
-->
</div>
<!--
<script type="text/javascript">
    function hidePan() {
        var e = document.getElementById('pan')
        if (e.style.display == "none"){
            e.style.width = "300px";
            e.style.padding = "10px";
            e.style.display = "block";
        } else {
            e.style.width = 0
            e.style.padding = "0";
            e.style.display = "none";
        }
    }
</script>
-->
