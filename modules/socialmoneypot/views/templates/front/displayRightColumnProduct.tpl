<style media="screen">
.myicon {
    color : red;
    font-size : 20px;
}
.icon-32{
    width: 32px;
    height: 32px;
}
.icon-check{
    right: -10px;
    position: absolute;
    bottom: -10px;
    z-index: 300;
    font-size: 30px;
    color : green;
}
.wishlist-button{
    position: relative;
}
.wishlists{
    display: flex;
}
</style>
<div class="wishlists">
    {foreach from=$imgs item=img key=i}
    <form class="" action="" method="post">
        <input type="hidden" name="nListNumber" value="{$i+1}">
        <button type="submit" name="submitOnOffWishlist" class="btn btn-default wishlist-button">
            {foreach from=$wishes item=wish}
            {if $wish->list_number == $i+1 && !$wish->deleted}
            <i class="icon icon-check"></i>
            {/if}
            {/foreach}
            <img src="{$img}" alt="" class="icon-32">
        </button>
    </form>
    {/foreach}
</div>
