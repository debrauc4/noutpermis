<style media="screen">
    #progress-product{
        height: 150px;
        margin: 0;
    }
    #progress-product .progress-bar{
        display: flex;
        overflow: hidden;
    }
    #progress-product .progress-bar .img-container{
        background: white;
        height: 90px;
    }
    .smp-one-product .actions{
        height: 30px;
        display: flex;
        align-items: center;
        justify-content: space-around;
        background: white;
    }
    .smp-one-product .actions .icon-remove,
    .smp-one-product .actions .icon-check{
        font-size : 15px;
    }
    .smp-one-product .actions .icon-check{
        color : green;
    }
    .smp-one-product .actions .icon-remove{
        color : red;
    }
    .smp-one-product{
        border : 1px solid black;
    }
    .smp-one-product .title{
        width: 100%;
        height: 30px;
        color : black;
        font-size : 1.2em;
        background: white;
        text-align: center;
    }
    #progress-product .progress-bar .img-container img{
        max-height: 100%;
        width: auto;
    }
</style>
{extends file='page.tpl'}

{block name='page_header_container'}
<nav data-depth="2" class="breadcrumb hidden-sm-down">
    <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="http://guillaumelatrille.best/">
                <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
        </li>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="http://guillaumelatrille.best/mon-compte">
                <span itemprop="name">Votre compte</span>
            </a>
            <meta itemprop="position" content="2">
        </li>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="{$link->getModuleLink('socialmoneypot', 'managemoneypot')}">
                <span itemprop="name">Cagnottes</span>
            </a>
            <meta itemprop="position" content="2">
        </li>
    </ol>
</nav>
<header class="page-header">
    <h1>Mes cagnottes</h1>
</header>
{/block}

{block name='page_content'}
{if isset($id_product_contribution) && $id_product_contribution > 0}
    <input type="hidden" id="id_product_contribution" name="id_product_contribution" value="{$id_product_contribution}">
{/if}
{if isset($current)}
<div class="box">
    <h2>Cagnotte : "{$current->name}"</h2>
    <div class="row">
        <h4>Produits : </h4>
        {if $current->id_creator == $customer_id}
        <div class="col-lg-12">
            <form class="" action="{$link->getModuleLink('socialmoneypot', 'managemoneypot')}" method="post">
                <input type="hidden" name="id_smp_money_pot" value="{$current->id}">
                <select class="btn btn-primary" name="n_id_product" onchange="setProductAttributSelect(event)">
                    <option value="">--- Choississez un produit ---</option>
                    {foreach from=$product_list item=product}
                    <option value="{$product.obj.id_product}">{$product.obj.name}</option>
                    {/foreach}
                </select>
                <select class="btn btn-primary" name="n_id_product_attribute" id="n_id_product_attribute" style="display:none;">
                </select>
                <button class="btn btn-primary" type="submit" name="submiAddProductToSmp"><i class="icon icon-plus"></i></button>
            </form>
        </div>
        {/if}
        <div class="col-lg-12">
            <div class="progress" id="progress-product">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                    {assign var="amount_required" value=0}
                    {foreach from=$current->smp_products item=p key=i}
                    <div class="smp-one-product"  style="width : {$style_pdt_widths[$i]}%;">
                        <div class="actions" style="color:black;">

                            {assign var="amount_required_min" value=$amount_required}
                            {assign var="amount_required" value=$amount_required+$p->product_price_static}
                            {if $amount_required <= $current->amount}
                            <i class="icon icon-check"></i>
                            {else}
                            <i class="icon icon-remove"></i>
                                {if $cookie->id_customer == $current->id_creator && $amount_required_min >= $current->amount }
                                <form class="" action="{$link->getModuleLink('socialmoneypot', 'managemoneypot')}" method="post">
                                    <input type="hidden" name="id_smp_money_pot_product" value="{$p->id_smp_money_pot_product}">
                                    <input type="hidden" name="id_smp_money_pot" value="{$current->id_smp_money_pot}">
                                    <button type="submit" name="deleteSMPProduct" class="btn btn-primary">
                                        <i class="icon icon-trash"></i>
                                    </button>
                                </form>
                                {/if}
                            {/if}
                        </div>
                        <div class="img-container">
                            <img src="http://{$p->cover}" alt="">
                        </div>
                        <div class="title">
                            {$p->product->name}
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="progress">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: {$bar_width}%;">
                    <span>{$bar_width|intval}%</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <h4>Informations :</h4>
        <div class="col-lg-4">
            <p>
                <i class="icon icon-gift"></i> : {$current->receiver->firstname} {$current->receiver->lastname}<br />
                <i class="icon icon-calendar"></i> : {$current->date_end}<br />
                <i class="icon icon-euro"></i> : {$current->amount} € / {$current->amount_total} €<br />
            </p>
        </div>
        <div class="col-lg-4">
            <h4>Contributeurs</h4>
            <ul>
                {foreach from=$current->contributors item=contrib}
                <li>
                    {if $contrib->id_customer == $current->id_creator}
                    <i class="icon icon-legal"></i>
                    {/if}
                    {$contrib->customer->firstname} {$contrib->customer->lastname} : {$contrib->amount} €
                </li>
                {/foreach}
            </ul>
        </div>
        <div class="col-lg-4">
            <h4>Contribuer</h4>
            {if $current->amount < $current->amount_total  && $current->date_end > date('Y-m-d')}
            <form class="" action="{$link->getModuleLink('socialmoneypot', 'managemoneypot')}" method="post">
                <input type="text" name="n_contribution" value="">
                <input type="hidden" name="id_smp_money_pot" value="{$current->id_smp_money_pot}">
                <button type="submit" name="addContribution" class="btn btn-primary">
                    <i class="icon icon-shopping-cart"></i> <span>Contribuer</span>
                </button>
            </form>
            {else}
            <p>Les contributions sont arrivés à leur terme.</p>
            {/if}
        </div>
    </div>
</div>
{/if}

{if isset($adder)}
<form class="box" action="{$link->getModuleLink('socialmoneypot', 'managemoneypot')}" method="post">
    <div class="">
        <h2>Créer une cagnotte :</h2>
        <fieldset class="row">
            <div class="col-lg-6">
                <div class="password form-group">
                    <label for="n_name">
                        Nom de le cagnotte :
                    </label>
                    <input class="is_required validate form-control" type="text" name="n_name" id="n_name">
                </div>
            </div>
        </fieldset>
        <fieldset class="row">
            <div class="col-lg-6">
                <div class="password form-group">
                    <label for="n_name">
                        Date de fin :
                    </label>
                    <input class="is_required validate form-control" type="date" name="n_date" id="n_date">
                </div>
            </div>
        </fieldset>
        <fieldset class="row">
            <div class="col-lg-6">
                <div class="password form-group">
                    <label for="n_name">
                        Ami(e) :
                    </label>
                    <select class="is_required validate form-control" name="n_id_receiver">
                        <option value="">--- Choisir ---</option>
                        {foreach from=$friends item=friend}
                        {if $friend->id_customer_b > 0}
                            <option value="{$friend->id_customer_b}">{$friend->customer_b->firstname} {$friend->customer_b->lastname} <{$friend->mail}></option>
                        {/if}
                        {/foreach}
                    </select>
                    <span class="form_info">(Si vous ne trouvez pas votre ami, c'est parce qu'il n'a pas encore créer de compte.)</span>
                </div>
            </div>
        </fieldset>
        <fieldset class="row">
            <div class="col-lg-6">
                <div class="password form-group">
                    <label for="n_name">
                        Contributeurs :
                    </label>
                    <span style="display:flex;">
                        Ses amis : <input type="radio" name="friend" value="0" checked>
                    </span>
                    <span style="display:flex;">
                        Mes amis : <input type="radio" name="friend" value="1">
                    </span>
                    <span style="display:flex;">
                        Les deux : <input type="radio" name="friend" value="2">
                    </span>
                    <br />
                </div>
            </div>
        </fieldset>
        <div class="">
            <button class="btn btn-primary" type="submit" name="submitAddPot">Ajouter la cagnotte</button>
        </div>
    </div>
</form>
{/if}
<div class="box">
    <form class="row" action="{$link->getModuleLink('socialmoneypot', 'managemoneypot')}" method="post">
        <button class="btn btn-primary" type="submit" name="add_pot"><i class="material-icons">add</i> Créer une cagnotte</button>
    </form>
    <div class="row">
        <table class="col-lg-12 table">
            <tr>
                <th>Nom de la cagnotte</th>
                <th>Créateur</th>
                <th>Nombre de paticipant</th>
                <th>Destinataire</th>
                <th>Date de fin</th>
                <th>Actions</th>
            </tr>
            {foreach from=$pots item=pot }
            <tr>
                <td>{$pot->name}</td>
                <td>{$pot->creator->lastname} {$pot->creator->firstname}</td>
                <td>{$pot->contributors|count}</td>
                <td>{$pot->receiver->lastname} {$pot->receiver->firstname}</td>
                <td>{$pot->date_end}</td>
                <td>
                    <form class="" action="{$link->getModuleLink('socialmoneypot', 'managemoneypot')}" method="post">
                        <input type="hidden" name="id_smp_money_pot" value="{$pot->id}" required readonly>
                        <button type="submit" name="setCurrentMoneyPot"><i class="icon icon-eye"></i></button>
                    </form>
                </td>
            </tr>
            {/foreach}
        </table>
    </div>
</div>
{/block}
<script type="text/javascript">

{if isset($product_list)}
var products = {$product_list|@json_encode}
{else}
var products = []
{/if}

function setProductAttributSelect(event) {
    var id_product = event.target.value;
    var sel = document.getElementById('n_id_product_attribute');
    sel.style.display = 'none';
    sel.innerHTML = '';
    for (pdt of products) {
        if (parseInt(id_product) == pdt.obj.id_product){
            var view_attributes = false;
            for (attr of pdt.attributes) {
                view_attributes = true;
                var opt = document.createElement('option');
                opt.value = attr[0].id_product_attribute;
                var str = '';
                for (one_attr of attr) {
                    if (str != ''){
                        str += ' - '
                    }
                    str += one_attr.attribute_name
                }
                opt.appendChild( document.createTextNode(str) );
                sel.appendChild(opt);
            }
        }
    }
    if (view_attributes) {
        sel.style.display = 'inline';
    }
}
// self executing function here
(function() {
   // your page initialization code here
   // the DOM will be available here
   var pc = document.getElementById('id_product_contribution');
   if (pc != undefined){
       var idProduct = document.getElementById('id_product_contribution').value;
       idProduct = parseInt(idProduct);
       ajaxCart.refresh();
       ajaxCart.add(idProduct);
       ajaxCart.refresh();
   }
})();

</script>
