{extends file='page.tpl'}

{block name='page_content'}
{if isset($error)}
    <article class="alert alert-{$error.type}">
        <h4 class="title_block">{$error.title}</h4>
        <p class="">
            {$error.txt}
        </p>
    </article>
{/if}
{if isset($add_friend)}
<form class="box" action="{$link->getModuleLink('socialmoneypot', 'managefriend')}" method="post">
    <h4 class="title_block">Ajouter un ami :</h4>
    <fieldset class="row">
        <div class="col-lg-6">
            <div class="password form-group">
                <label for="n_mail">
                    Mail :
                </label>
                <input class="is_required validate form-control" type="text" name="n_mail" id="n_mail">
            </div>
        </div>
    </fieldset>
    <button class="btn btn-primary" type="submit" name="submitAddFriend">Ajouter</button>
</form>
{/if}
<div class="box">
    <h4 class="title_block">Amis</h4>
    <form class=""  action="{$link->getModuleLink('socialmoneypot', 'managefriend')}" method="post">
        <button class="btn btn-primary" type="submit" name="add_friend"><i class="icon icon-plus"></i> Ajouter un(e) ami(e)</button>
    </form>
    <div class="">
        {if isset($debug)}
        {$debug|print_r}
        {/if}
    </div>
    <div class="row">
        <table class="col-lg-12 table">
            <tr>
                <th>Inscrit</th>
                <th>Mail</th>
                <th>Nom - Prénom</th>
                <th>Actions</th>
            </tr>
            {foreach from=$friends item=friend}
            <tr>
                <td>
                    {if $friend->id_customer_b}
                    <i class="icon icon-check"></i>
                    {else}
                    <i class="icon icon-remove"></i>
                    {/if}
                </td>
                <td>{$friend->mail}</td>
                <td>
                    {if $friend->id_customer_b}
                    {$friend->customer_b->lastname} {$friend->customer_b->firstname}
                    {else}
                    -
                    {/if}
                </td>
                <td>
                    <form class="" action="{$link->getModuleLink('socialmoneypot', 'managefriend')}" method="post">
                        <input type="hidden" name="id_smp_friend" value="{$friend->id_smp_friend}">
                        <button type="submit" name="deleteFriend" onclick="return confirm('Are you sure you want to delete this friend ?');"><i class="material-icons">delete</i></button>
                        {if $friend->id_customer_b == 0}
                        <button type="submit" name="sendMail"><i class="material-icons">email</i></button>
                        {/if}
                    </form>

                </td>
            </tr>
            {/foreach}
        </table>
    </div>

</div>
{/block}
