<style media="screen">
.one-wish{
    display: flex;
    border : 1Px solid grey;
    border-radius: 4px;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
}
.one-wish img{
    width: 100px;
    height: 100px;
}

.deleteWish{
    position: relative;
    top: 0;
    right : 0;
}
.wishes-group-container{
    display: flex;
}
.wishes{
    padding: 0 20px;
}
.wishes h3{
    text-align: center;
}
.wishes h3 img{
    width: 50px;
    height: 50px;
}
</style>
<div class="box">
    <h2>Mes listes de souhait :</h2>
    <div class="row">
        <div class="wishes-group-container col-lg-12">
            {foreach from=$wish_group item=wishes key=i}
            <div class="wishes" style="width : {100 / $nb_wish_list}%;">
                <h3><img src="{$imgs[$i-1]}" alt=""></h3>
                {foreach from=$wishes item=wish}
                <div class="one-wish">
                    <form class="" action="" method="post">
                        <input type="hidden" name="id_smp_wishlist" value="{$wish->id_smp_wishlist}" readonly>
                        <button type="submit" class="deleteWish" name="deleteWish"><i class="icon icon-trash"></i></button>
                    </form>
                    <img src="http://{$wish->cover}" alt="">
                    <h4>{$wish->product->name}</h4>
                </div>
                {/foreach}
            </div>
            {/foreach}
        </div>
    </div>
</div>
