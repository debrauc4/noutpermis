{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<style media="screen">
    .preview img{
        width: 42px;
        height: 42px;
    }
</style>

<div class="row">
    <form class="" action="" method="post">
        <div class="panel col-lg-6">
            <h3><i class="icon icon-cogs"></i> Nombre de wishlists :</h3>
            <div class="row">
                <div class="col-lg-6">
                    <label class="control-label" for="">Nombre de liste</label>
                </div>
                <div class="col-lg-6">
                    <input type="text" name="nNumberList" value="{$cfg.nb_wishlist}">
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" value="1" name="submitSocialmoneypotNbList" class="btn btn-default pull-right">
                    <i class="process-icon-save"></i> Enregistrer <br />
                </button>
            </div>
        </div>
    </form>
    <form class="" action="" method="post" enctype="multipart/form-data">
        <div class="panel col-lg-6">
            <h3><i class="icon icon-cogs"></i> Icônes des wishlists :</h3>
            {for $i=1 to $cfg.nb_wishlist}
            <div class="row">
                <div class="col-lg-6">
                    <label class="control-label" for="">Liste n°{$i}</label>
                </div>
                <div class="col-lg-6">
                    <div class="preview">
                        <img src="{$cfg.img[$i]}" alt="">
                    </div>
                    <input type="file" name="nFileList{$i}" value="">
                </div>
            </div>
            {/for}
            <div class="panel-footer">
                <button type="submit" value="1" name="submitSocialmoneypotFilesList" class="btn btn-default pull-right">
                    <i class="process-icon-save"></i> Enregistrer
                </button>
            </div>
        </div>
    </form>
</div>
