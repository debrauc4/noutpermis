<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Salesrevenuepot extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'salesrevenuepot';
        $this->tab = 'administration';
        $this->version = '0.0.1';
        $this->author = 'FranceNovatech';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Sales Revenue Pot');
        $this->description = $this->l('Permet de faire beneficier tout les utilisateurs d\'une cagnotte sur le CA');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('SALESREVENUEPOT_LIVE_MODE', false);
        Configuration::updateValue('SALESREVENUEPOT_PERCENTAGE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHome');
    }

    public function uninstall()
    {
        Configuration::deleteByName('SALESREVENUEPOT_LIVE_MODE');
        Configuration::deleteByName('SALESREVENUEPOT_PERCENTAGE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitSalesrevenuepotModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSalesrevenuepotModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '%',
                        'desc' => $this->l('Enter the percentage of CA in jackpot'),
                        'name' => 'SALESREVENUEPOT_PERCENTAGE',
                        'label' => $this->l('Percentage :'),
                    ),
                    /*
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'SALESREVENUEPOT_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),

                    array(
                        'type' => 'password',
                        'name' => 'SALESREVENUEPOT_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                    */
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'SALESREVENUEPOT_LIVE_MODE' => Configuration::get('SALESREVENUEPOT_LIVE_MODE', true),
            'SALESREVENUEPOT_ACCOUNT_EMAIL' => Configuration::get('SALESREVENUEPOT_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'SALESREVENUEPOT_ACCOUNT_PASSWORD' => Configuration::get('SALESREVENUEPOT_ACCOUNT_PASSWORD', null),
            'SALESREVENUEPOT_PERCENTAGE' => Configuration::get('SALESREVENUEPOT_PERCENTAGE')
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayHome()
    {
        /* Place your code here. */
        $orders = array();
        $date_from = date('Y-m-01');
        $date_to = date("Y-m-d",strtotime($date_from . "+1 month"));
        $orders_tab = Order::getOrdersIdByDate($date_from, $date_to);
        $jackpot = 0;
        $customerJackpotSubscribed = false;
        foreach ($orders_tab as $id_order) {
            $n = new Order($id_order);
            $jackpot += ($n->total_paid * 0.05);
            if ($this->context->customer->id > 0 && $n->id_customer == $this->context->customer->id){
                $customerJackpotSubscribed = true;
            }
        }
        $next_month = date("m", strtotime(date('Y-m-d') . " +1 month"));
        $next_year = date("Y", strtotime(date('Y-m-d') . " +1 month"));
        $next_month = ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'][$next_month - 1];
        $this->context->smarty->assign(array('next_year' => $next_year));
        $this->context->smarty->assign(array('next_month' => $next_month));
        $this->context->smarty->assign(array('orders' => $orders));
        $this->context->smarty->assign(array('jackpot' => $jackpot));
        $this->context->smarty->assign(array('customerJackpotSubscribed' => $customerJackpotSubscribed));

        return $this->display(__FILE__, 'views/templates/front/homepage.tpl');
    }
}
