<style media="screen">
    #salesrevenuepot *{
        text-align: center;
    }
    #salesrevenuepot .left-info{
        text-align: left;
    }
</style>
<div id="salesrevenuepot" class="box">
    <h3>
        Cagnotte sur le chiffres d'affaires
    </h3>
    <h5>En commandant sur nos sites, vous êtes automatiquement enregistré pour notre cagnotte mensuel.</h5>
    <h2>Montant : {$jackpot|string_format:"%.2f"}€</h2>
    <h3>Tirage le : 01 {$next_month} {$next_year}</h3>
    <h5 class="left-info">
        {if isset($customerJackpotSubscribed) && $customerJackpotSubscribed}
        <span style="color : green;"><i class="icon icon-check"></i> Vous êtes inscrit pour la cagnotte.</span>
        {else}
        <span style="color : red;"><i class="icon icon-remove"></i> Vous n'êtes pas inscrit pour la cagnotte.</span>
        {/if}
    </h5>
</div>
