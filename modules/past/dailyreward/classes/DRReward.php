<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/

class DRReward extends ObjectModel
{
    public $id_customer;
    public $id_dr_reward;
    public $date_start;
    public $date_end;
    public static $definition = array(
      'table' => 'dr_reward',
      'primary' => 'id_dr_reward',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'date_start' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        'date_end' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
      ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null/*, Context $context = null*/)
    {
        parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new DRReward($one[DRReward::$definition['primary']]);
        }
        return $ret;
    }

    public static function getByIdCustomer($id_customer)
    {
        $sql = 'SELECT '. DRReward::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . DRReward::$definition['table'];
        $sql .= ' WHERE id_customer = ' . $id_customer;
        $rq = Db::getInstance()->ExecuteS($sql);
        return DRReward::genByDb($rq);
    }

    public static function getByDay($id_customer, $day)
    {
        $sql = 'SELECT '. DRReward::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . DRReward::$definition['table'];
        $sql .= ' WHERE id_customer = '. $id_customer .' AND date_signin = \'' . date("Y-m-d", strtotime($day)) . '\'';
        $rq = Db::getInstance()->ExecuteS($sql);
        if (count($rq) == 0) {
            return null;
        }
        return DRReward::genByDb($rq);
    }

    public static function getByInterval($id_customer, $start, $end)
    {
        $sql = 'SELECT '. DRReward::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . DRReward::$definition['table'];
        $sql .= ' WHERE date_signin >= \''. $start .'\'';
        $sql .= ' AND date_signin <= \'' . $end . '\'';
        $sql .= ' AND id_customer = ' . $id_customer;
        $sql .= ' ORDER BY date_signin DESC';
        $rq = Db::getInstance()->ExecuteS($sql);
        return DRReward::genByDb($rq);
    }

    public static function exist($id_customer, $start, $end)
    {
        $sql = 'SELECT '. DRReward::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . DRReward::$definition['table'];
        $sql .= ' WHERE date_start = \''. $start .'\'';
        $sql .= ' AND date_end = \'' . $end . '\'';
        $sql .= ' AND id_customer = ' . $id_customer;
        $rq = Db::getInstance()->ExecuteS($sql);
        return count($rq) > 0;
    }

    public static function installDb()
    {
        $tbl = _DB_PREFIX_. DRReward::$definition['table'];
        $primary = DRReward::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `'. $primary .'` int(11) NOT NULL,
            `id_customer` int(11) NOT NULL,
            `date_start` date NOT NULL,
            `date_end` date NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. DRReward::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
