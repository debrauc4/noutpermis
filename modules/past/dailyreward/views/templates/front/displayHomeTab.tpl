{*
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*}

<style media="screen">
#dr-container{
    height: auto;
    padding-bottom: 3px;
    border-bottom:  1px solid black;
    border-left:  1px solid black;
    border-right:  1px solid black;
    margin-bottom: 20px;

}
#dr-container h3{
    margin: 0;
    text-align: center;
}
.horizontal{
    display: flex;
    justify-content: flex-start;
    flex-wrap: wrap;
}
.reward-block{
    display: flex !important;
    align-items: center !important;
    justify-content: flex-start;
    flex-direction: column;
    width: 75px;
    min-height: 75px;
    margin: 3px 0px;
    text-align: center;
}
.reward-block .title{
    margin-top: 3px;
}
.img-container{
    width: 50px;
    height: 50px;
    font-size : 20px;
    color : grey;
    padding: 3px;
    border-radius: 3px;
    border : 2px dashed grey;
    display: flex;
    align-items: center;
    justify-content: space-around;
}
.earned .img-container{
    color : green;
}
.empty .img-container{
    color : grey;
}
#dr-title h3{
    margin-bottom : 5px;
    margin-top: 0px;
}
#dr-title{
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    padding: 15px 0px;
    text-align: center;
    color : white;
    background: #139dd3;
    border-left : 1px solid black;
    border-right : 1px solid black;
    border-top : 1px solid black;
}
.infos{
    padding: 5px 0px;
    text-align: center;
}
.infos h3{
    color: black;
}
.congrats-icon{
    font-size : 75px;
    margin-top : 7px;
    margin-bottom : 7px;
}
</style>
<div class="row" style="margin : 15px 0px;" >
    <div class="col-lg-12" id="dr-title" style="background-color : {$config.dailyreward_background_color|escape:'htmlall':'UTF-8'}" >
        <h3>Daily login rewards :</h3>
        <p>
            Earn up to {$config.dailyreward_discount|escape:'htmlall':'UTF-8'}% free with {$config.dailyreward_nb_day|escape:'htmlall':'UTF-8'} stamps !
        </p>
    </div>
    <div class="col-lg-12" id="dr-container">

        {if isset($reward) && $reward}
        <div class="infos" style="text-align : center;" >
            <h3>Congratulations !</h3>
            <p>
                You loged in {$config.dailyreward_nb_day|escape:'htmlall':'UTF-8'} days consecutively !<br />
                <i class="icon-trophy congrats-icon"  style="color : {$config.dailyreward_background_color|escape:'htmlall':'UTF-8'}"></i><br />
                A coupon code as added to your cart !
            </p>
        </div>
        {elseif isset($id_guest)}
        <div class="infos" style="text-align : center;margin-top : 10px;" >
            <p>
                Sign in every day and get rewards !
            </p>
        </div>
        {else}
        <div class="infos">
            <h3>Your stamps</h3>
            <p>
                You are only <b>{$nb_away|escape:'htmlall':'UTF-8'} stamps</b> away for claiming your discount!<br />
                View your latest coupon in Rewards :
            </p>
        </div>
        <div class="horizontal">
            {foreach from=$drds item=drd key=i}
            {if $drd == null}
            <div class="reward-block empty">
                <div class="img-container">
                    <i class="icon-trophy"></i>
                </div>
                <div class="title">
                    Day {$i+1|escape:'htmlall':'UTF-8'}
                </div>
            </div>
            {else}
            <div class="reward-block earned">
                <div class="img-container">
                    <i class="icon-trophy"></i>
                </div>
                <div class="title">
                    Day {$i+1|escape:'htmlall':'UTF-8'}
                </div>
            </div>
            {/if}
            {/foreach}
            <div class="reward-block empty">
                <div class="img-container">
                    <i class="icon-gift"></i>
                </div>
                <div class="title">
                    Claim reward
                </div>
            </div>
        </div>
        {/if}

    </div>
</div>
