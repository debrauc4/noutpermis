{*
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*}


<div class="panel">
	<h3><i class="icon icon-tags"></i> {l s='Documentation' mod='dailyreward'}</h3>
	<p>
		&raquo; {l s='You can get a PDF documentation to configure this module' mod='dailyreward'} :
		<ul>
			<li><a href="#" target="_blank">{l s='English' mod='dailyreward'}</a></li>
			<li><a href="#" target="_blank">{l s='French' mod='dailyreward'}</a></li>
		</ul>
	</p>
</div>
