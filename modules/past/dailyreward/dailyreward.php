<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__).'/classes/DRDay.php');
require_once(dirname(__FILE__).'/classes/DRReward.php');


class Dailyreward extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'dailyreward';
        $this->tab = 'advertising_marketing';
        $this->version = '0.0.1';
        $this->author = 'FranceNovatech';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Daily Rewards');
        $this->description = $this->l('Customer daily reward if connect every day');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        DRDay::installDb();
        Configuration::updateValue('DAILYREWARD_DISCOUNT', 30);
        Configuration::updateValue('DAILYREWARD_NB_DAY', 7);
        Configuration::updateValue('DAILYREWARD_PERCENT', 1);
        Configuration::updateValue('DAILYREWARD_VALIDITY_DAY', 30);
        Configuration::updateValue('DAILYREWARD_COUPON_PREFIX', 'DAILYREWARD');
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHomeTabContent');
    }

    public function uninstall()
    {
        DRDay::uninstallDb();
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitDailyrewardModule')) == true) {
            $this->postProcess();
        }
        $this->context->smarty->assign('module_dir', $this->_path);
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitDailyrewardModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $prefix_discount = 'Percentage of discount earned by the customer after being connected consecutively day';
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-calendar"></i>',
                        'desc' => $this->l('Number of consecutive days before earn the reward'),
                        'name' => 'DAILYREWARD_NB_DAY',
                        'label' => $this->l('Number of day'),
                    ),
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Discount type'),
                        'name' => 'DAILYREWARD_PERCENT',
                        'class' => 't',
                        'required'  => true,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'petit',
                                'value' => 1,
                                'label' => $this->l('Percent')
                            ),
                            array(
                                'id' => 'moyen',
                                'value' => 0,
                                'label' => $this->l('Amout')
                            ),
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-tag"></i>',
                        'desc' => $this->l($prefix_discount),
                        'name' => 'DAILYREWARD_DISCOUNT',
                        'label' => $this->l('Discount'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-calendar"></i>',
                        'desc' => $this->l('Validity of the coupon generated (in days).'),
                        'name' => 'DAILYREWARD_VALIDITY_DAY',
                        'label' => $this->l('Validity duration'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-pencil"></i>',
                        'desc' => $this->l('Prefix of the coupon generated.'),
                        'name' => 'DAILYREWARD_COUPON_PREFIX',
                        'label' => $this->l('Coupon prefix'),
                    ),
                    array(
                        'col' => 6,
                        'type' => 'color',
                        'prefix' => '<i class="icon-tint"></i>',
                        'desc' => $this->l('Background of header in home tabs'),
                        'name' => 'DAILYREWARD_BACKGROUND_COLOR',
                        'label' => $this->l('Background color'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'DAILYREWARD_BACKGROUND_COLOR' => Configuration::get('DAILYREWARD_BACKGROUND_COLOR', null),
            'DAILYREWARD_NB_DAY' => Configuration::get('DAILYREWARD_NB_DAY', null),
            'DAILYREWARD_DISCOUNT' => Configuration::get('DAILYREWARD_DISCOUNT', null),
            'DAILYREWARD_PERCENT' => Configuration::get('DAILYREWARD_PERCENT', null),
            'DAILYREWARD_VALIDITY_DAY' => Configuration::get('DAILYREWARD_VALIDITY_DAY', null),
            'DAILYREWARD_COUPON_PREFIX' => Configuration::get('DAILYREWARD_COUPON_PREFIX', null),

        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
        $id_customer = $this->context->customer->id;
        if ($id_customer > 0) {
            $drd = DRDay::getByDay($id_customer, date("Y-m-d"));
            if ($drd == null) {
                $drd = new DRDay();
                $drd->id_customer = $id_customer;
                $drd->date_signin = date('Y-m-d');
                $drd->id_dr_reward = 0;
                $drd->save();
            }
        }
    }

    public function genStamps($drds, $dailyreward_nb_day)
    {
        $today = date('Y-m-d');
        $todays = array();

        $nb_stamp = 0;
        //Recup consecutive days
        foreach ($drds as $drd) {
            $todays[] = $today;
            if ($drd->date_signin != $today) {
                break;
            }
            $nb_stamp++;
            $today = date('Y-m-d', strtotime('-1 days', strtotime($today)));
        }
        $reverse_tab = array();
        //add earned stamp
        for ($i = 0; $i < $nb_stamp; $i++) {
            $reverse_tab[] = $drds[$i];
        }
        $ordered_tab = array_reverse($reverse_tab);
        // add blank
        $nb_blank = $dailyreward_nb_day - $nb_stamp;
        for ($i = 0; $i < $nb_blank; $i++) {
            $ordered_tab[] = null;
        }
        return $ordered_tab;
    }

    public function createReward($id_customer, &$drds, $start, $end)
    {
        //Add to db
        $dr_r = new DRReward();
        $dr_r->id_customer = $id_customer;
        $dr_r->date_start = $start;
        $dr_r->date_end = $end;
        $dr_r->add();
        //Set promo code
        $cr = new CartRule();
        $cr->id_customer = $id_customer;
        if (Configuration::get('DAILYREWARD_PERCENT')) {
            $cr->reduction_percent = Configuration::get('DAILYREWARD_DISCOUNT');
        } else {
            $cr->reduction_amount = Configuration::get('DAILYREWARD_DISCOUNT');
        }
        $cr->date_from = date('Y-m-d H:i:s');
        $days = Configuration::get('DAILYREWARD_VALIDITY_DAY');
        $cr->date_to = date('Y-m-d H:i:s', strtotime("+". $days ." days", strtotime($cr->date_from)));
        $cr->cart_rule_restriction = 1;
        $cr->minimum_amount = 0;
        $cr->minimum_amount_tax = 0;
        $cr->minimum_amount_currency = 0;
        $cr->minimum_amount_shipping = 0;
        $cr->country_restriction = 0;
        $cr->carrier_restriction = 0;
        $cr->group_restriction = 0;
        $cr->cart_rule_restriction = 0;
        $cr->product_restriction = 0;
        $cr->shop_restriction = 0;
        $cr->code = Configuration::get('DAILYREWARD_COUPON_PREFIX') . '-' . Tools::strtoupper(Tools::passwdGen(6));
        $cr->free_shipping = 0;
        $cr->reduction_amount = 0;
        $cr->reduction_tax = 0;
        $cr->reduction_currency = 0;
        $cr->reduction_product = 0;
        $cr->gift_product = 0;
        $cr->gift_product_attribute = 0;
        $cr->highlight = 1;
        $cr->quantity = 1;
        $cr->quantity_per_user = 1;
        $cr->priority = 1;
        $cr->partial_use = 0;
        $cr->id_currency = 0;
        $cr->active = 1;
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $cr->name[$lang['id_lang']] = 'Daily Reward';
        }
        $cr->add();
        //add id_reward to dr_days rewarded (may be optimised)
        foreach ($drds as &$drd) {
            $drd->id_dr_reward = $dr_r->id;
            $drd->update();
        }
    }

    public function hookDisplayHomeTabContent($params)
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $cfg = array(
            'dailyreward_nb_day' => Configuration::get('DAILYREWARD_NB_DAY'),
            'dailyreward_discount' => Configuration::get('DAILYREWARD_DISCOUNT'),
            'dailyreward_background_color' => Configuration::get('DAILYREWARD_BACKGROUND_COLOR'),
        );
        if ($this->context->customer->id > 0) {
            $id_customer = $this->context->customer->id;
            $start = date('Y-m-d', strtotime('-'. $cfg['dailyreward_nb_day']. ' day'));
            $end = date('Y-m-d');
            $drds = DRDay::getByInterval($id_customer, $start, $end);
            $nb_away = $cfg['dailyreward_nb_day'] - count($drds);

            if (DRReward::exist($id_customer, $start, $end)) {
                $this->context->smarty->assign(array('reward' => true));
            } else {
                if ($nb_away == 0) {
                    $this->createReward($id_customer, $drds, $start, $end);
                    $this->context->smarty->assign(array('reward' => true));
                }
            }
            $stamps = $this->genStamps($drds, $cfg['dailyreward_nb_day']);
            $this->context->smarty->assign(array('nb_away' => $nb_away));
            $this->context->smarty->assign(array('drds' => $stamps));
        } else {
            $this->context->smarty->assign(array('id_guest' => 1));
        }
        $this->context->smarty->assign(array('config' => $cfg));
        return $this->display(__FILE__, 'views/templates/front/displayHomeTab.tpl');
    }
}
