{if isset($pmp)}
<div class="row">
    <div class="panel col-lg-6">
        <form class="" action="{$link->getAdminLink('AdminProspectmanagement')}" method="post">
            <h3>Add prospect</h3>
            <div class="row">
                <input type="hidden" name="nIdPmProspect" value="{$pmp->id}">
                <div class="form-group">
                    <label class="control-label col-lg-3" for="firstname">
                        <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Blabla">
                            Firstname
                        </span>
                    </label>
                    <div class="col-lg-5">
                        <input type="text" id="firstname" name="nFirstname" value="{$pmp->firstname}">
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label class="control-label col-lg-3" for="lastname">
                        <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Blabla">
                            Lastname
                        </span>
                    </label>
                    <div class="col-lg-5">
                        <input type="text" id="lastname" name="nLastname" value="{$pmp->lastname}">
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label class="control-label col-lg-3" for="mail">
                        <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Blabla">
                            Mail
                        </span>
                    </label>
                    <div class="col-lg-5">
                        <input type="text" id="mail" name="nMail" value="{$pmp->mail}" placeholder="example@gmail.com">
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label class="control-label col-lg-3" for="phone">
                        <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Blabla">
                            Phone
                        </span>
                    </label>
                    <div class="col-lg-5">
                        <input type="text" id="phone" name="nPhone" value="{$pmp->phone}">
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" name="addProspect" class="btn btn-default pull-right"><i class="process-icon-save"></i> Save</button>
            </div>
        </form>
    </div>
    {if $pmp->id >0}
    <div class="col-lg-6 panel">
        <h3>action</h3>
        <div class="row">
            <form class="well" action="{$link->getAdminLink('AdminProspectmanagement')}" method="post">
                <input type="hidden" name="nIdPmProspect" value="{$pmp->id}">
                <input type="text" name="newDetails" value="">
                <button type="submit" name="addProspectAction"> <i class="icon icon-plus"></i> ADD</button>
            </form>
            <table class="table">
                <tr>
                    <th>Date</th>
                    <th>Details</th>
                </tr>
                {foreach from=$pmp_actions item=pmp_action}
                <tr>
                    <td>{$pmp_action->date_add}</td>
                    <td>{$pmp_action->details}</td>
                    <td>
                        <form class="" action="{$link->getAdminLink('AdminProspectmanagement')}" method="post">
                            <input type="hidden" name="deletePmAction" value="{$pmp_action->id}">
                            <button type="submit" name="deleteProspectAction"><i class="icon icon-trash"></i> </button>
                        </form>
                    </td>
                </tr>
                {/foreach}
            </table>
        </div>
    </div>
    {/if}
</div>
{/if}
<div class="panel">
    <h3>Hello World</h3>
    <div class="row">
        <form class="" action="{$link->getAdminLink('AdminProspectmanagement')}" method="post">
            <button type="submit" class="btn btn-default" name="Add"> <i class="icon icon-plus"></i> ADD</button>
        </form>
    </div>
    <div class="row">

        <table class="table">
            <tr>
                <th>ID</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Mail</th>
                <th>Phone</th>
                <th>Actions</th>
            </tr>
            {foreach from=$prospects item=prospect}
            <tr>
                <td>{$prospect->id}</td>
                <td>{$prospect->firstname}</td>
                <td>{$prospect->lastname}</td>
                <td>{$prospect->mail}</td>
                <td>{$prospect->phone}</td>
                <td>
                    <form class="" action="{$link->getAdminLink('AdminProspectmanagement')}" method="post">
                        <input type="hidden" name="view_id" value="{$prospect->id}">
                        <button type="submit" name="viewProspect"><i class="icon icon-pencil"></i> </button>
                        <button type="submit" name="deleteProspect"><i class="icon icon-trash"></i> </button>
                    </form>
                </td>
            </tr>
            {/foreach}
        </table>
    </div>
</div>
