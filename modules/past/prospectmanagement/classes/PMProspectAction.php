<?php
/**
* 2019-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class PMProspectAction extends ObjectModel
{
    public $id_pm_prospect_action;
    public $id_pm_prospect;
    public $date_add;
    public $details;
    public $deleted;
    public static $definition = array(
      'table' => 'pm_prospect_action',
      'primary' => 'id_pm_prospect_action',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_pm_prospect' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'details' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
        'deleted' => array('type' => self::TYPE_INT, 'validate' => 'isBool'),
        'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
      ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null/*, Context $context = null*/)
    {
        parent::__construct($id, $id_lang, $id_shop);
    }

    public function add($autodate = true, $null_values = false)
    {
        $this->deleted = 0;
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new PMProspectAction($one[PMProspectAction::$definition['primary']]);
        }
        return $ret;
    }

    public static function getByIdPMProspect($id_pm_prospect)
    {
        $sql = 'SELECT '. PMProspectAction::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . PMProspectAction::$definition['table'];
        $sql .= ' WHERE id_pm_prospect = ' . $id_pm_prospect;
        $sql .= ' ORDER BY date_add';
        $rq = Db::getInstance()->ExecuteS($sql);
        return PMProspectAction::genByDb($rq);
    }

    public static function installDb()
    {
        $tbl = _DB_PREFIX_. PMProspectAction::$definition['table'];
        $primary = PMProspectAction::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `id_pm_prospect_action` int(11) NOT NULL,
            `id_pm_prospect` int(11) NOT NULL,
            `details` varchar(250) NOT NULL,
            `deleted` tinyint(1) NOT NULL,
            `date_add` date NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. PMProspectAction::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
