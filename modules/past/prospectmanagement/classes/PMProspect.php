<?php
/**
* 2019-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class PMProspect extends ObjectModel
{
    public $id_pm_prospect;
    public $id_employee;
    public $mail;
    public $firstname;
    public $lastname;
    public $phone;
    public $deleted;
    public $customer_obj;
    public $date_add;
    public $date_upd;
    public static $definition = array(
      'table' => 'pm_prospect',
      'primary' => 'id_pm_prospect',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_employee' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'mail' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail'),
        'firstname' => array('type' => self::TYPE_STRING, 'validate' => 'isName'),
        'lastname' => array('type' => self::TYPE_STRING, 'validate' => 'isName'),
        'phone' => array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber'),
        'deleted' => array('type' => self::TYPE_INT, 'validate' => 'isBool'),
        'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
      ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null/*, Context $context = null*/)
    {
        parent::__construct($id, $id_lang, $id_shop);
    }

    public function add($autodate = true, $null_values = false)
    {
        $this->deleted = 0;
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new PMProspect($one[PMProspect::$definition['primary']]);
        }
        return $ret;
    }

    public static function getByIdEmployee($id_employee)
    {
        $sql = 'SELECT '. PMProspect::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . PMProspect::$definition['table'];
        $sql .= ' WHERE id_employee = ' . $id_employee;
        $sql .= ' ORDER BY date_upd';
        $rq = Db::getInstance()->ExecuteS($sql);
        return PMProspect::genByDb($rq);
    }

    public static function installDb()
    {
        $tbl = _DB_PREFIX_. PMProspect::$definition['table'];
        $primary = PMProspect::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `id_pm_prospect` int(11) NOT NULL,
            `id_employee` int(11) NOT NULL,
            `mail` varchar(150) NOT NULL,
            `firstname` varchar(75) NOT NULL,
            `lastname` varchar(75) NOT NULL,
            `phone` varchar(20) NOT NULL,
            `deleted` tinyint(1) NOT NULL,
            `date_add` date NOT NULL,
            `date_upd` date NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. PMProspect::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
