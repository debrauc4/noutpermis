<?php

class AdminProspectmanagementController extends AdminModulesController
{

    /**
    * Instanciation de la classe
    * Définition des paramètres basiques obligatoires
    */
    public function __construct()
    {
        $this->bootstrap = true; //Gestion de l'affichage en mode bootstrap
        $this->lang = true; //Flag pour dire si utilisation de langues ou non

        //Appel de la fonction parente pour pouvoir utiliser la traduction ensuite
        parent::__construct();

        //Liste des champs de l'objet à afficher dans la liste

        //Ajout d'actions sur chaque ligne
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

    /**
    * Affichage du formulaire d'ajout / création de l'objet
    * @return string
    * @throws SmartyException
    */
    public function renderForm()
    {
        //Définition du formulaire d'édition
        $this->fields_form = [
        //Entête
        'legend' => [
        'title' => $this->module->l('Edit Sample'),
        'icon' => 'icon-cog'
        ],
        //Champs
        'input' => [
        [
        'type' => 'text', //Type de champ
        'label' => $this->module->l('name'), //Label
        'name' => 'name', //Nom
        'class' => 'input fixed-width-sm', //classes css
        'size' => 50, //longueur maximale du champ
        'required' => true, //Requis ou non
        'empty_message' => $this->l('Please fill the postcode'), //Message d'erreur si vide
        'hint' => $this->module->l('Enter sample name') //Indication complémentaires de saisie
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('code'),
        'name' => 'code',
        'class' => 'input fixed-width-sm',
        'size' => 5,
        'required' => true,
        'empty_message' => $this->module->l('Please fill the code'),
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('email'),
        'name' => 'email',
        'class' => 'input fixed-width-sm',
        'size' => 5,
        'required' => true,
        'empty_message' => $this->module->l('Please fill email'),
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('Title'),
        'name' => 'title',
        'class' => 'input fixed-width-sm',
        'lang' => true, //Flag pour utilisation des langues
        'required' => true,
        'empty_message' => $this->l('Please fill the title'),
        ],
        [
        'type' => 'textarea',
        'label' => $this->module->l('Description'),
        'name' => 'description',
        'lang' => true,
        'autoload_rte' => true, //Flag pour éditeur Wysiwyg
        ],
        ],
        //Boutton de soumission
        'submit' => [
        'title' => $this->l('Save'), //On garde volontairement la traduction de l'admin par défaut
        ]
        ];
        return parent::renderForm();
    }

    /*
    **
    */
    public function init(){
      parent::init();

    }



    public function postProcess(){
        global $cookie;
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        if (Tools::isSubmit('addProspect')){
            $n_id = Tools::getValue('nIdPmProspect');
            if($n_id>0){
                $n = new PMProspect($n_id);
            }
            else{
                $n = new PMProspect();
            }
            $n->mail = Tools::getValue('nMail');
            $n->id_employee = $cookie->id_employee;
            $n->firstname = Tools::getValue('nFirstname');
            $n->lastname = Tools::getValue('nLastname');
            $n->phone = Tools::getValue('nPhone');
            if($n_id>0){
                $n->update();
            }
            else{
                $n->add();
            }
            if (Tools::getValue('nIdPmProspect') !== null && Tools::getValue('nMail')){
            }
        }
        if(Tools::isSubmit('deleteProspectAction')){
            $p_action_id = Tools::getValue('deletePmAction');
            $p_action = new PMProspectAction($p_action_id);
            $p_action->delete();
        }
        if(Tools::isSubmit('addProspectAction')){
            $n = new PMProspectAction();
            $n->details= Tools::getValue('newDetails');
            $n->id_pm_prospect= Tools::getValue('nIdPmProspect');
            $n->add();
            $this->viewProspect($n->ip_pm_prospect);
        }
        if (Tools::isSubmit('deleteProspect')){
            $view_id = Tools::getValue('view_id');
            $pmp = new PMProspect($view_id);
            $pmp->delete();
        }
        if (Tools::isSubmit('Add')){
            $pmp = new PMProspect();
            $this->context->smarty->assign(array('pmp' => $pmp) );
        }
        if (Tools::isSubmit('viewProspect')){
            $id_pm_prospect= Tools::getValue('view_id');
            $this->viewProspect($id_pm_prospect);
        }
    }

    private function viewProspect($id_pm_prospect){
        $pmp = new PMProspect($id_pm_prospect);
        $this->context->smarty->assign(array('pmp' => $pmp) );
        $pmp_actions = PMProspectAction::getByIdPMProspect($id_pm_prospect);
        $this->context->smarty->assign(array('pmp_actions' => $pmp_actions));
    }

    public function initContent(){
        global $cookie;
        parent::initContent();
        $prospects = PMProspect::getByIdEmployee($cookie->id_employee);
        $this->context->smarty->assign(array(
            'prospects' => $prospects
        ));
        $this->setTemplate('../../../../modules/prospectmanagement/views/templates/admin/AdminProspectmanagement.tpl');
    }
}
