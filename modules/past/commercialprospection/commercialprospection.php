<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__).'/controllers/admin/AdminCommercialprospection.php');

class Commercialprospection extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'commercialprospection';
        $this->tab = 'administration';
        $this->version = '0.0.1';
        $this->author = 'FranceNovatech';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Commercial Prospection');
        $this->description = $this->l('Let your administrator to send a commercial proposition to particular customer.');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        $default_mail = 'no-reply@' . strtolower(Configuration::get('PS_SHOP_NAME')) . '.com';
        Configuration::updateValue('COMMERCIALPROSPECTION_LINK_NAME', 'Click here.');
        Configuration::updateValue('COMMERCIALPROSPECTION_SUBJECT', 'Cart creation.');
        Configuration::updateValue('COMMERCIALPROSPECTION_CORPSE', 'You can click on the link below to get a cart created for you.');
        Configuration::updateValue('COMMERCIALPROSPECTION_MAIL', $default_mail);
        Configuration::updateValue('COMMERCIALPROSPECTION_NAME_SENDER', Configuration::get('PS_SHOP_NAME'));
        return parent::install() &&
            $this->registerHook('header') &&
            $this->installTab() &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('COMMERCIALPROSPECTION_LINK_NAME');

        return parent::uninstall()
            && $this->uninstallTab();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitCommercialprospectionModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCommercialprospectionModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'tinymce' => true,
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-link"></i>',
                        'desc' => $this->l('Link name in e-mail corpse.'),
                        'name' => 'COMMERCIALPROSPECTION_LINK_NAME',
                        'label' => $this->l('Link name :'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-location-arrow"></i>',
                        'desc' => $this->l('Default subject for mail.'),
                        'name' => 'COMMERCIALPROSPECTION_SUBJECT',
                        'label' => $this->l('Subject :'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'textarea',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Default corpse for mail.'),
                        'name' => 'COMMERCIALPROSPECTION_CORPSE',
                        'label' => $this->l('Corpse :'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Sender mail adresse.'),
                        'name' => 'COMMERCIALPROSPECTION_MAIL',
                        'label' => $this->l('Sender mail :'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Sender name of mail sended.'),
                        'name' => 'COMMERCIALPROSPECTION_NAME_SENDER',
                        'label' => $this->l('Sender name :'),
                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'COMMERCIALPROSPECTION_LINK_NAME' => Configuration::get('COMMERCIALPROSPECTION_LINK_NAME', null),
            'COMMERCIALPROSPECTION_SUBJECT' => Configuration::get('COMMERCIALPROSPECTION_SUBJECT', null),
            'COMMERCIALPROSPECTION_CORPSE' => Configuration::get('COMMERCIALPROSPECTION_CORPSE', null),
            'COMMERCIALPROSPECTION_MAIL' => Configuration::get('COMMERCIALPROSPECTION_MAIL', null),
            'COMMERCIALPROSPECTION_NAME_SENDER' => Configuration::get('COMMERCIALPROSPECTION_NAME_SENDER', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        global $cookie;
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');

        $ret = 'Ok';
        if (Tools::getValue('commercialprospection')){
            $pstring = Tools::getValue('commercialprospection');
            $ptab = explode(',', $pstring);
            if ($this->context->customer->id){
                $this->context->cart->id_customer = $this->context->customer->id;
            } else {
                if($this->context->cookie->id_guest) {
                    $guest = new Guest($this->context->cookie->id_guest);
                    $this->context->cart->mobile_theme = $guest->mobile_theme;
                }
                $this->context->cart->add();
                $this->context->cart->id_guest = $cookie->id_guest;
                if($this->context->cart->id) {
                    $this->context->cookie->id_cart = (int)$this->context->cart->id;
                }
            }
            if ($this->context->cart->id > 0){
                foreach($this->context->cart->getProducts() as $p){
                    $this->context->cart->updateQty(0, $p['id_product']);
                }
                foreach ($ptab as $p){
                    $p_infos = explode(':', $p);
                    $this->context->cart->updateQty($p_infos[1], $p_infos[0]);
                }
                $this->context->cart->update();

            } else {
                $this->context->cart->id_guest = $this->context->cookie->id_guest;
                foreach ($ptab as $p){
                    $p_infos = explode(':', $p);
                    $this->context->cart->updateQty($p_infos[1], $p_infos[0]);
                }
                $this->context->cart->update();
            }
            return '<script type="text/javascript">
            function reloadPage(){
                window.location.href = "' . _PS_BASE_URL_.__PS_BASE_URI__ . 'index.php?controller=order"
            }
            window.onload = reloadPage();
            </script>';
        }
    }

    /**
     * Installation du controller dans le backoffice
     * @return boolean
     */
    protected function installTab()
    {
        $tab = new Tab();
        $tab->class_name = 'AdminCommercialprospection';
        $tab->module = $this->name;
        $tab->id_parent = (int)Tab::getIdFromClassName('DEFAULT');
        $tab->icon = 'icon-won';
        $languages = Language::getLanguages();
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Commercial prospection');
        }
        try {
            $tab->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }

        return true;
    }

    /**
     * Désinstallation du controller admin
     * @return boolean
     */
    protected function uninstallTab()
    {
        $idTab = (int)Tab::getIdFromClassName('AdminCommercialprospection');
        if ($idTab) {
            $tab = new Tab($idTab);
            try {
                $tab->delete();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        return true;
    }

}
