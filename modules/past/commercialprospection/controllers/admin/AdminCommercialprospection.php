<?php

class AdminCommercialprospectionController extends AdminModulesController
{

    /**
    * Instanciation de la classe
    * Définition des paramètres basiques obligatoires
    */
    public function __construct()
    {
        $this->bootstrap = true; //Gestion de l'affichage en mode bootstrap
        $this->lang = true; //Flag pour dire si utilisation de langues ou non

        //Appel de la fonction parente pour pouvoir utiliser la traduction ensuite
        parent::__construct();

        //Liste des champs de l'objet à afficher dans la liste

        //Ajout d'actions sur chaque ligne
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

    /**
    * Affichage du formulaire d'ajout / création de l'objet
    * @return string
    * @throws SmartyException
    */
    public function renderForm()
    {
        //Définition du formulaire d'édition
        $this->fields_form = [
        //Entête
        'legend' => [
        'title' => $this->module->l('Edit Sample'),
        'icon' => 'icon-cog'
        ],
        //Champs
        'input' => [
        [
        'type' => 'text', //Type de champ
        'label' => $this->module->l('name'), //Label
        'name' => 'name', //Nom
        'class' => 'input fixed-width-sm', //classes css
        'size' => 50, //longueur maximale du champ
        'required' => true, //Requis ou non
        'empty_message' => $this->l('Please fill the postcode'), //Message d'erreur si vide
        'hint' => $this->module->l('Enter sample name') //Indication complémentaires de saisie
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('code'),
        'name' => 'code',
        'class' => 'input fixed-width-sm',
        'size' => 5,
        'required' => true,
        'empty_message' => $this->module->l('Please fill the code'),
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('email'),
        'name' => 'email',
        'class' => 'input fixed-width-sm',
        'size' => 5,
        'required' => true,
        'empty_message' => $this->module->l('Please fill email'),
        ],
        [
        'type' => 'text',
        'label' => $this->module->l('Title'),
        'name' => 'title',
        'class' => 'input fixed-width-sm',
        'lang' => true, //Flag pour utilisation des langues
        'required' => true,
        'empty_message' => $this->l('Please fill the title'),
        ],
        [
        'type' => 'textarea',
        'label' => $this->module->l('Description'),
        'name' => 'description',
        'lang' => true,
        'autoload_rte' => true, //Flag pour éditeur Wysiwyg
        ],
        ],
        //Boutton de soumission
        'submit' => [
        'title' => $this->l('Save'), //On garde volontairement la traduction de l'admin par défaut
        ]
        ];
        return parent::renderForm();
    }

    /*
    **
    */
    public function init(){
      parent::init();

    }

    private function genCustomError($type, $title, $data){
        $this->context->smarty->assign(array(
            'custom_error' => array(
                'type' => $type,
                'title' => $title,
                'data' => $data,
            )
        ));
    }
    private function sendMail($mail, $product_string, $subject, $corpse){

        $link = _PS_BASE_URL_.__PS_BASE_URI__ . 'index.php?controller=order&commercialprospection=' . $product_string;
        $link_html = '<br /><a href="' . $link .'" >'. Configuration::get('COMMERCIALPROSPECTION_LINK_NAME').'</a>';
        return Mail::Send(
            $this->context->language->id,
            'newsletter',
            $subject,
            array(
                '{firstname}' => '', // sender email address
                '{lastname}' => '', // sender email address
                '{message}' => nl2br($corpse) . $link_html // email content
            ),
            $mail, // receiver email address
            NULL, //receiver name
            Configuration::get('COMMERCIALPROSPECTION_MAIL'),
            Configuration::get('COMMERCIALPROSPECTION_NAME_SENDER')
        );
    }

    public function postProcess(){
        if (Tools::isSubmit('submitSendCommercialProspection')){
            if ( !($ms = Tools::getValue('mailString')) ) {
                $this->genCustomError('danger', 'Error', 'Please add minimum one mail in the input.');
            } else if ( !($ps = Tools::getValue('productString')) ){
                $this->genCustomError('danger', 'Error', 'Please add minimum one product in the input.');
            } else if ( !($subject = Tools::getValue('msgSubject')) ) {
                $this->genCustomError('danger', 'Error', 'Please add a subject to your mail.');
            } else if ( !($corpse = Tools::getValue('msgCorpse')) ) {
                $this->genCustomError('danger', 'Error', 'Please add a corpse to your mail.');
            } else {
                $mtab = explode(',', $ms);
                foreach ($mtab as $mail){
                    if ($this->sendMail($mail, $ps, $subject, $corpse)){
                        $this->genCustomError('success', 'Send !', 'Your products are be send to mails');
                    } else {
                        $this->genCustomError('danger', 'Error', 'Error trying to send e-mail, check your configuration.');
                    }
                }
            }
        }
    }

    public function initContent(){
        parent::initContent();
        $customer_list = Customer::getCustomers();
        $id_lang = Context::getContext()->language->id;
        $product_list = Product::getProducts($id_lang, 1, NULL, 'id_product', 'ASC');
        $myproduct_list = array();
        for ($i = 0; $i < count($product_list); $i++)
        {
            $p = $product_list[$i];
            if ($p['active'] == "1"){
                $n = array();
                $n['data'] = $p;
                $image = Image::getCover($p['id_product']);
                $n['img_link'] = $this->context->link->getImageLink($p['link_rewrite'], $image['id_image'], 'home_default');
                $n['quantity'] = 1;
                array_push($myproduct_list, $n);
            }
        }
        $this->context->smarty->assign(array(
            'customer_list' => $customer_list,
            'product_list' => $myproduct_list,
            'config' => array(
                'subject' => Configuration::get('COMMERCIALPROSPECTION_SUBJECT'),
                'corpse' => Configuration::get('COMMERCIALPROSPECTION_CORPSE'),
            )
        ));
        $this->setTemplate('../../../../modules/commercialprospection/views/templates/admin/AdminCommercialprospection.tpl');
    }
}
