<style media="screen">
    .control-label{
        text-align: right;
    }
    .cp-one-product{
        height: 90px;
        margin: 8px 0px;
        border: 1px solid;
        border-radius : 3px;
        overflow: hidden;
    }
    .cp-one-product-name{
        overflow: hidden;
        height: 90px;
        display: flex;
        align-items: center;
        justify-content: space-around;
    }
    .cp-one-product-name img{
        width: auto;
        height: 100%;
    }
    .cp-one-product-desc{
        height: 90px;
    }
    .cp-one-product-desc .actions{
        background: yellow;
        height: 90px;
    }
    .cp-one-product-actions h4{
        display: flex;
        align-items: center;
    }
    .myprocess-icon{
        font-size : 30px;
    }
</style>
{if isset($custom_error)}
<div class="alert alert-{$custom_error.type}">
    <h4>{$custom_error.title}</h4>
    <p>
        {$custom_error.data}
    </p>
</div>
{else}
{/if}
<form class="" action="" method="post">

    <div class="panel">
        <h3>Send a cart</h3>
        <div class="row"><h4>Ajouter des destinataires : </h4></div>
        <div class="alert alert-info">
            <p>
                Vous pouvez ajouter des mails un par un, séparé d'une virgule (Ex : mail1,mail2,etc..). <br />
                Ou choisir dans votre liste de client.
            </p>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <input type="text" id="mail-string" name="mailString" value="">
            </div>
            <div class="col-lg-6">
                <div>
                    <h4>Liste de client : </h4>
                    {foreach from=$customer_list item=c}
                    <div onclick="addToMailString('{$c.email}')">
                        <button class="btn btn-default" style="width : 100%;text-align:left;" type="button" name="button">
                            {$c.firstname} {$c.lastname} ({$c.email})
                        </button>
                    </div>
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="row"><h4>Ajouter des produits à son panier : </h4></div>
        <div class="alert alert-info">
            <p>Un lien qui remplit un panier automatiquement sera généré. Séléctionnez le produit et la quantité à ajouter.</p>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <input id="product-string" type="hidden" name="productString" value="">
                <div class="" id="cp-selected-product">

                </div>

            </div>
            <div class="col-lg-6">
                <div>
                    <h4>Ajouter des produits : </h4>
                    {foreach from=$product_list item=p}
                    <div onclick="addToProductString({$p.data.id_product})">
                        <button class="btn btn-default" style="width : 100%;text-align:left;" type="button" name="button">
                            {$p.data.name}
                        </button>
                    </div>
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="row"><h4>Texte à envoyer : </h4></div>
        <div class="form-group row">
            <label class="control-label col-lg-3" for="reference">
                <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Your internal reference code for this product. Allowed special characters .-_#\">
                    Sujet :
                </span>
            </label>
            <div class="col-lg-5">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="icon icon-location-arrow"></i>
                    </span>
                    <input type="text" name="msgSubject" value="{$config.subject}">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-lg-3" for="reference">
                <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Your internal reference code for this product. Allowed special characters .-_#\">
                    Corps du message :
                </span>
            </label>
            <div class="col-lg-5">
                <textarea name="msgCorpse" rows="8" cols="80" name="mailCorpse">{$config.corpse}</textarea>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" onclick="genProductString()" name="submitSendCommercialProspection" class="btn btn-default pull-right"><i class="icon-location-arrow myprocess-icon"></i><br /> Envoyer</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var products = {$product_list|@json_encode}
    var selected_products = [];
    function cleanEmpty(arr) {
        var clean = false;
        while(clean == false){
            clean = true;
            for (var i = 0; i < arr.length; i++){
                if (!arr[i]){
                    arr.splice(i, 1);
                    clean = false;
                    break;
                }
            }
        }
        return arr;
    }

    function exist(arr, value) {
        for (var i = 0; i < arr.length; i++){
            if (arr[i] == value){
                return i;
            }
        }
        return -1;
    }

    function addToMailString(mail) {
        var v = document.getElementById('mail-string').value;
        var tab = v.split(',');
        var ind = exist(tab, mail);
        if ( ind < 0 ) {
            tab.push(mail);
        } else {
            tab.splice(ind, 1)
        }
        tab = cleanEmpty(tab);
        document.getElementById('mail-string').value = tab.join(',');
    }

    function getProductById(id) {
        for (one of products) {
            if (one.data.id_product == id) {
                return one;
            }
        }
        return undefined;
    }
    function addToProductString(p_id) {
        var p = getProductById(p_id);
        var ind = exist(selected_products, p);
        if (ind < 0){
            selected_products.push(p);
        } else {
            selected_products.splice(ind, 1)
        }
        drawSelectedProducts();
        console.log('selected :', selected_products);
    }

    function genOneProductImage(product){
        var img = document.createElement('IMG');
        img.src = product.img_link
        var nImageDiv = document.createElement('div');
        nImageDiv.className = 'cp-one-product-name col-lg-2';
        nImageDiv.appendChild(img);
        return nImageDiv;
    }
    function genOneProductDesc(product) {
        var h3 = document.createElement('h4');
        var h4 = document.createElement('h5');
        h3.innerHTML = product.data.name;
        h4.innerHTML = 'Ref.:' + product.data.reference;
        var nDescDiv = document.createElement('div');
        nDescDiv.className = 'cp-one-product-desc col-lg-7';
        nDescDiv.appendChild(h3);
        nDescDiv.appendChild(h4);
        return nDescDiv;
    }
    function genOneProductAction(product){
        var h4 = document.createElement('h4');
        var input = '<input type="text" value="1" onchange="changeQuantity('+ product.data.id_product +', event)" />';
        var nActionsDiv = document.createElement('div');
        nActionsDiv.className = 'cp-one-product-actions col-lg-3';
        h4.innerHTML = 'Qty. : ' + input;
        var h4_trash = document.createElement('h4');
        h4_trash.innerHTML = '<button class="btn btn-default" onclick="deleteSelectedProductById(' + product.data.id_product + ')"><i class="icon-trash"></i></button>' ;
        nActionsDiv.appendChild(h4);
        nActionsDiv.appendChild(h4_trash);
        return nActionsDiv;
    }

    function genOneProduct(product){
        var nDiv = document.createElement('div');
        nDiv.className = 'cp-one-product row';
        nDiv.appendChild(genOneProductImage(product));
        nDiv.appendChild(genOneProductDesc(product));
        nDiv.appendChild(genOneProductAction(product));
        return nDiv;
    }

    function drawSelectedProducts() {
        var sp_div = document.getElementById('cp-selected-product');
        sp_div.innerHTML = ''
        for (p of selected_products) {
            sp_div.appendChild(genOneProduct(p));
        }
    }

    function deleteSelectedProductById(p_id) {
        for (var i = 0; i < selected_products.length; i++) {
            if (selected_products[i].data.id_product == p_id){
                selected_products.splice(i, 1)
                break;
            }
        }
    }

    for (var i = 0; i < selected_products.length; i++) {
        if (selected_products[i].data.id_product == p_id){
            selected_products[i].quantity = n_qty;
            break;
        }
    }
    function changeQuantity(p_id, evt) {
        var n_qty = evt.target.value;
        if (n_qty < 0){
            evt.target.value = 1;
            n_qty = 1;
        }
        for (var i = 0; i < selected_products.length; i++){
            if (selected_products[i].data.id_product == p_id){
                selected_products[i].quantity = n_qty;
            }
        }
        console.log('ChangeQty ; ', p_id, );
    }

    function genProductString() {
        var str = ''
        var i = 0;
        console.log('Before submit')
        for (one of selected_products) {
            if (i > 0){
                str += ',';
            }
            var id = one.data.id_product;
            var qty = one.quantity;
            str += id + ':' + qty;
            i++;
        }
        console.log('Before submit : ', str)
        document.getElementById('product-string').value = str;
    }
</script>
