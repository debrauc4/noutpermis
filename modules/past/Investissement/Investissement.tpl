
<style>
#chart svg {
  height: 400px;
}
</style>
<div class="panel">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-8">
        <div id="chart">
          <svg></svg>
        </div>
      </div>
      <div class="col-md-4">
        <table id="chart-data" class="table table-condensed table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th>Prix</th>
            </tr>
          </thead>
          {foreach from=$product_prices item=pp}
          <tr>
            <td>{$pp.datesend}</td>
            <td>{$pp.price} €</td>
          </tr>
          {/foreach}
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function getData(){
    var data = {$product_prices|@json_encode};
    var prices = []
    var ws_prices = []
    for (var i = 0; i < data.length; i++) {
      var d = new Date(data[i].datesend)
      if (data[i].id_price_invest == 1)
        prices.push([d.getTime(), data[i].price]);
      else if (data[i].id_price_invest == 2)
        ws_prices.push([d.getTime(), data[i].price]);
    }
    return [{
      "key": "Prix de vente",
      "values": prices
    }];
  }
  nv.addGraph(function() {
    var chart = nv.models.lineChart()
    .x(function(d) { return d[0] })
    .y(function(d) { return d[1]/100 }) //adjusting, 100% is 1.00, not 100 as it is in the data
    .color(d3.scale.category10().range())
    .useInteractiveGuideline(true)
    ;

    chart.xAxis
    .tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
    .tickFormat(function(d) {
      return d3.time.format('%x')(new Date(d))
    });

    chart.yAxis
    .tickFormat(d3.format('1f'));

    d3.select('#chart svg')
    .datum(getData())
    .call(chart);

    //TODO: Figure out a good way to do this automatically
    nv.utils.windowResize(chart.update);

    return chart;
  });

</script>
