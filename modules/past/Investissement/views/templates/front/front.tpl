
<style media="screen">
  #chart svg{
    height: 400px;
  }
</style>
<h4>Evolution du prix :</h4>

<div id="chart">
  <svg></svg>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.4/nv.d3.min.css"/>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" charset="utf-8"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.4/nv.d3.min.js"></script>
<script type="text/javascript">
  function getData(){
    var data = {$product_prices|@json_encode};
    var fmt = []
    for (var i = 0; i < data.length; i++) {
      var d = new Date(data[i].datesend)
      fmt.push([d.getTime(), data[i].price]);
    }
    return [{
      "key": "Prix",
      "values": fmt
    }];
  }
  nv.addGraph(function() {
    var chart = nv.models.cumulativeLineChart()
    .x(function(d) { return d[0] })
    .y(function(d) { return d[1]/100 }) //adjusting, 100% is 1.00, not 100 as it is in the data
    .color(d3.scale.category10().range())
    .useInteractiveGuideline(true)
    ;

    chart.xAxis
    .tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
    .tickFormat(function(d) {
      return d3.time.format('%x')(new Date(d))
    });

    chart.yAxis
    .tickFormat(d3.format(',.1%'));

    d3.select('#chart svg')
    .datum(getData())
    .call(chart);

    //TODO: Figure out a good way to do this automatically
    nv.utils.windowResize(chart.update);

    return chart;
  });

</script>
