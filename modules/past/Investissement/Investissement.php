<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Investissement extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'Investissement';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Guterwein';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Investissement Guterwein');
        $this->description = $this->l('Investissement guterwein');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('INVESTISSEMENT_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('ActionProductUpdate') &&
            $this->registerHook('displayAdminProductsExtra') &&
            $this->registerHook('displayProductExtraContent');
    }

    public function uninstall()
    {
        Configuration::deleteByName('INVESTISSEMENT_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitInvestissementModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitInvestissementModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'INVESTISSEMENT_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'INVESTISSEMENT_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'INVESTISSEMENT_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'INVESTISSEMENT_LIVE_MODE' => Configuration::get('INVESTISSEMENT_LIVE_MODE', true),
            'INVESTISSEMENT_ACCOUNT_EMAIL' => Configuration::get('INVESTISSEMENT_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'INVESTISSEMENT_ACCOUNT_PASSWORD' => Configuration::get('INVESTISSEMENT_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    public function hookDisplayProductExtraContent()
    {
        /* Place your code here. */
        return "<p style='color:blue;'>hookDisplayProductExtraContent</p>";
    }

    public function hookDisplayProductTab(){
      return "<p style='color:blue;'>hookDisplayProductTab</p>";
    }
    public function hookDisplayProductTabContent($params){
      $id_product = Tools::getValue('id_product');
      $product_prices = $this->_getAllProductPrices($id_product);
      $this->context->smarty->assign('product_prices', $product_prices);
      $this->context->smarty->assign('debuga', $id_product);
      $this->context->smarty->assign('debugb', $product_prices);

      return $this->display(__FILE__, 'views/templates/front/front.tpl');
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    private function _getAllProductPrices($id_product)
    {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'investissement WHERE id_product = ' . $id_product . ' ORDER BY datesend DESC';
        return Db::getInstance()->executes($sql);
    }

    public function hookDisplayAdminProductsExtra($params)
    {
      //return "<p>Hello world</p>";
      $id_product = Tools::getValue('id_product');
      $product_prices = $this->_getAllProductPrices($id_product);
      $this->context->smarty->assign('product_prices', $product_prices);
      return $this->display(__FILE__, 'Investissement.tpl');
    }
    public function hookActionProductUpdate($params) {
      //$this->context->smarty->assign('params', $params);
      Db::getInstance()->insert('investissement', array(
        'id_product' => $params['id_product'],
        'datesend' => date('Y-m-d H:i:s'),
        'id_price_invest' => 1,
        'price' => $params['product']->price
      ));
      Db::getInstance()->insert('investissement', array(
        'id_product' => $params['id_product'],
        'datesend' => date('Y-m-d H:i:s'),
        'id_price_invest' => 2,
        'price' => $params['product']->wholesale_price
      ));
    }
}
