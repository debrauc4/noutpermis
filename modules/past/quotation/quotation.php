<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr Academic Free License (AFL 3.0)
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_ . '../tools/tcpdf/tcpdf.php';

class Quotation extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'quotation';
        $this->tab = 'administration';
        $this->version = '0.0.1';
        $this->author = 'FranceNovatech';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Quotation');
        $this->description = $this->l('Create a button in cart \"Free quotation\",\
         the user is able to download a PDF of quotation of list of products');

        $this->confirmUninstall = $this->l('');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('QUOTATION_LIVE_MODE', false);
        Configuration::updateValue('QUOTATION_NUMBER', 1);
        Configuration::updateValue('QUOTATION_PREFIX', '#QU');
        Configuration::updateValue('QUOTATION_PICTURES', false);
        Configuration::updateValue('QUOTATION_TAX', false);
        Configuration::updateValue('QUOTATION_LEGAL_FREE_TEXT');
        Configuration::updateValue('QUOTATION_FOOTER_TEXT');
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayShoppingCart') &&
            $this->registerHook('displayHomeTab');
    }

    public function uninstall()
    {
        Configuration::deleteByName('QUOTATION_LIVE_MODE');
        Configuration::deleteByName('QUOTATION_NUMBER');
        Configuration::deleteByName('QUOTATION_PREFIX');
        Configuration::deleteByName('QUOTATION_PICTURES');
        Configuration::deleteByName('QUOTATION_TAX');
        Configuration::deleteByName('QUOTATION_LEGAL_FREE_TEXT');
        Configuration::deleteByName('QUOTATION_FOOTER_TEXT');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitQuotationModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitQuotationModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'col' => 3,
                        'desc' => $this->l('Enter Prefix "#QU" as default'),
                        'name' => 'QUOTATION_PREFIX',
                        'label' => 'Quotation Prefix',
                    ),
                    array(
                        'type' => 'switch',
                        'col' => 3,
                        'desc' => $this->l('Enable pictures products'),
                        'name' => 'QUOTATION_PICTURES',
                        'label' => 'Quotation Pictures',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'col' => 3,
                        'desc' => $this->l('Enable Tax'),
                        'name' => 'QUOTATION_TAX',
                        'label' => 'Quotation Tax',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'col' => 9,
                        'desc' => $this->l('Use this text for specific legal information.'),
                        'name' => 'QUOTATION_LEGAL_FREE_TEXT',
                        'label' => 'Quotation Legal Free Text',
                    ),
                    array(
                        'type' => 'text',
                        'col' => 6,
                        'desc' => $this->l('This text will appear at the bottom of the quotation.'),
                        'name' => 'QUOTATION_FOOTER_TEXT',
                        'label' => 'Quotation Footer Text',
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'QUOTATION_LIVE_MODE' => Configuration::get('QUOTATION_LIVE_MODE', true),
            'QUOTATION_ACCOUNT_EMAIL' => Configuration::get('QUOTATION_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'QUOTATION_ACCOUNT_PASSWORD' => Configuration::get('QUOTATION_ACCOUNT_PASSWORD', null),
            'QUOTATION_PREFIX' => Configuration::get('QUOTATION_PREFIX', '#QU'),
            'QUOTATION_PICTURES' => Configuration::get('QUOTATION_PICTURES', false),
            'QUOTATION_TAX' => Configuration::get('QUOTATION_TAX', false),
            'QUOTATION_LEGAL_FREE_TEXT' => Configuration::get('QUOTATION_LEGAL_FREE_TEXT'),
            'QUOTATION_FOOTER_TEXT' => Configuration::get('QUOTATION_FOOTER_TEXT'),
        );
    }

    /*class PDF extends FPDF{
        // Page footer
        function footer() {
            // Position at 1.5 cm from bottom
            $this->SetY(-15);
            // Arial italic 8
            $this->SetFont('Arial','I',8);
            // Page number
            $this->Cell(0,10,Configuration::get('QUOTATION_FOOTER_TEXT'),0,0,'C');
        }
    }*/

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayHomeTab()
    {
        /* Place your code here. */
    }

    public function hookDisplayShoppingCart($params)
    {
        /* Place your code here. */
        if (Tools::isSubmit('Devis')) {
            $this->genCartQuotation($params);
        }
        return $this->display(__FILE__, 'views/templates/hookDisplayShoppingCart.tpl');
    }

    public function genCartQuotationProducts(&$pdf)
    {
        $pdf->Cell('10', '10', 'Qté', 1);
        $pdf->Cell('110', '10', 'Désignation', 1);
        $pdf->Cell('35', '10', 'Prix u HT', 1);
        $pdf->Cell('35', '10', 'Prix total HT', 1);
        $pdf->Ln();
        $products = $this->context->cart->getProducts();
        $total_ht = 0;
        $total_ttc = 0;
        foreach ($products as &$pdt) {
            $pdf->Cell('10', '10', $pdt['cart_quantity'], 1);
            $pdf->Cell('110', '10', $pdt['name'] . "\r\n - " . $pdt['attributes_small'], 1);
            $price_ht = Product::getPriceStatic($pdt['id_product'], false, $pdt['id_product_attribute'], 2);
            $price_ttc = Product::getPriceStatic($pdt['id_product'], true, $pdt['id_product_attribute'], 2);
            $total_ht += $price_ht;
            $total_ttc += $price_ttc;
            $pdf->Cell('35', '10', $price_ht . '€', 1);
            $pdf->Cell('35', '10', ($price_ht * $pdt['cart_quantity']) . '€', 1);
            $pdf->Ln();
        }
        $pdf->Cell('10', '10', '', 0);
        $pdf->Cell('110', '10', '', 0);
        $pdf->Cell('35', '10', 'Total HT', 1);
        $pdf->Cell('35', '10', $total_ht . '€', 1);
        $pdf->Ln();
        $pdf->Cell('10', '10', '', 0);
        $pdf->Cell('110', '10', '', 0);
        $pdf->Cell('35', '10', 'T.V.A. 8,5% ', 1);
        $pdf->Cell('35', '10', ($total_ttc - $total_ht) . '€', 1);
        $pdf->Ln();
        $pdf->Cell('10', '10', '', 0);
        $pdf->Cell('110', '10', '', 0);
        $pdf->Cell('35', '10', 'Total TTC', 1);
        $pdf->Cell('35', '10', $total_ttc . '€', 1);
    }

    private function getTotalTTC()
    {
        $products =$this->context->cart->getProducts();
        $total_wt= 0;
        foreach ($products as $p) {
            $total_wt= $p['total_wt']+ $total_wt;
        }
        return $total_wt;
    }

    private function getTotalHT()
    {
        $products =$this->context->cart->getProducts();
        $total_ht= 0;
        foreach ($products as $p) {
            $total_ht= $p['total']+ $total_ht;
        }
        return $total_ht;
    }

    private function getTotalTax()
    {
        $products =$this->context->cart->getProducts();
        $total_tax= 0;
        foreach ($products as $p) {
            $total_tax= $p['total_wt'] - $p['total'] + $total_tax;
        }
        return $total_tax;
    }

    public function genCartQuotation()
    {
        $shop_logo = _PS_BASE_URL_.__PS_BASE_URI__.'/img/'.Configuration::get('PS_LOGO');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->AddPage();
        $shop_logo = _PS_BASE_URL_.__PS_BASE_URI__.'/img/'.Configuration::get('PS_LOGO');
        $logo_h = Configuration::get('SHOP_LOGO_HEIGHT');
        $logo_norme =30;
        $logo_pdf_w_div= $logo_h / $logo_norme ;
        $logo_w = Configuration::get('SHOP_LOGO_WIDTH') / $logo_pdf_w_div;
        $quotation_number = Configuration::get('QUOTATION_NUMBER');
        Configuration::updateValue('QUOTATION_NUMBER', $quotation_number +1);
        $quotation_prefix = Configuration::get('QUOTATION_PREFIX');
        $quotation_pictures = Configuration::get('QUOTATION_PICTURES');
        $quotation_tax = Configuration::get('QUOTATION_TAX');
        $quotation_legal_free_text = Configuration::get('QUOTATION_LEGAL_FREE_TEXT');
        $quotation_footer_text = Configuration::get('QUOTATION_FOOTER_TEXT');
        $pdf->Ln();
        $t_date= date("m/d/y");
        $Customer = new Customer($this->context->customer->id);
        $pdf->Image($shop_logo, 10, 10, $logo_w, $logo_norme);
        $pdf->Cell('150', '', '');
        $pdf->Cell('40', '', 'QUOTATION', 0, 0, 'R');
        $pdf->Ln();
        $pdf->Cell('150', '', '');
        $pdf->Cell('40', '', $t_date, 0, 0, 'R');
        $pdf->Ln();
        $pdf->Cell('150', '', '');
        $pdf->Cell('40', '1', $quotation_prefix . str_pad($quotation_number, 7, "0", STR_PAD_LEFT), 0, 0, 'R');
        $pdf->Ln();
        if ($this->context->customer->id >0) {
            $pdf->Ln();
            $pdf->Cell('100', '15', '');
            $pdf->Ln();
            $pdf->Cell('40', '0', Configuration::get('PS_SHOP_NAME'));
            $pdf->Cell('90', '9', '');
            $pdf->Cell('60', '0', $Customer->lastname . ' ' .$Customer->firstname);
            $pdf->Ln();
            $pdf->Cell('40', '0', Configuration::get('BLOCKCONTACT_EMAIL'));
            $pdf->Cell('90', '9', '');
            $pdf->Cell('60', '0', $Customer->email);
            $pdf->Ln();
            $pdf->Cell('40', '0', Configuration::get('BLOCKCONTACT_TELNUMBER'));
            $pdf->Cell('90', '9', '');
            $pdf->Ln();
        } else {
            $pdf->Ln();
            $pdf->Cell('100', '15', '');
            $pdf->Ln();
            $pdf->Cell('40', '0', Configuration::get('PS_SHOP_NAME'));
            $pdf->Cell('90', '9', '');
            $pdf->Cell('95', '0', 'Visiteur');
            $pdf->Ln();
            $pdf->Cell('40', '0', Configuration::get('BLOCKCONTACT_EMAIL'));
            $pdf->Cell('90', '9', '');
            $pdf->Cell('60', '0', $Customer->email);
            $pdf->Ln();
            $pdf->Cell('40', '0', Configuration::get('BLOCKCONTACT_TELNUMBER'));
            $pdf->Cell('90', '9', '');
            $pdf->Ln();
        }
        $total_w_adj= 190/23;
        $price= 0;
        $products=$this->context->cart->getProducts();
        $tax_on =0;
        $tax_off=$total_w_adj*2;
        if ($quotation_tax) {
            $tax_on =$tax_off ;
            $tax_off =0;
        }
        $pdf->SetFont('', '', 10, '', 'false');
        $pdf->SetFillColor(240, 240, 240);
        $pdf->MultiCell($total_w_adj* 2.5, '9', 'Reference', 'TLB', 'C', 1, 0);
        $pdf->MultiCell($total_w_adj * 7.5 + $tax_off, '9', 'Product', 'TB', 'C', 1, 0);
        if ($quotation_tax) {
            $pdf->MultiCell($tax_on, '9', 'Tax Rate', 'TB', 'C', 1, 0);
        }
        $pdf->MultiCell($total_w_adj * 3, '9', 'Base Price (Tax excl.)', 'TB', 'C', 1, 0);
        $pdf->MultiCell($total_w_adj * 3, '9', 'Unit Price (Tax excl.)', 'TB', 'C', 1, 0);
        $pdf->MultiCell($total_w_adj * 2, '9', 'Qty.', 'TB', 'C', 1, 0);
        $pdf->MultiCell($total_w_adj * 3, '9', 'Total', 'TRB', 'C', 1, 0);
        $pdf->Ln();
        $array_rate = array();
        $array_tax = array();
        $fill = 0;
        $pdf->SetFillColor(249, 249, 249);
        $ind=0;

        foreach ($products as &$p) {
            $pics_on=0;
            if ($quotation_pictures) {
                $pics_on=15;
                $image = Image::getCover($p['id_product']);
                $link = new Link;//because getImageLInk is not static function
                $im_path = $link->getImageLink($p['link_rewrite'], $image['id_image']);
            }
            $b_border='';
            $b_p= '-';
            if ($p['price_without_reduction'] != $p['price_with_reduction']) {
                $b_p= number_format($p['price_wt'], 2, ',', '') . '€';
            }

            $pdf->SetFont('', '', 10, '', 'false');
            $pdf->MultiCell($total_w_adj* 2.5, '15', $p['reference'], 'L', 'L', $fill, 0);
            $pdf->SetFont('', '', 0, '', 'false');
            if ($quotation_pictures) {
                $img_x= $pdf->GetX()+2.5;
                $img_y= $pdf->GetY()+2.5;
                $pdf->MultiCell('15', '15', $pdf->Image('http://'.$im_path, $img_x, $img_y, 10, 10), 0, 'C', $fill, 0);
            }
            $pdf->SetFont('', '', 10, '', 'false');
            $pdf->MultiCell($total_w_adj *7.5 -$pics_on +$tax_off, '15', $p['name'].$p['attributes'], 0, 'L', $fill, 0);
            if ($quotation_tax) {
                $pdf->Cell($tax_on, '15', number_format($p['rate'], 2, ',', '') . ' %', 0, 0, 'C', $fill, 0);
            }
            $price_wt_r_wt_t= number_format($p['price_with_reduction_without_tax'], 2, ',', '');
            $pdf->Cell($total_w_adj* 3, '15', $b_p, 0, 0, 'C', $fill, 0);
            $pdf->Cell($total_w_adj* 3, '15', $price_wt_r_wt_t . ' €', 0, 0, 'C', $fill, 0);
            $pdf->Cell($total_w_adj* 2, '15', $p['quantity'], 0, 0, 'C', $fill, 0);
            $pdf->Cell($total_w_adj* 3, '15', number_format($p['total'], 2, ',', '') . ' €', 'R', 1, 'C', $fill, 0);
            $price = $price + $p['total_wt'];
            $ind= $ind +1;
            if ($fill== 1) {
                $fill=0;
            } else {
                $fill =1;
            }
            if (!isset($array_tax[$p['rate']])) {
                $array_tax[$p['rate']]= array('base_price' => 0, 'tax' => 0);
                $array_rate[] = $p['rate'];
            }
            $array_tax[$p['rate']]['base_price']= $array_tax[$p['rate']]['base_price'] + $p['total'];
            $array_tax[$p['rate']]['tax']= $array_tax[$p['rate']]['tax'] + $p['total'] * $p['rate'] / 100 ;
        }
        $pdf->MultiCell('190', '9', '', 'T');
        $b_border='';
        $fill=0;
        if ($quotation_tax) {
            $pdf->MultiCell($total_w_adj* 3, '9', 'Tax Deatail', 'LT', 'C', 1, 0);
            $pdf->MultiCell($total_w_adj * 3, '9', 'Tax Rate', 'T', 'C', 1, 0);
            $pdf->MultiCell($total_w_adj * 3, '9', 'Base price', 'T', 'C', 1, 0);
            $pdf->MultiCell($total_w_adj * 2.5, '9', 'Total Tax', 'TR', 'C', 1, 0);
        } else {
            $pdf->MultiCell($total_w_adj*(3+3+3+2.5), '9', '', '', '', 0, 0);
        }
        $total_ht_total= number_format($this->getTotalHT(), 2, ',', '');
        $pdf->MultiCell($total_w_adj* 2, '9', '', '', '', 0, 0);
        $pdf->MultiCell($total_w_adj* 5, '9', 'Total (Tax excl.)', 'LT', 'C', 1, 0);
        $pdf->MultiCell($total_w_adj * 4.5, '9', $total_ht_total . ' €', 'TR', 'C', 0, 1);
        $ind = 0;
        $total_tax =  number_format($this->getTotalTax(), 2, ',', '') . ' €';
        $total_wt = number_format($this->getTotalHT() + $this->getTotalTax(), 2, ',', '') . ' €';
        $tax_name = array( 0 => 'Total Tax', 'Total');
        $tax_arr = array( 0 => $total_tax, $total_wt);
        $one_product=0;
        $done_=0;
        foreach ($array_tax as &$a) {
            if ($ind == count($array_tax) -1) {
                $b_border='B';
            }
            if ($quotation_tax) {
                if ($ind == 1) {
                    $tax_border='B';
                }
                $a_rate= number_format($array_rate[$ind], 3, ',', '');
                $a_b_price=  number_format($a['base_price'], 2, ',', '');
                $a_tax= number_format($a['tax'], 2, ',', '') ;
                $pdf->Cell($total_w_adj* 3, '9', 'Product', 'L' . $b_border, 0, 'C', $fill, 0);
                $pdf->Cell($total_w_adj* 3, '9', $a_rate . ' %', $b_border, 0, 'C', $fill, 0);
                $pdf->Cell($total_w_adj* 3, '9', $a_b_price . ' €', $b_border, 0, 'C', $fill, 0);
                $pdf->Cell($total_w_adj* 2.5, '9', $a_tax . ' €', 'R' . $b_border, 0, 'C', $fill, 0);
            } else {
                $pdf->MultiCell($total_w_adj* (2+3+3+3+2.5), '9', '', '', '', 0, 0);
            }
            if ($fill== 1) {
                $fill=0;
            } else {
                $fill =1;
            }
            if ($quotation_tax) {
                if (count($array_rate) >=2) {
                    $pdf->MultiCell($total_w_adj* 2, '9', '', '', '', 0, 0);
                    $pdf->MultiCell($total_w_adj * 5, '9', $tax_name[$ind], 'L' . $tax_border, 'C', 1, 0);
                    $pdf->MultiCell($total_w_adj * 4.5, '9', $tax_arr[$ind], $tax_border . 'R', 'C', 0, 1);
                } else {
                    $one_product=1;
                    $pdf->MultiCell($total_w_adj* 2, '9', '', '', '', 0, 0);
                    $pdf->MultiCell($total_w_adj * 5, '9', $tax_name[$ind], 'L', 'C', 1, 0);
                    $pdf->MultiCell($total_w_adj * 4.5, '9', $tax_arr[$ind], 'R', 'C', 0, 1);
                }
                $ind=$ind +1;
            } elseif ($done_==0) {
                $pdf->MultiCell($total_w_adj * 5, '9', $tax_name[1], 'LB', 'C', 1, 0);
                $pdf->MultiCell($total_w_adj * 4.5, '9', $tax_arr[1], 'BR', 'C', 0, 1);
                $done_=1;
            }
        }
        if ($one_product==1 && $quotation_tax) {
            $pdf->MultiCell($total_w_adj* (2+3+3+3+2.5), '9', '', '', '', 0, 0);
            $pdf->MultiCell($total_w_adj * 5, '9', $tax_name[1], 'LB', 'C', 1, 0);
            $pdf->MultiCell($total_w_adj * 4.5, '9', $tax_arr[1], 'BR', 'C', 0, 1);
        }

        $pdf->Ln();
        $pdf->MultiCell('190', '27', $quotation_legal_free_text, '', 'L', 0, 1);
        $pdf->Ln();
        $pdf->Output(Configuration::get('PS_SHOP_NAME').'.pdf', 'D');
    }
}
