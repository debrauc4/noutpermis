{*
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable. 
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr Academic Free License (AFL 3.0)
* 
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
* 
*  International Registered Trademark & Property of FranceNovatech
*}


<div class="panel">
	Module created by <a href="http://www.francenovatech.fr" target="_blank">FranceNovatech.fr</a>
</div>
