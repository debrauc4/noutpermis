/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/

function ageVerificationGoBack()
{
  var v = document.getElementById('ageverificationpopup-redirectlink').value;
  if (v.indexOf('http://') < 0){
      v = 'http://' + v;
  }
  location.href = v;
  return false;
}

function checkBirthDate(year)
{
    var v = document.getElementById('ageverificationpopup-date').value;
    v = new Date(v);
    var ageDifMs = Date.now() - v.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    var age = Math.abs(ageDate.getUTCFullYear() - 1970);
    if (age < year){
        var v = document.getElementById('ageverificationpopup-redirectlink').value;
        if (v.indexOf('http://') < 0){
            v = 'http://' + v;
        }
        location.href = v;
    }
}
