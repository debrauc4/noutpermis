{*
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*}


{if $cookievalue == 0 }
<div id="ageverificationpopup">
    <div class="content">
        <input type="hidden" name="" id="ageverificationpopup-redirectlink" value="{$config.redirect_link|escape:'htmlall':'UTF-8'}" readonly>
        <h2 style="background-color:{$config.round_color|escape:'htmlall':'UTF-8'}">{$config.year|escape:'htmlall':'UTF-8'}+</h2>
        <h1>Age verification</h1>
        <p>To enter this website you must be at least {$config.year|escape:'htmlall':'UTF-8'} years orld.</p>
        {if !$config.type}
        <form class="actions" action="" method="post">
            <button type="button" name="underrageButton" class="btn btn-primary" onclick="ageVerificationGoBack()">I'm underrage</button>
            <button type="submit" name="overageButton" class="btn btn-primary">I'm over {$years|escape:'htmlall':'UTF-8'} years old</button>
        </form>
        {else}
        <p>Please enter your birthdate :</p>
        <input type="date" id="ageverificationpopup-date"class="btn btn-primary" style="background-color : {$config.round_color|escape:'htmlall':'UTF-8'}" name="" value="" >
        <br />
        <form class="second-actions" action="" method="post">
            <button type="submit" name="overageButton" class="btn btn-primary" onclick="checkBirthDate({$config.year|escape:'htmlall':'UTF-8'})">Enter</button>
        </form>
        {/if}
    </div>
</div>
{/if}
