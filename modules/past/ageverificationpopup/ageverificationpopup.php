<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Ageverificationpopup extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'ageverificationpopup';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'FranceNovatech';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Age Verification Pop Up');
        $this->description = $this->l('Your customer and guest have to verify their age to acces your site.');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('AGEVERIFICATIONPOPUP_TYPE', 1);
        Configuration::updateValue('AGEVERIFICATIONPOPUP_ROUND_COLOR', '#b60859');
        Configuration::updateValue('AGEVERIFICATIONPOPUP_YEAR', 18);
        Configuration::updateValue('AGEVERIFICATIONPOPUP_REDIRECT_LINK', 'www.google.com');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayFooter');
    }

    public function uninstall()
    {
        Configuration::deleteByName('AGEVERIFICATIONPOPUP_TYPE');
        Configuration::deleteByName('AGEVERIFICATIONPOPUP_ROUND_COLOR');
        Configuration::deleteByName('AGEVERIFICATIONPOPUP_YEAR');
        Configuration::deleteByName('AGEVERIFICATIONPOPUP_REDIRECT_LINK');


        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitAgeverificationpopupModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitAgeverificationpopupModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(

                    // Radio button
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Type'),
                        'name' => 'AGEVERIFICATIONPOPUP_TYPE',
                        'class' => 't',
                        'desc' => $this->l('Choose how to ask to your guest this age.'),
                        'required'  => true,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'petit',
                                'value' => '0',
                                'label' => $this->l('Buttons.')
                            ),
                            array(
                                'id' => 'moyen',
                                'value' => '1',
                                'label' => $this->l('Textbox enter age.')
                            ),

                        ),
                    ),
                    array(
                        'col' => 6,
                        'type' => 'color',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('The background color of the round'),
                        'name' => 'AGEVERIFICATIONPOPUP_ROUND_COLOR',
                        'label' => $this->l('Round color'),
                    ),
                    array(
                        'col' => 2,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-calendar"></i>',
                        'desc' => $this->l('Year to enter in website'),
                        'name' => 'AGEVERIFICATIONPOPUP_YEAR',
                        'label' => $this->l('Year'),
                    ),
                    array(
                        'col' => 2,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-link"></i>',
                        'desc' => $this->l('Link to redirect your user.'),
                        'name' => 'AGEVERIFICATIONPOPUP_REDIRECT_LINK',
                        'label' => $this->l('Link'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'AGEVERIFICATIONPOPUP_TYPE' => Configuration::get('AGEVERIFICATIONPOPUP_TYPE'),
            'AGEVERIFICATIONPOPUP_ROUND_COLOR' => Configuration::get('AGEVERIFICATIONPOPUP_ROUND_COLOR', null),
            'AGEVERIFICATIONPOPUP_YEAR' => Configuration::get('AGEVERIFICATIONPOPUP_YEAR', null),
            'AGEVERIFICATIONPOPUP_REDIRECT_LINK' => Configuration::get('AGEVERIFICATIONPOPUP_REDIRECT_LINK', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayFooter(array $params)
    {
        // Your code.
        if (Tools::isSubmit('overageButton')) {
            $this->context->cookie->__set('ageverificationpopup', Tools::getToken(false));
            $this->context->cookie->write();
        }
        $this->context->smarty->assign(array('years' => 18));
        $cookievalue = 0;
        if ($this->context->cookie->__get('ageverificationpopup') == Tools::getToken(false)) {
            $cookievalue = 1;
        }
        $this->context->smarty->assign(array('cookievalue' => $cookievalue));
        $this->context->smarty->assign(array(
            'config' => array(
                'round_color' => Configuration::get('AGEVERIFICATIONPOPUP_ROUND_COLOR'),
                'year' => Configuration::get('AGEVERIFICATIONPOPUP_YEAR'),
                'type' => Configuration::get('AGEVERIFICATIONPOPUP_TYPE'),
                'redirect_link' => Configuration::get('AGEVERIFICATIONPOPUP_REDIRECT_LINK'),
            )
        ));

        return $this->display(__FILE__, 'views/templates/front/popup.tpl');
    }
}
