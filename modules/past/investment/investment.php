<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__).'/classes/InPrice.php');

class Investment extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'investment';
        $this->tab = 'administration';
        $this->version = '0.0.1';
        $this->author = 'FranceNovatech';
        $this->need_instance = 0;

        /**
        * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
        */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Investment');
        $this->description = $this->l('Creat a graph on a product sheet.');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
    * Don't forget to create update methods if needed:
    * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
    */
    public function install()
    {
        Configuration::updateValue('INVESTMENT_LIVE_MODE', false);
        InPrice::installDb();
        $products = Product::getProducts(Context::getContext()->language->id, 0, 5000, 'name', 'ASC');
        foreach ($products as $p) {
            $in_price= new InPrice();
            $in_price->id_product= $p['id_product'];
            $in_price->price_wt= $p['price']+ $p['price']*$p['rate']/100;
            $in_price->date_send= date('Y-m-d');
            $in_price->add();
        }
        return parent::install() &&
        $this->registerHook('header') &&
        $this->registerHook('displayAdminProductsExtra') &&
        $this->registerHook('displayProductTabContent') &&
        $this->registerHook('backOfficeHeader') &&
        $this->registerHook('actionProductAdd') &&
        $this->registerHook('actionProductDelete') &&
        $this->registerHook('actionProductUpdate');
    }

    public function uninstall()
    {
        Configuration::deleteByName('INVESTMENT_LIVE_MODE');
        InPrice::uninstallDb();
        return parent::uninstall();
    }

    /**
    * Load the configuration form
    */
    public function getContent()
    {
        /**
        * If values have been submitted in the form, process.
        */
        if (((bool)Tools::isSubmit('submitInvestmentModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
    * Create the form that will be displayed in the configuration of your module.
    */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitInvestmentModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
        .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
        'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
        'languages' => $this->context->controller->getLanguages(),
        'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
    * Create the structure of your form.
    */
    protected function getConfigForm()
    {
        return array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('Settings'),
        'icon' => 'icon-cogs',
        ),
        'input' => array(
        array(
        'type' => 'switch',
        'label' => $this->l('Live mode'),
        'name' => 'INVESTMENT_LIVE_MODE',
        'is_bool' => true,
        'desc' => $this->l('Use this module in live mode'),
        'values' => array(
        array(
        'id' => 'active_on',
        'value' => true,
        'label' => $this->l('Enabled')
        ),
        array(
        'id' => 'active_off',
        'value' => false,
        'label' => $this->l('Disabled')
        )
        ),
        ),
        array(
        'col' => 3,
        'type' => 'text',
        'prefix' => '<i class="icon icon-envelope"></i>',
        'desc' => $this->l('Enter a valid email address'),
        'name' => 'INVESTMENT_ACCOUNT_EMAIL',
        'label' => $this->l('Email'),
        ),
        array(
        'type' => 'password',
        'name' => 'INVESTMENT_ACCOUNT_PASSWORD',
        'label' => $this->l('Password'),
        ),
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        ),
        ),
        );
    }

    /**
    * Set values for the inputs.
    */
    protected function getConfigFormValues()
    {
        return array(
        'INVESTMENT_LIVE_MODE' => Configuration::get('INVESTMENT_LIVE_MODE', true),
        'INVESTMENT_ACCOUNT_EMAIL' => Configuration::get('INVESTMENT_ACCOUNT_EMAIL', 'contact@prestashop.com'),
        'INVESTMENT_ACCOUNT_PASSWORD' => Configuration::get('INVESTMENT_ACCOUNT_PASSWORD', null),
        );
    }

    /**
    * Save form data.
    */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    public function hookDisplayProductTabContent($params)
    {
        $id_product = Tools::getValue('id_product');
        $in_price = InPrice::getByIdProduct($id_product);
        $this->context->smarty->assign('in_price', $in_price);
        return $this->display(__FILE__, 'views/templates/front/front.tpl');
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        if (Tools::isSubmit("submitAndStay")) {
            //push add button
            if (Tools::getValue("add_in_price")==1) {
                $in_price= new InPrice();
                $this->context->smarty->assign(array('in_current' => $in_price));
            }
            //modify form submitted
            if (Tools::getValue("n_in_date_send") && Tools::getValue("n_in_price_wt")) {
                $n_in_date_send= Tools::getValue("n_in_date_send");
                $n_in_date_send = strtotime($n_in_date_send);
                $n_in_date_send = date('Y-m-d', $n_in_date_send);
                $n_in_price_wt= Tools::getValue("n_in_price_wt");
                $id_in_price= Tools::getValue("id_in_price");
                if ($id_in_price>0) {
                    //modify InPrice
                    $in_price = new InPrice($id_in_price);
                    $in_price->date_send = $n_in_date_send;
                    $in_price->price_wt = $n_in_price_wt;
                    $in_price->update();
                } else {
                    //add new InPrice
                    $in_price = new InPrice();
                    $in_price->date_send = $n_in_date_send;
                    $in_price->price_wt = $n_in_price_wt;
                    $in_price->id_product= Tools::getValue("id_product");
                    $in_price->add();
                }
            }
            //push delete button (trash)
            if (Tools::getValue("delete_id_in_price")) {
                $delete_id_in_price= new InPrice(Tools::getValue("delete_id_in_price"));
                $delete_id_in_price->delete();
            }
            //push puncil button
            if (Tools::getValue("view_id_in_price")) {
                $price= new InPrice(Tools::getValue("view_id_in_price"));
                $this->context->smarty->assign(array('in_current' => $price));
            }
        }
        //chart data
        $id_product = Tools::getValue('id_product');
        $in_price = InPrice::getByIdProduct($id_product, true);
        $this->context->smarty->assign('in_price', $in_price);
        return $this->display(__FILE__, 'views/templates/admin/investment.tpl');
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be added on the FO.
    */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    private function addPrice($params)
    {
        if(isset($params['id_product'])){
            $product = $params['product'];
            $price = $product->price + $product->price* $product->tax_rate/100;
            $price = number_format($price, 2);
            $in_price = new InPrice();
            $in_price->id_product = $params['id_product'];
            $in_price->price_wt = $price;
            $in_price->add();
        }
    }

    public function hookActionProductAdd($params)
    {
        $this->addPrice($params);
    }

    public function hookActionProductDelete()
    {
        /* Place your code here. */
    }

    public function hookActionProductUpdate($params)
    {
        $this->addPrice($params);
    }
}
