{*
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*}

<style media="screen">
  #chart svg{
    height: 400px;
  }
</style>
<section class="page-product-box">
    <h3 class="page-product-heading">Price evolution</h3>
    <!-- full description -->
    <div class="rte">
        <div id="chart">
            <svg></svg>
        </div>
    </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.4/nv.d3.min.css"/>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" charset="utf-8"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.4/nv.d3.min.js"></script>
<script type="text/javascript">
  function getData(){
    var data ={$in_price|@json_encode|escape:'htmlall':'UTF-8'};
    var fmt = []
    for (var i = 0; i < data.length; i++) {
      var d = new Date(data[i].date_send)
      fmt.push([d.getTime(), parseFloat(data[i].price_wt)]);
      if (i+1 <data.length){
          var t = new Date(data[i +1].date_send)
          while(d.setDate(d.getDate()+1) < t) {
              fmt.push([d.getTime(), parseFloat(data[i].price_wt)]);
          }
      }
      else {
          var t = new Date()
          while(d.setDate(d.getDate()+1) <= t) {
              fmt.push([d.getTime(), parseFloat(data[i].price_wt)]);
          }
      }
    }
    return [{
      "key": "Prix",
      "values": fmt
    }];
  }
  function getMaxY(){
      var data = {$in_price|@json_encode|escape:'htmlall':'UTF-8'};
      var max = 0;
      for(var i = 0; i < data.length; i++){
          if(max< parseFloat(data[i].price_wt)){
              max =  parseFloat(data[i].price_wt);
          }
      }
      return max;
  }
  nv.addGraph(function() {
    var chart = nv.models.lineChart()
    .margin({ right : 50 })  //Adjust chart margins to give the x-axis some breathing room.
    .x(function(d) { return d[0] })
    .y(function(d) { return d[1] }) //adjusting, 100% is 1.00, not 100 as it is in the data
    .color(d3.scale.category10().range())
    .useInteractiveGuideline(true)
    ;

    chart.xAxis
    .tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
    .tickFormat(function(d) {
      return d3.time.format('%x')(new Date(d))
    });

    chart.yAxis.tickFormat(function(d) { return '€' + d3.format(',.02f')(d) });
    chart.lines.forceY([0,getMaxY()+getMaxY()/10])
    d3.select('#chart svg')
    .datum(getData())
    .call(chart);

    //TODO: Figure out a good way to do this automatically
    nv.utils.windowResize(chart.update);

    return chart;
  });

</script>
