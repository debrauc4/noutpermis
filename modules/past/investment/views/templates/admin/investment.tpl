{*
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*}

<style media="screen">
  #in-chart svg{
    height: 300px;
  }
</style>
{if isset($in_current)}
<div class="panel">
    <h3>Modify</h3>
    {if isset($in_current->id_in_price)}
    <input type="hidden" name="id_in_price" value="{$in_current->id_in_price|escape:'htmlall':'UTF-8'}">
    {else}
    <input type="hidden" name="id_in_price" value="0">
    {/if}
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="control-label col-lg-3" for="date_send">
                    <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="">
                        Date
                    </span>
                </label>
                <div class="col-lg-5">
                    <input type="date" id="date_send" required name="n_in_date_send" value="{$in_current->date_send|escape:'htmlall':'UTF-8'}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3" for="price_wt">
                    <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="">
                        Price
                    </span>
                </label>
                <div class="col-lg-5">
                    <input type="number" required step="0.01" min="0" id="price_wt" name="n_in_price_wt" value="{$in_current->price_wt|escape:'htmlall':'UTF-8'}">
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')|escape:'htmlall':'UTF-8'}" class="btn btn-default"><i class="process-icon-cancel"></i> Annuler</a>
		<button type="submit" name="submitAndStay" class="btn btn-default pull-right" onclick="setIdInPrice({$in_current->id|escape:'htmlall':'UTF-8'})"><i class="process-icon-save"></i> Enregistrer</button>
		<button type="submit" name="submitAndStay" class="btn btn-default pull-right" onclick="setIdInPrice({$in_current->id|escape:'htmlall':'UTF-8'})"><i class="process-icon-save"></i> Enregistrer et rester</button>
	</div>
</div>

{/if}

<div class="panel">
    <h3>Price evolution</h3>
    <div class="row">
        <div class="col-lg-8">
            <!-- full description -->
            <div id="in-chart">
                <svg></svg>
            </div>
        </div>
        <div class="col-lg-4">
            <button type="submit" class="btn btn-default" name="submitAndStay" onclick="setAddInPrice()"> <i class="icon icon-plus"></i> ADD</button>
            <input type="hidden" name="add_in_price" value="" id="add_in_price">
            <input type="hidden" name="view_id_in_price" value="" id="view_id_in_price">
            <input type="hidden" name="delete_id_in_price" value="" id="delete_id_in_price">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Price</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$in_price item=one}
                    <tr>
                        <td>{$one->id|escape:'htmlall':'UTF-8'}</td>
                        <td>{$one->date_send|escape:'htmlall':'UTF-8'}</td>
                        <td>{$one->price_wt|escape:'htmlall':'UTF-8'}</td>
                        <td>

                            <button type="submit" class="btn btn-default" name="submitAndStay" onclick="setViewIdInPrice({$one->id|escape:'htmlall':'UTF-8'})">
                                <i class="icon icon-pencil"></i>
                            </button>
                            <button type="submit" class="btn btn-default" name="submitAndStay" onclick="deleteData({$one->id|escape:'htmlall':'UTF-8'})">
                                <i class="icon icon-trash"></i>
                            </button>
                        </td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.4/nv.d3.min.css"/>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" charset="utf-8"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.4/nv.d3.min.js"></script>
<script type="text/javascript">

function setIdInPrice(id)
{
    id=(id==undefined)?0:id;
    document.getElementById("id_in_price").value=id;
}

function setAddInPrice()
{
    document.getElementById("add_in_price").value=1;
}

function setViewIdInPrice(id)
{
    id=(id==undefined)?0:id;
    document.getElementById("view_id_in_price").value=id;

}

function deleteData(id)
{
    document.getElementById("delete_id_in_price").value=id;
}


function getData(){
    var data = {$in_price|@json_encode|escape:'htmlall':'UTF-8'};
    var fmt = []
    for (var i = 0; i < data.length; i++) {
        var d = new Date(data[i].date_send)
        fmt.push([d.getTime(), parseFloat(data[i].price_wt)]);
        if (i+1 <data.length){
            var t = new Date(data[i +1].date_send)
            while(d.setDate(d.getDate()+1) < t) {
                fmt.push([d.getTime(), parseFloat(data[i].price_wt)]);
            }
        }
        else {
            var t = new Date()
            while(d.setDate(d.getDate()+1) <= t) {
                fmt.push([d.getTime(), parseFloat(data[i].price_wt)]);
            }
        }
    }
    return [{
        "key": "Prix",
        "values": fmt
    }];
}
  function getMaxY(){
      var data = {$in_price|@json_encode|escape:'htmlall':'UTF-8'};
      var max = 0;
      for(var i = 0; i < data.length; i++){
          if(max< parseFloat(data[i].price_wt)){
              max =  parseFloat(data[i].price_wt);
          }
      }
      return max;
  }
  nv.addGraph(function() {
    var chart = nv.models.lineChart()
    .margin({ right : 50 })  //Adjust chart margins to give the x-axis some breathing room.
    .x(function(d) { return d[0] })
    .y(function(d) { return d[1] }) //adjusting, 100% is 1.00, not 100 as it is in the data
    .color(d3.scale.category10().range())
    .useInteractiveGuideline(true)
    ;

    chart.xAxis
    .tickValues([1078030800000,1122782400000,1167541200000,1251691200000])
    .tickFormat(function(d) {
      return d3.time.format('%x')(new Date(d))
    });

    chart.yAxis.tickFormat(function(d) { return '€' + d3.format(',.02f')(d) });
    chart.lines.forceY([0,getMaxY()+getMaxY()/10])
    d3.select('#in-chart svg')
    .datum(getData())
    .call(chart);

    //TODO: Figure out a good way to do this automatically
    nv.utils.windowResize(chart.update);

    return chart;
  });

</script>
