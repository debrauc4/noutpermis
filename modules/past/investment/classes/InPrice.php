<?php
/**
* 2019-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class InPrice extends ObjectModel
{
    public $id_in_price;
    public $id_product;
    public $price_wt;
    public $date_send;
    public static $definition = array(
      'table' => 'in_price',
      'primary' => 'id_in_price',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'price_wt' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
        'date_send' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
      ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null/*, Context $context = null*/)
    {
        parent::__construct($id, $id_lang, $id_shop);
    }
    /*public function add($autodate = true, $null_values = false)
    {
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }*/

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new InPrice($one[InPrice::$definition['primary']]);
        }
        return $ret;
    }

    public static function getByIdProduct($id_product, $admin = false)
    {
        $sql = 'SELECT '. InPrice::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . InPrice::$definition['table'];
        $sql .= ' WHERE id_product = ' . $id_product;
        if (!$admin) {
            $sql .= ' AND date_send <= NOW()';
        }
        $sql .= ' ORDER BY date_send';
        $rq = Db::getInstance()->ExecuteS($sql);
        return InPrice::genByDb($rq);
    }

    public static function installDb()
    {
        $tbl = _DB_PREFIX_. InPrice::$definition['table'];
        $primary = InPrice::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `id_in_price` int(11) NOT NULL,
            `id_product` int(11) NOT NULL,
            `price_wt` float(24) NOT NULL,
            `date_send` date NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. InPrice::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
