<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/

class PRStar extends ObjectModel
{
    public $id_pr_star;
    public $id_product;
    public $id_customer;
    public $nb_star;
    public $date_add;
    public $hidden;
    public static $definition = array(
      'table' => 'pr_star',
      'primary' => 'id_pr_star',
      'multilang' => false,
      'multilang_shop' => false,
      'fields' => array(
        'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'nb_star' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'hidden' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
        'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
      ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null/*, Context $context = null*/)
    {
        parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if (!parent::add($autodate, $null_values)) {
            return false;
        }
    }

    public function delete()
    {
        parent::delete();
    }

    private static function genByDb($arr)
    {
        $ret = array();
        foreach ($arr as &$one) {
            $ret[] = new PRStar($one[PRStar::$definition['primary']]);
        }
        return $ret;
    }

    public static function exist($id_product, $id_customer)
    {
        $sql = 'SELECT '. PRStar::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . PRStar::$definition['table'];
        $sql .= ' WHERE id_product = ' . $id_product . ' AND id_customer = ' . $id_customer;
        $rq = Db::getInstance()->ExecuteS($sql);
        return (count($rq) > 0);
    }

    public static function getByIdProduct($id_product, $viewhidden = true)
    {
        $sql = 'SELECT '. PRStar::$definition['primary'];
        $sql .= ' FROM '._DB_PREFIX_ . PRStar::$definition['table'];
        $sql .= ' WHERE id_product = ' . $id_product;
        if (!$viewhidden) {
            $sql .= ' AND hidden = ' . '0';
        }
        $rq = Db::getInstance()->ExecuteS($sql);
        return PRStar::genByDb($rq);
    }


    public static function avg($pr_stars, $view_hidden = false)
    {
        $total = 0;
        $denom = 0;
        foreach ($pr_stars as $pr_star) {
            if ($view_hidden) {
                $total += $pr_star->nb_star;
                $denom++;
            } else {
                if ($pr_star->hidden == 0) {
                    //not visible
                    $total += $pr_star->nb_star;
                    $denom++;
                }
            }
        }
        if ($denom == 0) {
            return 0;
        }
        $total = $total / $denom;
        return $total;
    }

    public static function installDb()
    {
        $tbl = _DB_PREFIX_. PRStar::$definition['table'];
        $primary = PRStar::$definition['primary'];
        Db::getInstance()->execute(
            'CREATE TABLE `'. $tbl .'` (
            `id_pr_star` int(11) NOT NULL,
            `id_customer` int(11) NOT NULL,
            `id_product` int(11) NOT NULL,
            `nb_star` int(11) NOT NULL,
            `date_add` date NOT NULL,
            `hidden` tinyint(4) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'
        );
        Db::getInstance()->execute(
            'ALTER TABLE '. $tbl .'
            ADD PRIMARY KEY (`'. $primary .'`);'
        );
        Db::getInstance()->execute(
            'ALTER TABLE `'. $tbl .'`
            MODIFY `'. $primary .'` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;'
        );
    }

    public static function uninstallDb()
    {
        $tbl =  _DB_NAME_ . '.' . _DB_PREFIX_. PRStar::$definition['table'];
        $drop = "DROP TABLE " . $tbl;
        $truncate = "TRUNCATE ". $tbl;
        Db::getInstance()->execute($truncate);
        Db::getInstance()->execute($drop);
    }
}
