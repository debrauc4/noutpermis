<?php
/**
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__).'/classes/PRStar.php');

class ProductRanking extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'productranking';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'FranceNovatech';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Product Ranking');
        $this->description = $this->l('Customer can now rank your product with a number of star, choosed by you.');

        $this->confirmUninstall = $this->l('');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->module_key = 'c562a938a013b9deaf4e54b9b50e827d';
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('PRODUCTRANKING_STAR_COLOR', '#e7711b');
        Configuration::updateValue('PRODUCTRANKING_NB_STAR', 5);
        Configuration::updateValue('PRODUCTRANKING_STAR_ICON_FULL', 'icon-star');
        Configuration::updateValue('PRODUCTRANKING_STAR_ICON_EMPTY', 'icon-star-empty');
        Configuration::updateValue('PRODUCTRANKING_STAR_ICON_HALF', 'icon-star-half-empty');
        Configuration::updateValue('PRODUCTRANKING_VERIFY_BEFORE', false);
        Configuration::updateValue('PRODUCTRANKING_DISPLAY_DISTRIBUTION', false);
        PRStar::installDb();
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayRightColumnProduct') &&
            $this->registerHook('displayAdminProductsExtra') &&
            $this->registerHook('displayProductTabContent');
    }

    public function uninstall()
    {
        Configuration::deleteByName('PRODUCTRANKING_STAR_COLOR');
        Configuration::deleteByName('PRODUCTRANKING_NB_STAR');
        Configuration::deleteByName('PRODUCTRANKING_STAR_ICON_FULL');
        Configuration::deleteByName('PRODUCTRANKING_STAR_ICON_EMPTY');
        Configuration::deleteByName('PRODUCTRANKING_STAR_ICON_HALF');
        Configuration::deleteByName('PRODUCTRANKING_VERIFY_BEFORE');
        Configuration::deleteByName('PRODUCTRANKING_DISPLAY_DISTRIBUTION');
        PRStar::uninstallDb();
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        if (((bool)Tools::isSubmit('submitProductRankingModule')) == true) {
            $this->postProcess();
        }
        $this->context->smarty->assign('module_dir', $this->_path);
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitProductRankingModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $color = Configuration::get('PRODUCTRANKING_STAR_COLOR');
        $icon_full = Configuration::get('PRODUCTRANKING_STAR_ICON_FULL');
        $icon_empty = Configuration::get('PRODUCTRANKING_STAR_ICON_EMPTY');
        $icon_half = Configuration::get('PRODUCTRANKING_STAR_ICON_HALF');
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-star"></i>',
                        'desc' => $this->l('Enter a valid number'),
                        'name' => 'PRODUCTRANKING_NB_STAR',
                        'label' => $this->l('Number'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon '. $icon_full .'"></i>',
                        'desc' => $this->l('Enter a valid number'),
                        'name' => 'PRODUCTRANKING_STAR_ICON_FULL',
                        'label' => $this->l('Icon full'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon '. $icon_half .'"></i>',
                        'desc' => $this->l('Enter a valid number'),
                        'name' => 'PRODUCTRANKING_STAR_ICON_HALF',
                        'label' => $this->l('Icon half'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon '. $icon_empty .'"></i>',
                        'desc' => $this->l('Enter a valid number'),
                        'name' => 'PRODUCTRANKING_STAR_ICON_EMPTY',
                        'label' => $this->l('Icon empty'),
                    ),
                    array(
                        'col' => 3,
                        'prefix' => '<i class="icon icon-color"></i>',
                        'type' => 'color',
                        'desc' => $this->l('Hexadecimal color of stars'),
                        'name' => 'PRODUCTRANKING_STAR_COLOR',
                        'label' => $this->l('Color'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Verify'),
                        'name' => 'PRODUCTRANKING_VERIFY_BEFORE',
                        'is_bool' => true,
                        'desc' => $this->l('Verify before publication.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Distribution'),
                        'name' => 'PRODUCTRANKING_DISPLAY_DISTRIBUTION',
                        'is_bool' => true,
                        'desc' => $this->l('Display rating distribution.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
          'PRODUCTRANKING_NB_STAR' => Configuration::get('PRODUCTRANKING_NB_STAR'),
          'PRODUCTRANKING_STAR_COLOR' => Configuration::get('PRODUCTRANKING_STAR_COLOR'),
          'PRODUCTRANKING_STAR_ICON_FULL' => Configuration::get('PRODUCTRANKING_STAR_ICON_FULL'),
          'PRODUCTRANKING_STAR_ICON_HALF' => Configuration::get('PRODUCTRANKING_STAR_ICON_HALF'),
          'PRODUCTRANKING_STAR_ICON_EMPTY' => Configuration::get('PRODUCTRANKING_STAR_ICON_EMPTY'),
          'PRODUCTRANKING_VERIFY_BEFORE' => Configuration::get('PRODUCTRANKING_VERIFY_BEFORE'),
          'PRODUCTRANKING_DISPLAY_DISTRIBUTION' => Configuration::get('PRODUCTRANKING_DISPLAY_DISTRIBUTION'),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path.'views/js/back.js');
        $this->context->controller->addCSS($this->_path.'views/css/back.css');
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayProductTabContent()
    {
        /* Place your code here. */
    }

    private function getCustomerStar($stars, $id_customer)
    {
        if (!$id_customer) {
            return null;
        }
        foreach ($stars as $star) {
            if ($star->id_customer == $id_customer) {
                return $star;
            }
        }
        return null;
    }

    private function initReviewCustomer($id_product)
    {
        $stars = PRStar::getByIdProduct($id_product);
        $customer_star = $this->getCustomerStar($stars, $this->context->customer->id);
        $this->context->smarty->assign(array('customer_star' => $customer_star));
        return $customer_star;
    }

    private function initReview($id_product)
    {
        $stars = PRStar::getByIdProduct($id_product);
        $stars_no_hidden = PRStar::getByIdProduct($id_product, false);
        $avg = PRStar::avg($stars);
        $avg_hidden = PRStar::avg($stars, true);
        $bool_stars = array();
        $cfg_nb_star = Configuration::get('PRODUCTRANKING_NB_STAR');
        for ($i = 0; $i < $cfg_nb_star; $i++) {
            $ret = 'e';
            if ($avg >= $i + 1) {
                $ret = 'f';
            } elseif ($avg < $i+1 && $avg >= $i+0.5) {
                $ret = 'h';
            }
            $bool_stars[] = $ret;
        }
        $this->context->smarty->assign(array('avg' => $avg));
        $this->context->smarty->assign(array('avg_hidden' => $avg_hidden));
        $this->context->smarty->assign(array('pr_stars' => $stars));
        $this->context->smarty->assign(array('pr_stars_no_hidden' => $stars_no_hidden));
        $this->context->smarty->assign(array('bool_stars' => $bool_stars));
    }

    private function getConfig()
    {
        $cfg = array(
            'nb_star' => Configuration::get('PRODUCTRANKING_NB_STAR'),
            'color' =>Configuration::get('PRODUCTRANKING_STAR_COLOR'),
            'display_distribution' => Configuration::get('PRODUCTRANKING_DISPLAY_DISTRIBUTION'),
            'icon_half' =>Configuration::get('PRODUCTRANKING_STAR_COLOR'),
            'icon_full' => Configuration::get('PRODUCTRANKING_STAR_ICON_FULL'),
            'icon_half' => Configuration::get('PRODUCTRANKING_STAR_ICON_HALF'),
            'icon_empty' => Configuration::get('PRODUCTRANKING_STAR_ICON_EMPTY'),
        );
        $this->context->smarty->assign(array('cfg' => $cfg));
    }

    public function hookDisplayRightColumnProduct($params)
    {
        //star submit
        $err_signin = $this->l('You have to sign in before send a review.');
        $err_admin = $this->l('Impossible to add your review. Please contact administrator.');
        $err_already = $this->l('You already send a review');
        if (Tools::isSubmit('product_ranking')) {
            if (!$this->context->customer->id) {
                //not connected
                $this->context->smarty->assign(array(
                  'customerror' => array(
                    'type' => 'warning',
                    'txt' => $err_signin
                  )));
            } else {
                $n = new PRStar();
                $n->id_customer = $this->context->customer->id;
                $n->id_product = Tools::getValue('id_product');
                $n->nb_star = Tools::getValue('nb_star');
                if ($n->nb_star > Configuration::get('PRODUCTRANKING_NB_STAR') || $n->nb_star < 1) {
                    $this->context->smarty->assign(array(
                      'customerror' => array(
                        'type' => 'warning',
                        'txt' => $err_admin
                      )));
                } elseif (PRStar::exist(Tools::getValue('id_product'), $this->context->customer->id)) {
                    $this->context->smarty->assign(array('customerror' => array(
                      'type' => 'warning',
                      'txt' => $err_already
                    )));
                } else {
                    $error_txt = $this->l('Successfully added.');
                    if (Configuration::get('PRODUCTRANKING_VERIFY_BEFORE')) {
                        $n->hidden = 1;
                        $error_txt .= $this->l(' An administrator have to verified now');
                    }
                    $n->add();
                    $this->context->smarty->assign(array(
                      'customerror' => array(
                        'type' => 'success', 'txt' => $error_txt
                      )));
                }
            }
        }
        //!star submit
        $this->getConfig();
        $this->initReview(Tools::getValue('id_product'));
        $this->initReviewCustomer(Tools::getValue('id_product'));
        //distribution init
        if (Configuration::get('PRODUCTRANKING_DISPLAY_DISTRIBUTION')) {
            $pr_stars = PRStar::getByIdProduct(Tools::getValue('id_product'));
            $this->initDistribution($pr_stars);
        }
        return $this->display(__FILE__, 'views/templates/front/displayProductTabContent.tpl');
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        if (Tools::isSubmit('changeStatus')) {
            $id_pr_star = Tools::getValue('id_pr_star');
            $ps = new PRStar($id_pr_star);
            $ps->hidden = ($ps->hidden)? 0 : 1;
            $ps->save();
        }
        $this->getConfig();
        $this->initDistribution(PRStar::getByIdProduct(Tools::getValue('id_product'), false));
        $this->initReview(Tools::getValue('id_product'));
        return $this->display(__FILE__, 'views/templates/admin/displayAdminProductsExtra.tpl');
    }

    private function initDistribution($pr_stars)
    {
        $nb_star = Configuration::get('PRODUCTRANKING_NB_STAR');
        $ret = array_fill(0, $nb_star, 0);
        foreach ($pr_stars as $ps) {
            if (!$ps->hidden) {
                $ind = $ps->nb_star -1;
                $ret[$ind]++;
            }
        }
        $this->context->smarty->assign(array('distribution' => $ret));
        return $ret;
    }
}
