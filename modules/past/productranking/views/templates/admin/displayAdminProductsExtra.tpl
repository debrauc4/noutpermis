{*
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*}

<div class="panel">
  <h3 class="tab"> <i class="icon-info"></i> Information</h3>
  <div class="">

    {l s='Global review' mod='productranking'} : {$avg_hidden|round:2|escape:'htmlall':'UTF-8'}/{$cfg.nb_star|escape:'htmlall':'UTF-8'} <i class="icon-star"></i><br />
    {l s='Visible review' mod='productranking'} : {$avg|round:2|escape:'htmlall':'UTF-8'}/{$cfg.nb_star|escape:'htmlall':'UTF-8'} <i class="icon-star"></i><br />
  </div>
  <div class="distrib-bloc">
    <h6>{l s='Distribution' mod='productranking'} :</h6>
    <ul class="distrib-bloc-content">
      {for $m=0 to $cfg.nb_star-1|escape:'htmlall':'UTF-8'}
      <li>
        {$cfg.nb_star - $m|escape:'htmlall':'UTF-8'} )
        {for $j=0 to $cfg.nb_star-1}
          {if $j+1 > $cfg.nb_star - $m}
          <i style="color : {$cfg.color|escape:'htmlall':'UTF-8'}" data-nb="{$i|escape:'htmlall':'UTF-8'}" class="icon icon-star-empty icon-distrib"></i>
          {else}
          <i style="color : {$cfg.color|escape:'htmlall':'UTF-8'}" data-nb="{$i|escape:'htmlall':'UTF-8'}" class="icon icon-star icon-distrib"></i>
          {/if}
        {/for}
        {$distribution[$cfg.nb_star - $m - 1]|escape:'htmlall':'UTF-8'}} {l s='reviews' mod='productranking'}
      </li>
      {/for}
    </ul>
  </div>
  <div class="">
    <input type="hidden" name="id_pr_star" value="{$ps->id_pr_star|escape:'htmlall':'UTF-8'}" id="id_pr_star">
    <table class="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>{l s='Date add' mod='productranking'}</th>
          <th>{l s='Star number' mod='productranking'}</th>
          <th>{l s='Visible' mod='productranking'}</th>
        </tr>
      </thead>
      <tbody>
        {foreach item=ps from=$pr_stars}
        <tr>
          <td>{$ps->id_pr_star|escape:'htmlall':'UTF-8'}</td>
          <td>{$ps->date_add|escape:'htmlall':'UTF-8'}</td>
          <td>{$ps->nb_star|escape:'htmlall':'UTF-8'}</td>
          <td>
            <button class="change-status-button" type="submit" name="changeStatus" onclick="changeIdPrStar({$ps->id_pr_star|escape:'htmlall':'UTF-8'})" >
              {if $ps->hidden == 0}
              <i class="icon-check"></i>
              {else}
              <i class="icon-remove"></i>
              {/if}
            </button>

          </td>
        </tr>
        {/foreach}
      </tbody>
    </table>
  </div>
</div>
