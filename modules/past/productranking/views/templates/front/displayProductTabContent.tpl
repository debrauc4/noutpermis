{*
* 2018-2019 FranceNovatech
*
* NOTICE OF LICENSE
*
*  This source file is not redistribuable and resellable.
*  You can use this module if you bought it
*  Only FranceNovatech and agreed collaborators can distribute and sell this file.
*  @author    FranceNovatech <francenovatech@gmail.com>
*  @copyright 2018-2019 FranceNovatech - All rights reserved
*  @license   http://www.francenovatech.fr
*
*  DISCLAIMER
*
*  Do not edit or add to this file manually.
*
*  International Registered Trademark & Property of FranceNovatech
*}

<form class="" action="" method="post">
  <!-- Global note -->
  <span class="star-nb">
    {$avg|round:2|escape:'htmlall':'UTF-8'}/{$cfg.nb_star|escape:'htmlall':'UTF-8'}
  </span>
  <input type="hidden" id="nNbStar" name="nb_star" value="" readonly>
  {assign var="i" value="1"}
  {foreach from=$bool_stars item=bs}
    <button class="star-button" type="submit" data-nb="{$i|escape:'htmlall':'UTF-8'}" name="product_ranking" onclick="productRankingConfirmReview(event)">
      {if $bs == 'f'}
      <i style="color : {$cfg.color|escape:'htmlall':'UTF-8'}" data-nb="{$i|escape:'htmlall':'UTF-8'}" class="icon {$cfg.icon_full}"></i>
      {else if $bs == 'h'}
      <i style="color : {$cfg.color|escape:'htmlall':'UTF-8'}" data-nb="{$i|escape:'htmlall':'UTF-8'}" class="icon {$cfg.icon_half}"></i>
      {else if $bs == 'e'}
      <i style="color : {$cfg.color|escape:'htmlall':'UTF-8'}" data-nb="{$i|escape:'htmlall':'UTF-8'}" class="icon {$cfg.icon_empty}"></i>
      {/if}
    </button>
    {$i=$i|intval}
    {$i=$i+1|intval}
  {/foreach}
  <span> {$pr_stars_no_hidden|@count|escape:'htmlall':'UTF-8'} {l s='reviews' mod='productranking'}</span>
  <!-- Customer note -->
  {if count($pr_stars) == 0}
  <p>{l s='Be the first to review this product.' mod='productranking'}</p>
  {/if}<br />
  {if $customer_star}
    {l s='Your review' mod='productranking'} : {$customer_star->nb_star|escape:'htmlall':'UTF-8'} <i class="icon-star" style="color : {$cfg.color|escape:'htmlall':'UTF-8'}"></i>
  {/if}
  <!-- Distribution -->
  {if $cfg.display_distribution}
  <div class="distrib-bloc">
    <h6>{l s='Distribution' mod='productranking'} :</h6>
    <ul class="distrib-bloc-content">
      {for $m=0 to $cfg.nb_star-1}
      <li>
        {$cfg.nb_star - $m|escape:'htmlall':'UTF-8'} )
        {for $j=0 to $cfg.nb_star-1}
          {if $j+1 > $cfg.nb_star - $m}
          <i style="color : {$cfg.color|escape:'htmlall':'UTF-8'}" data-nb="{$i|escape:'htmlall':'UTF-8'}" class="icon icon-star-empty icon-distrib"></i>
          {else}
          <i style="color : {$cfg.color|escape:'htmlall':'UTF-8'}" data-nb="{$i|escape:'htmlall':'UTF-8'}" class="icon icon-star icon-distrib"></i>
          {/if}
        {/for}
        {$distribution[$cfg.nb_star - $m - 1]|escape:'htmlall':'UTF-8'} reviews
      </li>
      {/for}
    </ul>
  </div>
  {/if}
</form>

{if isset($customerror)}
<article class="alert alert-{$customerror.type|escape:'htmlall':'UTF-8'}">
  <p>{$customerror.txt|escape:'htmlall':'UTF-8'}</p>
</article>
{/if}
