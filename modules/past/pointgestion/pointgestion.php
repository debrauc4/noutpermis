<?php
/*
** Created for nout'permis, all right reserved
**
*/
if (!defined('_PS_VERSION_')) {
    exit;
}

class Pointgestion extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'pointgestion';
        $this->tab = 'administration';
        $this->version = '0.0.1';
        $this->author = 'Nout\'Permis';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Gestion de point');
        $this->description = $this->l('Permet de gerer les points et l\'agenda, destiné exclusivement à Nout\'Permis. All right reserved.');

        $this->confirmUninstall = $this->l('');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('POINTGESTION_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayProductExtraContent') &&
            $this->registerHook('displayProductTab') &&
            $this->registerHook('displayProductTabContent') &&
            $this->registerHook('actionProductUpdate') &&
            $this->registerHook('actionProductDelete') &&
            $this->registerHook('displayAdminProductsExtra');
    }

    public function uninstall()
    {
        Configuration::deleteByName('POINTGESTION_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitPointgestionModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPointgestionModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'POINTGESTION_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'POINTGESTION_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'POINTGESTION_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'POINTGESTION_LIVE_MODE' => Configuration::get('POINTGESTION_LIVE_MODE', true),
            'POINTGESTION_ACCOUNT_EMAIL' => Configuration::get('POINTGESTION_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'POINTGESTION_ACCOUNT_PASSWORD' => Configuration::get('POINTGESTION_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        //update
        if ($params['id_product'] > 0){
          if (NPPointGestion::alreadyExist($params['id_product'])){
            $pg = NPPointGestion::getByIdProduct($params['id_product']);
          }
          else {
            $n = new NPPointGestion();
            $n->id_product = $params['id_product'];
            $n->save();
            $pg = $n;
          }
          $this->context->smarty->assign(array('pg' => $pg));
        }
        return $this->display(__FILE__, 'pointgestion.tpl');
    }

    public function hookDisplayProductTab()
    {
        /* Place your code here. */
        return "Hello World";
    }

    public function hookDisplayProductTabContent()
    {
        /* Place your code here. */
        return $this->display(__FILE__, 'pointgestion_pdt_tab.tpl');
    }


    public function hookActionProductUpdate($params)
    {
      if (Tools::getValue('id_product')){
        $n = NPPointGestion::getByIdProduct(Tools::getValue('id_product'));
        $n->nb_day = (int)Tools::getValue('np_point_nb_day');
        $n->nb_point = (int)Tools::getValue('np_point_nb_point');
        $n->save();
      }
    }

    public function hookActionProductDelete($params)
    {
      $n = NPPointGestion::getByIdProduct(Tools::getValue('id_product'));
      $n->delete();
      return true;
    }

    public function hookDisplayProductExtraContent($params)
    {
      $pg = NPPointGestion::getByIdProduct(Tools::getValue('id_product'));
      $array = array();
      $array[] = (new PrestaShop\PrestaShop\Core\Product\ProductExtraContent())
      ->setTitle('Points')
      ->setContent('<b>'. $pg->nb_point .'</b> heures de conduites vous seront crédités, ces heures seront valides <b>'. $pg->nb_day .'</b> jours');
      return $array;
    }
}
