<div class="">
  {if isset($error)}
  <article class="alert alert-danger">
    {$error.text}
  </article>
  {/if}
  <div class="col-md-12">
    <h2>Points</h2>
  </div>
  <div class="col-md-12 form-group">
    <div class="row">
      <div class="col-xl-2 col-lg-3">
        <label class="form-control-label">
          Nombre de points
          <span class="help-box" data-toggle="popover" data-content="Nombre de points crédité pour l'achat de ce produit." data-original-title="" title=""></span>
        </label>
        <div class="input-group money-type">
          <input type="text" name="np_point_nb_point" id="np_point_nb_point" class="form-control"  value="{$pg->nb_point}">
          <div class="input-group-append">
            <span class="input-group-text"> €</span>
          </div>
        </div>
      </div>
      <div class="col-xl-2 col-lg-3">
        <label class="form-control-label">
          Validité (jours) :
          <span class="help-box" data-toggle="popover" data-content="Validité en jours du nombre de point crédité pour l'achat de ce produit." data-original-title="" title=""></span>
        </label>
        <div class="input-group money-type">
          <input type="text" id="np_point_nb_day" name="np_point_nb_day" class="form-control" value="{$pg->nb_day}">
          <div class="input-group-append">
            <span class="input-group-text">j</span>
          </div>
        </div>
      </div>
    </div>
      <!--

      <div class="col-xl-4 col-lg-6 mx-auto">
        <label class="form-control-label">
          Prix unitaire (HT)
          <span class="help-box" data-toggle="popover" data-content="Certains produits produits peuvent se vendre par unité (par litre, par kilo, etc.) et il s'agit ici du prix pour une unité. Par exemple, si vous vendez du tissu, vous devez indiquer le prix au mètre." data-original-title="" title=""></span>
        </label>
        <div class="row">
          <div class="col-md-6">

            <div class="input-group money-type">
              <input type="text" id="form_step2_unit_price" name="form[step2][unit_price]" class="form-control" value="0,000000">
              <div class="input-group-append">
                <span class="input-group-text"> €</span>
              </div>
            </div>
          </div>
          <div class="col-md-6">

            <input type="text" id="form_step2_unity" name="form[step2][unity]" placeholder="Par kilo, par litre" class="form-control">

          </div>
        </div>
      </div>
      <div class="col-md-2 col-md-offset-1 hide">
        <label class="form-control-label">Éco-participation (TTC)</label>

        <div class="input-group money-type">
          <input type="text" id="form_step2_ecotax" name="form[step2][ecotax]" data-eco-tax-rate="0" class="form-control" value="0,000000">
          <div class="input-group-append">
            <span class="input-group-text"> €</span>
          </div>
        </div>
      </div>
    </div>
  -->
  </div>
</div>
