<style media="screen">
#shopdispatcher{
    display: flex;
    overflow-x: scroll;
}
.sd-one-group{
    display: flex;
}
.sd-one-group-title{
    width: 200px;
    height: 300px;
    background : url('../img/footer-bg.png') repeat-x #333;
    display: flex;
    align-items: center;
    justify-content: space-around;
}
.sd-one-group h3{
    height: 75px;
    width: 150px;
    background: black;
    color : white;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: space-around;
    padding-left: 10px;
}
.sd-shops-container{
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    width: 0;
    overflow: hidden;
    flex-direction: column;
    overflow-y: scroll;
    background: #f2f2f2;
    max-height: 300px;
}
.sd-one-shop{
    width: 100%;
    text-align: left;
    background-repeat: no-repeat;
    background-size: 100% auto;
    background-position: center;
    display: flex;
    align-items: flex-end;
    justify-content: center;
    min-height: 100px;
}

.sd-one-shop-title{
    background: black;
    color: white !important;
    padding: 0 10px;
    text-align: center;
    height: 37px;
    display: flex;
    align-items: center;
    justify-content: space-around;
    font-size: 1.5em;
    width: auto;
    min-width: 250px;
}
.sd-one-shop-title a{
    color : white;
    width: 100%;
}
</style>
<div class="row">
    <div id="shopdispatcher" class="col-lg-12">
        {foreach from=$shops item=shop key=j}
        {if isset($shop.name)}
        <a target="_blank" href="http://{$shop.domain_ssl}{$shop.uri}" class="sd-one-shop" style="background-image: url('{$shop.logo}')">
            <div class="sd-one-shop-title" >
                <h4>{$shop.name}</h4>
            </div>
        </a>
        {/if}
        {/foreach}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var boxWidth = $(".box").width();
        $(".sd-one-group").click(function(){
            var c = $(this).children('.sd-shops-container')
            var w = c.width();
            if (w == 0){
                c.animate({
                    width: "400"
                });
            }
            else {
                c.animate({
                    width: "0"
                });
            }
            var self = this;
            $(".sd-one-group").each(function( index ) {
                if (self != this){
                    $(this).children('.sd-shops-container').animate({
                        width: "0"
                    });
                }
            });
        });
    });
</script>
