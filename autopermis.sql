-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 20, 2019 at 11:22 AM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autopermis`
--

-- --------------------------------------------------------

--
-- Table structure for table `ap_access`
--

CREATE TABLE `ap_access` (
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_tab` int(10) UNSIGNED NOT NULL,
  `view` int(11) NOT NULL,
  `add` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_access`
--

INSERT INTO `ap_access` (`id_profile`, `id_tab`, `view`, `add`, `edit`, `delete`) VALUES
(1, 0, 1, 1, 1, 1),
(1, 1, 1, 1, 1, 1),
(1, 5, 1, 1, 1, 1),
(1, 7, 1, 1, 1, 1),
(1, 9, 1, 1, 1, 1),
(1, 10, 1, 1, 1, 1),
(1, 11, 1, 1, 1, 1),
(1, 13, 1, 1, 1, 1),
(1, 14, 1, 1, 1, 1),
(1, 15, 1, 1, 1, 1),
(1, 16, 1, 1, 1, 1),
(1, 19, 1, 1, 1, 1),
(1, 20, 1, 1, 1, 1),
(1, 21, 1, 1, 1, 1),
(1, 22, 1, 1, 1, 1),
(1, 23, 1, 1, 1, 1),
(1, 24, 1, 1, 1, 1),
(1, 25, 1, 1, 1, 1),
(1, 26, 1, 1, 1, 1),
(1, 27, 1, 1, 1, 1),
(1, 28, 1, 1, 1, 1),
(1, 29, 1, 1, 1, 1),
(1, 31, 1, 1, 1, 1),
(1, 32, 1, 1, 1, 1),
(1, 33, 1, 1, 1, 1),
(1, 34, 1, 1, 1, 1),
(1, 35, 1, 1, 1, 1),
(1, 36, 1, 1, 1, 1),
(1, 38, 1, 1, 1, 1),
(1, 39, 1, 1, 1, 1),
(1, 40, 1, 1, 1, 1),
(1, 41, 1, 1, 1, 1),
(1, 42, 1, 1, 1, 1),
(1, 44, 1, 1, 1, 1),
(1, 45, 1, 1, 1, 1),
(1, 48, 1, 1, 1, 1),
(1, 50, 1, 1, 1, 1),
(1, 52, 1, 1, 1, 1),
(1, 53, 1, 1, 1, 1),
(1, 54, 1, 1, 1, 1),
(1, 55, 1, 1, 1, 1),
(1, 56, 1, 1, 1, 1),
(1, 57, 1, 1, 1, 1),
(1, 58, 1, 1, 1, 1),
(1, 59, 1, 1, 1, 1),
(1, 61, 1, 1, 1, 1),
(1, 62, 1, 1, 1, 1),
(1, 63, 1, 1, 1, 1),
(1, 66, 1, 1, 1, 1),
(1, 67, 1, 1, 1, 1),
(1, 68, 1, 1, 1, 1),
(1, 69, 1, 1, 1, 1),
(1, 70, 1, 1, 1, 1),
(1, 71, 1, 1, 1, 1),
(1, 73, 1, 1, 1, 1),
(1, 74, 1, 1, 1, 1),
(1, 75, 1, 1, 1, 1),
(1, 76, 1, 1, 1, 1),
(1, 77, 1, 1, 1, 1),
(1, 78, 1, 1, 1, 1),
(1, 80, 1, 1, 1, 1),
(1, 81, 1, 1, 1, 1),
(1, 82, 1, 1, 1, 1),
(1, 83, 1, 1, 1, 1),
(1, 84, 1, 1, 1, 1),
(1, 86, 1, 1, 1, 1),
(1, 87, 1, 1, 1, 1),
(1, 88, 1, 1, 1, 1),
(1, 89, 1, 1, 1, 1),
(1, 92, 1, 1, 1, 1),
(1, 93, 1, 1, 1, 1),
(1, 94, 1, 1, 1, 1),
(1, 95, 1, 1, 1, 1),
(1, 96, 1, 1, 1, 1),
(1, 99, 1, 1, 1, 1),
(1, 100, 1, 1, 1, 1),
(1, 101, 1, 1, 1, 1),
(1, 102, 1, 1, 1, 1),
(1, 103, 1, 1, 1, 1),
(1, 104, 1, 1, 1, 1),
(1, 105, 1, 1, 1, 1),
(1, 106, 1, 1, 1, 1),
(1, 107, 1, 1, 1, 1),
(1, 108, 1, 1, 1, 1),
(1, 109, 1, 1, 1, 1),
(2, 0, 1, 1, 1, 1),
(2, 1, 0, 0, 0, 0),
(2, 2, 0, 0, 0, 0),
(2, 3, 0, 0, 0, 0),
(2, 4, 0, 0, 0, 0),
(2, 5, 0, 0, 0, 0),
(2, 6, 0, 0, 0, 0),
(2, 7, 0, 0, 0, 0),
(2, 8, 0, 0, 0, 0),
(2, 9, 1, 1, 1, 1),
(2, 10, 1, 1, 1, 1),
(2, 11, 1, 1, 1, 1),
(2, 12, 0, 0, 0, 0),
(2, 13, 1, 0, 1, 0),
(2, 14, 1, 1, 1, 1),
(2, 15, 0, 0, 0, 0),
(2, 16, 0, 0, 0, 0),
(2, 17, 0, 0, 0, 0),
(2, 18, 0, 0, 0, 0),
(2, 19, 0, 0, 0, 0),
(2, 20, 1, 1, 1, 1),
(2, 21, 1, 1, 1, 1),
(2, 22, 1, 1, 1, 1),
(2, 23, 1, 1, 1, 1),
(2, 24, 0, 0, 0, 0),
(2, 25, 0, 0, 0, 0),
(2, 26, 0, 0, 0, 0),
(2, 27, 1, 1, 1, 1),
(2, 28, 0, 0, 0, 0),
(2, 29, 0, 0, 0, 0),
(2, 30, 1, 1, 1, 1),
(2, 31, 1, 1, 1, 1),
(2, 32, 1, 1, 1, 1),
(2, 33, 1, 1, 1, 1),
(2, 34, 1, 1, 1, 1),
(2, 35, 1, 1, 1, 1),
(2, 36, 0, 0, 0, 0),
(2, 37, 1, 1, 1, 1),
(2, 38, 1, 1, 1, 1),
(2, 39, 0, 0, 0, 0),
(2, 40, 0, 0, 0, 0),
(2, 41, 0, 0, 0, 0),
(2, 42, 0, 0, 0, 0),
(2, 43, 0, 0, 0, 0),
(2, 44, 0, 0, 0, 0),
(2, 45, 0, 0, 0, 0),
(2, 46, 0, 0, 0, 0),
(2, 47, 0, 0, 0, 0),
(2, 48, 1, 1, 1, 1),
(2, 49, 1, 1, 1, 1),
(2, 50, 0, 0, 0, 0),
(2, 51, 0, 0, 0, 0),
(2, 52, 0, 0, 0, 0),
(2, 53, 0, 0, 0, 0),
(2, 54, 0, 0, 0, 0),
(2, 55, 0, 0, 0, 0),
(2, 56, 0, 0, 0, 0),
(2, 57, 0, 0, 0, 0),
(2, 58, 0, 0, 0, 0),
(2, 59, 0, 0, 0, 0),
(2, 60, 1, 0, 1, 0),
(2, 61, 0, 0, 0, 0),
(2, 62, 0, 0, 0, 0),
(2, 63, 0, 0, 0, 0),
(2, 64, 0, 0, 0, 0),
(2, 65, 0, 0, 0, 0),
(2, 66, 0, 0, 0, 0),
(2, 67, 0, 0, 0, 0),
(2, 68, 0, 0, 0, 0),
(2, 69, 0, 0, 0, 0),
(2, 70, 0, 0, 0, 0),
(2, 71, 0, 0, 0, 0),
(2, 72, 0, 0, 0, 0),
(2, 73, 0, 0, 0, 0),
(2, 74, 0, 0, 0, 0),
(2, 75, 0, 0, 0, 0),
(2, 76, 0, 0, 0, 0),
(2, 77, 0, 0, 0, 0),
(2, 78, 0, 0, 0, 0),
(2, 79, 0, 0, 0, 0),
(2, 80, 0, 0, 0, 0),
(2, 81, 0, 0, 0, 0),
(2, 82, 0, 0, 0, 0),
(2, 83, 0, 0, 0, 0),
(2, 84, 0, 0, 0, 0),
(2, 85, 0, 0, 0, 0),
(2, 86, 0, 0, 0, 0),
(2, 87, 0, 0, 0, 0),
(2, 88, 0, 0, 0, 0),
(2, 89, 0, 0, 0, 0),
(2, 90, 0, 0, 0, 0),
(2, 91, 0, 0, 0, 0),
(2, 92, 0, 0, 0, 0),
(2, 93, 0, 0, 0, 0),
(2, 94, 1, 1, 1, 1),
(2, 95, 1, 1, 1, 1),
(2, 96, 1, 1, 1, 1),
(2, 97, 0, 0, 0, 0),
(2, 98, 0, 0, 0, 0),
(2, 99, 1, 1, 1, 1),
(2, 100, 1, 1, 1, 1),
(2, 101, 0, 0, 0, 0),
(2, 102, 0, 0, 0, 0),
(2, 103, 0, 0, 0, 0),
(2, 104, 0, 0, 0, 0),
(2, 105, 0, 0, 0, 0),
(2, 106, 0, 0, 0, 0),
(2, 107, 0, 0, 0, 0),
(2, 108, 0, 0, 0, 0),
(2, 109, 0, 0, 0, 0),
(3, 0, 1, 1, 1, 1),
(3, 1, 0, 0, 0, 0),
(3, 2, 0, 0, 0, 0),
(3, 3, 0, 0, 0, 0),
(3, 4, 0, 0, 0, 0),
(3, 5, 1, 0, 0, 0),
(3, 6, 0, 0, 0, 0),
(3, 7, 0, 0, 0, 0),
(3, 8, 0, 0, 0, 0),
(3, 9, 1, 1, 1, 1),
(3, 10, 0, 0, 0, 0),
(3, 11, 0, 0, 0, 0),
(3, 12, 0, 0, 0, 0),
(3, 13, 0, 0, 0, 0),
(3, 14, 0, 0, 0, 0),
(3, 15, 1, 0, 0, 0),
(3, 16, 1, 0, 0, 0),
(3, 17, 0, 0, 0, 0),
(3, 18, 0, 0, 0, 0),
(3, 19, 0, 0, 0, 0),
(3, 20, 0, 0, 0, 0),
(3, 21, 1, 1, 1, 1),
(3, 22, 1, 1, 1, 1),
(3, 23, 0, 0, 0, 0),
(3, 24, 0, 0, 0, 0),
(3, 25, 0, 0, 0, 0),
(3, 26, 0, 0, 0, 0),
(3, 27, 0, 0, 0, 0),
(3, 28, 0, 0, 0, 0),
(3, 29, 0, 0, 0, 0),
(3, 30, 0, 0, 0, 0),
(3, 31, 0, 0, 0, 0),
(3, 32, 0, 0, 0, 0),
(3, 33, 0, 0, 0, 0),
(3, 34, 0, 0, 0, 0),
(3, 35, 0, 0, 0, 0),
(3, 36, 0, 0, 0, 0),
(3, 37, 0, 0, 0, 0),
(3, 38, 0, 0, 0, 0),
(3, 39, 0, 0, 0, 0),
(3, 40, 0, 0, 0, 0),
(3, 41, 0, 0, 0, 0),
(3, 42, 0, 0, 0, 0),
(3, 43, 0, 0, 0, 0),
(3, 44, 0, 0, 0, 0),
(3, 45, 0, 0, 0, 0),
(3, 46, 0, 0, 0, 0),
(3, 47, 0, 0, 0, 0),
(3, 48, 0, 0, 0, 0),
(3, 49, 0, 0, 0, 0),
(3, 50, 0, 0, 0, 0),
(3, 51, 0, 0, 0, 0),
(3, 52, 0, 0, 0, 0),
(3, 53, 0, 0, 0, 0),
(3, 54, 0, 0, 0, 0),
(3, 55, 0, 0, 0, 0),
(3, 56, 0, 0, 0, 0),
(3, 57, 0, 0, 0, 0),
(3, 58, 0, 0, 0, 0),
(3, 59, 1, 1, 1, 1),
(3, 60, 0, 0, 0, 0),
(3, 61, 0, 0, 0, 0),
(3, 62, 0, 0, 0, 0),
(3, 63, 0, 0, 0, 0),
(3, 64, 0, 0, 0, 0),
(3, 65, 0, 0, 0, 0),
(3, 66, 0, 0, 0, 0),
(3, 67, 0, 0, 0, 0),
(3, 68, 0, 0, 0, 0),
(3, 69, 0, 0, 0, 0),
(3, 70, 1, 1, 1, 1),
(3, 71, 0, 0, 0, 0),
(3, 72, 0, 0, 0, 0),
(3, 73, 0, 0, 0, 0),
(3, 74, 0, 0, 0, 0),
(3, 75, 0, 0, 0, 0),
(3, 76, 0, 0, 0, 0),
(3, 77, 0, 0, 0, 0),
(3, 78, 0, 0, 0, 0),
(3, 79, 0, 0, 0, 0),
(3, 80, 0, 0, 0, 0),
(3, 81, 0, 0, 0, 0),
(3, 82, 0, 0, 0, 0),
(3, 83, 0, 0, 0, 0),
(3, 84, 0, 0, 0, 0),
(3, 85, 0, 0, 0, 0),
(3, 86, 0, 0, 0, 0),
(3, 87, 0, 0, 0, 0),
(3, 88, 0, 0, 0, 0),
(3, 89, 0, 0, 0, 0),
(3, 90, 0, 0, 0, 0),
(3, 91, 0, 0, 0, 0),
(3, 92, 0, 0, 0, 0),
(3, 93, 0, 0, 0, 0),
(3, 94, 0, 0, 0, 0),
(3, 95, 0, 0, 0, 0),
(3, 96, 0, 0, 0, 0),
(3, 97, 0, 0, 0, 0),
(3, 98, 0, 0, 0, 0),
(3, 99, 0, 0, 0, 0),
(3, 100, 0, 0, 0, 0),
(3, 101, 0, 0, 0, 0),
(3, 102, 0, 0, 0, 0),
(3, 103, 0, 0, 0, 0),
(3, 104, 0, 0, 0, 0),
(3, 105, 0, 0, 0, 0),
(3, 106, 0, 0, 0, 0),
(3, 107, 0, 0, 0, 0),
(3, 108, 0, 0, 0, 0),
(3, 109, 0, 0, 0, 0),
(4, 0, 1, 1, 1, 1),
(4, 1, 0, 0, 0, 0),
(4, 2, 0, 0, 0, 0),
(4, 3, 0, 0, 0, 0),
(4, 4, 0, 0, 0, 0),
(4, 5, 1, 0, 0, 0),
(4, 6, 0, 0, 0, 0),
(4, 7, 0, 0, 0, 0),
(4, 8, 0, 0, 0, 0),
(4, 9, 1, 1, 1, 1),
(4, 10, 1, 1, 1, 1),
(4, 11, 1, 1, 1, 1),
(4, 12, 0, 0, 0, 0),
(4, 13, 1, 0, 1, 0),
(4, 14, 0, 0, 0, 0),
(4, 15, 0, 0, 0, 0),
(4, 16, 0, 0, 0, 0),
(4, 17, 0, 0, 0, 0),
(4, 18, 0, 0, 0, 0),
(4, 19, 1, 1, 1, 1),
(4, 20, 1, 0, 0, 0),
(4, 21, 1, 1, 1, 1),
(4, 22, 1, 1, 1, 1),
(4, 23, 0, 0, 0, 0),
(4, 24, 0, 0, 0, 0),
(4, 25, 0, 0, 0, 0),
(4, 26, 1, 0, 0, 0),
(4, 27, 0, 0, 0, 0),
(4, 28, 0, 0, 0, 0),
(4, 29, 0, 0, 0, 0),
(4, 30, 1, 1, 1, 1),
(4, 31, 1, 1, 1, 1),
(4, 32, 0, 0, 0, 0),
(4, 33, 0, 0, 0, 0),
(4, 34, 1, 1, 1, 1),
(4, 35, 0, 0, 0, 0),
(4, 36, 1, 1, 1, 1),
(4, 37, 1, 1, 1, 1),
(4, 38, 1, 1, 1, 1),
(4, 39, 1, 1, 1, 1),
(4, 40, 1, 1, 1, 1),
(4, 41, 0, 0, 0, 0),
(4, 42, 0, 0, 0, 0),
(4, 43, 0, 0, 0, 0),
(4, 44, 0, 0, 0, 0),
(4, 45, 0, 0, 0, 0),
(4, 46, 0, 0, 0, 0),
(4, 47, 0, 0, 0, 0),
(4, 48, 0, 0, 0, 0),
(4, 49, 0, 0, 0, 0),
(4, 50, 0, 0, 0, 0),
(4, 51, 0, 0, 0, 0),
(4, 52, 0, 0, 0, 0),
(4, 53, 0, 0, 0, 0),
(4, 54, 0, 0, 0, 0),
(4, 55, 0, 0, 0, 0),
(4, 56, 0, 0, 0, 0),
(4, 57, 0, 0, 0, 0),
(4, 58, 0, 0, 0, 0),
(4, 59, 0, 0, 0, 0),
(4, 60, 1, 0, 1, 0),
(4, 61, 0, 0, 0, 0),
(4, 62, 0, 0, 0, 0),
(4, 63, 0, 0, 0, 0),
(4, 64, 0, 0, 0, 0),
(4, 65, 0, 0, 0, 0),
(4, 66, 0, 0, 0, 0),
(4, 67, 0, 0, 0, 0),
(4, 68, 0, 0, 0, 0),
(4, 69, 0, 0, 0, 0),
(4, 70, 0, 0, 0, 0),
(4, 71, 0, 0, 0, 0),
(4, 72, 0, 0, 0, 0),
(4, 73, 0, 0, 0, 0),
(4, 74, 0, 0, 0, 0),
(4, 75, 0, 0, 0, 0),
(4, 76, 0, 0, 0, 0),
(4, 77, 0, 0, 0, 0),
(4, 78, 0, 0, 0, 0),
(4, 79, 0, 0, 0, 0),
(4, 80, 0, 0, 0, 0),
(4, 81, 0, 0, 0, 0),
(4, 82, 0, 0, 0, 0),
(4, 83, 0, 0, 0, 0),
(4, 84, 1, 1, 1, 1),
(4, 85, 0, 0, 0, 0),
(4, 86, 0, 0, 0, 0),
(4, 87, 0, 0, 0, 0),
(4, 88, 0, 0, 0, 0),
(4, 89, 0, 0, 0, 0),
(4, 90, 0, 0, 0, 0),
(4, 91, 1, 1, 1, 1),
(4, 92, 0, 0, 0, 0),
(4, 93, 1, 1, 1, 1),
(4, 94, 0, 0, 0, 0),
(4, 95, 0, 0, 0, 0),
(4, 96, 0, 0, 0, 0),
(4, 97, 0, 0, 0, 0),
(4, 98, 0, 0, 0, 0),
(4, 99, 1, 0, 0, 0),
(4, 100, 0, 0, 0, 0),
(4, 101, 0, 0, 0, 0),
(4, 102, 0, 0, 0, 0),
(4, 103, 0, 0, 0, 0),
(4, 104, 0, 0, 0, 0),
(4, 105, 0, 0, 0, 0),
(4, 106, 0, 0, 0, 0),
(4, 107, 0, 0, 0, 0),
(4, 108, 0, 0, 0, 0),
(4, 109, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_accessory`
--

CREATE TABLE `ap_accessory` (
  `id_product_1` int(10) UNSIGNED NOT NULL,
  `id_product_2` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_address`
--

CREATE TABLE `ap_address` (
  `id_address` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_manufacturer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_supplier` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_warehouse` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `alias` varchar(32) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `other` text,
  `phone` varchar(32) DEFAULT NULL,
  `phone_mobile` varchar(32) DEFAULT NULL,
  `vat_number` varchar(32) DEFAULT NULL,
  `dni` varchar(16) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_address`
--

INSERT INTO `ap_address` (`id_address`, `id_country`, `id_state`, `id_customer`, `id_manufacturer`, `id_supplier`, `id_warehouse`, `alias`, `company`, `lastname`, `firstname`, `address1`, `address2`, `postcode`, `city`, `other`, `phone`, `phone_mobile`, `vat_number`, `dni`, `date_add`, `date_upd`, `active`, `deleted`) VALUES
(1, 8, 0, 1, 0, 0, 0, 'Mon adresse', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '75002', 'Paris ', '', '0102030405', '', '', '', '2019-06-08 02:34:25', '2019-06-08 02:34:25', 1, 0),
(2, 21, 32, 0, 0, 1, 0, 'supplier', 'Fashion', 'supplier', 'supplier', '767 Fifth Ave.', '', '10153', 'New York', '', '(212) 336-1440', '', '', '', '2019-06-08 02:34:25', '2019-06-08 02:34:25', 1, 0),
(3, 21, 32, 0, 1, 0, 0, 'manufacturer', 'Fashion', 'manufacturer', 'manufacturer', '767 Fifth Ave.', '', '10154', 'New York', '', '(212) 336-1666', '', '', '', '2019-06-08 02:34:25', '2019-06-08 02:34:25', 1, 0),
(4, 21, 9, 1, 0, 0, 0, 'My address', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '33133', 'Miami', '', '0102030405', '', '', '', '2019-06-08 02:34:25', '2019-06-08 02:34:25', 1, 0),
(5, 176, 0, 2, 0, 0, 0, 'Mon adresse', '', 'DEBRAUWERE', 'Carl', '68 rue de Suffren', 'L24D', '33600', 'Pessac', '', '', '061126406', '', '', '2019-06-12 23:54:44', '2019-06-12 23:54:44', 1, 0),
(6, 176, 0, 4, 0, 0, 0, 'mon adresse', '', 'MARTY', 'Anne-Patrice', 'rue suffren ', '', '97410', 'saint pierre', '', '262584838', '', '', '', '2019-06-15 15:46:57', '2019-06-15 15:46:57', 1, 0),
(7, 176, 0, 8, 0, 0, 0, 'Mon adresse', '', 'marie', 'Marie', '14 rue marie curie', '', '97400', 'saint denis', '', '0262262263', '0692565352', '', '', '2019-06-15 16:35:56', '2019-06-15 16:36:40', 1, 0),
(8, 176, 0, 10, 0, 0, 0, 'Mon adresse', '', 'gremon', 'Marion', ' rue du relai', 'bat 4 ', '97410', 'saint pierre', '', '0692565841', '0692565841', '', '', '2019-06-17 16:28:45', '2019-06-17 16:28:45', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_address_format`
--

CREATE TABLE `ap_address_format` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `format` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_address_format`
--

INSERT INTO `ap_address_format` (`id_country`, `format`) VALUES
(1, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(2, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(3, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(4, 'firstname lastname\ncompany\naddress1\naddress2\ncity State:name postcode\nCountry:name\nphone\nphone_mobile'),
(5, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(6, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(7, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(8, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(9, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(10, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(11, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(12, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(13, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(14, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(15, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(16, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(17, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity\npostcode\nCountry:name\nphone\nphone_mobile'),
(18, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(19, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(20, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(21, 'firstname lastname\ncompany\naddress1 address2\ncity, State:name postcode\nCountry:name\nphone\nphone_mobile'),
(22, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(23, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(24, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(25, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(26, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(27, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(28, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(29, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(30, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(31, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(32, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(33, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(34, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(35, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(36, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(37, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(38, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(39, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(40, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(41, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(42, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(43, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(44, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(45, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(46, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(47, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(48, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(49, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(50, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(51, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(52, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(53, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(54, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(55, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(56, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(57, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(58, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(59, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(60, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(61, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(62, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(63, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(64, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(65, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(66, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(67, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(68, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(69, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(70, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(71, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(72, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(73, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(74, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(75, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(76, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(77, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(78, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(79, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(80, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(81, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(82, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(83, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(84, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(85, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(86, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(87, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(88, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(89, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(90, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(91, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(92, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(93, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(94, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(95, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(96, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(97, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(98, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(99, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(100, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(101, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(102, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(103, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(104, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(105, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(106, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(107, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(108, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(109, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(110, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(111, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(112, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(113, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(114, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(115, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(116, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(117, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(118, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(119, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(120, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(121, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(122, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(123, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(124, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(125, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(126, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(127, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(128, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(129, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(130, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(131, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(132, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(133, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(134, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(135, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(136, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(137, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(138, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(139, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(140, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(141, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(142, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(143, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(144, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(145, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(146, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(147, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(148, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(149, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(150, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(151, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(152, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(153, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(154, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(155, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(156, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(157, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(158, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(159, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(160, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(161, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(162, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(163, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(164, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(165, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(166, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(167, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(168, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(169, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(170, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(171, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(172, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(173, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(174, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(175, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(176, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(177, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(178, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(179, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(180, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(181, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(182, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(183, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(184, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(185, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(186, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(187, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(188, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(189, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(190, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(191, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(192, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(193, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(194, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(195, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(196, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(197, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(198, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(199, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(200, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(201, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(202, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(203, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(204, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(205, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(206, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(207, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(208, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(209, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(210, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(211, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(212, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(213, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(214, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(215, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(216, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(217, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(218, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(219, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(220, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(221, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(222, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(223, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(224, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(225, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(226, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(227, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(228, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(229, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(230, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(231, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(232, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(233, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(234, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(235, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(236, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(237, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(238, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(239, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(240, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(241, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(242, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(243, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(244, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile');

-- --------------------------------------------------------

--
-- Table structure for table `ap_advice`
--

CREATE TABLE `ap_advice` (
  `id_advice` int(11) NOT NULL,
  `id_ps_advice` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `ids_tab` text,
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `location` enum('after','before') NOT NULL,
  `selector` varchar(255) DEFAULT NULL,
  `start_day` int(11) NOT NULL DEFAULT '0',
  `stop_day` int(11) NOT NULL DEFAULT '0',
  `weight` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_advice`
--

INSERT INTO `ap_advice` (`id_advice`, `id_ps_advice`, `id_tab`, `ids_tab`, `validated`, `hide`, `location`, `selector`, `start_day`, `stop_day`, `weight`) VALUES
(4, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(5, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(10, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(11, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(16, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(17, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(22, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(23, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(28, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(29, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(34, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(35, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(40, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(41, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(46, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(47, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(52, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(53, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(58, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(59, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(64, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(65, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(67, 853, 1, NULL, 1, 0, 'after', '#dashtrends', 0, 0, 1),
(68, 27, 1, NULL, 1, 0, 'after', '#dashtrends', 0, 0, 1),
(69, 674, 1, NULL, 1, 0, 'after', '#dashtrends', 0, 0, 1),
(70, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(71, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1),
(72, 2147483647, 1, NULL, 1, 0, 'after', '.dash_news', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_advice_lang`
--

CREATE TABLE `ap_advice_lang` (
  `id_advice` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `html` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_advice_lang`
--

INSERT INTO `ap_advice_lang` (`id_advice`, `id_lang`, `html`) VALUES
(4, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(5, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(10, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(11, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(16, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(17, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(22, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(23, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(28, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(29, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(34, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(35, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(40, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(41, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(46, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(47, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(52, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(53, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>');
INSERT INTO `ap_advice_lang` (`id_advice`, `id_lang`, `html`) VALUES
(58, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(59, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(64, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(65, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(67, 1, '<div id=\"wrap_id_advice_853\">\n            <div class=\"col-lg-6\">\n              <section id=\"\" class=\"panel panel-advice\" style=\"position: relative; padding: 10px; min-height: 75px;\">\n                <a href=\"#\" id=\"853\" class=\"close_link gamification_premium_close\" style=\"display: none; position: absolute; top: 10px; right: 10px;\">\n                  <i class=\"icon-remove\"></i>\n                </a>\n                <span class=\"gamification-close-confirmation hide\">\n                  Etes vous sur ?\n                  <button class=\"btn btn-default btn-sm\" data-advice=\"delete\">\n                    <i class=\"icon-trash\"></i>Supprimer\n                  </button>\n                  <button class=\"btn btn-default btn-sm\" data-advice=\"cancel\">Annuler</button>\n                </span>\n                <a class=\"preactivationLink row\" rel=\"mailchimp\" href=\"{link}AdminModules{/link}&install=mailchimp&module_name=mailchimp&redirectconfig\" style=\"display: table; margin: 0; text-decoration: none;\">\n                  <img src=\"https://storage.googleapis.com/production-gamification-assets/premium/mailchimp.png\" class=\"advice-img img-thumbnail\">\n                  <img src=\"https://gamification.prestashop.com/api/getAdviceImg/853.png\" class=\"hide\">\n                  <p class=\"advice-description\" style=\"padding: 0 20px; display: table-cell; vertical-align: middle;\">Augmentez vos ventes et connectez vos clients avec les outils de MailChimp. Configure now</p>\n                </a>\n              </section>\n            </div>\n            <script>\n              $(document).ready( function () {\n                var link = $(\'#wrap_id_advice_853 .preactivationLink\')[0].href;\n                link = encodeURIComponent(link);\n                var newUrl = \'https://gamification.prestashop.com/get_advice_link.php?id_advice=853&url=\'+link;\n                $(\'#wrap_id_advice_853 .preactivationLink\')[0].href = newUrl;\n              });\n            </script>\n          </div>'),
(68, 1, '<div id=\"wrap_id_advice_27\">\n            <div class=\"col-lg-6\">\n              <section id=\"\" class=\"panel panel-advice\" style=\"position: relative; padding: 10px; min-height: 75px;\">\n                <a href=\"#\" id=\"27\" class=\"close_link gamification_premium_close\" style=\"display: none; position: absolute; top: 10px; right: 10px;\">\n                  <i class=\"icon-remove\"></i>\n                </a>\n                <span class=\"gamification-close-confirmation hide\">\n                  Etes vous sur ?\n                  <button class=\"btn btn-default btn-sm\" data-advice=\"delete\">\n                    <i class=\"icon-trash\"></i>Supprimer\n                  </button>\n                  <button class=\"btn btn-default btn-sm\" data-advice=\"cancel\">Annuler</button>\n                </span>\n                <a class=\"preactivationLink row\" rel=\"paypal\" href=\"{link}AdminModules{/link}&install=paypal&module_name=paypal&redirectconfig\" style=\"display: table; margin: 0; text-decoration: none;\">\n                  <img src=\"https://storage.googleapis.com/production-gamification-assets/premium/paypal.png\" class=\"advice-img img-thumbnail\">\n                  <img src=\"https://gamification.prestashop.com/api/getAdviceImg/27.png\" class=\"hide\">\n                  <p class=\"advice-description\" style=\"padding: 0 20px; display: table-cell; vertical-align: middle;\">Améliorez vos ventes avec PayPal. Essayez le module gratuit</p>\n                </a>\n              </section>\n            </div>\n            <script>\n              $(document).ready( function () {\n                var link = $(\'#wrap_id_advice_27 .preactivationLink\')[0].href;\n                link = encodeURIComponent(link);\n                var newUrl = \'https://gamification.prestashop.com/get_advice_link.php?id_advice=27&url=\'+link;\n                $(\'#wrap_id_advice_27 .preactivationLink\')[0].href = newUrl;\n              });\n            </script>\n          </div>'),
(69, 1, '<div id=\"wrap_id_advice_674\">\n            <div class=\"col-lg-6\">\n              <section id=\"\" class=\"panel panel-advice\" style=\"position: relative; padding: 10px; min-height: 75px;\">\n                <a href=\"#\" id=\"674\" class=\"close_link gamification_premium_close\" style=\"display: none; position: absolute; top: 10px; right: 10px;\">\n                  <i class=\"icon-remove\"></i>\n                </a>\n                <span class=\"gamification-close-confirmation hide\">\n                  Etes vous sur ?\n                  <button class=\"btn btn-default btn-sm\" data-advice=\"delete\">\n                    <i class=\"icon-trash\"></i>Supprimer\n                  </button>\n                  <button class=\"btn btn-default btn-sm\" data-advice=\"cancel\">Annuler</button>\n                </span>\n                <a class=\"preactivationLink row\" rel=\"sendinblue\" href=\"{link}AdminModules{/link}&install=sendinblue&module_name=sendinblue&redirectconfig\" style=\"display: table; margin: 0; text-decoration: none;\">\n                  <img src=\"https://storage.googleapis.com/production-gamification-assets/premium/sendinblue.png\" class=\"advice-img img-thumbnail\">\n                  <img src=\"https://gamification.prestashop.com/api/getAdviceImg/674.png\" class=\"hide\">\n                  <p class=\"advice-description\" style=\"padding: 0 20px; display: table-cell; vertical-align: middle;\">Gérez facilement vos campagnes mails, newsletter, SMS sur une seule plateforme<br/></p>\n                </a>\n              </section>\n            </div>\n            <script>\n              $(document).ready( function () {\n                var link = $(\'#wrap_id_advice_674 .preactivationLink\')[0].href;\n                link = encodeURIComponent(link);\n                var newUrl = \'https://gamification.prestashop.com/get_advice_link.php?id_advice=674&url=\'+link;\n                $(\'#wrap_id_advice_674 .preactivationLink\')[0].href = newUrl;\n              });\n            </script>\n          </div>'),
(70, 1, ' <div id=\"wrap_id_advice_1533286918885\"> <section id=\"0_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Augmentez vos ventes en rappelant aux clients les articles qu\'ils ont laissés en suspens</b> </p> <p> 67 % des paniers d\'achat en ligne sont abandonnés avant d\'avoir été réglés. Les courriels de panier abandonné vous aident à faire un suivi auprès des personnes ayant délaissé leur panier et les convaincre d\'achever leur achat.  </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Récupérez les ventes perdues </a> </span> </div> </section><section id=\"1_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Vendre plus en récompensant des clients fidèles</b> </p> <p> Récompensez vos meilleurs clients au moyen d\'une série de courriels automatisés contenant des bons spéciaux ou d\'autres offres exclusives, et obtenez un nombre de commandes par destinataire presque 5 fois supérieur à ce que vous obtiendriez avec un envoi massif ordinaire. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Remerciez vos clients </a> </span> </div> </section><section id=\"2_Mailchimp\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Mailchimp.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1533286918885.png\"/> <p> <b>Boostez l\'engagement et les revenus en vous renseignant mieux au sujet de votre audience</b> </p> <p> Créez des courriels personnalisés et pertinents ciblant votre audience en fonction de données démographiques telles que le sexe et l\'âge. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1533286918885&url=https://addons.prestashop.com/fr/newsletter-sms/26957-mailchimp-for-prestashop.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DMailchimp\"> Voir comment MailChimp peut vous aider </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(2)+\'_Mailchimp\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(71, 1, ' <div id=\"wrap_id_advice_1539881809586\"> <section id=\"0_PrestaShop ERP\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/PrestaShop ERP.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1539881809586.png\"/> <p> <b>La suite PrestaShop ERP réalisée en partenariat avec Boost My Shop</b> </p> <p> PrestaShop ERP propose une suite de modules autonomes qui proposent aux commerçants des fonctionnalités standard et avancées de gestion des stocks, de gestion des commandes d\'approvisionnement et de préparation des achats. </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1539881809586&url=https://addons.prestashop.com/151_%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DPrestaShop ERP\"> En savoir plus </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_PrestaShop ERP\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>'),
(72, 1, ' <div id=\"wrap_id_advice_1529506639880\"> <section id=\"0_SitemapGenerator_addons\" class=\"panel\" style=\"display:none\"> <header class=\"panel-heading\"> <span class=\"icon-lightbulb\"></span> Conseil du jour </header> <div class=\"panel-body\" style=\"padding:0\"> <img class=\"img-responsive\" src=\"https://storage.googleapis.com/production-gamification-assets/tips/Sitemap Generator.png\" style=\"float:right; margin-left: 5px;\"> <img src=\"https://gamification.prestashop.com/api/getAdviceImg/1529506639880.png\"/> <p> <b>Nouveau : Module Générateur de Sitemap pour SEO (XML et HTML) </b> </p> <p> Avec le nouveau module Générateur de Sitemap pour SEO développé par PrestaShop, créez d\'un seul clic les sitemaps de votre boutique ! \n </p> <span class=\"text-right\" style=\"float:right\"> <a class=\"btn btn-default\" target=\"_blank\" href=\"https://gamification.prestashop.com/get_advice_link.php?id_advice=1529506639880&url=https://addons.prestashop.com/fr/seo-referencement-naturel/32219-generateur-de-sitemap-pour-seo-xml-et-html.html%3Futm_source%3Dback-office%26utm_medium%3Dtipoftheday%26utm_campaign%3Dpartenariats%26utm_content%3DSitemapGenerator_addons\"> Découvrir </a> </span> </div> </section><script> $(document).ready( function () { $(\'#\'+rand(0)+\'_SitemapGenerator_addons\').show(); }); function rand(nbr){ return Math.floor(Math.random()*(nbr+1)); } </script> </div>');

-- --------------------------------------------------------

--
-- Table structure for table `ap_alias`
--

CREATE TABLE `ap_alias` (
  `id_alias` int(10) UNSIGNED NOT NULL,
  `alias` varchar(255) NOT NULL,
  `search` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_alias`
--

INSERT INTO `ap_alias` (`id_alias`, `alias`, `search`, `active`) VALUES
(1, 'bloose', 'blouse', 1),
(2, 'blues', 'blouse', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_attachment`
--

CREATE TABLE `ap_attachment` (
  `id_attachment` int(10) UNSIGNED NOT NULL,
  `file` varchar(40) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `file_size` bigint(10) UNSIGNED NOT NULL DEFAULT '0',
  `mime` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attachment`
--

INSERT INTO `ap_attachment` (`id_attachment`, `file`, `file_name`, `file_size`, `mime`) VALUES
(1, '43a60117c27f6c65e348078c6e8db7d9d31c1009', 'Définition et classification des contrats.pdf', 28031, 'application/pdf');

-- --------------------------------------------------------

--
-- Table structure for table `ap_attachment_lang`
--

CREATE TABLE `ap_attachment_lang` (
  `id_attachment` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attachment_lang`
--

INSERT INTO `ap_attachment_lang` (`id_attachment`, `id_lang`, `name`, `description`) VALUES
(1, 1, 'Les contrat, les diffeents type', 'Test Carl, a supprimer');

-- --------------------------------------------------------

--
-- Table structure for table `ap_attribute`
--

CREATE TABLE `ap_attribute` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `color` varchar(32) DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attribute`
--

INSERT INTO `ap_attribute` (`id_attribute`, `id_attribute_group`, `color`, `position`) VALUES
(25, 4, '', 1),
(26, 4, '', 0),
(27, 5, '', 0),
(28, 5, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_attribute_group`
--

CREATE TABLE `ap_attribute_group` (
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `is_color_group` tinyint(1) NOT NULL DEFAULT '0',
  `group_type` enum('select','radio','color') NOT NULL DEFAULT 'select',
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attribute_group`
--

INSERT INTO `ap_attribute_group` (`id_attribute_group`, `is_color_group`, `group_type`, `position`) VALUES
(4, 0, 'radio', 0),
(5, 0, 'radio', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_attribute_group_lang`
--

CREATE TABLE `ap_attribute_group_lang` (
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `public_name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attribute_group_lang`
--

INSERT INTO `ap_attribute_group_lang` (`id_attribute_group`, `id_lang`, `name`, `public_name`) VALUES
(4, 1, 'Kilométrage ', 'Kilometrage'),
(5, 1, 'Supplément ', 'Supplément ');

-- --------------------------------------------------------

--
-- Table structure for table `ap_attribute_group_shop`
--

CREATE TABLE `ap_attribute_group_shop` (
  `id_attribute_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attribute_group_shop`
--

INSERT INTO `ap_attribute_group_shop` (`id_attribute_group`, `id_shop`) VALUES
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_attribute_impact`
--

CREATE TABLE `ap_attribute_impact` (
  `id_attribute_impact` int(10) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_attribute` int(11) UNSIGNED NOT NULL,
  `weight` decimal(20,6) NOT NULL,
  `price` decimal(17,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attribute_impact`
--

INSERT INTO `ap_attribute_impact` (`id_attribute_impact`, `id_product`, `id_attribute`, `weight`, `price`) VALUES
(1, 22, 25, '0.000000', '1.84'),
(2, 22, 26, '0.000000', '0.00'),
(3, 13, 25, '0.000000', '92.17'),
(4, 13, 26, '0.000000', '0.00'),
(5, 20, 25, '0.000000', '73.73'),
(6, 20, 26, '0.000000', '0.00'),
(7, 15, 25, '0.000000', '55.30'),
(8, 15, 26, '0.000000', '0.00'),
(9, 16, 25, '0.000000', '36.87'),
(10, 16, 26, '0.000000', '0.00'),
(11, 17, 25, '0.000000', '18.43'),
(12, 17, 26, '0.000000', '0.00'),
(13, 18, 25, '0.000000', '9.22'),
(14, 18, 26, '0.000000', '0.00'),
(15, 19, 25, '0.000000', '1.84'),
(16, 19, 26, '0.000000', '0.00'),
(17, 23, 26, '0.000000', '0.00'),
(18, 23, 25, '0.000000', '18.43'),
(19, 24, 26, '0.000000', '0.00'),
(20, 24, 25, '0.000000', '36.87'),
(21, 25, 26, '0.000000', '0.00'),
(22, 25, 25, '0.000000', '92.17');

-- --------------------------------------------------------

--
-- Table structure for table `ap_attribute_lang`
--

CREATE TABLE `ap_attribute_lang` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attribute_lang`
--

INSERT INTO `ap_attribute_lang` (`id_attribute`, `id_lang`, `name`) VALUES
(25, 1, 'Kilométrage illimité '),
(26, 1, 'Kilométrage limité '),
(27, 1, 'Semaine'),
(28, 1, 'Week-end ');

-- --------------------------------------------------------

--
-- Table structure for table `ap_attribute_shop`
--

CREATE TABLE `ap_attribute_shop` (
  `id_attribute` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_attribute_shop`
--

INSERT INTO `ap_attribute_shop` (`id_attribute`, `id_shop`) VALUES
(25, 1),
(26, 1),
(27, 1),
(28, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_badge`
--

CREATE TABLE `ap_badge` (
  `id_badge` int(11) NOT NULL,
  `id_ps_badge` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `id_group` int(11) NOT NULL,
  `group_position` int(11) NOT NULL,
  `scoring` int(11) NOT NULL,
  `awb` int(11) DEFAULT '0',
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_badge`
--

INSERT INTO `ap_badge` (`id_badge`, `id_ps_badge`, `type`, `id_group`, `group_position`, `scoring`, `awb`, `validated`) VALUES
(1, 159, 'feature', 41, 1, 5, 1, 0),
(2, 160, 'feature', 41, 2, 10, 1, 0),
(3, 161, 'feature', 41, 3, 15, 1, 0),
(4, 162, 'feature', 41, 4, 20, 1, 0),
(5, 163, 'feature', 41, 1, 5, 1, 0),
(6, 164, 'feature', 41, 2, 10, 1, 0),
(7, 165, 'feature', 41, 3, 15, 1, 0),
(8, 166, 'feature', 41, 4, 20, 1, 0),
(9, 195, 'feature', 41, 1, 5, 1, 0),
(10, 196, 'feature', 41, 2, 10, 1, 0),
(11, 229, 'feature', 41, 1, 5, 1, 0),
(12, 230, 'feature', 41, 2, 10, 1, 0),
(13, 231, 'feature', 41, 3, 15, 1, 0),
(14, 232, 'feature', 41, 4, 20, 1, 0),
(15, 233, 'feature', 41, 1, 5, 1, 0),
(16, 234, 'feature', 41, 2, 10, 1, 0),
(17, 235, 'feature', 41, 3, 15, 1, 0),
(18, 236, 'feature', 41, 4, 20, 1, 0),
(19, 241, 'feature', 41, 1, 5, 1, 0),
(20, 242, 'feature', 41, 2, 10, 1, 0),
(21, 243, 'feature', 41, 3, 15, 1, 0),
(22, 244, 'feature', 41, 4, 20, 1, 0),
(23, 249, 'feature', 41, 1, 5, 1, 0),
(24, 250, 'feature', 41, 2, 10, 1, 0),
(25, 251, 'feature', 41, 3, 15, 1, 0),
(26, 252, 'feature', 41, 4, 20, 1, 0),
(27, 253, 'feature', 41, 1, 5, 1, 0),
(28, 254, 'feature', 41, 2, 10, 1, 0),
(29, 255, 'feature', 41, 3, 15, 1, 0),
(30, 256, 'feature', 41, 4, 20, 1, 0),
(31, 261, 'feature', 41, 1, 5, 1, 0),
(32, 262, 'feature', 41, 2, 10, 1, 0),
(33, 269, 'feature', 41, 1, 5, 1, 0),
(34, 270, 'feature', 41, 2, 10, 1, 0),
(35, 271, 'feature', 41, 3, 15, 1, 0),
(36, 272, 'feature', 41, 4, 20, 1, 0),
(37, 273, 'feature', 41, 1, 5, 1, 0),
(38, 274, 'feature', 41, 2, 10, 1, 0),
(39, 275, 'feature', 41, 3, 15, 1, 0),
(40, 276, 'feature', 41, 4, 20, 1, 0),
(41, 277, 'feature', 41, 1, 5, 1, 0),
(42, 278, 'feature', 41, 2, 10, 1, 0),
(43, 279, 'feature', 41, 3, 15, 1, 0),
(44, 280, 'feature', 41, 4, 20, 1, 0),
(45, 281, 'feature', 41, 1, 5, 1, 0),
(46, 282, 'feature', 41, 2, 10, 1, 0),
(47, 283, 'feature', 41, 3, 15, 1, 0),
(48, 284, 'feature', 41, 4, 20, 1, 0),
(49, 285, 'feature', 41, 1, 5, 1, 0),
(50, 286, 'feature', 41, 2, 10, 1, 0),
(51, 287, 'feature', 41, 3, 15, 1, 0),
(52, 288, 'feature', 41, 4, 20, 1, 0),
(53, 289, 'feature', 41, 1, 5, 1, 0),
(54, 290, 'feature', 41, 2, 10, 1, 0),
(55, 291, 'feature', 41, 3, 15, 1, 0),
(56, 292, 'feature', 41, 4, 20, 1, 0),
(57, 293, 'feature', 41, 1, 5, 1, 0),
(58, 294, 'feature', 41, 2, 10, 1, 0),
(59, 295, 'feature', 41, 3, 15, 1, 0),
(60, 296, 'feature', 41, 4, 20, 1, 0),
(61, 297, 'feature', 41, 1, 5, 1, 0),
(62, 298, 'feature', 41, 2, 10, 1, 0),
(63, 299, 'feature', 41, 3, 15, 1, 0),
(64, 300, 'feature', 41, 4, 20, 1, 0),
(65, 301, 'feature', 41, 1, 5, 1, 0),
(66, 302, 'feature', 41, 2, 10, 1, 0),
(67, 303, 'feature', 41, 3, 15, 1, 0),
(68, 304, 'feature', 41, 4, 20, 1, 0),
(69, 305, 'feature', 41, 1, 5, 1, 0),
(70, 306, 'feature', 41, 2, 10, 1, 0),
(71, 307, 'feature', 41, 3, 15, 1, 0),
(72, 308, 'feature', 41, 4, 20, 1, 0),
(73, 309, 'feature', 41, 1, 5, 1, 0),
(74, 310, 'feature', 41, 2, 10, 1, 0),
(75, 311, 'feature', 41, 3, 15, 1, 0),
(76, 312, 'feature', 41, 4, 20, 1, 0),
(77, 313, 'feature', 41, 1, 5, 1, 0),
(78, 314, 'feature', 41, 2, 10, 1, 0),
(79, 315, 'feature', 41, 3, 15, 1, 0),
(80, 316, 'feature', 41, 4, 20, 1, 0),
(81, 317, 'feature', 41, 1, 5, 1, 0),
(82, 318, 'feature', 41, 2, 10, 1, 0),
(83, 319, 'feature', 41, 3, 15, 1, 0),
(84, 320, 'feature', 41, 4, 20, 1, 0),
(85, 321, 'feature', 41, 1, 5, 1, 0),
(86, 322, 'feature', 41, 2, 10, 1, 0),
(87, 323, 'feature', 41, 3, 15, 1, 0),
(88, 324, 'feature', 41, 4, 20, 1, 0),
(89, 325, 'feature', 41, 1, 5, 1, 0),
(90, 326, 'feature', 41, 2, 10, 1, 0),
(91, 327, 'feature', 41, 3, 15, 1, 0),
(92, 328, 'feature', 41, 4, 20, 1, 0),
(93, 329, 'feature', 41, 1, 5, 1, 0),
(94, 330, 'feature', 41, 2, 10, 1, 0),
(95, 331, 'feature', 41, 3, 15, 1, 0),
(96, 332, 'feature', 41, 4, 20, 1, 0),
(97, 333, 'feature', 41, 1, 5, 1, 0),
(98, 334, 'feature', 41, 2, 10, 1, 0),
(99, 335, 'feature', 41, 3, 15, 1, 0),
(100, 336, 'feature', 41, 4, 20, 1, 0),
(101, 337, 'feature', 41, 1, 5, 1, 0),
(102, 338, 'feature', 41, 2, 10, 1, 0),
(103, 339, 'feature', 41, 3, 15, 1, 0),
(104, 340, 'feature', 41, 4, 20, 1, 0),
(105, 341, 'feature', 41, 1, 5, 1, 0),
(106, 342, 'feature', 41, 2, 10, 1, 0),
(107, 343, 'feature', 41, 3, 15, 1, 0),
(108, 344, 'feature', 41, 4, 20, 1, 0),
(109, 345, 'feature', 41, 1, 5, 1, 0),
(110, 346, 'feature', 41, 2, 10, 1, 0),
(111, 347, 'feature', 41, 3, 15, 1, 0),
(112, 348, 'feature', 41, 4, 20, 1, 0),
(113, 349, 'feature', 41, 1, 5, 1, 0),
(114, 350, 'feature', 41, 2, 10, 1, 0),
(115, 351, 'feature', 41, 3, 15, 1, 0),
(116, 352, 'feature', 41, 4, 20, 1, 0),
(117, 353, 'feature', 41, 1, 5, 1, 0),
(118, 354, 'feature', 41, 2, 10, 1, 0),
(119, 355, 'feature', 41, 3, 15, 1, 0),
(120, 356, 'feature', 41, 4, 20, 1, 0),
(121, 357, 'feature', 41, 1, 5, 1, 0),
(122, 358, 'feature', 41, 2, 10, 1, 0),
(123, 359, 'feature', 41, 3, 15, 1, 0),
(124, 360, 'feature', 41, 4, 20, 1, 0),
(125, 1, 'feature', 1, 1, 10, 0, 1),
(126, 2, 'feature', 2, 1, 10, 0, 0),
(127, 3, 'feature', 2, 2, 15, 0, 0),
(128, 4, 'feature', 3, 1, 15, 0, 0),
(129, 5, 'feature', 3, 2, 15, 0, 0),
(130, 6, 'feature', 4, 1, 15, 0, 1),
(131, 7, 'feature', 4, 2, 15, 0, 0),
(132, 8, 'feature', 5, 1, 5, 0, 1),
(133, 9, 'feature', 5, 2, 10, 0, 1),
(134, 10, 'feature', 6, 1, 15, 0, 1),
(135, 11, 'feature', 6, 2, 10, 0, 0),
(136, 12, 'feature', 6, 3, 10, 0, 0),
(137, 13, 'feature', 5, 3, 10, 0, 0),
(138, 14, 'feature', 5, 4, 15, 0, 0),
(139, 15, 'feature', 5, 5, 20, 0, 0),
(140, 16, 'feature', 5, 6, 20, 0, 0),
(141, 17, 'achievement', 7, 1, 5, 0, 1),
(142, 18, 'achievement', 7, 2, 10, 0, 1),
(143, 19, 'feature', 8, 1, 15, 0, 1),
(144, 20, 'feature', 8, 2, 15, 0, 0),
(145, 21, 'feature', 9, 1, 15, 0, 0),
(146, 22, 'feature', 10, 1, 10, 0, 0),
(147, 23, 'feature', 10, 2, 10, 0, 0),
(148, 24, 'feature', 10, 3, 10, 0, 0),
(149, 25, 'feature', 10, 4, 10, 0, 0),
(150, 26, 'feature', 10, 5, 10, 0, 0),
(151, 27, 'feature', 4, 3, 10, 0, 0),
(152, 28, 'feature', 3, 3, 10, 0, 0),
(153, 29, 'achievement', 11, 1, 5, 0, 1),
(154, 30, 'achievement', 11, 2, 10, 0, 0),
(155, 31, 'achievement', 11, 3, 15, 0, 0),
(156, 32, 'achievement', 11, 4, 20, 0, 0),
(157, 33, 'achievement', 11, 5, 25, 0, 0),
(158, 34, 'achievement', 11, 6, 30, 0, 0),
(159, 35, 'achievement', 7, 3, 15, 0, 0),
(160, 36, 'achievement', 7, 4, 20, 0, 0),
(161, 37, 'achievement', 7, 5, 25, 0, 0),
(162, 38, 'achievement', 7, 6, 30, 0, 0),
(163, 39, 'achievement', 12, 1, 5, 0, 1),
(164, 40, 'achievement', 12, 2, 10, 0, 0),
(165, 41, 'achievement', 12, 3, 15, 0, 0),
(166, 42, 'achievement', 12, 4, 20, 0, 0),
(167, 43, 'achievement', 12, 5, 25, 0, 0),
(168, 44, 'achievement', 12, 6, 30, 0, 0),
(169, 45, 'achievement', 13, 1, 5, 0, 1),
(170, 46, 'achievement', 13, 2, 10, 0, 1),
(171, 47, 'achievement', 13, 3, 15, 0, 0),
(172, 48, 'achievement', 13, 4, 20, 0, 0),
(173, 49, 'achievement', 13, 5, 25, 0, 0),
(174, 50, 'achievement', 13, 6, 30, 0, 0),
(175, 51, 'achievement', 14, 1, 5, 0, 0),
(176, 52, 'achievement', 14, 2, 10, 0, 0),
(177, 53, 'achievement', 14, 3, 15, 0, 0),
(178, 54, 'achievement', 14, 4, 20, 0, 0),
(179, 55, 'achievement', 14, 5, 25, 0, 0),
(180, 56, 'achievement', 14, 6, 30, 0, 0),
(181, 57, 'achievement', 15, 1, 5, 0, 0),
(182, 58, 'achievement', 15, 2, 10, 0, 0),
(183, 59, 'achievement', 15, 3, 15, 0, 0),
(184, 60, 'achievement', 15, 4, 20, 0, 0),
(185, 61, 'achievement', 15, 5, 25, 0, 0),
(186, 62, 'achievement', 15, 6, 30, 0, 0),
(187, 63, 'achievement', 16, 1, 5, 0, 1),
(188, 64, 'achievement', 16, 2, 10, 0, 0),
(189, 65, 'achievement', 16, 3, 15, 0, 0),
(190, 66, 'achievement', 16, 4, 20, 0, 0),
(191, 67, 'achievement', 16, 5, 25, 0, 0),
(192, 68, 'achievement', 16, 6, 30, 0, 0),
(193, 74, 'international', 22, 1, 10, 0, 0),
(194, 75, 'international', 23, 1, 10, 0, 0),
(195, 83, 'international', 31, 1, 10, 0, 0),
(196, 84, 'international', 25, 1, 10, 0, 0),
(197, 85, 'international', 32, 1, 10, 0, 0),
(198, 86, 'international', 33, 1, 10, 0, 0),
(199, 87, 'international', 34, 1, 10, 0, 0),
(200, 88, 'feature', 35, 1, 5, 0, 1),
(201, 89, 'feature', 35, 2, 10, 0, 0),
(202, 90, 'feature', 35, 3, 10, 0, 0),
(203, 91, 'feature', 35, 4, 10, 0, 0),
(204, 92, 'feature', 35, 5, 10, 0, 0),
(205, 93, 'feature', 35, 6, 10, 0, 0),
(206, 94, 'feature', 36, 1, 5, 0, 1),
(207, 95, 'feature', 36, 2, 5, 0, 0),
(208, 96, 'feature', 36, 3, 10, 0, 0),
(209, 97, 'feature', 36, 4, 10, 0, 0),
(210, 98, 'feature', 36, 5, 20, 0, 0),
(211, 99, 'feature', 36, 6, 20, 0, 0),
(212, 100, 'feature', 8, 3, 15, 0, 1),
(213, 101, 'achievement', 37, 1, 5, 0, 0),
(214, 102, 'achievement', 37, 2, 5, 0, 0),
(215, 103, 'achievement', 37, 3, 10, 0, 0),
(216, 104, 'achievement', 37, 4, 10, 0, 0),
(217, 105, 'achievement', 37, 5, 15, 0, 0),
(218, 106, 'achievement', 37, 6, 15, 0, 0),
(219, 107, 'achievement', 38, 1, 10, 0, 0),
(220, 108, 'achievement', 38, 2, 10, 0, 0),
(221, 109, 'achievement', 38, 3, 15, 0, 0),
(222, 110, 'achievement', 38, 4, 20, 0, 0),
(223, 111, 'achievement', 38, 5, 25, 0, 0),
(224, 112, 'achievement', 38, 6, 30, 0, 0),
(225, 113, 'achievement', 39, 1, 10, 0, 1),
(226, 114, 'achievement', 39, 2, 20, 0, 0),
(227, 115, 'achievement', 39, 3, 30, 0, 0),
(228, 116, 'achievement', 39, 4, 40, 0, 0),
(229, 117, 'achievement', 39, 5, 50, 0, 0),
(230, 118, 'achievement', 39, 6, 50, 0, 0),
(231, 119, 'feature', 40, 1, 10, 0, 0),
(232, 120, 'feature', 40, 2, 15, 0, 0),
(233, 121, 'feature', 40, 3, 20, 0, 0),
(234, 122, 'feature', 40, 4, 25, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_badge_lang`
--

CREATE TABLE `ap_badge_lang` (
  `id_badge` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_badge_lang`
--

INSERT INTO `ap_badge_lang` (`id_badge`, `id_lang`, `name`, `description`, `group_name`) VALUES
(1, 1, 'Shopgate installed', 'You have installed the Shopgate module', 'Partners'),
(2, 1, 'Shopgate configured', 'You have configured the Shopgate module', 'Partners'),
(3, 1, 'Shopgate active', 'Your Shopgate module is active', 'Partners'),
(4, 1, 'Shopgate very active', 'Your Shopgate module is very active', 'Partners'),
(5, 1, 'Skrill installed', 'You have installed the Skrill module', 'Partners'),
(6, 1, 'Skrill configured', 'You have configured the Skrill module', 'Partners'),
(7, 1, 'Skrill active', 'Your Skrill module is active', 'Partners'),
(8, 1, 'Skrill very active', 'Your Skrill module is very active', 'Partners'),
(9, 1, 'Shopping Feed installed', 'You have installed the Shopping Feed module', 'Partners'),
(10, 1, 'Shopping Feed configured', 'You have configured the Shopping Feed module', 'Partners'),
(11, 1, 'Alliance Payment installed', 'You have installed the Alliance Payment module', 'Partners'),
(12, 1, 'Alliance Payment configured', 'You have configured the Alliance Payment module', 'Partners'),
(13, 1, 'Alliance Payment active', 'Your Alliance Payment module is active', 'Partners'),
(14, 1, 'Alliance Payment very active', 'Your Alliance Payment module is very active', 'Partners'),
(15, 1, 'Authorize Aim installed', 'You have installed the Authorize Aim module', 'Partners'),
(16, 1, 'Authorize Aim configured', 'You have configured the Authorize Aim module', 'Partners'),
(17, 1, 'Authorize Aim active', 'Your Authorize Aim module is active', 'Partners'),
(18, 1, 'Authorize Aim very active', 'Your Authorize Aim module is very active', 'Partners'),
(19, 1, 'Blue Pay installed', 'You have installed the Blue Pay module', 'Partners'),
(20, 1, 'Blue Pay configured', 'You have configured the Blue Pay module', 'Partners'),
(21, 1, 'Blue Pay active', 'Your Blue Pay module is active', 'Partners'),
(22, 1, 'Blue Pay very active', 'Your Blue Pay module is very active', 'Partners'),
(23, 1, 'Ebay installed', 'You have installed the Ebay module', 'Partners'),
(24, 1, 'Ebay configured', 'You have configured the Ebay module', 'Partners'),
(25, 1, 'Ebay active', 'Your Ebay module is active', 'Partners'),
(26, 1, 'Ebay very active', 'Your Ebay module is very active', 'Partners'),
(27, 1, 'PayPlug installed', 'You have installed the PayPlug module', 'Partners'),
(28, 1, 'PayPlug configured', 'You have configured the PayPlug module', 'Partners'),
(29, 1, 'PayPlug active', 'Your PayPlug module is active', 'Partners'),
(30, 1, 'PayPlug very active', 'Your PayPlug module is very active', 'Partners'),
(31, 1, 'Affinity Items installed', 'You have installed the Affinity Items module', 'Partners'),
(32, 1, 'Affinity Items configured', 'You have configured the Affinity Items module', 'Partners'),
(33, 1, 'DPD Poland installed', 'You have installed the DPD Poland module', 'Partners'),
(34, 1, 'DPD Poland configured', 'You have configured the DPD Poland module', 'Partners'),
(35, 1, 'DPD Poland active', 'Your DPD Poland module is active', 'Partners'),
(36, 1, 'DPD Poland very active', 'Your DPD Poland module is very active', 'Partners'),
(37, 1, 'Envoimoinscher installed', 'You have installed the Envoimoinscher module', 'Partners'),
(38, 1, 'Envoimoinscher configured', 'You have configured the Envoimoinscher module', 'Partners'),
(39, 1, 'Envoimoinscher active', 'Your Envoimoinscher module is active', 'Partners'),
(40, 1, 'Envoimoinscher very active', 'Your Envoimoinscher module is very active', 'Partners'),
(41, 1, 'Klik&Pay installed', 'You have installed the Klik&Pay module', 'Partners'),
(42, 1, 'Klik&Pay configured', 'You have configured the Klik&Pay module', 'Partners'),
(43, 1, 'Klik&Pay active', 'Your Klik&Pay module is active', 'Partners'),
(44, 1, 'Klik&Pay very active', 'Your Klik&Pay module is very active', 'Partners'),
(45, 1, 'Clickline installed', 'You have installed the Clickline module', 'Partners'),
(46, 1, 'Clickline configured', 'You have configured the Clickline module', 'Partners'),
(47, 1, 'Clickline active', 'Your Clickline module is active', 'Partners'),
(48, 1, 'Clickline very active', 'Your Clickline module is very active', 'Partners'),
(49, 1, 'CDiscount installed', 'You have installed the CDiscount module', 'Partners'),
(50, 1, 'CDiscount configured', 'You have configured the CDiscount module', 'Partners'),
(51, 1, 'CDiscount active', 'Your CDiscount module is active', 'Partners'),
(52, 1, 'CDiscount very active', 'Your CDiscount module is very active', 'Partners'),
(53, 1, 'illicoPresta installed', 'You have installed the illicoPresta module', 'Partners'),
(54, 1, 'illicoPresta configured', 'You have configured the illicoPresta module', 'Partners'),
(55, 1, 'illicoPresta active', 'Your illicoPresta module is active', 'Partners'),
(56, 1, 'illicoPresta very active', 'Your illicoPresta module is very active', 'Partners'),
(57, 1, 'NetReviews installed', 'You have installed the NetReviews module', 'Partners'),
(58, 1, 'NetReviews configured', 'You have configured the NetReviews module', 'Partners'),
(59, 1, 'NetReviews active', 'Your NetReviews module is active', 'Partners'),
(60, 1, 'NetReviews very active', 'Your NetReviews module is very active', 'Partners'),
(61, 1, 'Bluesnap installed', 'You have installed the Bluesnap module', 'Partners'),
(62, 1, 'Bluesnap configured', 'You have configured the Bluesnap module', 'Partners'),
(63, 1, 'Bluesnap active', 'Your Bluesnap module is active', 'Partners'),
(64, 1, 'Bluesnap very active', 'Your Bluesnap module is very active', 'Partners'),
(65, 1, 'Desjardins installed', 'You have installed the Desjardins module', 'Partners'),
(66, 1, 'Desjardins configured', 'You have configured the Desjardins module', 'Partners'),
(67, 1, 'Desjardins active', 'Your Desjardins module is active', 'Partners'),
(68, 1, 'Desjardins very active', 'Your Desjardins module is very active', 'Partners'),
(69, 1, 'First Data installed', 'You have installed the First Data module', 'Partners'),
(70, 1, 'First Data configured', 'You have configured the First Data module', 'Partners'),
(71, 1, 'First Data active', 'Your First Data module is active', 'Partners'),
(72, 1, 'First Data very active', 'Your First Data module is very active', 'Partners'),
(73, 1, 'Give.it installed', 'You have installed the Give.it module', 'Partners'),
(74, 1, 'Give.it configured', 'You have configured the Give.it module', 'Partners'),
(75, 1, 'Give.it active', 'Your Give.it module is active', 'Partners'),
(76, 1, 'Give.it very active', 'Your Give.it module is very active', 'Partners'),
(77, 1, 'Google Analytics installed', 'You have installed the Google Analytics module', 'Partners'),
(78, 1, 'Google Analytics configured', 'You have configured the Google Analytics module', 'Partners'),
(79, 1, 'Google Analytics active', 'Your Google Analytics module is active', 'Partners'),
(80, 1, 'Google Analytics very active', 'Your Google Analytics module is very active', 'Partners'),
(81, 1, 'PagSeguro installed', 'You have installed the PagSeguro module', 'Partners'),
(82, 1, 'PagSeguro configured', 'You have configured the PagSeguro module', 'Partners'),
(83, 1, 'PagSeguro active', 'Your PagSeguro module is active', 'Partners'),
(84, 1, 'PagSeguro very active', 'Your PagSeguro module is very active', 'Partners'),
(85, 1, 'Paypal MX installed', 'You have installed the Paypal MX module', 'Partners'),
(86, 1, 'Paypal MX configured', 'You have configured the Paypal MX module', 'Partners'),
(87, 1, 'Paypal MX active', 'Your Paypal MX module is active', 'Partners'),
(88, 1, 'Paypal MX very active', 'Your Paypal MX module is very active', 'Partners'),
(89, 1, 'Paypal USA installed', 'You have installed the Paypal USA module', 'Partners'),
(90, 1, 'Paypal USA configured', 'You have configured the Paypal USA module', 'Partners'),
(91, 1, 'Paypal USA active', 'Your Paypal USA module is active', 'Partners'),
(92, 1, 'Paypal USA very active', 'Your Paypal USA module is very active', 'Partners'),
(93, 1, 'PayULatam installed', 'You have installed the PayULatam module', 'Partners'),
(94, 1, 'PayULatam configured', 'You have configured the PayULatam module', 'Partners'),
(95, 1, 'PayULatam active', 'Your PayULatam module is active', 'Partners'),
(96, 1, 'PayULatam very active', 'Your PayULatam module is very active', 'Partners'),
(97, 1, 'PrestaStats installed', 'You have installed the PrestaStats module', 'Partners'),
(98, 1, 'PrestaStats configured', 'You have configured the PrestaStats module', 'Partners'),
(99, 1, 'PrestaStats active', 'Your PrestaStats module is active', 'Partners'),
(100, 1, 'PrestaStats very active', 'Your PrestaStats module is very active', 'Partners'),
(101, 1, 'Riskified installed', 'You have installed the Riskified module', 'Partners'),
(102, 1, 'Riskified configured', 'You have configured the Riskified module', 'Partners'),
(103, 1, 'Riskified active', 'Your Riskified module is active', 'Partners'),
(104, 1, 'Riskified very active', 'Your Riskified module is very active', 'Partners'),
(105, 1, 'Simplify installed', 'You have installed the Simplify module', 'Partners'),
(106, 1, 'Simplify configured', 'You have configured the Simplify module', 'Partners'),
(107, 1, 'Simplify active', 'Your Simplify module is active', 'Partners'),
(108, 1, 'Simplify very active', 'Your Simplify module is very active', 'Partners'),
(109, 1, 'VTPayment installed', 'You have installed the VTPayment module', 'Partners'),
(110, 1, 'VTPayment configured', 'You have configured the VTPayment module', 'Partners'),
(111, 1, 'VTPayment active', 'Your VTPayment module is active', 'Partners'),
(112, 1, 'VTPayment very active', 'Your VTPayment module is very active', 'Partners'),
(113, 1, 'Yotpo installed', 'You have installed the Yotpo module', 'Partners'),
(114, 1, 'Yotpo configured', 'You have configured the Yotpo module', 'Partners'),
(115, 1, 'Yotpo active', 'Your Yotpo module is active', 'Partners'),
(116, 1, 'Yotpo very active', 'Your Yotpo module is very active', 'Partners'),
(117, 1, 'Youstice installed', 'You have installed the Youstice module', 'Partners'),
(118, 1, 'Youstice configured', 'You have configured the Youstice module', 'Partners'),
(119, 1, 'Youstice active', 'Your Youstice module is active', 'Partners'),
(120, 1, 'Youstice very active', 'Your Youstice module is very active', 'Partners'),
(121, 1, 'Loyalty Lion installed', 'You have installed the Loyalty Lion module', 'Partners'),
(122, 1, 'Loyalty Lion configured', 'You have configured the Loyalty Lion module', 'Partners'),
(123, 1, 'Loyalty Lion active', 'Your Loyalty Lion module is active', 'Partners'),
(124, 1, 'Loyalty Lion very active', 'Your Loyalty Lion module is very active', 'Partners'),
(125, 1, 'SEO', 'You enabled the URL rewriting through the tab \"Preferences > SEO and URLs\".', 'SEO'),
(126, 1, 'Site Performance', 'You enabled CCC (Combine, Compress and Cache), Rijndael and Smarty through the tab \r\nAdvanced Parameters > Performance.', 'Site Performance'),
(127, 1, 'Site Performance', 'You enabled media servers through the tab \"Advanced parameters > Performance\".', 'Site Performance'),
(128, 1, 'Payment', 'You configured a payment solution on your shop.', 'Payment'),
(129, 1, 'Payment', 'You offer two different payment methods to your customers.', 'Payment'),
(130, 1, 'Shipping', 'You configured a carrier on your shop.', 'Shipping'),
(131, 1, 'Shipping', 'You offer two shipping solutions (carriers) to your customers.', 'Shipping'),
(132, 1, 'Catalog Size', 'You added your first product to your catalog!', 'Catalog Size'),
(133, 1, 'Catalog Size', 'You have 10 products within your catalog.', 'Catalog Size'),
(134, 1, 'Contact information', 'You configured your phone number so your customers can reach you!', 'Contact information'),
(135, 1, 'Contact information', 'You added a third email address to your contact form.', 'Contact information'),
(136, 1, 'Contact information', 'You suggest a total of 5 departments to be reached by your customers via your contact form.', 'Contact information'),
(137, 1, 'Catalog Size', 'You have 100 products within your catalog.', 'Catalog Size'),
(138, 1, 'Catalog Size', 'You have 1,000 products within your catalog.', 'Catalog Size'),
(139, 1, 'Catalog Size', 'You have 10,000 products within your catalog.', 'Catalog Size'),
(140, 1, 'Catalog Size', 'You have 100,000 products within your catalog.', 'Catalog Size'),
(141, 1, 'Days of Experience', 'You just installed PrestaShop!', 'Days of Experience'),
(142, 1, 'Days of Experience', 'You installed PrestaShop a week ago!', 'Days of Experience'),
(143, 1, 'Customization', 'You uploaded your own logo.', 'Customization'),
(144, 1, 'Customization', 'You installed a new template.', 'Customization'),
(145, 1, 'Addons', 'You connected your back-office to the Addons platform using your PrestaShop Addons account.', 'Addons'),
(146, 1, 'Multistores', 'You enabled the Multistores feature.', 'Multistores'),
(147, 1, 'Multistores', 'You manage two shops with the Multistores feature.', 'Multistores'),
(148, 1, 'Multistores', 'You manage two different groups of shops using the Multistores feature.', 'Multistores'),
(149, 1, 'Multistores', 'You manage five shops with the Multistores feature.', 'Multistores'),
(150, 1, 'Multistores', 'You manage five different groups of shops using the Multistores feature.', 'Multistores'),
(151, 1, 'Shipping', 'You offer three different shipping solutions (carriers) to your customers.', 'Shipping'),
(152, 1, 'Payment', 'You offer three different payment methods to your customers.', 'Payment'),
(153, 1, 'Revenue', 'You get this badge when you reach 200 USD in sales.', 'Revenue'),
(154, 1, 'Revenue', 'You get this badge when you reach 1000 USD in sales.', 'Revenue'),
(155, 1, 'Revenue', 'You get this badge when you reach 1000 USD in sales.', 'Revenue'),
(156, 1, 'Revenue', 'You get this badge when you reach 200 USD in sales.', 'Revenue'),
(157, 1, 'Revenue', 'You get this badge when you reach 1000 USD in sales.', 'Revenue'),
(158, 1, 'Revenue', 'You get this badge when you reach 1000 USD in sales.', 'Revenue'),
(159, 1, 'Days of Experience', 'You installed PrestaShop a month ago!', 'Days of Experience'),
(160, 1, 'Days of Experience', 'You installed PrestaShop six months ago!', 'Days of Experience'),
(161, 1, 'Days of Experience', 'You installed PrestaShop a year ago!', 'Days of Experience'),
(162, 1, 'Days of Experience', 'You installed PrestaShop two years ago!', 'Days of Experience'),
(163, 1, 'Visitors', 'You reached 10 visitors!', 'Visitors'),
(164, 1, 'Visitors', 'You reached 100 visitors!', 'Visitors'),
(165, 1, 'Visitors', 'You reached 1,000 visitors!', 'Visitors'),
(166, 1, 'Visitors', 'You reached 10,000 visitors!', 'Visitors'),
(167, 1, 'Visitors', 'You reached 100,000 visitors!', 'Visitors'),
(168, 1, 'Visitors', 'You reached 1,000,000 visitors!', 'Visitors'),
(169, 1, 'Customer Carts', 'Two carts have been created by visitors', 'Customer Carts'),
(170, 1, 'Customer Carts', 'Ten carts have been created by visitors.', 'Customer Carts'),
(171, 1, 'Customer Carts', 'A hundred carts have been created by visitors on your shop.', 'Customer Carts'),
(172, 1, 'Customer Carts', 'A thousand carts have been created by visitors on your shop.', 'Customer Carts'),
(173, 1, 'Customer Carts', '10,000 carts have been created by visitors.', 'Customer Carts'),
(174, 1, 'Customer Carts', '100,000 carts have been created by visitors.', 'Customer Carts'),
(175, 1, 'Orders', 'You received your first order.', 'Orders'),
(176, 1, 'Orders', '10 orders have been placed through your online shop.', 'Orders'),
(177, 1, 'Orders', 'You received 100 orders through your online shop!', 'Orders'),
(178, 1, 'Orders', 'You received 1,000 orders through your online shop, congrats!', 'Orders'),
(179, 1, 'Orders', 'You received 10,000 orders through your online shop, cheers!', 'Orders'),
(180, 1, 'Orders', 'You received 100,000 orders through your online shop!', 'Orders'),
(181, 1, 'Customer Service Threads', 'You received  your first customer\'s message.', 'Customer Service Threads'),
(182, 1, 'Customer Service Threads', 'You received 10 messages from your customers.', 'Customer Service Threads'),
(183, 1, 'Customer Service Threads', 'You received 100 messages from your customers.', 'Customer Service Threads'),
(184, 1, 'Customer Service Threads', 'You received 1,000 messages from your customers.', 'Customer Service Threads'),
(185, 1, 'Customer Service Threads', 'You received 10,000 messages from your customers.', 'Customer Service Threads'),
(186, 1, 'Customer Service Threads', 'You received 100,000 messages from your customers.', 'Customer Service Threads'),
(187, 1, 'Customers', 'You got the first customer registered on your shop!', 'Customers'),
(188, 1, 'Customers', 'You have over 10 customers registered on your shop.', 'Customers'),
(189, 1, 'Customers', 'You have over 100 customers registered on your shop.', 'Customers'),
(190, 1, 'Customers', 'You have over 1,000 customers registered on your shop.', 'Customers'),
(191, 1, 'Customers', 'You have over 10,000 customers registered on your shop.', 'Customers'),
(192, 1, 'Customers', 'You have over 100,000 customers registered on your shop.', 'Customers'),
(193, 1, 'North America', 'You got your first sale in North America', 'North America'),
(194, 1, 'Oceania', 'You got your first sale in Oceania', 'Oceania'),
(195, 1, 'Asia', 'You got your first sale in Asia', 'Asia'),
(196, 1, 'South America', 'You got your first sale in South America', 'South America'),
(197, 1, 'Europe', 'You got your first sale in  Europe!', 'Europe'),
(198, 1, 'Africa', 'You got your first sale in Africa', 'Africa'),
(199, 1, 'Maghreb', 'You got your first sale in Maghreb', 'Maghreb'),
(200, 1, 'Your Team\'s Employees', 'First employee account added to your shop', 'Your Team\'s Employees'),
(201, 1, 'Your Team\'s Employees', '3 employee accounts added to your shop', 'Your Team\'s Employees'),
(202, 1, 'Your Team\'s Employees', '5 employee accounts added to your shop', 'Your Team\'s Employees'),
(203, 1, 'Your Team\'s Employees', '10 employee accounts added to your shop', 'Your Team\'s Employees'),
(204, 1, 'Your Team\'s Employees', '20 employee accounts added to your shop', 'Your Team\'s Employees'),
(205, 1, 'Your Team\'s Employees', '40 employee accounts added to your shop', 'Your Team\'s Employees'),
(206, 1, 'Product Pictures', 'First photo added to your catalog', 'Product Pictures'),
(207, 1, 'Product Pictures', '50 photos added to your catalog', 'Product Pictures'),
(208, 1, 'Product Pictures', '100 photos added to your catalog', 'Product Pictures'),
(209, 1, 'Product Pictures', '1,000 photos added to your catalog', 'Product Pictures'),
(210, 1, 'Product Pictures', '10,000 photos added to your catalog', 'Product Pictures'),
(211, 1, 'Product Pictures', '50,000 photos added to your catalog', 'Product Pictures'),
(212, 1, 'Customization', 'First CMS page added to your catalog', 'Customization'),
(213, 1, 'Cart Rules', 'First cart rules configured on your shop', 'Cart Rules'),
(214, 1, 'Cart Rules', 'You have 10 cart rules configured on your shop', 'Cart Rules'),
(215, 1, 'Cart Rules', 'You have 100 cart rules configured on your shop', 'Cart Rules'),
(216, 1, 'Cart Rules', 'You have 500 cart rules configured on your shop', 'Cart Rules'),
(217, 1, 'Cart Rules', 'You have 1,000 cart rules configured on your shop', 'Cart Rules'),
(218, 1, 'Cart Rules', 'You have 5,000 cart rules configured on your shop', 'Cart Rules'),
(219, 1, 'International Orders', 'First international order placed on your shop.', 'International Orders'),
(220, 1, 'International Orders', '10 international orders placed on your shop.', 'International Orders'),
(221, 1, 'International Orders', '100 international orders placed on your shop!', 'International Orders'),
(222, 1, 'International Orders', '1,000 international orders placed on your shop!', 'International Orders'),
(223, 1, 'International Orders', '5,000 international orders placed on your shop!', 'International Orders'),
(224, 1, 'International Orders', '10,000 international orders placed on your shop!', 'International Orders'),
(225, 1, 'Store', 'First store configured on your shop!', 'Store'),
(226, 1, 'Store', 'You have 2 stores configured on your shop', 'Store'),
(227, 1, 'Store', 'You have 5 stores configured on your shop', 'Store'),
(228, 1, 'Store', 'You have 10 stores configured on your shop', 'Store'),
(229, 1, 'Store', 'You have 20 stores configured on your shop', 'Store'),
(230, 1, 'Store', 'You have 50 stores configured on your shop', 'Store'),
(231, 1, 'Webservice x1', 'First webservice account added to your shop', 'WebService'),
(232, 1, 'Webservice x2', '2 webservice accounts added to your shop', 'WebService'),
(233, 1, 'Webservice x3', '3 webservice accounts added to your shop', 'WebService'),
(234, 1, 'Webservice x4', '4 webservice accounts added to your shop', 'WebService');

-- --------------------------------------------------------

--
-- Table structure for table `ap_bonslick`
--

CREATE TABLE `ap_bonslick` (
  `id_tab` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_bonslick`
--

INSERT INTO `ap_bonslick` (`id_tab`, `id_shop`, `status`, `sort_order`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ap_bonslick_lang`
--

CREATE TABLE `ap_bonslick_lang` (
  `id_tab` int(10) UNSIGNED NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_bonslick_lang`
--

INSERT INTO `ap_bonslick_lang` (`id_tab`, `id_lang`, `title`, `url`, `image`, `description`) VALUES
(1, 1, 'LOCATION DE VOITURE A DOUBLE COMMANDE', 'index.php', '13140aa9567a7bde71a654d129287b8cb78c713e_locations-voitures-double-commandes.jpg', ''),
(2, 1, '.', 'www.google.fr', '040edddf5ad8796b43c4e016a7a647b01a99f337_route 112.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_carrier`
--

CREATE TABLE `ap_carrier` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_reference` int(10) UNSIGNED NOT NULL,
  `id_tax_rules_group` int(10) UNSIGNED DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_handling` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `range_behavior` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_module` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_free` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_external` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `need_range` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `external_module_name` varchar(64) DEFAULT NULL,
  `shipping_method` int(2) NOT NULL DEFAULT '0',
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `max_width` int(10) DEFAULT '0',
  `max_height` int(10) DEFAULT '0',
  `max_depth` int(10) DEFAULT '0',
  `max_weight` decimal(20,6) DEFAULT '0.000000',
  `grade` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_carrier`
--

INSERT INTO `ap_carrier` (`id_carrier`, `id_reference`, `id_tax_rules_group`, `name`, `url`, `active`, `deleted`, `shipping_handling`, `range_behavior`, `is_module`, `is_free`, `shipping_external`, `need_range`, `external_module_name`, `shipping_method`, `position`, `max_width`, `max_height`, `max_depth`, `max_weight`, `grade`) VALUES
(1, 1, 0, '0', '', 1, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, 0, 0, 0, '0.000000', 0),
(2, 2, 0, 'My carrier', '', 1, 0, 1, 0, 0, 0, 0, 0, '', 0, 1, 0, 0, 0, '0.000000', 0),
(3, 3, 0, 'Produit dématerialisé', '', 1, 0, 0, 0, 0, 1, 0, 0, '', 2, 2, 0, 0, 0, '0.000000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_carrier_group`
--

CREATE TABLE `ap_carrier_group` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_carrier_group`
--

INSERT INTO `ap_carrier_group` (`id_carrier`, `id_group`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ap_carrier_lang`
--

CREATE TABLE `ap_carrier_lang` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `delay` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_carrier_lang`
--

INSERT INTO `ap_carrier_lang` (`id_carrier`, `id_shop`, `id_lang`, `delay`) VALUES
(1, 1, 1, 'Retrait en magasin'),
(2, 1, 1, 'Livraison le lendemain !'),
(3, 1, 1, 'Instantanément crédité sur le compte');

-- --------------------------------------------------------

--
-- Table structure for table `ap_carrier_shop`
--

CREATE TABLE `ap_carrier_shop` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_carrier_shop`
--

INSERT INTO `ap_carrier_shop` (`id_carrier`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_carrier_tax_rules_group_shop`
--

CREATE TABLE `ap_carrier_tax_rules_group_shop` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_carrier_tax_rules_group_shop`
--

INSERT INTO `ap_carrier_tax_rules_group_shop` (`id_carrier`, `id_tax_rules_group`, `id_shop`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_carrier_zone`
--

CREATE TABLE `ap_carrier_zone` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_carrier_zone`
--

INSERT INTO `ap_carrier_zone` (`id_carrier`, `id_zone`) VALUES
(1, 1),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart`
--

CREATE TABLE `ap_cart` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delivery_option` text NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message` text,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  `allow_seperated_package` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cart`
--

INSERT INTO `ap_cart` (`id_cart`, `id_shop_group`, `id_shop`, `id_carrier`, `delivery_option`, `id_lang`, `id_address_delivery`, `id_address_invoice`, `id_currency`, `id_customer`, `id_guest`, `secure_key`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `allow_seperated_package`, `date_add`, `date_upd`) VALUES
(1, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2019-06-08 02:34:25', '2019-06-08 02:34:25'),
(2, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2019-06-08 02:34:25', '2019-06-08 02:34:25'),
(3, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2019-06-08 02:34:25', '2019-06-08 02:34:25'),
(4, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2019-06-08 02:34:25', '2019-06-08 02:34:25'),
(5, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2019-06-08 02:34:25', '2019-06-08 02:34:25'),
(6, 1, 1, 0, '', 1, 0, 0, 1, 2, 4, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-08 04:24:59', '2019-06-08 04:50:03'),
(7, 1, 1, 0, '', 1, 0, 0, 1, 2, 5, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-08 11:33:34', '2019-06-08 11:35:55'),
(8, 1, 1, 0, '', 1, 0, 0, 1, 2, 6, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-08 13:23:35', '2019-06-10 05:10:01'),
(9, 1, 1, 0, '', 1, 0, 0, 1, 2, 7, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-09 23:20:39', '2019-06-10 14:30:04'),
(10, 1, 1, 0, '', 1, 0, 0, 1, 2, 13, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-10 15:07:21', '2019-06-10 18:40:08'),
(11, 1, 1, 0, '', 1, 0, 0, 1, 2, 11, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-10 15:29:44', '2019-06-10 16:44:33'),
(12, 1, 1, 0, '', 1, 0, 0, 1, 4, 18, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-10 19:17:33', '2019-06-10 19:18:57'),
(13, 1, 1, 0, '', 1, 0, 0, 1, 4, 15, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-10 19:18:16', '2019-06-10 19:19:29'),
(14, 1, 1, 0, '', 1, 0, 0, 1, 4, 19, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-10 19:20:14', '2019-06-10 21:40:31'),
(15, 1, 1, 0, '', 1, 0, 0, 1, 4, 14, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-10 21:29:39', '2019-06-10 23:30:24'),
(16, 1, 1, 0, '', 1, 0, 0, 1, 2, 23, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-11 01:31:58', '2019-06-11 01:42:01'),
(17, 1, 1, 0, '', 1, 0, 0, 1, 2, 32, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-11 01:42:28', '2019-06-11 13:36:29'),
(18, 1, 1, 0, '', 1, 0, 0, 1, 4, 37, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-11 13:44:11', '2019-06-11 15:19:30'),
(19, 1, 1, 0, '', 1, 5, 5, 1, 2, 27, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 1, 0, '2019-06-11 22:01:39', '2019-06-19 13:51:53'),
(20, 1, 1, 0, '', 1, 0, 0, 1, 4, 38, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-12 01:13:25', '2019-06-12 01:19:58'),
(21, 1, 1, 0, '', 1, 5, 5, 1, 2, 41, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-12 01:20:49', '2019-06-12 23:55:03'),
(22, 1, 1, 0, '', 1, 0, 0, 1, 0, 44, '', 0, 0, '', 1, 0, '2019-06-12 01:33:54', '2019-06-12 01:38:22'),
(23, 1, 1, 0, '', 1, 0, 0, 1, 4, 47, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-12 16:00:29', '2019-06-12 16:03:02'),
(24, 1, 1, 0, '', 1, 5, 5, 1, 2, 41, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-12 23:55:50', '2019-06-12 23:56:04'),
(25, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:02:07', '2019-06-13 00:02:21'),
(26, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:06:06', '2019-06-13 00:06:21'),
(27, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:37:31', '2019-06-13 00:37:47'),
(28, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:38:54', '2019-06-13 00:39:05'),
(29, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:40:01', '2019-06-13 00:40:13'),
(30, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:41:40', '2019-06-13 00:41:56'),
(31, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:43:34', '2019-06-13 00:43:47'),
(32, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:45:13', '2019-06-13 00:45:25'),
(33, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:47:19', '2019-06-13 00:47:35'),
(34, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:49:15', '2019-06-13 00:49:28'),
(35, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:51:26', '2019-06-13 00:51:37'),
(36, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:55:33', '2019-06-13 00:55:51'),
(37, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 00:56:58', '2019-06-13 00:57:23'),
(38, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:01:49', '2019-06-13 01:02:00'),
(39, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:03:25', '2019-06-13 01:03:38'),
(40, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:05:26', '2019-06-13 01:05:36'),
(41, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:06:53', '2019-06-13 01:07:06'),
(42, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:08:21', '2019-06-13 01:08:30'),
(43, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:11:20', '2019-06-13 01:11:30'),
(44, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:11:56', '2019-06-13 01:12:09'),
(45, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:12:43', '2019-06-13 01:12:52'),
(46, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:17:19', '2019-06-13 01:17:32'),
(47, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:18:40', '2019-06-13 01:18:50'),
(48, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:21:34', '2019-06-13 01:21:45'),
(49, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:22:40', '2019-06-13 01:22:52'),
(50, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:24:16', '2019-06-13 01:24:27'),
(51, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:25:03', '2019-06-13 01:25:14'),
(52, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:26:09', '2019-06-13 01:26:22'),
(53, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:27:06', '2019-06-13 01:27:17'),
(54, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:28:31', '2019-06-13 01:28:41'),
(55, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:30:19', '2019-06-13 01:30:30'),
(56, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:34:02', '2019-06-13 01:34:16'),
(57, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:36:25', '2019-06-13 01:36:36'),
(58, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:37:52', '2019-06-13 01:38:02'),
(59, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:39:24', '2019-06-13 01:39:42'),
(60, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:40:34', '2019-06-13 01:40:50'),
(61, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:41:33', '2019-06-13 01:41:46'),
(62, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:43:13', '2019-06-13 01:43:26'),
(63, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:45:26', '2019-06-13 01:45:38'),
(64, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 01:47:20', '2019-06-13 01:47:29'),
(65, 1, 1, 0, '', 1, 5, 5, 1, 2, 53, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-13 16:01:42', '2019-06-13 18:27:14'),
(66, 1, 1, 0, '', 1, 6, 6, 1, 4, 64, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-15 15:27:48', '2019-06-15 15:47:44'),
(67, 1, 1, 0, '', 1, 7, 7, 1, 8, 74, '279eac2003c29a0db8f359010ba1dbbc', 0, 0, '', 0, 0, '2019-06-15 16:43:33', '2019-06-15 16:45:11'),
(68, 1, 1, 0, '', 1, 0, 0, 1, 0, 48, '', 0, 0, '', 1, 0, '2019-06-15 20:51:24', '2019-06-15 20:51:24'),
(69, 1, 1, 0, '', 1, 5, 5, 1, 2, 72, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-15 21:49:15', '2019-06-16 12:45:25'),
(70, 1, 1, 0, '', 1, 8, 8, 1, 10, 92, 'dc24e49fb4eec8c177eee411ac48b021', 0, 0, '', 0, 0, '2019-06-17 16:31:03', '2019-06-17 16:35:15'),
(71, 1, 1, 0, '', 1, 0, 0, 1, 9, 86, '926f5461e7e8334de6386c1bbbebd674', 0, 0, '', 0, 0, '2019-06-17 16:37:38', '2019-06-17 16:37:48'),
(72, 1, 1, 0, '', 1, 5, 5, 1, 2, 94, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-17 16:38:01', '2019-06-17 16:38:33'),
(73, 1, 1, 0, '', 1, 8, 8, 1, 10, 92, 'dc24e49fb4eec8c177eee411ac48b021', 0, 0, '', 0, 0, '2019-06-17 16:58:09', '2019-06-17 16:58:38'),
(74, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-18 12:32:53', '2019-06-18 12:33:03'),
(75, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-18 13:03:31', '2019-06-18 13:05:29'),
(76, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-18 13:06:56', '2019-06-18 13:07:09'),
(77, 1, 1, 0, '', 1, 5, 5, 1, 2, 2, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-18 14:28:07', '2019-06-18 14:28:22'),
(78, 1, 1, 0, '', 1, 6, 6, 1, 4, 99, '45b5535bdd60260f369a080845ab3e53', 0, 0, '', 0, 0, '2019-06-18 17:32:53', '2019-06-18 17:40:26'),
(79, 1, 1, 0, '', 1, 5, 5, 1, 2, 106, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-18 17:45:37', '2019-06-18 17:46:01'),
(80, 1, 1, 0, '', 1, 5, 5, 1, 2, 112, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-18 19:29:30', '2019-06-18 19:29:30'),
(81, 1, 1, 0, '', 1, 0, 0, 1, 11, 113, 'f90eae8a7c1f6c42a616faa0a89eef70', 0, 0, '', 0, 0, '2019-06-18 19:30:38', '2019-06-19 21:05:44'),
(82, 1, 1, 0, '', 1, 0, 0, 1, 0, 98, '', 0, 0, '', 0, 0, '2019-06-18 19:48:28', '2019-06-18 19:48:28'),
(83, 1, 1, 0, '', 1, 5, 5, 1, 2, 121, '0e3a71ce07c72faacb7058801be0f267', 0, 0, '', 0, 0, '2019-06-19 21:05:55', '2019-06-19 21:05:55');

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_cart_rule`
--

CREATE TABLE `ap_cart_cart_rule` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_cart_rule` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_product`
--

CREATE TABLE `ap_cart_product` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cart_product`
--

INSERT INTO `ap_cart_product` (`id_cart`, `id_product`, `id_address_delivery`, `id_shop`, `id_product_attribute`, `quantity`, `date_add`) VALUES
(14, 9, 0, 1, 0, 1, '2019-06-10 20:29:14'),
(19, 16, 5, 1, 54, 1, '2019-06-19 13:48:34'),
(21, 19, 5, 1, 0, 1, '2019-06-12 23:52:44'),
(22, 18, 0, 1, 0, 1, '2019-06-12 01:33:54'),
(24, 19, 5, 1, 0, 1, '2019-06-12 23:55:50'),
(25, 19, 5, 1, 0, 1, '2019-06-13 00:02:07'),
(26, 18, 5, 1, 0, 1, '2019-06-13 00:06:06'),
(27, 19, 5, 1, 0, 1, '2019-06-13 00:37:31'),
(28, 18, 5, 1, 0, 1, '2019-06-13 00:38:54'),
(29, 9, 5, 1, 0, 1, '2019-06-13 00:40:01'),
(30, 9, 5, 1, 0, 1, '2019-06-13 00:41:40'),
(31, 9, 5, 1, 0, 1, '2019-06-13 00:43:34'),
(32, 9, 5, 1, 0, 1, '2019-06-13 00:45:13'),
(33, 9, 5, 1, 0, 1, '2019-06-13 00:47:19'),
(34, 18, 5, 1, 0, 1, '2019-06-13 00:49:15'),
(35, 9, 5, 1, 0, 1, '2019-06-13 00:51:26'),
(36, 18, 5, 1, 0, 1, '2019-06-13 00:55:33'),
(37, 9, 5, 1, 0, 1, '2019-06-13 00:57:08'),
(38, 9, 5, 1, 0, 1, '2019-06-13 01:01:49'),
(39, 9, 5, 1, 0, 1, '2019-06-13 01:03:25'),
(40, 9, 5, 1, 0, 1, '2019-06-13 01:05:26'),
(41, 9, 5, 1, 0, 1, '2019-06-13 01:06:53'),
(42, 9, 5, 1, 0, 1, '2019-06-13 01:08:21'),
(43, 9, 5, 1, 0, 1, '2019-06-13 01:11:20'),
(44, 9, 5, 1, 0, 1, '2019-06-13 01:11:56'),
(45, 9, 5, 1, 0, 1, '2019-06-13 01:12:43'),
(46, 9, 5, 1, 0, 2, '2019-06-12 23:17:22'),
(47, 9, 5, 1, 0, 1, '2019-06-13 01:18:40'),
(48, 9, 5, 1, 0, 1, '2019-06-13 01:21:34'),
(49, 9, 5, 1, 0, 1, '2019-06-13 01:22:40'),
(50, 9, 5, 1, 0, 1, '2019-06-13 01:24:16'),
(51, 9, 5, 1, 0, 1, '2019-06-13 01:25:03'),
(52, 9, 5, 1, 0, 1, '2019-06-13 01:26:09'),
(53, 9, 5, 1, 0, 1, '2019-06-13 01:27:06'),
(54, 9, 5, 1, 0, 1, '2019-06-13 01:28:31'),
(55, 9, 5, 1, 0, 1, '2019-06-13 01:30:19'),
(56, 17, 5, 1, 0, 1, '2019-06-13 01:34:02'),
(57, 9, 5, 1, 0, 1, '2019-06-13 01:36:25'),
(58, 9, 5, 1, 0, 1, '2019-06-13 01:37:52'),
(59, 9, 5, 1, 0, 1, '2019-06-13 01:39:24'),
(60, 9, 5, 1, 0, 2, '2019-06-12 23:40:39'),
(61, 9, 5, 1, 0, 2, '2019-06-12 23:41:37'),
(62, 9, 5, 1, 0, 2, '2019-06-12 23:43:17'),
(63, 9, 5, 1, 0, 1, '2019-06-13 01:45:26'),
(64, 9, 5, 1, 0, 1, '2019-06-13 01:47:20'),
(65, 9, 5, 1, 0, 1, '2019-06-13 18:26:58'),
(66, 9, 6, 1, 0, 1, '2019-06-15 15:44:38'),
(67, 9, 7, 1, 0, 1, '2019-06-15 16:43:33'),
(68, 12, 0, 1, 0, 1, '2019-06-15 20:51:24'),
(69, 9, 5, 1, 0, 1, '2019-06-16 10:45:15'),
(70, 18, 8, 1, 0, 1, '2019-06-17 16:31:03'),
(71, 18, 0, 1, 0, 1, '2019-06-17 16:37:38'),
(72, 18, 5, 1, 0, 1, '2019-06-17 16:38:14'),
(73, 19, 8, 1, 0, 1, '2019-06-17 16:58:09'),
(74, 21, 5, 1, 0, 1, '2019-06-18 12:32:53'),
(75, 21, 5, 1, 0, 2, '2019-06-18 11:05:16'),
(76, 19, 5, 1, 0, 1, '2019-06-18 13:06:56'),
(77, 21, 5, 1, 0, 1, '2019-06-18 14:28:07'),
(78, 19, 6, 1, 0, 1, '2019-06-18 17:40:00'),
(79, 18, 5, 1, 0, 1, '2019-06-18 17:45:50'),
(82, 15, 0, 1, 0, 1, '2019-06-18 19:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule`
--

CREATE TABLE `ap_cart_rule` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `description` text,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity_per_user` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `partial_use` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `code` varchar(254) NOT NULL,
  `minimum_amount` decimal(17,2) NOT NULL DEFAULT '0.00',
  `minimum_amount_tax` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_amount_currency` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `minimum_amount_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `country_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `carrier_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `group_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `cart_rule_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `product_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shop_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `reduction_percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(17,2) NOT NULL DEFAULT '0.00',
  `reduction_tax` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `reduction_currency` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `reduction_product` int(10) NOT NULL DEFAULT '0',
  `gift_product` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gift_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `highlight` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_carrier`
--

CREATE TABLE `ap_cart_rule_carrier` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_combination`
--

CREATE TABLE `ap_cart_rule_combination` (
  `id_cart_rule_1` int(10) UNSIGNED NOT NULL,
  `id_cart_rule_2` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_country`
--

CREATE TABLE `ap_cart_rule_country` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_group`
--

CREATE TABLE `ap_cart_rule_group` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_lang`
--

CREATE TABLE `ap_cart_rule_lang` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_product_rule`
--

CREATE TABLE `ap_cart_rule_product_rule` (
  `id_product_rule` int(10) UNSIGNED NOT NULL,
  `id_product_rule_group` int(10) UNSIGNED NOT NULL,
  `type` enum('products','categories','attributes','manufacturers','suppliers') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_product_rule_group`
--

CREATE TABLE `ap_cart_rule_product_rule_group` (
  `id_product_rule_group` int(10) UNSIGNED NOT NULL,
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_product_rule_value`
--

CREATE TABLE `ap_cart_rule_product_rule_value` (
  `id_product_rule` int(10) UNSIGNED NOT NULL,
  `id_item` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cart_rule_shop`
--

CREATE TABLE `ap_cart_rule_shop` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_category`
--

CREATE TABLE `ap_category` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `id_shop_default` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `nleft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nright` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_root_category` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_category`
--

INSERT INTO `ap_category` (`id_category`, `id_parent`, `id_shop_default`, `level_depth`, `nleft`, `nright`, `active`, `date_add`, `date_upd`, `position`, `is_root_category`) VALUES
(1, 0, 1, 0, 1, 6, 1, '2019-06-08 02:34:20', '2019-06-08 02:34:20', 0, 0),
(2, 1, 1, 1, 2, 5, 1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0, 1),
(12, 2, 1, 2, 3, 4, 1, '2019-06-08 03:08:35', '2019-06-15 14:49:45', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_category_group`
--

CREATE TABLE `ap_category_group` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_category_group`
--

INSERT INTO `ap_category_group` (`id_category`, `id_group`) VALUES
(2, 0),
(2, 1),
(2, 2),
(2, 3),
(12, 1),
(12, 2),
(12, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ap_category_lang`
--

CREATE TABLE `ap_category_lang` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_category_lang`
--

INSERT INTO `ap_category_lang` (`id_category`, `id_shop`, `id_lang`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 1, 'Racine', '', 'racine', '', '', ''),
(2, 1, 1, 'Accueil', '', 'accueil', '', '', ''),
(12, 1, 1, 'FORFAITS ', '<p>Les forfaits de auto-permis</p>', 'forfaits', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_category_product`
--

CREATE TABLE `ap_category_product` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_category_product`
--

INSERT INTO `ap_category_product` (`id_category`, `id_product`, `position`) VALUES
(2, 12, 0),
(2, 13, 1),
(2, 18, 2),
(2, 22, 3),
(12, 19, 0),
(12, 18, 1),
(12, 17, 2),
(12, 16, 3),
(12, 15, 4),
(12, 20, 5),
(12, 13, 6),
(12, 12, 7),
(12, 11, 8),
(12, 10, 9),
(12, 9, 10),
(12, 21, 11),
(12, 23, 12),
(12, 24, 13),
(12, 25, 14);

-- --------------------------------------------------------

--
-- Table structure for table `ap_category_shop`
--

CREATE TABLE `ap_category_shop` (
  `id_category` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_category_shop`
--

INSERT INTO `ap_category_shop` (`id_category`, `id_shop`, `position`) VALUES
(1, 1, 0),
(2, 1, 0),
(12, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms`
--

CREATE TABLE `ap_cms` (
  `id_cms` int(10) UNSIGNED NOT NULL,
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `indexation` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms`
--

INSERT INTO `ap_cms` (`id_cms`, `id_cms_category`, `position`, `active`, `indexation`) VALUES
(1, 1, 0, 1, 0),
(2, 1, 1, 1, 0),
(3, 1, 2, 1, 0),
(4, 1, 3, 1, 0),
(5, 1, 4, 1, 0),
(6, 2, 0, 1, 1),
(7, 2, 1, 1, 1),
(8, 2, 2, 1, 1),
(9, 2, 3, 1, 0),
(10, 2, 4, 1, 1),
(11, 2, 5, 1, 1),
(12, 1, 5, 1, 0),
(13, 1, 6, 1, 0),
(14, 1, 7, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_block`
--

CREATE TABLE `ap_cms_block` (
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `location` tinyint(1) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `display_store` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_block`
--

INSERT INTO `ap_cms_block` (`id_cms_block`, `id_cms_category`, `location`, `position`, `display_store`) VALUES
(1, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_block_lang`
--

CREATE TABLE `ap_cms_block_lang` (
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_block_lang`
--

INSERT INTO `ap_cms_block_lang` (`id_cms_block`, `id_lang`, `name`) VALUES
(1, 1, 'Informations');

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_block_page`
--

CREATE TABLE `ap_cms_block_page` (
  `id_cms_block_page` int(10) UNSIGNED NOT NULL,
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_cms` int(10) UNSIGNED NOT NULL,
  `is_category` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_block_page`
--

INSERT INTO `ap_cms_block_page` (`id_cms_block_page`, `id_cms_block`, `id_cms`, `is_category`) VALUES
(1, 1, 1, 0),
(2, 1, 2, 0),
(3, 1, 3, 0),
(4, 1, 4, 0),
(5, 1, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_block_shop`
--

CREATE TABLE `ap_cms_block_shop` (
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_block_shop`
--

INSERT INTO `ap_cms_block_shop` (`id_cms_block`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_category`
--

CREATE TABLE `ap_cms_category` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_category`
--

INSERT INTO `ap_cms_category` (`id_cms_category`, `id_parent`, `level_depth`, `active`, `date_add`, `date_upd`, `position`) VALUES
(1, 0, 1, 1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0),
(2, 1, 2, 1, '2019-06-08 03:10:50', '2019-06-08 03:10:50', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_category_lang`
--

CREATE TABLE `ap_cms_category_lang` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_category_lang`
--

INSERT INTO `ap_cms_category_lang` (`id_cms_category`, `id_lang`, `id_shop`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 1, 'Accueil', '', 'accueil', '', '', ''),
(2, 1, 1, 'Documents', '', 'documents', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_category_shop`
--

CREATE TABLE `ap_cms_category_shop` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_category_shop`
--

INSERT INTO `ap_cms_category_shop` (`id_cms_category`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_lang`
--

CREATE TABLE `ap_cms_lang` (
  `id_cms` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `meta_title` varchar(128) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `content` longtext,
  `link_rewrite` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_lang`
--

INSERT INTO `ap_cms_lang` (`id_cms`, `id_lang`, `id_shop`, `meta_title`, `meta_description`, `meta_keywords`, `content`, `link_rewrite`) VALUES
(1, 1, 1, 'Livraison', 'Nos conditions de livraison', 'conditions, livraison, délais, expédition, colis', '<h2>Expéditions et retours</h2><h3>Expédition de votre colis</h3><p>Les colis sont généralement expédiés dans un délai de 2 jours après réception du paiement. Ils sont expédiés via UPS avec un numéro de suivi et remis sans signature. Les colis peuvent également être expédiés via UPS Extra et remis contre signature. Veuillez nous contacter avant de choisir ce mode de livraison, car il induit des frais supplémentaires. Quel que soit le mode de livraison choisi, nous vous envoyons un lien pour suivre votre colis en ligne.</p><p>Les frais d\'expédition incluent les frais de préparation et d\'emballage ainsi que les frais de port. Les frais de préparation sont fixes, tandis que les frais de transport varient selon le poids total du colis. Nous vous recommandons de regrouper tous vos articles dans une seule commande. Nous ne pouvons regrouper deux commandes placées séparément et des frais d\'expédition s\'appliquent à chacune d\'entre elles. Votre colis est expédié à vos propres risques, mais une attention particulière est portée aux objets fragiles.<br /><br />Les dimensions des boîtes sont appropriées et vos articles sont correctement protégés.</p>', 'livraison'),
(2, 1, 1, 'Mentions légales', 'Mentions légales', 'mentions, légales, crédits', '<h2>Mentions légales</h2><h3>Crédits</h3><p>Conception et production :</p><p>cette boutique en ligne a été créée à l\'aide du <a href=\"http://www.prestashop.com\">logiciel PrestaShop. </a>Rendez-vous sur le <a href=\"http://www.prestashop.com/blog/en/\">blog e-commerce de PrestaShop</a> pour vous tenir au courant des dernières actualités et obtenir des conseils sur la vente en ligne et la gestion d\'un site d\'e-commerce.</p>', 'mentions-legales'),
(3, 1, 1, 'Conditions d\'utilisation', 'Nos conditions d\'utilisation', 'conditions, utilisation, vente', '<h1 class=\"page-heading\">Conditions d\'utilisation</h1>\n<h3 class=\"page-subheading\">Règle n° 1</h3>\n<p class=\"bottom-indent\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<h3 class=\"page-subheading\">Règle n° 2</h3>\n<p class=\"bottom-indent\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>\n<h3 class=\"page-subheading\">Règle n° 3</h3>\n<p class=\"bottom-indent\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>', 'conditions-utilisation'),
(4, 1, 1, 'A propos', 'En savoir plus sur notre entreprise', 'à propos, informations', '<h1 class=\"page-heading bottom-indent\">A propos</h1>\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-block\">\n<h3 class=\"page-subheading\">Notre entreprise</h3>\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</strong></p>\n<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur adipisicing elit.</p>\n<ul class=\"list-1\">\n<li><em class=\"icon-ok\"></em>Produits haute qualité</li>\n<li><em class=\"icon-ok\"></em>Service client inégalé</li>\n<li><em class=\"icon-ok\"></em>Remboursement garanti pendant 30 jours</li>\n</ul>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Notre équipe</h3>\n<img title=\"cms-img\" src=\"../img/cms/cms-img.jpg\" alt=\"cms-img\" width=\"370\" height=\"192\" />\n<p><strong class=\"dark\">Lorem set sint occaecat cupidatat non </strong></p>\n<p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>\n</div>\n</div>\n<div class=\"col-xs-12 col-sm-4\">\n<div class=\"cms-box\">\n<h3 class=\"page-subheading\">Témoignages</h3>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Lorem ipsum dolor sit</strong></p>\n<div class=\"testimonials\">\n<div class=\"inner\"><span class=\"before\">“</span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod.<span class=\"after\">”</span></div>\n</div>\n<p><strong class=\"dark\">Ipsum dolor sit</strong></p>\n</div>\n</div>\n</div>', 'a-propos'),
(5, 1, 1, 'Paiement sécurisé', 'Notre méthode de paiement sécurisé', 'paiement sécurisé, ssl, visa, mastercard, paypal', '<h2>Paiement sécurisé</h2>\n<h3>Notre paiement sécurisé</h3><p>Avec SSL</p>\n<h3>Avec Visa/Mastercard/Paypal</h3><p>A propos de ce service</p>', 'paiement-securise'),
(6, 1, 1, 'Contrat de location', '', '', '<h1 class=\"western\"><span><span>CONTRAT DE LOCATION</span></span></h1>\n<hr /><h2 class=\"western\" align=\"left\"><span><span>1. DÉFINITIONS</span></span></h2>\n<p align=\"left\"><span><span>Nout\'Permis n\'est pas une auto-école et ne délivre pas, et n\'organise d\'examen de permis de conduire.</span></span></p>\n<p align=\"left\"><span>Dans le présent contrat, les mots suivants désignent :</span></p>\n<ul><li>\n<p align=\"left\">Loueur: entreprise <span>Nout\'Permis</span></p>\n</li>\n<li>\n<p align=\"left\">Tuteur: toute personne physique, titulaire du permis de conduire depuis plus de 5 (cinq) ans</p>\n</li>\n<li>\n<p align=\"left\">Apprenti: la personne physique inscrite, possédant son livret d\'apprentissage en cours de validité comportant un minimum de 20 heures en auto école et conduisant le véhicule, accompagnée du tuteur (ou le permis de conduire de l\'apprenti s\'il s\'agit de remise a niveau)</p>\n</li>\n<li>\n<p align=\"left\">Contrat: le contrat de location de véhicule, son règlement et ses annexes.</p>\n</li>\n</ul><p align=\"left\"><span>Entreprise Nout\'Permis<br />Et le Locataire défini par les éléments du formulaire d\'inscription qu\'il a rempli sur le site www.noutpermis.com (nom, prénom, adresse, CP, ville, numéro de téléphone portable, courriel...)</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>2. DURÉE</span></span></h2>\n<p align=\"left\"><span>Ce contrat court à compter de la date d\'inscription en agence. Celui-ci est conclu pour une durée minimale correspondant à l\'exercice de votre forfait (voir détail des forfaits sur le site ou en agence) . II peut être mis fin au présent contrat dans les conditions décrites par les dispositions des présentes relatives à la résiliation.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>3. CONDITIONS D\'ADHÉSION</span></span></h2>\n<p align=\"left\"><span>a) ne peuvent accéder à la location des véhicules du Loueur que les tuteurs disposant d\'un permis de conduire français ou d\'un pays membre de l\'Union Européenne en cours de validité depuis plus de 5 ans et agé de minimum 28 ans.<br />b) ne peuvent conduire des véhicules du Loueur que les Apprentis de plus de 18 ans, disposant d\'un livret d\'apprentissage en cours de validité comportant un minimum de 20 heures en auto école, les conduites accompagnées, les permis annulès qui sont en droit de repasser le permis (gendarmerie et attestation médicale à jour). ( et les personnes qui possèdent déjà le permis de conduire souhaitant faire de la remise a niveau)<br />c) le Locataire inscrit a accès aux véhicules (tous les jours de 6h à 22h)</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>4. AUTORISATION DE PRÉLÈVEMENT SUR CARTE BANCAIRE OU REMISE DE CHEQUE DE CAUTION OU D\'ESPÈCES (ÉQUIVALANT À UN DÉPÔT DE GARANTIE)</span></span></h2>\n<p align=\"left\"><span>Lors de l\'acceptation des présentes le Locataire autorise le Loueur à prélever sur sa carte bancaire ou à utiliser le chèque de caution ou les espèces pour toute prestation complémentaire prévue dans le présent contrat, en plus du forfait qu\'il a choisi, Cette autorisation est notamment constituée pour les dommages causés au véhicule, vol, contravention et d\'une manière générale toutes les dépenses consécutives à la responsabilité du Locataire. <br />Cette autorisation est obligatoire et personnelle à chaque Locataire dès le paiement. Son montant équivaut au montant de la franchise.<br />En cas de sinistre ou de fin de validité, l\'autorisation de prélèvement, le chèque de caution sur le compte bancaire du locataire ou les espèces, seront à reconstituer par le Locataire dans son intégralité pour qu\'il puisse continuer à bénéficier des services du Loueur.<br />Le conducteur doit être reconnu fautif ou responsable pour que le Loueur puisse conserver la franchise partiellement, ou totalement, jusqu\'à concurrence du montant des frais engagés par le Loueur. Au 1er Mai 2007 le montant de la franchise est de 500 (cinq cent) euros.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>5. USAGES INTERDITS</span></span></h2>\n<p align=\"left\"><span>L\'usage d\'un véhicule dans les circonstances décrites ci-dessous est interdit:</span></p>\n<ul><li>\n<p align=\"left\">dans une course de vitesse ou un concours</p>\n</li>\n<li>\n<p align=\"left\">dans le but de tirer, de pousser ou de propulser une remorque ou un autre véhicule ou dans toutes circonstances contradictoires au bon usage d\'un véhicule</p>\n</li>\n<li>\n<p align=\"left\">par un tuteur ou Apprenti se trouvant sous l\'influence de l\'alcool, de drogues ou de médicaments qui diminuent sa capacité à conduire normalement un véhicule</p>\n</li>\n<li>\n<p align=\"left\">dans l\'accomplissement d\'un délit ou d\'une activité illégale</p>\n</li>\n<li>\n<p align=\"left\">d\'une manière imprudente ou à mauvais escient, sans respecter le code de la route</p>\n</li>\n<li>\n<p align=\"left\">par une personne ayant volontairement donné au Loueur des informations erronées lors de son inscription.</p>\n</li>\n<li>\n<p align=\"left\">consommer de la nourriture, de la boisson ou du tabac</p>\n</li>\n<li>\n<p align=\"left\">Dans le cas où le locataire se fait rémunérer par l\'apprenti.</p>\n</li>\n<li>\n<p align=\"left\"><span>NOS ETABLISSEMENTS NOUT\'PERMIS NE PROPOSENT PAS DE MONITEURS</span></p>\n</li>\n</ul><p align=\"left\"><span>L\'autorisation d\'enseigner ne peut être délivrée qu\'aux personnes titulaires du brevet pour l\'exercice de la profession d\'enseignant de la conduite automobile et de la sécurité routière (BEPECASER). Source : Article R212-3 Modifié par Décret nº2004-106 du 29 janvier 2004 - art. 10 (V) JORF 5 février 2004</span></p>\n<p align=\"left\"><span>La formation, à titre onéreux ne peut être dispensée que dans le cadre d\'un établissement dont l\'exploitation est subordonnée à un agrément délivré par l\'autorité administrative (auto-école)</span></p>\n<p align=\"left\"><span>Source : Article L213-6 Modifié Loi nº2007-297 du 5 mars 2007 - art. 23 (V) JORF 7 mars 2007 en vigueur au plus tard le 7 mars 2009</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>6. RÉSERVATION</span></span></h2>\n<p align=\"left\"><span>Pour pouvoir utiliser les véhicules une réservation est obligatoire, elle peut se faire par téléphone ou directement sur le site www.noutpermis.com et règlement par avance des heures. <br />La réservation des véhicules se fait à partir de l\'heure convenue et toujours heure par heure (ex: vous réservez un véhicule pour 2 heures de 15h00 à 17h00).<br />La période de location minimale est d\'1 (une) heure en semaine, et de 2 (deux) heures les week-ends et jour fériés.<br />Une réservation ne peut être faite plus d\'un mois à l\'avance. Toute annulation d\'une réservation doit être faite 48 (quarante huit) heures avant l\'heure de réservation du véhicule. Aucune prolongation n\'est possible. Toute réservation sera dues si elle n\'est pas annulée 48 h avant l\'heure réservée. Il est possible d\'annuler vos heures de réservation (48 heures à l\'avance) par téléphone.<br />Le choix du véhicule n\'est pas libre pour le Locataire il se fait selon la disponibilité de nos différents véhicules.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>7. UTILISATION D\'UN VÉHICULE</span></span></h2>\n<p align=\"left\"><span>a) le Locataire doit se rendre à l\'agence indiquée lors de sa réservation 5(minutes) en avance pour prendre le véhicule. II doit le rapporter propre, en bon état de fonctionnement, au même lieu au plus tard 5(cinq) minutes avant la fin de la période pour laquelle il a réservé le véhicule.</span></p>\n<p align=\"left\"><span>b) la boîte à gants contient : une photocopie de la carte grise, la carte verte. En acceptant d\'utiliser le véhicule vous approuvez que son état est conforme à la fiche \"état du véhicule\". En cas de problème pendant la période d\'utilisation du véhicule, il est demandé au Locataire de signaler immédiatement les faits par téléphone à l\'agence.</span></p>\n<p align=\"left\"><span>c) Seul l\'apprenti et le locataire sont autorisés a bord du véhicule</span></p>\n<p align=\"left\"><span>d) Le transport d\'une personne mineur est strictement interdit.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>8. CALCUL DES KILOMÈTRES ET DU TEMPS</span></span></h2>\n<h3 class=\"western\" align=\"left\"><span><span>a) Principes d\'utilisation</span></span></h3>\n<p align=\"left\"><span>Le calcul des kilomètres parcourus commence à l\'endroit où le véhicule est pris en charge par le Locataire et se termine à son retour à la même place.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>9. PLEIN D\'ESSENCE</span></span></h2>\n<p align=\"left\"><span>Notre service de location de véhicules inclut, carburant (forfait open), assurances, entretien et services. Lorsque le Locataire utilise un véhicule du Loueur, le coût de l\'essence est assumé par le Loueur dans le cadre du forfait open. A l\'exception du forfait Open, le Locataire doit s\'assurer, lors de la restitution du véhicule, que la quantité de carburant du réservoir est identique à celle mesurée lors de l\'acquisition du véhicule. Le bon fonctionnement du service repose sur le bon comportement de tous les Locataires. Au cas où le Locataire devrait faire le plein, celui- ci doit demander une facture détaillée afin de se faire rembourser. II doit transmettre au Loueur le justificatif du montant payé qui lui sera remboursé en agence. (cf article 14 : \"Frais divers\").</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>10. ANOMALIES</span></span></h2>\n<p align=\"left\"><span>Le Locataire doit signaler au Loueur, dès qu\'il en a connaissance, toute irrégularité de fonctionnement d\'un véhicule telle que perte d\'huile, bruit anormal, affaiblissement de la batterie, manque de liquide lave glace, etc. Le Locataire s\'engage à informer dans les plus brefs délais le Loueur en cas d\'intervention par les forces de police sur un véhicule du Loueur à l\'occasion de son utilisation. Le Locataire reste personnellement responsable de toute peine, amende ou autre sanction qui pourrait résulter du non-respect de ces règles et modalités.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>11. PÉNALITÉS</span></span></h2>\n<h3 class=\"western\" align=\"left\"><span><span>a) Retard</span></span></h3>\n<p align=\"left\"><span>Excepté en cas de panne, sinistre, vol, incendie, tout retard par rapport à la fin de la réservation entraîne une pénalité de :</span></p>\n<ul><li>\n<p align=\"left\">7 (sept) euros par quart d\'heure entamé, à condition que ce retard ne pénalise aucun autre client.</p>\n</li>\n<li>\n<p align=\"left\">Si ce retard pénalise un client bénéficiant d\'une réservation valide, le Locataire retardataire se verra imputé d\'une pénalité forfaitaire doublée, soit de 15 (quinze) euros par quart d\'heure entamé.</p>\n</li>\n</ul><h3 class=\"western\" align=\"left\"><span><span>b) Annulation ou modification d\'une réservation</span></span></h3>\n<p align=\"left\"><span>Aucune pénalité pour toute réservation annulée ou modifiée 48 (quarante huit) heures avant le début de la période d\'utilisation. <br />Dans le cas contraire, si la réservation n\'est pas honorée, l\'heure sera due de plein droit. Si le nombre de réservations non honorées est supérieur à 3 (trois), le Loueur se réserve le droit de résilier l\'adhésion au service</span></p>\n<h3 class=\"western\" align=\"left\"><span><span>c) Pénalités générales</span></span></h3>\n<p align=\"left\"><span>De manière générale toute dépense générée par le Locataire directement ou indirectement sera due par le Locataire, et occasionnera des frais de gestion forfaitaire de 15 (quinze) euros, indépendamment de la refacturation des sommes engagées par le Loueur pour réparer, remplacer ou nettoyer l\'élément dégradé, notamment en cas d\'oubli, omission et négligence par le Locataire entraînant des inconvénients au Loueur ou à d\'autres Locataires, tels que :</span></p>\n<ul><li>\n<p align=\"left\">L\'oubli ou la perte de la clé,</p>\n</li>\n<li>\n<p align=\"left\">Rendre le véhicule à un autre endroit que celui prévu sans prévenir le service,</p>\n</li>\n<li>\n<p align=\"left\">Restituer le véhicule dans un état non satisfaisant (15 euros), poiles de chien vous sera facturé 50 euros...</p>\n</li>\n<li>\n<p align=\"left\">En cas de détérioration, de perte ou de vol d\'un des éléments suivants, le Locataire devra contacter le Loueur au plus vite:</p>\n<ul><li>\n<p align=\"left\">clé du véhicule</p>\n</li>\n<li>\n<p align=\"left\">papiers du véhicule</p>\n</li>\n<li>\n<p align=\"left\">détérioration ou dégradation à l\'intérieur ou à l\'extérieur du véhicule en fonction des dommages.</p>\n</li>\n</ul></li>\n<li>\n<p align=\"left\">une contravention non signalée et/ou non acquittée par le Locataire dans les délais impartis</p>\n</li>\n<li>\n<p align=\"left\">frais de gestion en cas de non paiement</p>\n</li>\n<li>\n<p align=\"left\">déplacement d\'un de nos agents pour assistance due à un mauvais usage ou un oubli d\'un adhérent.</p>\n</li>\n<li>\n<p align=\"left\">Sinistre responsable avec ou sans tiers.</p>\n</li>\n</ul><h2 class=\"western\" align=\"left\"><span><span>12. MODE DE RÈGLEMENT</span></span></h2>\n<p align=\"left\"><span>Le Locataire, du fait de son adhésion, autorise le Loueur à effectuer le prélèvement du montant des sommes qu\'il doit sur sa carte bancaire ou à encaisser le chèque de caution.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>13. FRAIS DIVERS</span></span></h2>\n<p align=\"left\"><span>Tous les documents seront envoyés automatiquement et gratuitement au Locataire par courriel. Si le Locataire souhaite recevoir par la poste un document ou une facture, le Loueur facturera 5 euros à chaque envoi par courrier. Tous les frais accessoires tels que parking, infraction au code de la route ou autres, restent à la charge du Locataire. Si le Locataire fait le plein de carburant il doit appeler le Loueur pour obtenir une autorisation de faire le plein à sa charge, qui lui sera remboursé contre un reçu détaillé, total à payer, TVA et les nom et adresse de la station. Le Locataire s\'engage à accepter de régler au Loueur toutes les sommes dues au titre de consommation du service et au titre de cet engagement contractuel dans le cas où sa responsabilité est engagée.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>14. ASSURANCES</span></span></h2>\n<p align=\"left\"><span>L\'apprenti et le tuteur sont couverts par les assurances suivantes:</span></p>\n<ul><li>\n<p align=\"left\">Responsabilité civile automobile.</p>\n</li>\n<li>\n<p align=\"left\">Dommages tous accidents : il sera fait application d\'une franchise restant à la charge du Locataire et pouvant être majorée des pénalités du présent contrat.</p>\n</li>\n<li>\n<p align=\"left\">Incendie, vol du véhicule avec une franchise à la charge du Locataire.</p>\n</li>\n<li>\n<p align=\"left\">Sécurité du conducteur.</p>\n</li>\n<li>\n<p align=\"left\">Catastrophes naturelles après déduction de la franchise légale en vigueur.</p>\n</li>\n</ul><p align=\"left\"><span>Au 1er Janvier 2010, la franchise s\'élève à 500 (cinq cents) euros.</span></p>\n<p align=\"left\"><span>En cas de sinistre responsable ou sans possibilité de recours contre un tiers identifié le Locataire sera alors responsable à concurrence de la franchise. Cette franchise sera également applicable sur les dommages occasionnés à des tiers même en l\'absence de dégât sur le véhicule loué.</span></p>\n<p align=\"left\"><span>Le bris de glaces, des optiques de phares et des rétroviseurs restent à la charge du locataire</span></p>\n<ul><li>\n<p align=\"left\">Responsabilité civile selon les dispositions légales</p>\n</li>\n<li>\n<p align=\"left\">Dommages tous accidents (\" tous risques \") : il sera fait application d\'une franchise restant à la charge du Locataire et pouvant être majorée des pénalités du présent contrat (cf article 12), excepté si la responsabilité de l\'accident est imputable à un tiers identifié.</p>\n</li>\n<li>\n<p align=\"left\">Incendie, vol du véhicule avec une franchise à la charge du Locataire</p>\n</li>\n<li>\n<p align=\"left\">Sécurité du conducteur selon le barème du droit commun</p>\n</li>\n<li>\n<p align=\"left\">Catastrophes naturelles après déduction de la franchise en vigueur Au 1er janvier 2010, la franchise s\'élève à 500 (cinq cents) euros.</p>\n</li>\n</ul><h3 class=\"western\" align=\"left\"><span><span>a) Responsabilité du Locataire</span></span></h3>\n<p align=\"left\"><span>Le Locataire est responsable de la pleine valeur de tout dommage causé à un véhicule non couvert par la police d\'assurance du Loueur ou par la garantie du fabricant du véhicule qui survient durant la période où il utilise ledit véhicule, de tout dommage attribuable à un animal, et de toute souillure nécessitant un nettoyage particulier. Sans égard à la franchise qu\'il peut avoir souscrit, le Locataire est responsable des dommages causés par la perte de la clé du véhicule ou pour tout autre dommage causé par sa faute et qui n\'est pas couvert par la police d\'assurance du Loueur ou par la garantie du fabricant du véhicule, notamment s\'il:</span></p>\n<ul><li>\n<p align=\"left\">utilise un véhicule à des fins interdites</p>\n</li>\n<li>\n<p align=\"left\">déroge à tout critère ou à toute condition du présent contrat, notamment s\'il omet de recueillir les informations requises ou de collaborer entièrement à la suite d\'un accident et que cette négligence entraîne des frais additionnels pour le Loueur</p>\n</li>\n<li>\n<p align=\"left\">utilise un véhicule de manière négligente, noie le moteur lors du démarrage ou omet de respecter les instructions contenues dans le manuel du propriétaire</p>\n</li>\n<li>\n<p align=\"left\">néglige de retirer la clé du véhicule ou de vérifier que le véhicule est fermé et que tous les ouvrants sont verrouillés (les portières, les glaces, le coffre, etc.)</p>\n</li>\n<li>\n<p align=\"left\">néglige d\'éteindre certains accessoires au retour du véhicule (phares, essuie-glaces, etc...)</p>\n</li>\n<li>\n<p align=\"left\">omet d\'aviser le Loueur du vol, du vandalisme ou des dommages causés à un véhicule du Loueur ou de tout accident dans un délai maximum de 24 (vingt-quatre) heures.</p>\n</li>\n<li>\n<p align=\"left\">communique de fausses informations au Loueur</p>\n</li>\n</ul><h3 class=\"western\" align=\"left\"><span><span>b) Déplacements à l\'étranger</span></span></h3>\n<p align=\"left\"><span>Le Locataire ne peut conduire ou utiliser les véhicules du Loueur hors du territoire Français métropolitain (ni en Corse, ni dans les départements et territoires d\'outre mer).</span></p>\n<h3 class=\"western\" align=\"left\"><span><span>c) Infractions au Code de la sécurité routière</span></span></h3>\n<p align=\"left\"><span>Le locataire est responsable des contraventions reçues pendant la période d\'utilisation d\'un véhicule. Le locataire doit signaler au Loueur, dès que possible, toute contravention qui ne peut être réglée dans les délais impartis (cas de recours, procédure judiciaire, etc.). Le locataire est tenu de signaler la présence d\'une contravention trouvée sur le véhicule lors de la prise de possession de celui-ci. Dans le cas contraire, le locataire concerné est responsable des frais encourus par le Loueur pour son omission.<br />À la fin de sa période de réservation, le locataire ne peut abandonner le véhicule du Loueur dans une zone comportant des restrictions de stationnement. Si le locataire abandonne un véhicule dans une zone comportant de telles restrictions, il doit en aviser le Loueur. À défaut, celui-ci reste responsable des frais encourus par le Loueur pour toute contravention ou remorquage occasionné par le défaut du locataire.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>15. PANNE, PROBLÈME OU ACCIDENT</span></span></h2>\n<h3 class=\"western\" align=\"left\"><span><span>a) Panne</span></span></h3>\n<p align=\"left\"><span>Lorsqu\'il utilise un véhicule, le Locataire doit s\'assurer de respecter le manuel du propriétaire. En cas de problème qui empêche ou limite l\'utilisation du véhicule ou qui est susceptible de porter atteinte à la sécurité des personnes, il doit communiquer avec le Loueur et disposer du véhicule de façon sécuritaire et selon les instructions du Loueur. Toute dépense doit être autorisée par le Loueur. S\'il y a lieu, les frais de dépannage, de garagiste et d\'autre nature doivent être autorisés par le service et identifiés au nom d\'un garage ou d\'une structure. Dans le cas où le Locataire doit assumer les frais, il obtient un remboursement en agence, sur présentation de pièces justificatives.</span></p>\n<h3 class=\"western\" align=\"left\"><span><span>b) Démarrage assisté</span></span></h3>\n<p align=\"left\"><span>Si un Locataire effectue un démarrage assisté, il doit en aviser le Loueur pour obtenir l\'autorisation. Le Locataire est entièrement responsable des dommages pouvant résulter d\'une mauvaise utilisation des câbles d\'appoint. II est strictement interdit d\'effectuer un démarrage assisté pour dépanner d\'autres véhicules que ceux du Loueur.</span></p>\n<h3 class=\"western\" align=\"left\"><span><span>c) Accident</span></span></h3>\n<p align=\"left\"><span>En cas d\'accident il faut appeler immédiatement le Loueur. Si celui-ci entraîne des dommages physiques ou matériels, après acceptation du loueur le Locataire pourra remplir un constat amiable et noter les renseignements suivants :</span></p>\n<ul><li>\n<p align=\"left\">la date, l\'heure, le lieu et les circonstances de l\'accident</p>\n</li>\n<li>\n<p align=\"left\">le numéro de plaque du ou des autres véhicules en cause, leur marque et leur année, leur numéro d\'identification (numéro de série) et le numéro de l\'attestation d\'assurance (avec nom et coordonnées de l\'assureur)</p>\n</li>\n<li>\n<p align=\"left\">le nom, l\'adresse et le numéro de téléphone des conducteurs en cause et leur numéro de permis de conduire</p>\n</li>\n<li>\n<p align=\"left\">les coordonnées du propriétaire du ou des véhicules (si ce n\'est pas le conducteur)</p>\n</li>\n<li>\n<p align=\"left\">les coordonnées du propriétaire du ou des véhicules (si ce n\'est pas le conducteur)le nom, l\'adresse et le numéro de téléphone des témoins, s\'il y a lieu (précisez s\'il s\'agit d\'un passager)</p>\n</li>\n<li>\n<p align=\"left\">une description des dommages causés aux véhicules</p>\n</li>\n<li>\n<p align=\"left\">le tout signé par les conducteurs en cause</p>\n</li>\n</ul><h3 class=\"western\" align=\"left\"><span><span>d) Accident suivi d\'un délit de fuite</span></span></h3>\n<p align=\"left\"><span>En cas d\'accident suivi d\'un délit de fuite, le Locataire doit obligatoirement faire établir un rapport de police.</span></p>\n<h3 class=\"western\" align=\"left\"><span><span>e) Enquête et procédure</span></span></h3>\n<p align=\"left\"><span>Le Locataire s\'engage à livrer au Loueur et à tout autre service de traitement de réclamation, les conclusions de tout rapport ou avis au sujet d\'une revendication ou d\'une poursuite contre le Loueur, relativement à un accident mettant en cause un véhicule du Loueur. Le Locataire s\'engage à collaborer entièrement avec le Loueur à l\'enquête et à la défense dans une affaire de revendication ou de poursuite de ce genre. Le Locataire autorise le Loueur à transmettre toutes informations uniquement aux services de police et à l\'assureur du véhicule du Loueur dans le cadre de cet article.</span></p>\n<h2 class=\"western\" align=\"left\"><span><span>16. RÉSILIATION</span></span></h2>\n<p align=\"left\"><span>Les forfaits ne sont ni remboursable, ni échangeable.<br />Les forfait ont une date de validé entre 1 (un) mois et 6 (mois) selon le forfait choisit (1-9h=1mois ; 10-19h=3mois ; +de20h=6mois)<br />Le Loueur peut mettre fin unilatéralement au contrat, de plein droit, et ceci sans préavis en cas de faute contractuelle commise par le Locataire et notamment dans les cas suivants :</span></p>\n<ul><li>\n<p align=\"left\">défaut de paiement d\'une seule des sommes facturées par le loueur à quelque titre que ce soit</p>\n</li>\n<li>\n<p align=\"left\">propos ou comportement outrageux envers le loueur</p>\n</li>\n<li>\n<p align=\"left\">défaut de paiement d\'une franchise, totale ou partielle, ou de pénalités</p>\n</li>\n<li>\n<p align=\"left\">fausse déclaration</p>\n</li>\n<li>\n<p align=\"left\">vol, fraude ou détérioration du matériel loué (véhicule ou accessoires)</p>\n</li>\n<li>\n<p align=\"left\">sous location à un tiers, ou prêt à un tiers du véhicule</p>\n</li>\n<li>\n<p align=\"left\">non restitution du matériel loué dans les délais contractuels sans avis du loueur</p>\n</li>\n<li>\n<p align=\"left\">usage illicite du véhicule prévu par le code de la route, le code des assurances, toute autre disposition réglementaire ou municipale</p>\n</li>\n<li>\n<p align=\"left\">usage du véhicule en dehors du territoire français</p>\n</li>\n<li>\n<p align=\"left\">non déclaration de changement d\'adresse, de carte bancaire ou d\'une invalidation du permis de conduire du tuteur ou du livret d\'apprentissage de l\'Apprenti</p>\n</li>\n<li>\n<p align=\"left\">usage du véhicule sous l\'emprise d\'un état alcoolique (concerne l\'Apprenti et le tuteur)</p>\n</li>\n<li>\n<p align=\"left\">usage sous l\'emprise de stupéfiants ou de médicaments pouvant entraîner une diminution de la vigilance (concerne l\'Apprenti et le tuteur).</p>\n</li>\n<li>\n<p align=\"left\">Constat de cendre de cigarettes ou de nourriture retrouvé dans le véhicule</p>\n</li>\n<li>\n<p align=\"left\">Transport d\'une personne mineur strictement interdit.</p>\n</li>\n<li>\n<p align=\"left\">Transport d\'une personne supplémentaire majeur dans le véhicule sans acceptation du loueur.</p>\n</li>\n</ul><p align=\"left\"><span>Dans ces cas de faute contractuelle du tuteur ou de l\'Apprenti, le Loueur se réserve la possibilité</span></p>', 'contrat-de-location'),
(7, 1, 1, 'Assurances et Franchises', '', '', '<h2 class=\"western\" align=\"left\"><span> ASSURANCES</span></h2>\n<h3 align=\"left\"><span>L\'apprenti et le tuteur sont couverts par les assurances suivantes:</span></h3>\n<ul><li>\n<h3 align=\"left\">Responsabilité civile automobile.</h3>\n</li>\n<li>\n<h3 align=\"left\">Dommages tous accidents : il sera fait application d\'une franchise restant à la charge du Locataire et pouvant être majorée des pénalités du présent contrat.</h3>\n</li>\n<li>\n<h3 align=\"left\">Incendie, vol du véhicule avec une franchise à la charge du Locataire.</h3>\n</li>\n<li>\n<h3 align=\"left\">Sécurité du conducteur.</h3>\n</li>\n<li>\n<h3 align=\"left\">Catastrophes naturelles après déduction de la franchise légale en vigueur.</h3>\n</li>\n</ul><p align=\"left\"><span>Au 1er Janvier 2010, la franchise s\'élève à 500 (cinq cents) euros.</span></p>\n<p align=\"left\"><span>En cas de sinistre responsable ou sans possibilité de recours contre un tiers identifié le Locataire sera alors responsable à concurrence de la franchise. Cette franchise sera également applicable sur les dommages occasionnés à des tiers même en l\'absence de dégât sur le véhicule loué.</span></p>\n<p align=\"left\"><span>Le bris de glaces, des optiques de phares et des rétroviseurs restent à la charge du locataire</span></p>\n<ul><li>\n<p align=\"left\">Responsabilité civile selon les dispositions légales</p>\n</li>\n<li>\n<p align=\"left\">Dommages tous accidents (\" tous risques \") : il sera fait application d\'une franchise restant à la charge du Locataire et pouvant être majorée des pénalités du présent contrat (cf article 12), excepté si la responsabilité de l\'accident est imputable à un tiers identifié.</p>\n</li>\n<li>\n<p align=\"left\">Incendie, vol du véhicule avec une franchise à la charge du Locataire</p>\n</li>\n<li>\n<p align=\"left\">Sécurité du conducteur selon le barème du droit commun</p>\n</li>\n<li>\n<p align=\"left\">Catastrophes naturelles après déduction de la franchise en vigueur Au 1er janvier 2010, la franchise s\'élève à 500 (cinq cents) euros.</p>\n</li>\n</ul><h3 class=\"western\" align=\"left\"><span>a) Responsabilité du Locataire</span></h3>\n<p align=\"left\"><span>Le Locataire est responsable de la pleine valeur de tout dommage causé à un véhicule non couvert par la police d\'assurance du Loueur ou par la garantie du fabricant du véhicule qui survient durant la période où il utilise ledit véhicule, de tout dommage attribuable à un animal, et de toute souillure nécessitant un nettoyage particulier. Sans égard à la franchise qu\'il peut avoir souscrit, le Locataire est responsable des dommages causés par la perte de la clé du véhicule ou pour tout autre dommage causé par sa faute et qui n\'est pas couvert par la police d\'assurance du Loueur ou par la garantie du fabricant du véhicule, notamment s\'il:</span></p>\n<ul><li>\n<p align=\"left\">utilise un véhicule à des fins interdites</p>\n</li>\n<li>\n<p align=\"left\">déroge à tout critère ou à toute condition du présent contrat, notamment s\'il omet de recueillir les informations requises ou de collaborer entièrement à la suite d\'un accident et que cette négligence entraîne des frais additionnels pour le Loueur</p>\n</li>\n<li>\n<p align=\"left\">utilise un véhicule de manière négligente, noie le moteur lors du démarrage ou omet de respecter les instructions contenues dans le manuel du propriétaire</p>\n</li>\n<li>\n<p align=\"left\">néglige de retirer la clé du véhicule ou de vérifier que le véhicule est fermé et que tous les ouvrants sont verrouillés (les portières, les glaces, le coffre, etc.)</p>\n</li>\n<li>\n<p align=\"left\">néglige d\'éteindre certains accessoires au retour du véhicule (phares, essuie-glaces, etc...)</p>\n</li>\n<li>\n<p align=\"left\">omet d\'aviser le Loueur du vol, du vandalisme ou des dommages causés à un véhicule du Loueur ou de tout accident dans un délai maximum de 24 (vingt-quatre) heures.</p>\n</li>\n<li>\n<p align=\"left\">communique de fausses informations au Loueur</p>\n</li>\n</ul><h3 class=\"western\" align=\"left\"><span>b) Déplacements à l\'étranger</span></h3>\n<p align=\"left\"><span>Le Locataire ne peut conduire ou utiliser les véhicules du Loueur hors du territoire Français métropolitain (ni en Corse, ni dans les départements et territoires d\'outre mer).</span></p>\n<h3 class=\"western\" align=\"left\"><span>c) Infractions au Code de la sécurité routière</span></h3>\n<p align=\"left\"><span>Le locataire est responsable des contraventions reçues pendant la période d\'utilisation d\'un véhicule. Le locataire doit signaler au Loueur, dès que possible, toute contravention qui ne peut être réglée dans les délais impartis (cas de recours, procédure judiciaire, etc.). Le locataire est tenu de signaler la présence d\'une contravention trouvée sur le véhicule lors de la prise de possession de celui-ci. Dans le cas contraire, le locataire concerné est responsable des frais encourus par le Loueur pour son omission.<br />À la fin de sa période de réservation, le locataire ne peut abandonner le véhicule du Loueur dans une zone comportant des restrictions de stationnement. Si le locataire abandonne un véhicule dans une zone comportant de telles restrictions, il doit en aviser le Loueur. À défaut, celui-ci reste responsable des frais encourus par le Loueur pour toute contravention ou remorquage occasionné par le défaut du locataire.</span></p>', 'assurance-et-franchises');
INSERT INTO `ap_cms_lang` (`id_cms`, `id_lang`, `id_shop`, `meta_title`, `meta_description`, `meta_keywords`, `content`, `link_rewrite`) VALUES
(8, 1, 1, 'Conditions Générales de Location', '', '', '<p></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">Auto-Permis – location de voiture à double commande pour particulier.</span></b></span></span></span></span></strong></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">Conditions Générales de</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">Location</span></b></span></span></span></span></strong></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">1.Notre accord :    </span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> Vous acceptez les modalités du contrat de location (Contrat). Lisez attentivement ce Contrat et dans le cas où vous avez des questions, n’hésitez pas à contacter. Nous sommes, avec vous, les seules parties au présent Contrat et il vous incombe de respecter l’ensemble des termes du présent Contrat même dans le cas où c’est une autre personne morale ou physique (telle une compagnie d’assurance) qui a organisé la location, négocié certains termes ou qui paie tout ou partie de la facture de location. Nous vous assurons que notre véhicule (Véhicule) est apte à prendre la route et aux normes des examens du permis de conduire et prêt à être loué dès le début de la période de location. Le présent Contrat constitue la totalité du Contrat entre vous et nous concernant la location du Véhicule et il ne peut être modifié sauf accord écrit contraire et à moins qu’il ne soit signé en notre nom et en votre nom.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">2.Période de location : </span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Nous convenons que vous avez le droit de louer le véhicule librement du lundi au dimanche mais jamais plus de 2 heures consécutives, sauf le forfait Examen qui vous laisse l\'usage du véhicule une demi journée, soit 4 heures.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Ainsi que les forfaits week-end 10, 20 ou 50 heures. Le véhicule devra être restitué aux horaires fixés par les forfaits concernés. Les forfaits week-end ne pourront pas être prolongés.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"background:transparent;\"> </span></span></strong></p>\n<ul><li>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:transparent;border:none;padding:0cm;font-style:normal;font-weight:normal;line-height:.69cm;\"><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\">Nos tarifs peuvent être obtenus sur le site (ainsi que de tous les autres montants facturables mentionnés dans le présent Contrat) pour les jours ou demi-journées, où vous avez loué le Véhicule. Nos tarifs en vigueur peuvent être supérieurs aux tarifs sur lesquels vous vous êtes initialement entendu(e) avec nous. De même, le montant journalier moyen des autres frais applicables (tels les produits optionnels) pendant la période de location réduite peut s’avérer supérieur. Vous ne pourrez plus non plus bénéficier d’une « offre spéciale » sur nos tarifs journaliers (par exemple les tarifs journaliers week-end qui ne sont proposés que si le Véhicule est loué pendant une durée minimum spécifiée).</span></span></span></p>\n</li>\n</ul><p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><span style=\"color:#666666;\"> <span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Vous pouvez choisir de restituer le véhicule pendant nos heures d’ouverture normales avant la date de restitution convenue en page 1 et ainsi résilier le contrat de façon anticipée. Si vous avez payé par avance les frais de locations afin de pouvoir bénéficier d’un tarif « offre spéciale », vous acceptez qu’aucun remboursement ne soit effectué en cas de résiliation anticipée. Autrement, toute modification de la date de restitution aura une incidence sur les frais mentionnés dans le Paragraphe 5, mais sauf mention expresse contraire ou obligation tacite, la résiliation anticipée du Contrat ne saura affecter les droits et obligations respectifs des parties dans le cadre du présent Contrat. Aucune des mesures administratives que nous prendrions en raison d’une prolongation de la période de location (et notamment aucune modification de nos dossiers, procédures de facturation, numéros de référence ou dates des documents) n’affectera vos obligations envers nous en application des modalités du contrat. </span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">3.Vos obligations : </span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Le Véhicule et les clés sont sous votre responsabilité. Vous devez toujours fermer le Véhicule à clé et le sécuriser, notamment en fermant toutes les ouvertures, après avoir quitté le véhicule. Le loueur décline toute responsabilité dans le cas d’oublis, pertes ou vols d’objets personnels dans le véhicule. Vous ou une autre personne ne devez effectuer aucune intervention sur notre Véhicule sans notre autorisation. Si nous vous y autorisons, nous ne vous rembourserons que si vous êtes en mesure de présenter une facture pour les interventions. Vous devez examiner le Véhicule avant d’en prendre possession. Vous devez cesser d’utiliser le Véhicule le plus rapidement possible et nous contacter dès que vous constatez un problème sur le Véhicule. Sauf accord contraire, vous devez nous restituer le Véhicule à la date et à l’heure indiquées en page 1. Un membre de notre personnel en uniforme doit voir le Véhicule afin de vérifier qu’il est en bon état. Si nous avons accepté de vous donner la possibilité de restituer le Véhicule en dehors des heures ouvrables, vous restez responsable du Véhicule et de son état jusqu’à ce qu’il soit de nouveau examiné par un membre de notre personnel. Vous devez vérifier que vous n’avez pas laissé d’effets personnels dans le véhicule avant de nous le restituer. En signant la déclaration de responsabilité de la page 1, vous reconnaissez que vous êtes responsable en tant qu’utilisateur du Véhicule. de tout délit, pénalité ou amende, vous incombant légalement, pour stationnement illégal, non-respect d’une voie réservée aux bus, péages ou infractions au code de la route ou contraventions dans un pays où le Véhicule est utilisé jusqu’à sa restitution.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">4.Utilisation du Véhicule : </span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Le véhicule doit être utilisé, dans des conditions normales d’utilisation du véhicule, avec toutes les précautions nécessaires et en bon père de famille. Le Véhicule ne doit pas être utilisé : </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">a</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">. par quelqu’un d’autre que vous, l\'accompagnant formateur et l\'apprenti conducteur tout deux déclarés sur le contrat de location.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><span style=\"color:#666666;\"> <span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">; </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">b</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">. par une personne qui n’a pas de permis de conduire valable pour la catégorie ou l’utilisation du véhicule loué. </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">c.</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> pour être reloué ou pour transporter des personnes à titre onéreux ; </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">d</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">. à des fins illégales ; </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">e</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">.pour faire des courses automobiles, des compétitions et des rallyes, tester la fiabilité et la vitesse du Véhicule  </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">f</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">.alors que vous êtes sous l’influence de l’alcool ou de drogues ; </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">g E</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">n dehors des personnes déclarées sur le contrat de location : l\'accompagant formateur et de l\'apprenti conducteur déclarés il est interdit de transporter toute autre personne, enfants ou animaux ; </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">h</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">. pour pousser ou remorquer un autre véhicule ou une remorque ; </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">i.</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">ailleurs que sur une autoroute, une route ou une voie privée, asphaltées ; </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">j</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">.   pour transporter des substances qui sont susceptibles de détériorer le véhicule ou d’entraîner des risques excessifs pour le véhicule,  </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">k</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">.   de manière imprudente ou négligente ; </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">l.</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">dans toute partie d’un aérodrome, terrain d’aviation, aéroport ou installation militaire prévue pour le décollage, l’atterrissage, le déplacement ou le stationne ment d’appareils et d’engins aériens, y compris les routes de service, aires de ravitaillement en carburant, aires de stationnement de matériel au sol, aires de stationnement, zones de maintenance et hangars associés.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">5.Prix :</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> Pour toutes les prestations facturées dont le prix est indiqué« /heure ou par jour » en page 1 : S’il est indiqué en page 1 heure = . Tous les prix sont valables pour un minimum d’1 heure. Pour toutes les durées de location indiquées \"forfait examen ou forfaits week-end\" en page 1  S’il est indiqué en page 1 jours consécutifs à compter du début de la location. .Les prix sont calculés en fonction du forfait choisi pour une période de validité propre au forfait choisi.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><span style=\"color:#666666;\"> <span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Autres obligations – Vous, l\'accompagnant formateur êtes redevable de: La totalité des amendes et des frais de justice, qui vous incombent légalement, relatifs au stationnement illégal, au non-respect d’une voie réservée aux bus, aux péages, aux infractions au code de la route ou autres contraventions liées au véhicule, à vous-même page 1 ou à un autre conducteur que vous avez autorisé à utiliser sans notre accord le Véhicule jusqu’à la restitution du Véhicule ; l\'apprenti conducteur ne peurt être tenu responsable des infractions au code de la route. Des frais de gestion raisonnables pour le traitement de toutes amendes ou contraventions liées au Véhicule, qui vous incombent (l\'accompagnant formateur) légalement pendant la période de location; Nos frais, y compris les frais juridiques raisonnables, alloués par les tribunaux en cas de jugement rendu en notre faveur, engagés pour collecter les sommes dont vous nous êtes redevable dans le cadre du présent Contrat ; Des frais d’abandon raisonnables pouvant aller jusqu’à 500 € si le Véhicule n’est pas restitué au lieu de location d’origine indiqué en page 1 ou dans une autre lieu pour lequelle nous aurions donné notre accord préalable ; Des frais de nettoyage pouvant aller jusqu’à 200 € si vous ne restituez pas le véhicule dans un état raisonnable résultant d’un usage normal, et correspondant aux coûts additionnels que nous aurons raisonnablement engagé par votre faute ; Les frais de récupération appliqués dans le cas où le dommage ou la panne du Véhicule résulte d’une erreur humaine et n’est pas couvert par le programme de dépannage d’un constructeur ; Le coût du remplissage du niveau de carburant à son niveau de départ au moment de la location dans l’hypothèse où le Véhicule serait restitué avec moins de carburant qu’au moment de la location. Le carburant sera facturé au prix en vigueur majoré de frais raisonnables pour le service carburant effectué par nos soins. Certains forfaits comprennent le carburant fourni. En cas de dommage, de perte ou de vol du Véhicule ou d’une pièce ou d’un accessoire, quelle qu’en soit la cause, et pour lequel vous êtes légalement responsable, vous serez redevable de : La valeur marchande de la réparation ou du remplacement du Véhicule ou d’une pièce ou d’un accessoire ou encore une valeur de réparation calculée d’après nos tarifs normaux pour les réparations mineures, Des frais de gestion raisonnables pouvant aller jusqu’à 150 € , Le manque à gagner au tarif journalier indiqué en page 1 (ou si aucun tarif n’est n’indiqué en page 1, au tarif de 45 € par jour) calculé d’après notre perte de revenus sur le Véhicule, pendant 30 jours maximum, sous réserve que nous ne soyons pas, de ce fait, indemnisés deux fois pour la même perte ; Une somme raisonnable pour la diminution de valeur du véhicule dont le montant sera déterminée par un expert automobile indépendant, Les frais de remorquage, d’entreposage et de fourrière. Nous avons le droit et la responsabilité exclusive de faire réparer le Véhicule et, à moins que vous n’ayez déjà réglé nos frais de réparation convenus, nous tenterons de réparer le Véhicule et de traiter le sinistre en temps voulu. Votre responsabilité en cas de dommage, de perte ou de vol du Véhicule peut être réduite par l’achat d’une couverture dommage (« domage collision ») (voir le Paragraphe 8). Vous paierez la taxe sur la valeur ajoutée et toutes les autres taxes dues (le cas échéant) vous incombant légalement sur les montants répertoriés dans le présent Paragraphe 5. Vous êtes redevable de l’ensemble des sommes dues, même si vous avez demandé à quelqu’un d’autre de les prendre en charge ou si nous avons facturé un tiers. Vous convenez que nous calculerons et débiterons le prix final sur votre carte de crédit et/ou de débit si telle est la forme d’acompte ou de caution utilisée et indiquée en page 1. Tous les montants facturés sont sous réserve de contrôle final. Avant de débiter sur votre carte de crédit et/ou débit des frais qui seraient finalisés ou apparaîtraient après la fin du Contrat, nous mettrons en œuvre des moyens raisonnables pour vous en informer. En application de l’article L 441-6 du Code de Commerce, dans l’hypothèse où vous seriez un professionnel, des pénalités de retard vous seront appliquées en cas de retard de paiement, d’un montant égal à trois fois et demi le taux d’intérêt légal en vigueur à compter de la date à laquelle le paiement était dû, ainsi que d’une indemnité forfaitaire de 40 € (applicable uniquement pour les professionnels) . Vous êtes redevable de dommages résultant d’une mauvaise appréciation du gabarit du véhicule loué ( sous caisse – pavillon) ainsi que tous dommages à l’intérieur du véhicule, aux pneus et aux jantes et ce même si la suppression de franchise est souscrite. Dans le cadre de la perte de la clé du véhicule ou de sa détérioration, une somme forfaitaire de 300.00 € correspondant à son remplacement et à l’immobilisation du véhicule vous sera facturé.)</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">6.Responsabilité envers les tiers :</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> Le locataire (l\'accompagnant formateur) de 23 ans ou plus est conidéré comme le conducteur sur le contrat de location original transporter des personnes où sont couverts par la protection de notre police d’assurance pour les dommages causés aux tiers. Cela inclut les réclamations des tiers sur les décès, préjudices corporels, matériels conformément à la réglementation en vigueur concernant l’assurance automobile obligatoire dans les pays où le Véhicule est autorisé à être utilisé. Le locataire et /ou le(s) conducteur(s) supplémentaire(s) autorisés par le Loueur seront lié(s) par les termes, conditions, limitations, exceptions et exclusions de notre police d’assurance. Vous trouverez une copie de celle-ci sur le site www.autopermis.re. La protection de notre police d’assurance telle que décrite ci-dessus ne saurait autoriser le locataire (accompagnant formateur) portés sur le contrat de location à manquer au respect des obligations prévues dans les conditions générales du présent Contrat. Notre assureur se réserve le droit d’exercer un recours contre le locataire ou le conducteur responsable de tout dommage causé aux tiers. Vous acceptez de coopérer et de nous assister, nous et nos assureurs, dans le cadre de l’enquête concernant tout recours d’un tiers et vous convenez que nous ou nos assureurs avons le droit exclusif de régler tout recours comme nous ou nos assureurs l’estiment nécessaire. </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">Vous convenez que le fait de ne pas signaler un sinistre dans le délai de 2 (deux) heures en utilisant le CONSTAT AMIABLE, le fait de ne pas coopérer ou de ne pas nous apporter votre assistance, toute fraude ou inexécution des modalités de notre police ou tout manquement au Paragraphe 4 invalidera la couverture fournie par notre police d’assurance de parc automobile. </span></b></span></span></span></span></strong></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">7.Responsabilité en cas de dommage et de vol :       </span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Sauf restriction, modification ou limitation prévue par la loi, vous acceptez d’être responsable en cas de dommage, de perte ou de vol du Véhicule, d’Accessoires en option ou de toute pièce ou de tout accessoire qui surviendrait au cours de la période de location, que ce soit à la suite d’une faute ou d’une négligence de votre part, de quelqu’un d’autre ou d’une catastrophe naturelle. Vous nous paierez le montant nécessaire pour réparer le Véhicule ou les Accessoires en option. Si nous obtenons le recouvrement d’un quelconque montant relatif à ces réparations, vous serez remboursés à hauteur dudit montant. Vous ne ferez réparer ni le Véhicule ni les Accessoires en option sans notre autorisation. Si le Véhicule est volé et n’est pas récupéré ou si nous estimons que le Véhicule est une épave, vous nous paierez la juste valeur marchande déduction faite de tout produit de la vente. Aux fins du présent Contrat, on entend par « juste valeur marchande » la valeur de revente du Véhicule juste avant la perte. Si les Accessoires en option ne sont pas restitués, vous nous paierez les frais de remplacement de ces Accessoires en option. Vous êtes redevable de l’ensemble des frais de remorquage, d’entreposage et de fourrière ainsi que des autres frais que nous engagerions pour récupérer le Véhicule et déterminer les dommages. Vous serez redevable, quelle que soit l’utilisation du parc, d’une somme pour perte de jouissance qui sera calculée de la façon suivante : (i) si nous estimons que le Véhicule est réparable : total des heures de main d’œuvre indiqué dans le devis de réparation divisé par 4 puis multiplié par le tarif journalier indiqué en page 1 ; (ii) si le Véhicule est perdu et n’est pas récupéré ou si nous estimons que le Véhicule est une épave : 15 jours au tarif journalier indiqué en page 1. Vous convenez également de payer : (a) des frais administratifs d’un montant de 60,00 € lorsque le devis de réparation est inférieur à 500,00 € , 100,00 € lorsque le devis de réparation est compris entre 500,00 € et 1 500,00 € et 150,00 € s’il est supérieur à 1 500,00 € ; (b) une somme au titre de la diminution de valeur, si le Véhicule est réparable, correspondant à 10 % du devis de réparation si les dommages s’élèvent à plus de 499,99 € . Si le Véhicule est restitué en dehors des heures d’ouverture ou dans un lieu autre que l’adresse de l’agence indiquée en page 1, tout dommage, perte ou vol du Véhicule ou d’Accessoires en option qui surviendrait avant qu’un de nos employés en uniforme n’enregistre le retour du Véhicule et n’inspecte celui-ci relève de votre responsabilité.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">8.Nos produits protection :</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> </span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><span style=\"color:#666666;\"> </span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">Votre responsabilité est et sera engagée concernant les bris de glace, les pneus, les clés perdues, les porte-clés, les transpondeurs, les Accessoires en option ou les dommages causés par l’utilisation de carburant inadapté, l’inexécution du Paragraphe 3, du Paragraphe 4 ou du Paragraphe 9 ou si vous ne prenez pas toutes les mesures raisonnables pour surveiller et sécuriser le Véhicule ou les clés ou d’un autre dispositif qui permet d’ouvrir le Véhicule et/ou de le faire démarrer. </span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">La franchise est limitée au montant indiqué au recto du présent contrat. Si le coût des dommages ( frais réels de réparation, frais d’expertise, frais de dossier fixés à 60.00 € TTC et frais d’immobilisation forfaitaires fixés à 40€00TTC) sont inférieurs à la franchise incompressible dommages ( respectivement de 1000.00 €, 1400.00 € ou de 2000.00 € selon les catégories et telle qu’elle figure en page 1 du contrat de location) seul ce montant sera facturé à chaque fois que le Véhicule est endommagé, volé ou perdu. , aucune franchise n’est due dès lors que les conditions générales de location figurant dans le paragraphe 3 «  Vos obligations » le paragraphe 4 «  utilisation du véhicule » et le paragraphe 9 sont respectées. Dans le cas d’un accident où votre responsabilité serait engagée totalement ou partiellement, ou de dommages occasionnés au véhicule sans qu’un tiers soit identifié et un constat amiable complété et permettant un recours, seuls les frais forfaitaires de dossiers fixés à 60.00 € TTC ainsi que les frais forfaitaires d’immobilisation de respectivement 60.00 € restent à la charge du locataire. </span></span></span></span></span><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">Vous convenez que la couverture ne vous exonère pas de votre responsabilité concernant les bris de glace, les pneus, les clés perdues, les porte-clés, les transpondeurs, les Accessoires en option et les dommages causés par l’utilisation de carburant inadapté ou l’inexécution du Paragraphe 3, du Paragraphe 4 ou du Paragraphe ou encore dans le cas où vous ne prenez pas toutes les mesures raisonnables pour surveiller et sécuriser le Véhicule ou les clés ou un autre dispositif qui permet d’ouvrir le Véhicule et/ou de le faire démarrer. </span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"text-decoration:underline;\"><span style=\"font-weight:normal;\"><span style=\"background:transparent;\">Protection pneus, bris de glace (PB)</span></span></span></span></span></span></span><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> : Si vous acceptez la protection PB en page 1, nous renoncerons à votre responsabilité concernant : – les frais de réparation et de remplacement des pneus (hors jantes). – les frais de réparation ou de remplacement des vitres et pare-brise.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">9.Que faire si le Véhicule est impliqué dans un accident, volé ou perdu :</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> Vous devez nous signaler tout accident, vol ou perte dans un délai maximum de 2 (deux) heures et nous le confirmer par écrit à l’aide du CONSTAT AMIABLE dans un délai maximum de 2 (deux) jours. Vous et le conducteur devez récupérer les noms et adresses de toutes les personnes impliquées, y compris les témoins, et nous les communiquer. Vous et le conducteur devez nous adresser tous les avis et autres documents relatifs à une procédure en justice résultant d’un accident, du vol ou de la perte du Véhicule. Vous convenez de coopérer avec nous et nos assureurs, notamment en répondant aux demandes d’informations exactes et complètes et en apportant votre assistance pour tout problème ou dans le cadre de toute procédure légale, y compris en nous autorisant à intenter une procédure en votre nom et en contestant en votre nom toute procédure intentée contre vous. Vous devez nous rendre les clés d’origine et signaler le vol ou la perte à la police dans les meilleurs délais possibles si le Véhicule est volé ou perdu.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">10.Protection des données :</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> Les informations contenues dans les conditions générales de location sont nécessaires à la bonne exécution du contrat de location. Elles peuvent faire l’objet d’un traitement automatisé à des fins de marketing, afin de faciliter nos relations commerciales, d’analyser des données, de fournir et d’améliorer nos services, de facturer nos produits et services. Ces informations sont destinées à Enterprise Auto-Permis, qui peut les communiquer à des fins de gestion, prospection, d’enquête, de sondage ou de statistiques à la société Enterprise Holding Inc., ses filiales et autres entités du Groupe de sociétés et ses prestataires situés aux États-Unis ayant adhéré aux principes du SafeHarbor, assurant ainsi que le transfert s’opère sous des garanties reconnues comme permettant de bénéficier d’un niveau de protection adéquat tel que prévu par la législation française et européenne relative à la protection des données personnelles. Enterprise Auto-Permis peut toutefois communiquer des informations sur les locataires à des sociétés externes à des fins de prospection commerciale ou pour la réalisation de sondages. Le Véhicule est équipé d’un système de localisation géographique activé en permanence afin de vous garantir une intervention rapide et en cas de non restitution ou de vol du Véhicule. Les locataires disposent d’un droit d’accès et de rectification de leurs données personnelles et du droit de s’opposer, sous réserve de motifs légitimes, au traitement de leurs données. </span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">11.Fin du Contrat : </span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Vous pouvez restituer le véhicule et résilier le présent contrat à tout moment de la période de location conformément aux termes du Paragraphe 2. Nous pouvons mettre fin au présent Contrat sur le champ sur simple avis écrit d’un manquement substantiel au présent Contrat. Par souci de clarté, il est précisé que le non-respect des paragraphes 3, 4 ou 5 est un « manquement substantiel » de votre part. À la résiliation du présent Contrat, si vous ne nous restituez pas le Véhicule dans les meilleurs délais, nous pouvons en reprendre possession et vous serez alors redevable des frais raisonnable que nous avons engagés pour en reprendre possession. La résiliation du présent Contrat ne saura compromettre les droits et recours acquis par les parties jusqu’à la résiliation et le maintien en vigueur de tout terme expressément désigné comme restant en vigueur ou restant tacitement en vigueur après la résiliation n’est sera pas affecté.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">12.Droit du Contrat :</span></b></span></span></span></span></strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\"> </span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Le présent Contrat est régi par les lois françaises et tout litige peut être résolu par les Tribunaux français. Dans le présent Paragraphe 12, on entend par « litige » les litiges contractuels et non contractuels.   Toute loi ou disposition légale mentionnée dans le présent Contrat renvoie à sa version amendée, étendue ou remise en vigueur à tout moment et englobe toute législation secondaire adoptée à tout moment en application de ladite loi ou disposition légale.</span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><strong><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><b><span style=\"background:transparent;\">13.Annulation :</span></b></span></span></span></span></strong></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;font-style:normal;font-weight:normal;\"><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\">Toute annulation doit être effectuée sur le site www.autopermis.re une, confirmation de cette annulation sera envoyée au client.</span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Le client peut annuler sa réservation au moins 48 heures avant le l\'heure réservée prévue de la location. A moins de 48 heures avant la réservation il sera impossible d\'annuler l\'heure sur l\'agenda du site et celle dernière sera due dans sa totalité. </span></span></span></span></span></p>\n<p align=\"justify\" style=\"margin-bottom:0cm;background:#f5f5f5;border:none;padding:0cm;\"><span style=\"color:#666666;\"><span style=\"font-family:marvelregular;\"><span style=\"font-size:medium;\"><span style=\"font-style:normal;\"><span style=\"font-weight:normal;\">Avant ses 48 heures, l\'annulation de la réservation peut se faire sans conséquence et directement sur l\'agenda du site. </span></span></span></span></span></p>\n<p></p>', 'conditions-generales-de-location'),
(9, 1, 1, 'charte de l\'accompagnant / formateur', '', '', '<p></p>\n<p><img src=\"http://www.autopermis.re/img/cms/CHARTE%20DE%20L%20ACCOMPAGNATEUR%20ET%20TEXTE%20DE%20LOI%20A%20FAIRE%20SIGNER-1.png\" alt=\"\" width=\"900\" height=\"1273\" /></p>\n<p><img src=\"http://www.autopermis.re/img/cms/CHARTE%20DE%20L%20ACCOMPAGNATEUR%20ET%20TEXTE%20DE%20LOI%20A%20FAIRE%20SIGNER-2.png\" alt=\"\" width=\"900\" height=\"1273\" /></p>\n<p></p>', 'charte-de-l-accompagnant-formateur'),
(10, 1, 1, 'Géolocalisation', '', '', '<h3>- Pour votre sécurité,</h3>\n<h3>- Pour une intervention rapide en cas d\'incident ou d\'accident,</h3>\n<h3>- en cas de vol ou de non retour du véhicule</h3>\n<h3></h3>\n<h2><span style=\"text-decoration:underline;\">Nos véhicules sont géolocalisés.</span></h2>', 'geolocalisation');
INSERT INTO `ap_cms_lang` (`id_cms`, `id_lang`, `id_shop`, `meta_title`, `meta_description`, `meta_keywords`, `content`, `link_rewrite`) VALUES
(11, 1, 1, 'Candidat', '', '', '<h2><span style=\"color:#8f28fe;\">Le cas du candidat libre depuis la Loi Macron</span></h2>\n<h2>Le permis de conduire est un service universel, et tous les candidats doivent pouvoir disposer des mêmes conditions d’accès. Depuis plusieurs années, il n’est<strong> plus obligatoire de passer par une auto-école</strong> pour passer son permis de conduire. Comme pour le baccalauréat, les aspirants conducteurs peuvent se présenter à l\'épreuve du permis en candidat libre.</h2>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><span>Cet apprentissage est en moyenne </span></span></span></span></span><span style=\"color:#8f28fe;\"><strong><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><b>30%</b></span></span></strong></span><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><span> moins cher qu’une formation réalisée en auto-école.</span></span></span></span></span></h5>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Le permis en candidat libre est une véritable solution pour les petits budgets. </span></span></span></h5>\n<h5><strong><span style=\"color:#000000;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><b>L’inscription</b></span></span></span></span></strong><strong><span style=\"color:#bd321f;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><b> </b></span></span></span></span></strong><strong><span style=\"color:#000000;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><b>est gratuite. Toutefois, dans certaines régions, une taxe existe sur le permis de conduire.</b></span></span></span></span></strong></h5>\n<h5 class=\"western\"><span style=\"color:#8f28fe;\"> <span style=\"font-family:\'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;\"><span style=\"font-size:xx-large;\">Pour vous i</span></span><span style=\"font-family:\'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;\"><span style=\"font-size:xx-large;\">nscrire :</span></span></span></h5>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><span>Pour pouvoir vous présenter aux épreuves du permis de conduire vous devez disposer d\'un</span></span></span></span></span><strong><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><span> </span></span></span></span></span></strong><strong><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><b>numéro <span style=\"color:#8f28fe;\">NEPH</span></b></span></span></span></span></strong><span style=\"color:#8f28fe;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"> (</span></span></span><span style=\"color:#565867;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><span><span style=\"color:#8f28fe;\">Numéro d\'Enregistrement Préfectoral Harmonisé)</span>, il s\'agit de votre numéro de candidat. La demande se fait en ligne sur le site de <span style=\"color:#8f28fe;\">l</span></span></span></span></span></span><span style=\"color:#8f28fe;\"><strong><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><b>\'ANTS</b></span></span></strong></span><span style=\"color:#565867;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><span><span style=\"color:#8f28fe;\"> (agence nationale des titres sécurisés)</span>.</span></span></span></span></span></h5>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Pour réaliser votre demande, rendez-vous sur le site de l\'ANTS en cliquant sur ce lien: <span style=\"color:#8f28fe;\">https://permisdeconduire.ants.gouv.fr/Services-associes/Effectuer-une-demande-de-permis-de-conduire-en-ligne</span></span></span></span></h5>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><span>Cliquez ensuite sur <span style=\"color:#2c9ffd;\">\"</span></span></span></span></span></span><strong><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><b><span style=\"color:#2c9ffd;\">je fais ma demande en ligne\"</span>,</b></span></span></span></span></strong><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span><span> créez votre compte.</span></span></span></span></span></h5>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Une fois votre compte ANTS crée, vous devez compléter le formulaire d\'inscription en 7 étapes: </span></span></span></h5>\n<ul><li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Choisir le motif de la demande (1)</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Renseigner l\'état civil (2)</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Renseigner l\'adresse, téléphone, mail (3)</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Saisir le code e-photo (4)</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Joindre les pièces justificatives*(5)</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Vérifier les informations saisies (6)</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Valider la demande (7)</span></span></span></h5>\n</li>\n</ul><h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">* Les pièces justificatives sont obligatoires pour que votre demande puisse être validée par l\'administration. On vous demandera de founir les pièces suivantes (liste non exhaustive):</span></span></span></h5>\n<ul><li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">un justificatif d\'identité,</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">un justificatif de domicile,</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">une photo numérique d\'identité</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">si vous êtes français et âgé de moins de 25 ans, le certificat individuel de participation à la journée défense et citoyenneté (JDC) ou l\'attestation provisoire en instance de convocation à la JDC ou l\'attestation individuelle d\'exemption,</span></span></span></h5>\n</li>\n<li>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">si vous êtes étranger, un justificatif de régularité du séjour ou si vous êtes dispensé de détenir un titre de séjour, une preuve de votre présence en France depuis au moins 6 mois (feuille de paie, quittance de loyer...).</span></span></span></h5>\n</li>\n</ul><h5><span style=\"color:#0a0a0a;\"> </span></h5>\n<h5><span style=\"color:#000000;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\">Une fois votre demande complétée et transmise à la préfecture, vous receverez votre numéro NEPH  par mail sous 3 à 4 semaines environ. Vous pourrez également télécharger depuis votre compte ANTS votre \"cerfa numérique\" sur lequel apparait votre numéro NEPH, votre photo et signature.</span></span></span></h5>\n<h5><span style=\"color:#0a0a0a;\"><span style=\"font-family:Montserrat, sans-serif;\"><span style=\"font-size:medium;\"><span style=\"color:#000000;\">Vous pouvez joindre un conseiller ANTS au</span> 0810 90 041.</span></span></span></h5>\n<h4 class=\"western\"><span style=\"color:#0a0a0a;\"> </span></h4>\n<h2><span style=\"color:#2c9ffd;\">VOUS AVEZ OBTENU VOTRE CODE DE LA ROUTE.</span></h2>\n<div>\n<div class=\"Content-gQTiGuK_LbC93ZzJGF-8u\">\n<h3><span style=\"color:#2c9ffd;\">Le permis de conduire en candidat libre</span></h3>\n<p>L\'épreuve de conduite reste publique, car elle reste organisée par les services de la Préfecture des différents départements dont dépendent les inspecteurs du permis de conduire. Les modalités d\'inscription à l\'examen du permis de conduire  sont les suivantes :</p>\n<ol><li>envoyer un dossier d\'inscription à la préfecture</li>\n<li>apprendre à conduire pendant que la préfecture traite le dossier du candidat</li>\n<li>recevoir une convocation à l\'examen directement chez soi 3 semaines avant la date de l\'examen.</li>\n<li>\n<div class=\"Container-1UqTp-95VKx05XHwXfwFTr Title-3ejQml1KMJOWTpFttVuAFt\">\n<div class=\"Content-una4U27L41Wj1vtLcFnsN\">\n<h1 class=\"styles-types_Reset__1NrGf styles-variants_black-bold__2iw4R styles-types_base-header1__2n9uP styles-types_medium-header1__3_0k9 Heading-15rNLX1tiO5JC_ckXkD6TC\"><span style=\"color:#2c9ffd;\">Se présenter à l\'épreuve du permis de conduire en tant que candidat libre</span></h1>\n</div>\n</div>\n<div class=\"Content-eKGts8giK6mBURhz3Ta-S\">\n<div class=\"Content-gQTiGuK_LbC93ZzJGF-8u\">\n<p>Comme pour de nombreux autres examens en France, il est tout à fait possible de passer son <a href=\"https://www.ornikar.com/permis\">permis de conduire</a> en candidat libre. Cette méthode de formation permet de se présenter aux épreuves du permis de conduire sans contrainte et en réalisant des économies.</p>\n<p></p>\n<h2><span style=\"color:#8f28fe;\">L\'épreuve pratique du permis de conduire</span></h2>\n<h3><span style=\"color:#8f28fe;\">S’inscrire à l’examen pratique en candidat libre</span></h3>\n<p>Après l’obtention de l’épreuve théorique, le candidat a 5 ans pour se présenter à pratique du permis de conduire. Contrairement à l\'épreuve théorique, l\'épreuve pratique n\'a pas été externalisée, c\'est donc toujours l\'État qui se charge de l\'<strong>organisation des épreuves</strong> et de la <strong>répartition des places d\'examen</strong>. Chaque préfecture gère ses places d\'examen et les distribue aux auto-écoles de son département et aux candidats libres.</p>\n<p>Comme pour l’épreuve théorique, le candidat doit envoyer une demande de place d\'examen comprenant son <strong>résultat favorable au code de la route</strong>.</p>\n<p><span>Une fois le dossier transmis à la préfecture, le candidat doit compter </span><strong>environ 2 mois </strong><span>pour une première présentation avant de recevoir sa convocation.  Les candidats peuvent alors se concentrer pleinement sur l’apprentissage de la conduite.</span></p>\n<h3>Apprendre la conduite sur un véhicule à double commande</h3>\n<p>Avec Auto-Permis les candidats libres peuvent se former sur de vrais véhicules à double commande, <strong>conformes à l’épreuve pratique</strong> du permis de conduire,<strong> assurés</strong> pendant toute la durée de la formation et le jour de l’épreuve.</p>\n<p>Le véhicule d’apprentissage est un véhicule équipé de double commande. Ce véhicule doit suivre plusieurs normes spécifiques, totalement adaptées à l’apprentissage de la conduite. Il est possible de trouver ce type de véhicules chez Auto-Permis à  Saint Pierre 97410. </p>\n</div>\n</div>\n</li>\n</ol></div>\n</div>\n<div>\n<div class=\"Container-3FCMBWZR6GVzK26pyqKnzz\">\n<div class=\"Content-2fqkCSin_UoS9saXZyQfAY\">\n<div class=\"styles-types_Reset__1NrGf styles-variants_black-bold__2iw4R styles-types_base-header4__2sF0j styles-types_medium-header4__37twH Title-1XG-Sfm31gOn3rro912Fo2\">\n<div>\n<div>Permis de conduire</div>\n<div>35% moins cher !</div>\n</div>\n</div>\n<div class=\"MediumButtonContainer-13pGL76yNjaoteEF3LYeSO\"></div>\n</div>\n</div>\n</div>\n<div>\n<div class=\"Content-gQTiGuK_LbC93ZzJGF-8u\">\n<h2><span style=\"color:#8f28fe;\">Les avantages du candidat libre</span></h2>\n<h3><span style=\"color:#8f28fe;\">L\'inscription à l\'épreuve</span></h3>\n<p><strong>L\'inscription en candidat libre est gratuite et individuelle</strong>. Le principal avantage tient au fait que les candidats ne dépendent pas du système d\'attribution des places d\'examen aux auto-écoles. En effet, les auto-écoles traditionnelles ne peuvent proposer qu\'un nombre limité de places à l\'examen chaque mois, ce qui pose malheureusement beaucoup de problèmes pour obtenir rapidement une place d\'examen.</p>\n<p><span style=\"text-decoration:underline;\"><strong><span style=\"color:#aa7ed8;text-decoration:underline;\">ATTENTION :</span></strong></span></p>\n<p><span style=\"text-decoration:underline;\"><strong><span style=\"color:#aa7ed8;text-decoration:underline;\">PENSEZ A RESERVER VOTRE PLACE D\'EXAMEN PRATIQUE</span></strong></span>.</p>\n<p>AUTO-PERMIS ne réserve pas d\'examen. AUTO-PERMIS est une société de location de voiture à double commande destinées à l\'apprentissage de la conduite et le passage des examens pratique du permis.</p>\n<p>AUTO-PERMIS n\'est pas une auto-école, <span style=\"color:#9c68d3;\">vous êtes le seul a gérer votre dossier préfecture et vos places d\'examen.</span></p>\n</div>\n</div>', 'candidat'),
(12, 1, 1, 'Présentation', '', '', '<div class=\"container animated fadeIn\">\n<div class=\"jss1\">\n<div class=\"jss2 jss38 text-center\">\n<h1 style=\"text-align:center;\"></h1>\n<h1 style=\"text-align:center;\"><span>Présentation</span></h1>\n<h1 style=\"text-align:center;\"><span>LOUER UN VÉHICULE À DOUBLE COMMANDE POUR QUI, ET DANS QUEL CAS ?</span></h1>\n</div>\n<div class=\"jss2 jss38\" style=\"text-align:center;\">\n<h2>Augmentez considérablement vos chances d\'obtenir votre permis rapidement et à moindre coût</h2>\n<p>AutoPermis.re vous donne la possibilité d\'augmenter vos chances de réussite à l\'examen et de compléter votre formation auto école à moindre coût. Vous êtes inscrit en auto-école, et vous désirez perfectionner votre conduite? Accompagné d\'une personne, titulaire du permis de conduire depuis plus de 5 ans, <span>AutoPermis.re</span> met à votre disposition un véhicule équipé pour l\'apprentissage à la conduite. Afin de répondre à toutes les attentes, <span>AutoPermis.re</span> propose des forfaits adaptés.</p>\n<p><img src=\"http://www.autopermis.re/img/cms/ciel%203%201.jpg\" alt=\"\" width=\"311\" height=\"293\" /><img src=\"http://www.autopermis.re/img/cms/ciel%202.jpg\" alt=\"\" width=\"301\" height=\"260\" /><img src=\"http://www.autopermis.re/img/cms/accompagnant%20formateu%201.png\" alt=\"\" width=\"297\" height=\"245\" /><img src=\"http://www.autopermis.re/img/cms/ciel%202%201.jpg\" alt=\"\" width=\"329\" height=\"245\" /></p>\n</div>\n<div class=\"jss2 jss38\" style=\"text-align:center;\">\n<h2>Pourquoi choisir Auto-Permis.re ?</h2>\n<p>Le permis de conduire est aujourd\'hui indispensable mais son coût reste particulièrement élevé. Auto-Permis.re vous permet de réduire ce coût tout en vous présentant à l\'examen dans les conditions les plus favorables. En effet, 55 % des candidats échouent à la première présentation au permis de conduire car ils ne sont pas suffisamment préparés à l\'examen. En revanche, 80 % réussissent à la seconde présentation: ils ont effectué en moyenne 30 à 35 heures de conduite et parcouru 250 kms. Accompagné d\'un parent, grand parent, frère, ami ou tout conducteur expérimenté et pédagogue, vous pourrez conduire en toute tranquillité afin de gagner en expérience et en confiance le jour de l\'examen.DÉJÀ INSCRIT EN AUTO-ÉCOLE</p>\n</div>\n</div>\n</div>\n<div class=\"container\" id=\"step1\" style=\"text-align:center;\">\n<div class=\"jss1\">\n<h3 class=\"jss2 jss38\"><span>1) Vous avez une date d\'examen</span></h3>\n</div>\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<p>Louez nos véhicules auto-école afin de vous entrainer à toute heure à moindre coût. Vous pourrez vous entraîner sur votre lieu d\'examen tout en étant accompagné de la personne de votre choix. Le parcours de votre examen n\'aura plus aucun secret pour vous le jour J.</p>\n</div>\n</div>\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<p>La solution <span>Auto-Permis.re</span> permet :</p>\n- De réduire le coût excessif des heures supplémentaires de votre auto-école<br />- De faire des économies<br />- De renforcer votre apprentissage<br />- D\'augmenter la durée de conduite sur votre lieu d\'examen<br />D\'augmenter votre confiance</div>\n<div class=\"jss2 jss38\"></div>\n</div>\n<div class=\"jss1\">\n<h3 class=\"jss2 jss38\"><span>2) Vous n\'avez pas encore de date d\'examen</span></h3>\n</div>\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<p>Vous avez effectué les 20h obligatoires en auto-école avec un professionnel et vous souhaitez perfectionner votre conduite à petit prix pour obtenir une date d\'examen. Louez nos véhicules école afin de compléter votre formation en auto-école, vous pourrez alors conduire quand vous le souhaitez et revoir tous les points que vous avez traités lors de apprentissage dans votre auto-école.</p>\n</div>\n</div>\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<p><strong>La location de nos véhicules à double commande permet:</strong></p>\n1. De mieux assimiler les objectifs non atteints lors de votre apprentissage dans votre auto-école<br />2. De faire des économies considérables<br />3. De vous faire gagner de la confiance et de l\'assurance</div>\n<div class=\"jss2 jss38\"></div>\n<div class=\"jss2 jss38\"></div>\n</div>\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<table style=\"margin-left:auto;margin-right:auto;\"><tbody><tr><th></th><th><span>Obligations</span></th><th><span>Documents à fournir</span></th></tr><tr><td><span>Apprenti(e)</span></td>\n<td>Avoir effectué 20 h en auto-école</td>\n<td>Copie des 2 premières pages de votre livret d\'apprentissage</td>\n</tr><tr><td><span>Accompagnateur</span></td>\n<td>Être titulaire du permis depuis plus de 5 ans</td>\n<td>Copie de votre permis de conduire CANDIDAT LIBRE</td>\n</tr></tbody></table><p></p>\n</div>\n</div>\n</div>\n<div class=\"container\" id=\"step2\" style=\"text-align:center;\">\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<p>Vous avez déposé un dossier à la préfecture afin de passer votre permis de conduire en candidat libre et vous avez besoin d\'un véhicule afin de passer votre examen. <span>AutoPermis.re</span> met à votre disposition des véhicules à double commande afin que vous puissiez passer votre examen du permis de conduire en conformité avec le code de la route et muni d\'un certificat d\'assurance.</p>\n<p></p>\n</div>\n</div>\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<table style=\"margin-left:auto;margin-right:auto;\"><tbody><tr><th></th><th><span>Obligations</span></th><th><span>Documents à fournir</span></th></tr><tr><td><span>Apprenti(e)</span></td>\n<td>Avoir effectué 20h en auto-école</td>\n<td>Copie des 2 premières pages de votre livret d\'apprentissage</td>\n</tr><tr><td><span>Accompagnateur</span></td>\n<td>Être titulaire du permis depuis plus de 5 ans</td>\n<td>Copie de votre permis de conduire </td>\n</tr></tbody></table><p></p>\n</div>\n</div>\n</div>\n<div class=\"container\" id=\"step3\" style=\"text-align:center;\">\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<p>Vous possédez déjà votre permis de conduire, mais vous n\'avez pas conduit depuis un certain temps et vous manquez de confiance en vous. <span>AutoPermis.re</span> vous permet de reprendre la main en étant accompagné de l\'un de vos proches. La location de nos véhicules écoles vous permettra de retrouver vos acquis. Cette solution est très avantageuse car vous reverrez les éléments que vous jugerez essentiels à votre réapprentissage à la conduite.</p>\n<p></p>\n</div>\n</div>\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<table style=\"height:51px;margin-left:auto;margin-right:auto;\" width=\"683\"><tbody><tr><th></th><th><span>Obligations</span></th><th><span>Documents à fournir</span></th></tr><tr><td><span>Apprenti(e)</span></td>\n<td>Être titulaire du permis</td>\n<td>Copie de votre permis de conduire</td>\n</tr><tr><td><span>Accompagnateur</span></td>\n<td>Être titulaire du permis depuis plus de 5 ans</td>\n<td>Copie de votre permis de conduire PERMIS</td>\n</tr></tbody></table><p></p>\n</div>\n</div>\n</div>\n<div class=\"container\" id=\"step4\">\n<div class=\"jss1\" style=\"text-align:center;\">\n<div class=\"jss2 jss38\">\n<p>Vous venez de valider la durée initiale de formation avec votre auto-école et vous vous apprêtez à conduire officiellement avec votre accompagnateur sur les voies publiques. Cette épreuve est souvent redoutée par de nombreux apprentis, car ils vont devoir conduire avec une personne autre que leur formateur.</p>\n</div>\n</div>\n<div class=\"jss1\" style=\"text-align:center;\">\n<div class=\"jss2 jss38\">\n<p>L\'initiation sur les véhicules <span>Auto-Permis.re</span> permet :</p>\n- De faire vos preuves devant votre accompagnateur<br />- De rassurer votre accompagnateur<br />- De mettre au clair certaines méthodes de conduite</div>\n<div class=\"jss2 jss38\"></div>\n</div>\n<div class=\"jss1\">\n<div class=\"jss2 jss38\">\n<table style=\"margin-left:auto;margin-right:auto;\"><tbody><tr style=\"text-align:center;\"><th></th><th><span>Obligations</span></th><th><span>Documents à fournir</span></th></tr><tr style=\"text-align:center;\"><td><span>Apprenti(e)</span></td>\n<td>Avoir effectué 20 h en auto-école</td>\n<td>Copie des 2 premières pages de votre livret d\'apprentissage</td>\n</tr><tr><td style=\"text-align:center;\"><span>Accompagnateur</span></td>\n<td style=\"text-align:center;\">Être titulaire du permis depuis plus de 5 ans</td>\n<td>Copie de votre permis de conduire</td>\n</tr></tbody></table></div>\n</div>\n</div>', 'presentation'),
(13, 1, 1, 'Véhicule', '', '', '<p style=\"text-align:center;\"><img src=\"http://www.autopermis.re/img/cms/opel-karl-cosmo.jpg\" alt=\"\" width=\"315\" height=\"210\" /></p>\n<p style=\"text-align:center;\">Nos véhicules sont équipés de double commande, double rétroviseur intérieur, double rétroviseur extérieur, commande individuelle du tableau de bord pour l\'accompagnant / formateur et un panneau de toit \"apprentissage\".</p>\n<p style=\"text-align:center;\">Nos véhicules sont aux normes des examens pratique du permis de conduire.</p>\n<p style=\"text-align:center;\">Nos véhicules sont assurés tout risque.</p>\n<p style=\"text-align:center;\">Nos véhicules sont géolocalisés.</p>', 'vehicule'),
(14, 1, 1, 'Comment ça marche ?', '', '', '<p></p>\n<h2>Candidat libre, Louez un véhicule d’apprentissage   </h2>\n<p>Vous avez opté pour l’apprentissage en candidat libre. conformément à la loi en vigueur votre apprentissage doit se faire à bord d\'un véhicule d\'apprentissage. Auto-Permis loue des voitures équipées de double commande, double rétroviseur intérieur, double rétroviseur extérieur, tableau de bord individuel pour l\'accompagnant/formateur, assurées tout risque et aux normes des examens du permis de conduire.</p>\n<p>Auto Permis vous fournira le certificat d’immatriculation, le certificat d’assurance du véhicule ainsi qu’un constat                                    amiable.</p>\n<p>Consultez nos tarifs et nos forfait sur ce site.</p>\n<p></p>\n<p><span><span class=\"enhance\"><strong>Les obligations à respecter </strong></span></span>:</p>\n<p>Vous devez avoir 16 ans minimum pour vous inscrire en candidat libre.</p>\n<p>Attention, vous ne pourrez passer l\'épreuve du code de la route qu\'à partir de 17 ans.</p>\n<p></p>\n<p>Le livret d’apprentissage est obligatoire. Vous pouvez le commander sur internet.</p>\n<p>-Pour connaitre les détails concernant, l’attestation d’accompagnateur</p>\n<p>-Pour connaître le contenu concernant l’attestation d’assurance </p>\n<p>-Pour connaître le contenu concernant les dispositifs et équipement de votre véhicule</p>\n<p> </p>\n<h2>L’apprentissage :</h2>\n<p><span class=\"enhance\"><strong>D’abord</strong></span> vous  allez vous entraîner à l’examen du code de la route grâce aux différents outils pédagogiques mis à votre disposition sur internet et par les livres de code.</p>\n<p>Pour connaître la procédure à suivre pour l’apprentissage du code de la route consulter les fiches pratique \"L\'examen théorique général\" et \"Réussir l\'examen théorique général\".</p>\n<p><span class=\"enhance\"><strong>Ensuite</strong></span>, vous allez vous exercer à la conduite.<span class=\"enhance\"> <strong>2 possibilités </strong></span>s’offrent alors à vous :</p>\n<ul><li>Apprendre la conduite avec  l’accompagnateur de votre choix</li>\n<li>Apprendre la conduite avec un enseignement dispensé par un moniteur d’auto-école.</li>\n</ul><p>Dans tous les cas, le <span class=\"enhance\"><strong>livret d’apprentissage </strong></span>est là pour vous guider tout au long de votre formation, suivez-le étape par étape.</p>\n<p>Il contient un calendrier de formation qu’il faudra compléter à chaque heure de conduite effectuée, il indique aussi les différentes étapes que vous devez valider avec votre accompagnateur.</p>\n<p>L’apprentissage se déroule en <strong>4 étapes</strong> distinctes:</p>\n<ul class=\"ul-check\"><li>La maîtrise de la voiture à allure lente ou modérée, le trafic étant faible ou nul;</li>\n<li>Choisir sa position sur la chaussée, franchir une intersection ou changer de direction ;</li>\n<li>Circuler dans des conditions normales sur route et en agglomération ;</li>\n<li>Connaître les situations présentant des difficultés particulières.</li>\n</ul><div class=\"cadre\">\n<p><span><span class=\"enhance\"><strong>A ne jamais oublier pendant vos heures de conduite</strong></span></span><span><span> :</span></span></p>\n<ul><li><span>Votre livret d’apprentissage</span></li>\n<li><span>Votre formulaire «cerfa numérique»</span></li>\n</ul></div>\n<p>Pour vous présenter à l’examen pratique, vous devez justifier de<span class=\"enhance\"><strong> 20 heures</strong></span> de conduite minimum.</p>\n<h2>La présentation aux examens du permis de conduire : </h2>\n<h3>L’examen du code de la route :</h3>\n<p>Après avoir déposé  votre dossier d’inscription au permis de conduire, vous allez recevoir votre numéro NEPH avec lequel vous pourrez vous inscrire chez l\'un des <strong>quatre opérateurs privés</strong> qui ont l\'<strong>agrément</strong> pour faire passer l\'examen : <strong>La Poste</strong>, <strong>Point code...</strong></p>\n<p>Vous devrez créer un compte sur l\'un des sites d\'opérateurs privés et payer 30 € pour passer l\'épreuve.</p>\n<p>Vous pouvez <strong>choisir le jour et l\'heure de votre examen sur les créneaux disponibles</strong> et mis à disposition par les opérateurs privés. Ils sont globalement ouverts de 9 h à 18 h avec un passage toutes les 30 minutes ou toutes les heures selon les centres.</p>\n<p> <strong><span class=\"enhance\">N’oubliez pas de vous munir de:</span></strong></p>\n<div class=\"cadre\">\n<ul><li><span><span class=\"enhance\"><span>votre pièce d’identité </span></span></span></li>\n<li><span class=\"enhance\"><span>votre convocation envoyée par mail par l\'opérateur privé</span></span></li>\n</ul></div>\n<p>Pour connaître le déroulement de cette épreuve, rendez-vous sur la fiche pratique \"l\'examen théorique général du permis de conduire\". </p>\n<h3>L’examen pratique :</h3>\n<p><span>Pour passer l\'épreuve pratique, vous devez prendre contact avec le service chargé localement de l\'organisation de l\'épreuve pratique de l\'examen (préfecture ou direction départementale des territoires).</span></p>\n<p><span style=\"color:#414856;font-family:\'roboto_regular\', Arial, sans-serif;\"><span>La demande peut se faire par courrier ou par mail, suivant la préfecture concernée.</span></span></p>\n<p><span style=\"color:#414856;font-family:\'roboto_regular\', Arial, sans-serif;\"><span>Pour savoir ou vous adresser rendez-vous sur le site du service publique </span></span>https://www.service-public.fr/particuliers/vosdroits/F2825</p>\n<p>Dans un délai de deux mois vous recevrez votre convocation pour l\'examen pratique. Votre convocation vous indiquera la date, le lieu et l’heure du rendez-vous de l’épreuve.</p>\n<p>Si vous avez effectué l’apprentissage de la conduite sans avoir eu recours aux services d’une auto-école, il faudra vous rendre sur place avec votre propre voiture équipée du système à double commande (comme celle qui a servi à votre apprentissage) avec votre accompagnateur.</p>\n<p>Si vous avez fait appel à une auto-école pour réaliser vos heures de conduite, il faudra vous organiser avec elle pour avoir un moniteur et une voiture à disposition le jour de votre examen.</p>\n<div class=\"cadre\">\n<p><span><span class=\"enhance\"><strong>Le jour J n’oubliez pas de vous munir de :</strong></span></span></p>\n<ul><li>Votre convocation à l’examen</li>\n<li>Votre pièce d’identité </li>\n<li>Votre formulaire « cerfa 02 »</li>\n<li> Votre livret d’apprentissage</li>\n<li> Votre attestation d’assurance « dans le cas où vous passez l’examen sur votre véhicule »</li>\n</ul><p><em>Toutes ces pièces seront vérifiées par votre examinateur le jour de l\'examen.</em></p>\n</div>\n<p>Pour connaître le déroulement de cette épreuve, consultez les fiches pratiques \" L\'épreuve pratique du permis B\" et \" Réussir l\'épreuve pratique du permis B\".</p>\n<p class=\"intercom-align-left\">Vous pourrez <b>consulter votre résultat 48h après votre épreuve</b> (week end et jours fériés non inclus) sur le site de la sécurité routière: http://www.securite-routiere.gouv.fr/permis-de-conduire/résultats-du-permis-de-conduire</p>\n<p class=\"intercom-align-left\">Pour cela, munissez vous de votre numéro NEPH, renseignez le avec votre date de naissance et  la catégorie de permis pour laquelle vous avez passé l\'épreuve pratique.</p>\n<p> </p>\n<p>Si votre C.E.P.C ( certificat d\'examen du permis de conduire) porte la mention insuffisant, vous n\'avez pas obtenu le minimum de point nécessaire pour obtenir votre permis de conduire. Dans ce cas, pour vous représenter, il vous suffit de faire une nouvelle demande de passage à l\'épreuve pratique à la préfecture de votre département.</p>\n<h5><strong>A consulter : Loi Macron, les Réformes du permis de conduire :</strong></h5>\n<p><a href=\"https://www.google.fr/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;ved=0ahUKEwiKlIzSrrDSAhVC2xoKHfxeClgQFggcMAA&amp;url=http%3A%2F%2Fwww.interieur.gouv.fr%2Fcontent%2Fdownload%2F71197%2F519942%2Ffile%2F2014-dossier-de-presse-reforme-du-permis-de-conduire-septembre.pdf&amp;usg=AFQjCNEoADXGZqqEdSkGz2DV1sA3GW-SVQ&amp;cad=rja\">https://www.google.fr/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;ved=0ahUKEwiKlIzSrrDSAhVC2xoKHfxeClgQFggcMAA&amp;url=http%3A%2F%2Fwww.interieur.gouv.fr%2Fcontent%2Fdownload%2F71197%2F519942%2Ffile%2F2014-dossier-de-presse-reforme-du-permis-de-conduire-septembre.pdf&amp;usg=AFQjCNEoADXGZqqEdSkGz2DV1sA3GW-SVQ&amp;cad=rja</a></p>\n<p></p>\n<p><a href=\"index.php?controller=authentication&amp;back=my-account\" class=\"btn btn-default\">Inscrivez-vous</a></p>', 'comment-ca-marche');

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_role`
--

CREATE TABLE `ap_cms_role` (
  `id_cms_role` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_cms` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_role_lang`
--

CREATE TABLE `ap_cms_role_lang` (
  `id_cms_role` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_cms_shop`
--

CREATE TABLE `ap_cms_shop` (
  `id_cms` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_cms_shop`
--

INSERT INTO `ap_cms_shop` (`id_cms`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_compare`
--

CREATE TABLE `ap_compare` (
  `id_compare` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_compare_product`
--

CREATE TABLE `ap_compare_product` (
  `id_compare` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_condition`
--

CREATE TABLE `ap_condition` (
  `id_condition` int(11) NOT NULL,
  `id_ps_condition` int(11) NOT NULL,
  `type` enum('configuration','install','sql') NOT NULL,
  `request` text,
  `operator` varchar(32) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  `result` varchar(64) DEFAULT NULL,
  `calculation_type` enum('hook','time') DEFAULT NULL,
  `calculation_detail` varchar(64) DEFAULT NULL,
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_condition`
--

INSERT INTO `ap_condition` (`id_condition`, `id_ps_condition`, `type`, `request`, `operator`, `value`, `result`, `calculation_type`, `calculation_detail`, `validated`, `date_add`, `date_upd`) VALUES
(1, 1, 'configuration', 'PS_REWRITING_SETTINGS', '==', '1', '1', 'hook', 'actionAdminMetaControllerUpdate_optionsAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(2, 2, 'configuration', 'PS_SMARTY_FORCE_COMPILE', '!=', '2', '1', 'hook', 'actionAdminPerformanceControllerSaveAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(3, 3, 'configuration', 'PS_CSS_THEME_CACHE', '==', '1', '', 'hook', 'actionAdminPerformanceControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(4, 4, 'configuration', 'PS_CIPHER_ALGORITHM', '==', '1', '', 'hook', 'actionAdminPerformanceControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(5, 5, 'configuration', 'PS_MEDIA_SERVERS', '==', '1', '', 'hook', 'actionAdminPerformanceControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(6, 6, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = \"displayPayment\" OR h.name = \"payment\") AND m.name NOT IN (\"bankwire\", \"cheque\", \"cashondelivery\")', '>', '0', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(7, 7, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = \"displayPayment\" OR h.name = \"payment\") AND m.name NOT IN (\"bankwire\", \"cheque\", \"cashondelivery\")', '>', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(8, 8, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN (\"0\", \"My carrier\")', '>', '0', '1', 'hook', 'actionObjectCarrierAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(9, 9, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN (\"0\", \"My carrier\")', '>', '1', '1', 'hook', 'actionObjectCarrierAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(10, 10, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE \"demo_%\"', '>', '0', '1', 'hook', 'actionObjectProductAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(11, 11, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE \"demo_%\"', '>', '9', '12', 'hook', 'actionObjectProductAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(12, 12, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE \"demo_%\"', '>', '99', '16', 'hook', 'actionObjectProductAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(13, 13, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE \"demo_%\"', '>', '999', '16', 'hook', 'actionObjectProductAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(14, 14, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE \"demo_%\"', '>', '9999', '16', 'hook', 'actionObjectProductAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(15, 15, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE \"demo_%\"', '>', '99999', '16', 'hook', 'actionObjectProductAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(16, 16, 'configuration', 'PS_SHOP_PHONE', '!=', '0', '1', 'hook', 'actionAdminStoresControllerUpdate_optionsAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(17, 17, 'sql', 'SELECT COUNT(*) FROM PREFIX_contact', '>', '2', '2', 'hook', 'actionObjectContactAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(18, 18, 'sql', 'SELECT COUNT(*) FROM PREFIX_contact', '>', '4', '2', 'hook', 'actionObjectContactAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(19, 19, 'install', '', '>', '0', '1', 'time', '1', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(20, 20, 'install', '', '>=', '7', '1', 'time', '1', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(21, 21, 'configuration', 'PS_LOGO', '!=', 'logo.jpg', '1', 'hook', 'actionAdminThemesControllerUpdate_optionsAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(22, 22, 'sql', 'SELECT COUNT(*) FROM PREFIX_theme WHERE directory != \"default\" AND directory != \"prestashop\" AND directory ! \"default-bootstrap\"', '>', '0', '0', 'hook', 'actionObjectShopUpdateAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(23, 23, 'configuration', 'PS_LOGGED_ON_ADDONS', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(24, 24, 'configuration', 'PS_MULTISHOP_FEATURE_ACTIVE', '==', '1', '', 'hook', 'actionAdminPreferencesControllerUpdate_optionsAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(25, 25, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop', '>', '1', '1', 'hook', 'actionObjectShopAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(26, 26, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop', '>', '4', '1', 'hook', 'actionObjectShopAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(27, 27, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop_group', '>', '5', '1', 'hook', 'actionObjectShopGroupAddAfter 	', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(28, 28, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop_group', '>', '1', '1', 'hook', 'actionObjectShopGroupAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(29, 29, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = \"displayPayment\" OR h.name = \"payment\") AND m.name NOT IN (\"bankwire\", \"cheque\", \"cashondelivery\")', '>', '2', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(30, 30, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN (\"0\", \"My carrier\")', '>', '2', '1', 'hook', 'actionObjectCarrierAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(31, 31, 'sql', 'SELECT SUM(total_paid_tax_excl / c.conversion_rate)\r\nFROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != \"XKBKNABJK\"', '>=', '200', '224', 'hook', 'actionOrderStatusUpdate', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(32, 32, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != \"XKBKNABJK\"', '>=', '2000', '40', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(33, 33, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != \"XKBKNABJK\"', '>=', '20000', '40', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(34, 34, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '200000', '40', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(35, 35, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '2000000', '40', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(36, 36, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '20000000', '40', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(37, 37, 'install', '', '>=', '30', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(38, 38, 'install', '', '>=', '182', '', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(39, 39, 'install', '', '>=', '365', '', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(40, 40, 'install', '', '>=', '730', '', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(41, 41, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '10', '20', 'time', '1', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(42, 42, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '100', '78', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(43, 43, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '1000', '78', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(44, 44, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '10000', '20', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(45, 45, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '100000', '20', 'time', '3', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(46, 46, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '1000000', '20', 'time', '4', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(47, 47, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != \"b44a6d9efd7a0076a0fbce6b15eaf3b1\"', '>=', '2', '2', 'hook', 'actionObjectCartAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(48, 48, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != \"b44a6d9efd7a0076a0fbce6b15eaf3b1\"', '>=', '10', '10', 'hook', 'actionObjectCartAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(49, 49, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != \"b44a6d9efd7a0076a0fbce6b15eaf3b1\"', '>=', '100', '78', 'hook', 'actionObjectCartAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(50, 50, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != \"b44a6d9efd7a0076a0fbce6b15eaf3b1\"', '>=', '1000', '68', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(51, 51, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != \"b44a6d9efd7a0076a0fbce6b15eaf3b1\"', '>=', '10000', '10', 'time', '4', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(52, 52, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != \"b44a6d9efd7a0076a0fbce6b15eaf3b1\"', '>=', '100000', '10', 'time', '8', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(53, 53, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\")', '>=', '1', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(54, 54, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\")', '>=', '10', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(55, 55, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\")', '>=', '100', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(56, 56, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\")', '>=', '1000', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(57, 57, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\")', '>=', '10000', '0', 'time', '4', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(58, 58, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\")', '>=', '100000', '0', 'time', '8', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(59, 59, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != \"pub@prestashop.com\"', '>=', '1', '1', 'hook', 'actionObjectCustomerAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(60, 60, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != \"pub@prestashop.com\"', '>=', '10', '9', 'hook', 'actionObjectCustomerAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(61, 61, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != \"pub@prestashop.com\"', '>=', '100', '9', 'hook', 'actionObjectCustomerAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(62, 62, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != \"pub@prestashop.com\"', '>=', '1000', '8', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(63, 63, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != \"pub@prestashop.com\"', '>=', '10000', '1', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(64, 64, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != \"pub@prestashop.com\"', '>=', '100000', '1', 'time', '4', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(65, 65, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '1', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(66, 66, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '10', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(67, 67, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '100', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(68, 68, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '1000', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(69, 69, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '10000', '0', 'time', '4', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(70, 70, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '100000', '0', 'time', '8', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(71, 76, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\" AND c.iso_code IN (\r\n\"CA\",\r\n\"GL\",\r\n\"PM\",\r\n\"US\"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(72, 79, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\" AND c.iso_code IN (\r\n\"UM\",\r\n\"AS\",\r\n\"AU\",\r\n\"CK\",\r\n\"FJ\",\r\n\"FM\",\r\n\"GU\",\r\n\"KI\",\r\n\"MH,\"\r\n\"MP\",\r\n\"NC\",\r\n\"NF\",\r\n\"NR\",\r\n\"NU\",\r\n\"NZ\",\r\n\"PF\",\r\n\"PG\",\r\n\"PN\",\r\n\"PW\",\r\n\"SB\",\r\n\"TK\",\r\n\"TO\",\r\n\"TV\",\r\n\"VU\",\r\n\"WF\",\r\n\"WS\"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(73, 85, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\" AND c.iso_code IN (\r\n\"KG\",\r\n\"KZ\",\r\n\"TJ\",\r\n\"TM\",\r\n\"UZ\",\r\n\"AE\",\r\n\"AM\",\r\n\"AZ\",\r\n\"BH\",\r\n\"CY\",\r\n\"GE\",\r\n\"IL\",\r\n\"IQ\",\r\n\"IR\",\r\n\"JO\",\r\n\"KW\",\r\n\"LB\",\r\n\"OM\",\r\n\"QA\",\r\n\"SA\",\r\n\"SY\",\r\n\"TR\",\r\n\"YE\",\r\n\"AF\",\r\n\"BD\",\r\n\"BT\",\r\n\"IN\",\r\n\"IO\",\r\n\"LK\",\r\n\"MV\",\r\n\"NP\",\r\n\"PK\",\r\n\"CN\",\r\n\"HK\",\r\n\"JP\",\r\n\"KP\",\r\n\"KR\",\r\n\"MO\",\r\n\"TW\",\r\n\"MN\",\r\n\"BN\",\r\n\"CC\",\r\n\"CX\",\r\n\"ID\",\r\n\"KH\",\r\n\"LA\",\r\n\"MM\",\r\n\"MY\",\r\n\"PH\",\r\n\"SG\",\r\n\"TH\",\r\n\"TP\",\r\n\"VN\"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(74, 86, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\" AND c.iso_code IN (\r\n\"BZ\",\r\n\"CR\",\r\n\"GT\",\r\n\"HN\",\r\n\"MX\",\r\n\"NI\",\r\n\"PA\",\r\n\"SV\",\r\n\"AG\",\r\n\"AI\",\r\n\"AN\",\r\n\"AW\",\r\n\"BB\",\r\n\"BM\",\r\n\"BS\",\r\n\"CU\",\r\n\"DM\",\r\n\"DO\",\r\n\"GD\",\r\n\"GP\",\r\n\"HT\",\r\n\"JM\",\r\n\"KN\",\r\n\"KY\",\r\n\"LC\",\r\n\"MQ\",\r\n\"MS\",\r\n\"PR\",\r\n\"TC\",\r\n\"TT\",\r\n\"VC\",\r\n\"VG\",\r\n\"VI\",\r\n\"AR\",\r\n\"BO\",\r\n\"BR\",\r\n\"CL\",\r\n\"CO\",\r\n\"EC\",\r\n\"FK\",\r\n\"GF\",\r\n\"GY\",\r\n\"PE\",\r\n\"PY\",\r\n\"SR\",\r\n\"UY\",\r\n\"VE\"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(75, 87, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\" AND c.iso_code IN (\r\n\"BE\",\r\n\"DE\",\r\n\"FR\",\r\n\"FX\",\r\n\"GB\",\r\n\"IE\",\r\n\"LU\",\r\n\"MC\",\r\n\"NL\",\r\n\"IT\",\r\n\"MT\",\r\n\"SM\",\r\n\"VA\",\r\n\"AD\",\r\n\"ES\",\r\n\"GI\",\r\n\"PT\",\r\n\"BY\",\r\n\"EE\",\r\n\"LT\",\r\n\"LV\",\r\n\"MD\",\r\n\"PL\",\r\n\"UA\",\r\n\"AL\",\r\n\"BA\",\r\n\"BG\",\r\n\"GR\",\r\n\"HR\",\r\n\"MK\",\r\n\"RO\",\r\n\"SI\",\r\n\"YU\",\r\n\"RU\",\r\n\"AT\",\r\n\"CH\",\r\n\"CZ\",\r\n\"HU\",\r\n\"LI\",\r\n\"SK\",\r\n\"DK\",\r\n\"FI\",\r\n\"FO\",\r\n\"IS\",\r\n\"NO\",\r\n\"SE\",\r\n\"SJ\"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(76, 88, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\" AND c.iso_code IN (\r\n\"BI\",\r\n\"CF\",\r\n\"CG\",\r\n\"RW\",\r\n\"TD\",\r\n\"ZR\",\r\n\"DJ\",\r\n\"ER\",\r\n\"ET\",\r\n\"KE\",\r\n\"SO\",\r\n\"TZ\",\r\n\"UG\",\r\n\"KM\",\r\n\"MG\",\r\n\"MU\",\r\n\"RE\",\r\n\"SC\",\r\n\"YT\",\r\n\"AO\",\r\n\"BW\",\r\n\"LS\",\r\n\"MW\",\r\n\"MZ\",\r\n\"NA\",\r\n\"SZ\",\r\n\"ZA\",\r\n\"ZM\",\r\n\"ZW\",\r\n\"BF\",\r\n\"BJ\",\r\n\"CI\",\r\n\"CM\",\r\n\"CV\",\r\n\"GA\",\r\n\"GH\",\r\n\"GM\",\r\n\"GN\",\r\n\"GQ\",\r\n\"GW\",\r\n\"LR\",\r\n\"ML\",\r\n\"MR\",\r\n\"NE\",\r\n\"NG\",\r\n\"SL\",\r\n\"SN\",\r\n\"ST\",\r\n\"TG\"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(77, 89, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\" AND c.iso_code IN (\r\n\"DZ\",\r\n\"EG\",\r\n\"EH\",\r\n\"LY\",\r\n\"MA\",\r\n\"SD\",\r\n\"TN\"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(78, 90, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '2', '2', 'hook', 'actionObjectEmployeeAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(79, 91, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '3', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(80, 92, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '5', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(81, 93, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '10', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(82, 94, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '20', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(83, 95, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '40', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(84, 96, 'sql', 'SELECT id_image FROM PREFIX_image WHERE id_image > 26', '>', '0', '27', 'hook', 'actionObjectImageAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(85, 97, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '50', '14', 'hook', 'actionObjectImageAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(86, 98, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '100', '14', 'hook', 'actionObjectImageAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(87, 99, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '1000', '4', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(88, 100, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '10000', '4', 'time', '4', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(89, 101, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '50000', '4', 'time', '8', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(90, 102, 'sql', 'SELECT id_cms FROM PREFIX_cms WHERE id_cms > 5', '>', '0', '6', 'hook', 'actionObjectCMSAddAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(91, 103, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '1', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(92, 104, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '10', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(93, 105, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '100', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(94, 106, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '1000', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(95, 107, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '500', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(96, 108, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '5000', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(97, 109, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\") AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\"', '>=', '1', '0', 'hook', 'newOrder', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(98, 110, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\") AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\"', '>=', '10', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(99, 111, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\") AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\"', '>=', '100', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(100, 112, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\") AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\"', '>=', '10000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(101, 113, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\") AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\"', '>=', '1000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(102, 114, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN (\"XKBKNABJK\", \"OHSATSERP\", \"FFATNOMMJ\", \"UOYEVOLI\", \"KHWLILZLL\") AND a.id_country != \"{config}PS_COUNTRY_DEFAULT{/config}\"', '>=', '5000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(103, 132, 'sql', 'SELECT count(id_configuration) FROM PREFIX_configuration WHERE `name` = \'PS_SHOP_DOMAIN\' AND value IN (\'127.0.0.1\', \'localhost\' )', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(104, 136, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%ebay%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(105, 140, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%moneybookers%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(106, 142, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%paypal%\"', '>=', '1', '1', 'hook', 'actionModuleInstallAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(107, 158, 'install', '', '>=', '90', '', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(108, 159, 'install', '', '<=', '90', '', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(109, 165, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '0', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 1, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(110, 166, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '1', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(111, 167, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '4', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(112, 168, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '9', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(113, 169, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '19', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(114, 170, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '49', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(115, 171, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '1', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(116, 172, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '2', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(117, 173, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '3', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(118, 174, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '4', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(119, 175, 'sql', 'SELECT count(*) FROM	 PREFIX_configuration WHERE name = \'PS_HOSTED_MODE\'', '==', '0', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(120, 209, 'configuration', 'EBAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(121, 320, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%shopgate%\" ', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(122, 322, 'configuration', 'SHOPGATE_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(123, 323, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%shoppingfluxexport%\" ', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(124, 324, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%shoppingfluxexport%\" ', '==', '0', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(125, 325, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE name LIKE \'SHOPPINGFLUXEXPORT_CONFIGURATION_OK\' OR name LIKE \'SHOPPINGFLUXEXPORT_CONFIGURED\'', '>=', '1', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(126, 326, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'MONEYBOOKERS_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'MB_PAY_TO_EMAIL \') AND ( value != \'testaccount2@moneybookers.com \'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(127, 358, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%ebay%\" AND os.logable = 1', '>=', '1', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(128, 359, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%ebay%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(129, 375, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%shopgate%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '1', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(130, 376, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%shopgate%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(131, 377, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%moneybookers%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '1', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(132, 394, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%sofortbanking%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(133, 399, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE \"demo_%\"', '>', '499', '', 'hook', 'actionObjectProductAddAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(134, 424, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%alliance3%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(135, 425, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'ALLIANCE3_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'ALLIANCE_DEMO\') AND ( value = \'0\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(136, 426, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%alliance3%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(137, 427, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%alliance3%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(138, 428, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%authorizeaim%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(139, 429, 'configuration', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'AUTHORIZEAIM_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'AUTHORIZE_AIM_SANDBOX\') AND ( value = \'0\'))', '==', '2', '', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(140, 430, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%authorizeaim%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(141, 431, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%authorizeaim%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(142, 434, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%bluepay%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(143, 435, 'configuration', 'BLUEPAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(144, 436, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%bluepay%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(145, 437, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%bluepay%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(146, 438, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%payplug%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(147, 439, 'configuration', 'PAYPLUG_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(148, 440, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%payplug%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(149, 441, 'sql', 'SELECT SUM(o.total_paid) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%payplug%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '10000', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(150, 442, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%affinityitems%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(151, 443, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE name LIKE \'AFFINITYITEMS_CONFIGURATION_OK\' AND value = \'1\'', '==', '1', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(152, 446, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%dpdpoland%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(153, 447, 'configuration', 'DPDPOLAND_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(154, 448, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like \"%dpdpoland%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(155, 449, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like \"%dpdpoland%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(156, 450, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%envoimoinscher%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(157, 451, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'ENVOIMOINSCHER_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'EMC_ENV \') AND ( value != \'TEST\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(158, 452, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like \"%envoimoinscher%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(159, 453, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like \"%envoimoinscher%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(160, 454, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%klikandpay%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(161, 455, 'configuration', 'KLIKANDPAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(162, 456, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%klikandpay%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(163, 457, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%klikandpay%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(164, 458, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%clickline%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(165, 459, 'configuration', 'CLICKLINE_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(166, 460, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like \"%clickline%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(167, 461, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like \"%clickline%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(168, 462, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%cdiscount%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(169, 463, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(170, 464, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%cdiscount%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(171, 465, 'sql', 'SELECT SUM(o.total_paid) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%cdiscount%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 365 DAY)', '>=', '500', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(172, 467, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%erpillicopresta%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(173, 468, 'configuration', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'ERPILLICOPRESTA_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'ERP_LICENCE_VALIDITY \') AND ( value == \'1\')) OR (( name LIKE \'ERP_MONTH_FREE_ACTIVE \') AND ( value == \'0\'))', '==', '3', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(174, 469, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(175, 470, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(176, 471, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%netreviews%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(177, 472, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'NETREVIEWS_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'AVISVERIFIES_URLCERTIFICAT \') AND ( value IS NOT LIKE \'%preprod%\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(178, 473, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(179, 474, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(180, 475, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%bluesnap%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(181, 476, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'BLUESNAP_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'BLUESNAP_SANDBOX \') AND ( value NOT LIKE \'%sandbox%\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(182, 477, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%bluesnap%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(183, 478, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%bluesnap%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(184, 479, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%desjardins%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(185, 480, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'DESJARDINS_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'DESJARDINS_MODE \') AND ( value NOT LIKE \'%test%\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(186, 481, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%desjardins%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(187, 482, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%desjardins%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(188, 483, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%firstdata%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(189, 484, 'configuration', 'FIRSTDATA_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(190, 485, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%firstdata%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(191, 486, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%firstdata%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(192, 487, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%giveit%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(193, 488, 'sql', 'GIVEIT_CONFIGURATION_OK', '>=', '1', '0', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(194, 489, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(195, 490, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(196, 491, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%ganalytics%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(197, 492, 'configuration', 'GANALYTICS_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(198, 493, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(199, 494, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(200, 496, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%pagseguro%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(201, 497, 'configuration', 'PAGSEGURO_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(202, 498, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%pagseguro%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(203, 499, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%pagseguro%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:39', '2019-06-20 13:12:39'),
(204, 500, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%paypalmx%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(205, 501, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'PAYPALMX_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'PAYPAL_MX_SANDBOX\') AND ( value = \'0\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(206, 502, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%paypalmx%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(207, 503, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%paypalmx%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(208, 505, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%paypalusa%\"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(209, 506, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'PAYPALUSA_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'PAYPAL_USA_SANDBOX\') AND ( value = \'0\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(210, 507, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%paypalusa%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40');
INSERT INTO `ap_condition` (`id_condition`, `id_ps_condition`, `type`, `request`, `operator`, `value`, `result`, `calculation_type`, `calculation_detail`, `validated`, `date_add`, `date_upd`) VALUES
(211, 508, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%paypalmx%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(212, 509, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%payulatam%\"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(213, 510, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'PAYULATAM_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'PAYU_LATAM_TEST\') AND ( value = \'1\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(214, 511, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%payulatam%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(215, 512, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%payulatam%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(216, 513, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%prestastats%\"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(217, 514, 'configuration', 'PRESTASTATS_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(218, 515, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(219, 516, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(220, 517, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%riskified%\"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(221, 518, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'RISKIFIED_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'RISKIFIED_MODE\') AND ( value = \'1\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(222, 519, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%riskified%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(223, 520, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%riskified%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(224, 521, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%simplifycommerce%\"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(225, 522, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'SIMPLIFY_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'SIMPLIFY_MODE\') AND ( value = \'1\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(226, 523, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%simplifycommerce%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(227, 524, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%simplifycommerce%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(228, 525, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%vtpayment%\"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(229, 526, 'configuration', 'VTPAYMENT_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(230, 527, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%vtpayment%\" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(231, 528, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like \"%vtpayment%\" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(232, 529, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%yotpo%\"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(233, 530, 'configuration', 'YOTPO_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(234, 531, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(235, 532, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(236, 533, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%yotpo%\"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(237, 534, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'YOUSTICERESOLUTIONSYSTEM_CONF_OK\') AND ( value = \'1\')) OR (( name LIKE \'YRS_SANDBOX\') AND ( value = \'0\'))', '==', '2', '0', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(238, 535, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(239, 536, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(240, 537, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like \"%loyaltylion%\"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(241, 538, 'configuration', 'LOYALTYLION_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(242, 539, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(243, 540, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40'),
(244, 542, 'sql', 'SELECT \'{config} PS_VERSION_DB{/config}\' >= \'1.7.0.0\' AND < \'1.8.0.0\'', '==', '1', '', 'time', '1', 0, '2019-06-20 13:12:40', '2019-06-20 13:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `ap_condition_advice`
--

CREATE TABLE `ap_condition_advice` (
  `id_condition` int(11) NOT NULL,
  `id_advice` int(11) NOT NULL,
  `display` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_condition_advice`
--

INSERT INTO `ap_condition_advice` (`id_condition`, `id_advice`, `display`) VALUES
(19, 1, 1),
(19, 2, 1),
(19, 3, 1),
(19, 4, 1),
(19, 5, 1),
(19, 6, 1),
(19, 7, 1),
(19, 8, 1),
(19, 9, 1),
(19, 10, 1),
(19, 11, 1),
(19, 12, 1),
(19, 13, 1),
(19, 14, 1),
(19, 15, 1),
(19, 16, 1),
(19, 17, 1),
(19, 18, 1),
(19, 19, 1),
(19, 20, 1),
(19, 21, 1),
(19, 22, 1),
(19, 23, 1),
(19, 24, 1),
(19, 25, 1),
(19, 26, 1),
(19, 27, 1),
(19, 28, 1),
(19, 29, 1),
(19, 30, 1),
(19, 31, 1),
(19, 32, 1),
(19, 33, 1),
(19, 34, 1),
(19, 35, 1),
(19, 36, 1),
(19, 37, 1),
(19, 38, 1),
(19, 39, 1),
(19, 40, 1),
(19, 41, 1),
(19, 42, 1),
(19, 43, 1),
(19, 44, 1),
(19, 45, 1),
(19, 46, 1),
(19, 47, 1),
(19, 48, 1),
(19, 49, 1),
(19, 50, 1),
(19, 51, 1),
(19, 52, 1),
(19, 53, 1),
(19, 54, 1),
(19, 55, 1),
(19, 56, 1),
(19, 57, 1),
(19, 58, 1),
(19, 59, 1),
(19, 60, 1),
(19, 61, 1),
(19, 62, 1),
(19, 63, 1),
(19, 64, 1),
(19, 65, 1),
(19, 66, 1),
(19, 67, 1),
(19, 68, 1),
(19, 69, 1),
(19, 70, 1),
(19, 71, 1),
(19, 72, 1),
(106, 2, 0),
(106, 8, 0),
(106, 14, 0),
(106, 20, 0),
(106, 26, 0),
(106, 32, 0),
(106, 38, 0),
(106, 44, 0),
(106, 50, 0),
(106, 56, 0),
(106, 62, 0),
(106, 68, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_condition_badge`
--

CREATE TABLE `ap_condition_badge` (
  `id_condition` int(11) NOT NULL,
  `id_badge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_condition_badge`
--

INSERT INTO `ap_condition_badge` (`id_condition`, `id_badge`) VALUES
(1, 125),
(2, 126),
(3, 126),
(4, 126),
(5, 127),
(6, 128),
(7, 129),
(8, 130),
(9, 131),
(10, 132),
(11, 133),
(12, 137),
(13, 138),
(14, 139),
(15, 140),
(16, 134),
(17, 135),
(18, 136),
(19, 141),
(20, 142),
(21, 143),
(22, 144),
(23, 145),
(24, 146),
(25, 147),
(26, 149),
(27, 150),
(28, 148),
(29, 152),
(30, 151),
(31, 153),
(32, 154),
(33, 155),
(34, 156),
(35, 157),
(36, 158),
(37, 159),
(38, 160),
(39, 161),
(40, 162),
(41, 163),
(42, 164),
(43, 165),
(44, 166),
(45, 167),
(46, 168),
(47, 169),
(48, 170),
(49, 171),
(50, 172),
(51, 173),
(52, 174),
(53, 175),
(54, 176),
(55, 177),
(56, 178),
(57, 179),
(58, 180),
(59, 187),
(60, 188),
(61, 189),
(62, 190),
(63, 191),
(64, 192),
(65, 181),
(66, 182),
(67, 183),
(68, 184),
(69, 185),
(70, 186),
(71, 193),
(72, 194),
(73, 195),
(74, 196),
(75, 197),
(76, 198),
(77, 199),
(78, 200),
(79, 201),
(80, 202),
(81, 203),
(82, 204),
(83, 205),
(84, 206),
(85, 207),
(86, 208),
(87, 209),
(88, 210),
(89, 211),
(90, 212),
(91, 213),
(92, 214),
(93, 215),
(94, 217),
(95, 216),
(96, 218),
(97, 219),
(98, 220),
(99, 221),
(100, 224),
(101, 222),
(102, 223),
(104, 23),
(105, 5),
(109, 225),
(110, 226),
(111, 227),
(112, 228),
(113, 229),
(114, 230),
(115, 231),
(116, 232),
(117, 233),
(118, 234),
(120, 24),
(121, 1),
(122, 2),
(123, 9),
(125, 10),
(126, 6),
(127, 25),
(128, 26),
(129, 3),
(130, 4),
(131, 7),
(132, 8),
(134, 11),
(135, 12),
(136, 13),
(137, 14),
(138, 15),
(139, 16),
(140, 17),
(141, 18),
(142, 19),
(143, 20),
(144, 21),
(145, 22),
(146, 27),
(147, 28),
(148, 29),
(149, 30),
(150, 31),
(151, 32),
(152, 33),
(153, 34),
(154, 35),
(155, 36),
(156, 37),
(157, 38),
(158, 39),
(159, 40),
(160, 41),
(161, 42),
(162, 43),
(163, 44),
(164, 45),
(165, 46),
(166, 47),
(167, 48),
(168, 49),
(169, 50),
(170, 51),
(171, 52),
(172, 53),
(173, 54),
(174, 55),
(175, 56),
(176, 57),
(177, 58),
(178, 59),
(179, 60),
(180, 61),
(181, 62),
(182, 63),
(183, 64),
(184, 65),
(185, 66),
(186, 67),
(187, 68),
(188, 69),
(189, 70),
(190, 71),
(191, 72),
(192, 73),
(193, 74),
(194, 75),
(195, 76),
(196, 77),
(197, 78),
(198, 79),
(199, 80),
(200, 81),
(201, 82),
(202, 83),
(203, 84),
(204, 85),
(205, 86),
(206, 87),
(207, 88),
(208, 89),
(209, 90),
(210, 91),
(211, 92),
(212, 93),
(213, 94),
(214, 95),
(215, 96),
(216, 97),
(217, 98),
(218, 99),
(219, 100),
(220, 101),
(221, 102),
(222, 103),
(223, 104),
(224, 105),
(225, 106),
(226, 107),
(227, 108),
(228, 109),
(229, 110),
(230, 111),
(231, 112),
(232, 113),
(233, 114),
(234, 115),
(235, 116),
(236, 117),
(237, 118),
(238, 119),
(239, 120),
(240, 121),
(241, 122),
(242, 123),
(243, 124);

-- --------------------------------------------------------

--
-- Table structure for table `ap_configuration`
--

CREATE TABLE `ap_configuration` (
  `id_configuration` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED DEFAULT NULL,
  `id_shop` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(254) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_configuration`
--

INSERT INTO `ap_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(1, NULL, NULL, 'PS_LANG_DEFAULT', '1', '2019-06-08 02:34:20', '2019-06-08 02:34:20'),
(2, NULL, NULL, 'PS_VERSION_DB', '1.6.1.24', '2019-06-08 02:34:20', '2019-06-08 02:34:20'),
(3, NULL, NULL, 'PS_INSTALL_VERSION', '1.6.1.24', '2019-06-08 02:34:20', '2019-06-08 02:34:20'),
(4, NULL, NULL, 'PS_GROUP_FEATURE_ACTIVE', '1', '2019-06-08 02:34:20', '2019-06-08 02:34:20'),
(5, NULL, NULL, 'PS_CARRIER_DEFAULT', '1', '2019-06-08 02:34:20', '2019-06-08 02:34:20'),
(6, NULL, NULL, 'PS_SEARCH_INDEXATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, NULL, NULL, 'PS_ONE_PHONE_AT_LEAST', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, NULL, NULL, 'PS_CURRENCY_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, NULL, NULL, 'PS_COUNTRY_DEFAULT', '176', '0000-00-00 00:00:00', '2019-06-08 02:34:23'),
(10, NULL, NULL, 'PS_REWRITING_SETTINGS', '0', '0000-00-00 00:00:00', '2019-06-12 01:30:04'),
(11, NULL, NULL, 'PS_ORDER_OUT_OF_STOCK', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, NULL, NULL, 'PS_LAST_QTIES', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, NULL, NULL, 'PS_CART_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, NULL, NULL, 'PS_CONDITIONS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, NULL, NULL, 'PS_RECYCLABLE_PACK', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, NULL, NULL, 'PS_GIFT_WRAPPING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, NULL, NULL, 'PS_GIFT_WRAPPING_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, NULL, NULL, 'PS_STOCK_MANAGEMENT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, NULL, NULL, 'PS_NAVIGATION_PIPE', '>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, NULL, NULL, 'PS_PRODUCTS_PER_PAGE', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, NULL, NULL, 'PS_PURCHASE_MINIMUM', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, NULL, NULL, 'PS_PRODUCTS_ORDER_WAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, NULL, NULL, 'PS_PRODUCTS_ORDER_BY', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, NULL, NULL, 'PS_DISPLAY_QTIES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, NULL, NULL, 'PS_SHIPPING_HANDLING', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, NULL, NULL, 'PS_SHIPPING_FREE_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, NULL, NULL, 'PS_SHIPPING_FREE_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, NULL, NULL, 'PS_SHIPPING_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, NULL, NULL, 'PS_TAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, NULL, NULL, 'PS_SHOP_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, NULL, NULL, 'PS_NB_DAYS_NEW_PRODUCT', '20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, NULL, NULL, 'PS_SSL_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, NULL, NULL, 'PS_WEIGHT_UNIT', 'kg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, NULL, NULL, 'PS_BLOCK_CART_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, NULL, NULL, 'PS_ORDER_RETURN', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, NULL, NULL, 'PS_ORDER_RETURN_NB_DAYS', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, NULL, NULL, 'PS_MAIL_TYPE', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, NULL, NULL, 'PS_PRODUCT_PICTURE_MAX_SIZE', '8388608', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, NULL, NULL, 'PS_PRODUCT_PICTURE_WIDTH', '64', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, NULL, NULL, 'PS_PRODUCT_PICTURE_HEIGHT', '64', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, NULL, NULL, 'PS_INVOICE_PREFIX', '#IN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, NULL, NULL, 'PS_INVCE_INVOICE_ADDR_RULES', '{\"avoid\":[]}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, NULL, NULL, 'PS_INVCE_DELIVERY_ADDR_RULES', '{\"avoid\":[]}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, NULL, NULL, 'PS_DELIVERY_PREFIX', '#DE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, NULL, NULL, 'PS_DELIVERY_NUMBER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, NULL, NULL, 'PS_RETURN_PREFIX', '#RE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, NULL, NULL, 'PS_INVOICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, NULL, NULL, 'PS_PASSWD_TIME_BACK', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, NULL, NULL, 'PS_PASSWD_TIME_FRONT', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, NULL, NULL, 'PS_DISP_UNAVAILABLE_ATTR', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, NULL, NULL, 'PS_SEARCH_MINWORDLEN', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, NULL, NULL, 'PS_SEARCH_BLACKLIST', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, NULL, NULL, 'PS_SEARCH_WEIGHT_PNAME', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, NULL, NULL, 'PS_SEARCH_WEIGHT_REF', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, NULL, NULL, 'PS_SEARCH_WEIGHT_SHORTDESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, NULL, NULL, 'PS_SEARCH_WEIGHT_DESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, NULL, NULL, 'PS_SEARCH_WEIGHT_CNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, NULL, NULL, 'PS_SEARCH_WEIGHT_MNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, NULL, NULL, 'PS_SEARCH_WEIGHT_TAG', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, NULL, NULL, 'PS_SEARCH_WEIGHT_ATTRIBUTE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, NULL, NULL, 'PS_SEARCH_WEIGHT_FEATURE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, NULL, NULL, 'PS_SEARCH_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, NULL, NULL, 'PS_TIMEZONE', 'Indian/Reunion', '0000-00-00 00:00:00', '2019-06-08 02:34:23'),
(64, NULL, NULL, 'PS_THEME_V11', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, NULL, NULL, 'PRESTASTORE_LIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, NULL, NULL, 'PS_TIN_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, NULL, NULL, 'PS_SHOW_ALL_MODULES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, NULL, NULL, 'PS_BACKUP_ALL', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, NULL, NULL, 'PS_1_3_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, NULL, NULL, 'PS_PRICE_ROUND_MODE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, NULL, NULL, 'PS_1_3_2_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, NULL, NULL, 'PS_CONDITIONS_CMS_ID', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, NULL, NULL, 'TRACKING_DIRECT_TRAFFIC', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, NULL, NULL, 'PS_META_KEYWORDS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, NULL, NULL, 'PS_DISPLAY_JQZOOM', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, NULL, NULL, 'PS_VOLUME_UNIT', 'L', '0000-00-00 00:00:00', '2019-06-08 02:34:24'),
(77, NULL, NULL, 'PS_CIPHER_ALGORITHM', '0', '0000-00-00 00:00:00', '2019-06-08 02:34:23'),
(78, NULL, NULL, 'PS_ATTRIBUTE_CATEGORY_DISPLAY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, NULL, NULL, 'PS_CUSTOMER_SERVICE_FILE_UPLOAD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, NULL, NULL, 'PS_CUSTOMER_SERVICE_SIGNATURE', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, NULL, NULL, 'PS_BLOCK_BESTSELLERS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, NULL, NULL, 'PS_BLOCK_NEWPRODUCTS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, NULL, NULL, 'PS_BLOCK_SPECIALS_DISPLAY', '1', '0000-00-00 00:00:00', '2019-06-12 01:17:24'),
(84, NULL, NULL, 'PS_STOCK_MVT_REASON_DEFAULT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, NULL, NULL, 'PS_COMPARATOR_MAX_ITEM', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, NULL, NULL, 'PS_ORDER_PROCESS_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, NULL, NULL, 'PS_SPECIFIC_PRICE_PRIORITIES', 'id_shop;id_currency;id_country;id_group', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, NULL, NULL, 'PS_TAX_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, NULL, NULL, 'PS_SMARTY_FORCE_COMPILE', '2', '0000-00-00 00:00:00', '2019-06-08 02:47:48'),
(90, NULL, NULL, 'PS_DISTANCE_UNIT', 'km', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, NULL, NULL, 'PS_STORES_DISPLAY_CMS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, NULL, NULL, 'PS_STORES_DISPLAY_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, NULL, NULL, 'PS_STORES_SIMPLIFIED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, NULL, NULL, 'SHOP_LOGO_WIDTH', '110', '0000-00-00 00:00:00', '2019-06-08 11:32:06'),
(95, NULL, NULL, 'SHOP_LOGO_HEIGHT', '92', '0000-00-00 00:00:00', '2019-06-08 11:32:06'),
(96, NULL, NULL, 'EDITORIAL_IMAGE_WIDTH', '530', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, NULL, NULL, 'EDITORIAL_IMAGE_HEIGHT', '228', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, NULL, NULL, 'PS_STATSDATA_CUSTOMER_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, NULL, NULL, 'PS_STATSDATA_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, NULL, NULL, 'PS_STATSDATA_PLUGINS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, NULL, NULL, 'PS_GEOLOCATION_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, NULL, NULL, 'PS_ALLOWED_COUNTRIES', 'AF;ZA;AX;AL;DZ;DE;AD;AO;AI;AQ;AG;AN;SA;AR;AM;AW;AU;AT;AZ;BS;BH;BD;BB;BY;BE;BZ;BJ;BM;BT;BO;BA;BW;BV;BR;BN;BG;BF;MM;BI;KY;KH;CM;CA;CV;CF;CL;CN;CX;CY;CC;CO;KM;CG;CD;CK;KR;KP;CR;CI;HR;CU;DK;DJ;DM;EG;IE;SV;AE;EC;ER;ES;EE;ET;FK;FO;FJ;FI;FR;GA;GM;GE;GS;GH;GI;GR;GD;GL;GP;GU;GT;GG;GN;GQ;GW;GY;GF;HT;HM;HN;HK;HU;IM;MU;VG;VI;IN;ID;IR;IQ;IS;IL;IT;JM;JP;JE;JO;KZ;KE;KG;KI;KW;LA;LS;LV;LB;LR;LY;LI;LT;LU;MO;MK;MG;MY;MW;MV;ML;MT;MP;MA;MH;MQ;MR;YT;MX;FM;MD;MC;MN;ME;MS;MZ;NA;NR;NP;NI;NE;NG;NU;NF;NO;NC;NZ;IO;OM;UG;UZ;PK;PW;PS;PA;PG;PY;NL;PE;PH;PN;PL;PF;PR;PT;QA;DO;CZ;RE;RO;GB;RU;RW;EH;BL;KN;SM;MF;PM;VA;VC;LC;SB;WS;AS;ST;SN;RS;SC;SL;SG;SK;SI;SO;SD;LK;SE;CH;SR;SJ;SZ;SY;TJ;TW;TZ;TD;TF;TH;TL;TG;TK;TO;TT;TN;TM;TC;TR;TV;UA;UY;US;VU;VE;VN;WF;YE;ZM;ZW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, NULL, NULL, 'PS_GEOLOCATION_BEHAVIOR', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, NULL, NULL, 'PS_LOCALE_LANGUAGE', 'fr', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, NULL, NULL, 'PS_LOCALE_COUNTRY', 're', '0000-00-00 00:00:00', '2019-06-08 02:34:23'),
(106, NULL, NULL, 'PS_ATTACHMENT_MAXIMUM_SIZE', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, NULL, NULL, 'PS_SMARTY_CACHE', '0', '0000-00-00 00:00:00', '2019-06-08 02:41:33'),
(108, NULL, NULL, 'PS_DIMENSION_UNIT', 'cm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, NULL, NULL, 'PS_GUEST_CHECKOUT_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, NULL, NULL, 'PS_DISPLAY_SUPPLIERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, NULL, NULL, 'PS_DISPLAY_BEST_SELLERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, NULL, NULL, 'PS_CATALOG_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, NULL, NULL, 'PS_GEOLOCATION_WHITELIST', '127;209.185.108;209.185.253;209.85.238;209.85.238.11;209.85.238.4;216.239.33.96;216.239.33.97;216.239.33.98;216.239.33.99;216.239.37.98;216.239.37.99;216.239.39.98;216.239.39.99;216.239.41.96;216.239.41.97;216.239.41.98;216.239.41.99;216.239.45.4;216.239.46;216.239.51.96;216.239.51.97;216.239.51.98;216.239.51.99;216.239.53.98;216.239.53.99;216.239.57.96;91.240.109;216.239.57.97;216.239.57.98;216.239.57.99;216.239.59.98;216.239.59.99;216.33.229.163;64.233.173.193;64.233.173.194;64.233.173.195;64.233.173.196;64.233.173.197;64.233.173.198;64.233.173.199;64.233.173.200;64.233.173.201;64.233.173.202;64.233.173.203;64.233.173.204;64.233.173.205;64.233.173.206;64.233.173.207;64.233.173.208;64.233.173.209;64.233.173.210;64.233.173.211;64.233.173.212;64.233.173.213;64.233.173.214;64.233.173.215;64.233.173.216;64.233.173.217;64.233.173.218;64.233.173.219;64.233.173.220;64.233.173.221;64.233.173.222;64.233.173.223;64.233.173.224;64.233.173.225;64.233.173.226;64.233.173.227;64.233.173.228;64.233.173.229;64.233.173.230;64.233.173.231;64.233.173.232;64.233.173.233;64.233.173.234;64.233.173.235;64.233.173.236;64.233.173.237;64.233.173.238;64.233.173.239;64.233.173.240;64.233.173.241;64.233.173.242;64.233.173.243;64.233.173.244;64.233.173.245;64.233.173.246;64.233.173.247;64.233.173.248;64.233.173.249;64.233.173.250;64.233.173.251;64.233.173.252;64.233.173.253;64.233.173.254;64.233.173.255;64.68.80;64.68.81;64.68.82;64.68.83;64.68.84;64.68.85;64.68.86;64.68.87;64.68.88;64.68.89;64.68.90.1;64.68.90.10;64.68.90.11;64.68.90.12;64.68.90.129;64.68.90.13;64.68.90.130;64.68.90.131;64.68.90.132;64.68.90.133;64.68.90.134;64.68.90.135;64.68.90.136;64.68.90.137;64.68.90.138;64.68.90.139;64.68.90.14;64.68.90.140;64.68.90.141;64.68.90.142;64.68.90.143;64.68.90.144;64.68.90.145;64.68.90.146;64.68.90.147;64.68.90.148;64.68.90.149;64.68.90.15;64.68.90.150;64.68.90.151;64.68.90.152;64.68.90.153;64.68.90.154;64.68.90.155;64.68.90.156;64.68.90.157;64.68.90.158;64.68.90.159;64.68.90.16;64.68.90.160;64.68.90.161;64.68.90.162;64.68.90.163;64.68.90.164;64.68.90.165;64.68.90.166;64.68.90.167;64.68.90.168;64.68.90.169;64.68.90.17;64.68.90.170;64.68.90.171;64.68.90.172;64.68.90.173;64.68.90.174;64.68.90.175;64.68.90.176;64.68.90.177;64.68.90.178;64.68.90.179;64.68.90.18;64.68.90.180;64.68.90.181;64.68.90.182;64.68.90.183;64.68.90.184;64.68.90.185;64.68.90.186;64.68.90.187;64.68.90.188;64.68.90.189;64.68.90.19;64.68.90.190;64.68.90.191;64.68.90.192;64.68.90.193;64.68.90.194;64.68.90.195;64.68.90.196;64.68.90.197;64.68.90.198;64.68.90.199;64.68.90.2;64.68.90.20;64.68.90.200;64.68.90.201;64.68.90.202;64.68.90.203;64.68.90.204;64.68.90.205;64.68.90.206;64.68.90.207;64.68.90.208;64.68.90.21;64.68.90.22;64.68.90.23;64.68.90.24;64.68.90.25;64.68.90.26;64.68.90.27;64.68.90.28;64.68.90.29;64.68.90.3;64.68.90.30;64.68.90.31;64.68.90.32;64.68.90.33;64.68.90.34;64.68.90.35;64.68.90.36;64.68.90.37;64.68.90.38;64.68.90.39;64.68.90.4;64.68.90.40;64.68.90.41;64.68.90.42;64.68.90.43;64.68.90.44;64.68.90.45;64.68.90.46;64.68.90.47;64.68.90.48;64.68.90.49;64.68.90.5;64.68.90.50;64.68.90.51;64.68.90.52;64.68.90.53;64.68.90.54;64.68.90.55;64.68.90.56;64.68.90.57;64.68.90.58;64.68.90.59;64.68.90.6;64.68.90.60;64.68.90.61;64.68.90.62;64.68.90.63;64.68.90.64;64.68.90.65;64.68.90.66;64.68.90.67;64.68.90.68;64.68.90.69;64.68.90.7;64.68.90.70;64.68.90.71;64.68.90.72;64.68.90.73;64.68.90.74;64.68.90.75;64.68.90.76;64.68.90.77;64.68.90.78;64.68.90.79;64.68.90.8;64.68.90.80;64.68.90.9;64.68.91;64.68.92;66.249.64;66.249.65;66.249.66;66.249.67;66.249.68;66.249.69;66.249.70;66.249.71;66.249.72;66.249.73;66.249.78;66.249.79;72.14.199;8.6.48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, NULL, NULL, 'PS_LOGS_BY_EMAIL', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, NULL, NULL, 'PS_COOKIE_CHECKIP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, NULL, NULL, 'PS_STORES_CENTER_LAT', '25.948969', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, NULL, NULL, 'PS_STORES_CENTER_LONG', '-80.226439', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, NULL, NULL, 'PS_USE_ECOTAX', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, NULL, NULL, 'PS_CANONICAL_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, NULL, NULL, 'PS_IMG_UPDATE_TIME', '1560832739', '0000-00-00 00:00:00', '2019-06-18 08:38:59'),
(121, NULL, NULL, 'PS_BACKUP_DROP_TABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, NULL, NULL, 'PS_OS_CHEQUE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, NULL, NULL, 'PS_OS_PAYMENT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, NULL, NULL, 'PS_OS_PREPARATION', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, NULL, NULL, 'PS_OS_SHIPPING', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, NULL, NULL, 'PS_OS_DELIVERED', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, NULL, NULL, 'PS_OS_CANCELED', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, NULL, NULL, 'PS_OS_REFUND', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, NULL, NULL, 'PS_OS_ERROR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, NULL, NULL, 'PS_OS_OUTOFSTOCK', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, NULL, NULL, 'PS_OS_BANKWIRE', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, NULL, NULL, 'PS_OS_PAYPAL', '11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, NULL, NULL, 'PS_OS_WS_PAYMENT', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, NULL, NULL, 'PS_OS_OUTOFSTOCK_PAID', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, NULL, NULL, 'PS_OS_OUTOFSTOCK_UNPAID', '13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, NULL, NULL, 'PS_OS_COD_VALIDATION', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, NULL, NULL, 'PS_LEGACY_IMAGES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, NULL, NULL, 'PS_IMAGE_QUALITY', 'jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, NULL, NULL, 'PS_PNG_QUALITY', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, NULL, NULL, 'PS_JPEG_QUALITY', '90', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, NULL, NULL, 'PS_COOKIE_LIFETIME_FO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, NULL, NULL, 'PS_COOKIE_LIFETIME_BO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, NULL, NULL, 'PS_RESTRICT_DELIVERED_COUNTRIES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, NULL, NULL, 'PS_SHOW_NEW_ORDERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, NULL, NULL, 'PS_SHOW_NEW_CUSTOMERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, NULL, NULL, 'PS_SHOW_NEW_MESSAGES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, NULL, NULL, 'PS_FEATURE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, NULL, NULL, 'PS_COMBINATION_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, NULL, NULL, 'PS_SPECIFIC_PRICE_FEATURE_ACTIVE', NULL, '0000-00-00 00:00:00', '2019-06-15 16:30:46'),
(150, NULL, NULL, 'PS_SCENE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, NULL, NULL, 'PS_VIRTUAL_PROD_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '2019-06-08 03:01:15'),
(152, NULL, NULL, 'PS_CUSTOMIZATION_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, NULL, NULL, 'PS_CART_RULE_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, NULL, NULL, 'PS_PACK_FEATURE_ACTIVE', NULL, '0000-00-00 00:00:00', '2019-06-19 20:53:46'),
(155, NULL, NULL, 'PS_ALIAS_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, NULL, NULL, 'PS_TAX_ADDRESS_TYPE', 'id_address_delivery', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, NULL, NULL, 'PS_SHOP_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, NULL, NULL, 'PS_CARRIER_DEFAULT_SORT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, NULL, NULL, 'PS_STOCK_MVT_INC_REASON_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, NULL, NULL, 'PS_STOCK_MVT_DEC_REASON_DEFAULT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, NULL, NULL, 'PS_ADVANCED_STOCK_MANAGEMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, NULL, NULL, 'PS_ADMINREFRESH_NOTIFICATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_TO', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_FROM', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, NULL, NULL, 'PS_CARRIER_DEFAULT_ORDER', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, NULL, NULL, 'PS_STOCK_MVT_SUPPLY_ORDER', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, NULL, NULL, 'PS_STOCK_CUSTOMER_ORDER_REASON', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, NULL, NULL, 'PS_UNIDENTIFIED_GROUP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, NULL, NULL, 'PS_GUEST_GROUP', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, NULL, NULL, 'PS_CUSTOMER_GROUP', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, NULL, NULL, 'PS_SMARTY_CONSOLE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, NULL, NULL, 'PS_INVOICE_MODEL', 'invoice', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, NULL, NULL, 'PS_LIMIT_UPLOAD_IMAGE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, NULL, NULL, 'PS_LIMIT_UPLOAD_FILE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, NULL, NULL, 'MB_PAY_TO_EMAIL', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, NULL, NULL, 'MB_SECRET_WORD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, NULL, NULL, 'MB_HIDE_LOGIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, NULL, NULL, 'MB_ID_LOGO', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, NULL, NULL, 'MB_ID_LOGO_WALLET', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, NULL, NULL, 'MB_PARAMETERS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, NULL, NULL, 'MB_PARAMETERS_2', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, NULL, NULL, 'MB_DISPLAY_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, NULL, NULL, 'MB_CANCEL_URL', 'http://www.yoursite.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, NULL, NULL, 'MB_LOCAL_METHODS', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, NULL, NULL, 'MB_INTER_METHODS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, NULL, NULL, 'BANK_WIRE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, NULL, NULL, 'CHEQUE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, NULL, NULL, 'PRODUCTS_VIEWED_NBR', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, NULL, NULL, 'BLOCK_CATEG_DHTML', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, NULL, NULL, 'BLOCK_CATEG_MAX_DEPTH', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, NULL, NULL, 'MANUFACTURER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '2019-06-08 02:34:31'),
(192, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '2019-06-08 02:34:31'),
(193, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, NULL, NULL, 'NEW_PRODUCTS_NBR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, NULL, NULL, 'PS_TOKEN_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, NULL, NULL, 'PS_STATS_RENDER', 'graphnvd3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, NULL, NULL, 'PS_STATS_OLD_CONNECT_AUTO_CLEAN', 'never', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, NULL, NULL, 'PS_STATS_GRID_RENDER', 'gridhtml', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, NULL, NULL, 'BLOCKTAGS_NBR', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, NULL, NULL, 'CHECKUP_DESCRIPTIONS_LT', '100', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, NULL, NULL, 'CHECKUP_DESCRIPTIONS_GT', '400', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, NULL, NULL, 'CHECKUP_IMAGES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, NULL, NULL, 'CHECKUP_IMAGES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, NULL, NULL, 'CHECKUP_SALES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, NULL, NULL, 'CHECKUP_SALES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, NULL, NULL, 'CHECKUP_STOCK_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, NULL, NULL, 'CHECKUP_STOCK_GT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, NULL, NULL, 'FOOTER_CMS', '0_3|0_4', '0000-00-00 00:00:00', '2019-06-08 02:34:31'),
(209, NULL, NULL, 'FOOTER_BLOCK_ACTIVATION', '1', '0000-00-00 00:00:00', '2019-06-15 16:12:39'),
(210, NULL, NULL, 'FOOTER_POWEREDBY', '0', '0000-00-00 00:00:00', '2019-06-15 16:12:39'),
(211, NULL, NULL, 'BLOCKADVERT_LINK', 'http://www.prestashop.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, NULL, NULL, 'BLOCKSTORE_IMG', 'store.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, NULL, NULL, 'BLOCKADVERT_IMG_EXT', 'jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, NULL, NULL, 'MOD_BLOCKTOPMENU_ITEMS', 'CAT3,CAT8,CAT5,LNK1', '0000-00-00 00:00:00', '2019-06-08 02:34:31'),
(215, NULL, NULL, 'MOD_BLOCKTOPMENU_SEARCH', '0', '0000-00-00 00:00:00', '2019-06-08 02:34:31'),
(216, NULL, NULL, 'BLOCKSOCIAL_FACEBOOK', 'https://www.facebook.com/Auto-Permis-428705551308456/', '0000-00-00 00:00:00', '2019-06-15 17:21:30'),
(217, NULL, NULL, 'BLOCKSOCIAL_TWITTER', NULL, '0000-00-00 00:00:00', '2019-06-15 17:21:30'),
(218, NULL, NULL, 'BLOCKSOCIAL_RSS', NULL, '0000-00-00 00:00:00', '2019-06-15 17:21:30'),
(219, NULL, NULL, 'BLOCKCONTACTINFOS_COMPANY', 'Auto Permis', '0000-00-00 00:00:00', '2019-06-15 15:27:09'),
(220, NULL, NULL, 'BLOCKCONTACTINFOS_ADDRESS', '68 rue suffren  - Saint Pierre 97410', '0000-00-00 00:00:00', '2019-06-15 15:27:09'),
(221, NULL, NULL, 'BLOCKCONTACTINFOS_PHONE', '0262.65.82.20', '0000-00-00 00:00:00', '2019-06-15 15:27:09'),
(222, NULL, NULL, 'BLOCKCONTACTINFOS_EMAIL', 'autopermis.info@gmail.com', '0000-00-00 00:00:00', '2019-06-15 15:27:09'),
(223, NULL, NULL, 'BLOCKCONTACT_TELNUMBER', '0262658220', '0000-00-00 00:00:00', '2019-06-15 14:57:13'),
(224, NULL, NULL, 'BLOCKCONTACT_EMAIL', 'autopermis.info@gmail.com', '0000-00-00 00:00:00', '2019-06-15 14:57:13'),
(225, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '2019-06-08 02:34:31'),
(226, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, NULL, NULL, 'SUPPLIER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '2019-06-08 02:34:31'),
(228, NULL, NULL, 'BLOCK_CATEG_NBR_COLUMN_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, NULL, NULL, 'UPGRADER_BACKUPDB_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, NULL, NULL, 'UPGRADER_BACKUPFILES_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, NULL, NULL, 'BLOCKREINSURANCE_NBBLOCKS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, NULL, NULL, 'HOMESLIDER_WIDTH', '3000', '0000-00-00 00:00:00', '2019-06-08 03:21:55'),
(233, NULL, NULL, 'HOMESLIDER_SPEED', '500', '0000-00-00 00:00:00', '2019-06-08 02:34:32'),
(234, NULL, NULL, 'HOMESLIDER_PAUSE', '3000', '0000-00-00 00:00:00', '2019-06-08 02:34:32'),
(235, NULL, NULL, 'HOMESLIDER_LOOP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, NULL, NULL, 'PS_BASE_DISTANCE_UNIT', 'm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, NULL, NULL, 'PS_SHOP_DOMAIN', 'www.autopermis.re', '0000-00-00 00:00:00', '2019-06-10 23:58:45'),
(238, NULL, NULL, 'PS_SHOP_DOMAIN_SSL', 'www.autopermis.re', '0000-00-00 00:00:00', '2019-06-10 23:58:45'),
(239, NULL, NULL, 'PS_SHOP_NAME', 'AutoPermis', '0000-00-00 00:00:00', '2019-06-08 02:34:23'),
(240, NULL, NULL, 'PS_SHOP_EMAIL', 'autopermis.info@gmail.com', '0000-00-00 00:00:00', '2019-06-10 15:13:24'),
(241, NULL, NULL, 'PS_MAIL_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, NULL, NULL, 'PS_SHOP_ACTIVITY', '0', '0000-00-00 00:00:00', '2019-06-08 02:34:23'),
(243, NULL, NULL, 'PS_LOGO', 'autopermis-logo-1559979125.jpg', '0000-00-00 00:00:00', '2019-06-08 11:32:05'),
(244, NULL, NULL, 'PS_FAVICON', 'favicon.ico', '0000-00-00 00:00:00', '2019-06-10 15:11:44'),
(245, NULL, NULL, 'PS_STORES_ICON', 'autopermis-logo_stores-1560165155.gif', '0000-00-00 00:00:00', '2019-06-10 15:12:35'),
(246, NULL, NULL, 'PS_ROOT_CATEGORY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, NULL, NULL, 'PS_HOME_CATEGORY', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, NULL, NULL, 'PS_CONFIGURATION_AGREMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, NULL, NULL, 'PS_MAIL_SERVER', 'smtp.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, NULL, NULL, 'PS_MAIL_USER', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, NULL, NULL, 'PS_MAIL_PASSWD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, NULL, NULL, 'PS_MAIL_SMTP_ENCRYPTION', 'off', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, NULL, NULL, 'PS_MAIL_SMTP_PORT', '25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, NULL, NULL, 'PS_MAIL_COLOR', '#db3484', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, NULL, NULL, 'NW_SALT', 'czSChUX508NqEJkY', '0000-00-00 00:00:00', '2019-06-08 02:34:31'),
(256, NULL, NULL, 'PS_PAYMENT_LOGO_CMS_ID', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, NULL, NULL, 'HOME_FEATURED_NBR', '150', '0000-00-00 00:00:00', '2019-06-10 20:32:39'),
(258, NULL, NULL, 'SEK_MIN_OCCURENCES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, NULL, NULL, 'SEK_FILTER_KW', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, NULL, NULL, 'PS_ALLOW_MOBILE_DEVICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, NULL, NULL, 'PS_CUSTOMER_CREATION_EMAIL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, NULL, NULL, 'PS_SMARTY_CONSOLE_KEY', 'SMARTY_DEBUG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, NULL, NULL, 'PS_DASHBOARD_USE_PUSH', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, NULL, NULL, 'PS_ATTRIBUTE_ANCHOR_SEPARATOR', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, NULL, NULL, 'CONF_AVERAGE_PRODUCT_MARGIN', '40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, NULL, NULL, 'PS_DASHBOARD_SIMULATION', '0', '0000-00-00 00:00:00', '2019-06-08 03:37:16'),
(267, NULL, NULL, 'PS_QUICK_VIEW', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, NULL, NULL, 'PS_USE_HTMLPURIFIER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, NULL, NULL, 'PS_SMARTY_CACHING_TYPE', 'filesystem', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, NULL, NULL, 'PS_SMARTY_CLEAR_CACHE', 'everytime', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, NULL, NULL, 'PS_DETECT_LANG', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, NULL, NULL, 'PS_DETECT_COUNTRY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, NULL, NULL, 'PS_ROUND_TYPE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, NULL, NULL, 'PS_PRICE_DISPLAY_PRECISION', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, NULL, NULL, 'PS_LOG_EMAILS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, NULL, NULL, 'PS_CUSTOMER_NWSL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, NULL, NULL, 'PS_CUSTOMER_OPTIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, NULL, NULL, 'PS_PACK_STOCK_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, NULL, NULL, 'PS_LOG_MODULE_PERFS_MODULO', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, NULL, NULL, 'PS_DISALLOW_HISTORY_REORDERING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, NULL, NULL, 'PS_DISPLAY_PRODUCT_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, NULL, NULL, 'PS_PRODUCT_WEIGHT_PRECISION', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, NULL, NULL, 'PS_ADVANCED_PAYMENT_API', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, NULL, NULL, 'PS_SC_TWITTER', '0', '2019-06-08 02:34:29', '2019-06-15 16:09:51'),
(285, NULL, NULL, 'PS_SC_FACEBOOK', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(286, NULL, NULL, 'PS_SC_GOOGLE', '0', '2019-06-08 02:34:29', '2019-06-15 16:09:51'),
(287, NULL, NULL, 'PS_SC_PINTEREST', '0', '2019-06-08 02:34:29', '2019-06-15 16:09:51'),
(288, NULL, NULL, 'BLOCKBANNER_IMG', NULL, '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(289, NULL, NULL, 'BLOCKBANNER_LINK', NULL, '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(290, NULL, NULL, 'BLOCKBANNER_DESC', NULL, '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(291, NULL, NULL, 'CONF_BANKWIRE_FIXED', '0.2', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(292, NULL, NULL, 'CONF_BANKWIRE_VAR', '2', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(293, NULL, NULL, 'CONF_BANKWIRE_FIXED_FOREIGN', '0.2', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(294, NULL, NULL, 'CONF_BANKWIRE_VAR_FOREIGN', '2', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(295, NULL, NULL, 'PS_BLOCK_BESTSELLERS_TO_DISPLAY', '10', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(296, NULL, NULL, 'PS_BLOCK_CART_XSELL_LIMIT', '12', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(297, NULL, NULL, 'PS_BLOCK_CART_SHOW_CROSSSELLING', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(298, NULL, NULL, 'BLOCKSOCIAL_YOUTUBE', NULL, '2019-06-08 02:34:29', '2019-06-15 17:21:30'),
(299, NULL, NULL, 'BLOCKSOCIAL_GOOGLE_PLUS', NULL, '2019-06-08 02:34:29', '2019-06-15 17:21:30'),
(300, NULL, NULL, 'BLOCKSOCIAL_PINTEREST', NULL, '2019-06-08 02:34:29', '2019-06-15 17:21:30'),
(301, NULL, NULL, 'BLOCKSOCIAL_VIMEO', NULL, '2019-06-08 02:34:29', '2019-06-15 17:21:30'),
(302, NULL, NULL, 'BLOCKSOCIAL_INSTAGRAM', NULL, '2019-06-08 02:34:29', '2019-06-15 17:21:30'),
(303, NULL, NULL, 'BLOCK_CATEG_ROOT_CATEGORY', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(304, NULL, NULL, 'blockfacebook_url', 'https://www.facebook.com/Auto-Permis-428705551308456/', '2019-06-08 02:34:29', '2019-06-15 16:10:10'),
(305, NULL, NULL, 'PS_LAYERED_HIDE_0_VALUES', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(306, NULL, NULL, 'PS_LAYERED_SHOW_QTIES', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(307, NULL, NULL, 'PS_LAYERED_FULL_TREE', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(308, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_USETAX', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(309, NULL, NULL, 'PS_LAYERED_FILTER_CATEGORY_DEPTH', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(310, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_QTY', '0', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(311, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_CDT', '0', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(312, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_MNF', '0', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(313, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_CAT', '0', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(314, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_ROUNDING', '1', '2019-06-08 02:34:29', '2019-06-08 02:34:29'),
(315, NULL, NULL, 'PS_LAYERED_INDEXED', '1', '2019-06-08 02:34:30', '2019-06-08 02:34:30'),
(316, NULL, NULL, 'FOOTER_PRICE-DROP', '0', '2019-06-08 02:34:31', '2019-06-15 16:12:39'),
(317, NULL, NULL, 'FOOTER_NEW-PRODUCTS', '1', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(318, NULL, NULL, 'FOOTER_BEST-SALES', '0', '2019-06-08 02:34:31', '2019-06-15 16:12:39'),
(319, NULL, NULL, 'FOOTER_CONTACT', '1', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(320, NULL, NULL, 'FOOTER_SITEMAP', '1', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(321, NULL, NULL, 'PS_NEWSLETTER_RAND', '1635891519128377463', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(322, NULL, NULL, 'BLOCKSPECIALS_NB_CACHES', '0', '2019-06-08 02:34:31', '2019-06-12 01:17:24'),
(323, NULL, NULL, 'BLOCKSPECIALS_SPECIALS_NBR', '30', '2019-06-08 02:34:31', '2019-06-12 01:17:24'),
(324, NULL, NULL, 'BLOCKTAGS_MAX_LEVEL', '3', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(325, NULL, NULL, 'CONF_CHEQUE_FIXED', '0.2', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(326, NULL, NULL, 'CONF_CHEQUE_VAR', '2', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(327, NULL, NULL, 'CONF_CHEQUE_FIXED_FOREIGN', '0.2', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(328, NULL, NULL, 'CONF_CHEQUE_VAR_FOREIGN', '2', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(329, NULL, NULL, 'DASHACTIVITY_CART_ACTIVE', '30', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(330, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MIN', '24', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(331, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MAX', '48', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(332, NULL, NULL, 'DASHACTIVITY_VISITOR_ONLINE', '30', '2019-06-08 02:34:31', '2019-06-08 02:34:31'),
(333, NULL, NULL, 'PS_DASHGOALS_CURRENT_YEAR', '2019', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(334, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_LAST_ORDER', '10', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(335, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_BEST_SELLER', '10', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(336, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_MOST_VIEWED', '10', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(337, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_TOP_SEARCH', '10', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(338, NULL, NULL, 'HOME_FEATURED_CAT', '12', '2019-06-08 02:34:32', '2019-06-10 12:06:50'),
(339, NULL, NULL, 'PRODUCTPAYMENTLOGOS_IMG', 'payment-logo.png', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(340, NULL, NULL, 'PRODUCTPAYMENTLOGOS_LINK', NULL, '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(341, NULL, NULL, 'PRODUCTPAYMENTLOGOS_TITLE', NULL, '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(342, NULL, NULL, 'PS_TC_THEMES', 'a:9:{i:0;s:6:\"theme1\";i:1;s:6:\"theme2\";i:2;s:6:\"theme3\";i:3;s:6:\"theme4\";i:4;s:6:\"theme5\";i:5;s:6:\"theme6\";i:6;s:6:\"theme7\";i:7;s:6:\"theme8\";i:8;s:6:\"theme9\";}', '2019-06-08 02:34:33', '2019-06-08 02:34:33'),
(343, NULL, NULL, 'PS_TC_FONTS', 'a:10:{s:5:\"font1\";s:9:\"Open Sans\";s:5:\"font2\";s:12:\"Josefin Slab\";s:5:\"font3\";s:4:\"Arvo\";s:5:\"font4\";s:4:\"Lato\";s:5:\"font5\";s:7:\"Volkorn\";s:5:\"font6\";s:13:\"Abril Fatface\";s:5:\"font7\";s:6:\"Ubuntu\";s:5:\"font8\";s:7:\"PT Sans\";s:5:\"font9\";s:15:\"Old Standard TT\";s:6:\"font10\";s:10:\"Droid Sans\";}', '2019-06-08 02:34:33', '2019-06-08 02:34:33'),
(344, NULL, NULL, 'PS_TC_THEME', 'theme3', '2019-06-08 02:34:33', '2019-06-08 11:35:20'),
(345, NULL, NULL, 'PS_TC_FONT', 'font3', '2019-06-08 02:34:33', '2019-06-08 02:47:37'),
(346, NULL, NULL, 'PS_TC_ACTIVE', '1', '2019-06-08 02:34:33', '2019-06-08 02:34:33'),
(347, NULL, NULL, 'PS_SET_DISPLAY_SUBCATEGORIES', '1', '2019-06-08 02:34:33', '2019-06-08 02:34:33'),
(348, NULL, NULL, 'GF_INSTALL_CALC', '1', '2019-06-08 02:34:35', '2019-06-08 02:37:06'),
(349, NULL, NULL, 'GF_CURRENT_LEVEL', '2', '2019-06-08 02:34:35', '2019-06-10 21:31:01'),
(350, NULL, NULL, 'GF_CURRENT_LEVEL_PERCENT', '50', '2019-06-08 02:34:35', '2019-06-19 13:42:23'),
(351, NULL, NULL, 'GF_NOTIFICATION', '6', '2019-06-08 02:34:35', '2019-06-19 13:42:23'),
(352, NULL, NULL, 'CRONJOBS_ADMIN_DIR', 'd20b186ea66a345b5fbba6650b970407', '2019-06-08 02:34:35', '2019-06-08 02:37:00'),
(353, NULL, NULL, 'CRONJOBS_MODE', 'webservice', '2019-06-08 02:34:35', '2019-06-08 02:34:35'),
(354, NULL, NULL, 'CRONJOBS_MODULE_VERSION', '1.4.0', '2019-06-08 02:34:35', '2019-06-08 02:34:35'),
(355, NULL, NULL, 'CRONJOBS_WEBSERVICE_ID', '3643769', '2019-06-08 02:34:35', '2019-06-08 02:34:35'),
(356, NULL, NULL, 'CRONJOBS_EXECUTION_TOKEN', 'c6051d22a1792353cbb7eee491f56823', '2019-06-08 02:34:35', '2019-06-08 02:34:35'),
(357, NULL, NULL, 'GF_NOT_VIEWED_BADGE', '130', '2019-06-08 02:37:07', '2019-06-19 13:42:23'),
(358, NULL, NULL, 'PS_CSS_THEME_CACHE', '0', '2019-06-08 02:41:33', '2019-06-08 02:41:33'),
(359, NULL, NULL, 'PS_JS_THEME_CACHE', '0', '2019-06-08 02:41:33', '2019-06-08 02:41:33'),
(360, NULL, NULL, 'PS_HTML_THEME_COMPRESSION', '0', '2019-06-08 02:41:33', '2019-06-08 02:41:33'),
(361, NULL, NULL, 'PS_JS_HTML_THEME_COMPRESSION', '0', '2019-06-08 02:41:33', '2019-06-08 02:41:33'),
(362, NULL, NULL, 'PS_JS_DEFER', '0', '2019-06-08 02:41:33', '2019-06-08 02:41:33'),
(363, NULL, NULL, 'PS_HTACCESS_CACHE_CONTROL', '1', '2019-06-08 02:41:33', '2019-06-10 01:51:22'),
(364, NULL, NULL, 'PS_DISABLE_NON_NATIVE_MODULE', '0', '2019-06-08 02:41:33', '2019-06-08 02:41:33'),
(365, NULL, NULL, 'PS_DISABLE_OVERRIDES', '0', '2019-06-08 02:41:33', '2019-06-08 02:41:33'),
(366, NULL, NULL, 'PS_CCCJS_VERSION', '1', '2019-06-08 02:42:34', '2019-06-08 02:42:34'),
(367, NULL, NULL, 'PS_CCCCSS_VERSION', '1', '2019-06-08 02:42:34', '2019-06-08 02:42:34'),
(368, NULL, NULL, 'BON_SLICK_CAROUSEL_LOOP', '1', '2019-06-08 03:23:38', '2019-06-08 03:23:38'),
(369, NULL, NULL, 'BON_SLICK_CAROUSEL_NAV', '1', '2019-06-08 03:23:38', '2019-06-08 03:23:38'),
(370, NULL, NULL, 'BON_SLICK_CAROUSEL_DOTS', '1', '2019-06-08 03:23:38', '2019-06-08 03:23:38'),
(371, NULL, NULL, 'BON_SLICK_CAROUSEL_DRAG', '1', '2019-06-08 03:23:38', '2019-06-08 03:23:38'),
(372, NULL, NULL, 'BON_SLICK_CAROUSEL_AUTOPLAY', '1', '2019-06-08 03:23:38', '2019-06-08 03:23:38'),
(373, NULL, NULL, 'BON_SLICK_CAROUSEL_TIME', '5000', '2019-06-08 03:23:38', '2019-06-08 03:23:38'),
(374, NULL, NULL, 'PS_SHOW_CAT_MODULES_1', NULL, '2019-06-08 03:26:06', '2019-06-17 16:32:22'),
(375, NULL, NULL, 'CHEQUE_NAME', 'Auto Permis', '2019-06-08 03:44:27', '2019-06-08 03:44:27'),
(376, NULL, NULL, 'CHEQUE_ADDRESS', '68 rue de Suffren', '2019-06-08 03:44:27', '2019-06-08 03:44:27'),
(377, NULL, NULL, 'BANK_WIRE_DETAILS', 'azd', '2019-06-08 03:44:48', '2019-06-08 03:44:48'),
(378, NULL, NULL, 'BANK_WIRE_OWNER', 'Auto permis', '2019-06-08 03:44:48', '2019-06-08 03:44:48'),
(379, NULL, NULL, 'BANK_WIRE_ADDRESS', 'azd', '2019-06-08 03:44:48', '2019-06-08 03:44:48'),
(380, NULL, NULL, 'PS_STORES_DISPLAY_SITEMAP', '0', '2019-06-10 15:13:24', '2019-06-10 15:13:24'),
(381, NULL, NULL, 'PS_SHOP_DETAILS', NULL, '2019-06-10 15:13:24', '2019-06-10 15:13:48'),
(382, NULL, NULL, 'PS_SHOP_ADDR1', '68 rue de suffren', '2019-06-10 15:13:24', '2019-06-10 15:13:48'),
(383, NULL, NULL, 'PS_SHOP_ADDR2', NULL, '2019-06-10 15:13:24', '2019-06-10 15:13:48'),
(384, NULL, NULL, 'PS_SHOP_CODE', NULL, '2019-06-10 15:13:24', '2019-06-10 15:13:48'),
(385, NULL, NULL, 'PS_SHOP_CITY', NULL, '2019-06-10 15:13:24', '2019-06-10 15:13:48'),
(386, NULL, NULL, 'PS_SHOP_COUNTRY_ID', '176', '2019-06-10 15:13:24', '2019-06-10 15:13:24'),
(387, NULL, NULL, 'PS_SHOP_COUNTRY', 'Réunion, Île de la', '2019-06-10 15:13:24', '2019-06-10 15:13:24'),
(388, NULL, NULL, 'PS_SHOP_PHONE', NULL, '2019-06-10 15:13:24', '2019-06-10 15:13:48'),
(389, NULL, NULL, 'PS_SHOP_FAX', NULL, '2019-06-10 15:13:24', '2019-06-10 15:13:48'),
(390, NULL, NULL, 'update_htmlbox', '0', '2019-06-10 21:58:57', '2019-06-10 21:58:57'),
(391, NULL, NULL, 'htmlbox_header', '0', '2019-06-10 21:58:57', '2019-06-10 21:58:57'),
(392, NULL, NULL, 'htmlbox_top', '0', '2019-06-10 21:58:57', '2019-06-10 21:58:57'),
(393, NULL, NULL, 'htmlbox_leftcol', '0', '2019-06-10 21:58:57', '2019-06-10 21:58:57'),
(394, NULL, NULL, 'htmlbox_rightcol', '1', '2019-06-10 21:58:57', '2019-06-10 21:58:57'),
(395, NULL, NULL, 'htmlbox_footercol', '0', '2019-06-10 21:58:57', '2019-06-10 21:58:57'),
(396, NULL, NULL, 'htmlbox_homecol', '0', '2019-06-10 21:58:57', '2019-06-10 21:58:57'),
(397, NULL, NULL, 'htmlbox_body', NULL, '2019-06-10 21:58:57', '2019-06-11 01:50:14'),
(398, NULL, NULL, 'htmlbox_ssl', NULL, '2019-06-10 21:58:57', '2019-06-11 01:50:14'),
(399, NULL, NULL, 'htmlbox_hook', 'top', '2019-06-10 21:59:18', '2019-06-10 21:59:36'),
(400, NULL, NULL, 'htmlbox_home', '1', '2019-06-10 22:02:23', '2019-06-10 22:02:23'),
(401, NULL, NULL, 'PS_ALLOW_ACCENTED_CHARS_URL', '0', '2019-06-10 23:58:13', '2019-06-10 23:58:13'),
(402, NULL, NULL, 'PS_HTACCESS_DISABLE_MULTIVIEWS', '0', '2019-06-10 23:58:13', '2019-06-10 23:58:13'),
(403, NULL, NULL, 'PS_HTACCESS_DISABLE_MODSEC', '0', '2019-06-10 23:58:13', '2019-06-10 23:58:13'),
(404, NULL, NULL, 'PS_MAIL_EMAIL_MESSAGE', '2', '2019-06-12 01:28:25', '2019-06-12 01:28:25'),
(405, NULL, NULL, 'PS_MAIL_DOMAIN', NULL, '2019-06-12 01:28:25', '2019-06-12 01:28:25'),
(406, NULL, NULL, 'PS_ROUTE_product_rule', NULL, '2019-06-12 01:30:04', '2019-06-12 01:30:04'),
(407, NULL, NULL, 'PS_ROUTE_category_rule', NULL, '2019-06-12 01:30:04', '2019-06-12 01:30:04'),
(408, NULL, NULL, 'PS_ROUTE_layered_rule', NULL, '2019-06-12 01:30:04', '2019-06-12 01:30:04'),
(409, NULL, NULL, 'PS_ROUTE_supplier_rule', NULL, '2019-06-12 01:30:04', '2019-06-12 01:30:04'),
(410, NULL, NULL, 'PS_ROUTE_manufacturer_rule', NULL, '2019-06-12 01:30:04', '2019-06-12 01:30:04'),
(411, NULL, NULL, 'PS_ROUTE_cms_rule', NULL, '2019-06-12 01:30:04', '2019-06-12 01:30:04'),
(412, NULL, NULL, 'PS_ROUTE_cms_category_rule', NULL, '2019-06-12 01:30:04', '2019-06-12 01:30:04'),
(413, NULL, NULL, 'PS_ROUTE_module', 'module/{module}{/:controller}', '2019-06-12 01:30:04', '2019-06-12 01:30:04'),
(414, NULL, NULL, 'PS_SHOW_CAT_MODULES_2', 'payments_gateways', '2019-06-12 16:48:45', '2019-06-15 17:33:06'),
(415, NULL, NULL, 'PS_REFERRERS_CACHE_LIKE', ' \'2019-06-11 00:00:00\' AND \'2019-06-11 23:59:59\' ', '2019-06-15 16:01:28', '2019-06-15 16:01:28'),
(416, NULL, NULL, 'PS_REFERRERS_CACHE_DATE', '2019-06-15 16:01:28', '2019-06-15 16:01:28', '2019-06-15 16:01:28'),
(417, NULL, NULL, 'FOOTER_CMS_TEXT_1', NULL, '2019-06-15 16:12:39', '2019-06-15 16:12:58'),
(418, NULL, NULL, 'CONF_PAYPAL_FIXED', '0.2', '2019-06-15 17:30:45', '2019-06-15 17:30:45'),
(419, NULL, NULL, 'CONF_PAYPAL_VAR', '2', '2019-06-15 17:30:45', '2019-06-15 17:30:45'),
(420, NULL, NULL, 'CONF_PAYPAL_FIXED_FOREIGN', '0.2', '2019-06-15 17:30:45', '2019-06-15 17:30:45'),
(421, NULL, NULL, 'CONF_PAYPAL_VAR_FOREIGN', '2', '2019-06-15 17:30:45', '2019-06-15 17:30:45'),
(422, NULL, NULL, 'PAYPAL_SANDBOX', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(423, NULL, NULL, 'PAYPAL_HEADER', NULL, '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(424, NULL, NULL, 'PAYPAL_BUSINESS', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(425, NULL, NULL, 'PAYPAL_BUSINESS_ACCOUNT', 'paypal@prestashop.com', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(426, NULL, NULL, 'PAYPAL_API_USER', NULL, '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(427, NULL, NULL, 'PAYPAL_API_PASSWORD', NULL, '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(428, NULL, NULL, 'PAYPAL_API_SIGNATURE', NULL, '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(429, NULL, NULL, 'PAYPAL_EXPRESS_CHECKOUT', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(430, NULL, NULL, 'PAYPAL_CAPTURE', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(431, NULL, NULL, 'PAYPAL_PAYMENT_METHOD', '1', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(432, NULL, NULL, 'PAYPAL_NEW', '1', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(433, NULL, NULL, 'PAYPAL_DEBUG_MODE', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(434, NULL, NULL, 'PAYPAL_SHIPPING_COST', '20', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(435, NULL, NULL, 'PAYPAL_VERSION', '3.12.1', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(436, NULL, NULL, 'PAYPAL_COUNTRY_DEFAULT', '176', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(437, NULL, NULL, 'PAYPAL_USE_3D_SECURE', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(438, NULL, NULL, 'PAYPAL_EXPRESS_CHECKOUT_SHORTCUT', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(439, NULL, NULL, 'VZERO_ENABLED', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(440, NULL, NULL, 'PAYPAL_BRAINTREE_ENABLED', '0', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(441, NULL, NULL, 'PAYPAL_OS_AUTHORIZATION', '15', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(442, NULL, NULL, 'PAYPAL_BT_OS_AUTHORIZATION', '16', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(443, NULL, NULL, 'PAYPAL_BRAINTREE_OS_AWAITING', '17', '2019-06-15 17:30:46', '2019-06-15 17:30:46'),
(444, NULL, NULL, 'PAYPAL_OS_AWAITING_HSS', '18', '2019-06-15 17:30:46', '2019-06-15 17:30:46');

-- --------------------------------------------------------

--
-- Table structure for table `ap_configuration_kpi`
--

CREATE TABLE `ap_configuration_kpi` (
  `id_configuration_kpi` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED DEFAULT NULL,
  `id_shop` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_configuration_kpi`
--

INSERT INTO `ap_configuration_kpi` (`id_configuration_kpi`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(1, NULL, NULL, 'DASHGOALS_TRAFFIC_01_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(2, NULL, NULL, 'DASHGOALS_CONVERSION_01_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(3, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_01_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(4, NULL, NULL, 'DASHGOALS_TRAFFIC_02_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(5, NULL, NULL, 'DASHGOALS_CONVERSION_02_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(6, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_02_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(7, NULL, NULL, 'DASHGOALS_TRAFFIC_03_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(8, NULL, NULL, 'DASHGOALS_CONVERSION_03_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(9, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_03_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(10, NULL, NULL, 'DASHGOALS_TRAFFIC_04_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(11, NULL, NULL, 'DASHGOALS_CONVERSION_04_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(12, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_04_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(13, NULL, NULL, 'DASHGOALS_TRAFFIC_05_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(14, NULL, NULL, 'DASHGOALS_CONVERSION_05_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(15, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_05_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(16, NULL, NULL, 'DASHGOALS_TRAFFIC_06_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(17, NULL, NULL, 'DASHGOALS_CONVERSION_06_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(18, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_06_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(19, NULL, NULL, 'DASHGOALS_TRAFFIC_07_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(20, NULL, NULL, 'DASHGOALS_CONVERSION_07_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(21, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_07_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(22, NULL, NULL, 'DASHGOALS_TRAFFIC_08_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(23, NULL, NULL, 'DASHGOALS_CONVERSION_08_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(24, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_08_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(25, NULL, NULL, 'DASHGOALS_TRAFFIC_09_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(26, NULL, NULL, 'DASHGOALS_CONVERSION_09_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(27, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_09_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(28, NULL, NULL, 'DASHGOALS_TRAFFIC_10_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(29, NULL, NULL, 'DASHGOALS_CONVERSION_10_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(30, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_10_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(31, NULL, NULL, 'DASHGOALS_TRAFFIC_11_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(32, NULL, NULL, 'DASHGOALS_CONVERSION_11_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(33, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_11_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(34, NULL, NULL, 'DASHGOALS_TRAFFIC_12_2019', '600', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(35, NULL, NULL, 'DASHGOALS_CONVERSION_12_2019', '2', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(36, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_12_2019', '80', '2019-06-08 02:34:32', '2019-06-08 02:34:32'),
(37, NULL, NULL, 'UPDATE_MODULES', '0', '2019-06-08 02:37:23', '2019-06-08 02:37:23');

-- --------------------------------------------------------

--
-- Table structure for table `ap_configuration_kpi_lang`
--

CREATE TABLE `ap_configuration_kpi_lang` (
  `id_configuration_kpi` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_configuration_lang`
--

CREATE TABLE `ap_configuration_lang` (
  `id_configuration` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_configuration_lang`
--

INSERT INTO `ap_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
(41, 1, '#FA', NULL),
(44, 1, '#LI', NULL),
(46, 1, '#RE', NULL),
(52, 1, 'alors|au|aucuns|aussi|autre|avant|avec|avoir|bon|car|ce|cela|ces|ceux|chaque|ci|comme|comment|dans|des|du|dedans|dehors|depuis|deux|devrait|doit|donc|dos|droite|début|elle|elles|en|encore|essai|est|et|eu|fait|faites|fois|font|force|haut|hors|ici|il|ils|je|juste|la|le|les|leur|là|ma|maintenant|mais|mes|mine|moins|mon|mot|même|ni|nommés|notre|nous|nouveaux|ou|où|par|parce|parole|pas|personnes|peut|peu|pièce|plupart|pour|pourquoi|quand|que|quel|quelle|quelles|quels|qui|sa|sans|ses|seulement|si|sien|son|sont|sous|soyez|sujet|sur|ta|tandis|tellement|tels|tes|ton|tous|tout|trop|très|tu|valeur|voie|voient|vont|votre|vous|vu|ça|étaient|état|étions|été|être', NULL),
(74, 1, '0', NULL),
(80, 1, 'Chère cliente, cher client,\n\nCordialement,\nLe service client', NULL),
(288, 1, 'sale70.png', '2019-06-08 02:34:29'),
(289, 1, '', '2019-06-08 02:34:29'),
(290, 1, '', '2019-06-08 02:34:29');

-- --------------------------------------------------------

--
-- Table structure for table `ap_connections`
--

CREATE TABLE `ap_connections` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_guest` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `ip_address` bigint(20) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_connections`
--

INSERT INTO `ap_connections` (`id_connections`, `id_shop_group`, `id_shop`, `id_guest`, `id_page`, `ip_address`, `date_add`, `http_referer`) VALUES
(1, 1, 1, 1, 1, 2130706433, '2019-06-08 02:34:27', 'https://www.prestashop.com'),
(2, 1, 1, 2, 1, 2779329714, '2019-06-08 02:36:01', ''),
(3, 1, 1, 3, 1, 1542483201, '2019-06-08 03:30:51', ''),
(4, 1, 1, 2, 1, 2779329714, '2019-06-08 03:33:05', ''),
(5, 1, 1, 2, 2, 2779329714, '2019-06-08 04:50:10', ''),
(6, 1, 1, 2, 1, 1123638594, '2019-06-08 06:07:16', ''),
(7, 1, 1, 2, 1, 2779329714, '2019-06-08 11:36:06', ''),
(8, 1, 1, 8, 1, 2588137271, '2019-06-08 13:11:49', ''),
(9, 1, 1, 9, 1, 2588137271, '2019-06-08 13:21:40', ''),
(10, 1, 1, 10, 1, 1542483201, '2019-06-09 03:32:04', ''),
(11, 1, 1, 2, 1, 2588137271, '2019-06-09 22:19:13', ''),
(12, 1, 1, 2, 1, 2779329714, '2019-06-10 01:15:54', ''),
(13, 1, 1, 2, 1, 2779329714, '2019-06-10 03:10:13', ''),
(14, 1, 1, 12, 1, 1542483201, '2019-06-10 03:32:05', ''),
(15, 1, 1, 2, 1, 2779329714, '2019-06-10 04:19:35', ''),
(16, 1, 1, 2, 1, 1123638594, '2019-06-10 05:04:47', ''),
(17, 1, 1, 2, 1, 2779329714, '2019-06-10 14:30:21', ''),
(18, 1, 1, 2, 1, 2779329714, '2019-06-10 15:42:32', ''),
(19, 1, 1, 17, 1, 2588137271, '2019-06-10 16:44:46', ''),
(20, 1, 1, 17, 1, 2588137271, '2019-06-10 18:02:47', ''),
(21, 1, 1, 17, 1, 2779329714, '2019-06-10 18:42:33', ''),
(22, 1, 1, 16, 1, 2588137271, '2019-06-10 19:09:26', ''),
(23, 1, 1, 17, 1, 2588137271, '2019-06-10 19:09:27', ''),
(24, 1, 1, 17, 2, 2588137271, '2019-06-10 19:16:35', ''),
(25, 1, 1, 17, 2, 2779329714, '2019-06-10 19:19:33', ''),
(26, 1, 1, 17, 1, 2588137271, '2019-06-10 21:22:27', ''),
(27, 1, 1, 20, 1, 2779329714, '2019-06-10 21:40:52', ''),
(28, 1, 1, 21, 1, 2588137271, '2019-06-10 23:33:07', ''),
(29, 1, 1, 20, 1, 2779329714, '2019-06-10 23:49:11', ''),
(30, 1, 1, 22, 1, 2779329714, '2019-06-10 23:58:51', ''),
(31, 1, 1, 2, 1, 2779329714, '2019-06-11 00:07:10', ''),
(32, 1, 1, 24, 1, 520978215, '2019-06-11 00:07:54', ''),
(33, 1, 1, 25, 1, 520978208, '2019-06-11 00:07:55', ''),
(34, 1, 1, 26, 1, 520975115, '2019-06-11 00:08:04', ''),
(35, 1, 1, 2, 1, 1123638735, '2019-06-11 00:12:55', ''),
(36, 1, 1, 28, 1, 520978196, '2019-06-11 00:43:44', ''),
(37, 1, 1, 29, 1, 520978178, '2019-06-11 00:43:44', ''),
(38, 1, 1, 30, 1, 520978196, '2019-06-11 00:43:44', ''),
(39, 1, 1, 31, 1, 520978188, '2019-06-11 00:43:45', ''),
(40, 1, 1, 2, 1, 2779329714, '2019-06-11 01:42:09', ''),
(41, 1, 1, 33, 1, 1542483201, '2019-06-11 03:40:00', ''),
(42, 1, 1, 34, 1, 2588137271, '2019-06-11 09:01:52', ''),
(43, 1, 1, 35, 1, 520975121, '2019-06-11 12:36:36', ''),
(44, 1, 1, 2, 1, 2779329714, '2019-06-11 13:23:45', ''),
(45, 1, 1, 36, 1, 2779329714, '2019-06-11 13:37:33', ''),
(46, 1, 1, 17, 1, 2779329714, '2019-06-11 13:41:16', ''),
(47, 1, 1, 34, 1, 2588137271, '2019-06-11 14:10:45', ''),
(48, 1, 1, 34, 1, 2588137271, '2019-06-11 15:09:09', ''),
(49, 1, 1, 17, 2, 2779329714, '2019-06-11 15:19:35', ''),
(50, 1, 1, 34, 1, 2588137271, '2019-06-11 15:43:57', ''),
(51, 1, 1, 34, 1, 2588137271, '2019-06-11 16:32:19', ''),
(52, 1, 1, 34, 1, 2588137271, '2019-06-11 17:04:57', ''),
(53, 1, 1, 34, 1, 2588137271, '2019-06-11 17:41:07', ''),
(54, 1, 1, 39, 1, 2588137271, '2019-06-11 18:25:54', 'http://m.facebook.com/'),
(55, 1, 1, 39, 1, 2588137271, '2019-06-11 21:58:33', 'http://m.facebook.com/'),
(56, 1, 1, 2, 1, 1123638735, '2019-06-11 22:01:25', ''),
(57, 1, 1, 40, 1, 1191612250, '2019-06-11 22:55:01', 'http://79.137.76.201:80/'),
(58, 1, 1, 17, 1, 2779329714, '2019-06-12 00:19:26', ''),
(59, 1, 1, 2, 1, 2779329714, '2019-06-12 01:20:35', ''),
(60, 1, 1, 42, 1, 520978180, '2019-06-12 01:33:19', ''),
(61, 1, 1, 43, 1, 520978196, '2019-06-12 01:33:23', ''),
(62, 1, 1, 44, 1, 1302626984, '2019-06-12 01:33:26', 'http://m.facebook.com'),
(63, 1, 1, 45, 1, 2779329714, '2019-06-12 01:33:52', 'http://m.facebook.com/'),
(64, 1, 1, 2, 1, 2779329714, '2019-06-12 02:03:59', ''),
(65, 1, 1, 46, 1, 1542483201, '2019-06-12 03:43:45', ''),
(66, 1, 1, 17, 1, 2588137271, '2019-06-12 15:01:54', ''),
(67, 1, 1, 17, 1, 2588137271, '2019-06-12 15:51:46', ''),
(68, 1, 1, 48, 1, 2588137271, '2019-06-12 16:14:53', ''),
(69, 1, 1, 34, 1, 2588137271, '2019-06-12 16:16:42', ''),
(70, 1, 1, 49, 1, 2588137271, '2019-06-12 17:03:38', ''),
(71, 1, 1, 50, 1, 1118171042, '2019-06-12 22:48:44', 'http://79.137.76.201/index.php'),
(72, 1, 1, 51, 1, 1118171042, '2019-06-12 22:48:45', ''),
(73, 1, 1, 52, 1, 1118171042, '2019-06-12 22:48:52', 'http://79.137.76.201:80'),
(74, 1, 1, 49, 1, 2588137271, '2019-06-12 22:52:51', ''),
(75, 1, 1, 2, 1, 2779329714, '2019-06-12 23:27:06', ''),
(76, 1, 1, 2, 1, 2779329714, '2019-06-13 02:00:00', ''),
(77, 1, 1, 54, 1, 1542483201, '2019-06-13 03:45:38', ''),
(78, 1, 1, 55, 3, 1836746851, '2019-06-13 10:18:40', 'http://79.137.76.201/index.php?'),
(79, 1, 1, 56, 1, 2016712724, '2019-06-13 12:14:45', 'http://79.137.76.201'),
(80, 1, 1, 57, 1, 2016712724, '2019-06-13 12:14:58', ''),
(81, 1, 1, 58, 1, 720112242, '2019-06-13 13:15:28', ''),
(82, 1, 1, 59, 1, 2869759496, '2019-06-13 14:35:40', ''),
(83, 1, 1, 60, 1, 2869759525, '2019-06-13 14:37:20', ''),
(84, 1, 1, 2, 1, 2779329714, '2019-06-13 16:01:29', ''),
(85, 1, 1, 61, 1, 918468838, '2019-06-13 16:38:12', ''),
(86, 1, 1, 62, 1, 2745989025, '2019-06-13 17:39:06', ''),
(87, 1, 1, 2, 1, 2779329714, '2019-06-13 18:40:01', ''),
(88, 1, 1, 2, 1, 2779329714, '2019-06-13 19:12:33', ''),
(89, 1, 1, 49, 1, 2588137271, '2019-06-13 20:48:19', ''),
(90, 1, 1, 63, 1, 2588137271, '2019-06-13 20:49:44', ''),
(91, 1, 1, 17, 1, 2588137271, '2019-06-13 20:49:45', ''),
(92, 1, 1, 65, 1, 1542483201, '2019-06-14 03:46:33', ''),
(93, 1, 1, 66, 1, 3091747387, '2019-06-14 09:37:23', ''),
(94, 1, 1, 67, 1, 1564451428, '2019-06-14 13:40:45', ''),
(95, 1, 1, 68, 1, 585005709, '2019-06-14 14:46:07', ''),
(96, 1, 1, 69, 1, 3453071058, '2019-06-14 15:30:21', 'http://79.137.76.201/'),
(97, 1, 1, 70, 1, 3453071058, '2019-06-14 15:30:23', 'http://79.137.76.201/'),
(98, 1, 1, 2, 1, 2779329714, '2019-06-14 17:30:06', ''),
(99, 1, 1, 2, 1, 2779329714, '2019-06-14 19:12:15', ''),
(100, 1, 1, 55, 1, 2588137271, '2019-06-14 22:17:56', ''),
(101, 1, 1, 17, 1, 2588137271, '2019-06-14 22:21:06', ''),
(102, 1, 1, 71, 1, 1542483201, '2019-06-15 03:50:09', ''),
(103, 1, 1, 17, 1, 2588137271, '2019-06-15 10:11:02', ''),
(104, 1, 1, 2, 1, 2588137271, '2019-06-15 14:33:23', ''),
(105, 1, 1, 17, 1, 2588137271, '2019-06-15 14:43:50', ''),
(106, 1, 1, 2, 1, 2588137271, '2019-06-15 15:01:48', ''),
(107, 1, 1, 17, 1, 2588137271, '2019-06-15 15:24:13', ''),
(108, 1, 1, 2, 1, 2588137271, '2019-06-15 15:46:06', ''),
(109, 1, 1, 73, 1, 2588137271, '2019-06-15 16:32:06', ''),
(110, 1, 1, 74, 1, 2588137271, '2019-06-15 16:33:07', ''),
(111, 1, 1, 75, 1, 1561852061, '2019-06-15 17:01:49', ''),
(112, 1, 1, 48, 1, 2779329714, '2019-06-15 18:42:21', ''),
(113, 1, 1, 76, 1, 520978182, '2019-06-15 19:01:45', ''),
(114, 1, 1, 48, 1, 2779329714, '2019-06-15 20:02:46', ''),
(115, 1, 1, 77, 1, 520975128, '2019-06-15 20:03:35', ''),
(116, 1, 1, 78, 1, 2588137271, '2019-06-15 20:03:37', ''),
(117, 1, 1, 79, 1, 2779329714, '2019-06-15 20:03:40', 'http://m.facebook.com/'),
(118, 1, 1, 80, 1, 2588137271, '2019-06-15 20:40:54', ''),
(119, 1, 1, 74, 1, 2588137271, '2019-06-15 20:46:29', ''),
(120, 1, 1, 81, 1, 865999165, '2019-06-15 23:12:28', ''),
(121, 1, 1, 82, 1, 1542483201, '2019-06-16 03:51:01', ''),
(122, 1, 1, 2, 1, 2779329714, '2019-06-16 12:44:49', ''),
(123, 1, 1, 83, 1, 1588720285, '2019-06-16 19:10:37', ''),
(124, 1, 1, 84, 1, 1542483201, '2019-06-17 03:52:07', ''),
(125, 1, 1, 85, 1, 1342978699, '2019-06-17 09:22:25', ''),
(126, 1, 1, 86, 1, 2779329714, '2019-06-17 11:42:12', ''),
(127, 1, 1, 86, 1, 2588137271, '2019-06-17 16:04:05', ''),
(128, 1, 1, 74, 1, 2588137271, '2019-06-17 16:05:15', ''),
(129, 1, 1, 87, 1, 1123636191, '2019-06-17 16:11:44', ''),
(130, 1, 1, 88, 1, 1113982525, '2019-06-17 16:11:44', ''),
(131, 1, 1, 89, 1, 1123637342, '2019-06-17 16:11:45', ''),
(132, 1, 1, 90, 1, 1113982527, '2019-06-17 16:12:08', ''),
(133, 1, 1, 91, 1, 1123637312, '2019-06-17 16:12:10', ''),
(134, 1, 1, 92, 4, 2588137271, '2019-06-17 16:25:05', ''),
(135, 1, 1, 93, 1, 2588137271, '2019-06-17 16:29:11', ''),
(136, 1, 1, 2, 2, 2588137271, '2019-06-17 16:37:57', ''),
(137, 1, 1, 85, 1, 1342978699, '2019-06-17 18:16:28', ''),
(138, 1, 1, 95, 1, 919225753, '2019-06-17 18:18:43', ''),
(139, 1, 1, 2, 1, 2779329714, '2019-06-17 20:04:44', ''),
(140, 1, 1, 96, 1, 1051564560, '2019-06-18 02:47:56', ''),
(141, 1, 1, 97, 1, 1542483201, '2019-06-18 03:51:46', ''),
(142, 1, 1, 98, 1, 2588137271, '2019-06-18 07:58:04', ''),
(143, 1, 1, 92, 1, 2588137271, '2019-06-18 08:01:26', ''),
(144, 1, 1, 17, 1, 2588137271, '2019-06-18 08:28:22', ''),
(145, 1, 1, 85, 1, 1555590157, '2019-06-18 09:41:21', ''),
(146, 1, 1, 100, 1, 2809324382, '2019-06-18 09:44:51', ''),
(147, 1, 1, 101, 1, 2809324382, '2019-06-18 09:45:04', ''),
(148, 1, 1, 85, 1, 1555590157, '2019-06-18 10:22:42', ''),
(149, 1, 1, 102, 1, 2448411791, '2019-06-18 12:19:39', ''),
(150, 1, 1, 103, 1, 918322347, '2019-06-18 14:53:15', ''),
(151, 1, 1, 104, 1, 1342978630, '2019-06-18 17:11:13', ''),
(152, 1, 1, 105, 1, 1371122751, '2019-06-18 17:28:27', ''),
(153, 1, 1, 2, 1, 1371122751, '2019-06-18 17:45:31', ''),
(154, 1, 1, 107, 1, 1342978757, '2019-06-18 18:21:07', ''),
(155, 1, 1, 108, 1, 1779209764, '2019-06-18 19:03:17', 'http://79.137.76.201/index.php'),
(156, 1, 1, 109, 1, 1779209764, '2019-06-18 19:03:18', ''),
(157, 1, 1, 110, 1, 1779209764, '2019-06-18 19:03:19', 'http://79.137.76.201/index.php?s=captcha'),
(158, 1, 1, 111, 1, 1779209764, '2019-06-18 19:03:21', 'http://79.137.76.201:80'),
(159, 1, 1, 2, 1, 1371122751, '2019-06-18 19:29:15', ''),
(160, 1, 1, 105, 1, 1371122751, '2019-06-18 19:29:36', ''),
(161, 1, 1, 114, 5, 2588137271, '2019-06-18 19:30:02', ''),
(162, 1, 1, 98, 1, 2588137271, '2019-06-18 19:33:30', ''),
(163, 1, 1, 98, 1, 2588137271, '2019-06-18 20:06:09', ''),
(164, 1, 1, 115, 1, 1191612250, '2019-06-18 20:51:50', 'http://79.137.76.201:80/'),
(165, 1, 1, 114, 1, 2588137271, '2019-06-18 22:33:46', ''),
(166, 1, 1, 114, 1, 2588137271, '2019-06-18 23:38:50', ''),
(167, 1, 1, 116, 1, 1542483201, '2019-06-19 03:55:07', ''),
(168, 1, 1, 105, 1, 1371123306, '2019-06-19 07:24:55', ''),
(169, 1, 1, 114, 1, 2588137271, '2019-06-19 12:53:43', ''),
(170, 1, 1, 2, 1, 1123638737, '2019-06-19 13:37:01', ''),
(171, 1, 1, 117, 1, 317584344, '2019-06-19 15:04:49', ''),
(172, 1, 1, 118, 1, 584776303, '2019-06-19 15:12:16', ''),
(173, 1, 1, 119, 1, 584181441, '2019-06-19 15:12:19', ''),
(174, 1, 1, 114, 1, 2588137271, '2019-06-19 16:38:34', ''),
(175, 1, 1, 105, 1, 1300808874, '2019-06-19 18:53:29', ''),
(176, 1, 1, 105, 1, 1300808762, '2019-06-19 20:24:20', ''),
(177, 1, 1, 120, 1, 315360109, '2019-06-19 20:35:57', ''),
(178, 1, 1, 2, 1, 1300808762, '2019-06-19 21:05:48', ''),
(179, 1, 1, 122, 1, 1542483201, '2019-06-20 03:53:14', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_connections_page`
--

CREATE TABLE `ap_connections_page` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_connections_source`
--

CREATE TABLE `ap_connections_source` (
  `id_connections_source` int(10) UNSIGNED NOT NULL,
  `id_connections` int(10) UNSIGNED NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `request_uri` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_connections_source`
--

INSERT INTO `ap_connections_source` (`id_connections_source`, `id_connections`, `http_referer`, `request_uri`, `keywords`, `date_add`) VALUES
(1, 6, 'http://79.137.76.201/autopermis/', '79.137.76.201/autopermis/prestashop/index.php', '', '2019-06-08 06:07:16'),
(2, 6, 'http://79.137.76.201/autopermis/', '79.137.76.201/autopermis/prestashop/index.php', '', '2019-06-08 06:07:57'),
(3, 8, 'http://79.137.76.201/autopermis/', '79.137.76.201/autopermis/prestashop/index.php', '', '2019-06-08 13:11:49'),
(4, 9, 'http://79.137.76.201/autopermis/', '79.137.76.201/autopermis/prestashop/index.php', '', '2019-06-08 13:21:40'),
(5, 6, 'http://79.137.76.201/autopermis/', '79.137.76.201/autopermis/prestashop/index.php', '', '2019-06-08 13:23:13'),
(6, 21, 'http://79.137.76.201/autopermis/', '79.137.76.201/autopermis/prestashop/index.php', '', '2019-06-10 19:09:22'),
(7, 22, 'http://79.137.76.201/autopermis/', '79.137.76.201/autopermis/prestashop/index.php', '', '2019-06-10 19:09:26'),
(8, 23, 'http://79.137.76.201/autopermis/', '79.137.76.201/autopermis/prestashop/index.php', '', '2019-06-10 19:09:27'),
(9, 29, 'http://www.autopermis.re/autopermis/', '79.137.76.201/autopermis/prestashop/index.php?', '', '2019-06-10 23:57:17'),
(10, 54, 'http://m.facebook.com/', 'www.autopermis.re/index.php', '', '2019-06-11 18:25:54'),
(11, 55, 'http://m.facebook.com/', 'www.autopermis.re/index.php', '', '2019-06-11 21:58:33'),
(12, 55, 'http://m.facebook.com/', 'www.autopermis.re/index.php', '', '2019-06-11 21:59:54'),
(13, 57, 'http://79.137.76.201:80/', 'www.autopermis.re/index.php?', '', '2019-06-11 22:55:01'),
(14, 63, 'http://m.facebook.com/', 'www.autopermis.re/index.php', '', '2019-06-12 01:33:52'),
(15, 63, 'http://m.facebook.com/', 'www.autopermis.re/index.php', '', '2019-06-12 01:38:52'),
(16, 71, 'http://79.137.76.201/index.php', 'www.autopermis.re/index.php?', '', '2019-06-12 22:48:44'),
(17, 78, 'http://79.137.76.201/index.php?', 'www.autopermis.re/index.php?id_product=37&id_product_attribute=0&rewrite=forfait-40-heures-&controller=product', '', '2019-06-13 10:18:40'),
(18, 78, 'http://79.137.76.201/index.php?', 'www.autopermis.re/index.php?id_product=37&id_product_attribute=0&rewrite=forfait-40-heures-&controller=product', '', '2019-06-13 10:19:09'),
(19, 78, 'http://79.137.76.201/index.php?', 'www.autopermis.re/index.php?id_product=37&id_product_attribute=0&rewrite=forfait-40-heures-&controller=product', '', '2019-06-13 10:21:47'),
(20, 96, 'http://79.137.76.201/', 'www.autopermis.re/index.php?', '', '2019-06-14 15:30:21'),
(21, 97, 'http://79.137.76.201/', 'www.autopermis.re/index.php?', '', '2019-06-14 15:30:23'),
(22, 117, 'http://m.facebook.com/', 'www.autopermis.re/index.php', '', '2019-06-15 20:03:40'),
(23, 155, 'http://79.137.76.201/index.php', 'www.autopermis.re/index.php?', '', '2019-06-18 19:03:17'),
(24, 157, 'http://79.137.76.201/index.php?s=captcha', 'www.autopermis.re/index.php?s=captcha', '', '2019-06-18 19:03:19'),
(25, 164, 'http://79.137.76.201:80/', 'www.autopermis.re/index.php?', '', '2019-06-18 20:51:50');

-- --------------------------------------------------------

--
-- Table structure for table `ap_contact`
--

CREATE TABLE `ap_contact` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `email` varchar(128) NOT NULL,
  `customer_service` tinyint(1) NOT NULL DEFAULT '0',
  `position` tinyint(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_contact`
--

INSERT INTO `ap_contact` (`id_contact`, `email`, `customer_service`, `position`) VALUES
(1, 'carl.debrauwere@gmail.com', 1, 0),
(2, 'carl.debrauwere@gmail.com', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_contact_lang`
--

CREATE TABLE `ap_contact_lang` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_contact_lang`
--

INSERT INTO `ap_contact_lang` (`id_contact`, `id_lang`, `name`, `description`) VALUES
(1, 1, 'Webmaster', 'En cas de problème technique sur ce site'),
(2, 1, 'Service client', 'Pour toute question sur un produit ou une commande');

-- --------------------------------------------------------

--
-- Table structure for table `ap_contact_shop`
--

CREATE TABLE `ap_contact_shop` (
  `id_contact` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_contact_shop`
--

INSERT INTO `ap_contact_shop` (`id_contact`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_country`
--

CREATE TABLE `ap_country` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `iso_code` varchar(3) NOT NULL,
  `call_prefix` int(10) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `contains_states` tinyint(1) NOT NULL DEFAULT '0',
  `need_identification_number` tinyint(1) NOT NULL DEFAULT '0',
  `need_zip_code` tinyint(1) NOT NULL DEFAULT '1',
  `zip_code_format` varchar(12) NOT NULL DEFAULT '',
  `display_tax_label` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_country`
--

INSERT INTO `ap_country` (`id_country`, `id_zone`, `id_currency`, `iso_code`, `call_prefix`, `active`, `contains_states`, `need_identification_number`, `need_zip_code`, `zip_code_format`, `display_tax_label`) VALUES
(1, 1, 0, 'DE', 49, 0, 0, 0, 1, 'NNNNN', 1),
(2, 1, 0, 'AT', 43, 0, 0, 0, 1, 'NNNN', 1),
(3, 1, 0, 'BE', 32, 0, 0, 0, 1, 'NNNN', 1),
(4, 2, 0, 'CA', 1, 0, 1, 0, 1, 'LNL NLN', 0),
(5, 3, 0, 'CN', 86, 0, 0, 0, 1, 'NNNNNN', 1),
(6, 1, 0, 'ES', 34, 0, 0, 1, 1, 'NNNNN', 1),
(7, 1, 0, 'FI', 358, 0, 0, 0, 1, 'NNNNN', 1),
(8, 1, 0, 'FR', 33, 0, 0, 0, 1, 'NNNNN', 1),
(9, 1, 0, 'GR', 30, 0, 0, 0, 1, 'NNNNN', 1),
(10, 1, 0, 'IT', 39, 0, 1, 0, 1, 'NNNNN', 1),
(11, 3, 0, 'JP', 81, 0, 1, 0, 1, 'NNN-NNNN', 1),
(12, 1, 0, 'LU', 352, 0, 0, 0, 1, 'NNNN', 1),
(13, 1, 0, 'NL', 31, 0, 0, 0, 1, 'NNNN LL', 1),
(14, 1, 0, 'PL', 48, 0, 0, 0, 1, 'NN-NNN', 1),
(15, 1, 0, 'PT', 351, 0, 0, 0, 1, 'NNNN-NNN', 1),
(16, 1, 0, 'CZ', 420, 0, 0, 0, 1, 'NNN NN', 1),
(17, 1, 0, 'GB', 44, 0, 0, 0, 1, '', 1),
(18, 1, 0, 'SE', 46, 0, 0, 0, 1, 'NNN NN', 1),
(19, 7, 0, 'CH', 41, 0, 0, 0, 1, 'NNNN', 1),
(20, 1, 0, 'DK', 45, 0, 0, 0, 1, 'NNNN', 1),
(21, 2, 0, 'US', 1, 0, 1, 0, 1, 'NNNNN', 0),
(22, 3, 0, 'HK', 852, 0, 0, 0, 0, '', 1),
(23, 7, 0, 'NO', 47, 0, 0, 0, 1, 'NNNN', 1),
(24, 5, 0, 'AU', 61, 0, 0, 0, 1, 'NNNN', 1),
(25, 3, 0, 'SG', 65, 0, 0, 0, 1, 'NNNNNN', 1),
(26, 1, 0, 'IE', 353, 0, 0, 0, 0, '', 1),
(27, 5, 0, 'NZ', 64, 0, 0, 0, 1, 'NNNN', 1),
(28, 3, 0, 'KR', 82, 0, 0, 0, 1, 'NNNNN', 1),
(29, 3, 0, 'IL', 972, 0, 0, 0, 1, 'NNNNNNN', 1),
(30, 4, 0, 'ZA', 27, 0, 0, 0, 1, 'NNNN', 1),
(31, 4, 0, 'NG', 234, 0, 0, 0, 1, '', 1),
(32, 4, 0, 'CI', 225, 0, 0, 0, 1, '', 1),
(33, 4, 0, 'TG', 228, 0, 0, 0, 1, '', 1),
(34, 6, 0, 'BO', 591, 0, 0, 0, 1, '', 1),
(35, 4, 0, 'MU', 230, 0, 0, 0, 1, '', 1),
(36, 1, 0, 'RO', 40, 0, 0, 0, 1, 'NNNNNN', 1),
(37, 1, 0, 'SK', 421, 0, 0, 0, 1, 'NNN NN', 1),
(38, 4, 0, 'DZ', 213, 0, 0, 0, 1, 'NNNNN', 1),
(39, 2, 0, 'AS', 0, 0, 0, 0, 1, '', 1),
(40, 7, 0, 'AD', 376, 0, 0, 0, 1, 'CNNN', 1),
(41, 4, 0, 'AO', 244, 0, 0, 0, 0, '', 1),
(42, 8, 0, 'AI', 0, 0, 0, 0, 1, '', 1),
(43, 2, 0, 'AG', 0, 0, 0, 0, 1, '', 1),
(44, 6, 0, 'AR', 54, 0, 1, 0, 1, 'LNNNNLLL', 1),
(45, 3, 0, 'AM', 374, 0, 0, 0, 1, 'NNNN', 1),
(46, 8, 0, 'AW', 297, 0, 0, 0, 1, '', 1),
(47, 3, 0, 'AZ', 994, 0, 0, 0, 1, 'CNNNN', 1),
(48, 2, 0, 'BS', 0, 0, 0, 0, 1, '', 1),
(49, 3, 0, 'BH', 973, 0, 0, 0, 1, '', 1),
(50, 3, 0, 'BD', 880, 0, 0, 0, 1, 'NNNN', 1),
(51, 2, 0, 'BB', 0, 0, 0, 0, 1, 'CNNNNN', 1),
(52, 7, 0, 'BY', 0, 0, 0, 0, 1, 'NNNNNN', 1),
(53, 8, 0, 'BZ', 501, 0, 0, 0, 0, '', 1),
(54, 4, 0, 'BJ', 229, 0, 0, 0, 0, '', 1),
(55, 2, 0, 'BM', 0, 0, 0, 0, 1, '', 1),
(56, 3, 0, 'BT', 975, 0, 0, 0, 1, '', 1),
(57, 4, 0, 'BW', 267, 0, 0, 0, 1, '', 1),
(58, 6, 0, 'BR', 55, 0, 0, 0, 1, 'NNNNN-NNN', 1),
(59, 3, 0, 'BN', 673, 0, 0, 0, 1, 'LLNNNN', 1),
(60, 4, 0, 'BF', 226, 0, 0, 0, 1, '', 1),
(61, 3, 0, 'MM', 95, 0, 0, 0, 1, '', 1),
(62, 4, 0, 'BI', 257, 0, 0, 0, 1, '', 1),
(63, 3, 0, 'KH', 855, 0, 0, 0, 1, 'NNNNN', 1),
(64, 4, 0, 'CM', 237, 0, 0, 0, 1, '', 1),
(65, 4, 0, 'CV', 238, 0, 0, 0, 1, 'NNNN', 1),
(66, 4, 0, 'CF', 236, 0, 0, 0, 1, '', 1),
(67, 4, 0, 'TD', 235, 0, 0, 0, 1, '', 1),
(68, 6, 0, 'CL', 56, 0, 0, 0, 1, 'NNN-NNNN', 1),
(69, 6, 0, 'CO', 57, 0, 0, 0, 1, 'NNNNNN', 1),
(70, 4, 0, 'KM', 269, 0, 0, 0, 1, '', 1),
(71, 4, 0, 'CD', 242, 0, 0, 0, 1, '', 1),
(72, 4, 0, 'CG', 243, 0, 0, 0, 1, '', 1),
(73, 8, 0, 'CR', 506, 0, 0, 0, 1, 'NNNNN', 1),
(74, 7, 0, 'HR', 385, 0, 0, 0, 1, 'NNNNN', 1),
(75, 8, 0, 'CU', 53, 0, 0, 0, 1, '', 1),
(76, 1, 0, 'CY', 357, 0, 0, 0, 1, 'NNNN', 1),
(77, 4, 0, 'DJ', 253, 0, 0, 0, 1, '', 1),
(78, 8, 0, 'DM', 0, 0, 0, 0, 1, '', 1),
(79, 8, 0, 'DO', 0, 0, 0, 0, 1, '', 1),
(80, 3, 0, 'TL', 670, 0, 0, 0, 1, '', 1),
(81, 6, 0, 'EC', 593, 0, 0, 0, 1, 'CNNNNNN', 1),
(82, 4, 0, 'EG', 20, 0, 0, 0, 1, 'NNNNN', 1),
(83, 8, 0, 'SV', 503, 0, 0, 0, 1, '', 1),
(84, 4, 0, 'GQ', 240, 0, 0, 0, 1, '', 1),
(85, 4, 0, 'ER', 291, 0, 0, 0, 1, '', 1),
(86, 1, 0, 'EE', 372, 0, 0, 0, 1, 'NNNNN', 1),
(87, 4, 0, 'ET', 251, 0, 0, 0, 1, '', 1),
(88, 8, 0, 'FK', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(89, 7, 0, 'FO', 298, 0, 0, 0, 1, '', 1),
(90, 5, 0, 'FJ', 679, 0, 0, 0, 1, '', 1),
(91, 4, 0, 'GA', 241, 0, 0, 0, 1, '', 1),
(92, 4, 0, 'GM', 220, 0, 0, 0, 1, '', 1),
(93, 3, 0, 'GE', 995, 0, 0, 0, 1, 'NNNN', 1),
(94, 4, 0, 'GH', 233, 0, 0, 0, 1, '', 1),
(95, 8, 0, 'GD', 0, 0, 0, 0, 1, '', 1),
(96, 7, 0, 'GL', 299, 0, 0, 0, 1, '', 1),
(97, 7, 0, 'GI', 350, 0, 0, 0, 1, '', 1),
(98, 8, 0, 'GP', 590, 0, 0, 0, 1, '', 1),
(99, 5, 0, 'GU', 0, 0, 0, 0, 1, '', 1),
(100, 8, 0, 'GT', 502, 0, 0, 0, 1, '', 1),
(101, 7, 0, 'GG', 0, 0, 0, 0, 1, 'LLN NLL', 1),
(102, 4, 0, 'GN', 224, 0, 0, 0, 1, '', 1),
(103, 4, 0, 'GW', 245, 0, 0, 0, 1, '', 1),
(104, 6, 0, 'GY', 592, 0, 0, 0, 1, '', 1),
(105, 8, 0, 'HT', 509, 0, 0, 0, 1, '', 1),
(106, 5, 0, 'HM', 0, 0, 0, 0, 1, '', 1),
(107, 7, 0, 'VA', 379, 0, 0, 0, 1, 'NNNNN', 1),
(108, 8, 0, 'HN', 504, 0, 0, 0, 1, '', 1),
(109, 7, 0, 'IS', 354, 0, 0, 0, 1, 'NNN', 1),
(110, 3, 0, 'IN', 91, 0, 0, 0, 1, 'NNN NNN', 1),
(111, 3, 0, 'ID', 62, 0, 1, 0, 1, 'NNNNN', 1),
(112, 3, 0, 'IR', 98, 0, 0, 0, 1, 'NNNNN-NNNNN', 1),
(113, 3, 0, 'IQ', 964, 0, 0, 0, 1, 'NNNNN', 1),
(114, 7, 0, 'IM', 0, 0, 0, 0, 1, 'CN NLL', 1),
(115, 8, 0, 'JM', 0, 0, 0, 0, 1, '', 1),
(116, 7, 0, 'JE', 0, 0, 0, 0, 1, 'CN NLL', 1),
(117, 3, 0, 'JO', 962, 0, 0, 0, 1, '', 1),
(118, 3, 0, 'KZ', 7, 0, 0, 0, 1, 'NNNNNN', 1),
(119, 4, 0, 'KE', 254, 0, 0, 0, 1, '', 1),
(120, 5, 0, 'KI', 686, 0, 0, 0, 1, '', 1),
(121, 3, 0, 'KP', 850, 0, 0, 0, 1, '', 1),
(122, 3, 0, 'KW', 965, 0, 0, 0, 1, '', 1),
(123, 3, 0, 'KG', 996, 0, 0, 0, 1, '', 1),
(124, 3, 0, 'LA', 856, 0, 0, 0, 1, '', 1),
(125, 1, 0, 'LV', 371, 0, 0, 0, 1, 'C-NNNN', 1),
(126, 3, 0, 'LB', 961, 0, 0, 0, 1, '', 1),
(127, 4, 0, 'LS', 266, 0, 0, 0, 1, '', 1),
(128, 4, 0, 'LR', 231, 0, 0, 0, 1, '', 1),
(129, 4, 0, 'LY', 218, 0, 0, 0, 1, '', 1),
(130, 1, 0, 'LI', 423, 0, 0, 0, 1, 'NNNN', 1),
(131, 1, 0, 'LT', 370, 0, 0, 0, 1, 'NNNNN', 1),
(132, 3, 0, 'MO', 853, 0, 0, 0, 0, '', 1),
(133, 7, 0, 'MK', 389, 0, 0, 0, 1, '', 1),
(134, 4, 0, 'MG', 261, 0, 0, 0, 1, '', 1),
(135, 4, 0, 'MW', 265, 0, 0, 0, 1, '', 1),
(136, 3, 0, 'MY', 60, 0, 0, 0, 1, 'NNNNN', 1),
(137, 3, 0, 'MV', 960, 0, 0, 0, 1, '', 1),
(138, 4, 0, 'ML', 223, 0, 0, 0, 1, '', 1),
(139, 1, 0, 'MT', 356, 0, 0, 0, 1, 'LLL NNNN', 1),
(140, 5, 0, 'MH', 692, 0, 0, 0, 1, '', 1),
(141, 8, 0, 'MQ', 596, 0, 0, 0, 1, '', 1),
(142, 4, 0, 'MR', 222, 0, 0, 0, 1, '', 1),
(143, 1, 0, 'HU', 36, 0, 0, 0, 1, 'NNNN', 1),
(144, 4, 0, 'YT', 262, 0, 0, 0, 1, '', 1),
(145, 2, 0, 'MX', 52, 0, 1, 1, 1, 'NNNNN', 1),
(146, 5, 0, 'FM', 691, 0, 0, 0, 1, '', 1),
(147, 7, 0, 'MD', 373, 0, 0, 0, 1, 'C-NNNN', 1),
(148, 7, 0, 'MC', 377, 0, 0, 0, 1, '980NN', 1),
(149, 3, 0, 'MN', 976, 0, 0, 0, 1, '', 1),
(150, 7, 0, 'ME', 382, 0, 0, 0, 1, 'NNNNN', 1),
(151, 8, 0, 'MS', 0, 0, 0, 0, 1, '', 1),
(152, 4, 0, 'MA', 212, 0, 0, 0, 1, 'NNNNN', 1),
(153, 4, 0, 'MZ', 258, 0, 0, 0, 1, '', 1),
(154, 4, 0, 'NA', 264, 0, 0, 0, 1, '', 1),
(155, 5, 0, 'NR', 674, 0, 0, 0, 1, '', 1),
(156, 3, 0, 'NP', 977, 0, 0, 0, 1, '', 1),
(157, 8, 0, 'AN', 599, 0, 0, 0, 1, '', 1),
(158, 5, 0, 'NC', 687, 0, 0, 0, 1, '', 1),
(159, 8, 0, 'NI', 505, 0, 0, 0, 1, 'NNNNNN', 1),
(160, 4, 0, 'NE', 227, 0, 0, 0, 1, '', 1),
(161, 5, 0, 'NU', 683, 0, 0, 0, 1, '', 1),
(162, 5, 0, 'NF', 0, 0, 0, 0, 1, '', 1),
(163, 5, 0, 'MP', 0, 0, 0, 0, 1, '', 1),
(164, 3, 0, 'OM', 968, 0, 0, 0, 1, '', 1),
(165, 3, 0, 'PK', 92, 0, 0, 0, 1, '', 1),
(166, 5, 0, 'PW', 680, 0, 0, 0, 1, '', 1),
(167, 3, 0, 'PS', 0, 0, 0, 0, 1, '', 1),
(168, 8, 0, 'PA', 507, 0, 0, 0, 1, 'NNNNNN', 1),
(169, 5, 0, 'PG', 675, 0, 0, 0, 1, '', 1),
(170, 6, 0, 'PY', 595, 0, 0, 0, 1, '', 1),
(171, 6, 0, 'PE', 51, 0, 0, 0, 1, '', 1),
(172, 3, 0, 'PH', 63, 0, 0, 0, 1, 'NNNN', 1),
(173, 5, 0, 'PN', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(174, 8, 0, 'PR', 0, 0, 0, 0, 1, 'NNNNN', 1),
(175, 3, 0, 'QA', 974, 0, 0, 0, 1, '', 1),
(176, 4, 0, 'RE', 262, 1, 0, 0, 1, '', 1),
(177, 7, 0, 'RU', 7, 0, 0, 0, 1, 'NNNNNN', 1),
(178, 4, 0, 'RW', 250, 0, 0, 0, 1, '', 1),
(179, 8, 0, 'BL', 0, 0, 0, 0, 1, '', 1),
(180, 8, 0, 'KN', 0, 0, 0, 0, 1, '', 1),
(181, 8, 0, 'LC', 0, 0, 0, 0, 1, '', 1),
(182, 8, 0, 'MF', 0, 0, 0, 0, 1, '', 1),
(183, 8, 0, 'PM', 508, 0, 0, 0, 1, '', 1),
(184, 8, 0, 'VC', 0, 0, 0, 0, 1, '', 1),
(185, 5, 0, 'WS', 685, 0, 0, 0, 1, '', 1),
(186, 7, 0, 'SM', 378, 0, 0, 0, 1, 'NNNNN', 1),
(187, 4, 0, 'ST', 239, 0, 0, 0, 1, '', 1),
(188, 3, 0, 'SA', 966, 0, 0, 0, 1, '', 1),
(189, 4, 0, 'SN', 221, 0, 0, 0, 1, '', 1),
(190, 7, 0, 'RS', 381, 0, 0, 0, 1, 'NNNNN', 1),
(191, 4, 0, 'SC', 248, 0, 0, 0, 1, '', 1),
(192, 4, 0, 'SL', 232, 0, 0, 0, 1, '', 1),
(193, 1, 0, 'SI', 386, 0, 0, 0, 1, 'C-NNNN', 1),
(194, 5, 0, 'SB', 677, 0, 0, 0, 1, '', 1),
(195, 4, 0, 'SO', 252, 0, 0, 0, 1, '', 1),
(196, 8, 0, 'GS', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(197, 3, 0, 'LK', 94, 0, 0, 0, 1, 'NNNNN', 1),
(198, 4, 0, 'SD', 249, 0, 0, 0, 1, '', 1),
(199, 8, 0, 'SR', 597, 0, 0, 0, 1, '', 1),
(200, 7, 0, 'SJ', 0, 0, 0, 0, 1, '', 1),
(201, 4, 0, 'SZ', 268, 0, 0, 0, 1, '', 1),
(202, 3, 0, 'SY', 963, 0, 0, 0, 1, '', 1),
(203, 3, 0, 'TW', 886, 0, 0, 0, 1, 'NNNNN', 1),
(204, 3, 0, 'TJ', 992, 0, 0, 0, 1, '', 1),
(205, 4, 0, 'TZ', 255, 0, 0, 0, 1, '', 1),
(206, 3, 0, 'TH', 66, 0, 0, 0, 1, 'NNNNN', 1),
(207, 5, 0, 'TK', 690, 0, 0, 0, 1, '', 1),
(208, 5, 0, 'TO', 676, 0, 0, 0, 1, '', 1),
(209, 6, 0, 'TT', 0, 0, 0, 0, 1, '', 1),
(210, 4, 0, 'TN', 216, 0, 0, 0, 1, '', 1),
(211, 7, 0, 'TR', 90, 0, 0, 0, 1, 'NNNNN', 1),
(212, 3, 0, 'TM', 993, 0, 0, 0, 1, '', 1),
(213, 8, 0, 'TC', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(214, 5, 0, 'TV', 688, 0, 0, 0, 1, '', 1),
(215, 4, 0, 'UG', 256, 0, 0, 0, 1, '', 1),
(216, 1, 0, 'UA', 380, 0, 0, 0, 1, 'NNNNN', 1),
(217, 3, 0, 'AE', 971, 0, 0, 0, 1, '', 1),
(218, 6, 0, 'UY', 598, 0, 0, 0, 1, '', 1),
(219, 3, 0, 'UZ', 998, 0, 0, 0, 1, '', 1),
(220, 5, 0, 'VU', 678, 0, 0, 0, 1, '', 1),
(221, 6, 0, 'VE', 58, 0, 0, 0, 1, '', 1),
(222, 3, 0, 'VN', 84, 0, 0, 0, 1, 'NNNNNN', 1),
(223, 2, 0, 'VG', 0, 0, 0, 0, 1, 'CNNNN', 1),
(224, 2, 0, 'VI', 0, 0, 0, 0, 1, '', 1),
(225, 5, 0, 'WF', 681, 0, 0, 0, 1, '', 1),
(226, 4, 0, 'EH', 0, 0, 0, 0, 1, '', 1),
(227, 3, 0, 'YE', 967, 0, 0, 0, 1, '', 1),
(228, 4, 0, 'ZM', 260, 0, 0, 0, 1, '', 1),
(229, 4, 0, 'ZW', 263, 0, 0, 0, 1, '', 1),
(230, 7, 0, 'AL', 355, 0, 0, 0, 1, 'NNNN', 1),
(231, 3, 0, 'AF', 93, 0, 0, 0, 1, 'NNNN', 1),
(232, 5, 0, 'AQ', 0, 0, 0, 0, 1, '', 1),
(233, 1, 0, 'BA', 387, 0, 0, 0, 1, '', 1),
(234, 5, 0, 'BV', 0, 0, 0, 0, 1, '', 1),
(235, 5, 0, 'IO', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(236, 1, 0, 'BG', 359, 0, 0, 0, 1, 'NNNN', 1),
(237, 8, 0, 'KY', 0, 0, 0, 0, 1, '', 1),
(238, 3, 0, 'CX', 0, 0, 0, 0, 1, '', 1),
(239, 3, 0, 'CC', 0, 0, 0, 0, 1, '', 1),
(240, 5, 0, 'CK', 682, 0, 0, 0, 1, '', 1),
(241, 6, 0, 'GF', 594, 0, 0, 0, 1, '', 1),
(242, 5, 0, 'PF', 689, 0, 0, 0, 1, '', 1),
(243, 5, 0, 'TF', 0, 0, 0, 0, 1, '', 1),
(244, 7, 0, 'AX', 0, 0, 0, 0, 1, 'NNNNN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_country_lang`
--

CREATE TABLE `ap_country_lang` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_country_lang`
--

INSERT INTO `ap_country_lang` (`id_country`, `id_lang`, `name`) VALUES
(1, 1, 'Allemagne'),
(2, 1, 'Autriche'),
(3, 1, 'Belgique'),
(4, 1, 'Canada'),
(5, 1, 'Chine'),
(6, 1, 'Espagne'),
(7, 1, 'Finlande'),
(8, 1, 'France'),
(9, 1, 'Grèce'),
(10, 1, 'Italie'),
(11, 1, 'Japon'),
(12, 1, 'Luxembourg'),
(13, 1, 'Pays-bas'),
(14, 1, 'Pologne'),
(15, 1, 'Portugal'),
(16, 1, 'République Tchèque'),
(17, 1, 'Royaume-Uni'),
(18, 1, 'Suède'),
(19, 1, 'Suisse'),
(20, 1, 'Danemark'),
(21, 1, 'États-Unis'),
(22, 1, 'Hong-Kong'),
(23, 1, 'Norvège'),
(24, 1, 'Australie'),
(25, 1, 'Singapour'),
(26, 1, 'Irlande'),
(27, 1, 'Nouvelle-Zélande'),
(28, 1, 'Corée du Sud'),
(29, 1, 'Israël'),
(30, 1, 'Afrique du Sud'),
(31, 1, 'Nigeria'),
(32, 1, 'Côte d\'Ivoire'),
(33, 1, 'Togo'),
(34, 1, 'Bolivie'),
(35, 1, 'Ile Maurice'),
(36, 1, 'Roumanie'),
(37, 1, 'Slovaquie'),
(38, 1, 'Algérie'),
(39, 1, 'Samoa Américaines'),
(40, 1, 'Andorre'),
(41, 1, 'Angola'),
(42, 1, 'Anguilla'),
(43, 1, 'Antigua et Barbuda'),
(44, 1, 'Argentine'),
(45, 1, 'Arménie'),
(46, 1, 'Aruba'),
(47, 1, 'Azerbaïdjan'),
(48, 1, 'Bahamas'),
(49, 1, 'Bahreïn'),
(50, 1, 'Bangladesh'),
(51, 1, 'Barbade'),
(52, 1, 'Bélarus'),
(53, 1, 'Belize'),
(54, 1, 'Bénin'),
(55, 1, 'Bermudes'),
(56, 1, 'Bhoutan'),
(57, 1, 'Botswana'),
(58, 1, 'Brésil'),
(59, 1, 'Brunéi Darussalam'),
(60, 1, 'Burkina Faso'),
(61, 1, 'Burma (Myanmar)'),
(62, 1, 'Burundi'),
(63, 1, 'Cambodge'),
(64, 1, 'Cameroun'),
(65, 1, 'Cap-Vert'),
(66, 1, 'Centrafricaine, République'),
(67, 1, 'Tchad'),
(68, 1, 'Chili'),
(69, 1, 'Colombie'),
(70, 1, 'Comores'),
(71, 1, 'Congo, Rép. Dém.'),
(72, 1, 'Congo, Rép.'),
(73, 1, 'Costa Rica'),
(74, 1, 'Croatie'),
(75, 1, 'Cuba'),
(76, 1, 'Chypre'),
(77, 1, 'Djibouti'),
(78, 1, 'Dominica'),
(79, 1, 'République Dominicaine'),
(80, 1, 'Timor oriental'),
(81, 1, 'Équateur'),
(82, 1, 'Égypte'),
(83, 1, 'El Salvador'),
(84, 1, 'Guinée Équatoriale'),
(85, 1, 'Érythrée'),
(86, 1, 'Estonie'),
(87, 1, 'Éthiopie'),
(88, 1, 'Falkland, Îles'),
(89, 1, 'Féroé, Îles'),
(90, 1, 'Fidji'),
(91, 1, 'Gabon'),
(92, 1, 'Gambie'),
(93, 1, 'Géorgie'),
(94, 1, 'Ghana'),
(95, 1, 'Grenade'),
(96, 1, 'Groenland'),
(97, 1, 'Gibraltar'),
(98, 1, 'Guadeloupe'),
(99, 1, 'Guam'),
(100, 1, 'Guatemala'),
(101, 1, 'Guernesey'),
(102, 1, 'Guinée'),
(103, 1, 'Guinée-Bissau'),
(104, 1, 'Guyana'),
(105, 1, 'Haîti'),
(106, 1, 'Heard, Île et Mcdonald, Îles'),
(107, 1, 'Saint-Siege (État de la Cité du Vatican)'),
(108, 1, 'Honduras'),
(109, 1, 'Islande'),
(110, 1, 'Inde'),
(111, 1, 'Indonésie'),
(112, 1, 'Iran'),
(113, 1, 'Iraq'),
(114, 1, 'Man, Île de'),
(115, 1, 'Jamaique'),
(116, 1, 'Jersey'),
(117, 1, 'Jordanie'),
(118, 1, 'Kazakhstan'),
(119, 1, 'Kenya'),
(120, 1, 'Kiribati'),
(121, 1, 'Corée, Rép. Populaire Dém. de'),
(122, 1, 'Koweït'),
(123, 1, 'Kirghizistan'),
(124, 1, 'Laos'),
(125, 1, 'Lettonie'),
(126, 1, 'Liban'),
(127, 1, 'Lesotho'),
(128, 1, 'Libéria'),
(129, 1, 'Libyenne, Jamahiriya Arabe'),
(130, 1, 'Liechtenstein'),
(131, 1, 'Lituanie'),
(132, 1, 'Macao'),
(133, 1, 'Macédoine'),
(134, 1, 'Madagascar'),
(135, 1, 'Malawi'),
(136, 1, 'Malaisie'),
(137, 1, 'Maldives'),
(138, 1, 'Mali'),
(139, 1, 'Malte'),
(140, 1, 'Marshall, Îles'),
(141, 1, 'Martinique'),
(142, 1, 'Mauritanie'),
(143, 1, 'Hongrie'),
(144, 1, 'Mayotte'),
(145, 1, 'Mexique'),
(146, 1, 'Micronésie'),
(147, 1, 'Moldova'),
(148, 1, 'Monaco'),
(149, 1, 'Mongolie'),
(150, 1, 'Monténégro'),
(151, 1, 'Montserrat'),
(152, 1, 'Maroc'),
(153, 1, 'Mozambique'),
(154, 1, 'Namibie'),
(155, 1, 'Nauru'),
(156, 1, 'Népal'),
(157, 1, 'Antilles Néerlandaises'),
(158, 1, 'Nouvelle-Calédonie'),
(159, 1, 'Nicaragua'),
(160, 1, 'Niger'),
(161, 1, 'Niué'),
(162, 1, 'Norfolk, Île'),
(163, 1, 'Mariannes du Nord, Îles'),
(164, 1, 'Oman'),
(165, 1, 'Pakistan'),
(166, 1, 'Palaos'),
(167, 1, 'Palestinien Occupé, Territoire'),
(168, 1, 'Panama'),
(169, 1, 'Papouasie-Nouvelle-Guinée'),
(170, 1, 'Paraguay'),
(171, 1, 'Pérou'),
(172, 1, 'Philippines'),
(173, 1, 'Pitcairn'),
(174, 1, 'Porto Rico'),
(175, 1, 'Qatar'),
(176, 1, 'Réunion, Île de la'),
(177, 1, 'Russie, Fédération de'),
(178, 1, 'Rwanda'),
(179, 1, 'Saint-Barthélemy'),
(180, 1, 'Saint-Kitts-et-Nevis'),
(181, 1, 'Sainte-Lucie'),
(182, 1, 'Saint-Martin'),
(183, 1, 'Saint-Pierre-et-Miquelon'),
(184, 1, 'Saint-Vincent-et-Les Grenadines'),
(185, 1, 'Samoa'),
(186, 1, 'Saint-Marin'),
(187, 1, 'Sao Tomé-et-Principe'),
(188, 1, 'Arabie Saoudite'),
(189, 1, 'Sénégal'),
(190, 1, 'Serbie'),
(191, 1, 'Seychelles'),
(192, 1, 'Sierra Leone'),
(193, 1, 'Slovénie'),
(194, 1, 'Salomon, Îles'),
(195, 1, 'Somalie'),
(196, 1, 'Géorgie du Sud et les Îles Sandwich du Sud'),
(197, 1, 'Sri Lanka'),
(198, 1, 'Soudan'),
(199, 1, 'Suriname'),
(200, 1, 'Svalbard et Île Jan Mayen'),
(201, 1, 'Swaziland'),
(202, 1, 'Syrienne'),
(203, 1, 'Taïwan'),
(204, 1, 'Tadjikistan'),
(205, 1, 'Tanzanie'),
(206, 1, 'Thaïlande'),
(207, 1, 'Tokelau'),
(208, 1, 'Tonga'),
(209, 1, 'Trinité-et-Tobago'),
(210, 1, 'Tunisie'),
(211, 1, 'Turquie'),
(212, 1, 'Turkménistan'),
(213, 1, 'Turks et Caiques, Îles'),
(214, 1, 'Tuvalu'),
(215, 1, 'Ouganda'),
(216, 1, 'Ukraine'),
(217, 1, 'Émirats Arabes Unis'),
(218, 1, 'Uruguay'),
(219, 1, 'Ouzbékistan'),
(220, 1, 'Vanuatu'),
(221, 1, 'Venezuela'),
(222, 1, 'Vietnam'),
(223, 1, 'Îles Vierges Britanniques'),
(224, 1, 'Îles Vierges des États-Unis'),
(225, 1, 'Wallis et Futuna'),
(226, 1, 'Sahara Occidental'),
(227, 1, 'Yémen'),
(228, 1, 'Zambie'),
(229, 1, 'Zimbabwe'),
(230, 1, 'Albanie'),
(231, 1, 'Afghanistan'),
(232, 1, 'Antarctique'),
(233, 1, 'Bosnie-Herzégovine'),
(234, 1, 'Bouvet, Île'),
(235, 1, 'Océan Indien, Territoire Britannique de L\''),
(236, 1, 'Bulgarie'),
(237, 1, 'Caïmans, Îles'),
(238, 1, 'Christmas, Île'),
(239, 1, 'Cocos (Keeling), Îles'),
(240, 1, 'Cook, Îles'),
(241, 1, 'Guyane Française'),
(242, 1, 'Polynésie Française'),
(243, 1, 'Terres Australes Françaises'),
(244, 1, 'Åland, Îles');

-- --------------------------------------------------------

--
-- Table structure for table `ap_country_shop`
--

CREATE TABLE `ap_country_shop` (
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_country_shop`
--

INSERT INTO `ap_country_shop` (`id_country`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(172, 1),
(173, 1),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(178, 1),
(179, 1),
(180, 1),
(181, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1),
(192, 1),
(193, 1),
(194, 1),
(195, 1),
(196, 1),
(197, 1),
(198, 1),
(199, 1),
(200, 1),
(201, 1),
(202, 1),
(203, 1),
(204, 1),
(205, 1),
(206, 1),
(207, 1),
(208, 1),
(209, 1),
(210, 1),
(211, 1),
(212, 1),
(213, 1),
(214, 1),
(215, 1),
(216, 1),
(217, 1),
(218, 1),
(219, 1),
(220, 1),
(221, 1),
(222, 1),
(223, 1),
(224, 1),
(225, 1),
(226, 1),
(227, 1),
(228, 1),
(229, 1),
(230, 1),
(231, 1),
(232, 1),
(233, 1),
(234, 1),
(235, 1),
(236, 1),
(237, 1),
(238, 1),
(239, 1),
(240, 1),
(241, 1),
(242, 1),
(243, 1),
(244, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_cronjobs`
--

CREATE TABLE `ap_cronjobs` (
  `id_cronjob` int(10) NOT NULL,
  `id_module` int(10) DEFAULT NULL,
  `description` text,
  `task` text,
  `hour` int(11) DEFAULT '-1',
  `day` int(11) DEFAULT '-1',
  `month` int(11) DEFAULT '-1',
  `day_of_week` int(11) DEFAULT '-1',
  `updated_at` datetime DEFAULT NULL,
  `one_shot` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `id_shop` int(11) DEFAULT '0',
  `id_shop_group` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_currency`
--

CREATE TABLE `ap_currency` (
  `id_currency` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `iso_code_num` varchar(3) NOT NULL DEFAULT '0',
  `sign` varchar(8) NOT NULL,
  `blank` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `format` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `decimals` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `conversion_rate` decimal(13,6) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_currency`
--

INSERT INTO `ap_currency` (`id_currency`, `name`, `iso_code`, `iso_code_num`, `sign`, `blank`, `format`, `decimals`, `conversion_rate`, `deleted`, `active`) VALUES
(1, 'Euro', 'EUR', '978', '€', 1, 2, 1, '1.000000', 0, 1),
(2, 'Dollar', 'USD', '840', '$', 0, 1, 1, '1.127656', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_currency_shop`
--

CREATE TABLE `ap_currency_shop` (
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_currency_shop`
--

INSERT INTO `ap_currency_shop` (`id_currency`, `id_shop`, `conversion_rate`) VALUES
(1, 1, '1.000000'),
(2, 1, '1.127656');

-- --------------------------------------------------------

--
-- Table structure for table `ap_customer`
--

CREATE TABLE `ap_customer` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_gender` int(10) UNSIGNED NOT NULL,
  `id_default_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED DEFAULT NULL,
  `id_risk` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `company` varchar(64) DEFAULT NULL,
  `siret` varchar(14) DEFAULT NULL,
  `ape` varchar(5) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `birthday` date DEFAULT NULL,
  `newsletter` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ip_registration_newsletter` varchar(15) DEFAULT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `optin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `website` varchar(128) DEFAULT NULL,
  `outstanding_allow_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `show_public_prices` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `max_payment_days` int(10) UNSIGNED NOT NULL DEFAULT '60',
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `note` text,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_customer`
--

INSERT INTO `ap_customer` (`id_customer`, `id_shop_group`, `id_shop`, `id_gender`, `id_default_group`, `id_lang`, `id_risk`, `company`, `siret`, `ape`, `firstname`, `lastname`, `email`, `passwd`, `last_passwd_gen`, `birthday`, `newsletter`, `ip_registration_newsletter`, `newsletter_date_add`, `optin`, `website`, `outstanding_allow_amount`, `show_public_prices`, `max_payment_days`, `secure_key`, `note`, `active`, `is_guest`, `deleted`, `date_add`, `date_upd`) VALUES
(1, 1, 1, 1, 3, 1, 0, '', '', '', 'John', 'DOE', 'pub@prestashop.com', '98173eb25854fb69d276b160621c7457', '2019-06-07 18:34:25', '1970-01-15', 1, '', '2013-12-13 08:19:15', 1, '', '0.000000', 0, 0, 'bdf45af325ad80200ea4a877da6411f8', '', 1, 0, 0, '2019-06-08 02:34:25', '2019-06-08 02:34:25'),
(2, 1, 1, 1, 3, 1, 0, NULL, NULL, NULL, 'Carl', 'DEBRAUWERE', 'carl.debrauwere@gmail.com', '426bd045300d6d3d746f7f90090442a7', '2019-06-07 19:07:24', '1993-04-30', 1, '154.67.215.55', '2019-06-15 15:23:31', 0, NULL, '0.000000', 0, 0, '0e3a71ce07c72faacb7058801be0f267', 'Fiston !!', 1, 0, 0, '2019-06-08 03:07:24', '2019-06-16 14:11:44'),
(3, 1, 1, 1, 3, 1, 0, NULL, NULL, NULL, 'Herve', 'DEBRAUWERE', 'dbe974@gmail.com', 'c06f6dd76958ad91c1f4862793c0b81a', '2019-06-08 05:23:27', '1966-02-27', 0, NULL, '0000-00-00 00:00:00', 0, NULL, '0.000000', 0, 0, 'a1366dfc69ac034fd2460385fabf076f', NULL, 1, 0, 0, '2019-06-08 13:23:27', '2019-06-08 13:23:27'),
(4, 1, 1, 2, 3, 1, 0, NULL, NULL, NULL, 'Anne-Patrice', 'MARTY', 'a_patrice_nc@yahoo.fr', '97d0e288a0331215d60e39819065228c', '2019-06-10 11:11:44', '1966-10-25', 0, NULL, '0000-00-00 00:00:00', 0, NULL, '0.000000', 0, 0, '45b5535bdd60260f369a080845ab3e53', NULL, 1, 0, 0, '2019-06-10 19:11:44', '2019-06-10 19:11:44'),
(6, 1, 1, 1, 3, 1, 0, NULL, NULL, NULL, 'Carl', 'DEBRAU', 'carldebrauwere@gmail.com', '8e035d3fa357713200c06b3379427967', '2019-06-12 08:31:36', '1993-04-30', 0, NULL, '0000-00-00 00:00:00', 0, NULL, '0.000000', 0, 0, '3f0e57fade0be9b2757b76e0992fe17d', NULL, 1, 0, 0, '2019-06-12 16:31:36', '2019-06-12 16:31:36'),
(7, 1, 1, 1, 3, 1, 0, NULL, NULL, NULL, 'Annabelle', 'VAN BURGEN', 'test@gmail.com', '8e035d3fa357713200c06b3379427967', '2019-06-12 09:04:20', '1976-07-16', 0, NULL, '0000-00-00 00:00:00', 0, NULL, '0.000000', 0, 0, '1dc17cf9925ed250938c9db9ec7d2d9e', NULL, 1, 0, 0, '2019-06-12 17:04:20', '2019-06-12 17:04:20'),
(8, 1, 1, 0, 3, 1, 0, NULL, NULL, NULL, 'Marie', 'marie', 'marie@gmail.com', 'e3007b84e588a099c412a0cf0829ce0d', '2019-06-15 08:34:43', '1982-06-14', 0, NULL, '0000-00-00 00:00:00', 0, NULL, '0.000000', 0, 0, '279eac2003c29a0db8f359010ba1dbbc', NULL, 1, 0, 0, '2019-06-15 16:34:43', '2019-06-15 16:34:43'),
(9, 1, 1, 0, 3, 1, 0, NULL, NULL, NULL, 'Test', 'Test', 'needtodelete@gmail.com', '426bd045300d6d3d746f7f90090442a7', '2019-06-17 03:42:53', '1994-04-30', 1, '165.169.52.178', '2019-06-17 11:42:53', 1, NULL, '0.000000', 0, 0, '926f5461e7e8334de6386c1bbbebd674', NULL, 1, 0, 0, '2019-06-17 11:42:53', '2019-06-17 11:42:53'),
(10, 1, 1, 2, 3, 1, 0, NULL, NULL, NULL, 'Marion', 'gremon', 'mariongremau@gmail.com', '97d0e288a0331215d60e39819065228c', '2019-06-17 08:26:55', '1985-06-14', 0, NULL, '0000-00-00 00:00:00', 0, NULL, '0.000000', 0, 0, 'dc24e49fb4eec8c177eee411ac48b021', NULL, 1, 0, 0, '2019-06-17 16:26:55', '2019-06-17 16:26:55'),
(11, 1, 1, 1, 3, 1, 0, NULL, NULL, NULL, 'Carl', 'DEBRAUWERE', 'debrauc4@gmail.com', '426bd045300d6d3d746f7f90090442a7', '2019-06-18 09:36:55', '1993-04-30', 0, NULL, '0000-00-00 00:00:00', 0, NULL, '0.000000', 0, 0, 'f90eae8a7c1f6c42a616faa0a89eef70', NULL, 1, 0, 0, '2019-06-18 17:36:55', '2019-06-18 17:36:55');

-- --------------------------------------------------------

--
-- Table structure for table `ap_customer_group`
--

CREATE TABLE `ap_customer_group` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_customer_group`
--

INSERT INTO `ap_customer_group` (`id_customer`, `id_group`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ap_customer_message`
--

CREATE TABLE `ap_customer_message` (
  `id_customer_message` int(10) UNSIGNED NOT NULL,
  `id_customer_thread` int(11) DEFAULT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `message` mediumtext NOT NULL,
  `file_name` varchar(18) DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `user_agent` varchar(128) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `read` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_customer_message_sync_imap`
--

CREATE TABLE `ap_customer_message_sync_imap` (
  `md5_header` varbinary(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_customer_thread`
--

CREATE TABLE `ap_customer_thread` (
  `id_customer_thread` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_contact` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `id_product` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('open','closed','pending1','pending2') NOT NULL DEFAULT 'open',
  `email` varchar(128) NOT NULL,
  `token` varchar(12) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_customization`
--

CREATE TABLE `ap_customization` (
  `id_customization` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_address_delivery` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `quantity_refunded` int(11) NOT NULL DEFAULT '0',
  `quantity_returned` int(11) NOT NULL DEFAULT '0',
  `in_cart` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_customization_field`
--

CREATE TABLE `ap_customization_field` (
  `id_customization_field` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_customization_field_lang`
--

CREATE TABLE `ap_customization_field_lang` (
  `id_customization_field` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_customized_data`
--

CREATE TABLE `ap_customized_data` (
  `id_customization` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `index` int(3) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_date_range`
--

CREATE TABLE `ap_date_range` (
  `id_date_range` int(10) UNSIGNED NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_delivery`
--

CREATE TABLE `ap_delivery` (
  `id_delivery` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED DEFAULT NULL,
  `id_shop_group` int(10) UNSIGNED DEFAULT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_range_price` int(10) UNSIGNED DEFAULT NULL,
  `id_range_weight` int(10) UNSIGNED DEFAULT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_delivery`
--

INSERT INTO `ap_delivery` (`id_delivery`, `id_shop`, `id_shop_group`, `id_carrier`, `id_range_price`, `id_range_weight`, `id_zone`, `price`) VALUES
(1, NULL, NULL, 2, 0, 1, 1, '5.000000'),
(2, NULL, NULL, 2, 0, 1, 2, '5.000000'),
(3, NULL, NULL, 2, 1, 0, 1, '5.000000'),
(4, NULL, NULL, 2, 1, 0, 2, '5.000000');

-- --------------------------------------------------------

--
-- Table structure for table `ap_employee`
--

CREATE TABLE `ap_employee` (
  `id_employee` int(10) UNSIGNED NOT NULL,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stats_date_from` date DEFAULT NULL,
  `stats_date_to` date DEFAULT NULL,
  `stats_compare_from` date DEFAULT NULL,
  `stats_compare_to` date DEFAULT NULL,
  `stats_compare_option` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `preselect_date_range` varchar(32) DEFAULT NULL,
  `bo_color` varchar(32) DEFAULT NULL,
  `bo_theme` varchar(32) DEFAULT NULL,
  `bo_css` varchar(64) DEFAULT NULL,
  `default_tab` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bo_width` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bo_menu` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `optin` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `id_last_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_last_customer_message` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_last_customer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_connection_date` date DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_employee`
--

INSERT INTO `ap_employee` (`id_employee`, `id_profile`, `id_lang`, `lastname`, `firstname`, `email`, `passwd`, `last_passwd_gen`, `stats_date_from`, `stats_date_to`, `stats_compare_from`, `stats_compare_to`, `stats_compare_option`, `preselect_date_range`, `bo_color`, `bo_theme`, `bo_css`, `default_tab`, `bo_width`, `bo_menu`, `active`, `optin`, `id_last_order`, `id_last_customer_message`, `id_last_customer`, `last_connection_date`) VALUES
(1, 1, 1, 'DEBRAUWERE', 'Carl', 'carl.debrauwere@gmail.com', '426bd045300d6d3d746f7f90090442a7', '2019-06-07 18:34:24', '2019-01-01', '2019-06-10', '0000-00-00', '0000-00-00', 1, 'year', '', 'default', 'admin-theme.css', 1, 0, 1, 1, 1, 47, 0, 10, '2019-06-20'),
(2, 1, 1, 'Marty', 'Anne-Patrice', 'a_patrice_nc@yahoo.fr', '97d0e288a0331215d60e39819065228c', '2019-06-08 12:55:57', '2019-06-11', '2019-06-11', '0000-00-00', '0000-00-00', 1, 'day', '', 'default', 'admin-theme.css', 1, 0, 1, 1, 1, 5, 0, 7, '2019-06-19');

-- --------------------------------------------------------

--
-- Table structure for table `ap_employee_shop`
--

CREATE TABLE `ap_employee_shop` (
  `id_employee` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_employee_shop`
--

INSERT INTO `ap_employee_shop` (`id_employee`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_feature`
--

CREATE TABLE `ap_feature` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_feature`
--

INSERT INTO `ap_feature` (`id_feature`, `position`) VALUES
(1, 0),
(2, 1),
(3, 2),
(4, 3),
(5, 4),
(6, 5),
(7, 6);

-- --------------------------------------------------------

--
-- Table structure for table `ap_feature_lang`
--

CREATE TABLE `ap_feature_lang` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_feature_lang`
--

INSERT INTO `ap_feature_lang` (`id_feature`, `id_lang`, `name`) VALUES
(5, 1, 'Compositions'),
(1, 1, 'Hauteur'),
(2, 1, 'Largeur'),
(4, 1, 'Poids'),
(3, 1, 'Profondeur'),
(7, 1, 'Propriétés'),
(6, 1, 'Styles');

-- --------------------------------------------------------

--
-- Table structure for table `ap_feature_product`
--

CREATE TABLE `ap_feature_product` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_feature_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_feature_shop`
--

CREATE TABLE `ap_feature_shop` (
  `id_feature` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_feature_shop`
--

INSERT INTO `ap_feature_shop` (`id_feature`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_feature_value`
--

CREATE TABLE `ap_feature_value` (
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  `id_feature` int(10) UNSIGNED NOT NULL,
  `custom` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_feature_value`
--

INSERT INTO `ap_feature_value` (`id_feature_value`, `id_feature`, `custom`) VALUES
(1, 5, 0),
(2, 5, 0),
(3, 5, 0),
(4, 5, 0),
(5, 5, 0),
(6, 5, 0),
(7, 5, 0),
(8, 5, 0),
(9, 5, 0),
(10, 6, 0),
(11, 6, 0),
(12, 6, 0),
(13, 6, 0),
(14, 6, 0),
(15, 6, 0),
(16, 6, 0),
(17, 7, 0),
(18, 7, 0),
(19, 7, 0),
(20, 7, 0),
(21, 7, 0),
(22, 1, 1),
(23, 2, 1),
(24, 4, 1),
(25, 3, 1),
(26, 1, 1),
(27, 2, 1),
(28, 4, 1),
(29, 3, 1),
(30, 1, 1),
(31, 2, 1),
(32, 4, 1),
(33, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_feature_value_lang`
--

CREATE TABLE `ap_feature_value_lang` (
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_feature_value_lang`
--

INSERT INTO `ap_feature_value_lang` (`id_feature_value`, `id_lang`, `value`) VALUES
(1, 1, 'Polyester'),
(2, 1, 'Laine'),
(3, 1, 'Viscose'),
(4, 1, 'Elasthanne'),
(5, 1, 'Coton'),
(6, 1, 'Soie'),
(7, 1, 'Daim'),
(8, 1, 'Paille'),
(9, 1, 'Cuir'),
(10, 1, 'Classique'),
(11, 1, 'Décontracté'),
(12, 1, 'Militaire'),
(13, 1, 'Féminin'),
(14, 1, 'Rock'),
(15, 1, 'Basique'),
(16, 1, 'Habillé'),
(17, 1, 'Manches courtes'),
(18, 1, 'Robe colorée'),
(19, 1, 'Robe courte'),
(20, 1, 'Robe midi'),
(21, 1, 'Maxi-robe'),
(22, 1, '2.75 in'),
(23, 1, '2.06 in'),
(24, 1, '49.2 g'),
(25, 1, '0.26 in'),
(26, 1, '1.07 in'),
(27, 1, '1.62 in'),
(28, 1, '15.5 g'),
(29, 1, '0.41 in (clip included)'),
(30, 1, '4.33 in'),
(31, 1, '2.76 in'),
(32, 1, '120g'),
(33, 1, '0.31 in');

-- --------------------------------------------------------

--
-- Table structure for table `ap_gender`
--

CREATE TABLE `ap_gender` (
  `id_gender` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_gender`
--

INSERT INTO `ap_gender` (`id_gender`, `type`) VALUES
(1, 0),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_gender_lang`
--

CREATE TABLE `ap_gender_lang` (
  `id_gender` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_gender_lang`
--

INSERT INTO `ap_gender_lang` (`id_gender`, `id_lang`, `name`) VALUES
(1, 1, 'M'),
(2, 1, 'Mme');

-- --------------------------------------------------------

--
-- Table structure for table `ap_group`
--

CREATE TABLE `ap_group` (
  `id_group` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(17,2) NOT NULL DEFAULT '0.00',
  `price_display_method` tinyint(4) NOT NULL DEFAULT '0',
  `show_prices` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_group`
--

INSERT INTO `ap_group` (`id_group`, `reduction`, `price_display_method`, `show_prices`, `date_add`, `date_upd`) VALUES
(1, '0.00', 0, 1, '2019-06-08 02:34:20', '2019-06-08 02:34:20'),
(2, '0.00', 0, 1, '2019-06-08 02:34:20', '2019-06-08 02:34:20'),
(3, '0.00', 0, 1, '2019-06-08 02:34:20', '2019-06-08 02:34:20');

-- --------------------------------------------------------

--
-- Table structure for table `ap_group_lang`
--

CREATE TABLE `ap_group_lang` (
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_group_lang`
--

INSERT INTO `ap_group_lang` (`id_group`, `id_lang`, `name`) VALUES
(1, 1, 'Visiteur'),
(2, 1, 'Invité'),
(3, 1, 'Client');

-- --------------------------------------------------------

--
-- Table structure for table `ap_group_reduction`
--

CREATE TABLE `ap_group_reduction` (
  `id_group_reduction` mediumint(8) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(4,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_group_shop`
--

CREATE TABLE `ap_group_shop` (
  `id_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_group_shop`
--

INSERT INTO `ap_group_shop` (`id_group`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_guest`
--

CREATE TABLE `ap_guest` (
  `id_guest` int(10) UNSIGNED NOT NULL,
  `id_operating_system` int(10) UNSIGNED DEFAULT NULL,
  `id_web_browser` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `javascript` tinyint(1) DEFAULT '0',
  `screen_resolution_x` smallint(5) UNSIGNED DEFAULT NULL,
  `screen_resolution_y` smallint(5) UNSIGNED DEFAULT NULL,
  `screen_color` tinyint(3) UNSIGNED DEFAULT NULL,
  `sun_java` tinyint(1) DEFAULT NULL,
  `adobe_flash` tinyint(1) DEFAULT NULL,
  `adobe_director` tinyint(1) DEFAULT NULL,
  `apple_quicktime` tinyint(1) DEFAULT NULL,
  `real_player` tinyint(1) DEFAULT NULL,
  `windows_media` tinyint(1) DEFAULT NULL,
  `accept_language` varchar(8) DEFAULT NULL,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_guest`
--

INSERT INTO `ap_guest` (`id_guest`, `id_operating_system`, `id_web_browser`, `id_customer`, `javascript`, `screen_resolution_x`, `screen_resolution_y`, `screen_color`, `sun_java`, `adobe_flash`, `adobe_director`, `apple_quicktime`, `real_player`, `windows_media`, `accept_language`, `mobile_theme`) VALUES
(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(2, 6, 11, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(3, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(8, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(9, 0, 11, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(10, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(12, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(16, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(17, 0, 11, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(20, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(21, 0, 11, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(22, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(33, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(34, 0, 11, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(36, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(39, 7, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 1),
(40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(44, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en-us', 1),
(45, 7, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 1),
(46, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(48, 7, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 1),
(49, 0, 11, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(50, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(51, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(52, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(54, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(55, 7, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 1),
(56, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'zh', 0),
(57, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'zh', 0),
(58, 7, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1),
(59, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1),
(60, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1),
(61, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(62, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'ru', 0),
(63, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(65, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(66, 4, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(67, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(69, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'zh-cn', 0),
(70, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'zh-cn', 0),
(71, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(73, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(74, 0, 11, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(75, 7, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 1),
(76, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(77, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(78, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(79, 7, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 1),
(80, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(81, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'ru', 0),
(82, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(83, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(84, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(85, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(86, 6, 11, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(87, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(89, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(91, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(92, 0, 11, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(93, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(95, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(97, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(98, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(102, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(103, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(104, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(105, 6, 11, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(107, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(108, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(109, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(110, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(111, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(114, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(115, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(116, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
(117, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(118, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(119, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
(120, 4, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(122, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_homeslider`
--

CREATE TABLE `ap_homeslider` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_homeslider`
--

INSERT INTO `ap_homeslider` (`id_homeslider_slides`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_homeslider_slides`
--

CREATE TABLE `ap_homeslider_slides` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_homeslider_slides`
--

INSERT INTO `ap_homeslider_slides` (`id_homeslider_slides`, `position`, `active`) VALUES
(1, 0, 1),
(2, 2, 1),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_homeslider_slides_lang`
--

CREATE TABLE `ap_homeslider_slides_lang` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `legend` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_homeslider_slides_lang`
--

INSERT INTO `ap_homeslider_slides_lang` (`id_homeslider_slides`, `id_lang`, `title`, `description`, `legend`, `url`, `image`) VALUES
(1, 1, 'Sample 1', '<h2>EXCEPTEUR<br />OCCAECAT</h2>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n<p><button class=\"btn btn-default\" type=\"button\">Shop now !</button></p>', 'sample-1', 'http://www.prestashop.com/?utm_source=back-office&utm_medium=v16_homeslider&utm_campaign=back-office-FR&utm_content=download', 'd91cb7fa1c50be5c264ab2ed7d158c724cd18162_download (1).jpeg'),
(2, 1, 'Sample 2', '<h2>EXCEPTEUR<br />OCCAECAT</h2>\n				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n				<p><button class=\"btn btn-default\" type=\"button\">Shop now !</button></p>', 'sample-2', 'http://www.prestashop.com/?utm_source=back-office&utm_medium=v16_homeslider&utm_campaign=back-office-FR&utm_content=download', 'sample-2.jpg'),
(3, 1, 'Sample 3', '<h2>EXCEPTEUR<br />OCCAECAT</h2>\n				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n				<p><button class=\"btn btn-default\" type=\"button\">Shop now !</button></p>', 'sample-3', 'http://www.prestashop.com/?utm_source=back-office&utm_medium=v16_homeslider&utm_campaign=back-office-FR&utm_content=download', 'sample-3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ap_hook`
--

CREATE TABLE `ap_hook` (
  `id_hook` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `position` tinyint(1) NOT NULL DEFAULT '1',
  `live_edit` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_hook`
--

INSERT INTO `ap_hook` (`id_hook`, `name`, `title`, `description`, `position`, `live_edit`) VALUES
(1, 'displayPayment', 'Payment', 'This hook displays new elements on the payment page', 1, 1),
(2, 'actionValidateOrder', 'New orders', '', 1, 0),
(3, 'displayMaintenance', 'Maintenance Page', 'This hook displays new elements on the maintenance page', 1, 0),
(4, 'actionPaymentConfirmation', 'Payment confirmation', 'This hook displays new elements after the payment is validated', 1, 0),
(5, 'displayPaymentReturn', 'Payment return', '', 1, 0),
(6, 'actionUpdateQuantity', 'Quantity update', 'Quantity is updated only when a customer effectively places their order', 1, 0),
(7, 'displayRightColumn', 'Right column blocks', 'This hook displays new elements in the right-hand column', 1, 1),
(8, 'displayLeftColumn', 'Left column blocks', 'This hook displays new elements in the left-hand column', 1, 1),
(9, 'displayHome', 'Homepage content', 'This hook displays new elements on the homepage', 1, 1),
(10, 'Header', 'Pages html head section', 'This hook adds additional elements in the head section of your pages (head section of html)', 1, 0),
(11, 'actionCartSave', 'Cart creation and update', 'This hook is displayed when a product is added to the cart or if the cart\'s content is modified', 1, 0),
(12, 'actionAuthentication', 'Successful customer authentication', 'This hook is displayed after a customer successfully signs in', 1, 0),
(13, 'actionProductAdd', 'Product creation', 'This hook is displayed after a product is created', 1, 0),
(14, 'actionProductUpdate', 'Product update', 'This hook is displayed after a product has been updated', 1, 0),
(15, 'displayTop', 'Top of pages', 'This hook displays additional elements at the top of your pages', 1, 0),
(16, 'displayRightColumnProduct', 'New elements on the product page (right column)', 'This hook displays new elements in the right-hand column of the product page', 1, 0),
(17, 'actionProductDelete', 'Product deletion', 'This hook is called when a product is deleted', 1, 0),
(18, 'displayFooterProduct', 'Product footer', 'This hook adds new blocks under the product\'s description', 1, 1),
(19, 'displayInvoice', 'Invoice', 'This hook displays new blocks on the invoice (order)', 1, 0),
(20, 'actionOrderStatusUpdate', 'Order status update - Event', 'This hook launches modules when the status of an order changes.', 1, 0),
(21, 'displayAdminOrder', 'Display new elements in the Back Office, tab AdminOrder', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office', 1, 0),
(22, 'displayAdminOrderTabOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel tabs', 1, 0),
(23, 'displayAdminOrderTabShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel tabs', 1, 0),
(24, 'displayAdminOrderContentOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel content', 1, 0),
(25, 'displayAdminOrderContentShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel content', 1, 0),
(26, 'displayFooter', 'Footer', 'This hook displays new blocks in the footer', 1, 0),
(27, 'displayPDFInvoice', 'PDF Invoice', 'This hook allows you to display additional information on PDF invoices', 1, 0),
(28, 'displayInvoiceLegalFreeText', 'PDF Invoice - Legal Free Text', 'This hook allows you to modify the legal free text on PDF invoices', 1, 0),
(29, 'displayAdminCustomers', 'Display new elements in the Back Office, tab AdminCustomers', 'This hook launches modules when the AdminCustomers tab is displayed in the Back Office', 1, 0),
(30, 'displayOrderConfirmation', 'Order confirmation page', 'This hook is called within an order\'s confirmation page', 1, 0),
(31, 'actionCustomerAccountAdd', 'Successful customer account creation', 'This hook is called when a new customer creates an account successfully', 1, 0),
(32, 'displayCustomerAccount', 'Customer account displayed in Front Office', 'This hook displays new elements on the customer account page', 1, 0),
(33, 'displayCustomerIdentityForm', 'Customer identity form displayed in Front Office', 'This hook displays new elements on the form to update a customer identity', 1, 0),
(34, 'actionOrderSlipAdd', 'Order slip creation', 'This hook is called when a new credit slip is added regarding client order', 1, 0),
(35, 'displayProductTab', 'Tabs on product page', 'This hook is called on the product page\'s tab', 1, 0),
(36, 'displayProductTabContent', 'Tabs content on the product page', 'This hook is called on the product page\'s tab', 1, 0),
(37, 'displayShoppingCartFooter', 'Shopping cart footer', 'This hook displays some specific information on the shopping cart\'s page', 1, 0),
(38, 'displayCustomerAccountForm', 'Customer account creation form', 'This hook displays some information on the form to create a customer account', 1, 0),
(39, 'displayAdminStatsModules', 'Stats - Modules', '', 1, 0),
(40, 'displayAdminStatsGraphEngine', 'Graph engines', '', 1, 0),
(41, 'actionOrderReturn', 'Returned product', 'This hook is displayed when a customer returns a product ', 1, 0),
(42, 'displayProductButtons', 'Product page actions', 'This hook adds new action buttons on the product page', 1, 0),
(43, 'displayBackOfficeHome', 'Administration panel homepage', 'This hook is displayed on the admin panel\'s homepage', 1, 0),
(44, 'displayAdminStatsGridEngine', 'Grid engines', '', 1, 0),
(45, 'actionWatermark', 'Watermark', '', 1, 0),
(46, 'actionProductCancel', 'Product cancelled', 'This hook is called when you cancel a product in an order', 1, 0),
(47, 'displayLeftColumnProduct', 'New elements on the product page (left column)', 'This hook displays new elements in the left-hand column of the product page', 1, 0),
(48, 'actionProductOutOfStock', 'Out-of-stock product', 'This hook displays new action buttons if a product is out of stock', 1, 0),
(49, 'actionProductAttributeUpdate', 'Product attribute update', 'This hook is displayed when a product\'s attribute is updated', 1, 0),
(50, 'displayCarrierList', 'Extra carrier (module mode)', '', 1, 0),
(51, 'displayShoppingCart', 'Shopping cart - Additional button', 'This hook displays new action buttons within the shopping cart', 1, 0),
(52, 'actionSearch', 'Search', '', 1, 0),
(53, 'displayBeforePayment', 'Redirect during the order process', 'This hook redirects the user to the module instead of displaying payment modules', 1, 0),
(54, 'actionCarrierUpdate', 'Carrier Update', 'This hook is called when a carrier is updated', 1, 0),
(55, 'actionOrderStatusPostUpdate', 'Post update of order status', '', 1, 0),
(56, 'displayCustomerAccountFormTop', 'Block above the form for create an account', 'This hook is displayed above the customer\'s account creation form', 1, 0),
(57, 'displayBackOfficeHeader', 'Administration panel header', 'This hook is displayed in the header of the admin panel', 1, 0),
(58, 'displayBackOfficeTop', 'Administration panel hover the tabs', 'This hook is displayed on the roll hover of the tabs within the admin panel', 1, 0),
(59, 'displayBackOfficeFooter', 'Administration panel footer', 'This hook is displayed within the admin panel\'s footer', 1, 0),
(60, 'actionProductAttributeDelete', 'Product attribute deletion', 'This hook is displayed when a product\'s attribute is deleted', 1, 0),
(61, 'actionCarrierProcess', 'Carrier process', '', 1, 0),
(62, 'actionOrderDetail', 'Order detail', 'This hook is used to set the follow-up in Smarty when an order\'s detail is called', 1, 0),
(63, 'displayBeforeCarrier', 'Before carriers list', 'This hook is displayed before the carrier list in Front Office', 1, 0),
(64, 'displayOrderDetail', 'Order detail', 'This hook is displayed within the order\'s details in Front Office', 1, 0),
(65, 'actionPaymentCCAdd', 'Payment CC added', '', 1, 0),
(66, 'displayProductComparison', 'Extra product comparison', '', 1, 0),
(67, 'actionCategoryAdd', 'Category creation', 'This hook is displayed when a category is created', 1, 0),
(68, 'actionCategoryUpdate', 'Category modification', 'This hook is displayed when a category is modified', 1, 0),
(69, 'actionCategoryDelete', 'Category deletion', 'This hook is displayed when a category is deleted', 1, 0),
(70, 'actionBeforeAuthentication', 'Before authentication', 'This hook is displayed before the customer\'s authentication', 1, 0),
(71, 'displayPaymentTop', 'Top of payment page', 'This hook is displayed at the top of the payment page', 1, 0),
(72, 'actionHtaccessCreate', 'After htaccess creation', 'This hook is displayed after the htaccess creation', 1, 0),
(73, 'actionAdminMetaSave', 'After saving the configuration in AdminMeta', 'This hook is displayed after saving the configuration in AdminMeta', 1, 0),
(74, 'displayAttributeGroupForm', 'Add fields to the form \'attribute group\'', 'This hook adds fields to the form \'attribute group\'', 1, 0),
(75, 'actionAttributeGroupSave', 'Saving an attribute group', 'This hook is called while saving an attributes group', 1, 0),
(76, 'actionAttributeGroupDelete', 'Deleting attribute group', 'This hook is called while deleting an attributes  group', 1, 0),
(77, 'displayFeatureForm', 'Add fields to the form \'feature\'', 'This hook adds fields to the form \'feature\'', 1, 0),
(78, 'actionFeatureSave', 'Saving attributes\' features', 'This hook is called while saving an attributes features', 1, 0),
(79, 'actionFeatureDelete', 'Deleting attributes\' features', 'This hook is called while deleting an attributes features', 1, 0),
(80, 'actionProductSave', 'Saving products', 'This hook is called while saving products', 1, 0),
(81, 'actionProductListOverride', 'Assign a products list to a category', 'This hook assigns a products list to a category', 1, 0),
(82, 'displayAttributeGroupPostProcess', 'On post-process in admin attribute group', 'This hook is called on post-process in admin attribute group', 1, 0),
(83, 'displayFeaturePostProcess', 'On post-process in admin feature', 'This hook is called on post-process in admin feature', 1, 0),
(84, 'displayFeatureValueForm', 'Add fields to the form \'feature value\'', 'This hook adds fields to the form \'feature value\'', 1, 0),
(85, 'displayFeatureValuePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 0),
(86, 'actionFeatureValueDelete', 'Deleting attributes\' features\' values', 'This hook is called while deleting an attributes features value', 1, 0),
(87, 'actionFeatureValueSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 0),
(88, 'displayAttributeForm', 'Add fields to the form \'attribute value\'', 'This hook adds fields to the form \'attribute value\'', 1, 0),
(89, 'actionAttributePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 0),
(90, 'actionAttributeDelete', 'Deleting an attributes features value', 'This hook is called while deleting an attributes features value', 1, 0),
(91, 'actionAttributeSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 0),
(92, 'actionTaxManager', 'Tax Manager Factory', '', 1, 0),
(93, 'displayMyAccountBlock', 'My account block', 'This hook displays extra information within the \'my account\' block\"', 1, 0),
(94, 'actionModuleInstallBefore', 'actionModuleInstallBefore', '', 1, 0),
(95, 'actionModuleInstallAfter', 'actionModuleInstallAfter', '', 1, 0),
(96, 'displayHomeTab', 'Home Page Tabs', 'This hook displays new elements on the homepage tabs', 1, 1),
(97, 'displayHomeTabContent', 'Home Page Tabs Content', 'This hook displays new elements on the homepage tabs content', 1, 1),
(98, 'displayTopColumn', 'Top column blocks', 'This hook displays new elements in the top of columns', 1, 1),
(99, 'displayBackOfficeCategory', 'Display new elements in the Back Office, tab AdminCategories', 'This hook launches modules when the AdminCategories tab is displayed in the Back Office', 1, 0),
(100, 'displayProductListFunctionalButtons', 'Display new elements in the Front Office, products list', 'This hook launches modules when the products list is displayed in the Front Office', 1, 0),
(101, 'displayNav', 'Navigation', '', 1, 1),
(102, 'displayOverrideTemplate', 'Change the default template of current controller', '', 1, 0),
(103, 'actionAdminLoginControllerSetMedia', 'Set media on admin login page header', 'This hook is called after adding media to admin login page header', 1, 0),
(104, 'actionOrderEdited', 'Order edited', 'This hook is called when an order is edited.', 1, 0),
(105, 'actionEmailAddBeforeContent', 'Add extra content before mail content', 'This hook is called just before fetching mail template', 1, 0),
(106, 'actionEmailAddAfterContent', 'Add extra content after mail content', 'This hook is called just after fetching mail template', 1, 0),
(107, 'displayCartExtraProductActions', 'Extra buttons in shopping cart', 'This hook adds extra buttons to the product lines, in the shopping cart', 1, 0),
(108, 'actionObjectProductUpdateAfter', 'actionObjectProductUpdateAfter', '', 0, 0),
(109, 'actionObjectProductDeleteAfter', 'actionObjectProductDeleteAfter', '', 0, 0),
(110, 'displayCompareExtraInformation', 'displayCompareExtraInformation', '', 1, 1),
(111, 'displaySocialSharing', 'displaySocialSharing', '', 1, 1),
(112, 'displayBanner', 'displayBanner', '', 1, 1),
(113, 'actionObjectLanguageAddAfter', 'actionObjectLanguageAddAfter', '', 0, 0),
(114, 'displayPaymentEU', 'displayPaymentEU', '', 1, 1),
(115, 'actionCartListOverride', 'actionCartListOverride', '', 0, 0),
(116, 'actionAdminMetaControllerUpdate_optionsBefore', 'actionAdminMetaControllerUpdate_optionsBefore', '', 0, 0),
(117, 'actionAdminLanguagesControllerStatusBefore', 'actionAdminLanguagesControllerStatusBefore', '', 0, 0),
(118, 'actionObjectCmsUpdateAfter', 'actionObjectCmsUpdateAfter', '', 0, 0),
(119, 'actionObjectCmsDeleteAfter', 'actionObjectCmsDeleteAfter', '', 0, 0),
(120, 'actionShopDataDuplication', 'actionShopDataDuplication', '', 0, 0),
(121, 'actionAdminStoresControllerUpdate_optionsAfter', 'actionAdminStoresControllerUpdate_optionsAfter', '', 0, 0),
(122, 'actionObjectManufacturerDeleteAfter', 'actionObjectManufacturerDeleteAfter', '', 0, 0),
(123, 'actionObjectManufacturerAddAfter', 'actionObjectManufacturerAddAfter', '', 0, 0),
(124, 'actionObjectManufacturerUpdateAfter', 'actionObjectManufacturerUpdateAfter', '', 0, 0),
(126, 'actionModuleRegisterHookAfter', 'actionModuleRegisterHookAfter', '', 0, 0),
(127, 'actionModuleUnRegisterHookAfter', 'actionModuleUnRegisterHookAfter', '', 0, 0),
(128, 'displayMyAccountBlockfooter', 'My account block', 'Display extra informations inside the \"my account\" block', 1, 0),
(129, 'registerGDPRConsent', 'registerGDPRConsent', '', 0, 0),
(130, 'actionExportGDPRData', 'actionExportGDPRData', '', 0, 0),
(131, 'actionDeleteGDPRCustomer', 'actionDeleteGDPRCustomer', '', 0, 0),
(132, 'displayMobileTopSiteMap', 'displayMobileTopSiteMap', '', 1, 1),
(133, 'displaySearch', 'displaySearch', '', 1, 1),
(134, 'actionObjectSupplierDeleteAfter', 'actionObjectSupplierDeleteAfter', '', 0, 0),
(135, 'actionObjectSupplierAddAfter', 'actionObjectSupplierAddAfter', '', 0, 0),
(136, 'actionObjectSupplierUpdateAfter', 'actionObjectSupplierUpdateAfter', '', 0, 0),
(137, 'actionObjectCategoryUpdateAfter', 'actionObjectCategoryUpdateAfter', '', 0, 0),
(138, 'actionObjectCategoryDeleteAfter', 'actionObjectCategoryDeleteAfter', '', 0, 0),
(139, 'actionObjectCategoryAddAfter', 'actionObjectCategoryAddAfter', '', 0, 0),
(140, 'actionObjectCmsAddAfter', 'actionObjectCmsAddAfter', '', 0, 0),
(141, 'actionObjectProductAddAfter', 'actionObjectProductAddAfter', '', 0, 0),
(142, 'dashboardZoneOne', 'dashboardZoneOne', '', 0, 0),
(143, 'dashboardData', 'dashboardData', '', 0, 0),
(144, 'actionObjectOrderAddAfter', 'actionObjectOrderAddAfter', '', 0, 0),
(145, 'actionObjectCustomerAddAfter', 'actionObjectCustomerAddAfter', '', 0, 0),
(146, 'actionObjectCustomerMessageAddAfter', 'actionObjectCustomerMessageAddAfter', '', 0, 0),
(147, 'actionObjectCustomerThreadAddAfter', 'actionObjectCustomerThreadAddAfter', '', 0, 0),
(148, 'actionObjectOrderReturnAddAfter', 'actionObjectOrderReturnAddAfter', '', 0, 0),
(149, 'actionAdminControllerSetMedia', 'actionAdminControllerSetMedia', '', 0, 0),
(150, 'dashboardZoneTwo', 'dashboardZoneTwo', '', 0, 0),
(151, 'actionAdminMetaControllerUpdate_optionsAfter', 'actionAdminMetaControllerUpdate_optionsAfter', '', 0, 0),
(152, 'actionAdminPerformanceControllerSaveAfter', 'actionAdminPerformanceControllerSaveAfter', '', 0, 0),
(153, 'actionObjectCarrierAddAfter', 'actionObjectCarrierAddAfter', '', 0, 0),
(154, 'actionObjectContactAddAfter', 'actionObjectContactAddAfter', '', 0, 0),
(155, 'actionAdminThemesControllerUpdate_optionsAfter', 'actionAdminThemesControllerUpdate_optionsAfter', '', 0, 0),
(156, 'actionObjectShopUpdateAfter', 'actionObjectShopUpdateAfter', '', 0, 0),
(157, 'actionAdminPreferencesControllerUpdate_optionsAfter', 'actionAdminPreferencesControllerUpdate_optionsAfter', '', 0, 0),
(158, 'actionObjectShopAddAfter', 'actionObjectShopAddAfter', '', 0, 0),
(159, 'actionObjectShopGroupAddAfter', 'actionObjectShopGroupAddAfter', '', 0, 0),
(160, 'actionObjectCartAddAfter', 'actionObjectCartAddAfter', '', 0, 0),
(161, 'actionObjectEmployeeAddAfter', 'actionObjectEmployeeAddAfter', '', 0, 0),
(162, 'actionObjectImageAddAfter', 'actionObjectImageAddAfter', '', 0, 0),
(163, 'actionObjectCartRuleAddAfter', 'actionObjectCartRuleAddAfter', '', 0, 0),
(164, 'actionAdminStoresControllerSaveAfter', 'actionAdminStoresControllerSaveAfter', '', 0, 0),
(165, 'actionAdminWebserviceControllerSaveAfter', 'actionAdminWebserviceControllerSaveAfter', '', 0, 0),
(166, 'displayProductExtraContent', 'displayProductExtraContent', '', 1, 1),
(167, 'displayAdminProductsExtra', 'displayAdminProductsExtra', '', 1, 1),
(168, 'displayCustomSlick', 'displayCustomSlick', '', 1, 1),
(169, 'topcolumn', 'topcolumn', '', 0, 0),
(170, 'actionBeforeCartUpdateQty', 'actionBeforeCartUpdateQty', '', 0, 0),
(171, 'displayMobileHeader', 'displayMobileHeader', '', 1, 1),
(172, 'displayMobileShoppingCartTop', 'displayMobileShoppingCartTop', '', 1, 1),
(173, 'displayMobileAddToCartTop', 'displayMobileAddToCartTop', '', 1, 1),
(174, 'actionPSCleanerGetModulesTables', 'actionPSCleanerGetModulesTables', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_hook_alias`
--

CREATE TABLE `ap_hook_alias` (
  `id_hook_alias` int(10) UNSIGNED NOT NULL,
  `alias` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_hook_alias`
--

INSERT INTO `ap_hook_alias` (`id_hook_alias`, `alias`, `name`) VALUES
(1, 'payment', 'displayPayment'),
(2, 'newOrder', 'actionValidateOrder'),
(3, 'paymentConfirm', 'actionPaymentConfirmation'),
(4, 'paymentReturn', 'displayPaymentReturn'),
(5, 'updateQuantity', 'actionUpdateQuantity'),
(6, 'rightColumn', 'displayRightColumn'),
(7, 'leftColumn', 'displayLeftColumn'),
(8, 'home', 'displayHome'),
(9, 'displayHeader', 'Header'),
(10, 'cart', 'actionCartSave'),
(11, 'authentication', 'actionAuthentication'),
(12, 'addproduct', 'actionProductAdd'),
(13, 'updateproduct', 'actionProductUpdate'),
(14, 'top', 'displayTop'),
(15, 'extraRight', 'displayRightColumnProduct'),
(16, 'deleteproduct', 'actionProductDelete'),
(17, 'productfooter', 'displayFooterProduct'),
(18, 'invoice', 'displayInvoice'),
(19, 'updateOrderStatus', 'actionOrderStatusUpdate'),
(20, 'adminOrder', 'displayAdminOrder'),
(21, 'footer', 'displayFooter'),
(22, 'PDFInvoice', 'displayPDFInvoice'),
(23, 'adminCustomers', 'displayAdminCustomers'),
(24, 'orderConfirmation', 'displayOrderConfirmation'),
(25, 'createAccount', 'actionCustomerAccountAdd'),
(26, 'customerAccount', 'displayCustomerAccount'),
(27, 'orderSlip', 'actionOrderSlipAdd'),
(28, 'productTab', 'displayProductTab'),
(29, 'productTabContent', 'displayProductTabContent'),
(30, 'shoppingCart', 'displayShoppingCartFooter'),
(31, 'createAccountForm', 'displayCustomerAccountForm'),
(32, 'AdminStatsModules', 'displayAdminStatsModules'),
(33, 'GraphEngine', 'displayAdminStatsGraphEngine'),
(34, 'orderReturn', 'actionOrderReturn'),
(35, 'productActions', 'displayProductButtons'),
(36, 'backOfficeHome', 'displayBackOfficeHome'),
(37, 'GridEngine', 'displayAdminStatsGridEngine'),
(38, 'watermark', 'actionWatermark'),
(39, 'cancelProduct', 'actionProductCancel'),
(40, 'extraLeft', 'displayLeftColumnProduct'),
(41, 'productOutOfStock', 'actionProductOutOfStock'),
(42, 'updateProductAttribute', 'actionProductAttributeUpdate'),
(43, 'extraCarrier', 'displayCarrierList'),
(44, 'shoppingCartExtra', 'displayShoppingCart'),
(45, 'search', 'actionSearch'),
(46, 'backBeforePayment', 'displayBeforePayment'),
(47, 'updateCarrier', 'actionCarrierUpdate'),
(48, 'postUpdateOrderStatus', 'actionOrderStatusPostUpdate'),
(49, 'createAccountTop', 'displayCustomerAccountFormTop'),
(50, 'backOfficeHeader', 'displayBackOfficeHeader'),
(51, 'backOfficeTop', 'displayBackOfficeTop'),
(52, 'backOfficeFooter', 'displayBackOfficeFooter'),
(53, 'deleteProductAttribute', 'actionProductAttributeDelete'),
(54, 'processCarrier', 'actionCarrierProcess'),
(55, 'orderDetail', 'actionOrderDetail'),
(56, 'beforeCarrier', 'displayBeforeCarrier'),
(57, 'orderDetailDisplayed', 'displayOrderDetail'),
(58, 'paymentCCAdded', 'actionPaymentCCAdd'),
(59, 'extraProductComparison', 'displayProductComparison'),
(60, 'categoryAddition', 'actionCategoryAdd'),
(61, 'categoryUpdate', 'actionCategoryUpdate'),
(62, 'categoryDeletion', 'actionCategoryDelete'),
(63, 'beforeAuthentication', 'actionBeforeAuthentication'),
(64, 'paymentTop', 'displayPaymentTop'),
(65, 'afterCreateHtaccess', 'actionHtaccessCreate'),
(66, 'afterSaveAdminMeta', 'actionAdminMetaSave'),
(67, 'attributeGroupForm', 'displayAttributeGroupForm'),
(68, 'afterSaveAttributeGroup', 'actionAttributeGroupSave'),
(69, 'afterDeleteAttributeGroup', 'actionAttributeGroupDelete'),
(70, 'featureForm', 'displayFeatureForm'),
(71, 'afterSaveFeature', 'actionFeatureSave'),
(72, 'afterDeleteFeature', 'actionFeatureDelete'),
(73, 'afterSaveProduct', 'actionProductSave'),
(74, 'productListAssign', 'actionProductListOverride'),
(75, 'postProcessAttributeGroup', 'displayAttributeGroupPostProcess'),
(76, 'postProcessFeature', 'displayFeaturePostProcess'),
(77, 'featureValueForm', 'displayFeatureValueForm'),
(78, 'postProcessFeatureValue', 'displayFeatureValuePostProcess'),
(79, 'afterDeleteFeatureValue', 'actionFeatureValueDelete'),
(80, 'afterSaveFeatureValue', 'actionFeatureValueSave'),
(81, 'attributeForm', 'displayAttributeForm'),
(82, 'postProcessAttribute', 'actionAttributePostProcess'),
(83, 'afterDeleteAttribute', 'actionAttributeDelete'),
(84, 'afterSaveAttribute', 'actionAttributeSave'),
(85, 'taxManager', 'actionTaxManager'),
(86, 'myAccountBlock', 'displayMyAccountBlock');

-- --------------------------------------------------------

--
-- Table structure for table `ap_hook_module`
--

CREATE TABLE `ap_hook_module` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_hook` int(10) UNSIGNED NOT NULL,
  `position` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_hook_module`
--

INSERT INTO `ap_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
(1, 1, 10, 1),
(1, 1, 16, 1),
(1, 1, 108, 1),
(1, 1, 109, 1),
(1, 1, 110, 1),
(1, 1, 111, 1),
(2, 1, 113, 1),
(3, 1, 1, 1),
(3, 1, 5, 1),
(3, 1, 114, 1),
(4, 1, 8, 1),
(4, 1, 13, 1),
(4, 1, 14, 1),
(4, 1, 17, 1),
(4, 1, 55, 1),
(5, 1, 115, 1),
(7, 1, 67, 1),
(7, 1, 68, 1),
(7, 1, 69, 1),
(7, 1, 99, 1),
(7, 1, 116, 1),
(7, 1, 117, 1),
(11, 1, 74, 1),
(11, 1, 75, 1),
(11, 1, 76, 1),
(11, 1, 77, 1),
(11, 1, 78, 1),
(11, 1, 79, 1),
(11, 1, 80, 1),
(11, 1, 81, 1),
(11, 1, 82, 1),
(11, 1, 83, 1),
(11, 1, 84, 1),
(11, 1, 85, 1),
(11, 1, 86, 1),
(11, 1, 87, 1),
(11, 1, 88, 1),
(11, 1, 89, 1),
(11, 1, 90, 1),
(11, 1, 91, 1),
(12, 1, 118, 1),
(12, 1, 119, 1),
(12, 1, 120, 1),
(12, 1, 121, 1),
(16, 1, 122, 1),
(16, 1, 123, 1),
(16, 1, 124, 1),
(17, 1, 126, 1),
(17, 1, 127, 1),
(20, 1, 26, 1),
(20, 1, 31, 1),
(20, 1, 129, 1),
(20, 1, 130, 1),
(20, 1, 131, 1),
(22, 1, 15, 1),
(22, 1, 132, 1),
(22, 1, 133, 1),
(23, 1, 96, 1),
(25, 1, 134, 1),
(25, 1, 135, 1),
(25, 1, 136, 1),
(27, 1, 137, 1),
(27, 1, 138, 1),
(27, 1, 139, 1),
(27, 1, 140, 1),
(27, 1, 141, 1),
(28, 1, 101, 1),
(31, 1, 143, 1),
(31, 1, 144, 1),
(31, 1, 145, 1),
(31, 1, 146, 1),
(31, 1, 147, 1),
(31, 1, 148, 1),
(31, 1, 149, 1),
(32, 1, 150, 1),
(34, 1, 52, 1),
(35, 1, 40, 1),
(36, 1, 44, 1),
(38, 1, 9, 1),
(40, 1, 39, 1),
(50, 1, 12, 1),
(63, 1, 57, 1),
(64, 1, 2, 1),
(64, 1, 20, 1),
(64, 1, 95, 1),
(64, 1, 151, 1),
(64, 1, 152, 1),
(64, 1, 153, 1),
(64, 1, 154, 1),
(64, 1, 155, 1),
(64, 1, 156, 1),
(64, 1, 157, 1),
(64, 1, 158, 1),
(64, 1, 159, 1),
(64, 1, 160, 1),
(64, 1, 161, 1),
(64, 1, 162, 1),
(64, 1, 163, 1),
(64, 1, 164, 1),
(64, 1, 165, 1),
(66, 1, 142, 1),
(67, 1, 29, 1),
(67, 1, 32, 1),
(67, 1, 97, 1),
(67, 1, 98, 1),
(68, 1, 35, 1),
(68, 1, 36, 1),
(68, 1, 166, 1),
(68, 1, 167, 1),
(69, 1, 168, 1),
(70, 1, 7, 1),
(70, 1, 169, 1),
(71, 1, 18, 1),
(71, 1, 21, 1),
(71, 1, 27, 1),
(71, 1, 30, 1),
(71, 1, 46, 1),
(71, 1, 51, 1),
(71, 1, 53, 1),
(71, 1, 170, 1),
(71, 1, 171, 1),
(71, 1, 172, 1),
(71, 1, 173, 1),
(71, 1, 174, 1),
(2, 1, 10, 2),
(5, 1, 15, 2),
(6, 1, 26, 2),
(7, 1, 8, 2),
(8, 1, 101, 2),
(11, 1, 67, 2),
(11, 1, 68, 2),
(11, 1, 69, 2),
(18, 1, 126, 2),
(18, 1, 127, 2),
(19, 1, 13, 2),
(19, 1, 14, 2),
(19, 1, 17, 2),
(23, 1, 97, 2),
(27, 1, 108, 2),
(27, 1, 109, 2),
(27, 1, 118, 2),
(27, 1, 119, 2),
(27, 1, 120, 2),
(27, 1, 122, 2),
(27, 1, 123, 2),
(27, 1, 124, 2),
(27, 1, 134, 2),
(27, 1, 135, 2),
(27, 1, 136, 2),
(30, 1, 1, 2),
(30, 1, 5, 2),
(30, 1, 114, 2),
(31, 1, 142, 2),
(32, 1, 55, 2),
(32, 1, 143, 2),
(32, 1, 149, 2),
(33, 1, 150, 2),
(34, 1, 144, 2),
(39, 1, 42, 2),
(41, 1, 39, 2),
(50, 1, 31, 2),
(60, 1, 52, 2),
(63, 1, 9, 2),
(63, 1, 113, 2),
(64, 1, 57, 2),
(64, 1, 121, 2),
(64, 1, 140, 2),
(64, 1, 141, 2),
(64, 1, 145, 2),
(64, 1, 147, 2),
(67, 1, 16, 2),
(67, 1, 20, 2),
(67, 1, 36, 2),
(67, 1, 96, 2),
(67, 1, 166, 2),
(67, 1, 167, 2),
(69, 1, 98, 2),
(71, 1, 7, 2),
(4, 1, 10, 3),
(7, 1, 26, 3),
(9, 1, 9, 3),
(10, 1, 101, 3),
(11, 1, 8, 3),
(23, 1, 13, 3),
(23, 1, 14, 3),
(23, 1, 17, 3),
(27, 1, 15, 3),
(27, 1, 68, 3),
(33, 1, 143, 3),
(33, 1, 149, 3),
(34, 1, 150, 3),
(37, 1, 120, 3),
(42, 1, 39, 3),
(63, 1, 98, 3),
(64, 1, 144, 3),
(65, 1, 57, 3),
(65, 1, 126, 3),
(65, 1, 127, 3),
(71, 1, 1, 3),
(71, 1, 5, 3),
(71, 1, 55, 3),
(71, 1, 114, 3),
(5, 1, 10, 4),
(12, 1, 8, 4),
(12, 1, 26, 4),
(13, 1, 9, 4),
(26, 1, 13, 4),
(26, 1, 14, 4),
(26, 1, 17, 4),
(28, 1, 15, 4),
(34, 1, 143, 4),
(35, 1, 149, 4),
(38, 1, 68, 4),
(43, 1, 39, 4),
(67, 1, 57, 4),
(70, 1, 101, 4),
(6, 1, 10, 5),
(16, 1, 8, 5),
(18, 1, 26, 5),
(38, 1, 13, 5),
(38, 1, 14, 5),
(38, 1, 17, 5),
(40, 1, 15, 5),
(44, 1, 39, 5),
(69, 1, 57, 5),
(70, 1, 9, 5),
(7, 1, 10, 6),
(15, 1, 26, 6),
(17, 1, 8, 6),
(41, 1, 15, 6),
(45, 1, 39, 6),
(68, 1, 14, 6),
(68, 1, 17, 6),
(71, 1, 57, 6),
(8, 1, 10, 7),
(19, 1, 8, 7),
(46, 1, 39, 7),
(50, 1, 26, 7),
(67, 1, 14, 7),
(67, 1, 17, 7),
(70, 1, 15, 7),
(9, 1, 10, 8),
(21, 1, 8, 8),
(47, 1, 39, 8),
(63, 1, 26, 8),
(10, 1, 10, 9),
(23, 1, 8, 9),
(48, 1, 39, 9),
(70, 1, 26, 9),
(11, 1, 10, 10),
(24, 1, 8, 10),
(49, 1, 39, 10),
(12, 1, 10, 11),
(25, 1, 8, 11),
(51, 1, 39, 11),
(14, 1, 10, 12),
(26, 1, 8, 12),
(52, 1, 39, 12),
(15, 1, 10, 13),
(29, 1, 8, 13),
(53, 1, 39, 13),
(16, 1, 10, 14),
(54, 1, 39, 14),
(63, 1, 8, 14),
(17, 1, 10, 15),
(55, 1, 39, 15),
(70, 1, 8, 15),
(18, 1, 10, 16),
(56, 1, 39, 16),
(19, 1, 10, 17),
(57, 1, 39, 17),
(20, 1, 10, 18),
(58, 1, 39, 18),
(21, 1, 10, 19),
(59, 1, 39, 19),
(22, 1, 10, 20),
(60, 1, 39, 20),
(23, 1, 10, 21),
(61, 1, 39, 21),
(24, 1, 10, 22),
(62, 1, 39, 22),
(25, 1, 10, 23),
(26, 1, 10, 24),
(27, 1, 10, 25),
(28, 1, 10, 26),
(29, 1, 10, 27),
(37, 1, 10, 28),
(38, 1, 10, 29),
(39, 1, 10, 30),
(63, 1, 10, 31),
(68, 1, 10, 32),
(69, 1, 10, 33),
(70, 1, 10, 34),
(67, 1, 10, 35),
(71, 1, 10, 36);

-- --------------------------------------------------------

--
-- Table structure for table `ap_hook_module_exceptions`
--

CREATE TABLE `ap_hook_module_exceptions` (
  `id_hook_module_exceptions` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_hook` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_hook_module_exceptions`
--

INSERT INTO `ap_hook_module_exceptions` (`id_hook_module_exceptions`, `id_shop`, `id_module`, `id_hook`, `file_name`) VALUES
(1, 1, 4, 8, 'category'),
(2, 1, 16, 8, 'category'),
(3, 1, 17, 8, 'category'),
(4, 1, 21, 8, 'category'),
(5, 1, 25, 8, 'category');

-- --------------------------------------------------------

--
-- Table structure for table `ap_image`
--

CREATE TABLE `ap_image` (
  `id_image` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` smallint(2) UNSIGNED NOT NULL DEFAULT '0',
  `cover` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_image`
--

INSERT INTO `ap_image` (`id_image`, `id_product`, `position`, `cover`) VALUES
(26, 10, 1, 1),
(27, 11, 1, 1),
(28, 12, 1, 1),
(29, 13, 1, 1),
(31, 15, 1, 1),
(33, 16, 1, 1),
(34, 17, 1, 1),
(36, 19, 1, 1),
(37, 9, 1, 1),
(38, 18, 1, 1),
(39, 20, 1, 1),
(40, 23, 1, 1),
(41, 24, 1, 1),
(42, 25, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_image_lang`
--

CREATE TABLE `ap_image_lang` (
  `id_image` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `legend` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_image_lang`
--

INSERT INTO `ap_image_lang` (`id_image`, `id_lang`, `legend`) VALUES
(26, 1, 'FORFAIT WEEK-END ET FERIE - 50 HEURES'),
(27, 1, 'FORFAIT WEEK-END ET FERIE - 20 HEURES'),
(28, 1, 'FORFAIT WEEK-END ET FERIE 10 HEURES'),
(29, 1, 'FORFAIT 50 HEURES'),
(29, 2, 'FORFAIT 50 HEURES'),
(31, 1, 'FORFAIT 30 HEURES'),
(33, 1, 'FORFAIT  20 HEURES'),
(34, 1, 'FORFAIT LIBRE 10 HEURES'),
(36, 1, 'FORFAIT LIBRE 1 HEURE'),
(37, 1, 'FORFAIT EXAMEN CANDIDAT LIBRE'),
(38, 1, 'FORFAIT LIBRE 5 HEURES'),
(39, 1, 'FORFAIT 40 HEURES'),
(40, 1, 'FORFAIT WEEK-END - 1 JOUR - 10 HEURES (Semaines, week-ends et jours fériés)'),
(41, 1, 'FORFAIT JOURNEE -  2 JOURS - 20 HEURES (semaine, week-end et jours fériés)'),
(42, 1, 'FORFAIT  -WEEK-END - 50 Heures (semaine, week-end et jours fériés)');

-- --------------------------------------------------------

--
-- Table structure for table `ap_image_shop`
--

CREATE TABLE `ap_image_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_image` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `cover` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_image_shop`
--

INSERT INTO `ap_image_shop` (`id_product`, `id_image`, `id_shop`, `cover`) VALUES
(9, 37, 1, 1),
(10, 26, 1, 1),
(11, 27, 1, 1),
(12, 28, 1, 1),
(13, 29, 1, 1),
(15, 31, 1, 1),
(16, 33, 1, 1),
(17, 34, 1, 1),
(18, 38, 1, 1),
(19, 36, 1, 1),
(20, 39, 1, 1),
(23, 40, 1, 1),
(24, 41, 1, 1),
(25, 42, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_image_type`
--

CREATE TABLE `ap_image_type` (
  `id_image_type` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `width` int(10) UNSIGNED NOT NULL,
  `height` int(10) UNSIGNED NOT NULL,
  `products` tinyint(1) NOT NULL DEFAULT '1',
  `categories` tinyint(1) NOT NULL DEFAULT '1',
  `manufacturers` tinyint(1) NOT NULL DEFAULT '1',
  `suppliers` tinyint(1) NOT NULL DEFAULT '1',
  `scenes` tinyint(1) NOT NULL DEFAULT '1',
  `stores` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_image_type`
--

INSERT INTO `ap_image_type` (`id_image_type`, `name`, `width`, `height`, `products`, `categories`, `manufacturers`, `suppliers`, `scenes`, `stores`) VALUES
(1, 'cart_default', 80, 80, 1, 0, 0, 0, 0, 0),
(2, 'small_default', 98, 98, 1, 0, 1, 1, 0, 0),
(3, 'medium_default', 125, 125, 1, 1, 1, 1, 0, 1),
(4, 'home_default', 250, 250, 1, 0, 0, 0, 0, 0),
(5, 'large_default', 458, 458, 1, 0, 1, 1, 0, 0),
(6, 'thickbox_default', 800, 800, 1, 0, 0, 0, 0, 0),
(7, 'category_default', 870, 217, 0, 1, 0, 0, 0, 0),
(8, 'scene_default', 870, 270, 0, 0, 0, 0, 1, 0),
(9, 'm_scene_default', 161, 58, 0, 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_import_match`
--

CREATE TABLE `ap_import_match` (
  `id_import_match` int(10) NOT NULL,
  `name` varchar(32) NOT NULL,
  `match` text NOT NULL,
  `skip` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_info`
--

CREATE TABLE `ap_info` (
  `id_info` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_info`
--

INSERT INTO `ap_info` (`id_info`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_info_lang`
--

CREATE TABLE `ap_info_lang` (
  `id_info` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_info_lang`
--

INSERT INTO `ap_info_lang` (`id_info`, `id_lang`, `text`) VALUES
(1, 1, '<ul><li><em class=\"icon-laptop\" id=\"icon-truck\"></em>\n<div class=\"type-text\">\n<h3></h3>\n<h3>France Novatech <a href=\"http://www.francenovatech.fr\" target=\"_blank\">(Lien)</a></h3>\nSaint-Pierre 97410\n<p>Conception de logiciel et site web</p>\n</div>\n</li>\n<li><em class=\"icon-car\" id=\"icon-phone\"></em>\n<div class=\"type-text\">\n<h3>Opel Mascareigne</h3>\nSaint Pierre - 97410\n<p>Concessionnaire OPEL</p>\n</div>\n</li>\n<li><em class=\"icon-shield\" id=\"icon-credit-card\"></em>\n<h3 class=\"type-text\">Groupama OI</h3>\nSaint Pierre - 97410\n<p>Assurances </p>\n</li>\n</ul>'),
(2, 1, '<h3>Auto Permis</h3>\n<h4>Auto Permis est la première société de location de voiture à double commande pour pour candidat libre à la Réunion.</h4>\n<p>Depuis 2012, la loi Macron ré-écrit le permis de conduire à grand coup de réforme pour que le permis soit accessible à tous, et dans son prix et dans sa longueur.</p>\n<p>Auto Permis met à votre disposition des véhicules à double commande pour votre sécurité et Vous donne la possibilité de vous former et de passer vos épreuves du permis avec vos proches.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `ap_lang`
--

CREATE TABLE `ap_lang` (
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `iso_code` char(2) NOT NULL,
  `language_code` char(5) NOT NULL,
  `date_format_lite` char(32) NOT NULL DEFAULT 'Y-m-d',
  `date_format_full` char(32) NOT NULL DEFAULT 'Y-m-d H:i:s',
  `is_rtl` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_lang`
--

INSERT INTO `ap_lang` (`id_lang`, `name`, `active`, `iso_code`, `language_code`, `date_format_lite`, `date_format_full`, `is_rtl`) VALUES
(1, 'Français (French)', 1, 'fr', 'fr-fr', 'd/m/Y', 'd/m/Y H:i:s', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_lang_shop`
--

CREATE TABLE `ap_lang_shop` (
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_lang_shop`
--

INSERT INTO `ap_lang_shop` (`id_lang`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_category`
--

CREATE TABLE `ap_layered_category` (
  `id_layered_category` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_value` int(10) UNSIGNED DEFAULT '0',
  `type` enum('category','id_feature','id_attribute_group','quantity','condition','manufacturer','weight','price') NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `filter_type` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `filter_show_limit` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_layered_category`
--

INSERT INTO `ap_layered_category` (`id_layered_category`, `id_shop`, `id_category`, `id_value`, `type`, `position`, `filter_type`, `filter_show_limit`) VALUES
(1, 1, 2, NULL, 'category', 1, 0, 0),
(2, 1, 2, 1, 'id_attribute_group', 2, 0, 0),
(3, 1, 2, 3, 'id_attribute_group', 3, 0, 0),
(4, 1, 2, 5, 'id_feature', 4, 0, 0),
(5, 1, 2, 6, 'id_feature', 5, 0, 0),
(6, 1, 2, 7, 'id_feature', 6, 0, 0),
(7, 1, 2, NULL, 'quantity', 7, 0, 0),
(8, 1, 2, NULL, 'manufacturer', 8, 0, 0),
(9, 1, 2, NULL, 'condition', 9, 0, 0),
(10, 1, 2, NULL, 'weight', 10, 0, 0),
(11, 1, 2, NULL, 'price', 11, 0, 0),
(12, 1, 4, NULL, 'category', 1, 0, 0),
(13, 1, 4, 1, 'id_attribute_group', 2, 0, 0),
(14, 1, 4, 3, 'id_attribute_group', 3, 0, 0),
(15, 1, 4, 5, 'id_feature', 4, 0, 0),
(16, 1, 4, 6, 'id_feature', 5, 0, 0),
(17, 1, 4, 7, 'id_feature', 6, 0, 0),
(18, 1, 4, NULL, 'quantity', 7, 0, 0),
(19, 1, 4, NULL, 'manufacturer', 8, 0, 0),
(20, 1, 4, NULL, 'condition', 9, 0, 0),
(21, 1, 4, NULL, 'weight', 10, 0, 0),
(22, 1, 4, NULL, 'price', 11, 0, 0),
(23, 1, 5, NULL, 'category', 1, 0, 0),
(24, 1, 5, 1, 'id_attribute_group', 2, 0, 0),
(25, 1, 5, 3, 'id_attribute_group', 3, 0, 0),
(26, 1, 5, 5, 'id_feature', 4, 0, 0),
(27, 1, 5, 6, 'id_feature', 5, 0, 0),
(28, 1, 5, 7, 'id_feature', 6, 0, 0),
(29, 1, 5, NULL, 'quantity', 7, 0, 0),
(30, 1, 5, NULL, 'manufacturer', 8, 0, 0),
(31, 1, 5, NULL, 'condition', 9, 0, 0),
(32, 1, 5, NULL, 'weight', 10, 0, 0),
(33, 1, 5, NULL, 'price', 11, 0, 0),
(34, 1, 7, NULL, 'category', 1, 0, 0),
(35, 1, 7, 1, 'id_attribute_group', 2, 0, 0),
(36, 1, 7, 3, 'id_attribute_group', 3, 0, 0),
(37, 1, 7, 5, 'id_feature', 4, 0, 0),
(38, 1, 7, 6, 'id_feature', 5, 0, 0),
(39, 1, 7, 7, 'id_feature', 6, 0, 0),
(40, 1, 7, NULL, 'quantity', 7, 0, 0),
(41, 1, 7, NULL, 'manufacturer', 8, 0, 0),
(42, 1, 7, NULL, 'condition', 9, 0, 0),
(43, 1, 7, NULL, 'weight', 10, 0, 0),
(44, 1, 7, NULL, 'price', 11, 0, 0),
(45, 1, 8, NULL, 'category', 1, 0, 0),
(46, 1, 8, 1, 'id_attribute_group', 2, 0, 0),
(47, 1, 8, 3, 'id_attribute_group', 3, 0, 0),
(48, 1, 8, 5, 'id_feature', 4, 0, 0),
(49, 1, 8, 6, 'id_feature', 5, 0, 0),
(50, 1, 8, 7, 'id_feature', 6, 0, 0),
(51, 1, 8, NULL, 'quantity', 7, 0, 0),
(52, 1, 8, NULL, 'manufacturer', 8, 0, 0),
(53, 1, 8, NULL, 'condition', 9, 0, 0),
(54, 1, 8, NULL, 'weight', 10, 0, 0),
(55, 1, 8, NULL, 'price', 11, 0, 0),
(56, 1, 9, NULL, 'category', 1, 0, 0),
(57, 1, 9, 1, 'id_attribute_group', 2, 0, 0),
(58, 1, 9, 3, 'id_attribute_group', 3, 0, 0),
(59, 1, 9, 5, 'id_feature', 4, 0, 0),
(60, 1, 9, 6, 'id_feature', 5, 0, 0),
(61, 1, 9, 7, 'id_feature', 6, 0, 0),
(62, 1, 9, NULL, 'quantity', 7, 0, 0),
(63, 1, 9, NULL, 'manufacturer', 8, 0, 0),
(64, 1, 9, NULL, 'condition', 9, 0, 0),
(65, 1, 9, NULL, 'weight', 10, 0, 0),
(66, 1, 9, NULL, 'price', 11, 0, 0),
(67, 1, 10, NULL, 'category', 1, 0, 0),
(68, 1, 10, 1, 'id_attribute_group', 2, 0, 0),
(69, 1, 10, 3, 'id_attribute_group', 3, 0, 0),
(70, 1, 10, 5, 'id_feature', 4, 0, 0),
(71, 1, 10, 6, 'id_feature', 5, 0, 0),
(72, 1, 10, 7, 'id_feature', 6, 0, 0),
(73, 1, 10, NULL, 'quantity', 7, 0, 0),
(74, 1, 10, NULL, 'manufacturer', 8, 0, 0),
(75, 1, 10, NULL, 'condition', 9, 0, 0),
(76, 1, 10, NULL, 'weight', 10, 0, 0),
(77, 1, 10, NULL, 'price', 11, 0, 0),
(78, 1, 11, NULL, 'category', 1, 0, 0),
(79, 1, 11, 1, 'id_attribute_group', 2, 0, 0),
(80, 1, 11, 3, 'id_attribute_group', 3, 0, 0),
(81, 1, 11, 5, 'id_feature', 4, 0, 0),
(82, 1, 11, 6, 'id_feature', 5, 0, 0),
(83, 1, 11, 7, 'id_feature', 6, 0, 0),
(84, 1, 11, NULL, 'quantity', 7, 0, 0),
(85, 1, 11, NULL, 'manufacturer', 8, 0, 0),
(86, 1, 11, NULL, 'condition', 9, 0, 0),
(87, 1, 11, NULL, 'weight', 10, 0, 0),
(88, 1, 11, NULL, 'price', 11, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_filter`
--

CREATE TABLE `ap_layered_filter` (
  `id_layered_filter` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `filters` text,
  `n_categories` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_filter`
--

INSERT INTO `ap_layered_filter` (`id_layered_filter`, `name`, `filters`, `n_categories`, `date_add`) VALUES
(1, 'Mon modèle 2019-06-08', 'a:13:{s:10:\"categories\";a:8:{i:0;i:2;i:2;i:4;i:3;i:5;i:4;i:7;i:5;i:8;i:6;i:9;i:7;i:10;i:8;i:11;}s:9:\"shop_list\";a:1:{i:1;i:1;}s:31:\"layered_selection_subcategories\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_1\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_3\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:24:\"layered_selection_feat_5\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:24:\"layered_selection_feat_6\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:24:\"layered_selection_feat_7\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:23:\"layered_selection_stock\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:30:\"layered_selection_manufacturer\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:27:\"layered_selection_condition\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:31:\"layered_selection_weight_slider\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:30:\"layered_selection_price_slider\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}}', 9, '2019-06-08 00:34:30');

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_filter_shop`
--

CREATE TABLE `ap_layered_filter_shop` (
  `id_layered_filter` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_filter_shop`
--

INSERT INTO `ap_layered_filter_shop` (`id_layered_filter`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_friendly_url`
--

CREATE TABLE `ap_layered_friendly_url` (
  `id_layered_friendly_url` int(11) NOT NULL,
  `url_key` varchar(32) NOT NULL,
  `data` varchar(200) NOT NULL,
  `id_lang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_friendly_url`
--

INSERT INTO `ap_layered_friendly_url` (`id_layered_friendly_url`, `url_key`, `data`, `id_lang`) VALUES
(1, '36a16718f4cb2ce8d9f3b35d415e63d6', 'a:1:{s:8:\"category\";a:1:{i:2;s:1:\"2\";}}', 1),
(2, '7a4ffc9cb67cf29eef533c17393dfc3e', 'a:1:{s:8:\"category\";a:1:{i:7;s:1:\"7\";}}', 1),
(3, '51e8d398ed66c295641c8dca1161ebd6', 'a:1:{s:8:\"category\";a:1:{i:3;s:1:\"3\";}}', 1),
(4, '0f41ee5f2abf55e5393f435e5e0bd9d4', 'a:1:{s:8:\"category\";a:1:{i:1;s:1:\"1\";}}', 1),
(5, 'ae60a0eece843ba21f76674d40c774b6', 'a:1:{s:8:\"category\";a:1:{i:8;s:1:\"8\";}}', 1),
(6, 'c5d0498c409ac4d725f0759736f8470a', 'a:1:{s:8:\"category\";a:1:{i:11;s:2:\"11\";}}', 1),
(7, '65c1021a065e407623177257c509da90', 'a:1:{s:8:\"category\";a:1:{i:10;s:2:\"10\";}}', 1),
(8, '2e0199d50a3c1ee9fb40e6318f2ca6be', 'a:1:{s:8:\"category\";a:1:{i:9;s:1:\"9\";}}', 1),
(9, '6fc253242f3fe98946ecdd26762e95eb', 'a:1:{s:8:\"category\";a:1:{i:5;s:1:\"5\";}}', 1),
(10, 'c66ef06ef2ca8b06dd3d19b70727adb7', 'a:1:{s:8:\"category\";a:1:{i:4;s:1:\"4\";}}', 1),
(11, 'c4d7335317f2f1ba381e038fb625d918', 'a:1:{s:10:\"id_feature\";a:1:{i:1;s:3:\"5_1\";}}', 1),
(12, '518876640cfedb267b8df5683b243a83', 'a:1:{s:10:\"id_feature\";a:1:{i:2;s:3:\"5_2\";}}', 1),
(13, '823192a052e44927f06b39b32bcef002', 'a:1:{s:10:\"id_feature\";a:1:{i:3;s:3:\"5_3\";}}', 1),
(14, 'b738d5f9723e1d914d9ba5c7a0265d44', 'a:1:{s:10:\"id_feature\";a:1:{i:4;s:3:\"5_4\";}}', 1),
(15, 'c4379cd7a4a9ee1db8aed47692d81117', 'a:1:{s:10:\"id_feature\";a:1:{i:5;s:3:\"5_5\";}}', 1),
(16, '957ab4ceed0e6b5def103afd50c00541', 'a:1:{s:10:\"id_feature\";a:1:{i:6;s:3:\"5_6\";}}', 1),
(17, '07cf4b14281fa5bc7ec5bfec06e39805', 'a:1:{s:10:\"id_feature\";a:1:{i:7;s:3:\"5_7\";}}', 1),
(18, '368959b3bd09a132e577e2bebc8f2686', 'a:1:{s:10:\"id_feature\";a:1:{i:8;s:3:\"5_8\";}}', 1),
(19, 'c42fcf15b837dfe9d2ddd6264567944a', 'a:1:{s:10:\"id_feature\";a:1:{i:9;s:3:\"5_9\";}}', 1),
(20, '185d6e37f34a04af8207e7fe8f3c4d2e', 'a:1:{s:10:\"id_feature\";a:1:{i:10;s:4:\"6_10\";}}', 1),
(21, '0dd0ff328ece7352a6df6754b07137da', 'a:1:{s:10:\"id_feature\";a:1:{i:11;s:4:\"6_11\";}}', 1),
(22, '0fddc3ff926b2198af48f892e1d2f21d', 'a:1:{s:10:\"id_feature\";a:1:{i:12;s:4:\"6_12\";}}', 1),
(23, 'eb4ca448a07a6639ebc63d8acf45b166', 'a:1:{s:10:\"id_feature\";a:1:{i:13;s:4:\"6_13\";}}', 1),
(24, '00dff7b63b6f7ddb4b341a9308422730', 'a:1:{s:10:\"id_feature\";a:1:{i:14;s:4:\"6_14\";}}', 1),
(25, '2c30af3405dcf541af55d379e5f70ad2', 'a:1:{s:10:\"id_feature\";a:1:{i:15;s:4:\"6_15\";}}', 1),
(26, 'd6147565988940adfe08e736ed044c03', 'a:1:{s:10:\"id_feature\";a:1:{i:16;s:4:\"6_16\";}}', 1),
(27, '7aecf03cdc7cb968b794038a9eb617f5', 'a:1:{s:10:\"id_feature\";a:1:{i:17;s:4:\"7_17\";}}', 1),
(28, '0c5c01c6a5703cbeabcaa24ff4a3f18e', 'a:1:{s:10:\"id_feature\";a:1:{i:18;s:4:\"7_18\";}}', 1),
(29, 'c1cce7ff7605ea2677865d04038a693a', 'a:1:{s:10:\"id_feature\";a:1:{i:19;s:4:\"7_19\";}}', 1),
(30, '5e33f621d32f8aacd7fea1a3e381f58f', 'a:1:{s:10:\"id_feature\";a:1:{i:20;s:4:\"7_20\";}}', 1),
(31, 'bc632b4cd2605492c3f0ec2c9f904eb8', 'a:1:{s:10:\"id_feature\";a:1:{i:21;s:4:\"7_21\";}}', 1),
(32, '0b2aaba61c30885d941453599e20f408', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:1;s:3:\"1_1\";}}', 1),
(33, '656d3b6e07efc838f64977a0a4ce7533', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:2;s:3:\"1_2\";}}', 1),
(34, '2b4f155ba70596a04b22182d9d6fb462', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:3;s:3:\"1_3\";}}', 1),
(35, 'b329c7e7fea645456048d3482ededb40', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:4;s:3:\"1_4\";}}', 1),
(36, '6016e3155ed97b7d0a951061c2b73535', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:5;s:3:\"3_5\";}}', 1),
(37, '57b75870d64c76eae692c80b50fa58e2', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:6;s:3:\"3_6\";}}', 1),
(38, 'b2bf64fa7f72f53eb93e7805555e3cd2', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:7;s:3:\"3_7\";}}', 1),
(39, '2819c36f4dbaaedc9b2b35b42097edaa', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:8;s:3:\"3_8\";}}', 1),
(40, '36cbb04f09f486c4c527921d9b43bd95', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:9;s:3:\"3_9\";}}', 1),
(41, 'fa8f6104126208864be7953faa622383', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:10;s:4:\"3_10\";}}', 1),
(42, '4f6be0e4cd0cfb9226e1b3edf90e17da', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:11;s:4:\"3_11\";}}', 1),
(43, '27efddb9fdf432904fcbad936620a2c8', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:12;s:4:\"3_12\";}}', 1),
(44, '74ed66030793dee307982ace222b4726', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:13;s:4:\"3_13\";}}', 1),
(45, 'e3e573cccebcd56436d674887f5ecdf7', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:14;s:4:\"3_14\";}}', 1),
(46, '78ab5a36f2160733f009dfba8c2c4b02', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:15;s:4:\"3_15\";}}', 1),
(47, 'c9675dd50f5bce7a8aab98e299befe14', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:16;s:4:\"3_16\";}}', 1),
(48, '03012ad1764f10e703d360ca9185b337', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:17;s:4:\"3_17\";}}', 1),
(49, '2b92704d5b1a18938fbda7b201c2d94a', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:24;s:4:\"3_24\";}}', 1),
(50, 'c8bab8565a8dc38e4ba196d04b1ab75a', 'a:1:{s:8:\"quantity\";a:1:{i:0;i:0;}}', 1),
(51, 'aecc91ef502878d2b704377d49f7e8fb', 'a:1:{s:8:\"quantity\";a:1:{i:0;i:1;}}', 1),
(52, '47a6f6a9db83de31a52dc9366236ac18', 'a:1:{s:9:\"condition\";a:1:{s:3:\"new\";s:3:\"new\";}}', 1),
(53, 'e67a9c5323b69dd8689bd121632c9f0d', 'a:1:{s:9:\"condition\";a:1:{s:4:\"used\";s:4:\"used\";}}', 1),
(54, '843057e2c505cc5164e3cb9729cad768', 'a:1:{s:9:\"condition\";a:1:{s:11:\"refurbished\";s:11:\"refurbished\";}}', 1),
(55, 'aa4848c11a9cd606b68ee0a7d3be1097', 'a:1:{s:12:\"manufacturer\";a:1:{i:1;s:1:\"1\";}}', 1),
(56, '03c8c4cf29ea8a405778f138021df5df', 'a:1:{s:8:\"category\";a:1:{i:7;s:1:\"7\";}}', 2),
(57, '2def08957abfc829e80d5279c5086b73', 'a:1:{s:8:\"category\";a:1:{i:9;s:1:\"9\";}}', 2),
(58, 'eaaa28d2b62b097bb8706dd014c8203b', 'a:1:{s:8:\"category\";a:1:{i:8;s:1:\"8\";}}', 2),
(59, '84ce4d36b2b77bb85d2a7aebd27c8a67', 'a:1:{s:8:\"category\";a:1:{i:10;s:2:\"10\";}}', 2),
(60, 'e22ad4e9f8f445df1283ec3383c55ed8', 'a:1:{s:8:\"category\";a:1:{i:2;s:1:\"2\";}}', 2),
(61, '3f1005f8be7881795fc5feddfdba756f', 'a:1:{s:8:\"category\";a:1:{i:1;s:1:\"1\";}}', 2),
(62, '3f9036e3dcf0507782e3d6a1d3ca1fe1', 'a:1:{s:8:\"category\";a:1:{i:11;s:2:\"11\";}}', 2),
(63, '6fc253242f3fe98946ecdd26762e95eb', 'a:1:{s:8:\"category\";a:1:{i:5;s:1:\"5\";}}', 2),
(64, 'c66ef06ef2ca8b06dd3d19b70727adb7', 'a:1:{s:8:\"category\";a:1:{i:4;s:1:\"4\";}}', 2),
(65, '929674e49248753da273092629bb45ec', 'a:1:{s:8:\"category\";a:1:{i:3;s:1:\"3\";}}', 2),
(66, 'c4d7335317f2f1ba381e038fb625d918', 'a:1:{s:10:\"id_feature\";a:1:{i:1;s:3:\"5_1\";}}', 2),
(67, '18f41c9cab1c150e429f1b670cae3bc1', 'a:1:{s:10:\"id_feature\";a:1:{i:2;s:3:\"5_2\";}}', 2),
(68, '823192a052e44927f06b39b32bcef002', 'a:1:{s:10:\"id_feature\";a:1:{i:3;s:3:\"5_3\";}}', 2),
(69, '905fe5b57eb2e1353911171da4ee7706', 'a:1:{s:10:\"id_feature\";a:1:{i:4;s:3:\"5_4\";}}', 2),
(70, 'ebb42f1bbf0d25b40049c14f1860b952', 'a:1:{s:10:\"id_feature\";a:1:{i:5;s:3:\"5_5\";}}', 2),
(71, 'f9a71edd8befbb99baceadc2b2fbe793', 'a:1:{s:10:\"id_feature\";a:1:{i:6;s:3:\"5_6\";}}', 2),
(72, 'e195459fb3d97a32e94673db75dcf299', 'a:1:{s:10:\"id_feature\";a:1:{i:7;s:3:\"5_7\";}}', 2),
(73, 'b7783cae5eeefc81ff4a69f4ea712ea7', 'a:1:{s:10:\"id_feature\";a:1:{i:8;s:3:\"5_8\";}}', 2),
(74, '45f1d9162a9fe2ffcf9f365eace9eeec', 'a:1:{s:10:\"id_feature\";a:1:{i:9;s:3:\"5_9\";}}', 2),
(75, '7a04872959f09781f3b883a91c5332c7', 'a:1:{s:10:\"id_feature\";a:1:{i:10;s:4:\"6_10\";}}', 2),
(76, '025d11eb379709c8e409a7d712d8e362', 'a:1:{s:10:\"id_feature\";a:1:{i:11;s:4:\"6_11\";}}', 2),
(77, 'e224c427b75f7805c14e8b63ca9e4e0c', 'a:1:{s:10:\"id_feature\";a:1:{i:12;s:4:\"6_12\";}}', 2),
(78, '677717092975926de02151dd9227864e', 'a:1:{s:10:\"id_feature\";a:1:{i:13;s:4:\"6_13\";}}', 2),
(79, '00dff7b63b6f7ddb4b341a9308422730', 'a:1:{s:10:\"id_feature\";a:1:{i:14;s:4:\"6_14\";}}', 2),
(80, 'ff721a9727728b15cd4654a462aaeea0', 'a:1:{s:10:\"id_feature\";a:1:{i:15;s:4:\"6_15\";}}', 2),
(81, '0327a5c6fbcd99ae1fa8ef01f1e7e100', 'a:1:{s:10:\"id_feature\";a:1:{i:16;s:4:\"6_16\";}}', 2),
(82, '58ddd7a988c042c25121ffeb149f3ac7', 'a:1:{s:10:\"id_feature\";a:1:{i:17;s:4:\"7_17\";}}', 2),
(83, 'b7248af6c62c1e54b6f13739739e2d17', 'a:1:{s:10:\"id_feature\";a:1:{i:18;s:4:\"7_18\";}}', 2),
(84, 'b97d201e9d169f46c2a9e6fa356e40d2', 'a:1:{s:10:\"id_feature\";a:1:{i:19;s:4:\"7_19\";}}', 2),
(85, 'de50b73f078d5cde7cc9d8efc61c9e55', 'a:1:{s:10:\"id_feature\";a:1:{i:20;s:4:\"7_20\";}}', 2),
(86, '85a3c64761151fe72e5d027e729072a3', 'a:1:{s:10:\"id_feature\";a:1:{i:21;s:4:\"7_21\";}}', 2),
(87, '97d9dd08827238b39342d37e16ee7fc3', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:1;s:3:\"1_1\";}}', 2),
(88, '2f3d5048a6335cac20241e0f8cb5294e', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:2;s:3:\"1_2\";}}', 2),
(89, '19819345209f29bb2865355fa2cdb800', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:3;s:3:\"1_3\";}}', 2),
(90, '27dd5799da96500f9e0ab61387a556b5', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:4;s:3:\"1_4\";}}', 2),
(91, '6a73ce72468db97129f092fa3d9a0b2e', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:5;s:3:\"3_5\";}}', 2),
(92, 'f1fc935c7d64dfac606eb814dcc6c4a7', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:6;s:3:\"3_6\";}}', 2),
(93, 'f036e061c6e0e9cd6b3c463f72f524a5', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:7;s:3:\"3_7\";}}', 2),
(94, '468a278b79ece55c0ed0d3bd1b2dd01f', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:8;s:3:\"3_8\";}}', 2),
(95, '8996dbd99c9d2240f117ba0d26b39b10', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:9;s:3:\"3_9\";}}', 2),
(96, '601a4dd13077730810f102b18680b537', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:10;s:4:\"3_10\";}}', 2),
(97, '0a68b3ba0819d7126935f51335ef9503', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:11;s:4:\"3_11\";}}', 2),
(98, '5f556205d67d7c26c2726dba638c2d95', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:12;s:4:\"3_12\";}}', 2),
(99, '4b4bb79b20455e8047c972f9ca69cd72', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:13;s:4:\"3_13\";}}', 2),
(100, '54dd539ce8bbf02b44485941f2d8d80b', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:14;s:4:\"3_14\";}}', 2),
(101, '73b845a28e9ced9709fa414f9b97dae9', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:15;s:4:\"3_15\";}}', 2),
(102, 'be50cfae4c360fdb124af017a4e80905', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:16;s:4:\"3_16\";}}', 2),
(103, '4c4550abfc4eec4c91e558fa9b5171c9', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:17;s:4:\"3_17\";}}', 2),
(104, 'ab223cc0ca7ebf34af71e067556ee2aa', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:24;s:4:\"3_24\";}}', 2),
(105, '14ef3952eddf958ec1f628065f6c7689', 'a:1:{s:8:\"quantity\";a:1:{i:0;i:0;}}', 2),
(106, '19e5bdea58716c8f3ff52345d1b5a442', 'a:1:{s:8:\"quantity\";a:1:{i:0;i:1;}}', 2),
(107, '11c2881845b925423888cd329d0c4953', 'a:1:{s:9:\"condition\";a:1:{s:3:\"new\";s:3:\"new\";}}', 2),
(108, '074755ccbf623ca666bd866203d0dec7', 'a:1:{s:9:\"condition\";a:1:{s:4:\"used\";s:4:\"used\";}}', 2),
(109, '70b63b881a45f66c86ea78ace4cfb6a7', 'a:1:{s:9:\"condition\";a:1:{s:11:\"refurbished\";s:11:\"refurbished\";}}', 2),
(110, '7b51d2594a28b8f82cfe82b0c3f161e7', 'a:1:{s:12:\"manufacturer\";a:1:{i:1;s:1:\"1\";}}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_indexable_attribute_group`
--

CREATE TABLE `ap_layered_indexable_attribute_group` (
  `id_attribute_group` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_indexable_attribute_group`
--

INSERT INTO `ap_layered_indexable_attribute_group` (`id_attribute_group`, `indexable`) VALUES
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_indexable_attribute_group_lang_value`
--

CREATE TABLE `ap_layered_indexable_attribute_group_lang_value` (
  `id_attribute_group` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_indexable_attribute_group_lang_value`
--

INSERT INTO `ap_layered_indexable_attribute_group_lang_value` (`id_attribute_group`, `id_lang`, `url_name`, `meta_title`) VALUES
(4, 1, 'kilometrage', ''),
(5, 1, 'supplement', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_indexable_attribute_lang_value`
--

CREATE TABLE `ap_layered_indexable_attribute_lang_value` (
  `id_attribute` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_indexable_attribute_lang_value`
--

INSERT INTO `ap_layered_indexable_attribute_lang_value` (`id_attribute`, `id_lang`, `url_name`, `meta_title`) VALUES
(25, 1, 'kilometrage-illimite', ''),
(26, 1, 'kilometrage-limite', ''),
(27, 1, 'semaine', ''),
(28, 1, 'weekend', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_indexable_feature`
--

CREATE TABLE `ap_layered_indexable_feature` (
  `id_feature` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_indexable_feature`
--

INSERT INTO `ap_layered_indexable_feature` (`id_feature`, `indexable`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_indexable_feature_lang_value`
--

CREATE TABLE `ap_layered_indexable_feature_lang_value` (
  `id_feature` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_indexable_feature_value_lang_value`
--

CREATE TABLE `ap_layered_indexable_feature_value_lang_value` (
  `id_feature_value` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_price_index`
--

CREATE TABLE `ap_layered_price_index` (
  `id_product` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `price_min` int(11) NOT NULL,
  `price_max` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_price_index`
--

INSERT INTO `ap_layered_price_index` (`id_product`, `id_currency`, `id_shop`, `price_min`, `price_max`) VALUES
(1, 1, 1, 16, 17),
(1, 2, 1, 18, 19),
(2, 1, 1, 26, 27),
(2, 2, 1, 30, 30),
(3, 1, 1, 25, 26),
(3, 2, 1, 29, 29),
(4, 1, 1, 50, 51),
(4, 2, 1, 57, 58),
(5, 1, 1, 28, 29),
(5, 2, 1, 32, 33),
(6, 1, 1, 30, 31),
(6, 2, 1, 34, 34),
(7, 1, 1, 16, 16),
(7, 2, 1, 18, 18),
(8, 1, 1, 22, 24),
(8, 2, 1, 24, 27),
(9, 1, 1, 91, 99),
(10, 1, 1, 459, 499),
(11, 1, 1, 247, 269),
(12, 1, 1, 137, 149),
(13, 1, 1, 589, 640),
(14, 1, 1, 451, 490),
(14, 2, 1, 509, 553),
(15, 1, 1, 423, 459),
(16, 1, 1, 312, 339),
(17, 1, 1, 155, 169),
(18, 1, 1, 91, 99),
(19, 1, 1, 20, 22),
(20, 1, 1, 525, 570),
(21, 1, 1, 92, 100),
(22, 1, 1, 101, 111),
(23, 1, 1, 137, 149),
(24, 1, 1, 247, 269),
(25, 1, 1, 496, 539);

-- --------------------------------------------------------

--
-- Table structure for table `ap_layered_product_attribute`
--

CREATE TABLE `ap_layered_product_attribute` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_attribute_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_layered_product_attribute`
--

INSERT INTO `ap_layered_product_attribute` (`id_attribute`, `id_product`, `id_attribute_group`, `id_shop`) VALUES
(1, 1, 1, 1),
(1, 2, 1, 1),
(1, 3, 1, 1),
(1, 4, 1, 1),
(1, 5, 1, 1),
(1, 6, 1, 1),
(1, 7, 1, 1),
(2, 1, 1, 1),
(2, 2, 1, 1),
(2, 3, 1, 1),
(2, 4, 1, 1),
(2, 5, 1, 1),
(2, 6, 1, 1),
(2, 7, 1, 1),
(3, 1, 1, 1),
(3, 2, 1, 1),
(3, 3, 1, 1),
(3, 4, 1, 1),
(3, 5, 1, 1),
(3, 6, 1, 1),
(3, 7, 1, 1),
(7, 4, 3, 1),
(8, 2, 3, 1),
(8, 6, 3, 1),
(11, 2, 3, 1),
(11, 5, 3, 1),
(13, 1, 3, 1),
(13, 3, 3, 1),
(13, 5, 3, 1),
(14, 1, 3, 1),
(14, 5, 3, 1),
(15, 7, 3, 1),
(16, 5, 3, 1),
(16, 6, 3, 1),
(16, 7, 3, 1),
(24, 4, 3, 1),
(25, 13, 4, 1),
(25, 15, 4, 1),
(25, 16, 4, 1),
(25, 20, 4, 1),
(25, 22, 4, 1),
(25, 23, 4, 1),
(25, 24, 4, 1),
(25, 25, 4, 1),
(26, 13, 4, 1),
(26, 15, 4, 1),
(26, 16, 4, 1),
(26, 20, 4, 1),
(26, 22, 4, 1),
(26, 23, 4, 1),
(26, 24, 4, 1),
(26, 25, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_linksmenutop`
--

CREATE TABLE `ap_linksmenutop` (
  `id_linksmenutop` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `new_window` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_linksmenutop_lang`
--

CREATE TABLE `ap_linksmenutop_lang` (
  `id_linksmenutop` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `label` varchar(128) NOT NULL,
  `link` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_log`
--

CREATE TABLE `ap_log` (
  `id_log` int(10) UNSIGNED NOT NULL,
  `severity` tinyint(1) NOT NULL,
  `error_code` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `object_type` varchar(32) DEFAULT NULL,
  `object_id` int(10) UNSIGNED DEFAULT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_log`
--

INSERT INTO `ap_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_employee`, `date_add`, `date_upd`) VALUES
(1, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-08 02:36:59', '2019-06-08 02:36:59'),
(2, 1, 0, 'Création : Product', 'Product', 8, 1, '2019-06-08 03:00:33', '2019-06-08 03:00:33'),
(3, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 03:01:15', '2019-06-08 03:01:15'),
(4, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 03:01:16', '2019-06-08 03:01:16'),
(5, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 03:02:25', '2019-06-08 03:02:25'),
(6, 1, 0, 'Suppression : Category', 'Category', 3, 1, '2019-06-08 03:08:12', '2019-06-08 03:08:12'),
(7, 1, 0, 'Création : Category', 'Category', 12, 1, '2019-06-08 03:08:35', '2019-06-08 03:08:35'),
(8, 1, 0, 'Création : CMSCategory', 'CMSCategory', 2, 1, '2019-06-08 03:10:50', '2019-06-08 03:10:50'),
(9, 1, 0, 'modification CMS', 'CMS', 6, 1, '2019-06-08 03:12:20', '2019-06-08 03:12:20'),
(10, 1, 0, 'Création : Tab', 'Tab', 107, 1, '2019-06-08 03:42:44', '2019-06-08 03:42:44'),
(11, 1, 0, 'modification Tab', 'Tab', 107, 1, '2019-06-08 03:43:25', '2019-06-08 03:43:25'),
(12, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 05:48:04', '2019-06-08 05:48:04'),
(13, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-08 11:28:17', '2019-06-08 11:28:17'),
(14, 1, 0, 'Suppression : Product', 'Product', 1, 1, '2019-06-08 11:29:02', '2019-06-08 11:29:02'),
(15, 1, 0, 'Suppression : Product', 'Product', 2, 1, '2019-06-08 11:29:02', '2019-06-08 11:29:02'),
(16, 1, 0, 'Suppression : Product', 'Product', 3, 1, '2019-06-08 11:29:02', '2019-06-08 11:29:02'),
(17, 1, 0, 'Suppression : Product', 'Product', 4, 1, '2019-06-08 11:29:02', '2019-06-08 11:29:02'),
(18, 1, 0, 'Suppression : Product', 'Product', 5, 1, '2019-06-08 11:29:03', '2019-06-08 11:29:03'),
(19, 1, 0, 'Suppression : Product', 'Product', 6, 1, '2019-06-08 11:29:03', '2019-06-08 11:29:03'),
(20, 1, 0, 'Suppression : Product', 'Product', 7, 1, '2019-06-08 11:29:03', '2019-06-08 11:29:03'),
(21, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-08 18:14:17', '2019-06-08 18:14:17'),
(22, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 18:24:41', '2019-06-08 18:24:41'),
(23, 1, 0, 'Import : Produits', '', 0, 1, '2019-06-08 18:28:17', '2019-06-08 18:28:17'),
(24, 1, 0, 'Import : Produits', '', 0, 1, '2019-06-08 18:29:49', '2019-06-08 18:29:49'),
(25, 1, 0, 'modification Product', 'Product', 9, 1, '2019-06-08 18:30:26', '2019-06-08 18:30:26'),
(26, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 18:57:56', '2019-06-08 18:57:56'),
(27, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 18:58:15', '2019-06-08 18:58:15'),
(28, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 19:06:21', '2019-06-08 19:06:21'),
(29, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 19:13:26', '2019-06-08 19:13:26'),
(30, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 19:13:35', '2019-06-08 19:13:35'),
(31, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-08 19:31:37', '2019-06-08 19:31:37'),
(32, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 19:31:51', '2019-06-08 19:31:51'),
(33, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 19:32:09', '2019-06-08 19:32:09'),
(34, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-08 20:54:42', '2019-06-08 20:54:42'),
(35, 1, 0, 'Création : Employee', 'Employee', 2, 1, '2019-06-08 20:55:57', '2019-06-08 20:55:57'),
(36, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-08 20:56:21', '2019-06-08 20:56:21'),
(37, 1, 0, 'modification Employee', 'Employee', 2, 1, '2019-06-08 20:59:10', '2019-06-08 20:59:10'),
(38, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-08 20:59:28', '2019-06-08 20:59:28'),
(39, 1, 0, 'modification Employee', 'Employee', 2, 1, '2019-06-08 20:59:38', '2019-06-08 20:59:38'),
(40, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 2, '2019-06-08 21:00:06', '2019-06-08 21:00:06'),
(41, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-08 21:06:47', '2019-06-08 21:06:47'),
(42, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 21:12:46', '2019-06-08 21:12:46'),
(43, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 21:13:34', '2019-06-08 21:13:34'),
(44, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 21:16:00', '2019-06-08 21:16:00'),
(45, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 21:17:36', '2019-06-08 21:17:36'),
(46, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-08 21:36:18', '2019-06-08 21:36:18'),
(47, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-09 14:58:51', '2019-06-09 14:58:51'),
(48, 1, 0, 'modification Product', 'Product', 16, 1, '2019-06-09 15:01:52', '2019-06-09 15:01:52'),
(49, 1, 0, 'modification Product', 'Product', 16, 1, '2019-06-09 15:04:00', '2019-06-09 15:04:00'),
(50, 1, 0, 'modification Category', 'Category', 12, 1, '2019-06-09 15:06:25', '2019-06-09 15:06:25'),
(51, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-09 15:07:02', '2019-06-09 15:07:02'),
(52, 1, 0, 'modification Product', 'Product', 19, 1, '2019-06-09 15:09:57', '2019-06-09 15:09:57'),
(53, 1, 0, 'Création : Attachment', 'Attachment', 1, 1, '2019-06-09 15:12:20', '2019-06-09 15:12:20'),
(54, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-09 15:53:22', '2019-06-09 15:53:22'),
(55, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-09 16:31:12', '2019-06-09 16:31:12'),
(56, 1, 0, 'Création : Tab', 'Tab', 108, 1, '2019-06-09 16:31:46', '2019-06-09 16:31:46'),
(57, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-09 18:18:59', '2019-06-09 18:18:59'),
(58, 1, 0, 'Création : Tab', 'Tab', 109, 1, '2019-06-09 18:19:51', '2019-06-09 18:19:51'),
(59, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-09 20:41:04', '2019-06-09 20:41:04'),
(60, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-09 21:59:53', '2019-06-09 21:59:53'),
(61, 1, 0, 'Création : QuickAccess', 'QuickAccess', 4, 2, '2019-06-09 22:25:11', '2019-06-09 22:25:11'),
(62, 1, 0, 'modification Tab', 'Tab', 107, 1, '2019-06-09 22:44:41', '2019-06-09 22:44:41'),
(63, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-09 22:56:35', '2019-06-09 22:56:35'),
(64, 1, 0, 'modification CMS', 'CMS', 7, 1, '2019-06-09 23:05:24', '2019-06-09 23:05:24'),
(65, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-09 23:34:05', '2019-06-09 23:34:05'),
(66, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 00:39:08', '2019-06-10 00:39:08'),
(67, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 00:59:09', '2019-06-10 00:59:09'),
(68, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 02:10:33', '2019-06-10 02:10:33'),
(69, 1, 0, 'modification Product', 'Product', 10, 1, '2019-06-10 04:35:27', '2019-06-10 04:35:27'),
(70, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 04:55:48', '2019-06-10 04:55:48'),
(71, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 04:58:19', '2019-06-10 04:58:19'),
(72, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 04:58:35', '2019-06-10 04:58:35'),
(73, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 04:58:56', '2019-06-10 04:58:56'),
(74, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 04:59:15', '2019-06-10 04:59:15'),
(75, 1, 0, 'Connexion au back-office depuis 81.185.166.70', '', 0, 1, '2019-06-10 04:59:54', '2019-06-10 04:59:54'),
(76, 1, 0, 'Connexion au back-office depuis 81.185.166.70', '', 0, 1, '2019-06-10 05:01:34', '2019-06-10 05:01:34'),
(77, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-10 11:43:24', '2019-06-10 11:43:24'),
(78, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 11:49:24', '2019-06-10 11:49:24'),
(79, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 13:28:24', '2019-06-10 13:28:24'),
(80, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-10 14:45:45', '2019-06-10 14:45:45'),
(81, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-10 15:06:44', '2019-06-10 15:06:44'),
(82, 1, 0, 'modification Store', 'Store', 1, 1, '2019-06-10 15:10:36', '2019-06-10 15:10:36'),
(83, 1, 0, 'modification Store', 'Store', 1, 1, '2019-06-10 15:10:51', '2019-06-10 15:10:51'),
(84, 1, 0, 'Création : TaxRulesGroup', 'TaxRulesGroup', 1, 1, '2019-06-10 15:15:33', '2019-06-10 15:15:33'),
(85, 1, 0, 'modification Product', 'Product', 8, 1, '2019-06-10 15:18:40', '2019-06-10 15:18:40'),
(86, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-10 15:25:51', '2019-06-10 15:25:51'),
(87, 1, 0, 'modification Product', 'Product', 8, 2, '2019-06-10 15:41:25', '2019-06-10 15:41:25'),
(88, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 15:44:50', '2019-06-10 15:44:50'),
(89, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-10 16:12:03', '2019-06-10 16:12:03'),
(90, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-10 16:48:51', '2019-06-10 16:48:51'),
(91, 1, 0, 'modification Product', 'Product', 8, 2, '2019-06-10 16:51:28', '2019-06-10 16:51:28'),
(92, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-10 16:51:55', '2019-06-10 16:51:55'),
(93, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-10 16:53:12', '2019-06-10 16:53:12'),
(94, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-10 16:54:45', '2019-06-10 16:54:45'),
(95, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-10 16:56:55', '2019-06-10 16:56:55'),
(96, 1, 0, 'Suppression : QuickAccess', 'QuickAccess', 3, 1, '2019-06-10 16:57:01', '2019-06-10 16:57:01'),
(97, 1, 0, 'Suppression : QuickAccess', 'QuickAccess', 2, 1, '2019-06-10 16:57:07', '2019-06-10 16:57:07'),
(98, 1, 0, 'Suppression : QuickAccess', 'QuickAccess', 1, 1, '2019-06-10 16:57:12', '2019-06-10 16:57:12'),
(99, 1, 0, 'Création : QuickAccess', 'QuickAccess', 5, 1, '2019-06-10 16:57:24', '2019-06-10 16:57:24'),
(100, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-10 16:57:27', '2019-06-10 16:57:27'),
(101, 1, 0, 'Création : QuickAccess', 'QuickAccess', 6, 1, '2019-06-10 16:57:34', '2019-06-10 16:57:34'),
(102, 1, 0, 'Création : QuickAccess', 'QuickAccess', 7, 1, '2019-06-10 16:57:45', '2019-06-10 16:57:45'),
(103, 1, 0, 'Création : QuickAccess', 'QuickAccess', 8, 1, '2019-06-10 16:58:24', '2019-06-10 16:58:24'),
(104, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-10 16:58:24', '2019-06-10 16:58:24'),
(105, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-10 16:59:16', '2019-06-10 16:59:16'),
(106, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-10 16:59:35', '2019-06-10 16:59:35'),
(107, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-10 17:00:09', '2019-06-10 17:00:09'),
(108, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-10 17:00:30', '2019-06-10 17:00:30'),
(109, 1, 0, 'modification Product', 'Product', 14, 2, '2019-06-10 17:01:20', '2019-06-10 17:01:20'),
(110, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-10 17:01:58', '2019-06-10 17:01:58'),
(111, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-10 17:02:39', '2019-06-10 17:02:39'),
(112, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-10 17:03:09', '2019-06-10 17:03:09'),
(113, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-10 17:03:34', '2019-06-10 17:03:34'),
(114, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-10 17:03:35', '2019-06-10 17:03:35'),
(115, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-10 17:03:59', '2019-06-10 17:03:59'),
(116, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-10 17:04:22', '2019-06-10 17:04:22'),
(117, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-10 17:04:37', '2019-06-10 17:04:37'),
(118, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-10 17:04:48', '2019-06-10 17:04:48'),
(119, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-10 17:05:04', '2019-06-10 17:05:04'),
(120, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-10 17:17:58', '2019-06-10 17:17:58'),
(121, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-10 17:20:25', '2019-06-10 17:20:25'),
(122, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-10 17:21:18', '2019-06-10 17:21:18'),
(123, 1, 0, 'Suppression : Language', 'Language', 2, 1, '2019-06-10 17:22:02', '2019-06-10 17:22:02'),
(124, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-10 17:22:05', '2019-06-10 17:22:05'),
(125, 1, 0, 'modification Product', 'Product', 14, 2, '2019-06-10 17:22:41', '2019-06-10 17:22:41'),
(126, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-10 17:23:09', '2019-06-10 17:23:09'),
(127, 1, 0, 'modification Product', 'Product', 8, 2, '2019-06-10 17:23:49', '2019-06-10 17:23:49'),
(128, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-10 17:24:23', '2019-06-10 17:24:23'),
(129, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-10 17:24:56', '2019-06-10 17:24:56'),
(130, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-10 17:25:36', '2019-06-10 17:25:36'),
(131, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-10 17:26:23', '2019-06-10 17:26:23'),
(132, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-10 17:27:03', '2019-06-10 17:27:03'),
(133, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-10 17:27:44', '2019-06-10 17:27:44'),
(134, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 18:39:32', '2019-06-10 18:39:32'),
(135, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 19:10:27', '2019-06-10 19:10:27'),
(136, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 20:16:58', '2019-06-10 20:16:58'),
(137, 1, 0, 'modification Product', 'Product', 9, 1, '2019-06-10 20:17:41', '2019-06-10 20:17:41'),
(138, 1, 0, 'modification Product', 'Product', 10, 1, '2019-06-10 20:18:02', '2019-06-10 20:18:02'),
(139, 1, 0, 'modification Product', 'Product', 11, 1, '2019-06-10 20:30:13', '2019-06-10 20:30:13'),
(140, 1, 0, 'modification Product', 'Product', 12, 1, '2019-06-10 20:30:26', '2019-06-10 20:30:26'),
(141, 1, 0, 'modification Product', 'Product', 13, 1, '2019-06-10 20:30:41', '2019-06-10 20:30:41'),
(142, 1, 0, 'modification Product', 'Product', 14, 1, '2019-06-10 20:31:04', '2019-06-10 20:31:04'),
(143, 1, 0, 'modification Product', 'Product', 15, 1, '2019-06-10 20:31:21', '2019-06-10 20:31:21'),
(144, 1, 0, 'modification Product', 'Product', 16, 1, '2019-06-10 20:31:34', '2019-06-10 20:31:34'),
(145, 1, 0, 'modification Product', 'Product', 17, 1, '2019-06-10 20:31:50', '2019-06-10 20:31:50'),
(146, 1, 0, 'modification Product', 'Product', 18, 1, '2019-06-10 20:32:05', '2019-06-10 20:32:05'),
(147, 1, 0, 'modification Product', 'Product', 16, 1, '2019-06-10 20:35:09', '2019-06-10 20:35:09'),
(148, 1, 0, 'modification Product', 'Product', 10, 1, '2019-06-10 20:35:21', '2019-06-10 20:35:21'),
(149, 1, 0, 'modification Product', 'Product', 9, 1, '2019-06-10 20:35:35', '2019-06-10 20:35:35'),
(150, 1, 0, 'modification Product', 'Product', 19, 1, '2019-06-10 20:35:55', '2019-06-10 20:35:55'),
(151, 1, 0, 'Suppression : Product', 'Product', 8, 1, '2019-06-10 20:36:03', '2019-06-10 20:36:03'),
(152, 1, 0, 'modification Product', 'Product', 15, 1, '2019-06-10 20:36:23', '2019-06-10 20:36:23'),
(153, 1, 0, 'modification Product', 'Product', 10, 1, '2019-06-10 20:36:36', '2019-06-10 20:36:36'),
(154, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-10 21:30:59', '2019-06-10 21:30:59'),
(155, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 21:36:03', '2019-06-10 21:36:03'),
(156, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-10 21:44:31', '2019-06-10 21:44:31'),
(157, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-10 22:02:03', '2019-06-10 22:02:03'),
(158, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-10 22:04:14', '2019-06-10 22:04:14'),
(159, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-10 22:05:36', '2019-06-10 22:05:36'),
(160, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-10 22:16:25', '2019-06-10 22:16:25'),
(161, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-10 22:17:25', '2019-06-10 22:17:25'),
(162, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-10 22:23:54', '2019-06-10 22:23:54'),
(163, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-10 22:24:21', '2019-06-10 22:24:21'),
(164, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-10 22:31:04', '2019-06-10 22:31:04'),
(165, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-10 22:35:04', '2019-06-10 22:35:04'),
(166, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-10 22:38:05', '2019-06-10 22:38:05'),
(167, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-10 22:39:50', '2019-06-10 22:39:50'),
(168, 1, 0, 'modification Product', 'Product', 14, 2, '2019-06-10 22:40:19', '2019-06-10 22:40:19'),
(169, 1, 0, 'modification Product', 'Product', 14, 2, '2019-06-10 22:44:03', '2019-06-10 22:44:03'),
(170, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-10 22:44:28', '2019-06-10 22:44:28'),
(171, 1, 0, 'modification Product', 'Product', 14, 2, '2019-06-10 22:45:15', '2019-06-10 22:45:15'),
(172, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-10 22:45:42', '2019-06-10 22:45:42'),
(173, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-10 22:49:27', '2019-06-10 22:49:27'),
(174, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-10 22:51:11', '2019-06-10 22:51:11'),
(175, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-10 22:53:00', '2019-06-10 22:53:00'),
(176, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-10 22:53:32', '2019-06-10 22:53:32'),
(177, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-10 22:55:40', '2019-06-10 22:55:40'),
(178, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-10 22:55:40', '2019-06-10 22:55:40'),
(179, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-10 22:56:05', '2019-06-10 22:56:05'),
(180, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-10 22:58:34', '2019-06-10 22:58:34'),
(181, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-10 23:00:29', '2019-06-10 23:00:29'),
(182, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-10 23:00:58', '2019-06-10 23:00:58'),
(183, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-10 23:06:21', '2019-06-10 23:06:21'),
(184, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-10 23:07:01', '2019-06-10 23:07:01'),
(185, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-10 23:07:30', '2019-06-10 23:07:30'),
(186, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-10 23:13:43', '2019-06-10 23:13:43'),
(187, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-10 23:14:58', '2019-06-10 23:14:58'),
(188, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-10 23:15:28', '2019-06-10 23:15:28'),
(189, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-10 23:17:18', '2019-06-10 23:17:18'),
(190, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 23:28:00', '2019-06-10 23:28:00'),
(191, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-10 23:57:45', '2019-06-10 23:57:45'),
(192, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-11 00:08:28', '2019-06-11 00:08:28'),
(193, 1, 0, 'Suppression : Customer', 'Customer', 5, 1, '2019-06-11 00:26:33', '2019-06-11 00:26:33'),
(194, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-11 01:30:09', '2019-06-11 01:30:09'),
(195, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-11 08:18:33', '2019-06-11 08:18:33'),
(196, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-11 08:21:47', '2019-06-11 08:21:47'),
(197, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-11 13:36:11', '2019-06-11 13:36:11'),
(198, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-11 14:29:56', '2019-06-11 14:29:56'),
(199, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-11 14:39:18', '2019-06-11 14:39:18'),
(200, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-11 14:44:57', '2019-06-11 14:44:57'),
(201, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-11 14:48:37', '2019-06-11 14:48:37'),
(202, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-11 14:50:01', '2019-06-11 14:50:01'),
(203, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-11 14:51:32', '2019-06-11 14:51:32'),
(204, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-11 14:54:08', '2019-06-11 14:54:08'),
(205, 1, 0, 'modification Product', 'Product', 14, 2, '2019-06-11 14:55:31', '2019-06-11 14:55:31'),
(206, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-11 14:56:21', '2019-06-11 14:56:21'),
(207, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-11 14:57:14', '2019-06-11 14:57:14'),
(208, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-11 14:57:34', '2019-06-11 14:57:34'),
(209, 1, 0, 'modification Product', 'Product', 14, 2, '2019-06-11 14:57:54', '2019-06-11 14:57:54'),
(210, 1, 0, 'modification Product', 'Product', 14, 2, '2019-06-11 14:58:18', '2019-06-11 14:58:18'),
(211, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-11 14:58:38', '2019-06-11 14:58:38'),
(212, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-11 14:59:30', '2019-06-11 14:59:30'),
(213, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-11 15:00:21', '2019-06-11 15:00:21'),
(214, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-11 15:01:09', '2019-06-11 15:01:09'),
(215, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-11 15:44:36', '2019-06-11 15:44:36'),
(216, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-11 16:32:14', '2019-06-11 16:32:14'),
(217, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-11 17:00:42', '2019-06-11 17:00:42'),
(218, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-11 17:58:57', '2019-06-11 17:58:57'),
(219, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-12 00:15:46', '2019-06-12 00:15:46'),
(220, 1, 0, 'modification CMS', 'CMS', 12, 1, '2019-06-12 00:40:29', '2019-06-12 00:40:29'),
(221, 1, 0, 'modification CMS', 'CMS', 13, 1, '2019-06-12 00:44:17', '2019-06-12 00:44:17'),
(222, 1, 0, 'modification CMS', 'CMS', 12, 1, '2019-06-12 00:59:47', '2019-06-12 00:59:47'),
(223, 1, 0, 'Supplier status switched to disable', 'Supplier', 1, 1, '2019-06-12 01:35:50', '2019-06-12 01:35:50'),
(224, 1, 0, 'Manufacturer status switched to disable', 'Manufacturer', 1, 1, '2019-06-12 01:37:06', '2019-06-12 01:37:06'),
(225, 1, 0, 'Currency status switched to disable', 'Currency', 2, 1, '2019-06-12 01:38:37', '2019-06-12 01:38:37'),
(226, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 1, '2019-06-12 16:04:32', '2019-06-12 16:04:32'),
(227, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 1, '2019-06-12 16:05:19', '2019-06-12 16:05:19'),
(228, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-12 16:17:00', '2019-06-12 16:17:00'),
(229, 1, 0, 'modification CMS', 'CMS', 12, 2, '2019-06-12 16:23:14', '2019-06-12 16:23:14'),
(230, 1, 0, 'modification CMS', 'CMS', 12, 2, '2019-06-12 16:23:52', '2019-06-12 16:23:52'),
(231, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 1, '2019-06-12 16:29:52', '2019-06-12 16:29:52'),
(232, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-12 16:39:22', '2019-06-12 16:39:22'),
(233, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-12 16:41:44', '2019-06-12 16:41:44'),
(234, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-12 16:43:24', '2019-06-12 16:43:24'),
(235, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-12 16:44:58', '2019-06-12 16:44:58'),
(236, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-12 22:23:18', '2019-06-12 22:23:18'),
(237, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-12 22:52:19', '2019-06-12 22:52:19'),
(238, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-12 23:28:02', '2019-06-12 23:28:02'),
(239, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-12 23:51:24', '2019-06-12 23:51:24'),
(240, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 21, 0, '2019-06-12 23:55:17', '2019-06-12 23:55:17'),
(241, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 24, 0, '2019-06-12 23:56:13', '2019-06-12 23:56:13'),
(242, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 25, 0, '2019-06-13 00:02:27', '2019-06-13 00:02:27'),
(243, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 26, 0, '2019-06-13 00:06:27', '2019-06-13 00:06:27'),
(244, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 27, 0, '2019-06-13 00:37:56', '2019-06-13 00:37:56'),
(245, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 28, 0, '2019-06-13 00:39:10', '2019-06-13 00:39:10'),
(246, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 29, 0, '2019-06-13 00:40:19', '2019-06-13 00:40:19'),
(247, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 30, 0, '2019-06-13 00:42:02', '2019-06-13 00:42:02'),
(248, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 31, 0, '2019-06-13 00:43:53', '2019-06-13 00:43:53'),
(249, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 32, 0, '2019-06-13 00:45:30', '2019-06-13 00:45:30'),
(250, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 33, 0, '2019-06-13 00:47:44', '2019-06-13 00:47:44'),
(251, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 34, 0, '2019-06-13 00:49:33', '2019-06-13 00:49:33'),
(252, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 35, 0, '2019-06-13 00:51:45', '2019-06-13 00:51:45'),
(253, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 36, 0, '2019-06-13 00:56:12', '2019-06-13 00:56:12'),
(254, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 37, 0, '2019-06-13 00:57:36', '2019-06-13 00:57:36'),
(255, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 38, 0, '2019-06-13 01:02:09', '2019-06-13 01:02:09'),
(256, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 39, 0, '2019-06-13 01:03:52', '2019-06-13 01:03:52'),
(257, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 40, 0, '2019-06-13 01:05:42', '2019-06-13 01:05:42'),
(258, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 41, 0, '2019-06-13 01:07:18', '2019-06-13 01:07:18'),
(259, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 42, 0, '2019-06-13 01:08:39', '2019-06-13 01:08:39'),
(260, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 43, 0, '2019-06-13 01:11:34', '2019-06-13 01:11:34'),
(261, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 44, 0, '2019-06-13 01:12:14', '2019-06-13 01:12:14'),
(262, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 45, 0, '2019-06-13 01:12:57', '2019-06-13 01:12:57'),
(263, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 46, 0, '2019-06-13 01:17:37', '2019-06-13 01:17:37'),
(264, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 47, 0, '2019-06-13 01:18:59', '2019-06-13 01:18:59'),
(265, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 48, 0, '2019-06-13 01:21:50', '2019-06-13 01:21:50'),
(266, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 49, 0, '2019-06-13 01:22:58', '2019-06-13 01:22:58'),
(267, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 50, 0, '2019-06-13 01:24:32', '2019-06-13 01:24:32'),
(268, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 51, 0, '2019-06-13 01:25:19', '2019-06-13 01:25:19'),
(269, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 52, 0, '2019-06-13 01:26:30', '2019-06-13 01:26:30'),
(270, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 53, 0, '2019-06-13 01:27:22', '2019-06-13 01:27:22'),
(271, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 54, 0, '2019-06-13 01:29:07', '2019-06-13 01:29:07'),
(272, 4, 1, 'Cart cannot be loaded or an order has already been placed using this cart', '', 0, 0, '2019-06-13 01:29:07', '2019-06-13 01:29:07'),
(273, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 55, 0, '2019-06-13 01:30:37', '2019-06-13 01:30:37'),
(274, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 56, 0, '2019-06-13 01:34:22', '2019-06-13 01:34:22'),
(275, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 57, 0, '2019-06-13 01:36:41', '2019-06-13 01:36:41'),
(276, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 58, 0, '2019-06-13 01:38:09', '2019-06-13 01:38:09'),
(277, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 59, 0, '2019-06-13 01:39:48', '2019-06-13 01:39:48'),
(278, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 60, 0, '2019-06-13 01:40:57', '2019-06-13 01:40:57'),
(279, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 61, 0, '2019-06-13 01:41:55', '2019-06-13 01:41:55'),
(280, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 62, 0, '2019-06-13 01:43:32', '2019-06-13 01:43:32'),
(281, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 63, 0, '2019-06-13 01:46:46', '2019-06-13 01:46:46'),
(282, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 64, 0, '2019-06-13 01:47:35', '2019-06-13 01:47:35'),
(283, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 65, 0, '2019-06-13 18:27:27', '2019-06-13 18:27:27'),
(284, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-13 20:53:28', '2019-06-13 20:53:28'),
(285, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-13 22:13:17', '2019-06-13 22:13:17'),
(286, 1, 0, 'Création : QuickAccess', 'QuickAccess', 9, 1, '2019-06-13 22:16:53', '2019-06-13 22:16:53'),
(287, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-13 22:40:16', '2019-06-13 22:40:16'),
(288, 1, 0, 'modification CMS', 'CMS', 11, 2, '2019-06-13 22:43:41', '2019-06-13 22:43:41'),
(289, 1, 0, 'modification CMS', 'CMS', 11, 2, '2019-06-13 22:49:29', '2019-06-13 22:49:29'),
(290, 1, 0, 'modification CMS', 'CMS', 11, 2, '2019-06-13 22:51:14', '2019-06-13 22:51:14'),
(291, 1, 0, 'modification CMS', 'CMS', 13, 2, '2019-06-13 23:04:48', '2019-06-13 23:04:48'),
(292, 1, 0, 'modification CMS', 'CMS', 13, 2, '2019-06-13 23:05:50', '2019-06-13 23:05:50'),
(293, 1, 0, 'modification CMS', 'CMS', 13, 2, '2019-06-13 23:15:13', '2019-06-13 23:15:13'),
(294, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-14 22:22:20', '2019-06-14 22:22:20'),
(295, 1, 0, 'modification CMS', 'CMS', 14, 2, '2019-06-14 22:36:33', '2019-06-14 22:36:33'),
(296, 1, 0, 'modification CMS', 'CMS', 14, 2, '2019-06-14 22:37:50', '2019-06-14 22:37:50'),
(297, 1, 0, 'modification CMS', 'CMS', 14, 2, '2019-06-14 22:47:04', '2019-06-14 22:47:04'),
(298, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-15 10:10:54', '2019-06-15 10:10:54'),
(299, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-15 10:28:15', '2019-06-15 10:28:15'),
(300, 1, 0, 'modification Category', 'Category', 12, 2, '2019-06-15 10:56:19', '2019-06-15 10:56:19'),
(301, 1, 0, 'modification Category', 'Category', 12, 2, '2019-06-15 10:57:40', '2019-06-15 10:57:40'),
(302, 1, 0, 'modification Category', 'Category', 12, 2, '2019-06-15 10:58:13', '2019-06-15 10:58:13'),
(303, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-15 14:34:50', '2019-06-15 14:34:50'),
(304, 1, 0, 'Création : QuickAccess', 'QuickAccess', 10, 2, '2019-06-15 14:44:56', '2019-06-15 14:44:56'),
(305, 1, 0, 'modification CMS', 'CMS', 13, 2, '2019-06-15 14:46:49', '2019-06-15 14:46:49'),
(306, 1, 0, 'modification Category', 'Category', 12, 2, '2019-06-15 14:49:45', '2019-06-15 14:49:45'),
(307, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 1, '2019-06-15 14:50:48', '2019-06-15 14:50:48'),
(308, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-15 15:15:03', '2019-06-15 15:15:03'),
(309, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 1, '2019-06-15 15:46:08', '2019-06-15 15:46:08'),
(310, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 66, 0, '2019-06-15 15:48:33', '2019-06-15 15:48:33'),
(311, 1, 0, 'modification Product', 'Product', 9, 1, '2019-06-15 15:51:00', '2019-06-15 15:51:00'),
(312, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-15 16:01:28', '2019-06-15 16:01:28'),
(313, 1, 0, 'modification Product', 'Product', 9, 1, '2019-06-15 16:29:34', '2019-06-15 16:29:34'),
(314, 1, 0, 'modification Product', 'Product', 9, 1, '2019-06-15 16:29:57', '2019-06-15 16:29:57'),
(315, 1, 0, 'modification Product', 'Product', 20, 1, '2019-06-15 16:30:33', '2019-06-15 16:30:33'),
(316, 1, 0, 'Suppression : Product', 'Product', 14, 1, '2019-06-15 16:30:46', '2019-06-15 16:30:46'),
(317, 1, 0, 'modification Product', 'Product', 20, 1, '2019-06-15 16:31:03', '2019-06-15 16:31:03'),
(318, 1, 0, 'Product status switched to enable', 'Product', 20, 1, '2019-06-15 16:31:28', '2019-06-15 16:31:28'),
(319, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 67, 0, '2019-06-15 16:45:28', '2019-06-15 16:45:28'),
(320, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 1, '2019-06-15 17:12:31', '2019-06-15 17:12:31'),
(321, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-15 17:15:10', '2019-06-15 17:15:10'),
(322, 1, 0, 'modification CMS', 'CMS', 9, 2, '2019-06-15 17:16:14', '2019-06-15 17:16:14'),
(323, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-15 20:46:41', '2019-06-15 20:46:41'),
(324, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 20:55:37', '2019-06-15 20:55:37'),
(325, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 20:55:37', '2019-06-15 20:55:37'),
(326, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 20:55:38', '2019-06-15 20:55:38'),
(327, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 20:55:38', '2019-06-15 20:55:38'),
(328, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 20:55:39', '2019-06-15 20:55:39'),
(329, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 20:55:40', '2019-06-15 20:55:40'),
(330, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 20:55:41', '2019-06-15 20:55:41'),
(331, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-15 20:56:07', '2019-06-15 20:56:07'),
(332, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-15 21:00:41', '2019-06-15 21:00:41'),
(333, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-15 21:01:54', '2019-06-15 21:01:54'),
(334, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-15 21:02:35', '2019-06-15 21:02:35'),
(335, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-15 21:03:10', '2019-06-15 21:03:10'),
(336, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-15 21:03:54', '2019-06-15 21:03:54'),
(337, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-15 21:04:29', '2019-06-15 21:04:29'),
(338, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 21:05:00', '2019-06-15 21:05:00'),
(339, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-15 21:05:41', '2019-06-15 21:05:41'),
(340, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-15 21:50:59', '2019-06-15 21:50:59'),
(341, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-16 12:00:27', '2019-06-16 12:00:27'),
(342, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 69, 0, '2019-06-16 12:47:49', '2019-06-16 12:47:49'),
(343, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-16 13:19:34', '2019-06-16 13:19:34'),
(344, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-17 11:07:04', '2019-06-17 11:07:04'),
(345, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-17 11:41:29', '2019-06-17 11:41:29'),
(346, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-17 12:20:34', '2019-06-17 12:20:34'),
(347, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-17 13:44:25', '2019-06-17 13:44:25'),
(348, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-17 16:05:05', '2019-06-17 16:05:05'),
(349, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-17 16:05:50', '2019-06-17 16:05:50'),
(350, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-17 16:08:23', '2019-06-17 16:08:23'),
(351, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-17 16:14:12', '2019-06-17 16:14:12'),
(352, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-17 16:15:08', '2019-06-17 16:15:08'),
(353, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 1, '2019-06-17 16:30:59', '2019-06-17 16:30:59'),
(354, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-17 16:32:04', '2019-06-17 16:32:04'),
(355, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 70, 0, '2019-06-17 16:35:33', '2019-06-17 16:35:33'),
(356, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 72, 0, '2019-06-17 16:38:39', '2019-06-17 16:38:39'),
(357, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-17 16:46:16', '2019-06-17 16:46:16'),
(358, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-17 16:48:02', '2019-06-17 16:48:02'),
(359, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-17 16:48:20', '2019-06-17 16:48:20'),
(360, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-17 16:48:44', '2019-06-17 16:48:44'),
(361, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-17 16:50:41', '2019-06-17 16:50:41'),
(362, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-17 16:51:21', '2019-06-17 16:51:21'),
(363, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 73, 0, '2019-06-17 16:58:58', '2019-06-17 16:58:58'),
(364, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-17 17:01:04', '2019-06-17 17:01:04'),
(365, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-17 17:03:35', '2019-06-17 17:03:35'),
(366, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-17 17:05:32', '2019-06-17 17:05:32'),
(367, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-17 17:05:40', '2019-06-17 17:05:40'),
(368, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-17 20:05:08', '2019-06-17 20:05:08'),
(369, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-17 20:52:42', '2019-06-17 20:52:42'),
(370, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-18 08:02:33', '2019-06-18 08:02:33'),
(371, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-18 08:52:26', '2019-06-18 08:52:26'),
(372, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-18 09:15:30', '2019-06-18 09:15:30'),
(373, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-18 09:55:48', '2019-06-18 09:55:48'),
(374, 1, 0, 'modification CMS', 'CMS', 12, 2, '2019-06-18 09:59:09', '2019-06-18 09:59:09'),
(375, 1, 0, 'Connexion au back-office depuis 77.136.82.242', '', 0, 1, '2019-06-18 10:45:49', '2019-06-18 10:45:49'),
(376, 1, 0, 'Connexion au back-office depuis 81.185.162.167', '', 0, 1, '2019-06-18 11:32:42', '2019-06-18 11:32:42'),
(377, 1, 0, 'Connexion au back-office depuis 77.136.41.64', '', 0, 1, '2019-06-18 11:44:51', '2019-06-18 11:44:51'),
(378, 1, 0, 'Création : Product', 'Product', 21, 1, '2019-06-18 12:30:45', '2019-06-18 12:30:45'),
(379, 1, 0, 'modification Product', 'Product', 21, 1, '2019-06-18 12:32:20', '2019-06-18 12:32:20'),
(380, 1, 0, 'modification Product', 'Product', 21, 1, '2019-06-18 12:32:39', '2019-06-18 12:32:39'),
(381, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 74, 0, '2019-06-18 12:33:11', '2019-06-18 12:33:11'),
(382, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 75, 0, '2019-06-18 13:05:35', '2019-06-18 13:05:35'),
(383, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 76, 0, '2019-06-18 13:07:16', '2019-06-18 13:07:16'),
(384, 1, 0, 'Connexion au back-office depuis 77.136.84.86', '', 0, 1, '2019-06-18 14:18:50', '2019-06-18 14:18:50'),
(385, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 77, 0, '2019-06-18 14:28:30', '2019-06-18 14:28:30'),
(386, 1, 0, 'Connexion au back-office depuis 77.136.84.86', '', 0, 1, '2019-06-18 14:44:06', '2019-06-18 14:44:06'),
(387, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-18 16:42:14', '2019-06-18 16:42:14'),
(388, 1, 0, 'modification CMS', 'CMS', 12, 2, '2019-06-18 16:55:11', '2019-06-18 16:55:11'),
(389, 1, 0, 'modification CMS', 'CMS', 12, 2, '2019-06-18 16:56:46', '2019-06-18 16:56:46'),
(390, 1, 0, 'modification CMS', 'CMS', 12, 2, '2019-06-18 16:58:34', '2019-06-18 16:58:34'),
(391, 1, 0, 'modification CMS', 'CMS', 14, 2, '2019-06-18 17:05:32', '2019-06-18 17:05:32'),
(392, 1, 0, 'modification CMS', 'CMS', 14, 2, '2019-06-18 17:05:32', '2019-06-18 17:05:32'),
(393, 1, 0, 'Connexion au back-office depuis 81.185.172.63', '', 0, 1, '2019-06-18 17:24:07', '2019-06-18 17:24:07'),
(394, 1, 0, 'Connexion au back-office depuis 81.185.172.63', '', 0, 1, '2019-06-18 17:29:43', '2019-06-18 17:29:43'),
(395, 1, 0, 'modification CMS', 'CMS', 12, 2, '2019-06-18 17:31:32', '2019-06-18 17:31:32'),
(396, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 78, 0, '2019-06-18 17:40:43', '2019-06-18 17:40:43'),
(397, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 79, 0, '2019-06-18 17:46:07', '2019-06-18 17:46:07'),
(398, 1, 0, 'Connexion au back-office depuis 81.185.172.63', '', 0, 1, '2019-06-18 19:28:12', '2019-06-18 19:28:12'),
(399, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-18 19:31:09', '2019-06-18 19:31:09'),
(400, 1, 0, 'Connexion au back-office depuis 81.185.172.63', '', 0, 1, '2019-06-18 19:48:57', '2019-06-18 19:48:57'),
(401, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-18 19:53:04', '2019-06-18 19:53:04'),
(402, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-18 19:53:40', '2019-06-18 19:53:40'),
(403, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-18 19:57:19', '2019-06-18 19:57:19'),
(404, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-18 19:58:27', '2019-06-18 19:58:27'),
(405, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-18 19:59:39', '2019-06-18 19:59:39'),
(406, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 20:00:52', '2019-06-18 20:00:52'),
(407, 1, 0, 'modification Product', 'Product', 20, 2, '2019-06-18 20:01:53', '2019-06-18 20:01:53'),
(408, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-18 20:02:58', '2019-06-18 20:02:58'),
(409, 1, 0, 'modification Product', 'Product', 9, 2, '2019-06-18 20:04:26', '2019-06-18 20:04:26'),
(410, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-18 20:05:37', '2019-06-18 20:05:37'),
(411, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-18 20:06:30', '2019-06-18 20:06:30'),
(412, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-18 20:07:14', '2019-06-18 20:07:14'),
(413, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-18 20:19:48', '2019-06-18 20:19:48'),
(414, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-18 20:31:51', '2019-06-18 20:31:51'),
(415, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-18 20:36:24', '2019-06-18 20:36:24'),
(416, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-18 20:36:26', '2019-06-18 20:36:26'),
(417, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-18 22:34:11', '2019-06-18 22:34:11'),
(418, 1, 0, 'modification Product', 'Product', 10, 2, '2019-06-18 22:36:59', '2019-06-18 22:36:59'),
(419, 1, 0, 'modification Product', 'Product', 12, 2, '2019-06-18 22:39:59', '2019-06-18 22:39:59'),
(420, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-18 22:41:14', '2019-06-18 22:41:14'),
(421, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:26', '2019-06-18 22:44:26'),
(422, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:27', '2019-06-18 22:44:27'),
(423, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:28', '2019-06-18 22:44:28'),
(424, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:28', '2019-06-18 22:44:28'),
(425, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:28', '2019-06-18 22:44:28'),
(426, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:29', '2019-06-18 22:44:29'),
(427, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:29', '2019-06-18 22:44:29'),
(428, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:29', '2019-06-18 22:44:29'),
(429, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:29', '2019-06-18 22:44:29'),
(430, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:44:30', '2019-06-18 22:44:30'),
(431, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-18 22:45:05', '2019-06-18 22:45:05'),
(432, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-18 22:47:29', '2019-06-18 22:47:29'),
(433, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-18 22:48:33', '2019-06-18 22:48:33'),
(434, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-18 22:49:11', '2019-06-18 22:49:11'),
(435, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-18 22:49:12', '2019-06-18 22:49:12'),
(436, 1, 0, 'modification Product', 'Product', 20, 2, '2019-06-18 22:50:24', '2019-06-18 22:50:24'),
(437, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-18 22:51:12', '2019-06-18 22:51:12'),
(438, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:51:48', '2019-06-18 22:51:48'),
(439, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:51:49', '2019-06-18 22:51:49'),
(440, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:51:49', '2019-06-18 22:51:49'),
(441, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:51:50', '2019-06-18 22:51:50'),
(442, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:51:50', '2019-06-18 22:51:50'),
(443, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-18 22:51:50', '2019-06-18 22:51:50'),
(444, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-18 22:52:24', '2019-06-18 22:52:24'),
(445, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-18 22:52:24', '2019-06-18 22:52:24'),
(446, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-18 22:54:31', '2019-06-18 22:54:31');
INSERT INTO `ap_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_employee`, `date_add`, `date_upd`) VALUES
(447, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-18 22:54:31', '2019-06-18 22:54:31'),
(448, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-18 23:15:04', '2019-06-18 23:15:04'),
(449, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 1, '2019-06-19 12:36:06', '2019-06-19 12:36:06'),
(450, 1, 0, 'Création : Product', 'Product', 22, 1, '2019-06-19 12:37:19', '2019-06-19 12:37:19'),
(451, 1, 0, 'Création : AttributeGroup', 'AttributeGroup', 4, 1, '2019-06-19 12:39:40', '2019-06-19 12:39:40'),
(452, 1, 0, 'Création : Attribute', 'Attribute', 25, 1, '2019-06-19 12:42:32', '2019-06-19 12:42:32'),
(453, 1, 0, 'Création : Attribute', 'Attribute', 26, 1, '2019-06-19 12:43:09', '2019-06-19 12:43:09'),
(454, 1, 0, 'modification Product', 'Product', 22, 1, '2019-06-19 12:45:36', '2019-06-19 12:45:36'),
(455, 1, 0, 'modification Product', 'Product', 22, 1, '2019-06-19 12:46:19', '2019-06-19 12:46:19'),
(456, 1, 0, 'Création : AttributeGroup', 'AttributeGroup', 5, 1, '2019-06-19 12:48:53', '2019-06-19 12:48:53'),
(457, 1, 0, 'Création : Attribute', 'Attribute', 27, 1, '2019-06-19 12:49:14', '2019-06-19 12:49:14'),
(458, 1, 0, 'Création : Attribute', 'Attribute', 28, 1, '2019-06-19 12:49:50', '2019-06-19 12:49:50'),
(459, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-19 12:53:36', '2019-06-19 12:53:36'),
(460, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-19 12:55:34', '2019-06-19 12:55:34'),
(461, 1, 0, 'modification Product', 'Product', 20, 2, '2019-06-19 13:04:18', '2019-06-19 13:04:18'),
(462, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-19 13:07:20', '2019-06-19 13:07:20'),
(463, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-19 13:08:39', '2019-06-19 13:08:39'),
(464, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-19 13:13:04', '2019-06-19 13:13:04'),
(465, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-19 13:13:40', '2019-06-19 13:13:40'),
(466, 1, 0, 'modification Product', 'Product', 20, 2, '2019-06-19 13:15:09', '2019-06-19 13:15:09'),
(467, 1, 0, 'modification Product', 'Product', 20, 2, '2019-06-19 13:15:42', '2019-06-19 13:15:42'),
(468, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-19 13:16:26', '2019-06-19 13:16:26'),
(469, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-19 13:16:44', '2019-06-19 13:16:44'),
(470, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-19 13:16:53', '2019-06-19 13:16:53'),
(471, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-19 13:18:47', '2019-06-19 13:18:47'),
(472, 1, 0, 'modification Product', 'Product', 15, 2, '2019-06-19 13:19:10', '2019-06-19 13:19:10'),
(473, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-19 13:20:22', '2019-06-19 13:20:22'),
(474, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-19 13:20:43', '2019-06-19 13:20:43'),
(475, 1, 0, 'modification Product', 'Product', 17, 2, '2019-06-19 13:21:04', '2019-06-19 13:21:04'),
(476, 1, 0, 'modification Product', 'Product', 18, 2, '2019-06-19 13:22:40', '2019-06-19 13:22:40'),
(477, 1, 0, 'modification Product', 'Product', 19, 2, '2019-06-19 13:24:07', '2019-06-19 13:24:07'),
(478, 1, 0, 'modification Product', 'Product', 13, 2, '2019-06-19 13:42:58', '2019-06-19 13:42:58'),
(479, 1, 0, 'modification Product', 'Product', 16, 2, '2019-06-19 13:48:01', '2019-06-19 13:48:01'),
(480, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-19 15:21:34', '2019-06-19 15:21:34'),
(481, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-19 15:28:07', '2019-06-19 15:28:07'),
(482, 1, 0, 'Création : Product', 'Product', 23, 2, '2019-06-19 15:31:24', '2019-06-19 15:31:24'),
(483, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 15:32:43', '2019-06-19 15:32:43'),
(484, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 15:33:27', '2019-06-19 15:33:27'),
(485, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 15:33:46', '2019-06-19 15:33:46'),
(486, 1, 0, 'Création : Product', 'Product', 24, 2, '2019-06-19 15:45:01', '2019-06-19 15:45:01'),
(487, 1, 0, 'modification Product', 'Product', 11, 2, '2019-06-19 15:48:31', '2019-06-19 15:48:31'),
(488, 1, 0, 'modification Product', 'Product', 24, 2, '2019-06-19 15:50:00', '2019-06-19 15:50:00'),
(489, 1, 0, 'Création : Product', 'Product', 25, 2, '2019-06-19 15:55:52', '2019-06-19 15:55:52'),
(490, 1, 0, 'modification Product', 'Product', 25, 2, '2019-06-19 15:57:02', '2019-06-19 15:57:02'),
(491, 1, 0, 'modification Product', 'Product', 25, 2, '2019-06-19 15:58:10', '2019-06-19 15:58:10'),
(492, 1, 0, 'modification Product', 'Product', 25, 2, '2019-06-19 15:59:02', '2019-06-19 15:59:02'),
(493, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 16:02:29', '2019-06-19 16:02:29'),
(494, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 16:03:01', '2019-06-19 16:03:01'),
(495, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 16:03:56', '2019-06-19 16:03:56'),
(496, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 16:05:02', '2019-06-19 16:05:02'),
(497, 1, 0, 'Connexion au back-office depuis 77.154.225.102', '', 0, 1, '2019-06-19 16:30:37', '2019-06-19 16:30:37'),
(498, 1, 0, 'Connexion au back-office depuis 77.136.196.58', '', 0, 1, '2019-06-19 20:23:48', '2019-06-19 20:23:48'),
(499, 1, 0, 'Connexion au back-office depuis 154.67.215.55', '', 0, 2, '2019-06-19 20:37:29', '2019-06-19 20:37:29'),
(500, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 20:43:24', '2019-06-19 20:43:24'),
(501, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 20:45:16', '2019-06-19 20:45:16'),
(502, 1, 0, 'modification Product', 'Product', 23, 2, '2019-06-19 20:47:41', '2019-06-19 20:47:41'),
(503, 1, 0, 'modification Product', 'Product', 24, 2, '2019-06-19 20:48:29', '2019-06-19 20:48:29'),
(504, 1, 0, 'modification Product', 'Product', 25, 2, '2019-06-19 20:49:40', '2019-06-19 20:49:40'),
(505, 1, 0, 'modification Product', 'Product', 25, 2, '2019-06-19 20:50:39', '2019-06-19 20:50:39'),
(506, 1, 0, 'modification Product', 'Product', 24, 1, '2019-06-19 20:53:30', '2019-06-19 20:53:30'),
(507, 1, 0, 'modification Product', 'Product', 25, 1, '2019-06-19 20:53:46', '2019-06-19 20:53:46'),
(508, 1, 0, 'Connexion au back-office depuis 165.169.52.178', '', 0, 1, '2019-06-20 13:11:31', '2019-06-20 13:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `ap_mail`
--

CREATE TABLE `ap_mail` (
  `id_mail` int(11) UNSIGNED NOT NULL,
  `recipient` varchar(126) NOT NULL,
  `template` varchar(62) NOT NULL,
  `subject` varchar(254) NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_manufacturer`
--

CREATE TABLE `ap_manufacturer` (
  `id_manufacturer` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_manufacturer`
--

INSERT INTO `ap_manufacturer` (`id_manufacturer`, `name`, `date_add`, `date_upd`, `active`) VALUES
(1, 'Fashion Manufacturer', '2019-06-08 02:34:25', '2019-06-12 01:37:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_manufacturer_lang`
--

CREATE TABLE `ap_manufacturer_lang` (
  `id_manufacturer` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `short_description` text,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_manufacturer_lang`
--

INSERT INTO `ap_manufacturer_lang` (`id_manufacturer`, `id_lang`, `description`, `short_description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_manufacturer_shop`
--

CREATE TABLE `ap_manufacturer_shop` (
  `id_manufacturer` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_manufacturer_shop`
--

INSERT INTO `ap_manufacturer_shop` (`id_manufacturer`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_memcached_servers`
--

CREATE TABLE `ap_memcached_servers` (
  `id_memcached_server` int(11) UNSIGNED NOT NULL,
  `ip` varchar(254) NOT NULL,
  `port` int(11) UNSIGNED NOT NULL,
  `weight` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_message`
--

CREATE TABLE `ap_message` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `private` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_message_readed`
--

CREATE TABLE `ap_message_readed` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_meta`
--

CREATE TABLE `ap_meta` (
  `id_meta` int(10) UNSIGNED NOT NULL,
  `page` varchar(64) NOT NULL,
  `configurable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_meta`
--

INSERT INTO `ap_meta` (`id_meta`, `page`, `configurable`) VALUES
(1, 'pagenotfound', 1),
(2, 'best-sales', 1),
(3, 'contact', 1),
(4, 'index', 1),
(5, 'manufacturer', 1),
(6, 'new-products', 1),
(7, 'password', 1),
(8, 'prices-drop', 1),
(9, 'sitemap', 1),
(10, 'supplier', 1),
(11, 'address', 1),
(12, 'addresses', 1),
(13, 'authentication', 1),
(14, 'cart', 1),
(15, 'discount', 1),
(16, 'history', 1),
(17, 'identity', 1),
(18, 'my-account', 1),
(19, 'order-follow', 1),
(20, 'order-slip', 1),
(21, 'order', 1),
(22, 'search', 1),
(23, 'stores', 1),
(24, 'order-opc', 1),
(25, 'guest-tracking', 1),
(26, 'order-confirmation', 1),
(27, 'product', 0),
(28, 'category', 0),
(29, 'cms', 0),
(30, 'module-cheque-payment', 0),
(31, 'module-cheque-validation', 0),
(32, 'module-bankwire-validation', 0),
(33, 'module-bankwire-payment', 0),
(34, 'module-cashondelivery-validation', 0),
(35, 'products-comparison', 1),
(36, 'module-blocknewsletter-verification', 1),
(37, 'module-cronjobs-callback', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_meta_lang`
--

CREATE TABLE `ap_meta_lang` (
  `id_meta` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `url_rewrite` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_meta_lang`
--

INSERT INTO `ap_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
(1, 1, 1, 'Erreur 404', 'Impossible de trouver la page', '', 'page-introuvable'),
(2, 1, 1, 'Meilleures ventes', 'Nos meilleures ventes', '', 'meilleures-ventes'),
(3, 1, 1, 'Nous contacter', 'Utiliser le formulaire pour nous contacter', '', 'nous-contacter'),
(4, 1, 1, '', 'Boutique propulsée par PrestaShop', '', ''),
(5, 1, 1, 'Fabricants', 'Liste des fabricants', '', 'fabricants'),
(6, 1, 1, 'Nouveaux produits', 'Nos nouveaux produits', '', 'nouveaux-produits'),
(7, 1, 1, 'Mot de passe oublié', 'Entrez l\'adresse e-mail que vous utilisez pour vous connecter afin de recevoir un e-mail avec un nouveau mot de passe', '', 'recuperation-mot-de-passe'),
(8, 1, 1, 'Promotions', 'Nos promotions', '', 'promotions'),
(9, 1, 1, 'Plan du site', 'Vous êtes perdu ? Trouvez ce que vous cherchez', '', 'plan-site'),
(10, 1, 1, 'Fournisseurs', 'Liste des fournisseurs', '', 'fournisseur'),
(11, 1, 1, 'Adresse', '', '', 'adresse'),
(12, 1, 1, 'Adresses', '', '', 'adresses'),
(13, 1, 1, 'Connexion', '', '', 'connexion'),
(14, 1, 1, 'Panier', '', '', 'panier'),
(15, 1, 1, 'Réduction', '', '', 'reduction'),
(16, 1, 1, 'Historique des commandes', '', '', 'historique-commandes'),
(17, 1, 1, 'Identité', '', '', 'identite'),
(18, 1, 1, 'Mon compte', '', '', 'mon-compte'),
(19, 1, 1, 'Suivi de commande', '', '', 'suivi-commande'),
(20, 1, 1, 'Avoirs', '', '', 'avoirs'),
(21, 1, 1, 'Commande', '', '', 'commande'),
(22, 1, 1, 'Recherche', '', '', 'recherche'),
(23, 1, 1, 'Magasins', '', '', 'magasins'),
(24, 1, 1, 'Commande', '', '', 'commande-rapide'),
(25, 1, 1, 'Suivi de commande invité', '', '', 'suivi-commande-invite'),
(26, 1, 1, 'Confirmation de commande', '', '', 'confirmation-commande'),
(35, 1, 1, 'Comparaison de produits', '', '', 'comparaison-produits'),
(36, 1, 1, '', '', '', ''),
(37, 1, 1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_module`
--

CREATE TABLE `ap_module` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `version` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_module`
--

INSERT INTO `ap_module` (`id_module`, `name`, `active`, `version`) VALUES
(1, 'socialsharing', 1, '1.4.3'),
(2, 'blockbanner', 1, '1.4.1'),
(3, 'bankwire', 1, '1.1.2'),
(4, 'blockbestsellers', 1, '1.8.1'),
(5, 'blockcart', 1, '1.6.2'),
(6, 'blocksocial', 1, '1.2.2'),
(7, 'blockcategories', 1, '2.9.4'),
(8, 'blockcurrencies', 1, '0.4.1'),
(9, 'blockfacebook', 1, '1.4.1'),
(10, 'blocklanguages', 1, '1.5.1'),
(11, 'blocklayered', 1, '2.2.1'),
(12, 'blockcms', 1, '2.1.2'),
(13, 'blockcmsinfo', 1, '1.6.1'),
(14, 'blockcontact', 1, '1.4.1'),
(15, 'blockcontactinfos', 1, '1.2.1'),
(16, 'blockmanufacturer', 1, '1.4.1'),
(17, 'blockmyaccount', 1, '1.4.1'),
(18, 'blockmyaccountfooter', 1, '1.6.1'),
(19, 'blocknewproducts', 1, '1.10.1'),
(20, 'blocknewsletter', 1, '2.4.0'),
(21, 'blockpaymentlogo', 1, '0.4.1'),
(22, 'blocksearch', 1, '1.7.1'),
(23, 'blockspecials', 1, '1.3.1'),
(24, 'blockstore', 1, '1.3.1'),
(25, 'blocksupplier', 1, '1.2.1'),
(26, 'blocktags', 1, '1.3.1'),
(27, 'blocktopmenu', 1, '2.2.4'),
(28, 'blockuserinfo', 1, '0.4.1'),
(29, 'blockviewed', 1, '1.3.1'),
(30, 'cheque', 1, '2.7.2'),
(31, 'dashactivity', 1, '1.0.0'),
(32, 'dashtrends', 1, '1.0.0'),
(33, 'dashgoals', 1, '1.0.0'),
(34, 'dashproducts', 1, '1.0.0'),
(35, 'graphnvd3', 1, '1.5.1'),
(36, 'gridhtml', 1, '1.3.1'),
(37, 'homeslider', 1, '1.6.1'),
(38, 'homefeatured', 1, '1.8.1'),
(39, 'productpaymentlogos', 1, '1.4.1'),
(40, 'pagesnotfound', 1, '1.5.1'),
(41, 'sekeywords', 1, '1.4.1'),
(42, 'statsbestcategories', 1, '1.5.1'),
(43, 'statsbestcustomers', 1, '1.5.1'),
(44, 'statsbestproducts', 1, '1.5.1'),
(45, 'statsbestsuppliers', 1, '1.4.1'),
(46, 'statsbestvouchers', 1, '1.5.1'),
(47, 'statscarrier', 1, '1.4.1'),
(48, 'statscatalog', 1, '1.4.0'),
(49, 'statscheckup', 1, '1.5.0'),
(50, 'statsdata', 1, '1.6.2'),
(51, 'statsequipment', 1, '1.3.1'),
(52, 'statsforecast', 1, '1.4.1'),
(53, 'statslive', 1, '1.3.1'),
(54, 'statsnewsletter', 1, '1.4.2'),
(55, 'statsorigin', 1, '1.4.1'),
(56, 'statspersonalinfos', 1, '1.4.1'),
(57, 'statsproduct', 1, '1.5.1'),
(58, 'statsregistrations', 1, '1.4.1'),
(59, 'statssales', 1, '1.3.1'),
(60, 'statssearch', 1, '1.4.1'),
(61, 'statsstock', 1, '1.6.0'),
(62, 'statsvisits', 1, '1.6.1'),
(63, 'themeconfigurator', 1, '2.1.3'),
(64, 'gamification', 1, '2.2.1'),
(65, 'cronjobs', 1, '1.4.0'),
(66, 'psaddonsconnect', 1, '1.0.1'),
(67, 'npcalendar', 1, '0.0.1'),
(68, 'pointgestion', 1, '0.0.1'),
(69, 'bonslick', 1, '1.0.0'),
(70, 'htmlbox', 1, '1.7.1'),
(71, 'paypal', 1, '3.12.1');

-- --------------------------------------------------------

--
-- Table structure for table `ap_modules_perfs`
--

CREATE TABLE `ap_modules_perfs` (
  `id_modules_perfs` int(11) UNSIGNED NOT NULL,
  `session` int(11) UNSIGNED NOT NULL,
  `module` varchar(62) NOT NULL,
  `method` varchar(126) NOT NULL,
  `time_start` double UNSIGNED NOT NULL,
  `time_end` double UNSIGNED NOT NULL,
  `memory_start` int(10) UNSIGNED NOT NULL,
  `memory_end` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_module_access`
--

CREATE TABLE `ap_module_access` (
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_module` int(10) UNSIGNED NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `configure` tinyint(1) NOT NULL DEFAULT '0',
  `uninstall` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_module_access`
--

INSERT INTO `ap_module_access` (`id_profile`, `id_module`, `view`, `configure`, `uninstall`) VALUES
(2, 1, 1, 1, 1),
(2, 2, 1, 1, 1),
(2, 3, 1, 1, 1),
(2, 4, 1, 1, 1),
(2, 5, 1, 1, 1),
(2, 6, 1, 1, 1),
(2, 7, 1, 1, 1),
(2, 8, 1, 1, 1),
(2, 9, 1, 1, 1),
(2, 10, 1, 1, 1),
(2, 11, 1, 1, 1),
(2, 12, 1, 1, 1),
(2, 13, 1, 1, 1),
(2, 14, 1, 1, 1),
(2, 15, 1, 1, 1),
(2, 16, 1, 1, 1),
(2, 17, 1, 1, 1),
(2, 18, 1, 1, 1),
(2, 19, 1, 1, 1),
(2, 20, 1, 1, 1),
(2, 21, 1, 1, 1),
(2, 22, 1, 1, 1),
(2, 23, 1, 1, 1),
(2, 24, 1, 1, 1),
(2, 25, 1, 1, 1),
(2, 26, 1, 1, 1),
(2, 27, 1, 1, 1),
(2, 28, 1, 1, 1),
(2, 29, 1, 1, 1),
(2, 30, 1, 1, 1),
(2, 31, 1, 1, 1),
(2, 32, 1, 1, 1),
(2, 33, 1, 1, 1),
(2, 34, 1, 1, 1),
(2, 35, 1, 1, 1),
(2, 36, 1, 1, 1),
(2, 37, 1, 1, 1),
(2, 38, 1, 1, 1),
(2, 39, 1, 1, 1),
(2, 40, 1, 1, 1),
(2, 41, 1, 1, 1),
(2, 42, 1, 1, 1),
(2, 43, 1, 1, 1),
(2, 44, 1, 1, 1),
(2, 45, 1, 1, 1),
(2, 46, 1, 1, 1),
(2, 47, 1, 1, 1),
(2, 48, 1, 1, 1),
(2, 49, 1, 1, 1),
(2, 50, 1, 1, 1),
(2, 51, 1, 1, 1),
(2, 52, 1, 1, 1),
(2, 53, 1, 1, 1),
(2, 54, 1, 1, 1),
(2, 55, 1, 1, 1),
(2, 56, 1, 1, 1),
(2, 57, 1, 1, 1),
(2, 58, 1, 1, 1),
(2, 59, 1, 1, 1),
(2, 60, 1, 1, 1),
(2, 61, 1, 1, 1),
(2, 62, 1, 1, 1),
(2, 63, 1, 1, 1),
(2, 64, 1, 1, 1),
(2, 65, 1, 1, 1),
(2, 66, 1, 1, 1),
(2, 67, 1, 1, 1),
(2, 68, 1, 1, 1),
(2, 69, 1, 1, 1),
(2, 70, 1, 1, 1),
(2, 71, 1, 1, 1),
(3, 1, 1, 0, 0),
(3, 2, 1, 0, 0),
(3, 3, 1, 0, 0),
(3, 4, 1, 0, 0),
(3, 5, 1, 0, 0),
(3, 6, 1, 0, 0),
(3, 7, 1, 0, 0),
(3, 8, 1, 0, 0),
(3, 9, 1, 0, 0),
(3, 10, 1, 0, 0),
(3, 11, 1, 0, 0),
(3, 12, 1, 0, 0),
(3, 13, 1, 0, 0),
(3, 14, 1, 0, 0),
(3, 15, 1, 0, 0),
(3, 16, 1, 0, 0),
(3, 17, 1, 0, 0),
(3, 18, 1, 0, 0),
(3, 19, 1, 0, 0),
(3, 20, 1, 0, 0),
(3, 21, 1, 0, 0),
(3, 22, 1, 0, 0),
(3, 23, 1, 0, 0),
(3, 24, 1, 0, 0),
(3, 25, 1, 0, 0),
(3, 26, 1, 0, 0),
(3, 27, 1, 0, 0),
(3, 28, 1, 0, 0),
(3, 29, 1, 0, 0),
(3, 30, 1, 0, 0),
(3, 31, 1, 0, 0),
(3, 32, 1, 0, 0),
(3, 33, 1, 0, 0),
(3, 34, 1, 0, 0),
(3, 35, 1, 0, 0),
(3, 36, 1, 0, 0),
(3, 37, 1, 0, 0),
(3, 38, 1, 0, 0),
(3, 39, 1, 0, 0),
(3, 40, 1, 0, 0),
(3, 41, 1, 0, 0),
(3, 42, 1, 0, 0),
(3, 43, 1, 0, 0),
(3, 44, 1, 0, 0),
(3, 45, 1, 0, 0),
(3, 46, 1, 0, 0),
(3, 47, 1, 0, 0),
(3, 48, 1, 0, 0),
(3, 49, 1, 0, 0),
(3, 50, 1, 0, 0),
(3, 51, 1, 0, 0),
(3, 52, 1, 0, 0),
(3, 53, 1, 0, 0),
(3, 54, 1, 0, 0),
(3, 55, 1, 0, 0),
(3, 56, 1, 0, 0),
(3, 57, 1, 0, 0),
(3, 58, 1, 0, 0),
(3, 59, 1, 0, 0),
(3, 60, 1, 0, 0),
(3, 61, 1, 0, 0),
(3, 62, 1, 0, 0),
(3, 63, 1, 0, 0),
(3, 64, 1, 0, 0),
(3, 65, 1, 0, 0),
(3, 66, 1, 0, 0),
(3, 67, 1, 0, 0),
(3, 68, 1, 0, 0),
(3, 69, 1, 0, 0),
(3, 70, 1, 0, 0),
(3, 71, 1, 0, 0),
(4, 1, 1, 1, 1),
(4, 2, 1, 1, 1),
(4, 3, 1, 1, 1),
(4, 4, 1, 1, 1),
(4, 5, 1, 1, 1),
(4, 6, 1, 1, 1),
(4, 7, 1, 1, 1),
(4, 8, 1, 1, 1),
(4, 9, 1, 1, 1),
(4, 10, 1, 1, 1),
(4, 11, 1, 1, 1),
(4, 12, 1, 1, 1),
(4, 13, 1, 1, 1),
(4, 14, 1, 1, 1),
(4, 15, 1, 1, 1),
(4, 16, 1, 1, 1),
(4, 17, 1, 1, 1),
(4, 18, 1, 1, 1),
(4, 19, 1, 1, 1),
(4, 20, 1, 1, 1),
(4, 21, 1, 1, 1),
(4, 22, 1, 1, 1),
(4, 23, 1, 1, 1),
(4, 24, 1, 1, 1),
(4, 25, 1, 1, 1),
(4, 26, 1, 1, 1),
(4, 27, 1, 1, 1),
(4, 28, 1, 1, 1),
(4, 29, 1, 1, 1),
(4, 30, 1, 1, 1),
(4, 31, 1, 1, 1),
(4, 32, 1, 1, 1),
(4, 33, 1, 1, 1),
(4, 34, 1, 1, 1),
(4, 35, 1, 1, 1),
(4, 36, 1, 1, 1),
(4, 37, 1, 1, 1),
(4, 38, 1, 1, 1),
(4, 39, 1, 1, 1),
(4, 40, 1, 1, 1),
(4, 41, 1, 1, 1),
(4, 42, 1, 1, 1),
(4, 43, 1, 1, 1),
(4, 44, 1, 1, 1),
(4, 45, 1, 1, 1),
(4, 46, 1, 1, 1),
(4, 47, 1, 1, 1),
(4, 48, 1, 1, 1),
(4, 49, 1, 1, 1),
(4, 50, 1, 1, 1),
(4, 51, 1, 1, 1),
(4, 52, 1, 1, 1),
(4, 53, 1, 1, 1),
(4, 54, 1, 1, 1),
(4, 55, 1, 1, 1),
(4, 56, 1, 1, 1),
(4, 57, 1, 1, 1),
(4, 58, 1, 1, 1),
(4, 59, 1, 1, 1),
(4, 60, 1, 1, 1),
(4, 61, 1, 1, 1),
(4, 62, 1, 1, 1),
(4, 63, 1, 1, 1),
(4, 64, 1, 1, 1),
(4, 65, 1, 1, 1),
(4, 66, 1, 1, 1),
(4, 67, 1, 1, 1),
(4, 68, 1, 1, 1),
(4, 69, 1, 1, 1),
(4, 70, 1, 1, 1),
(4, 71, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_module_country`
--

CREATE TABLE `ap_module_country` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_country` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_module_country`
--

INSERT INTO `ap_module_country` (`id_module`, `id_shop`, `id_country`) VALUES
(3, 1, 176),
(30, 1, 176),
(71, 1, 176);

-- --------------------------------------------------------

--
-- Table structure for table `ap_module_currency`
--

CREATE TABLE `ap_module_currency` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_currency` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_module_currency`
--

INSERT INTO `ap_module_currency` (`id_module`, `id_shop`, `id_currency`) VALUES
(3, 1, 1),
(3, 1, 2),
(30, 1, 1),
(30, 1, 2),
(71, 1, -2);

-- --------------------------------------------------------

--
-- Table structure for table `ap_module_group`
--

CREATE TABLE `ap_module_group` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_group` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_module_group`
--

INSERT INTO `ap_module_group` (`id_module`, `id_shop`, `id_group`) VALUES
(1, 1, 1),
(1, 1, 2),
(1, 1, 3),
(2, 1, 1),
(2, 1, 2),
(2, 1, 3),
(3, 1, 1),
(3, 1, 2),
(3, 1, 3),
(4, 1, 1),
(4, 1, 2),
(4, 1, 3),
(5, 1, 1),
(5, 1, 2),
(5, 1, 3),
(6, 1, 1),
(6, 1, 2),
(6, 1, 3),
(7, 1, 1),
(7, 1, 2),
(7, 1, 3),
(8, 1, 1),
(8, 1, 2),
(8, 1, 3),
(9, 1, 1),
(9, 1, 2),
(9, 1, 3),
(10, 1, 1),
(10, 1, 2),
(10, 1, 3),
(11, 1, 1),
(11, 1, 2),
(11, 1, 3),
(12, 1, 1),
(12, 1, 2),
(12, 1, 3),
(13, 1, 1),
(13, 1, 2),
(13, 1, 3),
(14, 1, 1),
(14, 1, 2),
(14, 1, 3),
(15, 1, 1),
(15, 1, 2),
(15, 1, 3),
(16, 1, 1),
(16, 1, 2),
(16, 1, 3),
(17, 1, 1),
(17, 1, 2),
(17, 1, 3),
(18, 1, 1),
(18, 1, 2),
(18, 1, 3),
(19, 1, 1),
(19, 1, 2),
(19, 1, 3),
(20, 1, 1),
(20, 1, 2),
(20, 1, 3),
(21, 1, 1),
(21, 1, 2),
(21, 1, 3),
(22, 1, 1),
(22, 1, 2),
(22, 1, 3),
(23, 1, 1),
(23, 1, 2),
(23, 1, 3),
(24, 1, 1),
(24, 1, 2),
(24, 1, 3),
(25, 1, 1),
(25, 1, 2),
(25, 1, 3),
(26, 1, 1),
(26, 1, 2),
(26, 1, 3),
(27, 1, 1),
(27, 1, 2),
(27, 1, 3),
(28, 1, 1),
(28, 1, 2),
(28, 1, 3),
(29, 1, 1),
(29, 1, 2),
(29, 1, 3),
(30, 1, 1),
(30, 1, 2),
(30, 1, 3),
(31, 1, 1),
(31, 1, 2),
(31, 1, 3),
(32, 1, 1),
(32, 1, 2),
(32, 1, 3),
(33, 1, 1),
(33, 1, 2),
(33, 1, 3),
(34, 1, 1),
(34, 1, 2),
(34, 1, 3),
(35, 1, 1),
(35, 1, 2),
(35, 1, 3),
(36, 1, 1),
(36, 1, 2),
(36, 1, 3),
(37, 1, 1),
(37, 1, 2),
(37, 1, 3),
(38, 1, 1),
(38, 1, 2),
(38, 1, 3),
(39, 1, 1),
(39, 1, 2),
(39, 1, 3),
(40, 1, 1),
(40, 1, 2),
(40, 1, 3),
(41, 1, 1),
(41, 1, 2),
(41, 1, 3),
(42, 1, 1),
(42, 1, 2),
(42, 1, 3),
(43, 1, 1),
(43, 1, 2),
(43, 1, 3),
(44, 1, 1),
(44, 1, 2),
(44, 1, 3),
(45, 1, 1),
(45, 1, 2),
(45, 1, 3),
(46, 1, 1),
(46, 1, 2),
(46, 1, 3),
(47, 1, 1),
(47, 1, 2),
(47, 1, 3),
(48, 1, 1),
(48, 1, 2),
(48, 1, 3),
(49, 1, 1),
(49, 1, 2),
(49, 1, 3),
(50, 1, 1),
(50, 1, 2),
(50, 1, 3),
(51, 1, 1),
(51, 1, 2),
(51, 1, 3),
(52, 1, 1),
(52, 1, 2),
(52, 1, 3),
(53, 1, 1),
(53, 1, 2),
(53, 1, 3),
(54, 1, 1),
(54, 1, 2),
(54, 1, 3),
(55, 1, 1),
(55, 1, 2),
(55, 1, 3),
(56, 1, 1),
(56, 1, 2),
(56, 1, 3),
(57, 1, 1),
(57, 1, 2),
(57, 1, 3),
(58, 1, 1),
(58, 1, 2),
(58, 1, 3),
(59, 1, 1),
(59, 1, 2),
(59, 1, 3),
(60, 1, 1),
(60, 1, 2),
(60, 1, 3),
(61, 1, 1),
(61, 1, 2),
(61, 1, 3),
(62, 1, 1),
(62, 1, 2),
(62, 1, 3),
(63, 1, 1),
(63, 1, 2),
(63, 1, 3),
(64, 1, 1),
(64, 1, 2),
(64, 1, 3),
(65, 1, 1),
(65, 1, 2),
(65, 1, 3),
(66, 1, 1),
(66, 1, 2),
(66, 1, 3),
(67, 1, 1),
(67, 1, 2),
(67, 1, 3),
(68, 1, 1),
(68, 1, 2),
(68, 1, 3),
(69, 1, 1),
(69, 1, 2),
(69, 1, 3),
(70, 1, 1),
(70, 1, 2),
(70, 1, 3),
(71, 1, 1),
(71, 1, 2),
(71, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ap_module_preference`
--

CREATE TABLE `ap_module_preference` (
  `id_module_preference` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `interest` tinyint(1) DEFAULT NULL,
  `favorite` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_module_preference`
--

INSERT INTO `ap_module_preference` (`id_module_preference`, `id_employee`, `module`, `interest`, `favorite`) VALUES
(1, 1, 'npcalendar', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_module_shop`
--

CREATE TABLE `ap_module_shop` (
  `id_module` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `enable_device` tinyint(1) NOT NULL DEFAULT '7'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_module_shop`
--

INSERT INTO `ap_module_shop` (`id_module`, `id_shop`, `enable_device`) VALUES
(1, 1, 7),
(2, 1, 3),
(3, 1, 7),
(4, 1, 7),
(5, 1, 7),
(6, 1, 7),
(7, 1, 7),
(8, 1, 7),
(9, 1, 7),
(10, 1, 7),
(11, 1, 7),
(12, 1, 7),
(13, 1, 1),
(14, 1, 7),
(15, 1, 7),
(16, 1, 7),
(17, 1, 7),
(18, 1, 7),
(19, 1, 7),
(20, 1, 7),
(21, 1, 7),
(22, 1, 7),
(23, 1, 7),
(24, 1, 7),
(25, 1, 7),
(26, 1, 7),
(27, 1, 7),
(28, 1, 7),
(29, 1, 7),
(30, 1, 7),
(31, 1, 7),
(32, 1, 7),
(33, 1, 7),
(34, 1, 7),
(35, 1, 7),
(36, 1, 7),
(38, 1, 7),
(39, 1, 7),
(40, 1, 7),
(41, 1, 7),
(42, 1, 7),
(43, 1, 7),
(44, 1, 7),
(45, 1, 7),
(46, 1, 7),
(47, 1, 7),
(48, 1, 7),
(49, 1, 7),
(50, 1, 7),
(51, 1, 7),
(52, 1, 7),
(53, 1, 7),
(54, 1, 7),
(55, 1, 7),
(56, 1, 7),
(57, 1, 7),
(58, 1, 7),
(59, 1, 7),
(60, 1, 7),
(61, 1, 7),
(62, 1, 7),
(63, 1, 7),
(64, 1, 7),
(65, 1, 7),
(66, 1, 7),
(67, 1, 7),
(69, 1, 7),
(70, 1, 7),
(71, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `ap_newsletter`
--

CREATE TABLE `ap_newsletter` (
  `id` int(6) NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `email` varchar(255) NOT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `ip_registration_newsletter` varchar(15) NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_np_calendar_slot`
--

CREATE TABLE `ap_np_calendar_slot` (
  `id` int(11) NOT NULL,
  `date_slot` date NOT NULL,
  `hour` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_np_customer_point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_np_calendar_slot`
--

INSERT INTO `ap_np_calendar_slot` (`id`, `date_slot`, `hour`, `id_customer`, `id_np_customer_point`) VALUES
(71, '2019-06-19', 14, 2, 44),
(72, '2019-06-18', 9, 2, 40),
(73, '2019-06-18', 10, 2, 40),
(74, '2019-06-22', 9, 2, 48),
(75, '2019-06-22', 10, 2, 48),
(76, '2019-06-22', 11, 2, 48),
(77, '2019-06-19', 7, 4, 49);

-- --------------------------------------------------------

--
-- Table structure for table `ap_np_customer`
--

CREATE TABLE `ap_np_customer` (
  `id` int(11) NOT NULL,
  `id_np_customer_type` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `verified` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_np_customer`
--

INSERT INTO `ap_np_customer` (`id`, `id_np_customer_type`, `id_customer`, `verified`) VALUES
(1, 1, 2, 1),
(3, 0, 1, 0),
(4, 4, 4, 1),
(5, 0, 3, 0),
(6, 0, 5, 0),
(7, 1, 6, 1),
(8, 1, 7, 1),
(9, 4, 8, 1),
(10, 1, 9, 1),
(11, 5, 10, 1),
(12, 1, 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_np_customer_file`
--

CREATE TABLE `ap_np_customer_file` (
  `id` int(11) NOT NULL,
  `id_np_customer_file_to_upload` int(11) NOT NULL,
  `id_np_customer` int(11) NOT NULL,
  `file` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_np_customer_file`
--

INSERT INTO `ap_np_customer_file` (`id`, `id_np_customer_file_to_upload`, `id_np_customer`, `file`) VALUES
(3, 9, 1, 'http://79.137.76.201/upload/5cfe4a238ceff.pdf'),
(7, 9, 4, 'http://79.137.76.201/upload/5cfe93b843fb3.jpg'),
(8, 9, 7, 'http://www.autopermis.re/upload/5d00f0f983490.pdf'),
(9, 9, 7, 'http://www.autopermis.re/upload/5d00f1547575b.pdf'),
(10, 9, 8, 'http://www.autopermis.re/upload/5d00f917e33e8.bmp'),
(11, 12, 8, 'http://www.autopermis.re/upload/5d00f917e5822.pdf'),
(12, 10, 4, 'http://www.autopermis.re/upload/5d04d6f419b4e.png'),
(13, 10, 4, 'http://www.autopermis.re/upload/5d04d93d7d910.png'),
(14, 10, 9, 'http://www.autopermis.re/upload/5d04e6f217d3c.png'),
(25, 9, 10, 'http://www.autopermis.re/upload/5d0760f30c91a.png'),
(26, 12, 10, 'http://www.autopermis.re/upload/5d0760f30e7b2.png'),
(27, 11, 11, 'http://www.autopermis.re/upload/5d0787eb198ec.pdf'),
(28, 9, 12, 'http://www.autopermis.re/upload/5d0a6365348d7.jpeg'),
(29, 12, 12, 'http://www.autopermis.re/upload/5d0a636536257.png'),
(30, 9, 12, 'http://www.autopermis.re/upload/5d0a6b6893c9d.jpeg'),
(31, 12, 12, 'http://www.autopermis.re/upload/5d0a6b6896bac.png');

-- --------------------------------------------------------

--
-- Table structure for table `ap_np_customer_file_to_upload`
--

CREATE TABLE `ap_np_customer_file_to_upload` (
  `id` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `id_np_customer_type` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `file_example` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_np_customer_file_to_upload`
--

INSERT INTO `ap_np_customer_file_to_upload` (`id`, `description`, `id_np_customer_type`, `name`, `file_example`) VALUES
(9, 'Merci de bien vouloir nous fournir un scan de votre permis de conduire de + de 5 ans.', 1, 'Permis de conduire (5 ans)', 'http://79.137.76.201/upload/5cfda1eef0fb1.jpg'),
(10, 'Merci de bien vouloir nous fournir l\'attestation de l\'obtention du code (sur votre dossier de préfecture).', 4, '[Dossier préfecture] Attestation d\'obtention du co', 'http://www.autopermis.re/upload/5d04d9149cdaf.png'),
(11, 'Veuillez vous munir de votre livret de formation justifiant des 20 heures de pratique en auto-école.', 5, 'Votre livret de formation ', ''),
(12, 'Merci de bien vouloir imprimer, remplir et signer la charte de l\'accompagnant.', 1, 'Charte de l\'accompagnant', 'http://www.autopermis.re/upload/5d00f8bd37bb9.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `ap_np_customer_point`
--

CREATE TABLE `ap_np_customer_point` (
  `id` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `nb_point` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `weekend` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_np_customer_point`
--

INSERT INTO `ap_np_customer_point` (`id`, `date_add`, `date_end`, `nb_point`, `id_product`, `id_order`, `id_customer`, `weekend`) VALUES
(40, '2019-06-13 18:27:50', '2019-07-13 00:00:00', 4, 9, 48, 2, 0),
(41, '2019-06-15 15:50:03', '2019-07-15 00:00:00', 4, 9, 49, 4, 0),
(42, '2019-06-15 16:45:42', '2019-07-15 00:00:00', 4, 9, 50, 8, 0),
(43, '2019-06-17 16:35:49', '2019-07-17 00:00:00', 5, 18, 52, 10, 0),
(44, '2019-06-17 16:38:50', '2019-07-17 00:00:00', 5, 18, 53, 2, 0),
(45, '2019-06-17 17:00:01', '2019-06-17 00:00:00', 0, 19, 54, 10, 0),
(47, '2019-06-18 14:27:35', '2019-07-18 00:00:00', 1, 19, 57, 2, 0),
(48, '2019-06-18 14:28:39', '2019-07-08 00:00:00', 3, 21, 58, 2, 1),
(49, '2019-06-18 17:44:19', '2019-07-18 00:00:00', 1, 19, 59, 4, 0),
(50, '2019-06-18 17:46:25', '2019-07-18 00:00:00', 5, 18, 60, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_np_customer_type`
--

CREATE TABLE `ap_np_customer_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_np_customer_type`
--

INSERT INTO `ap_np_customer_type` (`id`, `name`) VALUES
(1, 'Accompagnant'),
(4, 'Candidat Libre'),
(5, 'Perfectionnement');

-- --------------------------------------------------------

--
-- Table structure for table `ap_np_product_point`
--

CREATE TABLE `ap_np_product_point` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `nb_day` int(11) NOT NULL,
  `nb_point` int(11) NOT NULL,
  `weekend` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ap_np_product_point`
--

INSERT INTO `ap_np_product_point` (`id`, `id_product`, `nb_day`, `nb_point`, `weekend`) VALUES
(1, 1, 30, 10, '0'),
(2, 19, 30, 1, '0'),
(3, 21, 20, 3, '1'),
(4, 22, 0, 0, '0'),
(5, 23, 30, 10, '1'),
(6, 24, 30, 20, '0'),
(7, 27, 60, 30, '0'),
(8, 28, 0, 0, '0'),
(9, 29, 30, 1, '0'),
(10, 30, 0, 0, '0'),
(11, 31, 0, 0, '0'),
(12, 32, 0, 0, '0'),
(19, 10, 240, 50, '0'),
(20, 9, 30, 4, '0'),
(21, 16, 90, 21, '0'),
(22, 18, 30, 5, '0'),
(23, 11, 180, 20, '0'),
(24, 12, 30, 10, '0'),
(26, 15, 180, 32, '0'),
(27, 13, 240, 54, '0'),
(28, 17, 90, 10, '0'),
(29, 20, 180, 43, '0'),
(30, 25, 180, 50, '1');

-- --------------------------------------------------------

--
-- Table structure for table `ap_operating_system`
--

CREATE TABLE `ap_operating_system` (
  `id_operating_system` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_operating_system`
--

INSERT INTO `ap_operating_system` (`id_operating_system`, `name`) VALUES
(1, 'Windows XP'),
(2, 'Windows Vista'),
(3, 'Windows 7'),
(4, 'Windows 8'),
(5, 'MacOsX'),
(6, 'Linux'),
(7, 'Android');

-- --------------------------------------------------------

--
-- Table structure for table `ap_orders`
--

CREATE TABLE `ap_orders` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `reference` varchar(9) DEFAULT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `current_state` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `payment` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `module` varchar(255) DEFAULT NULL,
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message` text,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_number` varchar(64) DEFAULT NULL,
  `total_discounts` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_real` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `carrier_tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `total_wrapping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `round_mode` tinyint(1) NOT NULL DEFAULT '2',
  `round_type` tinyint(1) NOT NULL DEFAULT '1',
  `invoice_number` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `delivery_number` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `invoice_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  `valid` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_orders`
--

INSERT INTO `ap_orders` (`id_order`, `reference`, `id_shop_group`, `id_shop`, `id_carrier`, `id_lang`, `id_customer`, `id_cart`, `id_currency`, `id_address_delivery`, `id_address_invoice`, `current_state`, `secure_key`, `payment`, `conversion_rate`, `module`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `shipping_number`, `total_discounts`, `total_discounts_tax_incl`, `total_discounts_tax_excl`, `total_paid`, `total_paid_tax_incl`, `total_paid_tax_excl`, `total_paid_real`, `total_products`, `total_products_wt`, `total_shipping`, `total_shipping_tax_incl`, `total_shipping_tax_excl`, `carrier_tax_rate`, `total_wrapping`, `total_wrapping_tax_incl`, `total_wrapping_tax_excl`, `round_mode`, `round_type`, `invoice_number`, `delivery_number`, `invoice_date`, `delivery_date`, `valid`, `date_add`, `date_upd`) VALUES
(1, 'XKBKNABJK', 1, 1, 2, 1, 1, 1, 1, 4, 4, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '55.000000', '55.000000', '55.000000', '0.000000', '53.000000', '53.000000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-08 02:34:25', '2019-06-08 02:34:27'),
(2, 'OHSATSERP', 1, 1, 2, 1, 1, 2, 1, 4, 4, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '75.900000', '75.900000', '75.900000', '0.000000', '73.900000', '73.900000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-08 02:34:25', '2019-06-13 01:59:00'),
(3, 'UOYEVOLI', 1, 1, 2, 1, 1, 3, 1, 4, 4, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '76.010000', '76.010000', '76.010000', '0.000000', '74.010000', '74.010000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-08 02:34:25', '2019-06-13 01:59:00'),
(4, 'FFATNOMMJ', 1, 1, 2, 1, 1, 4, 1, 4, 4, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '89.890000', '89.890000', '89.890000', '0.000000', '87.890000', '87.890000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-08 02:34:25', '2019-06-13 01:59:00'),
(5, 'KHWLILZLL', 1, 1, 2, 1, 1, 5, 1, 4, 4, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Bank wire', '1.000000', 'bankwire', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '71.510000', '71.510000', '71.510000', '0.000000', '69.510000', '69.510000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 1, 0, '2019-06-10 01:22:13', '0000-00-00 00:00:00', 0, '2019-06-08 02:34:25', '2019-06-13 01:59:00'),
(6, 'PNXJYBQLT', 1, 1, 0, 1, 2, 21, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '22.000000', '22.000000', '20.280000', '0.000000', '20.280000', '22.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-12 23:55:16', '2019-06-13 01:59:00'),
(7, 'UDMIFPPJS', 1, 1, 0, 1, 2, 24, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '22.000000', '22.000000', '20.280000', '0.000000', '20.280000', '22.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-12 23:56:13', '2019-06-13 01:59:00'),
(8, 'QBRRYNEKS', 1, 1, 0, 1, 2, 25, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '22.000000', '22.000000', '20.280000', '0.000000', '20.280000', '22.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:02:27', '2019-06-13 01:59:00'),
(9, 'QJWVYNDPN', 1, 1, 0, 1, 2, 26, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:06:26', '2019-06-13 01:59:00'),
(10, 'EZTVNROMD', 1, 1, 0, 1, 2, 27, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '22.000000', '22.000000', '20.280000', '0.000000', '20.280000', '22.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:37:55', '2019-06-13 01:58:59'),
(11, 'UUMWUTRTM', 1, 1, 0, 1, 2, 28, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:39:10', '2019-06-13 01:58:59'),
(12, 'LRYTCHGIQ', 1, 1, 0, 1, 2, 29, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:40:18', '2019-06-13 01:58:59'),
(13, 'VHFFJENBB', 1, 1, 0, 1, 2, 30, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:42:02', '2019-06-13 01:58:59'),
(14, 'QYDEHLHLN', 1, 1, 0, 1, 2, 31, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:43:52', '2019-06-13 01:58:59'),
(15, 'UUEFHGEQH', 1, 1, 0, 1, 2, 32, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:45:30', '2019-06-13 01:58:59'),
(16, 'DTOPZZZFP', 1, 1, 0, 1, 2, 33, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Virement bancaire', '1.000000', 'bankwire', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:47:43', '2019-06-13 01:58:59'),
(17, 'AZCXIDZPJ', 1, 1, 0, 1, 2, 34, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:49:32', '2019-06-13 01:58:59'),
(18, 'KJYPNAQGV', 1, 1, 0, 1, 2, 35, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:51:45', '2019-06-13 01:58:59'),
(19, 'VBDGIKPCM', 1, 1, 0, 1, 2, 36, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:56:11', '2019-06-13 01:58:59'),
(20, 'VMEFCBKCD', 1, 1, 0, 1, 2, 37, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 00:57:36', '2019-06-13 01:58:59'),
(21, 'CARKCOEUB', 1, 1, 0, 1, 2, 38, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:02:09', '2019-06-13 01:58:58'),
(22, 'NGAMMESBM', 1, 1, 0, 1, 2, 39, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:03:51', '2019-06-13 01:58:58'),
(23, 'CJZWPEJLJ', 1, 1, 0, 1, 2, 40, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:05:41', '2019-06-13 01:58:58'),
(24, 'FPLTBYZKN', 1, 1, 0, 1, 2, 41, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:07:17', '2019-06-13 01:58:58'),
(25, 'YWAQQCDSM', 1, 1, 0, 1, 2, 42, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:08:39', '2019-06-13 01:58:58'),
(26, 'BCUNUMVRK', 1, 1, 0, 1, 2, 43, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:11:34', '2019-06-13 01:58:58'),
(27, 'HLETWHIRU', 1, 1, 0, 1, 2, 44, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:12:13', '2019-06-13 01:58:58'),
(28, 'LWRCQOTLJ', 1, 1, 0, 1, 2, 45, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:12:57', '2019-06-13 01:58:58'),
(29, 'NLIXDKOZY', 1, 1, 0, 1, 2, 46, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '198.000000', '198.000000', '182.490000', '0.000000', '182.490000', '198.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:17:37', '2019-06-13 01:58:58'),
(30, 'SWPSXGMJJ', 1, 1, 0, 1, 2, 47, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:18:58', '2019-06-13 01:58:58'),
(31, 'RWXAUGZNT', 1, 1, 0, 1, 2, 48, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:21:49', '2019-06-13 01:58:57'),
(32, 'BFYJGJZAL', 1, 1, 0, 1, 2, 49, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:22:58', '2019-06-13 01:58:57'),
(33, 'MEFYNBLGY', 1, 1, 0, 1, 2, 50, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:24:31', '2019-06-13 01:58:57'),
(34, 'JDCLWWRYC', 1, 1, 0, 1, 2, 51, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:25:18', '2019-06-13 01:58:57'),
(35, 'ZMRHJBMGR', 1, 1, 0, 1, 2, 52, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Virement bancaire', '1.000000', 'bankwire', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:26:29', '2019-06-13 01:58:57'),
(36, 'PLZKWJKYX', 1, 1, 0, 1, 2, 53, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:27:21', '2019-06-13 01:58:57'),
(37, 'AVMZWPUCK', 1, 1, 0, 1, 2, 54, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:28:46', '2019-06-13 01:58:57'),
(38, 'SHVTWVBAG', 1, 1, 0, 1, 2, 55, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:30:37', '2019-06-13 01:58:57'),
(39, 'TIPNEBCZD', 1, 1, 0, 1, 2, 56, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '169.000000', '169.000000', '155.760000', '0.000000', '155.760000', '169.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:34:22', '2019-06-13 01:58:57'),
(40, 'BHOKMYGFC', 1, 1, 0, 1, 2, 57, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Virement bancaire', '1.000000', 'bankwire', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:36:40', '2019-06-13 01:58:57'),
(41, 'TKJQJRSLP', 1, 1, 0, 1, 2, 58, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:38:09', '2019-06-13 01:58:56'),
(42, 'CSBUDSWLD', 1, 1, 0, 1, 2, 59, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:39:47', '2019-06-13 01:58:56'),
(43, 'BYBMJATKZ', 1, 1, 0, 1, 2, 60, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '198.000000', '198.000000', '182.490000', '0.000000', '182.490000', '198.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:40:57', '2019-06-13 01:58:56'),
(44, 'GGZBNCWXF', 1, 1, 0, 1, 2, 61, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '198.000000', '198.000000', '182.490000', '0.000000', '182.490000', '198.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:41:54', '2019-06-13 01:58:56'),
(45, 'GCVYLKNMO', 1, 1, 0, 1, 2, 62, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '198.000000', '198.000000', '182.490000', '0.000000', '182.490000', '198.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:43:32', '2019-06-13 01:58:56'),
(46, 'OQTBRCEOP', 1, 1, 0, 1, 2, 63, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '0.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-06-13 01:45:43', '2019-06-13 01:58:56'),
(47, 'RMXXHAUIJ', 1, 1, 0, 1, 2, 64, 1, 5, 5, 6, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 2, 0, '2019-06-13 01:48:04', '0000-00-00 00:00:00', 0, '2019-06-13 01:47:34', '2019-06-13 01:58:56'),
(48, 'GXLGXDWUU', 1, 1, 0, 1, 2, 65, 1, 5, 5, 9, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 3, 0, '2019-06-13 18:27:50', '0000-00-00 00:00:00', 0, '2019-06-13 18:27:26', '2019-06-13 18:27:50'),
(49, 'CREFISIGD', 1, 1, 0, 1, 4, 66, 1, 6, 6, 9, '45b5535bdd60260f369a080845ab3e53', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 4, 0, '2019-06-15 15:50:03', '0000-00-00 00:00:00', 0, '2019-06-15 15:48:32', '2019-06-15 15:50:03'),
(50, 'OALQWXSKZ', 1, 1, 0, 1, 8, 67, 1, 7, 7, 9, '279eac2003c29a0db8f359010ba1dbbc', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 5, 0, '2019-06-15 16:45:42', '0000-00-00 00:00:00', 0, '2019-06-15 16:45:27', '2019-06-15 16:45:42'),
(51, 'TWXPKXFNL', 1, 1, 0, 1, 2, 69, 1, 5, 5, 9, '0e3a71ce07c72faacb7058801be0f267', 'Virement bancaire', '1.000000', 'bankwire', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 12, 0, '2019-06-18 14:23:25', '0000-00-00 00:00:00', 0, '2019-06-16 12:47:48', '2019-06-18 14:23:25'),
(52, 'IFPVYRRJO', 1, 1, 0, 1, 10, 70, 1, 8, 8, 9, 'dc24e49fb4eec8c177eee411ac48b021', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 6, 0, '2019-06-17 16:35:49', '0000-00-00 00:00:00', 0, '2019-06-17 16:35:32', '2019-06-17 16:35:49'),
(53, 'KICDSEUNK', 1, 1, 0, 1, 2, 72, 1, 5, 5, 9, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 7, 0, '2019-06-17 16:38:50', '0000-00-00 00:00:00', 0, '2019-06-17 16:38:38', '2019-06-17 16:38:50'),
(54, 'XMBPNEYOQ', 1, 1, 0, 1, 10, 73, 1, 8, 8, 2, 'dc24e49fb4eec8c177eee411ac48b021', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '22.000000', '22.000000', '20.280000', '22.000000', '20.280000', '22.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 8, 0, '2019-06-17 17:00:01', '0000-00-00 00:00:00', 1, '2019-06-17 16:58:57', '2019-06-17 17:00:02'),
(55, 'ZQAUHYZOL', 1, 1, 0, 1, 2, 74, 1, 5, 5, 9, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '100.000000', '100.000000', '92.170000', '100.000000', '92.170000', '100.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 9, 0, '2019-06-18 13:03:53', '0000-00-00 00:00:00', 0, '2019-06-18 12:33:10', '2019-06-18 13:03:53'),
(56, 'PBVLYMDEQ', 1, 1, 0, 1, 2, 75, 1, 5, 5, 9, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '200.000000', '200.000000', '184.330000', '200.000000', '184.330000', '200.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 10, 0, '2019-06-18 13:06:06', '0000-00-00 00:00:00', 0, '2019-06-18 13:05:35', '2019-06-18 14:21:45'),
(57, 'USQPMIMBI', 1, 1, 0, 1, 2, 76, 1, 5, 5, 2, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '22.000000', '22.000000', '20.280000', '22.000000', '20.280000', '22.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 11, 0, '2019-06-18 13:07:41', '0000-00-00 00:00:00', 1, '2019-06-18 13:07:16', '2019-06-18 14:27:35'),
(58, 'RQFXJBUVY', 1, 1, 0, 1, 2, 77, 1, 5, 5, 9, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '100.000000', '100.000000', '92.170000', '100.000000', '92.170000', '100.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 13, 0, '2019-06-18 14:28:39', '0000-00-00 00:00:00', 0, '2019-06-18 14:28:29', '2019-06-18 14:28:39'),
(59, 'UHFNULNGZ', 1, 1, 0, 1, 4, 78, 1, 6, 6, 9, '45b5535bdd60260f369a080845ab3e53', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '22.000000', '22.000000', '20.280000', '22.000000', '20.280000', '22.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 14, 0, '2019-06-18 17:44:19', '0000-00-00 00:00:00', 0, '2019-06-18 17:40:43', '2019-06-18 17:44:19'),
(60, 'SEEKQPPHO', 1, 1, 0, 1, 2, 79, 1, 5, 5, 9, '0e3a71ce07c72faacb7058801be0f267', 'Chèque', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '99.000000', '99.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', '0.000000', '0.000', '0.000000', '0.000000', '0.000000', 2, 2, 15, 0, '2019-06-18 17:46:25', '0000-00-00 00:00:00', 0, '2019-06-18 17:46:07', '2019-06-18 17:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_carrier`
--

CREATE TABLE `ap_order_carrier` (
  `id_order_carrier` int(11) NOT NULL,
  `id_order` int(11) UNSIGNED NOT NULL,
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_order_invoice` int(11) UNSIGNED DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_excl` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_incl` decimal(20,6) DEFAULT NULL,
  `tracking_number` varchar(64) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_carrier`
--

INSERT INTO `ap_order_carrier` (`id_order_carrier`, `id_order`, `id_carrier`, `id_order_invoice`, `weight`, `shipping_cost_tax_excl`, `shipping_cost_tax_incl`, `tracking_number`, `date_add`) VALUES
(1, 1, 2, 0, '0.000000', '2.000000', '2.000000', '', '2019-06-08 02:34:27'),
(2, 2, 2, 0, '0.000000', '2.000000', '2.000000', '', '2019-06-08 02:34:27'),
(3, 3, 2, 0, '0.000000', '2.000000', '2.000000', '', '2019-06-08 02:34:27'),
(4, 4, 2, 0, '0.000000', '2.000000', '2.000000', '', '2019-06-08 02:34:27'),
(5, 5, 2, 1, '0.000000', '2.000000', '2.000000', '', '2019-06-08 02:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_cart_rule`
--

CREATE TABLE `ap_order_cart_rule` (
  `id_order_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_order_invoice` int(10) UNSIGNED DEFAULT '0',
  `name` varchar(254) NOT NULL,
  `value` decimal(17,2) NOT NULL DEFAULT '0.00',
  `value_tax_excl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_detail`
--

CREATE TABLE `ap_order_detail` (
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_order_invoice` int(11) DEFAULT NULL,
  `id_warehouse` int(10) UNSIGNED DEFAULT '0',
  `id_shop` int(11) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_in_stock` int(10) NOT NULL DEFAULT '0',
  `product_quantity_refunded` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_return` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_reinjected` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `group_reduction` decimal(10,2) NOT NULL DEFAULT '0.00',
  `product_quantity_discount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `product_ean13` varchar(13) DEFAULT NULL,
  `product_upc` varchar(12) DEFAULT NULL,
  `product_reference` varchar(32) DEFAULT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_weight` decimal(20,6) NOT NULL,
  `id_tax_rules_group` int(11) UNSIGNED DEFAULT '0',
  `tax_computation_method` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `tax_name` varchar(16) NOT NULL,
  `tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `ecotax` decimal(21,6) NOT NULL DEFAULT '0.000000',
  `ecotax_tax_rate` decimal(5,3) NOT NULL DEFAULT '0.000',
  `discount_quantity_applied` tinyint(1) NOT NULL DEFAULT '0',
  `download_hash` varchar(255) DEFAULT NULL,
  `download_nb` int(10) UNSIGNED DEFAULT '0',
  `download_deadline` datetime DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `purchase_supplier_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `original_product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `original_wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_detail`
--

INSERT INTO `ap_order_detail` (`id_order_detail`, `id_order`, `id_order_invoice`, `id_warehouse`, `id_shop`, `product_id`, `product_attribute_id`, `product_name`, `product_quantity`, `product_quantity_in_stock`, `product_quantity_refunded`, `product_quantity_return`, `product_quantity_reinjected`, `product_price`, `reduction_percent`, `reduction_amount`, `reduction_amount_tax_incl`, `reduction_amount_tax_excl`, `group_reduction`, `product_quantity_discount`, `product_ean13`, `product_upc`, `product_reference`, `product_supplier_reference`, `product_weight`, `id_tax_rules_group`, `tax_computation_method`, `tax_name`, `tax_rate`, `ecotax`, `ecotax_tax_rate`, `discount_quantity_applied`, `download_hash`, `download_nb`, `download_deadline`, `total_price_tax_incl`, `total_price_tax_excl`, `unit_price_tax_incl`, `unit_price_tax_excl`, `total_shipping_price_tax_incl`, `total_shipping_price_tax_excl`, `purchase_supplier_price`, `original_product_price`, `original_wholesale_price`) VALUES
(1, 1, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, '26.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_2', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '27.000000', '27.000000', '27.000000', '27.000000', '0.000000', '0.000000', '0.000000', '26.999852', '8.100000'),
(2, 1, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, '25.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_3', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '26.000000', '26.000000', '26.000000', '26.000000', '0.000000', '0.000000', '0.000000', '25.999852', '7.800000'),
(3, 2, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, '26.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_2', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '27.000000', '27.000000', '27.000000', '27.000000', '0.000000', '0.000000', '0.000000', '26.999852', '8.100000'),
(4, 2, 0, 0, 1, 6, 32, 'Printed Summer Dress - Color : Yellow, Size : M', 1, 1, 0, 0, 0, '30.502569', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_6', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '30.500000', '30.500000', '30.500000', '30.500000', '0.000000', '0.000000', '0.000000', '30.502569', '9.150000'),
(5, 2, 0, 0, 1, 7, 34, 'Printed Chiffon Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, '20.501236', '20.00', '0.000000', '0.000000', '0.000000', '0.00', '17.400000', '', '', 'demo_7', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.400000', '16.400000', '16.400000', '16.400000', '0.000000', '0.000000', '0.000000', '20.501236', '6.150000'),
(6, 3, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, '16.510000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_1', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.510000', '16.510000', '16.510000', '16.510000', '0.000000', '0.000000', '0.000000', '16.510000', '4.950000'),
(7, 3, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, '26.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_2', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '27.000000', '27.000000', '27.000000', '27.000000', '0.000000', '0.000000', '0.000000', '26.999852', '8.100000'),
(8, 3, 0, 0, 1, 6, 32, 'Printed Summer Dress - Color : Yellow, Size : M', 1, 1, 0, 0, 0, '30.502569', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_6', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '30.500000', '30.500000', '30.500000', '30.500000', '0.000000', '0.000000', '0.000000', '30.502569', '9.150000'),
(9, 4, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, '16.510000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_1', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.510000', '16.510000', '16.510000', '16.510000', '0.000000', '0.000000', '0.000000', '16.510000', '4.950000'),
(10, 4, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, '25.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_3', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '26.000000', '26.000000', '26.000000', '26.000000', '0.000000', '0.000000', '0.000000', '25.999852', '7.800000'),
(11, 4, 0, 0, 1, 5, 19, 'Printed Summer Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, '30.506321', '5.00', '0.000000', '0.000000', '0.000000', '0.00', '29.980000', '', '', 'demo_5', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '28.980000', '28.980000', '28.980000', '28.980000', '0.000000', '0.000000', '0.000000', '30.506321', '9.150000'),
(12, 4, 0, 0, 1, 7, 34, 'Printed Chiffon Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, '20.501236', '20.00', '0.000000', '0.000000', '0.000000', '0.00', '17.400000', '', '', 'demo_7', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.400000', '16.400000', '16.400000', '16.400000', '0.000000', '0.000000', '0.000000', '20.501236', '6.150000'),
(13, 5, 1, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, '16.510000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_1', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.510000', '16.510000', '16.510000', '16.510000', '0.000000', '0.000000', '0.000000', '16.510000', '4.950000'),
(14, 5, 1, 0, 1, 2, 7, 'Blouse - Color : Black, Size : S', 1, 1, 0, 0, 0, '26.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_2', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '27.000000', '27.000000', '27.000000', '27.000000', '0.000000', '0.000000', '0.000000', '26.999852', '8.100000'),
(15, 5, 1, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, '25.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_3', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '26.000000', '26.000000', '26.000000', '26.000000', '0.000000', '0.000000', '0.000000', '25.999852', '7.800000'),
(16, 6, 0, 0, 1, 19, 0, 'FORFAIT LIBRE 1 HEURE', 1, 0, 0, 0, 0, '20.276498', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '22.000000', '20.280000', '22.000000', '20.276498', '0.000000', '0.000000', '0.000000', '20.276498', '0.000000'),
(17, 7, 0, 0, 1, 19, 0, 'FORFAIT LIBRE 1 HEURE', 1, -1, 0, 0, 0, '20.276498', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '22.000000', '20.280000', '22.000000', '20.276498', '0.000000', '0.000000', '0.000000', '20.276498', '0.000000'),
(18, 8, 0, 0, 1, 19, 0, 'FORFAIT LIBRE 1 HEURE', 1, -2, 0, 0, 0, '20.276498', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '22.000000', '20.280000', '22.000000', '20.276498', '0.000000', '0.000000', '0.000000', '20.276498', '0.000000'),
(19, 9, 0, 0, 1, 18, 0, 'FORFAIT LIBRE 5 HEURES', 1, 0, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(20, 10, 0, 0, 1, 19, 0, 'FORFAIT LIBRE 1 HEURE', 1, -3, 0, 0, 0, '20.276498', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '22.000000', '20.280000', '22.000000', '20.276498', '0.000000', '0.000000', '0.000000', '20.276498', '0.000000'),
(21, 11, 0, 0, 1, 18, 0, 'FORFAIT LIBRE 5 HEURES', 1, -1, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(22, 12, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, 0, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(23, 13, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -1, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(24, 14, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -2, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(25, 15, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -3, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(26, 16, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -4, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(27, 17, 0, 0, 1, 18, 0, 'FORFAIT LIBRE 5 HEURES', 1, -2, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(28, 18, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -5, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(29, 19, 0, 0, 1, 18, 0, 'FORFAIT LIBRE 5 HEURES', 1, -3, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(30, 20, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -6, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(31, 21, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -7, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(32, 22, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -8, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(33, 23, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -9, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(34, 24, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -10, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(35, 25, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -11, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(36, 26, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -12, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(37, 27, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -13, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(38, 28, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -14, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(39, 29, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 2, -15, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '198.000000', '182.490000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(40, 30, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -17, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(41, 31, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -18, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(42, 32, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -19, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(43, 33, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -20, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(44, 34, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -21, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(45, 35, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -22, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(46, 36, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -23, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(47, 37, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -24, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(48, 38, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -25, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(49, 39, 0, 0, 1, 17, 0, 'FORFAIT LIBRE 10 HEURES', 1, 0, 0, 0, 0, '155.760369', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '169.000000', '155.760000', '169.000000', '155.760369', '0.000000', '0.000000', '0.000000', '155.760369', '0.000000'),
(50, 40, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -26, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(51, 41, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -27, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(52, 42, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -28, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(53, 43, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 2, -29, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '198.000000', '182.490000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(54, 44, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 2, -31, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '198.000000', '182.490000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(55, 45, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 2, -33, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '198.000000', '182.490000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(56, 46, 0, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -35, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(57, 47, 2, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -36, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(58, 48, 3, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -2, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(59, 49, 4, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -3, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(60, 50, 5, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -4, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(61, 51, 12, 0, 1, 9, 0, 'FORFAIT EXAMEN CANDIDAT LIBRE', 1, -5, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(62, 52, 6, 0, 1, 18, 0, 'FORFAIT LIBRE 5 HEURES', 1, 0, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(63, 53, 7, 0, 1, 18, 0, 'FORFAIT LIBRE 5 HEURES', 1, -1, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000'),
(64, 54, 8, 0, 1, 19, 0, 'FORFAIT LIBRE 1 HEURE', 1, 0, 0, 0, 0, '20.276498', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '22.000000', '20.280000', '22.000000', '20.276498', '0.000000', '0.000000', '0.000000', '20.276498', '0.000000'),
(65, 55, 9, 0, 1, 21, 0, 'ProduitCarlTest', 1, 0, 0, 0, 0, '92.165899', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '100.000000', '92.170000', '100.000000', '92.165899', '0.000000', '0.000000', '0.000000', '92.165899', '0.000000'),
(66, 56, 10, 0, 1, 21, 0, 'ProduitCarlTest', 2, -1, 0, 0, 0, '92.165899', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '200.000000', '184.330000', '100.000000', '92.165899', '0.000000', '0.000000', '0.000000', '92.165899', '0.000000'),
(67, 57, 11, 0, 1, 19, 0, 'FORFAIT LIBRE 1 HEURE', 1, -1, 0, 0, 0, '20.276498', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '22.000000', '20.280000', '22.000000', '20.276498', '0.000000', '0.000000', '0.000000', '20.276498', '0.000000'),
(68, 58, 13, 0, 1, 21, 0, 'ProduitCarlTest', 1, -3, 0, 0, 0, '92.165899', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '100.000000', '92.170000', '100.000000', '92.165899', '0.000000', '0.000000', '0.000000', '92.165899', '0.000000'),
(69, 59, 14, 0, 1, 19, 0, 'FORFAIT LIBRE 1 HEURE', 1, -2, 0, 0, 0, '20.276498', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '22.000000', '20.280000', '22.000000', '20.276498', '0.000000', '0.000000', '0.000000', '20.276498', '0.000000'),
(70, 60, 15, 0, 1, 18, 0, 'FORFAIT LIBRE 5 HEURES', 1, -2, 0, 0, 0, '91.244240', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '0.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '99.000000', '91.240000', '99.000000', '91.244240', '0.000000', '0.000000', '0.000000', '91.244240', '0.000000');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_detail_tax`
--

CREATE TABLE `ap_order_detail_tax` (
  `id_order_detail` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_amount` decimal(16,6) NOT NULL DEFAULT '0.000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_detail_tax`
--

INSERT INTO `ap_order_detail_tax` (`id_order_detail`, `id_tax`, `unit_amount`, `total_amount`) VALUES
(16, 1, '1.723502', '1.720000'),
(17, 1, '1.723502', '1.720000'),
(18, 1, '1.723502', '1.720000'),
(19, 1, '7.755760', '7.760000'),
(20, 1, '1.723502', '1.720000'),
(21, 1, '7.755760', '7.760000'),
(22, 1, '7.755760', '7.760000'),
(23, 1, '7.755760', '7.760000'),
(24, 1, '7.755760', '7.760000'),
(25, 1, '7.755760', '7.760000'),
(26, 1, '7.755760', '7.760000'),
(27, 1, '7.755760', '7.760000'),
(28, 1, '7.755760', '7.760000'),
(29, 1, '7.755760', '7.760000'),
(30, 1, '7.755760', '7.760000'),
(31, 1, '7.755760', '7.760000'),
(32, 1, '7.755760', '7.760000'),
(33, 1, '7.755760', '7.760000'),
(34, 1, '7.755760', '7.760000'),
(35, 1, '7.755760', '7.760000'),
(36, 1, '7.755760', '7.760000'),
(37, 1, '7.755760', '7.760000'),
(38, 1, '7.755760', '7.760000'),
(39, 1, '7.755760', '15.510000'),
(40, 1, '7.755760', '7.760000'),
(41, 1, '7.755760', '7.760000'),
(42, 1, '7.755760', '7.760000'),
(43, 1, '7.755760', '7.760000'),
(44, 1, '7.755760', '7.760000'),
(45, 1, '7.755760', '7.760000'),
(46, 1, '7.755760', '7.760000'),
(47, 1, '7.755760', '7.760000'),
(48, 1, '7.755760', '7.760000'),
(49, 1, '13.239631', '13.240000'),
(50, 1, '7.755760', '7.760000'),
(51, 1, '7.755760', '7.760000'),
(52, 1, '7.755760', '7.760000'),
(53, 1, '7.755760', '15.510000'),
(54, 1, '7.755760', '15.510000'),
(55, 1, '7.755760', '15.510000'),
(56, 1, '7.755760', '7.760000'),
(57, 1, '7.755760', '7.760000'),
(58, 1, '7.755760', '7.760000'),
(59, 1, '7.755760', '7.760000'),
(60, 1, '7.755760', '7.760000'),
(61, 1, '7.755760', '7.760000'),
(62, 1, '7.755760', '7.760000'),
(63, 1, '7.755760', '7.760000'),
(64, 1, '1.723502', '1.720000'),
(65, 1, '7.834101', '7.830000'),
(66, 1, '7.834101', '15.670000'),
(67, 1, '1.723502', '1.720000'),
(68, 1, '7.834101', '7.830000'),
(69, 1, '1.723502', '1.720000'),
(70, 1, '7.755760', '7.760000');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_history`
--

CREATE TABLE `ap_order_history` (
  `id_order_history` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_history`
--

INSERT INTO `ap_order_history` (`id_order_history`, `id_employee`, `id_order`, `id_order_state`, `date_add`) VALUES
(1, 0, 1, 1, '2019-06-08 02:34:27'),
(2, 0, 2, 1, '2019-06-08 02:34:27'),
(3, 0, 3, 1, '2019-06-08 02:34:27'),
(4, 0, 4, 1, '2019-06-08 02:34:27'),
(5, 0, 5, 10, '2019-06-08 02:34:27'),
(6, 1, 1, 6, '2019-06-08 02:34:27'),
(7, 1, 3, 8, '2019-06-08 02:34:27'),
(8, 0, 6, 1, '2019-06-12 23:55:16'),
(9, 0, 6, 13, '2019-06-12 23:55:16'),
(10, 0, 7, 1, '2019-06-12 23:56:13'),
(11, 0, 7, 13, '2019-06-12 23:56:13'),
(12, 0, 8, 1, '2019-06-13 00:02:27'),
(13, 0, 8, 13, '2019-06-13 00:02:27'),
(14, 0, 9, 1, '2019-06-13 00:06:26'),
(15, 0, 9, 13, '2019-06-13 00:06:27'),
(16, 0, 10, 1, '2019-06-13 00:37:56'),
(17, 0, 10, 13, '2019-06-13 00:37:56'),
(18, 0, 11, 1, '2019-06-13 00:39:10'),
(19, 0, 11, 13, '2019-06-13 00:39:10'),
(20, 0, 12, 1, '2019-06-13 00:40:18'),
(21, 0, 12, 13, '2019-06-13 00:40:19'),
(22, 0, 13, 1, '2019-06-13 00:42:02'),
(23, 0, 13, 13, '2019-06-13 00:42:02'),
(24, 0, 14, 1, '2019-06-13 00:43:52'),
(25, 0, 14, 13, '2019-06-13 00:43:53'),
(26, 0, 15, 1, '2019-06-13 00:45:30'),
(27, 0, 15, 13, '2019-06-13 00:45:30'),
(28, 0, 16, 10, '2019-06-13 00:47:43'),
(29, 0, 16, 13, '2019-06-13 00:47:44'),
(30, 0, 17, 1, '2019-06-13 00:49:32'),
(31, 0, 17, 13, '2019-06-13 00:49:32'),
(32, 0, 18, 1, '2019-06-13 00:51:45'),
(33, 0, 18, 13, '2019-06-13 00:51:45'),
(34, 0, 19, 1, '2019-06-13 00:56:11'),
(35, 0, 19, 13, '2019-06-13 00:56:11'),
(36, 0, 20, 1, '2019-06-13 00:57:36'),
(37, 0, 20, 13, '2019-06-13 00:57:36'),
(38, 0, 21, 1, '2019-06-13 01:02:09'),
(39, 0, 21, 13, '2019-06-13 01:02:09'),
(40, 0, 22, 1, '2019-06-13 01:03:52'),
(41, 0, 22, 13, '2019-06-13 01:03:52'),
(42, 0, 23, 1, '2019-06-13 01:05:41'),
(43, 0, 23, 13, '2019-06-13 01:05:42'),
(44, 0, 24, 1, '2019-06-13 01:07:18'),
(45, 0, 24, 13, '2019-06-13 01:07:18'),
(46, 0, 25, 1, '2019-06-13 01:08:39'),
(47, 0, 25, 13, '2019-06-13 01:08:39'),
(48, 0, 26, 1, '2019-06-13 01:11:34'),
(49, 0, 26, 13, '2019-06-13 01:11:34'),
(50, 0, 27, 1, '2019-06-13 01:12:13'),
(51, 0, 27, 13, '2019-06-13 01:12:14'),
(52, 0, 28, 1, '2019-06-13 01:12:57'),
(53, 0, 28, 13, '2019-06-13 01:12:57'),
(54, 0, 29, 1, '2019-06-13 01:17:37'),
(55, 0, 29, 13, '2019-06-13 01:17:37'),
(56, 0, 30, 1, '2019-06-13 01:18:58'),
(57, 0, 30, 13, '2019-06-13 01:18:58'),
(58, 0, 31, 1, '2019-06-13 01:21:49'),
(59, 0, 31, 13, '2019-06-13 01:21:50'),
(60, 0, 32, 1, '2019-06-13 01:22:58'),
(61, 0, 32, 13, '2019-06-13 01:22:58'),
(62, 0, 33, 1, '2019-06-13 01:24:31'),
(63, 0, 33, 13, '2019-06-13 01:24:31'),
(64, 0, 34, 1, '2019-06-13 01:25:18'),
(65, 0, 34, 13, '2019-06-13 01:25:19'),
(66, 0, 35, 10, '2019-06-13 01:26:29'),
(67, 0, 35, 13, '2019-06-13 01:26:30'),
(68, 0, 36, 1, '2019-06-13 01:27:21'),
(69, 0, 36, 13, '2019-06-13 01:27:21'),
(70, 0, 38, 1, '2019-06-13 01:30:37'),
(71, 0, 38, 13, '2019-06-13 01:30:37'),
(72, 0, 39, 1, '2019-06-13 01:34:22'),
(73, 0, 39, 13, '2019-06-13 01:34:22'),
(74, 0, 40, 10, '2019-06-13 01:36:40'),
(75, 0, 40, 13, '2019-06-13 01:36:41'),
(76, 0, 41, 1, '2019-06-13 01:38:09'),
(77, 0, 41, 13, '2019-06-13 01:38:09'),
(78, 0, 42, 1, '2019-06-13 01:39:47'),
(79, 0, 42, 13, '2019-06-13 01:39:47'),
(80, 0, 43, 1, '2019-06-13 01:40:57'),
(81, 0, 43, 13, '2019-06-13 01:40:57'),
(82, 0, 44, 1, '2019-06-13 01:41:54'),
(83, 0, 44, 13, '2019-06-13 01:41:55'),
(84, 0, 45, 1, '2019-06-13 01:43:32'),
(85, 0, 45, 13, '2019-06-13 01:43:32'),
(86, 0, 47, 1, '2019-06-13 01:47:35'),
(87, 0, 47, 13, '2019-06-13 01:47:35'),
(88, 1, 47, 2, '2019-06-13 01:48:04'),
(89, 1, 47, 9, '2019-06-13 01:48:32'),
(90, 1, 47, 2, '2019-06-13 01:54:16'),
(91, 1, 47, 12, '2019-06-13 01:54:50'),
(92, 1, 47, 2, '2019-06-13 01:56:49'),
(93, 1, 47, 6, '2019-06-13 01:58:56'),
(94, 1, 46, 6, '2019-06-13 01:58:56'),
(95, 1, 45, 6, '2019-06-13 01:58:56'),
(96, 1, 44, 6, '2019-06-13 01:58:56'),
(97, 1, 43, 6, '2019-06-13 01:58:56'),
(98, 1, 42, 6, '2019-06-13 01:58:56'),
(99, 1, 41, 6, '2019-06-13 01:58:56'),
(100, 1, 40, 6, '2019-06-13 01:58:57'),
(101, 1, 39, 6, '2019-06-13 01:58:57'),
(102, 1, 38, 6, '2019-06-13 01:58:57'),
(103, 1, 37, 6, '2019-06-13 01:58:57'),
(104, 1, 36, 6, '2019-06-13 01:58:57'),
(105, 1, 35, 6, '2019-06-13 01:58:57'),
(106, 1, 34, 6, '2019-06-13 01:58:57'),
(107, 1, 33, 6, '2019-06-13 01:58:57'),
(108, 1, 32, 6, '2019-06-13 01:58:57'),
(109, 1, 31, 6, '2019-06-13 01:58:57'),
(110, 1, 30, 6, '2019-06-13 01:58:58'),
(111, 1, 29, 6, '2019-06-13 01:58:58'),
(112, 1, 28, 6, '2019-06-13 01:58:58'),
(113, 1, 27, 6, '2019-06-13 01:58:58'),
(114, 1, 26, 6, '2019-06-13 01:58:58'),
(115, 1, 25, 6, '2019-06-13 01:58:58'),
(116, 1, 24, 6, '2019-06-13 01:58:58'),
(117, 1, 23, 6, '2019-06-13 01:58:58'),
(118, 1, 22, 6, '2019-06-13 01:58:58'),
(119, 1, 21, 6, '2019-06-13 01:58:58'),
(120, 1, 20, 6, '2019-06-13 01:58:58'),
(121, 1, 19, 6, '2019-06-13 01:58:59'),
(122, 1, 18, 6, '2019-06-13 01:58:59'),
(123, 1, 17, 6, '2019-06-13 01:58:59'),
(124, 1, 16, 6, '2019-06-13 01:58:59'),
(125, 1, 15, 6, '2019-06-13 01:58:59'),
(126, 1, 14, 6, '2019-06-13 01:58:59'),
(127, 1, 13, 6, '2019-06-13 01:58:59'),
(128, 1, 12, 6, '2019-06-13 01:58:59'),
(129, 1, 11, 6, '2019-06-13 01:58:59'),
(130, 1, 10, 6, '2019-06-13 01:58:59'),
(131, 1, 9, 6, '2019-06-13 01:59:00'),
(132, 1, 8, 6, '2019-06-13 01:59:00'),
(133, 1, 7, 6, '2019-06-13 01:59:00'),
(134, 1, 6, 6, '2019-06-13 01:59:00'),
(135, 1, 5, 6, '2019-06-13 01:59:00'),
(136, 1, 4, 6, '2019-06-13 01:59:00'),
(137, 1, 3, 6, '2019-06-13 01:59:00'),
(138, 1, 2, 6, '2019-06-13 01:59:00'),
(139, 0, 48, 1, '2019-06-13 18:27:26'),
(140, 0, 48, 13, '2019-06-13 18:27:26'),
(141, 1, 48, 9, '2019-06-13 18:27:50'),
(142, 0, 49, 1, '2019-06-15 15:48:32'),
(143, 0, 49, 13, '2019-06-15 15:48:32'),
(144, 1, 49, 9, '2019-06-15 15:50:03'),
(145, 0, 50, 1, '2019-06-15 16:45:27'),
(146, 0, 50, 13, '2019-06-15 16:45:27'),
(147, 1, 50, 9, '2019-06-15 16:45:42'),
(148, 0, 51, 10, '2019-06-16 12:47:48'),
(149, 0, 51, 13, '2019-06-16 12:47:48'),
(150, 0, 52, 1, '2019-06-17 16:35:32'),
(151, 0, 52, 13, '2019-06-17 16:35:32'),
(152, 1, 52, 9, '2019-06-17 16:35:49'),
(153, 0, 53, 1, '2019-06-17 16:38:39'),
(154, 0, 53, 13, '2019-06-17 16:38:39'),
(155, 1, 53, 9, '2019-06-17 16:38:50'),
(156, 0, 54, 1, '2019-06-17 16:58:58'),
(157, 0, 54, 13, '2019-06-17 16:58:58'),
(158, 2, 54, 2, '2019-06-17 17:00:02'),
(159, 0, 55, 1, '2019-06-18 12:33:10'),
(160, 0, 55, 13, '2019-06-18 12:33:11'),
(161, 1, 55, 9, '2019-06-18 13:03:53'),
(162, 0, 56, 1, '2019-06-18 13:05:35'),
(163, 0, 56, 13, '2019-06-18 13:05:35'),
(164, 1, 56, 9, '2019-06-18 13:06:06'),
(165, 0, 57, 1, '2019-06-18 13:07:16'),
(166, 0, 57, 13, '2019-06-18 13:07:16'),
(167, 1, 57, 2, '2019-06-18 13:07:42'),
(168, 1, 56, 2, '2019-06-18 14:19:49'),
(169, 1, 56, 9, '2019-06-18 14:20:38'),
(170, 1, 56, 15, '2019-06-18 14:21:36'),
(171, 1, 56, 9, '2019-06-18 14:21:45'),
(172, 1, 51, 9, '2019-06-18 14:23:25'),
(173, 1, 57, 9, '2019-06-18 14:26:39'),
(174, 1, 57, 2, '2019-06-18 14:27:35'),
(175, 0, 58, 1, '2019-06-18 14:28:29'),
(176, 0, 58, 13, '2019-06-18 14:28:29'),
(177, 1, 58, 9, '2019-06-18 14:28:39'),
(178, 0, 59, 1, '2019-06-18 17:40:43'),
(179, 0, 59, 13, '2019-06-18 17:40:43'),
(180, 2, 59, 9, '2019-06-18 17:44:19'),
(181, 0, 60, 1, '2019-06-18 17:46:07'),
(182, 0, 60, 13, '2019-06-18 17:46:07'),
(183, 1, 60, 9, '2019-06-18 17:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_invoice`
--

CREATE TABLE `ap_order_invoice` (
  `id_order_invoice` int(11) UNSIGNED NOT NULL,
  `id_order` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `delivery_number` int(11) NOT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `total_discount_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discount_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `shipping_tax_computation_method` int(10) UNSIGNED NOT NULL,
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `shop_address` text,
  `invoice_address` text,
  `delivery_address` text,
  `note` text,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_invoice`
--

INSERT INTO `ap_order_invoice` (`id_order_invoice`, `id_order`, `number`, `delivery_number`, `delivery_date`, `total_discount_tax_excl`, `total_discount_tax_incl`, `total_paid_tax_excl`, `total_paid_tax_incl`, `total_products`, `total_products_wt`, `total_shipping_tax_excl`, `total_shipping_tax_incl`, `shipping_tax_computation_method`, `total_wrapping_tax_excl`, `total_wrapping_tax_incl`, `shop_address`, `invoice_address`, `delivery_address`, `note`, `date_add`) VALUES
(1, 5, 1, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '71.510000', '71.510000', '69.510000', '69.510000', '2.000000', '2.000000', 0, '0.000000', '0.000000', 'AutoPermis', '', '', '', '2019-06-10 01:22:13'),
(2, 47, 2, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-13 01:48:04'),
(3, 48, 3, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-13 18:27:50'),
(4, 49, 4, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-15 15:50:03'),
(5, 50, 5, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-15 16:45:42'),
(6, 52, 6, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-17 16:35:49'),
(7, 53, 7, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-17 16:38:50'),
(8, 54, 8, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '20.280000', '22.000000', '20.280000', '22.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-17 17:00:01'),
(9, 55, 9, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '92.170000', '100.000000', '92.170000', '100.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-18 13:03:53'),
(10, 56, 10, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '184.330000', '200.000000', '184.330000', '200.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-18 13:06:06'),
(11, 57, 11, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '20.280000', '22.000000', '20.280000', '22.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-18 13:07:41'),
(12, 51, 12, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-18 14:23:25'),
(13, 58, 13, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '92.170000', '100.000000', '92.170000', '100.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-18 14:28:39'),
(14, 59, 14, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '20.280000', '22.000000', '20.280000', '22.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-18 17:44:19'),
(15, 60, 15, 0, '0000-00-00 00:00:00', '0.000000', '0.000000', '91.240000', '99.000000', '91.240000', '99.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'AutoPermis<br />68 rue de suffren<br />Réunion, Île de la', '', '', '', '2019-06-18 17:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_invoice_payment`
--

CREATE TABLE `ap_order_invoice_payment` (
  `id_order_invoice` int(11) UNSIGNED NOT NULL,
  `id_order_payment` int(11) UNSIGNED NOT NULL,
  `id_order` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_invoice_payment`
--

INSERT INTO `ap_order_invoice_payment` (`id_order_invoice`, `id_order_payment`, `id_order`) VALUES
(2, 1, 47),
(3, 2, 48),
(4, 3, 49),
(5, 4, 50),
(12, 11, 51),
(6, 5, 52),
(7, 6, 53),
(8, 7, 54),
(9, 8, 55),
(10, 9, 56),
(11, 10, 57),
(13, 12, 58),
(14, 13, 59),
(15, 14, 60);

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_invoice_tax`
--

CREATE TABLE `ap_order_invoice_tax` (
  `id_order_invoice` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `amount` decimal(10,6) NOT NULL DEFAULT '0.000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_message`
--

CREATE TABLE `ap_order_message` (
  `id_order_message` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_message`
--

INSERT INTO `ap_order_message` (`id_order_message`, `date_add`) VALUES
(1, '2019-06-08 02:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_message_lang`
--

CREATE TABLE `ap_order_message_lang` (
  `id_order_message` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_message_lang`
--

INSERT INTO `ap_order_message_lang` (`id_order_message`, `id_lang`, `name`, `message`) VALUES
(1, 1, 'Retard', 'Bonjour,\n\nMalheureusement, un article que vous avez commandé est actuellement en rupture de stock. Pour cette raison, il est possible que la livraison de votre commande soit légèrement retardée.\nNous vous prions de bien vouloir accepter nos excuses. Nous faisons tout notre possible pour remédier à cette situation.\n\nCordialement,');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_payment`
--

CREATE TABLE `ap_order_payment` (
  `id_order_payment` int(11) NOT NULL,
  `order_reference` varchar(9) DEFAULT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `transaction_id` varchar(254) DEFAULT NULL,
  `card_number` varchar(254) DEFAULT NULL,
  `card_brand` varchar(254) DEFAULT NULL,
  `card_expiration` char(7) DEFAULT NULL,
  `card_holder` varchar(254) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_payment`
--

INSERT INTO `ap_order_payment` (`id_order_payment`, `order_reference`, `id_currency`, `amount`, `payment_method`, `conversion_rate`, `transaction_id`, `card_number`, `card_brand`, `card_expiration`, `card_holder`, `date_add`) VALUES
(1, 'RMXXHAUIJ', 1, '99.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-13 01:48:04'),
(2, 'GXLGXDWUU', 1, '99.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-13 18:27:50'),
(3, 'CREFISIGD', 1, '99.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-15 15:50:03'),
(4, 'OALQWXSKZ', 1, '99.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-15 16:45:42'),
(5, 'IFPVYRRJO', 1, '99.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-17 16:35:49'),
(6, 'KICDSEUNK', 1, '99.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-17 16:38:50'),
(7, 'XMBPNEYOQ', 1, '22.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-17 17:00:02'),
(8, 'ZQAUHYZOL', 1, '100.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-18 13:03:53'),
(9, 'PBVLYMDEQ', 1, '200.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-18 13:06:06'),
(10, 'USQPMIMBI', 1, '22.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-18 13:07:42'),
(11, 'TWXPKXFNL', 1, '99.00', 'Virement bancaire', '1.000000', '', '', '', '', '', '2019-06-18 14:23:25'),
(12, 'RQFXJBUVY', 1, '100.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-18 14:28:39'),
(13, 'UHFNULNGZ', 1, '22.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-18 17:44:19'),
(14, 'SEEKQPPHO', 1, '99.00', 'Chèque', '1.000000', '', '', '', '', '', '2019-06-18 17:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_return`
--

CREATE TABLE `ap_order_return` (
  `id_order_return` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `question` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_return_detail`
--

CREATE TABLE `ap_order_return_detail` (
  `id_order_return` int(10) UNSIGNED NOT NULL,
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `id_customization` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_return_state`
--

CREATE TABLE `ap_order_return_state` (
  `id_order_return_state` int(10) UNSIGNED NOT NULL,
  `color` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_return_state`
--

INSERT INTO `ap_order_return_state` (`id_order_return_state`, `color`) VALUES
(1, '#4169E1'),
(2, '#8A2BE2'),
(3, '#32CD32'),
(4, '#DC143C'),
(5, '#108510');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_return_state_lang`
--

CREATE TABLE `ap_order_return_state_lang` (
  `id_order_return_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_return_state_lang`
--

INSERT INTO `ap_order_return_state_lang` (`id_order_return_state`, `id_lang`, `name`) VALUES
(1, 1, 'En attente de confirmation'),
(2, 1, 'En attente du colis'),
(3, 1, 'Colis reçu'),
(4, 1, 'Retour refusé'),
(5, 1, 'Retour terminé');

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_slip`
--

CREATE TABLE `ap_order_slip` (
  `id_order_slip` int(10) UNSIGNED NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `total_products_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_products_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_incl` decimal(20,6) DEFAULT NULL,
  `shipping_cost` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `amount` decimal(10,2) NOT NULL,
  `shipping_cost_amount` decimal(10,2) NOT NULL,
  `partial` tinyint(1) NOT NULL,
  `order_slip_type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_slip_detail`
--

CREATE TABLE `ap_order_slip_detail` (
  `id_order_slip` int(10) UNSIGNED NOT NULL,
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `unit_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `unit_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `amount_tax_excl` decimal(20,6) DEFAULT NULL,
  `amount_tax_incl` decimal(20,6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_slip_detail_tax`
--

CREATE TABLE `ap_order_slip_detail_tax` (
  `id_order_slip_detail` int(11) UNSIGNED NOT NULL,
  `id_tax` int(11) UNSIGNED NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_amount` decimal(16,6) NOT NULL DEFAULT '0.000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_state`
--

CREATE TABLE `ap_order_state` (
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `invoice` tinyint(1) UNSIGNED DEFAULT '0',
  `send_email` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `module_name` varchar(255) DEFAULT NULL,
  `color` varchar(32) DEFAULT NULL,
  `unremovable` tinyint(1) UNSIGNED NOT NULL,
  `hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `logable` tinyint(1) NOT NULL DEFAULT '0',
  `delivery` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipped` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `paid` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `pdf_invoice` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `pdf_delivery` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_state`
--

INSERT INTO `ap_order_state` (`id_order_state`, `invoice`, `send_email`, `module_name`, `color`, `unremovable`, `hidden`, `logable`, `delivery`, `shipped`, `paid`, `pdf_invoice`, `pdf_delivery`, `deleted`) VALUES
(1, 0, 1, 'cheque', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 1, 1, '', '#32CD32', 1, 0, 1, 0, 0, 1, 1, 0, 0),
(3, 1, 1, '', '#FF8C00', 1, 0, 1, 1, 0, 1, 0, 0, 0),
(4, 1, 1, '', '#8A2BE2', 1, 0, 1, 1, 1, 1, 0, 0, 0),
(5, 1, 0, '', '#108510', 1, 0, 1, 1, 1, 1, 0, 0, 0),
(6, 0, 1, '', '#DC143C', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 1, 1, '', '#ec2e15', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 0, 1, '', '#8f0621', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 1, 1, '', '#FF69B4', 1, 0, 0, 0, 0, 1, 0, 0, 0),
(10, 0, 1, 'bankwire', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 0, 0, '', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 1, 1, '', '#32CD32', 1, 0, 1, 0, 0, 1, 0, 0, 0),
(13, 0, 1, '', '#FF69B4', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 0, 0, 'cashondelivery', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 1, 0, '', '#DDEEFF', 0, 0, 1, 0, 0, 0, 0, 0, 0),
(16, 1, 0, '', '#4169E1', 0, 0, 1, 0, 0, 0, 0, 0, 0),
(17, 0, 0, '', '#4169E1', 0, 0, 1, 0, 0, 0, 0, 0, 0),
(18, 0, 0, '', '#DDEEFF', 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_order_state_lang`
--

CREATE TABLE `ap_order_state_lang` (
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `template` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_order_state_lang`
--

INSERT INTO `ap_order_state_lang` (`id_order_state`, `id_lang`, `name`, `template`) VALUES
(1, 1, 'En attente de paiement par chèque', 'cheque'),
(2, 1, 'Paiement accepté', 'payment'),
(3, 1, 'En cours de préparation', 'preparation'),
(4, 1, 'Expédié', 'shipped'),
(5, 1, 'Livré', ''),
(6, 1, 'Annulé', 'order_canceled'),
(7, 1, 'Remboursé', 'refund'),
(8, 1, 'Erreur de paiement', 'payment_error'),
(9, 1, 'En attente de réapprovisionnement (payé)', 'outofstock'),
(10, 1, 'En attente de virement bancaire', 'bankwire'),
(11, 1, 'En attente de paiement PayPal', ''),
(12, 1, 'Paiement à distance accepté', 'payment'),
(13, 1, 'En attente de réapprovisionnement (non payé)', 'outofstock'),
(14, 1, 'En attente de paiement à la livraison', 'cashondelivery'),
(15, 1, 'Autorisation acceptée par PayPal', ''),
(16, 1, 'Autorisation acceptée par Braintree', ''),
(17, 1, 'En attente de paiement Braintree', ''),
(18, 1, 'En attente de confirmation par PayPal', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_pack`
--

CREATE TABLE `ap_pack` (
  `id_product_pack` int(10) UNSIGNED NOT NULL,
  `id_product_item` int(10) UNSIGNED NOT NULL,
  `id_product_attribute_item` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_page`
--

CREATE TABLE `ap_page` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `id_page_type` int(10) UNSIGNED NOT NULL,
  `id_object` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_page`
--

INSERT INTO `ap_page` (`id_page`, `id_page_type`, `id_object`) VALUES
(1, 1, NULL),
(2, 2, NULL),
(3, 3, 37),
(4, 4, NULL),
(5, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_pagenotfound`
--

CREATE TABLE `ap_pagenotfound` (
  `id_pagenotfound` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `request_uri` varchar(256) NOT NULL,
  `http_referer` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_pagenotfound`
--

INSERT INTO `ap_pagenotfound` (`id_pagenotfound`, `id_shop`, `id_shop_group`, `request_uri`, `http_referer`, `date_add`) VALUES
(1, 1, 1, '/index.php?fc=module&module=npcalendar&controller=default', 'http://www.autopermis.re/index.php', '2019-06-17 14:44:03');

-- --------------------------------------------------------

--
-- Table structure for table `ap_page_type`
--

CREATE TABLE `ap_page_type` (
  `id_page_type` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_page_type`
--

INSERT INTO `ap_page_type` (`id_page_type`, `name`) VALUES
(2, 'authentication'),
(5, 'cms'),
(1, 'index'),
(3, 'product'),
(4, 'search');

-- --------------------------------------------------------

--
-- Table structure for table `ap_page_viewed`
--

CREATE TABLE `ap_page_viewed` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_date_range` int(10) UNSIGNED NOT NULL,
  `counter` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_paypal_braintree`
--

CREATE TABLE `ap_paypal_braintree` (
  `id_paypal_braintree` int(11) NOT NULL,
  `id_cart` int(11) NOT NULL,
  `nonce_payment_token` varchar(255) NOT NULL,
  `client_token` text NOT NULL,
  `transaction` varchar(255) DEFAULT NULL,
  `datas` varchar(255) DEFAULT NULL,
  `id_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_paypal_capture`
--

CREATE TABLE `ap_paypal_capture` (
  `id_paypal_capture` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `capture_amount` float NOT NULL,
  `result` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_paypal_customer`
--

CREATE TABLE `ap_paypal_customer` (
  `id_paypal_customer` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `paypal_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_paypal_hss_email_error`
--

CREATE TABLE `ap_paypal_hss_email_error` (
  `id_paypal_hss_email_error` int(11) NOT NULL,
  `id_cart` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_paypal_login_user`
--

CREATE TABLE `ap_paypal_login_user` (
  `id_paypal_login_user` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `token_type` varchar(255) NOT NULL,
  `expires_in` varchar(255) NOT NULL,
  `refresh_token` varchar(255) NOT NULL,
  `id_token` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `account_type` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `verified_account` varchar(255) NOT NULL,
  `zoneinfo` varchar(255) NOT NULL,
  `age_range` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_paypal_order`
--

CREATE TABLE `ap_paypal_order` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_transaction` varchar(255) NOT NULL,
  `id_invoice` varchar(255) DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `total_paid` varchar(50) NOT NULL,
  `shipping` varchar(50) NOT NULL,
  `capture` int(2) NOT NULL,
  `payment_date` varchar(50) NOT NULL,
  `payment_method` int(2) UNSIGNED NOT NULL,
  `payment_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_paypal_plus_pui`
--

CREATE TABLE `ap_paypal_plus_pui` (
  `id_paypal_plus_pui` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `pui_informations` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_product`
--

CREATE TABLE `ap_product` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_supplier` int(10) UNSIGNED DEFAULT NULL,
  `id_manufacturer` int(10) UNSIGNED DEFAULT NULL,
  `id_category_default` int(10) UNSIGNED DEFAULT NULL,
  `id_shop_default` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `on_sale` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `online_only` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT '0.00',
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `width` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `height` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `depth` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `out_of_stock` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `quantity_discount` tinyint(1) DEFAULT '0',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `redirect_type` enum('','404','301','302') NOT NULL DEFAULT '',
  `id_product_redirected` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `available_date` date NOT NULL DEFAULT '0000-00-00',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_is_pack` tinyint(1) NOT NULL DEFAULT '0',
  `cache_has_attachments` tinyint(1) NOT NULL DEFAULT '0',
  `is_virtual` tinyint(1) NOT NULL DEFAULT '0',
  `cache_default_attribute` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT '0',
  `pack_stock_type` int(11) UNSIGNED NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product`
--

INSERT INTO `ap_product` (`id_product`, `id_supplier`, `id_manufacturer`, `id_category_default`, `id_shop_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ean13`, `upc`, `ecotax`, `quantity`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `reference`, `supplier_reference`, `location`, `width`, `height`, `depth`, `weight`, `out_of_stock`, `quantity_discount`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_is_pack`, `cache_has_attachments`, `is_virtual`, `cache_default_attribute`, `date_add`, `date_upd`, `advanced_stock_management`, `pack_stock_type`) VALUES
(9, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '91.244240', '0.000000', 'heure', '4.000186', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 1, 0, '2019-06-08 18:29:49', '2019-06-18 20:04:26', 0, 3),
(10, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '459.907834', '0.000000', 'heure', '50.044378', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 1, 0, '2019-06-08 18:29:49', '2019-06-18 22:36:59', 0, 3),
(11, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '247.926267', '0.000000', 'heure', '19.994054', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 1, 0, '2019-06-08 18:29:49', '2019-06-19 15:48:30', 0, 3),
(12, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '137.327189', '0.000000', '', '10.001980', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 1, 0, '2019-06-08 18:29:49', '2019-06-18 22:39:59', 0, 3),
(13, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '497.695853', '0.000000', 'heure', '50.019684', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 49, '2019-06-08 18:29:49', '2019-06-19 13:42:58', 0, 3),
(15, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '367.741935', '0.000000', 'heure', '30.019750', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 52, '2019-06-08 18:29:49', '2019-06-19 13:19:10', 0, 3),
(16, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '275.576037', '0.000000', 'heure', '20.012784', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 54, '2019-06-08 18:29:49', '2019-06-19 13:48:01', 0, 3),
(17, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '155.760369', '0.000000', 'heure', '10.003877', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 56, '2019-06-08 18:29:49', '2019-06-19 13:21:04', 0, 3),
(18, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '91.244240', '0.000000', 'heure', '5.002425', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 58, '2019-06-08 18:29:49', '2019-06-19 13:22:40', 0, 3),
(19, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '20.276478', '0.000000', 'heure', '1.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 60, '2019-06-08 18:29:49', '2019-06-19 13:24:07', 0, 3),
(20, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '451.612903', '0.000000', 'heure', '40.001143', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 50, '2019-06-15 16:30:19', '2019-06-19 13:15:42', 0, 3),
(21, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '92.165899', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 1, 0, '2019-06-18 12:30:45', '2019-06-18 12:32:39', 0, 3),
(22, 0, 0, 2, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '100.000000', '100.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 46, '2019-06-19 12:37:19', '2019-06-19 12:46:19', 0, 3),
(23, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '137.327189', '0.000000', 'heure', '10.001980', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 62, '2019-06-19 15:31:24', '2019-06-19 20:47:40', 0, 3),
(24, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '247.926267', '0.000000', 'heure', '20.010191', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 64, '2019-06-19 15:45:01', '2019-06-19 20:53:30', 0, 3),
(25, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '496.774194', '0.000000', 'heure', '49.977283', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '0.000000', 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 66, '2019-06-19 15:55:52', '2019-06-19 20:53:45', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_attachment`
--

CREATE TABLE `ap_product_attachment` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_attachment` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_attribute`
--

CREATE TABLE `ap_product_attribute` (
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `default_on` tinyint(1) UNSIGNED DEFAULT NULL,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `available_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product_attribute`
--

INSERT INTO `ap_product_attribute` (`id_product_attribute`, `id_product`, `reference`, `supplier_reference`, `location`, `ean13`, `upc`, `wholesale_price`, `price`, `ecotax`, `quantity`, `weight`, `unit_price_impact`, `default_on`, `minimal_quantity`, `available_date`) VALUES
(46, 22, '', '', '', '', '', '0.000000', '1.843318', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(47, 22, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(48, 13, '', '', '', '', '', '0.000000', '92.165899', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(49, 13, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(50, 20, '', '', '', '', '', '0.000000', '73.732719', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(51, 20, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(52, 15, '', '', '', '', '', '0.000000', '55.299539', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(53, 15, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(54, 16, '', '', '', '', '', '0.000000', '36.866359', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(55, 16, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(56, 17, '', '', '', '', '', '0.000000', '18.433180', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(57, 17, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(58, 18, '', '', '', '', '', '0.000000', '9.216590', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(59, 18, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(60, 19, '', '', '', '', '', '0.000000', '1.843318', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(61, 19, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(62, 23, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(63, 23, '', '', '', '', '', '0.000000', '18.433180', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(64, 24, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(65, 24, '', '', '', '', '', '0.000000', '36.866359', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(66, 25, '', '', '', '', '', '0.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 1, 1, '0000-00-00'),
(67, 25, '', '', '', '', '', '0.000000', '92.165899', '0.000000', 0, '0.000000', '0.000000', NULL, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_attribute_combination`
--

CREATE TABLE `ap_product_attribute_combination` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product_attribute_combination`
--

INSERT INTO `ap_product_attribute_combination` (`id_attribute`, `id_product_attribute`) VALUES
(25, 46),
(26, 47),
(25, 48),
(26, 49),
(25, 50),
(26, 51),
(25, 52),
(26, 53),
(25, 54),
(26, 55),
(25, 56),
(26, 57),
(25, 58),
(26, 59),
(25, 60),
(26, 61),
(26, 62),
(25, 63),
(26, 64),
(25, 65),
(26, 66),
(25, 67);

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_attribute_image`
--

CREATE TABLE `ap_product_attribute_image` (
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_image` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_attribute_shop`
--

CREATE TABLE `ap_product_attribute_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `default_on` tinyint(1) UNSIGNED DEFAULT NULL,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `available_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product_attribute_shop`
--

INSERT INTO `ap_product_attribute_shop` (`id_product`, `id_product_attribute`, `id_shop`, `wholesale_price`, `price`, `ecotax`, `weight`, `unit_price_impact`, `default_on`, `minimal_quantity`, `available_date`) VALUES
(22, 46, 1, '0.000000', '1.843318', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(22, 47, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(13, 48, 1, '0.000000', '92.165899', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(13, 49, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(20, 50, 1, '0.000000', '73.732719', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(20, 51, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(15, 52, 1, '0.000000', '55.299539', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(15, 53, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(16, 54, 1, '0.000000', '36.866359', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(16, 55, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(17, 56, 1, '0.000000', '18.433180', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(17, 57, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(18, 58, 1, '0.000000', '9.216590', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(18, 59, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(19, 60, 1, '0.000000', '1.843318', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(19, 61, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(23, 62, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(23, 63, 1, '0.000000', '18.433180', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(24, 64, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(24, 65, 1, '0.000000', '36.866359', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00'),
(25, 66, 1, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', 1, 1, '0000-00-00'),
(25, 67, 1, '0.000000', '92.165899', '0.000000', '0.000000', '0.000000', NULL, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_carrier`
--

CREATE TABLE `ap_product_carrier` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_carrier_reference` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product_carrier`
--

INSERT INTO `ap_product_carrier` (`id_product`, `id_carrier_reference`, `id_shop`) VALUES
(13, 3, 1),
(16, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_country_tax`
--

CREATE TABLE `ap_product_country_tax` (
  `id_product` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_download`
--

CREATE TABLE `ap_product_download` (
  `id_product_download` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `display_filename` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_expiration` datetime DEFAULT NULL,
  `nb_days_accessible` int(10) UNSIGNED DEFAULT NULL,
  `nb_downloadable` int(10) UNSIGNED DEFAULT '1',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `is_shareable` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_group_reduction_cache`
--

CREATE TABLE `ap_product_group_reduction_cache` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(4,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_lang`
--

CREATE TABLE `ap_product_lang` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `description_short` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `available_now` varchar(255) DEFAULT NULL,
  `available_later` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product_lang`
--

INSERT INTO `ap_product_lang` (`id_product`, `id_shop`, `id_lang`, `description`, `description_short`, `link_rewrite`, `meta_description`, `meta_keywords`, `meta_title`, `name`, `available_now`, `available_later`) VALUES
(9, 1, 1, '<h1>Forfait EXAMEN. </h1>\n<h2>4 heures de location - carburant compris - assurance tout risque.</h2>\n<p>Votre date d\'examen pratique approche? pensez à réserver votre forfait examen pour vous assurer</p>\n<p>d\'avoir le véhicule à double commande le jour J.</p>', '<h1><strong>FORFAIT EXAMEN CANDIDAT LIBRE</strong></h1>', 'forfait-examen-candidat-libre', '', '', '', 'FORFAIT EXAMEN CANDIDAT LIBRE', '', ''),
(10, 1, 1, '<h1><span>FORFAIT JOURNEE - Semaine - 50 heures ( 5 jours du lundi au vendredi)</span></h1>\n<h2><span>Validité du forfait : 7 jours</span></h2>\n<h3>           . Location 1 semaine </h3>\n<h3>           . 499 €</h3>\n<h3>           . 800 km</h3>\n<h3>           . Kilométrage illimité :  +100 €</h3>\n<p>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</p>\n<h2><strong>WEEK END ET JOUR FERIE : 539 €</strong></h2>\n<h3>Idéal pour une formation accélérée en toute tranquillité.</h3>', '<h1><strong> </strong></h1>\n<p></p>', 'forfait-week-end-et-ferie-50-heures', '', '', '', 'FORFAIT JOURNEE - \"Semaine\" 50 HEURES', '', ''),
(11, 1, 1, '<ul><li>\n<h2>FORFAIT  SEMAINE  2 jours  - 20 heures (du lundi au vendredi)</h2>\n</li>\n<li>\n<h3>229 €</h3>\n</li>\n<li>\n<h3>350 kms</h3>\n</li>\n<li>\n<h3>Validité du Forfait : 3 MOIS</h3>\n</li>\n<li>\n<h3>Kilomètrage illimité +40 euros</h3>\n</li>\n</ul><ul><li>\n<h3>kilométre supplementaire :  0.60 €</h3>\n</li>\n</ul>', '<h1></h1>', 'forfait-week-end-et-ferie-20-heures', '', '', '', 'FORFAIT  JOURNEE - \"2 jours\"  -  20 HEURES', '', ''),
(12, 1, 1, '<h2>Forfait JOURNEE - 1 JOUR : 10 heures (du lundi au vendredi)</h2>\n<h2>Validité du forfait : 1 jour.</h2>\n<ul><li>\n<h2>Location 1 jour </h2>\n</li>\n<li>\n<h3>129 €</h3>\n</li>\n<li>\n<h3>200 kms</h3>\n</li>\n<li>\n<h3>kilométrage illimité + 20 €</h3>\n</li>\n<li>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</li>\n<li>\n<h3>Week end et jour férié 149 €                                                                                                                        Le forfait pratique pour prendre le temps de s\'exercer en toute tranquillité et sans contrainte de temps.</h3>\n</li>\n</ul>', '<h1></h1>', 'forfait-week-end-et-ferie-10-heures', '', '', '', 'FORFAIT JOURNEE - 1 JOUR  : 10 HEURES', '', ''),
(13, 1, 1, '<h1>FORFAIT 50 HEURES </h1>\n<h2><em>+ 4 HEURES OFFERTES (OFFRE DE LANCEMENT)</em></h2>\n<h3>Validité du forfait : 8 mois </h3>\n<h3>540 €</h3>\n<h3>1000 km</h3>\n<h3>kilométrage illimité : + 100 €</h3>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>', '<h1>heures fractionnables - CARBURANT OFFERT</h1>', 'forfait-50-heures', '', '', '', 'FORFAIT LIBRE 50 HEURES + 4 heures offertes', '', ''),
(15, 1, 1, '<h1>FORFAIT LIBRE - 30 HEURES</h1>\n<h2><em>+ 2 heures offertes (offre de lancement)</em></h2>\n<h3><em>Validité du forfait : 6 mois</em></h3>\n<h3>399 €</h3>\n<h3>600 km</h3>\n<h3>kilométrage illimité : + 60 €</h3>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>\n<p></p>\n<p></p>', '<h1>HEURES FRACTIONNABLES - CARBURANT OFFERT</h1>', 'forfait-30-heures', 'forfait libre 30 heures', '', '', 'FORFAIT LIBRE 30 HEURES + deux heures offertes', '', ''),
(16, 1, 1, '<h1><strong>FORFAIT  20 HEURES</strong></h1>\n<h2><em><strong>+ 1 heure offerte (offre de lancement)</strong></em></h2>\n<h3><b>Validité du forfait : 6 mois</b></h3>\n<h3><b>299 €</b></h3>\n<h3><b>400 kilométres</b></h3>\n<h3><b>kilométrage illimité : + 40 euros</b></h3>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>', '<h1><strong>HEURES FRACTIONNABLES - </strong>CARBURANT OFFERT</h1>', 'forfait-20-heures', '', '', '', 'FORFAIT  LIBRE  20 HEURES + une heure offerte', '', ''),
(17, 1, 1, '<h1><strong>FORFAIT LIBRE - 10 HEURES</strong></h1>\n<h2><strong>Carburant offert<em></em></strong></h2>\n<h3><b>Validité du forfait : 3 mois</b></h3>\n<h3><b>169 €</b></h3>\n<h3><b>200 km</b></h3>\n<h3><b>kilométrage illimité + 20 €</b></h3>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>', '<h2><strong>HEURES FRACTIONNABLES - CARBURANT OFFERT</strong></h2>', 'forfait-libre-10-heures', '', '', '', 'FORFAIT LIBRE 10 HEURES', '', ''),
(18, 1, 1, '<h1><strong>FORFAIT LIBRE - 5 HEURES</strong></h1>\n<h2><em>Carburant offert</em></h2>\n<h3><strong>Validité du forfait : 1 mois</strong></h3>\n<h3><strong>99 €</strong></h3>\n<h3><strong>100 km</strong></h3>\n<h3><strong>Kilométrage illimité + 10 €</strong></h3>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>', '<h2><strong>HEURES FRACTIONNABLES - CARBURANT OFFERT</strong></h2>', 'forfait-libre-5-heures', '', '', '', 'FORFAIT LIBRE 5 HEURES', '', ''),
(19, 1, 1, '<h1><strong>FORFAIT LIBRE - 1 HEURE</strong></h1>\n<h2><em><strong>Carburant offert</strong></em></h2>\n<h3><b>Validité du forfait : 1 mois</b></h3>\n<h3>22 €</h3>\n<h3>20 km</h3>\n<h3>kilométrage illimité + 2 €</h3>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>', '<h2>HEURES FRACTIONNABLES - CARBURANT OFFERT</h2>\n<p></p>', 'forfait-libre-1-heure', '', '', '', 'FORFAIT LIBRE 1 HEURE', '', ''),
(20, 1, 1, '<h1><strong>FORFAIT LIBRE 40 HEURES</strong></h1>\n<h2><em><b>+ 3 heures offertes (offre de lancement)</b></em></h2>\n<h3><b>Validité du forfait : 6 mois</b></h3>\n<h3><b>490 €</b></h3>\n<h3><b>800 kilomètres</b></h3>\n<h3><b>kilométrage illimité : + 80 euros</b></h3>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>', '<h2>HEURES FRACTIONNABLES - CARBURANT OFFERT</h2>', 'forfait-40-heures', '', '', '', 'FORFAIT LIBRE 40 HEURES + 3 heures offertes', '', ''),
(21, 1, 1, '', '<p>Delete on prod mode</p>', 'produitcarltest', '', '', '', 'ProduitCarlTest', '', ''),
(22, 1, 1, '', '', 'test2', '', '', '', 'Test2', '', ''),
(23, 1, 1, '<h2>Forfait WEEK-END - 1 JOUR : 10 heures (du lundi au dimanche)</h2>\n<h2>Validité du forfait : 1 mois.</h2>\n<ul><li>\n<h2>Location 1 jour </h2>\n</li>\n<li>\n<h3>149 €</h3>\n</li>\n<li>\n<h3>200 kms</h3>\n</li>\n<li>\n<h3>kilométrage illimité + 20 €</h3>\n</li>\n<li>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>\n</li>\n</ul>', '', 'forfait-journee-1-jour-10-heures-semaines-week-ends-et-jours-feries', '', '', '', 'FORFAIT WEEK-END - 1 JOUR - 10 HEURES (Semaines, week-ends et jours fériés)', '', ''),
(24, 1, 1, '<ul><li>\n<h2>FORFAIT WEEK END - 2 jours - 20 heures (du lundi au dimanche)</h2>\n</li>\n<li>\n<h3>269 €</h3>\n</li>\n<li>\n<h3>350 kms</h3>\n</li>\n<li>\n<h3>Validité du Forfait : 3 MOIS</h3>\n</li>\n<li>\n<h3>Kilomètrage illimité + 40 euros</h3>\n</li>\n</ul><ul><li>\n<h3>0.60 € par kilomètre supplémentaire pour les forfaits hors illimité.</h3>\n</li>\n<li>\n<h2>Forfait 2 jours (Semaine, Week end et jour férié) : Le forfait idéal pour les personnes n\'ayant pas de temps pendant la semaine et qui permet de s\'exercer en continu sur des routes moins denses.</h2>\n</li>\n</ul>', '', 'forfait-journee-2-jours-20-heures-semaine-week-end-et-jours-feries', '', '', '', 'FORFAIT JOURNEE -  2 JOURS - 20 HEURES (semaine, week-end et jours fériés)', '', ''),
(25, 1, 1, '<h1><span>FORFAIT JOURNEE -WEEK-END - 50 Heures (du lundi au dimanche)</span></h1>\n<h2><span>Validité du forfait : 7 jours</span></h2>\n<h3>           . 6 mois</h3>\n<h3>           . 539 €</h3>\n<h3>           . 800 km</h3>\n<h3>           . Kilométrage illimité :  +100 €</h3>\n<h3>0.60 euros du kilomètre supplémentaire pour les forfaits hors kilométrage illimité.</h3>\n<h2><strong>FORFAIT WEEK END ET JOUR FERIE : </strong></h2>\n<h3>Idéal pour une formation accélérée en toute tranquillité.</h3>', '', 'forfait-week-end-50-heures-semaine-week-end-et-jours-feries', '', '', '', 'FORFAIT  -WEEK-END - 50 Heures (semaine, week-end et jours fériés)', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_sale`
--

CREATE TABLE `ap_product_sale` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sale_nbr` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_upd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product_sale`
--

INSERT INTO `ap_product_sale` (`id_product`, `quantity`, `sale_nbr`, `date_upd`) VALUES
(19, 2, 2, '2019-06-18');

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_shop`
--

CREATE TABLE `ap_product_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `id_category_default` int(10) UNSIGNED DEFAULT NULL,
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `on_sale` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `online_only` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT '0.00',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `redirect_type` enum('','404','301','302') NOT NULL DEFAULT '',
  `id_product_redirected` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `available_date` date NOT NULL DEFAULT '0000-00-00',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_default_attribute` int(10) UNSIGNED DEFAULT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `pack_stock_type` int(11) UNSIGNED NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product_shop`
--

INSERT INTO `ap_product_shop` (`id_product`, `id_shop`, `id_category_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ecotax`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_default_attribute`, `advanced_stock_management`, `date_add`, `date_upd`, `pack_stock_type`) VALUES
(9, 1, 12, 1, 0, 0, '0.000000', 1, '91.244240', '0.000000', 'heure', '4.000186', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2019-06-08 18:29:49', '2019-06-18 20:04:26', 3),
(10, 1, 12, 1, 0, 0, '0.000000', 1, '459.907834', '0.000000', 'heure', '50.044378', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2019-06-08 18:29:49', '2019-06-18 22:36:59', 3),
(11, 1, 12, 1, 0, 0, '0.000000', 1, '247.926267', '0.000000', 'heure', '19.994054', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2019-06-08 18:29:49', '2019-06-19 15:48:30', 3),
(12, 1, 12, 1, 0, 0, '0.000000', 1, '137.327189', '0.000000', '', '10.001980', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2019-06-08 18:29:49', '2019-06-18 22:39:59', 3),
(13, 1, 12, 1, 0, 0, '0.000000', 1, '497.695853', '0.000000', 'heure', '50.019684', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 49, 0, '2019-06-08 18:29:49', '2019-06-19 13:42:58', 3),
(15, 1, 12, 1, 0, 0, '0.000000', 1, '367.741935', '0.000000', 'heure', '30.019750', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 52, 0, '2019-06-08 18:29:49', '2019-06-19 13:19:10', 3),
(16, 1, 12, 1, 0, 0, '0.000000', 1, '275.576037', '0.000000', 'heure', '20.012784', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 54, 0, '2019-06-08 18:29:49', '2019-06-19 13:48:01', 3),
(17, 1, 12, 1, 0, 0, '0.000000', 1, '155.760369', '0.000000', 'heure', '10.003877', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 56, 0, '2019-06-08 18:29:49', '2019-06-19 13:21:04', 3),
(18, 1, 12, 1, 0, 0, '0.000000', 1, '91.244240', '0.000000', 'heure', '5.002425', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 58, 0, '2019-06-08 18:29:49', '2019-06-19 13:22:40', 3),
(19, 1, 12, 1, 0, 0, '0.000000', 1, '20.276478', '0.000000', 'heure', '1.000000', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 60, 0, '2019-06-08 18:29:49', '2019-06-19 13:24:07', 3),
(20, 1, 12, 1, 0, 0, '0.000000', 1, '451.612903', '0.000000', 'heure', '40.001143', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 50, 0, '2019-06-15 16:30:19', '2019-06-19 13:15:42', 3),
(21, 1, 12, 1, 0, 0, '0.000000', 1, '92.165899', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2019-06-18 12:30:45', '2019-06-18 12:32:39', 3),
(22, 1, 2, 1, 0, 0, '0.000000', 1, '100.000000', '100.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 46, 0, '2019-06-19 12:37:19', '2019-06-19 12:46:19', 3),
(23, 1, 12, 1, 0, 0, '0.000000', 1, '137.327189', '0.000000', 'heure', '10.001980', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 62, 0, '2019-06-19 15:31:24', '2019-06-19 20:47:40', 3),
(24, 1, 12, 1, 0, 0, '0.000000', 1, '247.926267', '0.000000', 'heure', '20.010191', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 64, 0, '2019-06-19 15:45:01', '2019-06-19 20:53:30', 3),
(25, 1, 12, 1, 0, 0, '0.000000', 1, '496.774194', '0.000000', 'heure', '49.977283', '0.00', 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 66, 0, '2019-06-19 15:55:52', '2019-06-19 20:53:45', 3);

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_supplier`
--

CREATE TABLE `ap_product_supplier` (
  `id_product_supplier` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_supplier_price_te` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `id_currency` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_product_tag`
--

CREATE TABLE `ap_product_tag` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_tag` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_product_tag`
--

INSERT INTO `ap_product_tag` (`id_product`, `id_tag`, `id_lang`) VALUES
(13, 26, 1),
(18, 26, 1),
(19, 26, 1),
(25, 26, 1),
(10, 29, 1),
(11, 29, 1),
(12, 29, 1),
(13, 29, 1),
(18, 29, 1),
(19, 29, 1),
(10, 31, 1),
(11, 31, 1),
(12, 31, 1),
(13, 31, 1),
(18, 31, 1),
(19, 31, 1),
(25, 31, 1),
(10, 32, 1),
(11, 32, 1),
(12, 32, 1),
(13, 32, 1),
(18, 32, 1),
(19, 32, 1),
(23, 32, 1),
(24, 32, 1),
(25, 32, 1),
(10, 33, 1),
(11, 33, 1),
(12, 33, 1),
(13, 33, 1),
(18, 33, 1),
(19, 33, 1),
(23, 33, 1),
(24, 33, 1),
(25, 33, 1),
(10, 34, 1),
(11, 34, 1),
(12, 34, 1),
(13, 34, 1),
(18, 34, 1),
(24, 34, 1),
(25, 34, 1),
(10, 36, 1),
(11, 36, 1),
(12, 36, 1),
(18, 36, 1),
(19, 36, 1),
(23, 36, 1),
(24, 36, 1),
(25, 36, 1),
(10, 41, 1),
(11, 41, 1),
(18, 41, 1),
(19, 41, 1),
(23, 41, 1),
(24, 41, 1),
(25, 41, 1),
(10, 43, 1),
(12, 43, 1),
(18, 43, 1),
(24, 43, 1),
(18, 50, 1),
(19, 50, 1),
(9, 68, 1),
(10, 75, 1),
(12, 76, 1),
(18, 112, 1),
(19, 113, 1),
(19, 114, 1),
(19, 115, 1),
(19, 116, 1),
(24, 116, 1),
(19, 117, 1),
(13, 118, 1),
(13, 119, 1),
(13, 120, 1),
(23, 133, 1),
(24, 133, 1),
(25, 133, 1),
(23, 135, 1),
(24, 135, 1),
(25, 135, 1),
(23, 137, 1),
(25, 137, 1),
(23, 138, 1),
(25, 138, 1),
(11, 140, 1),
(23, 154, 1),
(23, 155, 1),
(24, 157, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_profile`
--

CREATE TABLE `ap_profile` (
  `id_profile` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_profile`
--

INSERT INTO `ap_profile` (`id_profile`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Table structure for table `ap_profile_lang`
--

CREATE TABLE `ap_profile_lang` (
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_profile_lang`
--

INSERT INTO `ap_profile_lang` (`id_lang`, `id_profile`, `name`) VALUES
(1, 1, 'SuperAdmin'),
(1, 2, 'Logisticien'),
(1, 3, 'Traducteur'),
(1, 4, 'Commercial');

-- --------------------------------------------------------

--
-- Table structure for table `ap_quick_access`
--

CREATE TABLE `ap_quick_access` (
  `id_quick_access` int(10) UNSIGNED NOT NULL,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_quick_access`
--

INSERT INTO `ap_quick_access` (`id_quick_access`, `new_window`, `link`) VALUES
(4, 0, 'index.php?controller=AdminNpcustomer'),
(5, 0, 'index.php?controller=AdminNpcalendar'),
(6, 0, 'index.php?controller=AdminProducts'),
(7, 0, 'index.php?controller=AdminCustomers'),
(8, 0, 'index.php?controller=AdminModules&configure=bonslick&tab_module=front_office_features&module_name=bonslick'),
(9, 0, 'index.php?controller=AdminModules&configure=themeconfigurator'),
(10, 0, 'index.php?controller=AdminCmsContent');

-- --------------------------------------------------------

--
-- Table structure for table `ap_quick_access_lang`
--

CREATE TABLE `ap_quick_access_lang` (
  `id_quick_access` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_quick_access_lang`
--

INSERT INTO `ap_quick_access_lang` (`id_quick_access`, `id_lang`, `name`) VALUES
(4, 1, 'Documents clients'),
(5, 1, 'Calendrier'),
(6, 1, 'Forfaits'),
(7, 1, 'Clients'),
(8, 1, 'Slider image'),
(9, 1, 'Image Infos'),
(10, 1, 'CMS');

-- --------------------------------------------------------

--
-- Table structure for table `ap_range_price`
--

CREATE TABLE `ap_range_price` (
  `id_range_price` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_range_price`
--

INSERT INTO `ap_range_price` (`id_range_price`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
(1, 2, '0.000000', '10000.000000');

-- --------------------------------------------------------

--
-- Table structure for table `ap_range_weight`
--

CREATE TABLE `ap_range_weight` (
  `id_range_weight` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_range_weight`
--

INSERT INTO `ap_range_weight` (`id_range_weight`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
(1, 2, '0.000000', '10000.000000');

-- --------------------------------------------------------

--
-- Table structure for table `ap_referrer`
--

CREATE TABLE `ap_referrer` (
  `id_referrer` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  `http_referer_regexp` varchar(64) DEFAULT NULL,
  `http_referer_like` varchar(64) DEFAULT NULL,
  `request_uri_regexp` varchar(64) DEFAULT NULL,
  `request_uri_like` varchar(64) DEFAULT NULL,
  `http_referer_regexp_not` varchar(64) DEFAULT NULL,
  `http_referer_like_not` varchar(64) DEFAULT NULL,
  `request_uri_regexp_not` varchar(64) DEFAULT NULL,
  `request_uri_like_not` varchar(64) DEFAULT NULL,
  `base_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `percent_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `click_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_referrer_cache`
--

CREATE TABLE `ap_referrer_cache` (
  `id_connections_source` int(11) UNSIGNED NOT NULL,
  `id_referrer` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_referrer_shop`
--

CREATE TABLE `ap_referrer_shop` (
  `id_referrer` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_visitors` int(11) DEFAULT NULL,
  `cache_visits` int(11) DEFAULT NULL,
  `cache_pages` int(11) DEFAULT NULL,
  `cache_registrations` int(11) DEFAULT NULL,
  `cache_orders` int(11) DEFAULT NULL,
  `cache_sales` decimal(17,2) DEFAULT NULL,
  `cache_reg_rate` decimal(5,4) DEFAULT NULL,
  `cache_order_rate` decimal(5,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_request_sql`
--

CREATE TABLE `ap_request_sql` (
  `id_request_sql` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `sql` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_required_field`
--

CREATE TABLE `ap_required_field` (
  `id_required_field` int(11) NOT NULL,
  `object_name` varchar(32) NOT NULL,
  `field_name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_risk`
--

CREATE TABLE `ap_risk` (
  `id_risk` int(11) UNSIGNED NOT NULL,
  `percent` tinyint(3) NOT NULL,
  `color` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_risk`
--

INSERT INTO `ap_risk` (`id_risk`, `percent`, `color`) VALUES
(1, 0, '#32CD32'),
(2, 35, '#FF8C00'),
(3, 75, '#DC143C'),
(4, 100, '#ec2e15');

-- --------------------------------------------------------

--
-- Table structure for table `ap_risk_lang`
--

CREATE TABLE `ap_risk_lang` (
  `id_risk` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_risk_lang`
--

INSERT INTO `ap_risk_lang` (`id_risk`, `id_lang`, `name`) VALUES
(1, 1, 'Aucun'),
(2, 1, 'Faible'),
(3, 1, 'Moyen'),
(4, 1, 'Élevé');

-- --------------------------------------------------------

--
-- Table structure for table `ap_scene`
--

CREATE TABLE `ap_scene` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_scene_category`
--

CREATE TABLE `ap_scene_category` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_scene_lang`
--

CREATE TABLE `ap_scene_lang` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_scene_products`
--

CREATE TABLE `ap_scene_products` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `x_axis` int(4) NOT NULL,
  `y_axis` int(4) NOT NULL,
  `zone_width` int(3) NOT NULL,
  `zone_height` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_scene_shop`
--

CREATE TABLE `ap_scene_shop` (
  `id_scene` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_search_engine`
--

CREATE TABLE `ap_search_engine` (
  `id_search_engine` int(10) UNSIGNED NOT NULL,
  `server` varchar(64) NOT NULL,
  `getvar` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_search_engine`
--

INSERT INTO `ap_search_engine` (`id_search_engine`, `server`, `getvar`) VALUES
(1, 'google', 'q'),
(2, 'aol', 'q'),
(3, 'yandex', 'text'),
(4, 'ask.com', 'q'),
(5, 'nhl.com', 'q'),
(6, 'yahoo', 'p'),
(7, 'baidu', 'wd'),
(8, 'lycos', 'query'),
(9, 'exalead', 'q'),
(10, 'search.live', 'q'),
(11, 'voila', 'rdata'),
(12, 'altavista', 'q'),
(13, 'bing', 'q'),
(14, 'daum', 'q'),
(15, 'eniro', 'search_word'),
(16, 'naver', 'query'),
(17, 'msn', 'q'),
(18, 'netscape', 'query'),
(19, 'cnn', 'query'),
(20, 'about', 'terms'),
(21, 'mamma', 'query'),
(22, 'alltheweb', 'q'),
(23, 'virgilio', 'qs'),
(24, 'alice', 'qs'),
(25, 'najdi', 'q'),
(26, 'mama', 'query'),
(27, 'seznam', 'q'),
(28, 'onet', 'qt'),
(29, 'szukacz', 'q'),
(30, 'yam', 'k'),
(31, 'pchome', 'q'),
(32, 'kvasir', 'q'),
(33, 'sesam', 'q'),
(34, 'ozu', 'q'),
(35, 'terra', 'query'),
(36, 'mynet', 'q'),
(37, 'ekolay', 'q'),
(38, 'rambler', 'words');

-- --------------------------------------------------------

--
-- Table structure for table `ap_search_index`
--

CREATE TABLE `ap_search_index` (
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_word` int(11) UNSIGNED NOT NULL,
  `weight` smallint(4) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_search_index`
--

INSERT INTO `ap_search_index` (`id_product`, `id_word`, `weight`) VALUES
(9, 1059, 1),
(9, 1432, 1),
(9, 1433, 1),
(9, 1434, 1),
(9, 1435, 1),
(9, 1436, 1),
(9, 1437, 1),
(9, 1438, 1),
(9, 1439, 1),
(9, 1440, 1),
(9, 1441, 1),
(9, 1442, 1),
(9, 1443, 1),
(9, 1444, 1),
(9, 1445, 1),
(9, 1446, 1),
(9, 980, 3),
(9, 2162, 3),
(9, 979, 6),
(9, 1006, 6),
(9, 1007, 6),
(9, 1008, 6),
(9, 1002, 7),
(9, 1003, 7),
(9, 977, 9),
(9, 1001, 14),
(10, 1079, 1),
(10, 1080, 1),
(10, 1081, 1),
(10, 1446, 1),
(10, 1467, 1),
(10, 1470, 1),
(10, 1472, 1),
(10, 1473, 1),
(10, 1474, 1),
(10, 1483, 1),
(10, 1531, 1),
(10, 1533, 1),
(10, 1534, 1),
(10, 1548, 1),
(10, 1549, 1),
(10, 1550, 1),
(10, 1568, 1),
(10, 1817, 1),
(10, 1818, 1),
(10, 1840, 1),
(10, 1841, 1),
(10, 1462, 2),
(10, 1529, 2),
(10, 1530, 2),
(10, 980, 3),
(10, 1001, 4),
(10, 1002, 4),
(10, 1003, 4),
(10, 1444, 4),
(10, 2162, 4),
(10, 2570, 4),
(10, 2571, 4),
(10, 2574, 4),
(10, 2575, 4),
(10, 2576, 4),
(10, 2577, 4),
(10, 2661, 4),
(10, 1432, 5),
(10, 979, 6),
(10, 1062, 6),
(10, 1085, 6),
(10, 1086, 6),
(10, 1087, 6),
(10, 1790, 7),
(10, 1059, 11),
(10, 977, 12),
(10, 1468, 12),
(10, 2569, 16),
(11, 1467, 1),
(11, 1468, 1),
(11, 1471, 1),
(11, 1472, 1),
(11, 1473, 1),
(11, 1474, 1),
(11, 1481, 1),
(11, 1482, 1),
(11, 1529, 1),
(11, 1530, 1),
(11, 1531, 1),
(11, 1565, 1),
(11, 1840, 1),
(11, 1841, 1),
(11, 980, 3),
(11, 2162, 3),
(11, 1002, 4),
(11, 1003, 4),
(11, 1432, 4),
(11, 1444, 4),
(11, 2570, 4),
(11, 2571, 4),
(11, 2574, 4),
(11, 2575, 4),
(11, 2576, 4),
(11, 2577, 4),
(11, 2661, 4),
(11, 979, 6),
(11, 1062, 6),
(11, 1085, 6),
(11, 1086, 6),
(11, 1087, 6),
(11, 1790, 6),
(11, 1059, 11),
(11, 1462, 11),
(11, 977, 12),
(11, 2569, 12),
(12, 1079, 1),
(12, 1080, 1),
(12, 1081, 1),
(12, 1438, 1),
(12, 1467, 1),
(12, 1471, 1),
(12, 1472, 1),
(12, 1473, 1),
(12, 1474, 1),
(12, 1488, 1),
(12, 1527, 1),
(12, 1528, 1),
(12, 1531, 1),
(12, 1532, 1),
(12, 1533, 1),
(12, 1534, 1),
(12, 1535, 1),
(12, 1840, 1),
(12, 1841, 1),
(12, 1842, 1),
(12, 1485, 2),
(12, 1529, 2),
(12, 1530, 2),
(12, 980, 3),
(12, 1001, 4),
(12, 1002, 4),
(12, 1003, 4),
(12, 1444, 4),
(12, 2162, 4),
(12, 2570, 4),
(12, 2571, 4),
(12, 2574, 4),
(12, 2575, 4),
(12, 2576, 4),
(12, 2577, 4),
(12, 1432, 5),
(12, 979, 6),
(12, 1062, 6),
(12, 1085, 6),
(12, 1086, 6),
(12, 1087, 6),
(12, 1059, 7),
(12, 1790, 7),
(12, 977, 13),
(12, 1446, 14),
(12, 2569, 16),
(13, 1433, 1),
(13, 1465, 1),
(13, 1466, 1),
(13, 1467, 1),
(13, 1472, 1),
(13, 1473, 1),
(13, 1474, 1),
(13, 1531, 1),
(13, 1565, 1),
(13, 1566, 1),
(13, 1567, 1),
(13, 1568, 1),
(13, 1710, 1),
(13, 1918, 1),
(13, 4134, 2),
(13, 980, 3),
(13, 1432, 4),
(13, 1445, 4),
(13, 1530, 4),
(13, 2162, 4),
(13, 2570, 4),
(13, 2571, 4),
(13, 2572, 4),
(13, 2573, 4),
(13, 2574, 4),
(13, 2575, 4),
(13, 2576, 4),
(13, 2577, 4),
(13, 979, 6),
(13, 1062, 6),
(13, 1529, 6),
(13, 1464, 7),
(13, 1002, 8),
(13, 1444, 8),
(13, 977, 12),
(13, 2569, 12),
(13, 1003, 14),
(13, 1059, 19),
(15, 1433, 1),
(15, 1465, 1),
(15, 1466, 1),
(15, 1467, 1),
(15, 1472, 1),
(15, 1473, 1),
(15, 1474, 1),
(15, 1531, 1),
(15, 1565, 1),
(15, 1632, 1),
(15, 1633, 1),
(15, 1710, 1),
(15, 1918, 1),
(15, 4134, 2),
(15, 980, 3),
(15, 1530, 4),
(15, 2162, 4),
(15, 979, 6),
(15, 1062, 6),
(15, 1529, 6),
(15, 1003, 7),
(15, 1464, 7),
(15, 977, 8),
(15, 1059, 15),
(16, 1433, 1),
(16, 1465, 1),
(16, 1466, 1),
(16, 1467, 1),
(16, 1472, 1),
(16, 1473, 1),
(16, 1474, 1),
(16, 1565, 1),
(16, 1593, 1),
(16, 1650, 1),
(16, 1651, 1),
(16, 1710, 1),
(16, 1918, 1),
(16, 1531, 2),
(16, 4134, 2),
(16, 980, 3),
(16, 1530, 4),
(16, 2162, 4),
(16, 979, 6),
(16, 1003, 6),
(16, 1062, 6),
(16, 1529, 6),
(16, 1548, 6),
(16, 1073, 7),
(16, 1564, 7),
(16, 977, 8),
(16, 1059, 8),
(17, 1467, 1),
(17, 1472, 1),
(17, 1473, 1),
(17, 1474, 1),
(17, 1528, 1),
(17, 1531, 1),
(17, 1565, 1),
(17, 1667, 1),
(17, 1918, 1),
(17, 1433, 2),
(17, 1529, 2),
(17, 1530, 2),
(17, 1710, 2),
(17, 980, 3),
(17, 2162, 4),
(17, 979, 6),
(17, 1008, 6),
(17, 1062, 6),
(17, 1003, 7),
(17, 977, 8),
(17, 1059, 8),
(18, 1467, 1),
(18, 1472, 1),
(18, 1473, 1),
(18, 1474, 1),
(18, 1531, 1),
(18, 1565, 1),
(18, 1568, 1),
(18, 1918, 1),
(18, 1433, 2),
(18, 1529, 2),
(18, 1530, 2),
(18, 1710, 2),
(18, 980, 3),
(18, 1001, 4),
(18, 1002, 4),
(18, 1432, 4),
(18, 1445, 4),
(18, 2162, 4),
(18, 2570, 4),
(18, 2571, 4),
(18, 2572, 4),
(18, 2573, 4),
(18, 2574, 4),
(18, 2575, 4),
(18, 2576, 4),
(18, 2577, 4),
(18, 2661, 4),
(18, 979, 6),
(18, 1008, 6),
(18, 1062, 6),
(18, 1444, 8),
(18, 977, 12),
(18, 1059, 12),
(18, 1003, 15),
(18, 2569, 16),
(19, 1059, 1),
(19, 1467, 1),
(19, 1472, 1),
(19, 1473, 1),
(19, 1474, 1),
(19, 1531, 1),
(19, 1565, 1),
(19, 1918, 1),
(19, 1433, 2),
(19, 1529, 2),
(19, 1530, 2),
(19, 1710, 2),
(19, 1001, 4),
(19, 1002, 4),
(19, 1438, 4),
(19, 1445, 4),
(19, 1548, 4),
(19, 2162, 4),
(19, 2570, 4),
(19, 2571, 4),
(19, 2572, 4),
(19, 2573, 4),
(19, 2574, 4),
(19, 2575, 4),
(19, 2576, 4),
(19, 2661, 4),
(19, 2867, 4),
(19, 1008, 6),
(19, 1076, 6),
(19, 1432, 8),
(19, 1444, 8),
(19, 2569, 8),
(19, 979, 9),
(19, 1003, 11),
(19, 1073, 11),
(19, 977, 12),
(20, 1433, 1),
(20, 1465, 1),
(20, 1466, 1),
(20, 1467, 1),
(20, 1470, 1),
(20, 1472, 1),
(20, 1473, 1),
(20, 1474, 1),
(20, 1565, 1),
(20, 1593, 1),
(20, 1710, 1),
(20, 1918, 1),
(20, 3094, 1),
(20, 1531, 2),
(20, 4134, 2),
(20, 1530, 4),
(20, 2162, 4),
(20, 1529, 6),
(20, 1003, 7),
(20, 1464, 7),
(20, 977, 8),
(20, 1059, 15),
(21, 2951, 1),
(21, 2952, 1),
(21, 2953, 1),
(21, 2162, 3),
(21, 2950, 6),
(22, 1530, 2),
(22, 4134, 2),
(22, 4133, 3),
(22, 1529, 4),
(22, 4132, 6),
(23, 1432, 1),
(23, 1467, 1),
(23, 1471, 1),
(23, 1472, 1),
(23, 1473, 1),
(23, 1474, 1),
(23, 1528, 1),
(23, 1531, 1),
(23, 1565, 1),
(23, 1840, 1),
(23, 1842, 1),
(23, 4641, 1),
(23, 4134, 2),
(23, 1002, 4),
(23, 1003, 4),
(23, 1444, 4),
(23, 1445, 4),
(23, 1530, 4),
(23, 2162, 4),
(23, 2570, 4),
(23, 2571, 4),
(23, 2572, 4),
(23, 2573, 4),
(23, 2574, 4),
(23, 2576, 4),
(23, 2661, 4),
(23, 4663, 4),
(23, 4664, 4),
(23, 1462, 6),
(23, 1529, 6),
(23, 4638, 6),
(23, 4639, 6),
(23, 4640, 6),
(23, 1059, 7),
(23, 1080, 7),
(23, 977, 8),
(23, 1446, 8),
(23, 2569, 12),
(23, 1079, 13),
(24, 1081, 1),
(24, 1446, 1),
(24, 1467, 1),
(24, 1471, 1),
(24, 1472, 1),
(24, 1473, 1),
(24, 1474, 1),
(24, 1482, 1),
(24, 1483, 1),
(24, 1484, 1),
(24, 1485, 1),
(24, 1486, 1),
(24, 1487, 1),
(24, 1488, 1),
(24, 1489, 1),
(24, 1490, 1),
(24, 1491, 1),
(24, 1531, 1),
(24, 1565, 1),
(24, 1791, 1),
(24, 1840, 1),
(24, 4641, 1),
(24, 4134, 2),
(24, 1002, 4),
(24, 1003, 4),
(24, 1432, 4),
(24, 1438, 4),
(24, 1444, 4),
(24, 1530, 4),
(24, 2162, 4),
(24, 2570, 4),
(24, 2571, 4),
(24, 2574, 4),
(24, 2576, 4),
(24, 2577, 4),
(24, 2661, 4),
(24, 4663, 4),
(24, 4780, 4),
(24, 1529, 5),
(24, 1790, 6),
(24, 4640, 6),
(24, 1059, 7),
(24, 1001, 8),
(24, 1079, 8),
(24, 1080, 8),
(24, 1468, 8),
(24, 977, 10),
(24, 1462, 14),
(24, 2569, 16),
(25, 1081, 1),
(25, 1446, 1),
(25, 1467, 1),
(25, 1470, 1),
(25, 1472, 1),
(25, 1473, 1),
(25, 1474, 1),
(25, 1483, 1),
(25, 1531, 1),
(25, 1533, 1),
(25, 1534, 1),
(25, 1548, 1),
(25, 1549, 1),
(25, 1550, 1),
(25, 1565, 1),
(25, 1568, 1),
(25, 1790, 1),
(25, 1818, 1),
(25, 1840, 1),
(25, 4641, 1),
(25, 4134, 2),
(25, 1002, 4),
(25, 1003, 4),
(25, 1432, 4),
(25, 1445, 4),
(25, 1530, 4),
(25, 2162, 4),
(25, 2570, 4),
(25, 2571, 4),
(25, 2572, 4),
(25, 2573, 4),
(25, 2574, 4),
(25, 2575, 4),
(25, 2576, 4),
(25, 2577, 4),
(25, 2661, 4),
(25, 4663, 4),
(25, 1468, 6),
(25, 1529, 6),
(25, 4640, 6),
(25, 1059, 7),
(25, 1462, 7),
(25, 1444, 8),
(25, 977, 9),
(25, 2569, 12),
(25, 1079, 14),
(25, 1080, 14);

-- --------------------------------------------------------

--
-- Table structure for table `ap_search_word`
--

CREATE TABLE `ap_search_word` (
  `id_word` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `word` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_search_word`
--

INSERT INTO `ap_search_word` (`id_word`, `id_shop`, `id_lang`, `word`) VALUES
(1472, 1, 1, '060'),
(1568, 1, 1, '100'),
(1567, 1, 1, '1000'),
(1527, 1, 1, '129'),
(1842, 1, 1, '149'),
(1667, 1, 1, '169'),
(1528, 1, 1, '200'),
(1481, 1, 1, '229'),
(1791, 1, 1, '269'),
(1650, 1, 1, '299'),
(1482, 1, 1, '350'),
(1632, 1, 1, '399'),
(1651, 1, 1, '400'),
(3094, 1, 1, '490'),
(1817, 1, 1, '499'),
(1818, 1, 1, '539'),
(1566, 1, 1, '540'),
(1633, 1, 1, '600'),
(1470, 1, 1, '800'),
(2572, 1, 1, '974'),
(1550, 1, 1, 'acceleree'),
(4133, 1, 1, 'accueil'),
(2577, 1, 1, 'annule'),
(1439, 1, 1, 'approche'),
(1435, 1, 1, 'assurance'),
(1442, 1, 1, 'assurer'),
(2570, 1, 1, 'auto'),
(1484, 1, 1, 'ayant'),
(1002, 1, 1, 'candidat'),
(1433, 1, 1, 'carburant'),
(4664, 1, 1, 'cher'),
(2575, 1, 1, 'com'),
(4780, 1, 1, 'comman'),
(1445, 1, 1, 'commande'),
(1434, 1, 1, 'compris'),
(2576, 1, 1, 'conduire'),
(2661, 1, 1, 'conduite'),
(1489, 1, 1, 'continu'),
(1535, 1, 1, 'contrainte'),
(1437, 1, 1, 'date'),
(2951, 1, 1, 'delete'),
(1491, 1, 1, 'denses'),
(4641, 1, 1, 'dimanche'),
(1444, 1, 1, 'double'),
(2571, 1, 1, 'ecole'),
(1080, 1, 1, 'end'),
(4639, 1, 1, 'ends'),
(1531, 1, 1, 'euros'),
(1001, 1, 1, 'examen'),
(1488, 1, 1, 'exercer'),
(1081, 1, 1, 'ferie'),
(4640, 1, 1, 'feries'),
(977, 1, 1, 'forfait'),
(2162, 1, 1, 'forfaits'),
(1549, 1, 1, 'formation'),
(1918, 1, 1, 'fractionnables'),
(1073, 1, 1, 'heure'),
(1059, 1, 1, 'heures'),
(1483, 1, 1, 'ideal'),
(1530, 1, 1, 'illimite'),
(1446, 1, 1, 'jour'),
(1790, 1, 1, 'journee'),
(1462, 1, 1, 'jours'),
(1529, 1, 1, 'kilometrage'),
(1473, 1, 1, 'kilometre'),
(1593, 1, 1, 'kilometres'),
(1471, 1, 1, 'kms'),
(1466, 1, 1, 'lancement'),
(1003, 1, 1, 'libre'),
(4134, 1, 1, 'limite'),
(1432, 1, 1, 'location'),
(1840, 1, 1, 'lundi'),
(2953, 1, 1, 'mode'),
(1565, 1, 1, 'mois'),
(1710, 1, 1, 'offert'),
(1564, 1, 1, 'offerte'),
(1464, 1, 1, 'offertes'),
(1465, 1, 1, 'offre'),
(1486, 1, 1, 'pendant'),
(1440, 1, 1, 'pensez'),
(4663, 1, 1, 'perfectionnemen'),
(1487, 1, 1, 'permet'),
(2569, 1, 1, 'permis'),
(1438, 1, 1, 'pratique'),
(1532, 1, 1, 'prendre'),
(2952, 1, 1, 'prod'),
(2950, 1, 1, 'produitcarltest'),
(1441, 1, 1, 'reserver'),
(2573, 1, 1, 'reunion'),
(1436, 1, 1, 'risque'),
(1490, 1, 1, 'routes'),
(1468, 1, 1, 'semaine'),
(4638, 1, 1, 'semaines'),
(1474, 1, 1, 'supplementaire'),
(1485, 1, 1, 'temps'),
(4132, 1, 1, 'test2'),
(1533, 1, 1, 'toute'),
(1534, 1, 1, 'tranquillite'),
(1548, 1, 1, 'une'),
(1467, 1, 1, 'validite'),
(1443, 1, 1, 'vehicule'),
(1841, 1, 1, 'vendredi'),
(2574, 1, 1, 'voiture'),
(2867, 1, 1, 'voiure'),
(1079, 1, 1, 'week');

-- --------------------------------------------------------

--
-- Table structure for table `ap_sekeyword`
--

CREATE TABLE `ap_sekeyword` (
  `id_sekeyword` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `keyword` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_shop`
--

CREATE TABLE `ap_shop` (
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `id_category` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_theme` int(1) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_shop`
--

INSERT INTO `ap_shop` (`id_shop`, `id_shop_group`, `name`, `id_category`, `id_theme`, `active`, `deleted`) VALUES
(1, 1, 'AutoPermis', 2, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_shop_group`
--

CREATE TABLE `ap_shop_group` (
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `share_customer` tinyint(1) NOT NULL,
  `share_order` tinyint(1) NOT NULL,
  `share_stock` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_shop_group`
--

INSERT INTO `ap_shop_group` (`id_shop_group`, `name`, `share_customer`, `share_order`, `share_stock`, `active`, `deleted`) VALUES
(1, 'Default', 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_shop_url`
--

CREATE TABLE `ap_shop_url` (
  `id_shop_url` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `domain` varchar(150) NOT NULL,
  `domain_ssl` varchar(150) NOT NULL,
  `physical_uri` varchar(64) NOT NULL,
  `virtual_uri` varchar(64) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_shop_url`
--

INSERT INTO `ap_shop_url` (`id_shop_url`, `id_shop`, `domain`, `domain_ssl`, `physical_uri`, `virtual_uri`, `main`, `active`) VALUES
(1, 1, 'www.autopermis.re', 'www.autopermis.re', '/', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_smarty_cache`
--

CREATE TABLE `ap_smarty_cache` (
  `id_smarty_cache` char(40) NOT NULL,
  `name` char(40) NOT NULL,
  `cache_id` varchar(254) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_smarty_last_flush`
--

CREATE TABLE `ap_smarty_last_flush` (
  `type` enum('compile','template') NOT NULL,
  `last_flush` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_smarty_last_flush`
--

INSERT INTO `ap_smarty_last_flush` (`type`, `last_flush`) VALUES
('compile', '2019-06-08 00:42:34'),
('template', '2019-06-18 06:38:59');

-- --------------------------------------------------------

--
-- Table structure for table `ap_smarty_lazy_cache`
--

CREATE TABLE `ap_smarty_lazy_cache` (
  `template_hash` varchar(32) NOT NULL DEFAULT '',
  `cache_id` varchar(255) NOT NULL DEFAULT '',
  `compile_id` varchar(32) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_specific_price`
--

CREATE TABLE `ap_specific_price` (
  `id_specific_price` int(10) UNSIGNED NOT NULL,
  `id_specific_price_rule` int(11) UNSIGNED NOT NULL,
  `id_cart` int(11) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_specific_price_priority`
--

CREATE TABLE `ap_specific_price_priority` (
  `id_specific_price_priority` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `priority` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_specific_price_priority`
--

INSERT INTO `ap_specific_price_priority` (`id_specific_price_priority`, `id_product`, `priority`) VALUES
(1, 8, 'id_shop;id_currency;id_country;id_group'),
(6, 9, 'id_shop;id_currency;id_country;id_group'),
(19, 16, 'id_shop;id_currency;id_country;id_group'),
(22, 19, 'id_shop;id_currency;id_country;id_group'),
(23, 10, 'id_shop;id_currency;id_country;id_group'),
(30, 11, 'id_shop;id_currency;id_country;id_group'),
(31, 12, 'id_shop;id_currency;id_country;id_group'),
(37, 14, 'id_shop;id_currency;id_country;id_group'),
(38, 15, 'id_shop;id_currency;id_country;id_group'),
(40, 13, 'id_shop;id_currency;id_country;id_group'),
(41, 17, 'id_shop;id_currency;id_country;id_group'),
(43, 18, 'id_shop;id_currency;id_country;id_group'),
(134, 20, 'id_shop;id_currency;id_country;id_group'),
(166, 21, 'id_shop;id_currency;id_country;id_group'),
(214, 22, 'id_shop;id_currency;id_country;id_group'),
(236, 23, 'id_shop;id_currency;id_country;id_group'),
(240, 24, 'id_shop;id_currency;id_country;id_group'),
(241, 25, 'id_shop;id_currency;id_country;id_group');

-- --------------------------------------------------------

--
-- Table structure for table `ap_specific_price_rule`
--

CREATE TABLE `ap_specific_price_rule` (
  `id_specific_price_rule` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `price` decimal(20,6) DEFAULT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_specific_price_rule_condition`
--

CREATE TABLE `ap_specific_price_rule_condition` (
  `id_specific_price_rule_condition` int(11) UNSIGNED NOT NULL,
  `id_specific_price_rule_condition_group` int(11) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_specific_price_rule_condition_group`
--

CREATE TABLE `ap_specific_price_rule_condition_group` (
  `id_specific_price_rule_condition_group` int(11) UNSIGNED NOT NULL,
  `id_specific_price_rule` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_state`
--

CREATE TABLE `ap_state` (
  `id_state` int(10) UNSIGNED NOT NULL,
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_zone` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(7) NOT NULL,
  `tax_behavior` smallint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_state`
--

INSERT INTO `ap_state` (`id_state`, `id_country`, `id_zone`, `name`, `iso_code`, `tax_behavior`, `active`) VALUES
(1, 21, 2, 'Alabama', 'AL', 0, 1),
(2, 21, 2, 'Alaska', 'AK', 0, 1),
(3, 21, 2, 'Arizona', 'AZ', 0, 1),
(4, 21, 2, 'Arkansas', 'AR', 0, 1),
(5, 21, 2, 'California', 'CA', 0, 1),
(6, 21, 2, 'Colorado', 'CO', 0, 1),
(7, 21, 2, 'Connecticut', 'CT', 0, 1),
(8, 21, 2, 'Delaware', 'DE', 0, 1),
(9, 21, 2, 'Florida', 'FL', 0, 1),
(10, 21, 2, 'Georgia', 'GA', 0, 1),
(11, 21, 2, 'Hawaii', 'HI', 0, 1),
(12, 21, 2, 'Idaho', 'ID', 0, 1),
(13, 21, 2, 'Illinois', 'IL', 0, 1),
(14, 21, 2, 'Indiana', 'IN', 0, 1),
(15, 21, 2, 'Iowa', 'IA', 0, 1),
(16, 21, 2, 'Kansas', 'KS', 0, 1),
(17, 21, 2, 'Kentucky', 'KY', 0, 1),
(18, 21, 2, 'Louisiana', 'LA', 0, 1),
(19, 21, 2, 'Maine', 'ME', 0, 1),
(20, 21, 2, 'Maryland', 'MD', 0, 1),
(21, 21, 2, 'Massachusetts', 'MA', 0, 1),
(22, 21, 2, 'Michigan', 'MI', 0, 1),
(23, 21, 2, 'Minnesota', 'MN', 0, 1),
(24, 21, 2, 'Mississippi', 'MS', 0, 1),
(25, 21, 2, 'Missouri', 'MO', 0, 1),
(26, 21, 2, 'Montana', 'MT', 0, 1),
(27, 21, 2, 'Nebraska', 'NE', 0, 1),
(28, 21, 2, 'Nevada', 'NV', 0, 1),
(29, 21, 2, 'New Hampshire', 'NH', 0, 1),
(30, 21, 2, 'New Jersey', 'NJ', 0, 1),
(31, 21, 2, 'New Mexico', 'NM', 0, 1),
(32, 21, 2, 'New York', 'NY', 0, 1),
(33, 21, 2, 'North Carolina', 'NC', 0, 1),
(34, 21, 2, 'North Dakota', 'ND', 0, 1),
(35, 21, 2, 'Ohio', 'OH', 0, 1),
(36, 21, 2, 'Oklahoma', 'OK', 0, 1),
(37, 21, 2, 'Oregon', 'OR', 0, 1),
(38, 21, 2, 'Pennsylvania', 'PA', 0, 1),
(39, 21, 2, 'Rhode Island', 'RI', 0, 1),
(40, 21, 2, 'South Carolina', 'SC', 0, 1),
(41, 21, 2, 'South Dakota', 'SD', 0, 1),
(42, 21, 2, 'Tennessee', 'TN', 0, 1),
(43, 21, 2, 'Texas', 'TX', 0, 1),
(44, 21, 2, 'Utah', 'UT', 0, 1),
(45, 21, 2, 'Vermont', 'VT', 0, 1),
(46, 21, 2, 'Virginia', 'VA', 0, 1),
(47, 21, 2, 'Washington', 'WA', 0, 1),
(48, 21, 2, 'West Virginia', 'WV', 0, 1),
(49, 21, 2, 'Wisconsin', 'WI', 0, 1),
(50, 21, 2, 'Wyoming', 'WY', 0, 1),
(51, 21, 2, 'Puerto Rico', 'PR', 0, 1),
(52, 21, 2, 'US Virgin Islands', 'VI', 0, 1),
(53, 21, 2, 'District of Columbia', 'DC', 0, 1),
(54, 145, 2, 'Aguascalientes', 'AGS', 0, 1),
(55, 145, 2, 'Baja California', 'BCN', 0, 1),
(56, 145, 2, 'Baja California Sur', 'BCS', 0, 1),
(57, 145, 2, 'Campeche', 'CAM', 0, 1),
(58, 145, 2, 'Chiapas', 'CHP', 0, 1),
(59, 145, 2, 'Chihuahua', 'CHH', 0, 1),
(60, 145, 2, 'Coahuila', 'COA', 0, 1),
(61, 145, 2, 'Colima', 'COL', 0, 1),
(62, 145, 2, 'Distrito Federal', 'DIF', 0, 1),
(63, 145, 2, 'Durango', 'DUR', 0, 1),
(64, 145, 2, 'Guanajuato', 'GUA', 0, 1),
(65, 145, 2, 'Guerrero', 'GRO', 0, 1),
(66, 145, 2, 'Hidalgo', 'HID', 0, 1),
(67, 145, 2, 'Jalisco', 'JAL', 0, 1),
(68, 145, 2, 'Estado de México', 'MEX', 0, 1),
(69, 145, 2, 'Michoacán', 'MIC', 0, 1),
(70, 145, 2, 'Morelos', 'MOR', 0, 1),
(71, 145, 2, 'Nayarit', 'NAY', 0, 1),
(72, 145, 2, 'Nuevo León', 'NLE', 0, 1),
(73, 145, 2, 'Oaxaca', 'OAX', 0, 1),
(74, 145, 2, 'Puebla', 'PUE', 0, 1),
(75, 145, 2, 'Querétaro', 'QUE', 0, 1),
(76, 145, 2, 'Quintana Roo', 'ROO', 0, 1),
(77, 145, 2, 'San Luis Potosí', 'SLP', 0, 1),
(78, 145, 2, 'Sinaloa', 'SIN', 0, 1),
(79, 145, 2, 'Sonora', 'SON', 0, 1),
(80, 145, 2, 'Tabasco', 'TAB', 0, 1),
(81, 145, 2, 'Tamaulipas', 'TAM', 0, 1),
(82, 145, 2, 'Tlaxcala', 'TLA', 0, 1),
(83, 145, 2, 'Veracruz', 'VER', 0, 1),
(84, 145, 2, 'Yucatán', 'YUC', 0, 1),
(85, 145, 2, 'Zacatecas', 'ZAC', 0, 1),
(86, 4, 2, 'Ontario', 'ON', 0, 1),
(87, 4, 2, 'Quebec', 'QC', 0, 1),
(88, 4, 2, 'British Columbia', 'BC', 0, 1),
(89, 4, 2, 'Alberta', 'AB', 0, 1),
(90, 4, 2, 'Manitoba', 'MB', 0, 1),
(91, 4, 2, 'Saskatchewan', 'SK', 0, 1),
(92, 4, 2, 'Nova Scotia', 'NS', 0, 1),
(93, 4, 2, 'New Brunswick', 'NB', 0, 1),
(94, 4, 2, 'Newfoundland and Labrador', 'NL', 0, 1),
(95, 4, 2, 'Prince Edward Island', 'PE', 0, 1),
(96, 4, 2, 'Northwest Territories', 'NT', 0, 1),
(97, 4, 2, 'Yukon', 'YT', 0, 1),
(98, 4, 2, 'Nunavut', 'NU', 0, 1),
(99, 44, 6, 'Buenos Aires', 'B', 0, 1),
(100, 44, 6, 'Catamarca', 'K', 0, 1),
(101, 44, 6, 'Chaco', 'H', 0, 1),
(102, 44, 6, 'Chubut', 'U', 0, 1),
(103, 44, 6, 'Ciudad de Buenos Aires', 'C', 0, 1),
(104, 44, 6, 'Córdoba', 'X', 0, 1),
(105, 44, 6, 'Corrientes', 'W', 0, 1),
(106, 44, 6, 'Entre Ríos', 'E', 0, 1),
(107, 44, 6, 'Formosa', 'P', 0, 1),
(108, 44, 6, 'Jujuy', 'Y', 0, 1),
(109, 44, 6, 'La Pampa', 'L', 0, 1),
(110, 44, 6, 'La Rioja', 'F', 0, 1),
(111, 44, 6, 'Mendoza', 'M', 0, 1),
(112, 44, 6, 'Misiones', 'N', 0, 1),
(113, 44, 6, 'Neuquén', 'Q', 0, 1),
(114, 44, 6, 'Río Negro', 'R', 0, 1),
(115, 44, 6, 'Salta', 'A', 0, 1),
(116, 44, 6, 'San Juan', 'J', 0, 1),
(117, 44, 6, 'San Luis', 'D', 0, 1),
(118, 44, 6, 'Santa Cruz', 'Z', 0, 1),
(119, 44, 6, 'Santa Fe', 'S', 0, 1),
(120, 44, 6, 'Santiago del Estero', 'G', 0, 1),
(121, 44, 6, 'Tierra del Fuego', 'V', 0, 1),
(122, 44, 6, 'Tucumán', 'T', 0, 1),
(123, 10, 1, 'Agrigento', 'AG', 0, 1),
(124, 10, 1, 'Alessandria', 'AL', 0, 1),
(125, 10, 1, 'Ancona', 'AN', 0, 1),
(126, 10, 1, 'Aosta', 'AO', 0, 1),
(127, 10, 1, 'Arezzo', 'AR', 0, 1),
(128, 10, 1, 'Ascoli Piceno', 'AP', 0, 1),
(129, 10, 1, 'Asti', 'AT', 0, 1),
(130, 10, 1, 'Avellino', 'AV', 0, 1),
(131, 10, 1, 'Bari', 'BA', 0, 1),
(132, 10, 1, 'Barletta-Andria-Trani', 'BT', 0, 1),
(133, 10, 1, 'Belluno', 'BL', 0, 1),
(134, 10, 1, 'Benevento', 'BN', 0, 1),
(135, 10, 1, 'Bergamo', 'BG', 0, 1),
(136, 10, 1, 'Biella', 'BI', 0, 1),
(137, 10, 1, 'Bologna', 'BO', 0, 1),
(138, 10, 1, 'Bolzano', 'BZ', 0, 1),
(139, 10, 1, 'Brescia', 'BS', 0, 1),
(140, 10, 1, 'Brindisi', 'BR', 0, 1),
(141, 10, 1, 'Cagliari', 'CA', 0, 1),
(142, 10, 1, 'Caltanissetta', 'CL', 0, 1),
(143, 10, 1, 'Campobasso', 'CB', 0, 1),
(144, 10, 1, 'Carbonia-Iglesias', 'CI', 0, 1),
(145, 10, 1, 'Caserta', 'CE', 0, 1),
(146, 10, 1, 'Catania', 'CT', 0, 1),
(147, 10, 1, 'Catanzaro', 'CZ', 0, 1),
(148, 10, 1, 'Chieti', 'CH', 0, 1),
(149, 10, 1, 'Como', 'CO', 0, 1),
(150, 10, 1, 'Cosenza', 'CS', 0, 1),
(151, 10, 1, 'Cremona', 'CR', 0, 1),
(152, 10, 1, 'Crotone', 'KR', 0, 1),
(153, 10, 1, 'Cuneo', 'CN', 0, 1),
(154, 10, 1, 'Enna', 'EN', 0, 1),
(155, 10, 1, 'Fermo', 'FM', 0, 1),
(156, 10, 1, 'Ferrara', 'FE', 0, 1),
(157, 10, 1, 'Firenze', 'FI', 0, 1),
(158, 10, 1, 'Foggia', 'FG', 0, 1),
(159, 10, 1, 'Forlì-Cesena', 'FC', 0, 1),
(160, 10, 1, 'Frosinone', 'FR', 0, 1),
(161, 10, 1, 'Genova', 'GE', 0, 1),
(162, 10, 1, 'Gorizia', 'GO', 0, 1),
(163, 10, 1, 'Grosseto', 'GR', 0, 1),
(164, 10, 1, 'Imperia', 'IM', 0, 1),
(165, 10, 1, 'Isernia', 'IS', 0, 1),
(166, 10, 1, 'L\'Aquila', 'AQ', 0, 1),
(167, 10, 1, 'La Spezia', 'SP', 0, 1),
(168, 10, 1, 'Latina', 'LT', 0, 1),
(169, 10, 1, 'Lecce', 'LE', 0, 1),
(170, 10, 1, 'Lecco', 'LC', 0, 1),
(171, 10, 1, 'Livorno', 'LI', 0, 1),
(172, 10, 1, 'Lodi', 'LO', 0, 1),
(173, 10, 1, 'Lucca', 'LU', 0, 1),
(174, 10, 1, 'Macerata', 'MC', 0, 1),
(175, 10, 1, 'Mantova', 'MN', 0, 1),
(176, 10, 1, 'Massa', 'MS', 0, 1),
(177, 10, 1, 'Matera', 'MT', 0, 1),
(178, 10, 1, 'Medio Campidano', 'VS', 0, 1),
(179, 10, 1, 'Messina', 'ME', 0, 1),
(180, 10, 1, 'Milano', 'MI', 0, 1),
(181, 10, 1, 'Modena', 'MO', 0, 1),
(182, 10, 1, 'Monza e della Brianza', 'MB', 0, 1),
(183, 10, 1, 'Napoli', 'NA', 0, 1),
(184, 10, 1, 'Novara', 'NO', 0, 1),
(185, 10, 1, 'Nuoro', 'NU', 0, 1),
(186, 10, 1, 'Ogliastra', 'OG', 0, 1),
(187, 10, 1, 'Olbia-Tempio', 'OT', 0, 1),
(188, 10, 1, 'Oristano', 'OR', 0, 1),
(189, 10, 1, 'Padova', 'PD', 0, 1),
(190, 10, 1, 'Palermo', 'PA', 0, 1),
(191, 10, 1, 'Parma', 'PR', 0, 1),
(192, 10, 1, 'Pavia', 'PV', 0, 1),
(193, 10, 1, 'Perugia', 'PG', 0, 1),
(194, 10, 1, 'Pesaro-Urbino', 'PU', 0, 1),
(195, 10, 1, 'Pescara', 'PE', 0, 1),
(196, 10, 1, 'Piacenza', 'PC', 0, 1),
(197, 10, 1, 'Pisa', 'PI', 0, 1),
(198, 10, 1, 'Pistoia', 'PT', 0, 1),
(199, 10, 1, 'Pordenone', 'PN', 0, 1),
(200, 10, 1, 'Potenza', 'PZ', 0, 1),
(201, 10, 1, 'Prato', 'PO', 0, 1),
(202, 10, 1, 'Ragusa', 'RG', 0, 1),
(203, 10, 1, 'Ravenna', 'RA', 0, 1),
(204, 10, 1, 'Reggio Calabria', 'RC', 0, 1),
(205, 10, 1, 'Reggio Emilia', 'RE', 0, 1),
(206, 10, 1, 'Rieti', 'RI', 0, 1),
(207, 10, 1, 'Rimini', 'RN', 0, 1),
(208, 10, 1, 'Roma', 'RM', 0, 1),
(209, 10, 1, 'Rovigo', 'RO', 0, 1),
(210, 10, 1, 'Salerno', 'SA', 0, 1),
(211, 10, 1, 'Sassari', 'SS', 0, 1),
(212, 10, 1, 'Savona', 'SV', 0, 1),
(213, 10, 1, 'Siena', 'SI', 0, 1),
(214, 10, 1, 'Siracusa', 'SR', 0, 1),
(215, 10, 1, 'Sondrio', 'SO', 0, 1),
(216, 10, 1, 'Taranto', 'TA', 0, 1),
(217, 10, 1, 'Teramo', 'TE', 0, 1),
(218, 10, 1, 'Terni', 'TR', 0, 1),
(219, 10, 1, 'Torino', 'TO', 0, 1),
(220, 10, 1, 'Trapani', 'TP', 0, 1),
(221, 10, 1, 'Trento', 'TN', 0, 1),
(222, 10, 1, 'Treviso', 'TV', 0, 1),
(223, 10, 1, 'Trieste', 'TS', 0, 1),
(224, 10, 1, 'Udine', 'UD', 0, 1),
(225, 10, 1, 'Varese', 'VA', 0, 1),
(226, 10, 1, 'Venezia', 'VE', 0, 1),
(227, 10, 1, 'Verbano-Cusio-Ossola', 'VB', 0, 1),
(228, 10, 1, 'Vercelli', 'VC', 0, 1),
(229, 10, 1, 'Verona', 'VR', 0, 1),
(230, 10, 1, 'Vibo Valentia', 'VV', 0, 1),
(231, 10, 1, 'Vicenza', 'VI', 0, 1),
(232, 10, 1, 'Viterbo', 'VT', 0, 1),
(233, 111, 3, 'Aceh', 'AC', 0, 1),
(234, 111, 3, 'Bali', 'BA', 0, 1),
(235, 111, 3, 'Bangka', 'BB', 0, 1),
(236, 111, 3, 'Banten', 'BT', 0, 1),
(237, 111, 3, 'Bengkulu', 'BE', 0, 1),
(238, 111, 3, 'Central Java', 'JT', 0, 1),
(239, 111, 3, 'Central Kalimantan', 'KT', 0, 1),
(240, 111, 3, 'Central Sulawesi', 'ST', 0, 1),
(241, 111, 3, 'Coat of arms of East Java', 'JI', 0, 1),
(242, 111, 3, 'East kalimantan', 'KI', 0, 1),
(243, 111, 3, 'East Nusa Tenggara', 'NT', 0, 1),
(244, 111, 3, 'Lambang propinsi', 'GO', 0, 1),
(245, 111, 3, 'Jakarta', 'JK', 0, 1),
(246, 111, 3, 'Jambi', 'JA', 0, 1),
(247, 111, 3, 'Lampung', 'LA', 0, 1),
(248, 111, 3, 'Maluku', 'MA', 0, 1),
(249, 111, 3, 'North Maluku', 'MU', 0, 1),
(250, 111, 3, 'North Sulawesi', 'SA', 0, 1),
(251, 111, 3, 'North Sumatra', 'SU', 0, 1),
(252, 111, 3, 'Papua', 'PA', 0, 1),
(253, 111, 3, 'Riau', 'RI', 0, 1),
(254, 111, 3, 'Lambang Riau', 'KR', 0, 1),
(255, 111, 3, 'Southeast Sulawesi', 'SG', 0, 1),
(256, 111, 3, 'South Kalimantan', 'KS', 0, 1),
(257, 111, 3, 'South Sulawesi', 'SN', 0, 1),
(258, 111, 3, 'South Sumatra', 'SS', 0, 1),
(259, 111, 3, 'West Java', 'JB', 0, 1),
(260, 111, 3, 'West Kalimantan', 'KB', 0, 1),
(261, 111, 3, 'West Nusa Tenggara', 'NB', 0, 1),
(262, 111, 3, 'Lambang Provinsi Papua Barat', 'PB', 0, 1),
(263, 111, 3, 'West Sulawesi', 'SR', 0, 1),
(264, 111, 3, 'West Sumatra', 'SB', 0, 1),
(265, 111, 3, 'Yogyakarta', 'YO', 0, 1),
(266, 11, 3, 'Aichi', '23', 0, 1),
(267, 11, 3, 'Akita', '05', 0, 1),
(268, 11, 3, 'Aomori', '02', 0, 1),
(269, 11, 3, 'Chiba', '12', 0, 1),
(270, 11, 3, 'Ehime', '38', 0, 1),
(271, 11, 3, 'Fukui', '18', 0, 1),
(272, 11, 3, 'Fukuoka', '40', 0, 1),
(273, 11, 3, 'Fukushima', '07', 0, 1),
(274, 11, 3, 'Gifu', '21', 0, 1),
(275, 11, 3, 'Gunma', '10', 0, 1),
(276, 11, 3, 'Hiroshima', '34', 0, 1),
(277, 11, 3, 'Hokkaido', '01', 0, 1),
(278, 11, 3, 'Hyogo', '28', 0, 1),
(279, 11, 3, 'Ibaraki', '08', 0, 1),
(280, 11, 3, 'Ishikawa', '17', 0, 1),
(281, 11, 3, 'Iwate', '03', 0, 1),
(282, 11, 3, 'Kagawa', '37', 0, 1),
(283, 11, 3, 'Kagoshima', '46', 0, 1),
(284, 11, 3, 'Kanagawa', '14', 0, 1),
(285, 11, 3, 'Kochi', '39', 0, 1),
(286, 11, 3, 'Kumamoto', '43', 0, 1),
(287, 11, 3, 'Kyoto', '26', 0, 1),
(288, 11, 3, 'Mie', '24', 0, 1),
(289, 11, 3, 'Miyagi', '04', 0, 1),
(290, 11, 3, 'Miyazaki', '45', 0, 1),
(291, 11, 3, 'Nagano', '20', 0, 1),
(292, 11, 3, 'Nagasaki', '42', 0, 1),
(293, 11, 3, 'Nara', '29', 0, 1),
(294, 11, 3, 'Niigata', '15', 0, 1),
(295, 11, 3, 'Oita', '44', 0, 1),
(296, 11, 3, 'Okayama', '33', 0, 1),
(297, 11, 3, 'Okinawa', '47', 0, 1),
(298, 11, 3, 'Osaka', '27', 0, 1),
(299, 11, 3, 'Saga', '41', 0, 1),
(300, 11, 3, 'Saitama', '11', 0, 1),
(301, 11, 3, 'Shiga', '25', 0, 1),
(302, 11, 3, 'Shimane', '32', 0, 1),
(303, 11, 3, 'Shizuoka', '22', 0, 1),
(304, 11, 3, 'Tochigi', '09', 0, 1),
(305, 11, 3, 'Tokushima', '36', 0, 1),
(306, 11, 3, 'Tokyo', '13', 0, 1),
(307, 11, 3, 'Tottori', '31', 0, 1),
(308, 11, 3, 'Toyama', '16', 0, 1),
(309, 11, 3, 'Wakayama', '30', 0, 1),
(310, 11, 3, 'Yamagata', '06', 0, 1),
(311, 11, 3, 'Yamaguchi', '35', 0, 1),
(312, 11, 3, 'Yamanashi', '19', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_statssearch`
--

CREATE TABLE `ap_statssearch` (
  `id_statssearch` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `keywords` varchar(255) NOT NULL,
  `results` int(6) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_statssearch`
--

INSERT INTO `ap_statssearch` (`id_statssearch`, `id_shop`, `id_shop_group`, `keywords`, `results`, `date_add`) VALUES
(1, 1, 1, '20 ', 0, '2019-06-10 03:06:03'),
(2, 1, 1, '20 he', 0, '2019-06-10 03:06:04'),
(3, 1, 1, '20 heure', 3, '2019-06-10 03:06:06'),
(4, 1, 1, '20 heure', 3, '2019-06-10 03:06:07'),
(5, 1, 1, '1 he', 0, '2019-06-10 03:06:57'),
(6, 1, 1, '1 heure', 3, '2019-06-10 03:06:57'),
(7, 1, 1, '1 heure', 3, '2019-06-10 03:06:58'),
(8, 1, 1, '1 heur', 3, '2019-06-10 03:07:11'),
(9, 1, 1, '1 heure', 3, '2019-06-10 03:07:11'),
(10, 1, 1, 'cha', 0, '2019-06-15 18:28:07'),
(11, 1, 1, 'charte', 0, '2019-06-15 18:28:10'),
(12, 1, 1, 'per', 1, '2019-06-15 18:28:25'),
(13, 1, 1, 'perm', 1, '2019-06-15 18:28:25'),
(14, 1, 1, 'permi', 0, '2019-06-15 18:28:26'),
(15, 1, 1, 'permis', 0, '2019-06-15 18:28:28'),
(16, 1, 1, 'can', 1, '2019-06-15 18:28:46'),
(17, 1, 1, 'cand', 1, '2019-06-15 18:28:46'),
(18, 1, 1, 'candi', 1, '2019-06-15 18:28:47'),
(19, 1, 1, 'candida', 1, '2019-06-15 18:28:48'),
(20, 1, 1, 'candidat', 1, '2019-06-15 18:28:48'),
(21, 1, 1, 'candidat ', 1, '2019-06-15 18:28:49'),
(22, 1, 1, 'candidat ', 1, '2019-06-15 18:28:51'),
(23, 1, 1, 'candida', 1, '2019-06-15 18:30:04'),
(24, 1, 1, 'kih', 0, '2019-06-15 18:30:24'),
(25, 1, 1, 'kiho', 0, '2019-06-15 18:30:24'),
(26, 1, 1, 'kiho', 0, '2019-06-15 18:30:26'),
(27, 1, 1, 'kih', 0, '2019-06-15 18:30:43'),
(28, 1, 1, 'per', 1, '2019-06-15 18:30:46'),
(29, 1, 1, 'perm', 1, '2019-06-15 18:30:46'),
(30, 1, 1, 'permis', 0, '2019-06-15 18:30:47'),
(31, 1, 1, 'permis', 0, '2019-06-15 18:30:49'),
(32, 1, 1, 'permi', 0, '2019-06-15 18:31:29'),
(33, 1, 1, 'kiho', 0, '2019-06-15 18:33:23'),
(34, 1, 1, 'for', 10, '2019-06-17 14:08:31'),
(35, 1, 1, 'forfait', 10, '2019-06-17 14:08:33'),
(36, 1, 1, 'double commande', 2, '2019-06-17 14:14:19'),
(37, 1, 1, 'double commande', 2, '2019-06-17 14:14:19'),
(38, 1, 1, 'double commande', 2, '2019-06-17 14:14:34'),
(39, 1, 1, 'permis annule', 1, '2019-06-17 14:14:41'),
(40, 1, 1, 'permis annule', 1, '2019-06-17 14:14:41'),
(41, 1, 1, 'forfait', 10, '2019-06-17 14:25:05'),
(42, 1, 1, 'test', 1, '2019-06-19 10:46:33'),
(43, 1, 1, 'test2', 1, '2019-06-19 10:46:34');

-- --------------------------------------------------------

--
-- Table structure for table `ap_stock`
--

CREATE TABLE `ap_stock` (
  `id_stock` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `reference` varchar(32) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `physical_quantity` int(11) UNSIGNED NOT NULL,
  `usable_quantity` int(11) UNSIGNED NOT NULL,
  `price_te` decimal(20,6) DEFAULT '0.000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_stock_available`
--

CREATE TABLE `ap_stock_available` (
  `id_stock_available` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `quantity` int(10) NOT NULL DEFAULT '0',
  `depends_on_stock` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `out_of_stock` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_stock_available`
--

INSERT INTO `ap_stock_available` (`id_stock_available`, `id_product`, `id_product_attribute`, `id_shop`, `id_shop_group`, `quantity`, `depends_on_stock`, `out_of_stock`) VALUES
(54, 1, 0, 1, 0, 0, 0, 0),
(55, 2, 0, 1, 0, 0, 0, 0),
(56, 3, 0, 1, 0, 0, 0, 0),
(57, 4, 0, 1, 0, 0, 0, 0),
(58, 5, 0, 1, 0, 0, 0, 0),
(59, 6, 0, 1, 0, 0, 0, 0),
(60, 7, 0, 1, 0, 0, 0, 0),
(61, 9, 0, 1, 0, -6, 0, 1),
(62, 10, 0, 1, 0, 0, 0, 1),
(63, 11, 0, 1, 0, 0, 0, 1),
(64, 12, 0, 1, 0, 0, 0, 1),
(65, 13, 0, 1, 0, 0, 0, 1),
(67, 15, 0, 1, 0, 0, 0, 1),
(68, 16, 0, 1, 0, 0, 0, 1),
(69, 17, 0, 1, 0, 0, 0, 1),
(70, 18, 0, 1, 0, 0, 0, 1),
(71, 19, 0, 1, 0, 0, 0, 1),
(72, 20, 0, 1, 0, 0, 0, 1),
(73, 21, 0, 1, 0, -4, 0, 1),
(74, 22, 0, 1, 0, 0, 0, 1),
(75, 22, 46, 1, 0, 0, 0, 1),
(76, 22, 47, 1, 0, 0, 0, 1),
(77, 13, 48, 1, 0, 0, 0, 1),
(78, 13, 49, 1, 0, 0, 0, 1),
(79, 20, 50, 1, 0, 0, 0, 1),
(80, 20, 51, 1, 0, 0, 0, 1),
(81, 15, 52, 1, 0, 0, 0, 1),
(82, 15, 53, 1, 0, 0, 0, 1),
(83, 16, 54, 1, 0, 0, 0, 1),
(84, 16, 55, 1, 0, 0, 0, 1),
(85, 17, 56, 1, 0, 0, 0, 1),
(86, 17, 57, 1, 0, 0, 0, 1),
(87, 18, 58, 1, 0, 0, 0, 1),
(88, 18, 59, 1, 0, 0, 0, 1),
(89, 19, 60, 1, 0, 0, 0, 1),
(90, 19, 61, 1, 0, 0, 0, 1),
(91, 23, 0, 1, 0, 0, 0, 2),
(92, 23, 62, 1, 0, 0, 0, 2),
(93, 23, 63, 1, 0, 0, 0, 2),
(94, 24, 0, 1, 0, 0, 0, 2),
(95, 24, 64, 1, 0, 0, 0, 2),
(96, 24, 65, 1, 0, 0, 0, 2),
(97, 25, 0, 1, 0, 0, 0, 2),
(98, 25, 66, 1, 0, 0, 0, 2),
(99, 25, 67, 1, 0, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ap_stock_mvt`
--

CREATE TABLE `ap_stock_mvt` (
  `id_stock_mvt` bigint(20) UNSIGNED NOT NULL,
  `id_stock` int(11) UNSIGNED NOT NULL,
  `id_order` int(11) UNSIGNED DEFAULT NULL,
  `id_supply_order` int(11) UNSIGNED DEFAULT NULL,
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `physical_quantity` int(11) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `sign` tinyint(1) NOT NULL DEFAULT '1',
  `price_te` decimal(20,6) DEFAULT '0.000000',
  `last_wa` decimal(20,6) DEFAULT '0.000000',
  `current_wa` decimal(20,6) DEFAULT '0.000000',
  `referer` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_stock_mvt_reason`
--

CREATE TABLE `ap_stock_mvt_reason` (
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL,
  `sign` tinyint(1) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_stock_mvt_reason`
--

INSERT INTO `ap_stock_mvt_reason` (`id_stock_mvt_reason`, `sign`, `date_add`, `date_upd`, `deleted`) VALUES
(1, 1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0),
(2, -1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0),
(3, -1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0),
(4, -1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0),
(5, 1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0),
(6, -1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0),
(7, 1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0),
(8, 1, '2019-06-08 02:34:21', '2019-06-08 02:34:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_stock_mvt_reason_lang`
--

CREATE TABLE `ap_stock_mvt_reason_lang` (
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_stock_mvt_reason_lang`
--

INSERT INTO `ap_stock_mvt_reason_lang` (`id_stock_mvt_reason`, `id_lang`, `name`) VALUES
(1, 1, 'Augmentation'),
(2, 1, 'Baisse'),
(3, 1, 'Commande client'),
(4, 1, 'Régularisation suite à inventaire'),
(5, 1, 'Régularisation suite à inventaire'),
(6, 1, 'Transfert vers un autre entrepôt'),
(7, 1, 'Transfert depuis un autre entrepôt'),
(8, 1, 'Commande fournisseur');

-- --------------------------------------------------------

--
-- Table structure for table `ap_store`
--

CREATE TABLE `ap_store` (
  `id_store` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `postcode` varchar(12) NOT NULL,
  `latitude` decimal(13,8) DEFAULT NULL,
  `longitude` decimal(13,8) DEFAULT NULL,
  `hours` varchar(254) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `note` text,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_store`
--

INSERT INTO `ap_store` (`id_store`, `id_country`, `id_state`, `name`, `address1`, `address2`, `city`, `postcode`, `latitude`, `longitude`, `hours`, `phone`, `fax`, `email`, `note`, `active`, `date_add`, `date_upd`) VALUES
(1, 176, 0, 'AutoPermis.re', '68 rue de Suffren', '', 'Saint-Pierre', '97410', '-21.33751600', '55.47267600', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 1, '2019-06-08 02:34:28', '2019-06-10 15:10:51'),
(2, 21, 9, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', 'Miami', ' 33304', '26.13793600', '-80.13943500', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 0, '2019-06-08 02:34:28', '2019-06-10 15:09:05'),
(3, 21, 9, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', 'Miami', '33026', '26.00998700', '-80.29447200', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 0, '2019-06-08 02:34:28', '2019-06-10 15:09:05'),
(4, 21, 9, 'Coconut Grove', '2999 SW 32nd Avenue', '', 'Miami', ' 33133', '25.73629600', '-80.24479700', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 0, '2019-06-08 02:34:28', '2019-06-10 15:09:05'),
(5, 21, 9, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', 'Miami', '33181', '25.88674000', '-80.16329200', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 0, '2019-06-08 02:34:28', '2019-06-10 15:09:05');

-- --------------------------------------------------------

--
-- Table structure for table `ap_store_shop`
--

CREATE TABLE `ap_store_shop` (
  `id_store` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_store_shop`
--

INSERT INTO `ap_store_shop` (`id_store`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_supplier`
--

CREATE TABLE `ap_supplier` (
  `id_supplier` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_supplier`
--

INSERT INTO `ap_supplier` (`id_supplier`, `name`, `date_add`, `date_upd`, `active`) VALUES
(1, 'Fashion Supplier', '2019-06-08 02:34:25', '2019-06-12 01:35:50', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_supplier_lang`
--

CREATE TABLE `ap_supplier_lang` (
  `id_supplier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_supplier_lang`
--

INSERT INTO `ap_supplier_lang` (`id_supplier`, `id_lang`, `description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_supplier_shop`
--

CREATE TABLE `ap_supplier_shop` (
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_supplier_shop`
--

INSERT INTO `ap_supplier_shop` (`id_supplier`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_supply_order`
--

CREATE TABLE `ap_supply_order` (
  `id_supply_order` int(11) UNSIGNED NOT NULL,
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `supplier_name` varchar(64) NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_ref_currency` int(11) UNSIGNED NOT NULL,
  `reference` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `date_delivery_expected` datetime DEFAULT NULL,
  `total_te` decimal(20,6) DEFAULT '0.000000',
  `total_with_discount_te` decimal(20,6) DEFAULT '0.000000',
  `total_tax` decimal(20,6) DEFAULT '0.000000',
  `total_ti` decimal(20,6) DEFAULT '0.000000',
  `discount_rate` decimal(20,6) DEFAULT '0.000000',
  `discount_value_te` decimal(20,6) DEFAULT '0.000000',
  `is_template` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_supply_order_detail`
--

CREATE TABLE `ap_supply_order_detail` (
  `id_supply_order_detail` int(11) UNSIGNED NOT NULL,
  `id_supply_order` int(11) UNSIGNED NOT NULL,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `reference` varchar(32) NOT NULL,
  `supplier_reference` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `exchange_rate` decimal(20,6) DEFAULT '0.000000',
  `unit_price_te` decimal(20,6) DEFAULT '0.000000',
  `quantity_expected` int(11) UNSIGNED NOT NULL,
  `quantity_received` int(11) UNSIGNED NOT NULL,
  `price_te` decimal(20,6) DEFAULT '0.000000',
  `discount_rate` decimal(20,6) DEFAULT '0.000000',
  `discount_value_te` decimal(20,6) DEFAULT '0.000000',
  `price_with_discount_te` decimal(20,6) DEFAULT '0.000000',
  `tax_rate` decimal(20,6) DEFAULT '0.000000',
  `tax_value` decimal(20,6) DEFAULT '0.000000',
  `price_ti` decimal(20,6) DEFAULT '0.000000',
  `tax_value_with_order_discount` decimal(20,6) DEFAULT '0.000000',
  `price_with_order_discount_te` decimal(20,6) DEFAULT '0.000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_supply_order_history`
--

CREATE TABLE `ap_supply_order_history` (
  `id_supply_order_history` int(11) UNSIGNED NOT NULL,
  `id_supply_order` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `id_state` int(11) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_supply_order_receipt_history`
--

CREATE TABLE `ap_supply_order_receipt_history` (
  `id_supply_order_receipt_history` int(11) UNSIGNED NOT NULL,
  `id_supply_order_detail` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_supply_order_state`
--

CREATE TABLE `ap_supply_order_state` (
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `delivery_note` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '0',
  `receipt_state` tinyint(1) NOT NULL DEFAULT '0',
  `pending_receipt` tinyint(1) NOT NULL DEFAULT '0',
  `enclosed` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_supply_order_state`
--

INSERT INTO `ap_supply_order_state` (`id_supply_order_state`, `delivery_note`, `editable`, `receipt_state`, `pending_receipt`, `enclosed`, `color`) VALUES
(1, 0, 1, 0, 0, 0, '#faab00'),
(2, 1, 0, 0, 0, 0, '#273cff'),
(3, 0, 0, 0, 1, 0, '#ff37f5'),
(4, 0, 0, 1, 1, 0, '#ff3e33'),
(5, 0, 0, 1, 0, 1, '#00d60c'),
(6, 0, 0, 0, 0, 1, '#666666');

-- --------------------------------------------------------

--
-- Table structure for table `ap_supply_order_state_lang`
--

CREATE TABLE `ap_supply_order_state_lang` (
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_supply_order_state_lang`
--

INSERT INTO `ap_supply_order_state_lang` (`id_supply_order_state`, `id_lang`, `name`) VALUES
(1, 1, '1 - En cours de création'),
(2, 1, '2 - Commande validée'),
(3, 1, '3 - En attente de réception'),
(4, 1, '4 - Commande reçue partiellement'),
(5, 1, '5 - Commande reçue intégralement'),
(6, 1, '6 - Commande annulée');

-- --------------------------------------------------------

--
-- Table structure for table `ap_tab`
--

CREATE TABLE `ap_tab` (
  `id_tab` int(10) UNSIGNED NOT NULL,
  `id_parent` int(11) NOT NULL,
  `class_name` varchar(64) NOT NULL,
  `module` varchar(64) DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `hide_host_mode` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tab`
--

INSERT INTO `ap_tab` (`id_tab`, `id_parent`, `class_name`, `module`, `position`, `active`, `hide_host_mode`) VALUES
(1, 0, 'AdminDashboard', '', 0, 1, 0),
(2, -1, 'AdminCms', '', 0, 1, 0),
(3, -1, 'AdminCmsCategories', '', 1, 1, 0),
(4, -1, 'AdminAttributeGenerator', '', 2, 1, 0),
(5, -1, 'AdminSearch', '', 3, 1, 0),
(6, -1, 'AdminLogin', '', 4, 1, 0),
(7, -1, 'AdminShop', '', 5, 1, 0),
(8, -1, 'AdminShopUrl', '', 6, 1, 0),
(9, 0, 'AdminCatalog', '', 2, 1, 0),
(10, 0, 'AdminParentOrders', '', 3, 1, 0),
(11, 0, 'AdminParentCustomer', '', 4, 1, 0),
(12, 0, 'AdminPriceRule', '', 5, 1, 0),
(13, 0, 'AdminParentModules', '', 6, 1, 0),
(14, 0, 'AdminParentShipping', '', 7, 1, 0),
(15, 0, 'AdminParentLocalization', '', 8, 1, 0),
(16, 0, 'AdminParentPreferences', '', 9, 1, 0),
(17, 0, 'AdminTools', '', 10, 1, 0),
(18, 0, 'AdminAdmin', '', 11, 1, 0),
(19, 0, 'AdminParentStats', '', 12, 1, 0),
(20, 0, 'AdminStock', '', 13, 1, 0),
(21, 9, 'AdminProducts', '', 0, 1, 0),
(22, 9, 'AdminCategories', '', 1, 1, 0),
(23, 9, 'AdminTracking', '', 2, 1, 0),
(24, 9, 'AdminAttributesGroups', '', 3, 1, 0),
(25, 9, 'AdminFeatures', '', 4, 1, 0),
(26, 9, 'AdminManufacturers', '', 5, 1, 0),
(27, 9, 'AdminSuppliers', '', 6, 1, 0),
(28, 9, 'AdminTags', '', 7, 1, 0),
(29, 9, 'AdminAttachments', '', 8, 1, 0),
(30, 10, 'AdminOrders', '', 0, 1, 0),
(31, 10, 'AdminInvoices', '', 1, 1, 0),
(32, 10, 'AdminReturn', '', 2, 1, 0),
(33, 10, 'AdminDeliverySlip', '', 3, 1, 0),
(34, 10, 'AdminSlip', '', 4, 1, 0),
(35, 10, 'AdminStatuses', '', 5, 1, 0),
(36, 10, 'AdminOrderMessage', '', 6, 1, 0),
(37, 11, 'AdminCustomers', '', 0, 1, 0),
(38, 11, 'AdminAddresses', '', 1, 1, 0),
(39, 11, 'AdminGroups', '', 2, 1, 0),
(40, 11, 'AdminCarts', '', 3, 1, 0),
(41, 11, 'AdminCustomerThreads', '', 4, 1, 0),
(42, 11, 'AdminContacts', '', 5, 1, 0),
(43, 11, 'AdminGenders', '', 6, 1, 0),
(44, 11, 'AdminOutstanding', '', 7, 0, 0),
(45, 12, 'AdminCartRules', '', 0, 1, 0),
(46, 12, 'AdminSpecificPriceRule', '', 1, 1, 0),
(47, 12, 'AdminMarketing', '', 2, 1, 0),
(48, 14, 'AdminCarriers', '', 0, 1, 0),
(49, 14, 'AdminShipping', '', 1, 1, 0),
(50, 14, 'AdminCarrierWizard', '', 2, 1, 0),
(51, 15, 'AdminLocalization', '', 0, 1, 0),
(52, 15, 'AdminLanguages', '', 1, 1, 0),
(53, 15, 'AdminZones', '', 2, 1, 0),
(54, 15, 'AdminCountries', '', 3, 1, 0),
(55, 15, 'AdminStates', '', 4, 1, 0),
(56, 15, 'AdminCurrencies', '', 5, 1, 0),
(57, 15, 'AdminTaxes', '', 6, 1, 0),
(58, 15, 'AdminTaxRulesGroup', '', 7, 1, 0),
(59, 15, 'AdminTranslations', '', 8, 1, 0),
(60, 13, 'AdminModules', '', 0, 1, 0),
(61, 13, 'AdminAddonsCatalog', '', 1, 1, 0),
(62, 13, 'AdminModulesPositions', '', 2, 1, 0),
(63, 13, 'AdminPayment', '', 3, 1, 0),
(64, 16, 'AdminPreferences', '', 0, 1, 0),
(65, 16, 'AdminOrderPreferences', '', 1, 1, 0),
(66, 16, 'AdminPPreferences', '', 2, 1, 0),
(67, 16, 'AdminCustomerPreferences', '', 3, 1, 0),
(68, 16, 'AdminThemes', '', 4, 1, 0),
(69, 16, 'AdminMeta', '', 5, 1, 0),
(70, 16, 'AdminCmsContent', '', 6, 1, 0),
(71, 16, 'AdminImages', '', 7, 1, 0),
(72, 16, 'AdminStores', '', 8, 1, 0),
(73, 16, 'AdminSearchConf', '', 9, 1, 0),
(74, 16, 'AdminMaintenance', '', 10, 1, 0),
(75, 16, 'AdminGeolocation', '', 11, 1, 0),
(76, 17, 'AdminInformation', '', 0, 1, 0),
(77, 17, 'AdminPerformance', '', 1, 1, 0),
(78, 17, 'AdminEmails', '', 2, 1, 0),
(79, 17, 'AdminShopGroup', '', 3, 0, 0),
(80, 17, 'AdminImport', '', 4, 1, 0),
(81, 17, 'AdminBackup', '', 5, 1, 0),
(82, 17, 'AdminRequestSql', '', 6, 1, 0),
(83, 17, 'AdminLogs', '', 7, 1, 0),
(84, 17, 'AdminWebservice', '', 8, 1, 0),
(85, 18, 'AdminAdminPreferences', '', 0, 1, 0),
(86, 18, 'AdminQuickAccesses', '', 1, 1, 0),
(87, 18, 'AdminEmployees', '', 2, 1, 0),
(88, 18, 'AdminProfiles', '', 3, 1, 0),
(89, 18, 'AdminAccess', '', 4, 1, 0),
(90, 18, 'AdminTabs', '', 5, 1, 0),
(91, 19, 'AdminStats', '', 0, 1, 0),
(92, 19, 'AdminSearchEngines', '', 1, 1, 0),
(93, 19, 'AdminReferrers', '', 2, 1, 0),
(94, 20, 'AdminWarehouses', '', 0, 1, 0),
(95, 20, 'AdminStockManagement', '', 1, 1, 0),
(96, 20, 'AdminStockMvt', '', 2, 1, 0),
(97, 20, 'AdminStockInstantState', '', 3, 1, 0),
(98, 20, 'AdminStockCover', '', 4, 1, 0),
(99, 20, 'AdminSupplyOrders', '', 5, 1, 0),
(100, 20, 'AdminStockConfiguration', '', 6, 1, 0),
(101, -1, 'AdminBlockCategories', 'blockcategories', 7, 1, 0),
(102, -1, 'AdminDashgoals', 'dashgoals', 8, 1, 0),
(103, -1, 'AdminThemeConfigurator', 'themeconfigurator', 9, 1, 0),
(104, 18, 'AdminGamification', 'gamification', 6, 1, 0),
(105, -1, 'AdminCronJobs', 'cronjobs', 10, 1, 0),
(106, -1, 'AdminAjaxBonSlick', 'bonslick', 11, 1, 0),
(107, 0, 'AdminNpcalendar', '', 1, 1, 0),
(108, 107, 'AdminNpcalendar', '', 1, 1, 0),
(109, 107, 'AdminNpcustomer', '', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_tab_advice`
--

CREATE TABLE `ap_tab_advice` (
  `id_tab` int(11) NOT NULL,
  `id_advice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tab_advice`
--

INSERT INTO `ap_tab_advice` (`id_tab`, `id_advice`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 72);

-- --------------------------------------------------------

--
-- Table structure for table `ap_tab_lang`
--

CREATE TABLE `ap_tab_lang` (
  `id_tab` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tab_lang`
--

INSERT INTO `ap_tab_lang` (`id_tab`, `id_lang`, `name`) VALUES
(1, 1, 'Tableau de Bord'),
(2, 1, 'Pages CMS'),
(3, 1, 'Catégories CMS'),
(4, 1, 'Générateur de déclinaisons'),
(5, 1, 'Recherche'),
(6, 1, 'Connexion'),
(7, 1, 'Boutiques'),
(8, 1, 'URLs de boutique'),
(9, 1, 'Catalogue'),
(10, 1, 'Commandes'),
(11, 1, 'Clients'),
(12, 1, 'Promotions'),
(13, 1, 'Modules et Services'),
(14, 1, 'Transport'),
(15, 1, 'Localisation'),
(16, 1, 'Préférences'),
(17, 1, 'Paramètres avancés'),
(18, 1, 'Administration'),
(19, 1, 'Statistiques'),
(20, 1, 'Stock'),
(21, 1, 'Produits'),
(22, 1, 'Catégories'),
(23, 1, 'Suivi'),
(24, 1, 'Attributs et Valeurs'),
(25, 1, 'Caractéristiques'),
(26, 1, 'Marques'),
(27, 1, 'Fournisseurs'),
(28, 1, 'Tags'),
(29, 1, 'Documents joints'),
(30, 1, 'Commandes'),
(31, 1, 'Factures'),
(32, 1, 'Retours produits'),
(33, 1, 'Bons de livraison'),
(34, 1, 'Avoirs'),
(35, 1, 'Statuts'),
(36, 1, 'Messages prédéfinis'),
(37, 1, 'Clients'),
(38, 1, 'Adresses'),
(39, 1, 'Groupes'),
(40, 1, 'Paniers'),
(41, 1, 'SAV'),
(42, 1, 'Contacts'),
(43, 1, 'Titres de civilité'),
(44, 1, 'Encours'),
(45, 1, 'Règles paniers'),
(46, 1, 'Règles de prix catalogue'),
(47, 1, 'Marketing'),
(48, 1, 'Transporteurs'),
(49, 1, 'Préférences'),
(50, 1, 'Transporteur'),
(51, 1, 'Localisation'),
(52, 1, 'Langues'),
(53, 1, 'Zones'),
(54, 1, 'Pays'),
(55, 1, 'Etats'),
(56, 1, 'Devises'),
(57, 1, 'Taxes'),
(58, 1, 'Règles de taxes'),
(59, 1, 'Traductions'),
(60, 1, 'Modules et Services'),
(61, 1, 'Catalogue de modules et thèmes'),
(62, 1, 'Positions'),
(63, 1, 'Paiement'),
(64, 1, 'Générales'),
(65, 1, 'Commandes'),
(66, 1, 'Produits'),
(67, 1, 'Clients'),
(68, 1, 'Thèmes'),
(69, 1, 'SEO & URLs'),
(70, 1, 'CMS'),
(71, 1, 'Images'),
(72, 1, 'Coordonnées & magasins'),
(73, 1, 'Recherche'),
(74, 1, 'Maintenance'),
(75, 1, 'Géolocalisation'),
(76, 1, 'Informations'),
(77, 1, 'Performances'),
(78, 1, 'Emails'),
(79, 1, 'Multiboutique'),
(80, 1, 'Import'),
(81, 1, 'Sauvegarde BDD'),
(82, 1, 'Gestionnaire SQL'),
(83, 1, 'Log'),
(84, 1, 'Service web'),
(85, 1, 'Préférences'),
(86, 1, 'Accès rapide'),
(87, 1, 'Employés'),
(88, 1, 'Profils'),
(89, 1, 'Permissions'),
(90, 1, 'Menus'),
(91, 1, 'Statistiques'),
(92, 1, 'Moteurs de recherche'),
(93, 1, 'Sites affluents'),
(94, 1, 'Entrepôts'),
(95, 1, 'Gestion du stock'),
(96, 1, 'Mouvements de Stock'),
(97, 1, 'Etat instantané du stock'),
(98, 1, 'Couverture du stock'),
(99, 1, 'Commandes fournisseurs'),
(100, 1, 'Configuration'),
(101, 1, 'BlockCategories'),
(102, 1, 'Dashgoals'),
(103, 1, 'themeconfigurator'),
(104, 1, 'Merchant Expertise'),
(105, 1, 'Cron Jobs'),
(106, 1, 'bonslick'),
(107, 1, 'AutoPermis.re'),
(108, 1, 'Calendrier'),
(109, 1, 'Documents clients');

-- --------------------------------------------------------

--
-- Table structure for table `ap_tab_module_preference`
--

CREATE TABLE `ap_tab_module_preference` (
  `id_tab_module_preference` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `module` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_tag`
--

CREATE TABLE `ap_tag` (
  `id_tag` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tag`
--

INSERT INTO `ap_tag` (`id_tag`, `id_lang`, `name`) VALUES
(26, 1, 'double commande'),
(29, 1, 'auto école'),
(31, 1, 'location de voiture à double com'),
(32, 1, 'candidat libre'),
(33, 1, 'permis de conduire'),
(34, 1, 'permis annulé'),
(36, 1, 'permis B'),
(41, 1, 'conduite'),
(43, 1, 'examen du permis'),
(50, 1, 'la Réunion 974'),
(68, 1, 'EXAMEN'),
(75, 1, 'forfait 1 semaine - 50 heures'),
(76, 1, 'forfait 1 jour'),
(112, 1, 'forfait libre 5 heures'),
(113, 1, 'forfait'),
(114, 1, 'une'),
(115, 1, 'heure'),
(116, 1, 'examen pratique'),
(117, 1, 'location de voiure'),
(118, 1, 'candidat libre au permis'),
(119, 1, 'forfait 50 heures'),
(120, 1, '974 La Réunion'),
(133, 1, 'auto-école'),
(135, 1, 'perfectionnement'),
(137, 1, 'la Réunion'),
(138, 1, '974'),
(140, 1, 'forfait 20 heures - 2 jours'),
(154, 1, 'voiture à double commande'),
(155, 1, 'permis moins cher'),
(157, 1, 'location voiture à double comman');

-- --------------------------------------------------------

--
-- Table structure for table `ap_tag_count`
--

CREATE TABLE `ap_tag_count` (
  `id_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_tag` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_lang` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `counter` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tag_count`
--

INSERT INTO `ap_tag_count` (`id_group`, `id_tag`, `id_lang`, `id_shop`, `counter`) VALUES
(0, 68, 1, 1, 1),
(0, 75, 1, 1, 1),
(0, 76, 1, 1, 1),
(0, 112, 1, 1, 1),
(0, 113, 1, 1, 1),
(0, 114, 1, 1, 1),
(0, 115, 1, 1, 1),
(0, 117, 1, 1, 1),
(0, 118, 1, 1, 1),
(0, 119, 1, 1, 1),
(0, 120, 1, 1, 1),
(0, 140, 1, 1, 1),
(0, 154, 1, 1, 1),
(0, 155, 1, 1, 1),
(0, 157, 1, 1, 1),
(0, 50, 1, 1, 2),
(0, 116, 1, 1, 2),
(0, 137, 1, 1, 2),
(0, 138, 1, 1, 2),
(0, 133, 1, 1, 3),
(0, 135, 1, 1, 3),
(0, 26, 1, 1, 4),
(0, 43, 1, 1, 4),
(0, 29, 1, 1, 6),
(0, 31, 1, 1, 7),
(0, 34, 1, 1, 7),
(0, 41, 1, 1, 7),
(0, 36, 1, 1, 8),
(0, 32, 1, 1, 9),
(0, 33, 1, 1, 9),
(1, 68, 1, 1, 1),
(1, 75, 1, 1, 1),
(1, 76, 1, 1, 1),
(1, 112, 1, 1, 1),
(1, 113, 1, 1, 1),
(1, 114, 1, 1, 1),
(1, 115, 1, 1, 1),
(1, 117, 1, 1, 1),
(1, 118, 1, 1, 1),
(1, 119, 1, 1, 1),
(1, 120, 1, 1, 1),
(1, 140, 1, 1, 1),
(1, 154, 1, 1, 1),
(1, 155, 1, 1, 1),
(1, 157, 1, 1, 1),
(1, 50, 1, 1, 2),
(1, 116, 1, 1, 2),
(1, 137, 1, 1, 2),
(1, 138, 1, 1, 2),
(1, 133, 1, 1, 3),
(1, 135, 1, 1, 3),
(1, 26, 1, 1, 4),
(1, 43, 1, 1, 4),
(1, 29, 1, 1, 6),
(1, 31, 1, 1, 7),
(1, 34, 1, 1, 7),
(1, 41, 1, 1, 7),
(1, 36, 1, 1, 8),
(1, 32, 1, 1, 9),
(1, 33, 1, 1, 9),
(2, 68, 1, 1, 1),
(2, 75, 1, 1, 1),
(2, 76, 1, 1, 1),
(2, 112, 1, 1, 1),
(2, 113, 1, 1, 1),
(2, 114, 1, 1, 1),
(2, 115, 1, 1, 1),
(2, 117, 1, 1, 1),
(2, 118, 1, 1, 1),
(2, 119, 1, 1, 1),
(2, 120, 1, 1, 1),
(2, 140, 1, 1, 1),
(2, 154, 1, 1, 1),
(2, 155, 1, 1, 1),
(2, 157, 1, 1, 1),
(2, 50, 1, 1, 2),
(2, 116, 1, 1, 2),
(2, 137, 1, 1, 2),
(2, 138, 1, 1, 2),
(2, 133, 1, 1, 3),
(2, 135, 1, 1, 3),
(2, 26, 1, 1, 4),
(2, 43, 1, 1, 4),
(2, 29, 1, 1, 6),
(2, 31, 1, 1, 7),
(2, 34, 1, 1, 7),
(2, 41, 1, 1, 7),
(2, 36, 1, 1, 8),
(2, 32, 1, 1, 9),
(2, 33, 1, 1, 9),
(3, 68, 1, 1, 1),
(3, 75, 1, 1, 1),
(3, 76, 1, 1, 1),
(3, 112, 1, 1, 1),
(3, 113, 1, 1, 1),
(3, 114, 1, 1, 1),
(3, 115, 1, 1, 1),
(3, 117, 1, 1, 1),
(3, 118, 1, 1, 1),
(3, 119, 1, 1, 1),
(3, 120, 1, 1, 1),
(3, 140, 1, 1, 1),
(3, 154, 1, 1, 1),
(3, 155, 1, 1, 1),
(3, 157, 1, 1, 1),
(3, 50, 1, 1, 2),
(3, 116, 1, 1, 2),
(3, 137, 1, 1, 2),
(3, 138, 1, 1, 2),
(3, 133, 1, 1, 3),
(3, 135, 1, 1, 3),
(3, 26, 1, 1, 4),
(3, 43, 1, 1, 4),
(3, 29, 1, 1, 6),
(3, 31, 1, 1, 7),
(3, 34, 1, 1, 7),
(3, 41, 1, 1, 7),
(3, 36, 1, 1, 8),
(3, 32, 1, 1, 9),
(3, 33, 1, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `ap_tax`
--

CREATE TABLE `ap_tax` (
  `id_tax` int(10) UNSIGNED NOT NULL,
  `rate` decimal(10,3) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tax`
--

INSERT INTO `ap_tax` (`id_tax`, `rate`, `active`, `deleted`) VALUES
(1, '8.500', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_tax_lang`
--

CREATE TABLE `ap_tax_lang` (
  `id_tax` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tax_lang`
--

INSERT INTO `ap_tax_lang` (`id_tax`, `id_lang`, `name`) VALUES
(1, 1, 'TVA RE 8.5%');

-- --------------------------------------------------------

--
-- Table structure for table `ap_tax_rule`
--

CREATE TABLE `ap_tax_rule` (
  `id_tax_rule` int(11) NOT NULL,
  `id_tax_rules_group` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `zipcode_from` varchar(12) NOT NULL,
  `zipcode_to` varchar(12) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `behavior` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tax_rule`
--

INSERT INTO `ap_tax_rule` (`id_tax_rule`, `id_tax_rules_group`, `id_country`, `id_state`, `zipcode_from`, `zipcode_to`, `id_tax`, `behavior`, `description`) VALUES
(346, 1, 231, 0, '0', '0', 1, 0, ''),
(347, 1, 30, 0, '0', '0', 1, 0, ''),
(348, 1, 244, 0, '0', '0', 1, 0, ''),
(349, 1, 230, 0, '0', '0', 1, 0, ''),
(350, 1, 38, 0, '0', '0', 1, 0, ''),
(351, 1, 1, 0, '0', '0', 1, 0, ''),
(352, 1, 40, 0, '0', '0', 1, 0, ''),
(353, 1, 41, 0, '0', '0', 1, 0, ''),
(354, 1, 42, 0, '0', '0', 1, 0, ''),
(355, 1, 232, 0, '0', '0', 1, 0, ''),
(356, 1, 43, 0, '0', '0', 1, 0, ''),
(357, 1, 157, 0, '0', '0', 1, 0, ''),
(358, 1, 188, 0, '0', '0', 1, 0, ''),
(359, 1, 44, 0, '0', '0', 1, 0, ''),
(360, 1, 45, 0, '0', '0', 1, 0, ''),
(361, 1, 46, 0, '0', '0', 1, 0, ''),
(362, 1, 24, 0, '0', '0', 1, 0, ''),
(363, 1, 2, 0, '0', '0', 1, 0, ''),
(364, 1, 47, 0, '0', '0', 1, 0, ''),
(365, 1, 48, 0, '0', '0', 1, 0, ''),
(366, 1, 49, 0, '0', '0', 1, 0, ''),
(367, 1, 50, 0, '0', '0', 1, 0, ''),
(368, 1, 51, 0, '0', '0', 1, 0, ''),
(369, 1, 52, 0, '0', '0', 1, 0, ''),
(370, 1, 3, 0, '0', '0', 1, 0, ''),
(371, 1, 53, 0, '0', '0', 1, 0, ''),
(372, 1, 54, 0, '0', '0', 1, 0, ''),
(373, 1, 55, 0, '0', '0', 1, 0, ''),
(374, 1, 56, 0, '0', '0', 1, 0, ''),
(375, 1, 34, 0, '0', '0', 1, 0, ''),
(376, 1, 233, 0, '0', '0', 1, 0, ''),
(377, 1, 57, 0, '0', '0', 1, 0, ''),
(378, 1, 234, 0, '0', '0', 1, 0, ''),
(379, 1, 58, 0, '0', '0', 1, 0, ''),
(380, 1, 59, 0, '0', '0', 1, 0, ''),
(381, 1, 236, 0, '0', '0', 1, 0, ''),
(382, 1, 60, 0, '0', '0', 1, 0, ''),
(383, 1, 61, 0, '0', '0', 1, 0, ''),
(384, 1, 62, 0, '0', '0', 1, 0, ''),
(385, 1, 237, 0, '0', '0', 1, 0, ''),
(386, 1, 63, 0, '0', '0', 1, 0, ''),
(387, 1, 64, 0, '0', '0', 1, 0, ''),
(388, 1, 4, 0, '0', '0', 1, 0, ''),
(389, 1, 65, 0, '0', '0', 1, 0, ''),
(390, 1, 66, 0, '0', '0', 1, 0, ''),
(391, 1, 68, 0, '0', '0', 1, 0, ''),
(392, 1, 5, 0, '0', '0', 1, 0, ''),
(393, 1, 238, 0, '0', '0', 1, 0, ''),
(394, 1, 76, 0, '0', '0', 1, 0, ''),
(395, 1, 239, 0, '0', '0', 1, 0, ''),
(396, 1, 69, 0, '0', '0', 1, 0, ''),
(397, 1, 70, 0, '0', '0', 1, 0, ''),
(398, 1, 72, 0, '0', '0', 1, 0, ''),
(399, 1, 71, 0, '0', '0', 1, 0, ''),
(400, 1, 240, 0, '0', '0', 1, 0, ''),
(401, 1, 28, 0, '0', '0', 1, 0, ''),
(402, 1, 121, 0, '0', '0', 1, 0, ''),
(403, 1, 73, 0, '0', '0', 1, 0, ''),
(404, 1, 32, 0, '0', '0', 1, 0, ''),
(405, 1, 74, 0, '0', '0', 1, 0, ''),
(406, 1, 75, 0, '0', '0', 1, 0, ''),
(407, 1, 20, 0, '0', '0', 1, 0, ''),
(408, 1, 77, 0, '0', '0', 1, 0, ''),
(409, 1, 78, 0, '0', '0', 1, 0, ''),
(410, 1, 82, 0, '0', '0', 1, 0, ''),
(411, 1, 83, 0, '0', '0', 1, 0, ''),
(412, 1, 217, 0, '0', '0', 1, 0, ''),
(413, 1, 81, 0, '0', '0', 1, 0, ''),
(414, 1, 85, 0, '0', '0', 1, 0, ''),
(415, 1, 6, 0, '0', '0', 1, 0, ''),
(416, 1, 86, 0, '0', '0', 1, 0, ''),
(417, 1, 21, 0, '0', '0', 1, 0, ''),
(418, 1, 87, 0, '0', '0', 1, 0, ''),
(419, 1, 88, 0, '0', '0', 1, 0, ''),
(420, 1, 89, 0, '0', '0', 1, 0, ''),
(421, 1, 90, 0, '0', '0', 1, 0, ''),
(422, 1, 7, 0, '0', '0', 1, 0, ''),
(423, 1, 8, 0, '0', '0', 1, 0, ''),
(424, 1, 91, 0, '0', '0', 1, 0, ''),
(425, 1, 92, 0, '0', '0', 1, 0, ''),
(426, 1, 93, 0, '0', '0', 1, 0, ''),
(427, 1, 196, 0, '0', '0', 1, 0, ''),
(428, 1, 94, 0, '0', '0', 1, 0, ''),
(429, 1, 97, 0, '0', '0', 1, 0, ''),
(430, 1, 9, 0, '0', '0', 1, 0, ''),
(431, 1, 95, 0, '0', '0', 1, 0, ''),
(432, 1, 96, 0, '0', '0', 1, 0, ''),
(433, 1, 98, 0, '0', '0', 1, 0, ''),
(434, 1, 99, 0, '0', '0', 1, 0, ''),
(435, 1, 100, 0, '0', '0', 1, 0, ''),
(436, 1, 101, 0, '0', '0', 1, 0, ''),
(437, 1, 102, 0, '0', '0', 1, 0, ''),
(438, 1, 84, 0, '0', '0', 1, 0, ''),
(439, 1, 103, 0, '0', '0', 1, 0, ''),
(440, 1, 104, 0, '0', '0', 1, 0, ''),
(441, 1, 241, 0, '0', '0', 1, 0, ''),
(442, 1, 105, 0, '0', '0', 1, 0, ''),
(443, 1, 106, 0, '0', '0', 1, 0, ''),
(444, 1, 108, 0, '0', '0', 1, 0, ''),
(445, 1, 22, 0, '0', '0', 1, 0, ''),
(446, 1, 143, 0, '0', '0', 1, 0, ''),
(447, 1, 35, 0, '0', '0', 1, 0, ''),
(448, 1, 223, 0, '0', '0', 1, 0, ''),
(449, 1, 224, 0, '0', '0', 1, 0, ''),
(450, 1, 110, 0, '0', '0', 1, 0, ''),
(451, 1, 111, 0, '0', '0', 1, 0, ''),
(452, 1, 112, 0, '0', '0', 1, 0, ''),
(453, 1, 113, 0, '0', '0', 1, 0, ''),
(454, 1, 26, 0, '0', '0', 1, 0, ''),
(455, 1, 109, 0, '0', '0', 1, 0, ''),
(456, 1, 29, 0, '0', '0', 1, 0, ''),
(457, 1, 10, 0, '0', '0', 1, 0, ''),
(458, 1, 115, 0, '0', '0', 1, 0, ''),
(459, 1, 11, 0, '0', '0', 1, 0, ''),
(460, 1, 116, 0, '0', '0', 1, 0, ''),
(461, 1, 117, 0, '0', '0', 1, 0, ''),
(462, 1, 118, 0, '0', '0', 1, 0, ''),
(463, 1, 119, 0, '0', '0', 1, 0, ''),
(464, 1, 123, 0, '0', '0', 1, 0, ''),
(465, 1, 120, 0, '0', '0', 1, 0, ''),
(466, 1, 122, 0, '0', '0', 1, 0, ''),
(467, 1, 124, 0, '0', '0', 1, 0, ''),
(468, 1, 127, 0, '0', '0', 1, 0, ''),
(469, 1, 125, 0, '0', '0', 1, 0, ''),
(470, 1, 126, 0, '0', '0', 1, 0, ''),
(471, 1, 128, 0, '0', '0', 1, 0, ''),
(472, 1, 129, 0, '0', '0', 1, 0, ''),
(473, 1, 130, 0, '0', '0', 1, 0, ''),
(474, 1, 131, 0, '0', '0', 1, 0, ''),
(475, 1, 12, 0, '0', '0', 1, 0, ''),
(476, 1, 132, 0, '0', '0', 1, 0, ''),
(477, 1, 133, 0, '0', '0', 1, 0, ''),
(478, 1, 134, 0, '0', '0', 1, 0, ''),
(479, 1, 136, 0, '0', '0', 1, 0, ''),
(480, 1, 135, 0, '0', '0', 1, 0, ''),
(481, 1, 137, 0, '0', '0', 1, 0, ''),
(482, 1, 138, 0, '0', '0', 1, 0, ''),
(483, 1, 139, 0, '0', '0', 1, 0, ''),
(484, 1, 114, 0, '0', '0', 1, 0, ''),
(485, 1, 163, 0, '0', '0', 1, 0, ''),
(486, 1, 152, 0, '0', '0', 1, 0, ''),
(487, 1, 140, 0, '0', '0', 1, 0, ''),
(488, 1, 141, 0, '0', '0', 1, 0, ''),
(489, 1, 142, 0, '0', '0', 1, 0, ''),
(490, 1, 144, 0, '0', '0', 1, 0, ''),
(491, 1, 145, 0, '0', '0', 1, 0, ''),
(492, 1, 146, 0, '0', '0', 1, 0, ''),
(493, 1, 147, 0, '0', '0', 1, 0, ''),
(494, 1, 148, 0, '0', '0', 1, 0, ''),
(495, 1, 149, 0, '0', '0', 1, 0, ''),
(496, 1, 150, 0, '0', '0', 1, 0, ''),
(497, 1, 151, 0, '0', '0', 1, 0, ''),
(498, 1, 153, 0, '0', '0', 1, 0, ''),
(499, 1, 154, 0, '0', '0', 1, 0, ''),
(500, 1, 155, 0, '0', '0', 1, 0, ''),
(501, 1, 156, 0, '0', '0', 1, 0, ''),
(502, 1, 159, 0, '0', '0', 1, 0, ''),
(503, 1, 160, 0, '0', '0', 1, 0, ''),
(504, 1, 31, 0, '0', '0', 1, 0, ''),
(505, 1, 161, 0, '0', '0', 1, 0, ''),
(506, 1, 162, 0, '0', '0', 1, 0, ''),
(507, 1, 23, 0, '0', '0', 1, 0, ''),
(508, 1, 158, 0, '0', '0', 1, 0, ''),
(509, 1, 27, 0, '0', '0', 1, 0, ''),
(510, 1, 235, 0, '0', '0', 1, 0, ''),
(511, 1, 164, 0, '0', '0', 1, 0, ''),
(512, 1, 215, 0, '0', '0', 1, 0, ''),
(513, 1, 219, 0, '0', '0', 1, 0, ''),
(514, 1, 165, 0, '0', '0', 1, 0, ''),
(515, 1, 166, 0, '0', '0', 1, 0, ''),
(516, 1, 167, 0, '0', '0', 1, 0, ''),
(517, 1, 168, 0, '0', '0', 1, 0, ''),
(518, 1, 169, 0, '0', '0', 1, 0, ''),
(519, 1, 170, 0, '0', '0', 1, 0, ''),
(520, 1, 13, 0, '0', '0', 1, 0, ''),
(521, 1, 171, 0, '0', '0', 1, 0, ''),
(522, 1, 172, 0, '0', '0', 1, 0, ''),
(523, 1, 173, 0, '0', '0', 1, 0, ''),
(524, 1, 14, 0, '0', '0', 1, 0, ''),
(525, 1, 242, 0, '0', '0', 1, 0, ''),
(526, 1, 174, 0, '0', '0', 1, 0, ''),
(527, 1, 15, 0, '0', '0', 1, 0, ''),
(528, 1, 175, 0, '0', '0', 1, 0, ''),
(529, 1, 79, 0, '0', '0', 1, 0, ''),
(530, 1, 16, 0, '0', '0', 1, 0, ''),
(531, 1, 176, 0, '0', '0', 1, 0, ''),
(532, 1, 36, 0, '0', '0', 1, 0, ''),
(533, 1, 17, 0, '0', '0', 1, 0, ''),
(534, 1, 177, 0, '0', '0', 1, 0, ''),
(535, 1, 178, 0, '0', '0', 1, 0, ''),
(536, 1, 226, 0, '0', '0', 1, 0, ''),
(537, 1, 179, 0, '0', '0', 1, 0, ''),
(538, 1, 180, 0, '0', '0', 1, 0, ''),
(539, 1, 186, 0, '0', '0', 1, 0, ''),
(540, 1, 182, 0, '0', '0', 1, 0, ''),
(541, 1, 183, 0, '0', '0', 1, 0, ''),
(542, 1, 107, 0, '0', '0', 1, 0, ''),
(543, 1, 184, 0, '0', '0', 1, 0, ''),
(544, 1, 181, 0, '0', '0', 1, 0, ''),
(545, 1, 194, 0, '0', '0', 1, 0, ''),
(546, 1, 185, 0, '0', '0', 1, 0, ''),
(547, 1, 39, 0, '0', '0', 1, 0, ''),
(548, 1, 187, 0, '0', '0', 1, 0, ''),
(549, 1, 189, 0, '0', '0', 1, 0, ''),
(550, 1, 190, 0, '0', '0', 1, 0, ''),
(551, 1, 191, 0, '0', '0', 1, 0, ''),
(552, 1, 192, 0, '0', '0', 1, 0, ''),
(553, 1, 25, 0, '0', '0', 1, 0, ''),
(554, 1, 37, 0, '0', '0', 1, 0, ''),
(555, 1, 193, 0, '0', '0', 1, 0, ''),
(556, 1, 195, 0, '0', '0', 1, 0, ''),
(557, 1, 198, 0, '0', '0', 1, 0, ''),
(558, 1, 197, 0, '0', '0', 1, 0, ''),
(559, 1, 18, 0, '0', '0', 1, 0, ''),
(560, 1, 19, 0, '0', '0', 1, 0, ''),
(561, 1, 199, 0, '0', '0', 1, 0, ''),
(562, 1, 200, 0, '0', '0', 1, 0, ''),
(563, 1, 201, 0, '0', '0', 1, 0, ''),
(564, 1, 202, 0, '0', '0', 1, 0, ''),
(565, 1, 204, 0, '0', '0', 1, 0, ''),
(566, 1, 203, 0, '0', '0', 1, 0, ''),
(567, 1, 205, 0, '0', '0', 1, 0, ''),
(568, 1, 67, 0, '0', '0', 1, 0, ''),
(569, 1, 243, 0, '0', '0', 1, 0, ''),
(570, 1, 206, 0, '0', '0', 1, 0, ''),
(571, 1, 80, 0, '0', '0', 1, 0, ''),
(572, 1, 33, 0, '0', '0', 1, 0, ''),
(573, 1, 207, 0, '0', '0', 1, 0, ''),
(574, 1, 208, 0, '0', '0', 1, 0, ''),
(575, 1, 209, 0, '0', '0', 1, 0, ''),
(576, 1, 210, 0, '0', '0', 1, 0, ''),
(577, 1, 212, 0, '0', '0', 1, 0, ''),
(578, 1, 213, 0, '0', '0', 1, 0, ''),
(579, 1, 211, 0, '0', '0', 1, 0, ''),
(580, 1, 214, 0, '0', '0', 1, 0, ''),
(581, 1, 216, 0, '0', '0', 1, 0, ''),
(582, 1, 218, 0, '0', '0', 1, 0, ''),
(583, 1, 220, 0, '0', '0', 1, 0, ''),
(584, 1, 221, 0, '0', '0', 1, 0, ''),
(585, 1, 222, 0, '0', '0', 1, 0, ''),
(586, 1, 225, 0, '0', '0', 1, 0, ''),
(587, 1, 227, 0, '0', '0', 1, 0, ''),
(588, 1, 228, 0, '0', '0', 1, 0, ''),
(589, 1, 229, 0, '0', '0', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_tax_rules_group`
--

CREATE TABLE `ap_tax_rules_group` (
  `id_tax_rules_group` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tax_rules_group`
--

INSERT INTO `ap_tax_rules_group` (`id_tax_rules_group`, `name`, `active`, `deleted`, `date_add`, `date_upd`) VALUES
(1, 'TVA 8.5% (RE)', 1, 0, '2019-06-10 15:15:33', '2019-06-10 20:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `ap_tax_rules_group_shop`
--

CREATE TABLE `ap_tax_rules_group_shop` (
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_tax_rules_group_shop`
--

INSERT INTO `ap_tax_rules_group_shop` (`id_tax_rules_group`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_theme`
--

CREATE TABLE `ap_theme` (
  `id_theme` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `directory` varchar(64) NOT NULL,
  `responsive` tinyint(1) NOT NULL DEFAULT '0',
  `default_left_column` tinyint(1) NOT NULL DEFAULT '0',
  `default_right_column` tinyint(1) NOT NULL DEFAULT '0',
  `product_per_page` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_theme`
--

INSERT INTO `ap_theme` (`id_theme`, `name`, `directory`, `responsive`, `default_left_column`, `default_right_column`, `product_per_page`) VALUES
(1, 'default-bootstrap', 'default-bootstrap', 1, 1, 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `ap_themeconfigurator`
--

CREATE TABLE `ap_themeconfigurator` (
  `id_item` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `item_order` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `title_use` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `hook` varchar(100) DEFAULT NULL,
  `url` text,
  `target` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `image_w` varchar(10) DEFAULT NULL,
  `image_h` varchar(10) DEFAULT NULL,
  `html` text,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_themeconfigurator`
--

INSERT INTO `ap_themeconfigurator` (`id_item`, `id_shop`, `id_lang`, `item_order`, `title`, `title_use`, `hook`, `url`, `target`, `image`, `image_w`, `image_h`, `html`, `active`) VALUES
(2, 1, 1, 2, '', 0, 'home', 'index.php?id_cms=8&controller=cms&id_lang=1', 1, 'a0152ef3cf7106b9a02dd53cba6edeef6a9bc610_les-conditions-generales-de-vente-1.jpg', '383', '267', '', 1),
(3, 1, 1, 3, '', 0, 'home', 'index.php?id_cms=10&controller=cms&id_lang=1', 1, 'e8c0f49343c9f3bc25a18ab65a1df3e427ef6a9c_geolocalisation-2.jpg', '383', '267', '', 1),
(8, 1, 2, 1, '', 0, 'home', 'http://www.prestashop.com/', 0, '18fab512e1fa63038e374b97dd771fb707fb2791_download.jpeg', '383', '267', '', 1),
(9, 1, 2, 2, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img2.jpg', '383', '267', '', 1),
(10, 1, 2, 3, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img3.jpg', '383', '267', '', 1),
(11, 1, 2, 4, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img4.jpg', '383', '142', '', 1),
(12, 1, 2, 5, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img5.jpg', '777', '142', '', 1),
(13, 1, 2, 6, '', 0, 'top', 'http://www.prestashop.com/', 0, 'banner-img6.jpg', '381', '219', '', 1),
(14, 1, 2, 7, '', 0, 'top', 'http://www.prestashop.com/', 0, 'banner-img7.jpg', '381', '219', '', 1),
(16, 1, 1, 4, '', 0, 'home', 'index.php?id_cms=7&controller=cms&id_lang=1', 1, '9afc55adb25d222b38a611bfce2b8ba844a2c8cd_groupama-assurance-auto.png', '383', '142', '', 1),
(17, 1, 1, 5, '', 0, 'home', 'index.php?id_cms=11&controller=cms&id_lang=1', 1, '1f76bec6424a5efdfa1e0e5ad93541f01dc19508_femme-en-candidat-libre-1.jpg', '777', '142', '', 1),
(22, 1, 1, 1, '', 0, 'home', 'index.php?id_cms=6&controller=cms&id_lang=1', 0, '51f1864e15ed549fb021bd962f41a37be2dc5c14_contrat-image-info-2.jpg', '383', '267', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_theme_meta`
--

CREATE TABLE `ap_theme_meta` (
  `id_theme_meta` int(11) NOT NULL,
  `id_theme` int(11) NOT NULL,
  `id_meta` int(10) UNSIGNED NOT NULL,
  `left_column` tinyint(1) NOT NULL DEFAULT '1',
  `right_column` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_theme_meta`
--

INSERT INTO `ap_theme_meta` (`id_theme_meta`, `id_theme`, `id_meta`, `left_column`, `right_column`) VALUES
(1, 1, 1, 0, 0),
(2, 1, 2, 1, 0),
(3, 1, 3, 0, 0),
(4, 1, 4, 0, 0),
(5, 1, 5, 1, 0),
(6, 1, 6, 1, 0),
(7, 1, 7, 0, 0),
(8, 1, 8, 1, 0),
(9, 1, 9, 1, 0),
(10, 1, 10, 0, 0),
(11, 1, 11, 0, 0),
(12, 1, 12, 0, 0),
(13, 1, 13, 0, 0),
(14, 1, 14, 0, 0),
(15, 1, 15, 0, 0),
(16, 1, 16, 0, 0),
(17, 1, 17, 0, 0),
(18, 1, 18, 0, 0),
(19, 1, 19, 0, 0),
(20, 1, 20, 0, 0),
(21, 1, 21, 0, 0),
(22, 1, 22, 1, 0),
(23, 1, 23, 0, 0),
(24, 1, 24, 0, 0),
(25, 1, 25, 0, 0),
(26, 1, 26, 0, 0),
(27, 1, 28, 1, 0),
(28, 1, 29, 0, 0),
(29, 1, 27, 0, 0),
(30, 1, 30, 0, 0),
(31, 1, 31, 0, 0),
(32, 1, 32, 0, 0),
(33, 1, 33, 0, 0),
(34, 1, 34, 0, 0),
(35, 1, 36, 1, 0),
(36, 1, 37, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_theme_specific`
--

CREATE TABLE `ap_theme_specific` (
  `id_theme` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `entity` int(11) UNSIGNED NOT NULL,
  `id_object` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_timezone`
--

CREATE TABLE `ap_timezone` (
  `id_timezone` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_timezone`
--

INSERT INTO `ap_timezone` (`id_timezone`, `name`) VALUES
(1, 'Africa/Abidjan'),
(2, 'Africa/Accra'),
(3, 'Africa/Addis_Ababa'),
(4, 'Africa/Algiers'),
(5, 'Africa/Asmara'),
(6, 'Africa/Asmera'),
(7, 'Africa/Bamako'),
(8, 'Africa/Bangui'),
(9, 'Africa/Banjul'),
(10, 'Africa/Bissau'),
(11, 'Africa/Blantyre'),
(12, 'Africa/Brazzaville'),
(13, 'Africa/Bujumbura'),
(14, 'Africa/Cairo'),
(15, 'Africa/Casablanca'),
(16, 'Africa/Ceuta'),
(17, 'Africa/Conakry'),
(18, 'Africa/Dakar'),
(19, 'Africa/Dar_es_Salaam'),
(20, 'Africa/Djibouti'),
(21, 'Africa/Douala'),
(22, 'Africa/El_Aaiun'),
(23, 'Africa/Freetown'),
(24, 'Africa/Gaborone'),
(25, 'Africa/Harare'),
(26, 'Africa/Johannesburg'),
(27, 'Africa/Kampala'),
(28, 'Africa/Khartoum'),
(29, 'Africa/Kigali'),
(30, 'Africa/Kinshasa'),
(31, 'Africa/Lagos'),
(32, 'Africa/Libreville'),
(33, 'Africa/Lome'),
(34, 'Africa/Luanda'),
(35, 'Africa/Lubumbashi'),
(36, 'Africa/Lusaka'),
(37, 'Africa/Malabo'),
(38, 'Africa/Maputo'),
(39, 'Africa/Maseru'),
(40, 'Africa/Mbabane'),
(41, 'Africa/Mogadishu'),
(42, 'Africa/Monrovia'),
(43, 'Africa/Nairobi'),
(44, 'Africa/Ndjamena'),
(45, 'Africa/Niamey'),
(46, 'Africa/Nouakchott'),
(47, 'Africa/Ouagadougou'),
(48, 'Africa/Porto-Novo'),
(49, 'Africa/Sao_Tome'),
(50, 'Africa/Timbuktu'),
(51, 'Africa/Tripoli'),
(52, 'Africa/Tunis'),
(53, 'Africa/Windhoek'),
(54, 'America/Adak'),
(55, 'America/Anchorage '),
(56, 'America/Anguilla'),
(57, 'America/Antigua'),
(58, 'America/Araguaina'),
(59, 'America/Argentina/Buenos_Aires'),
(60, 'America/Argentina/Catamarca'),
(61, 'America/Argentina/ComodRivadavia'),
(62, 'America/Argentina/Cordoba'),
(63, 'America/Argentina/Jujuy'),
(64, 'America/Argentina/La_Rioja'),
(65, 'America/Argentina/Mendoza'),
(66, 'America/Argentina/Rio_Gallegos'),
(67, 'America/Argentina/Salta'),
(68, 'America/Argentina/San_Juan'),
(69, 'America/Argentina/San_Luis'),
(70, 'America/Argentina/Tucuman'),
(71, 'America/Argentina/Ushuaia'),
(72, 'America/Aruba'),
(73, 'America/Asuncion'),
(74, 'America/Atikokan'),
(75, 'America/Atka'),
(76, 'America/Bahia'),
(77, 'America/Barbados'),
(78, 'America/Belem'),
(79, 'America/Belize'),
(80, 'America/Blanc-Sablon'),
(81, 'America/Boa_Vista'),
(82, 'America/Bogota'),
(83, 'America/Boise'),
(84, 'America/Buenos_Aires'),
(85, 'America/Cambridge_Bay'),
(86, 'America/Campo_Grande'),
(87, 'America/Cancun'),
(88, 'America/Caracas'),
(89, 'America/Catamarca'),
(90, 'America/Cayenne'),
(91, 'America/Cayman'),
(92, 'America/Chicago'),
(93, 'America/Chihuahua'),
(94, 'America/Coral_Harbour'),
(95, 'America/Cordoba'),
(96, 'America/Costa_Rica'),
(97, 'America/Cuiaba'),
(98, 'America/Curacao'),
(99, 'America/Danmarkshavn'),
(100, 'America/Dawson'),
(101, 'America/Dawson_Creek'),
(102, 'America/Denver'),
(103, 'America/Detroit'),
(104, 'America/Dominica'),
(105, 'America/Edmonton'),
(106, 'America/Eirunepe'),
(107, 'America/El_Salvador'),
(108, 'America/Ensenada'),
(109, 'America/Fort_Wayne'),
(110, 'America/Fortaleza'),
(111, 'America/Glace_Bay'),
(112, 'America/Godthab'),
(113, 'America/Goose_Bay'),
(114, 'America/Grand_Turk'),
(115, 'America/Grenada'),
(116, 'America/Guadeloupe'),
(117, 'America/Guatemala'),
(118, 'America/Guayaquil'),
(119, 'America/Guyana'),
(120, 'America/Halifax'),
(121, 'America/Havana'),
(122, 'America/Hermosillo'),
(123, 'America/Indiana/Indianapolis'),
(124, 'America/Indiana/Knox'),
(125, 'America/Indiana/Marengo'),
(126, 'America/Indiana/Petersburg'),
(127, 'America/Indiana/Tell_City'),
(128, 'America/Indiana/Vevay'),
(129, 'America/Indiana/Vincennes'),
(130, 'America/Indiana/Winamac'),
(131, 'America/Indianapolis'),
(132, 'America/Inuvik'),
(133, 'America/Iqaluit'),
(134, 'America/Jamaica'),
(135, 'America/Jujuy'),
(136, 'America/Juneau'),
(137, 'America/Kentucky/Louisville'),
(138, 'America/Kentucky/Monticello'),
(139, 'America/Knox_IN'),
(140, 'America/La_Paz'),
(141, 'America/Lima'),
(142, 'America/Los_Angeles'),
(143, 'America/Louisville'),
(144, 'America/Maceio'),
(145, 'America/Managua'),
(146, 'America/Manaus'),
(147, 'America/Marigot'),
(148, 'America/Martinique'),
(149, 'America/Mazatlan'),
(150, 'America/Mendoza'),
(151, 'America/Menominee'),
(152, 'America/Merida'),
(153, 'America/Mexico_City'),
(154, 'America/Miquelon'),
(155, 'America/Moncton'),
(156, 'America/Monterrey'),
(157, 'America/Montevideo'),
(158, 'America/Montreal'),
(159, 'America/Montserrat'),
(160, 'America/Nassau'),
(161, 'America/New_York'),
(162, 'America/Nipigon'),
(163, 'America/Nome'),
(164, 'America/Noronha'),
(165, 'America/North_Dakota/Center'),
(166, 'America/North_Dakota/New_Salem'),
(167, 'America/Panama'),
(168, 'America/Pangnirtung'),
(169, 'America/Paramaribo'),
(170, 'America/Phoenix'),
(171, 'America/Port-au-Prince'),
(172, 'America/Port_of_Spain'),
(173, 'America/Porto_Acre'),
(174, 'America/Porto_Velho'),
(175, 'America/Puerto_Rico'),
(176, 'America/Rainy_River'),
(177, 'America/Rankin_Inlet'),
(178, 'America/Recife'),
(179, 'America/Regina'),
(180, 'America/Resolute'),
(181, 'America/Rio_Branco'),
(182, 'America/Rosario'),
(183, 'America/Santarem'),
(184, 'America/Santiago'),
(185, 'America/Santo_Domingo'),
(186, 'America/Sao_Paulo'),
(187, 'America/Scoresbysund'),
(188, 'America/Shiprock'),
(189, 'America/St_Barthelemy'),
(190, 'America/St_Johns'),
(191, 'America/St_Kitts'),
(192, 'America/St_Lucia'),
(193, 'America/St_Thomas'),
(194, 'America/St_Vincent'),
(195, 'America/Swift_Current'),
(196, 'America/Tegucigalpa'),
(197, 'America/Thule'),
(198, 'America/Thunder_Bay'),
(199, 'America/Tijuana'),
(200, 'America/Toronto'),
(201, 'America/Tortola'),
(202, 'America/Vancouver'),
(203, 'America/Virgin'),
(204, 'America/Whitehorse'),
(205, 'America/Winnipeg'),
(206, 'America/Yakutat'),
(207, 'America/Yellowknife'),
(208, 'Antarctica/Casey'),
(209, 'Antarctica/Davis'),
(210, 'Antarctica/DumontDUrville'),
(211, 'Antarctica/Mawson'),
(212, 'Antarctica/McMurdo'),
(213, 'Antarctica/Palmer'),
(214, 'Antarctica/Rothera'),
(215, 'Antarctica/South_Pole'),
(216, 'Antarctica/Syowa'),
(217, 'Antarctica/Vostok'),
(218, 'Arctic/Longyearbyen'),
(219, 'Asia/Aden'),
(220, 'Asia/Almaty'),
(221, 'Asia/Amman'),
(222, 'Asia/Anadyr'),
(223, 'Asia/Aqtau'),
(224, 'Asia/Aqtobe'),
(225, 'Asia/Ashgabat'),
(226, 'Asia/Ashkhabad'),
(227, 'Asia/Baghdad'),
(228, 'Asia/Bahrain'),
(229, 'Asia/Baku'),
(230, 'Asia/Bangkok'),
(231, 'Asia/Beirut'),
(232, 'Asia/Bishkek'),
(233, 'Asia/Brunei'),
(234, 'Asia/Calcutta'),
(235, 'Asia/Choibalsan'),
(236, 'Asia/Chongqing'),
(237, 'Asia/Chungking'),
(238, 'Asia/Colombo'),
(239, 'Asia/Dacca'),
(240, 'Asia/Damascus'),
(241, 'Asia/Dhaka'),
(242, 'Asia/Dili'),
(243, 'Asia/Dubai'),
(244, 'Asia/Dushanbe'),
(245, 'Asia/Gaza'),
(246, 'Asia/Harbin'),
(247, 'Asia/Ho_Chi_Minh'),
(248, 'Asia/Hong_Kong'),
(249, 'Asia/Hovd'),
(250, 'Asia/Irkutsk'),
(251, 'Asia/Istanbul'),
(252, 'Asia/Jakarta'),
(253, 'Asia/Jayapura'),
(254, 'Asia/Jerusalem'),
(255, 'Asia/Kabul'),
(256, 'Asia/Kamchatka'),
(257, 'Asia/Karachi'),
(258, 'Asia/Kashgar'),
(259, 'Asia/Kathmandu'),
(260, 'Asia/Katmandu'),
(261, 'Asia/Kolkata'),
(262, 'Asia/Krasnoyarsk'),
(263, 'Asia/Kuala_Lumpur'),
(264, 'Asia/Kuching'),
(265, 'Asia/Kuwait'),
(266, 'Asia/Macao'),
(267, 'Asia/Macau'),
(268, 'Asia/Magadan'),
(269, 'Asia/Makassar'),
(270, 'Asia/Manila'),
(271, 'Asia/Muscat'),
(272, 'Asia/Nicosia'),
(273, 'Asia/Novosibirsk'),
(274, 'Asia/Omsk'),
(275, 'Asia/Oral'),
(276, 'Asia/Phnom_Penh'),
(277, 'Asia/Pontianak'),
(278, 'Asia/Pyongyang'),
(279, 'Asia/Qatar'),
(280, 'Asia/Qyzylorda'),
(281, 'Asia/Rangoon'),
(282, 'Asia/Riyadh'),
(283, 'Asia/Saigon'),
(284, 'Asia/Sakhalin'),
(285, 'Asia/Samarkand'),
(286, 'Asia/Seoul'),
(287, 'Asia/Shanghai'),
(288, 'Asia/Singapore'),
(289, 'Asia/Taipei'),
(290, 'Asia/Tashkent'),
(291, 'Asia/Tbilisi'),
(292, 'Asia/Tehran'),
(293, 'Asia/Tel_Aviv'),
(294, 'Asia/Thimbu'),
(295, 'Asia/Thimphu'),
(296, 'Asia/Tokyo'),
(297, 'Asia/Ujung_Pandang'),
(298, 'Asia/Ulaanbaatar'),
(299, 'Asia/Ulan_Bator'),
(300, 'Asia/Urumqi'),
(301, 'Asia/Vientiane'),
(302, 'Asia/Vladivostok'),
(303, 'Asia/Yakutsk'),
(304, 'Asia/Yekaterinburg'),
(305, 'Asia/Yerevan'),
(306, 'Atlantic/Azores'),
(307, 'Atlantic/Bermuda'),
(308, 'Atlantic/Canary'),
(309, 'Atlantic/Cape_Verde'),
(310, 'Atlantic/Faeroe'),
(311, 'Atlantic/Faroe'),
(312, 'Atlantic/Jan_Mayen'),
(313, 'Atlantic/Madeira'),
(314, 'Atlantic/Reykjavik'),
(315, 'Atlantic/South_Georgia'),
(316, 'Atlantic/St_Helena'),
(317, 'Atlantic/Stanley'),
(318, 'Australia/ACT'),
(319, 'Australia/Adelaide'),
(320, 'Australia/Brisbane'),
(321, 'Australia/Broken_Hill'),
(322, 'Australia/Canberra'),
(323, 'Australia/Currie'),
(324, 'Australia/Darwin'),
(325, 'Australia/Eucla'),
(326, 'Australia/Hobart'),
(327, 'Australia/LHI'),
(328, 'Australia/Lindeman'),
(329, 'Australia/Lord_Howe'),
(330, 'Australia/Melbourne'),
(331, 'Australia/North'),
(332, 'Australia/NSW'),
(333, 'Australia/Perth'),
(334, 'Australia/Queensland'),
(335, 'Australia/South'),
(336, 'Australia/Sydney'),
(337, 'Australia/Tasmania'),
(338, 'Australia/Victoria'),
(339, 'Australia/West'),
(340, 'Australia/Yancowinna'),
(341, 'Europe/Amsterdam'),
(342, 'Europe/Andorra'),
(343, 'Europe/Athens'),
(344, 'Europe/Belfast'),
(345, 'Europe/Belgrade'),
(346, 'Europe/Berlin'),
(347, 'Europe/Bratislava'),
(348, 'Europe/Brussels'),
(349, 'Europe/Bucharest'),
(350, 'Europe/Budapest'),
(351, 'Europe/Chisinau'),
(352, 'Europe/Copenhagen'),
(353, 'Europe/Dublin'),
(354, 'Europe/Gibraltar'),
(355, 'Europe/Guernsey'),
(356, 'Europe/Helsinki'),
(357, 'Europe/Isle_of_Man'),
(358, 'Europe/Istanbul'),
(359, 'Europe/Jersey'),
(360, 'Europe/Kaliningrad'),
(361, 'Europe/Kiev'),
(362, 'Europe/Lisbon'),
(363, 'Europe/Ljubljana'),
(364, 'Europe/London'),
(365, 'Europe/Luxembourg'),
(366, 'Europe/Madrid'),
(367, 'Europe/Malta'),
(368, 'Europe/Mariehamn'),
(369, 'Europe/Minsk'),
(370, 'Europe/Monaco'),
(371, 'Europe/Moscow'),
(372, 'Europe/Nicosia'),
(373, 'Europe/Oslo'),
(374, 'Europe/Paris'),
(375, 'Europe/Podgorica'),
(376, 'Europe/Prague'),
(377, 'Europe/Riga'),
(378, 'Europe/Rome'),
(379, 'Europe/Samara'),
(380, 'Europe/San_Marino'),
(381, 'Europe/Sarajevo'),
(382, 'Europe/Simferopol'),
(383, 'Europe/Skopje'),
(384, 'Europe/Sofia'),
(385, 'Europe/Stockholm'),
(386, 'Europe/Tallinn'),
(387, 'Europe/Tirane'),
(388, 'Europe/Tiraspol'),
(389, 'Europe/Uzhgorod'),
(390, 'Europe/Vaduz'),
(391, 'Europe/Vatican'),
(392, 'Europe/Vienna'),
(393, 'Europe/Vilnius'),
(394, 'Europe/Volgograd'),
(395, 'Europe/Warsaw'),
(396, 'Europe/Zagreb'),
(397, 'Europe/Zaporozhye'),
(398, 'Europe/Zurich'),
(399, 'Indian/Antananarivo'),
(400, 'Indian/Chagos'),
(401, 'Indian/Christmas'),
(402, 'Indian/Cocos'),
(403, 'Indian/Comoro'),
(404, 'Indian/Kerguelen'),
(405, 'Indian/Mahe'),
(406, 'Indian/Maldives'),
(407, 'Indian/Mauritius'),
(408, 'Indian/Mayotte'),
(409, 'Indian/Reunion'),
(410, 'Pacific/Apia'),
(411, 'Pacific/Auckland'),
(412, 'Pacific/Chatham'),
(413, 'Pacific/Easter'),
(414, 'Pacific/Efate'),
(415, 'Pacific/Enderbury'),
(416, 'Pacific/Fakaofo'),
(417, 'Pacific/Fiji'),
(418, 'Pacific/Funafuti'),
(419, 'Pacific/Galapagos'),
(420, 'Pacific/Gambier'),
(421, 'Pacific/Guadalcanal'),
(422, 'Pacific/Guam'),
(423, 'Pacific/Honolulu'),
(424, 'Pacific/Johnston'),
(425, 'Pacific/Kiritimati'),
(426, 'Pacific/Kosrae'),
(427, 'Pacific/Kwajalein'),
(428, 'Pacific/Majuro'),
(429, 'Pacific/Marquesas'),
(430, 'Pacific/Midway'),
(431, 'Pacific/Nauru'),
(432, 'Pacific/Niue'),
(433, 'Pacific/Norfolk'),
(434, 'Pacific/Noumea'),
(435, 'Pacific/Pago_Pago'),
(436, 'Pacific/Palau'),
(437, 'Pacific/Pitcairn'),
(438, 'Pacific/Ponape'),
(439, 'Pacific/Port_Moresby'),
(440, 'Pacific/Rarotonga'),
(441, 'Pacific/Saipan'),
(442, 'Pacific/Samoa'),
(443, 'Pacific/Tahiti'),
(444, 'Pacific/Tarawa'),
(445, 'Pacific/Tongatapu'),
(446, 'Pacific/Truk'),
(447, 'Pacific/Wake'),
(448, 'Pacific/Wallis'),
(449, 'Pacific/Yap'),
(450, 'Brazil/Acre'),
(451, 'Brazil/DeNoronha'),
(452, 'Brazil/East'),
(453, 'Brazil/West'),
(454, 'Canada/Atlantic'),
(455, 'Canada/Central'),
(456, 'Canada/East-Saskatchewan'),
(457, 'Canada/Eastern'),
(458, 'Canada/Mountain'),
(459, 'Canada/Newfoundland'),
(460, 'Canada/Pacific'),
(461, 'Canada/Saskatchewan'),
(462, 'Canada/Yukon'),
(463, 'CET'),
(464, 'Chile/Continental'),
(465, 'Chile/EasterIsland'),
(466, 'CST6CDT'),
(467, 'Cuba'),
(468, 'EET'),
(469, 'Egypt'),
(470, 'Eire'),
(471, 'EST'),
(472, 'EST5EDT'),
(473, 'Etc/GMT'),
(474, 'Etc/GMT+0'),
(475, 'Etc/GMT+1'),
(476, 'Etc/GMT+10'),
(477, 'Etc/GMT+11'),
(478, 'Etc/GMT+12'),
(479, 'Etc/GMT+2'),
(480, 'Etc/GMT+3'),
(481, 'Etc/GMT+4'),
(482, 'Etc/GMT+5'),
(483, 'Etc/GMT+6'),
(484, 'Etc/GMT+7'),
(485, 'Etc/GMT+8'),
(486, 'Etc/GMT+9'),
(487, 'Etc/GMT-0'),
(488, 'Etc/GMT-1'),
(489, 'Etc/GMT-10'),
(490, 'Etc/GMT-11'),
(491, 'Etc/GMT-12'),
(492, 'Etc/GMT-13'),
(493, 'Etc/GMT-14'),
(494, 'Etc/GMT-2'),
(495, 'Etc/GMT-3'),
(496, 'Etc/GMT-4'),
(497, 'Etc/GMT-5'),
(498, 'Etc/GMT-6'),
(499, 'Etc/GMT-7'),
(500, 'Etc/GMT-8'),
(501, 'Etc/GMT-9'),
(502, 'Etc/GMT0'),
(503, 'Etc/Greenwich'),
(504, 'Etc/UCT'),
(505, 'Etc/Universal'),
(506, 'Etc/UTC'),
(507, 'Etc/Zulu'),
(508, 'Factory'),
(509, 'GB'),
(510, 'GB-Eire'),
(511, 'GMT'),
(512, 'GMT+0'),
(513, 'GMT-0'),
(514, 'GMT0'),
(515, 'Greenwich'),
(516, 'Hongkong'),
(517, 'HST'),
(518, 'Iceland'),
(519, 'Iran'),
(520, 'Israel'),
(521, 'Jamaica'),
(522, 'Japan'),
(523, 'Kwajalein'),
(524, 'Libya'),
(525, 'MET'),
(526, 'Mexico/BajaNorte'),
(527, 'Mexico/BajaSur'),
(528, 'Mexico/General'),
(529, 'MST'),
(530, 'MST7MDT'),
(531, 'Navajo'),
(532, 'NZ'),
(533, 'NZ-CHAT'),
(534, 'Poland'),
(535, 'Portugal'),
(536, 'PRC'),
(537, 'PST8PDT'),
(538, 'ROC'),
(539, 'ROK'),
(540, 'Singapore'),
(541, 'Turkey'),
(542, 'UCT'),
(543, 'Universal'),
(544, 'US/Alaska'),
(545, 'US/Aleutian'),
(546, 'US/Arizona'),
(547, 'US/Central'),
(548, 'US/East-Indiana'),
(549, 'US/Eastern'),
(550, 'US/Hawaii'),
(551, 'US/Indiana-Starke'),
(552, 'US/Michigan'),
(553, 'US/Mountain'),
(554, 'US/Pacific'),
(555, 'US/Pacific-New'),
(556, 'US/Samoa'),
(557, 'UTC'),
(558, 'W-SU'),
(559, 'WET'),
(560, 'Zulu');

-- --------------------------------------------------------

--
-- Table structure for table `ap_warehouse`
--

CREATE TABLE `ap_warehouse` (
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_address` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `management_type` enum('WA','FIFO','LIFO') NOT NULL DEFAULT 'WA',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_warehouse_carrier`
--

CREATE TABLE `ap_warehouse_carrier` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_warehouse_product_location`
--

CREATE TABLE `ap_warehouse_product_location` (
  `id_warehouse_product_location` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `location` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_warehouse_shop`
--

CREATE TABLE `ap_warehouse_shop` (
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_webservice_account`
--

CREATE TABLE `ap_webservice_account` (
  `id_webservice_account` int(11) NOT NULL,
  `key` varchar(32) NOT NULL,
  `description` text,
  `class_name` varchar(50) NOT NULL DEFAULT 'WebserviceRequest',
  `is_module` tinyint(2) NOT NULL DEFAULT '0',
  `module_name` varchar(50) DEFAULT NULL,
  `active` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_webservice_account_shop`
--

CREATE TABLE `ap_webservice_account_shop` (
  `id_webservice_account` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_webservice_permission`
--

CREATE TABLE `ap_webservice_permission` (
  `id_webservice_permission` int(11) NOT NULL,
  `resource` varchar(50) NOT NULL,
  `method` enum('GET','POST','PUT','DELETE','HEAD') NOT NULL,
  `id_webservice_account` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_web_browser`
--

CREATE TABLE `ap_web_browser` (
  `id_web_browser` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_web_browser`
--

INSERT INTO `ap_web_browser` (`id_web_browser`, `name`) VALUES
(1, 'Safari'),
(2, 'Safari iPad'),
(3, 'Firefox'),
(4, 'Opera'),
(5, 'IE 6'),
(6, 'IE 7'),
(7, 'IE 8'),
(8, 'IE 9'),
(9, 'IE 10'),
(10, 'IE 11'),
(11, 'Chrome');

-- --------------------------------------------------------

--
-- Table structure for table `ap_zone`
--

CREATE TABLE `ap_zone` (
  `id_zone` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_zone`
--

INSERT INTO `ap_zone` (`id_zone`, `name`, `active`) VALUES
(1, 'Europe', 1),
(2, 'North America', 1),
(3, 'Asia', 1),
(4, 'Africa', 1),
(5, 'Oceania', 1),
(6, 'South America', 1),
(7, 'Europe (non-EU)', 1),
(8, 'Central America/Antilla', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_zone_shop`
--

CREATE TABLE `ap_zone_shop` (
  `id_zone` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_zone_shop`
--

INSERT INTO `ap_zone_shop` (`id_zone`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ap_access`
--
ALTER TABLE `ap_access`
  ADD PRIMARY KEY (`id_profile`,`id_tab`);

--
-- Indexes for table `ap_accessory`
--
ALTER TABLE `ap_accessory`
  ADD KEY `accessory_product` (`id_product_1`,`id_product_2`);

--
-- Indexes for table `ap_address`
--
ALTER TABLE `ap_address`
  ADD PRIMARY KEY (`id_address`),
  ADD KEY `address_customer` (`id_customer`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `id_state` (`id_state`),
  ADD KEY `id_manufacturer` (`id_manufacturer`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_warehouse` (`id_warehouse`);

--
-- Indexes for table `ap_address_format`
--
ALTER TABLE `ap_address_format`
  ADD PRIMARY KEY (`id_country`);

--
-- Indexes for table `ap_advice`
--
ALTER TABLE `ap_advice`
  ADD PRIMARY KEY (`id_advice`);

--
-- Indexes for table `ap_advice_lang`
--
ALTER TABLE `ap_advice_lang`
  ADD PRIMARY KEY (`id_advice`,`id_lang`);

--
-- Indexes for table `ap_alias`
--
ALTER TABLE `ap_alias`
  ADD PRIMARY KEY (`id_alias`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `ap_attachment`
--
ALTER TABLE `ap_attachment`
  ADD PRIMARY KEY (`id_attachment`);

--
-- Indexes for table `ap_attachment_lang`
--
ALTER TABLE `ap_attachment_lang`
  ADD PRIMARY KEY (`id_attachment`,`id_lang`);

--
-- Indexes for table `ap_attribute`
--
ALTER TABLE `ap_attribute`
  ADD PRIMARY KEY (`id_attribute`),
  ADD KEY `attribute_group` (`id_attribute_group`);

--
-- Indexes for table `ap_attribute_group`
--
ALTER TABLE `ap_attribute_group`
  ADD PRIMARY KEY (`id_attribute_group`);

--
-- Indexes for table `ap_attribute_group_lang`
--
ALTER TABLE `ap_attribute_group_lang`
  ADD PRIMARY KEY (`id_attribute_group`,`id_lang`);

--
-- Indexes for table `ap_attribute_group_shop`
--
ALTER TABLE `ap_attribute_group_shop`
  ADD PRIMARY KEY (`id_attribute_group`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_attribute_impact`
--
ALTER TABLE `ap_attribute_impact`
  ADD PRIMARY KEY (`id_attribute_impact`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_attribute`);

--
-- Indexes for table `ap_attribute_lang`
--
ALTER TABLE `ap_attribute_lang`
  ADD PRIMARY KEY (`id_attribute`,`id_lang`),
  ADD KEY `id_lang` (`id_lang`,`name`);

--
-- Indexes for table `ap_attribute_shop`
--
ALTER TABLE `ap_attribute_shop`
  ADD PRIMARY KEY (`id_attribute`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_badge`
--
ALTER TABLE `ap_badge`
  ADD PRIMARY KEY (`id_badge`);

--
-- Indexes for table `ap_badge_lang`
--
ALTER TABLE `ap_badge_lang`
  ADD PRIMARY KEY (`id_badge`,`id_lang`);

--
-- Indexes for table `ap_bonslick`
--
ALTER TABLE `ap_bonslick`
  ADD PRIMARY KEY (`id_tab`,`id_shop`);

--
-- Indexes for table `ap_bonslick_lang`
--
ALTER TABLE `ap_bonslick_lang`
  ADD PRIMARY KEY (`id_tab`,`id_lang`);

--
-- Indexes for table `ap_carrier`
--
ALTER TABLE `ap_carrier`
  ADD PRIMARY KEY (`id_carrier`),
  ADD KEY `deleted` (`deleted`,`active`),
  ADD KEY `id_tax_rules_group` (`id_tax_rules_group`),
  ADD KEY `reference` (`id_reference`,`deleted`,`active`);

--
-- Indexes for table `ap_carrier_group`
--
ALTER TABLE `ap_carrier_group`
  ADD PRIMARY KEY (`id_carrier`,`id_group`);

--
-- Indexes for table `ap_carrier_lang`
--
ALTER TABLE `ap_carrier_lang`
  ADD PRIMARY KEY (`id_lang`,`id_shop`,`id_carrier`);

--
-- Indexes for table `ap_carrier_shop`
--
ALTER TABLE `ap_carrier_shop`
  ADD PRIMARY KEY (`id_carrier`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_carrier_tax_rules_group_shop`
--
ALTER TABLE `ap_carrier_tax_rules_group_shop`
  ADD PRIMARY KEY (`id_carrier`,`id_tax_rules_group`,`id_shop`);

--
-- Indexes for table `ap_carrier_zone`
--
ALTER TABLE `ap_carrier_zone`
  ADD PRIMARY KEY (`id_carrier`,`id_zone`);

--
-- Indexes for table `ap_cart`
--
ALTER TABLE `ap_cart`
  ADD PRIMARY KEY (`id_cart`),
  ADD KEY `cart_customer` (`id_customer`),
  ADD KEY `id_address_delivery` (`id_address_delivery`),
  ADD KEY `id_address_invoice` (`id_address_invoice`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `id_guest` (`id_guest`),
  ADD KEY `id_shop_group` (`id_shop_group`),
  ADD KEY `id_shop_2` (`id_shop`,`date_upd`),
  ADD KEY `id_shop` (`id_shop`,`date_add`);

--
-- Indexes for table `ap_cart_cart_rule`
--
ALTER TABLE `ap_cart_cart_rule`
  ADD PRIMARY KEY (`id_cart`,`id_cart_rule`),
  ADD KEY `id_cart_rule` (`id_cart_rule`);

--
-- Indexes for table `ap_cart_product`
--
ALTER TABLE `ap_cart_product`
  ADD PRIMARY KEY (`id_cart`,`id_product`,`id_product_attribute`,`id_address_delivery`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_cart_order` (`id_cart`,`date_add`,`id_product`,`id_product_attribute`);

--
-- Indexes for table `ap_cart_rule`
--
ALTER TABLE `ap_cart_rule`
  ADD PRIMARY KEY (`id_cart_rule`),
  ADD KEY `id_customer` (`id_customer`,`active`,`date_to`),
  ADD KEY `group_restriction` (`group_restriction`,`active`,`date_to`),
  ADD KEY `id_customer_2` (`id_customer`,`active`,`highlight`,`date_to`),
  ADD KEY `group_restriction_2` (`group_restriction`,`active`,`highlight`,`date_to`);

--
-- Indexes for table `ap_cart_rule_carrier`
--
ALTER TABLE `ap_cart_rule_carrier`
  ADD PRIMARY KEY (`id_cart_rule`,`id_carrier`);

--
-- Indexes for table `ap_cart_rule_combination`
--
ALTER TABLE `ap_cart_rule_combination`
  ADD PRIMARY KEY (`id_cart_rule_1`,`id_cart_rule_2`),
  ADD KEY `id_cart_rule_1` (`id_cart_rule_1`),
  ADD KEY `id_cart_rule_2` (`id_cart_rule_2`);

--
-- Indexes for table `ap_cart_rule_country`
--
ALTER TABLE `ap_cart_rule_country`
  ADD PRIMARY KEY (`id_cart_rule`,`id_country`);

--
-- Indexes for table `ap_cart_rule_group`
--
ALTER TABLE `ap_cart_rule_group`
  ADD PRIMARY KEY (`id_cart_rule`,`id_group`);

--
-- Indexes for table `ap_cart_rule_lang`
--
ALTER TABLE `ap_cart_rule_lang`
  ADD PRIMARY KEY (`id_cart_rule`,`id_lang`);

--
-- Indexes for table `ap_cart_rule_product_rule`
--
ALTER TABLE `ap_cart_rule_product_rule`
  ADD PRIMARY KEY (`id_product_rule`);

--
-- Indexes for table `ap_cart_rule_product_rule_group`
--
ALTER TABLE `ap_cart_rule_product_rule_group`
  ADD PRIMARY KEY (`id_product_rule_group`);

--
-- Indexes for table `ap_cart_rule_product_rule_value`
--
ALTER TABLE `ap_cart_rule_product_rule_value`
  ADD PRIMARY KEY (`id_product_rule`,`id_item`);

--
-- Indexes for table `ap_cart_rule_shop`
--
ALTER TABLE `ap_cart_rule_shop`
  ADD PRIMARY KEY (`id_cart_rule`,`id_shop`);

--
-- Indexes for table `ap_category`
--
ALTER TABLE `ap_category`
  ADD PRIMARY KEY (`id_category`),
  ADD KEY `category_parent` (`id_parent`),
  ADD KEY `nleftrightactive` (`nleft`,`nright`,`active`),
  ADD KEY `level_depth` (`level_depth`),
  ADD KEY `nright` (`nright`),
  ADD KEY `activenleft` (`active`,`nleft`),
  ADD KEY `activenright` (`active`,`nright`);

--
-- Indexes for table `ap_category_group`
--
ALTER TABLE `ap_category_group`
  ADD PRIMARY KEY (`id_category`,`id_group`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_group` (`id_group`);

--
-- Indexes for table `ap_category_lang`
--
ALTER TABLE `ap_category_lang`
  ADD PRIMARY KEY (`id_category`,`id_shop`,`id_lang`),
  ADD KEY `category_name` (`name`);

--
-- Indexes for table `ap_category_product`
--
ALTER TABLE `ap_category_product`
  ADD PRIMARY KEY (`id_category`,`id_product`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_category` (`id_category`,`position`);

--
-- Indexes for table `ap_category_shop`
--
ALTER TABLE `ap_category_shop`
  ADD PRIMARY KEY (`id_category`,`id_shop`);

--
-- Indexes for table `ap_cms`
--
ALTER TABLE `ap_cms`
  ADD PRIMARY KEY (`id_cms`);

--
-- Indexes for table `ap_cms_block`
--
ALTER TABLE `ap_cms_block`
  ADD PRIMARY KEY (`id_cms_block`);

--
-- Indexes for table `ap_cms_block_lang`
--
ALTER TABLE `ap_cms_block_lang`
  ADD PRIMARY KEY (`id_cms_block`,`id_lang`);

--
-- Indexes for table `ap_cms_block_page`
--
ALTER TABLE `ap_cms_block_page`
  ADD PRIMARY KEY (`id_cms_block_page`);

--
-- Indexes for table `ap_cms_block_shop`
--
ALTER TABLE `ap_cms_block_shop`
  ADD PRIMARY KEY (`id_cms_block`,`id_shop`);

--
-- Indexes for table `ap_cms_category`
--
ALTER TABLE `ap_cms_category`
  ADD PRIMARY KEY (`id_cms_category`),
  ADD KEY `category_parent` (`id_parent`);

--
-- Indexes for table `ap_cms_category_lang`
--
ALTER TABLE `ap_cms_category_lang`
  ADD PRIMARY KEY (`id_cms_category`,`id_shop`,`id_lang`),
  ADD KEY `category_name` (`name`);

--
-- Indexes for table `ap_cms_category_shop`
--
ALTER TABLE `ap_cms_category_shop`
  ADD PRIMARY KEY (`id_cms_category`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_cms_lang`
--
ALTER TABLE `ap_cms_lang`
  ADD PRIMARY KEY (`id_cms`,`id_shop`,`id_lang`);

--
-- Indexes for table `ap_cms_role`
--
ALTER TABLE `ap_cms_role`
  ADD PRIMARY KEY (`id_cms_role`,`id_cms`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `ap_cms_role_lang`
--
ALTER TABLE `ap_cms_role_lang`
  ADD PRIMARY KEY (`id_cms_role`,`id_lang`,`id_shop`);

--
-- Indexes for table `ap_cms_shop`
--
ALTER TABLE `ap_cms_shop`
  ADD PRIMARY KEY (`id_cms`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_compare`
--
ALTER TABLE `ap_compare`
  ADD PRIMARY KEY (`id_compare`);

--
-- Indexes for table `ap_compare_product`
--
ALTER TABLE `ap_compare_product`
  ADD PRIMARY KEY (`id_compare`,`id_product`);

--
-- Indexes for table `ap_condition`
--
ALTER TABLE `ap_condition`
  ADD PRIMARY KEY (`id_condition`,`id_ps_condition`);

--
-- Indexes for table `ap_condition_advice`
--
ALTER TABLE `ap_condition_advice`
  ADD PRIMARY KEY (`id_condition`,`id_advice`);

--
-- Indexes for table `ap_condition_badge`
--
ALTER TABLE `ap_condition_badge`
  ADD PRIMARY KEY (`id_condition`,`id_badge`);

--
-- Indexes for table `ap_configuration`
--
ALTER TABLE `ap_configuration`
  ADD PRIMARY KEY (`id_configuration`),
  ADD KEY `name` (`name`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_shop_group` (`id_shop_group`);

--
-- Indexes for table `ap_configuration_kpi`
--
ALTER TABLE `ap_configuration_kpi`
  ADD PRIMARY KEY (`id_configuration_kpi`),
  ADD KEY `name` (`name`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_shop_group` (`id_shop_group`);

--
-- Indexes for table `ap_configuration_kpi_lang`
--
ALTER TABLE `ap_configuration_kpi_lang`
  ADD PRIMARY KEY (`id_configuration_kpi`,`id_lang`);

--
-- Indexes for table `ap_configuration_lang`
--
ALTER TABLE `ap_configuration_lang`
  ADD PRIMARY KEY (`id_configuration`,`id_lang`);

--
-- Indexes for table `ap_connections`
--
ALTER TABLE `ap_connections`
  ADD PRIMARY KEY (`id_connections`),
  ADD KEY `id_guest` (`id_guest`),
  ADD KEY `date_add` (`date_add`),
  ADD KEY `id_page` (`id_page`);

--
-- Indexes for table `ap_connections_page`
--
ALTER TABLE `ap_connections_page`
  ADD PRIMARY KEY (`id_connections`,`id_page`,`time_start`);

--
-- Indexes for table `ap_connections_source`
--
ALTER TABLE `ap_connections_source`
  ADD PRIMARY KEY (`id_connections_source`),
  ADD KEY `connections` (`id_connections`),
  ADD KEY `orderby` (`date_add`),
  ADD KEY `http_referer` (`http_referer`),
  ADD KEY `request_uri` (`request_uri`);

--
-- Indexes for table `ap_contact`
--
ALTER TABLE `ap_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `ap_contact_lang`
--
ALTER TABLE `ap_contact_lang`
  ADD PRIMARY KEY (`id_contact`,`id_lang`);

--
-- Indexes for table `ap_contact_shop`
--
ALTER TABLE `ap_contact_shop`
  ADD PRIMARY KEY (`id_contact`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_country`
--
ALTER TABLE `ap_country`
  ADD PRIMARY KEY (`id_country`),
  ADD KEY `country_iso_code` (`iso_code`),
  ADD KEY `country_` (`id_zone`);

--
-- Indexes for table `ap_country_lang`
--
ALTER TABLE `ap_country_lang`
  ADD PRIMARY KEY (`id_country`,`id_lang`);

--
-- Indexes for table `ap_country_shop`
--
ALTER TABLE `ap_country_shop`
  ADD PRIMARY KEY (`id_country`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_cronjobs`
--
ALTER TABLE `ap_cronjobs`
  ADD PRIMARY KEY (`id_cronjob`),
  ADD KEY `id_module` (`id_module`);

--
-- Indexes for table `ap_currency`
--
ALTER TABLE `ap_currency`
  ADD PRIMARY KEY (`id_currency`);

--
-- Indexes for table `ap_currency_shop`
--
ALTER TABLE `ap_currency_shop`
  ADD PRIMARY KEY (`id_currency`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_customer`
--
ALTER TABLE `ap_customer`
  ADD PRIMARY KEY (`id_customer`),
  ADD KEY `customer_email` (`email`),
  ADD KEY `customer_login` (`email`,`passwd`),
  ADD KEY `id_customer_passwd` (`id_customer`,`passwd`),
  ADD KEY `id_gender` (`id_gender`),
  ADD KEY `id_shop_group` (`id_shop_group`),
  ADD KEY `id_shop` (`id_shop`,`date_add`);

--
-- Indexes for table `ap_customer_group`
--
ALTER TABLE `ap_customer_group`
  ADD PRIMARY KEY (`id_customer`,`id_group`),
  ADD KEY `customer_login` (`id_group`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `ap_customer_message`
--
ALTER TABLE `ap_customer_message`
  ADD PRIMARY KEY (`id_customer_message`),
  ADD KEY `id_customer_thread` (`id_customer_thread`),
  ADD KEY `id_employee` (`id_employee`);

--
-- Indexes for table `ap_customer_message_sync_imap`
--
ALTER TABLE `ap_customer_message_sync_imap`
  ADD KEY `md5_header_index` (`md5_header`(4));

--
-- Indexes for table `ap_customer_thread`
--
ALTER TABLE `ap_customer_thread`
  ADD PRIMARY KEY (`id_customer_thread`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_contact` (`id_contact`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `ap_customization`
--
ALTER TABLE `ap_customization`
  ADD PRIMARY KEY (`id_customization`,`id_cart`,`id_product`,`id_address_delivery`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_cart_product` (`id_cart`,`id_product`,`id_product_attribute`);

--
-- Indexes for table `ap_customization_field`
--
ALTER TABLE `ap_customization_field`
  ADD PRIMARY KEY (`id_customization_field`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `ap_customization_field_lang`
--
ALTER TABLE `ap_customization_field_lang`
  ADD PRIMARY KEY (`id_customization_field`,`id_lang`,`id_shop`);

--
-- Indexes for table `ap_customized_data`
--
ALTER TABLE `ap_customized_data`
  ADD PRIMARY KEY (`id_customization`,`type`,`index`);

--
-- Indexes for table `ap_date_range`
--
ALTER TABLE `ap_date_range`
  ADD PRIMARY KEY (`id_date_range`);

--
-- Indexes for table `ap_delivery`
--
ALTER TABLE `ap_delivery`
  ADD PRIMARY KEY (`id_delivery`),
  ADD KEY `id_zone` (`id_zone`),
  ADD KEY `id_carrier` (`id_carrier`,`id_zone`),
  ADD KEY `id_range_price` (`id_range_price`),
  ADD KEY `id_range_weight` (`id_range_weight`);

--
-- Indexes for table `ap_employee`
--
ALTER TABLE `ap_employee`
  ADD PRIMARY KEY (`id_employee`),
  ADD KEY `employee_login` (`email`,`passwd`),
  ADD KEY `id_employee_passwd` (`id_employee`,`passwd`),
  ADD KEY `id_profile` (`id_profile`);

--
-- Indexes for table `ap_employee_shop`
--
ALTER TABLE `ap_employee_shop`
  ADD PRIMARY KEY (`id_employee`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_feature`
--
ALTER TABLE `ap_feature`
  ADD PRIMARY KEY (`id_feature`);

--
-- Indexes for table `ap_feature_lang`
--
ALTER TABLE `ap_feature_lang`
  ADD PRIMARY KEY (`id_feature`,`id_lang`),
  ADD KEY `id_lang` (`id_lang`,`name`);

--
-- Indexes for table `ap_feature_product`
--
ALTER TABLE `ap_feature_product`
  ADD PRIMARY KEY (`id_feature`,`id_product`),
  ADD KEY `id_feature_value` (`id_feature_value`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `ap_feature_shop`
--
ALTER TABLE `ap_feature_shop`
  ADD PRIMARY KEY (`id_feature`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_feature_value`
--
ALTER TABLE `ap_feature_value`
  ADD PRIMARY KEY (`id_feature_value`),
  ADD KEY `feature` (`id_feature`);

--
-- Indexes for table `ap_feature_value_lang`
--
ALTER TABLE `ap_feature_value_lang`
  ADD PRIMARY KEY (`id_feature_value`,`id_lang`);

--
-- Indexes for table `ap_gender`
--
ALTER TABLE `ap_gender`
  ADD PRIMARY KEY (`id_gender`);

--
-- Indexes for table `ap_gender_lang`
--
ALTER TABLE `ap_gender_lang`
  ADD PRIMARY KEY (`id_gender`,`id_lang`),
  ADD KEY `id_gender` (`id_gender`);

--
-- Indexes for table `ap_group`
--
ALTER TABLE `ap_group`
  ADD PRIMARY KEY (`id_group`);

--
-- Indexes for table `ap_group_lang`
--
ALTER TABLE `ap_group_lang`
  ADD PRIMARY KEY (`id_group`,`id_lang`);

--
-- Indexes for table `ap_group_reduction`
--
ALTER TABLE `ap_group_reduction`
  ADD PRIMARY KEY (`id_group_reduction`),
  ADD UNIQUE KEY `id_group` (`id_group`,`id_category`);

--
-- Indexes for table `ap_group_shop`
--
ALTER TABLE `ap_group_shop`
  ADD PRIMARY KEY (`id_group`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_guest`
--
ALTER TABLE `ap_guest`
  ADD PRIMARY KEY (`id_guest`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_operating_system` (`id_operating_system`),
  ADD KEY `id_web_browser` (`id_web_browser`);

--
-- Indexes for table `ap_homeslider`
--
ALTER TABLE `ap_homeslider`
  ADD PRIMARY KEY (`id_homeslider_slides`,`id_shop`);

--
-- Indexes for table `ap_homeslider_slides`
--
ALTER TABLE `ap_homeslider_slides`
  ADD PRIMARY KEY (`id_homeslider_slides`);

--
-- Indexes for table `ap_homeslider_slides_lang`
--
ALTER TABLE `ap_homeslider_slides_lang`
  ADD PRIMARY KEY (`id_homeslider_slides`,`id_lang`);

--
-- Indexes for table `ap_hook`
--
ALTER TABLE `ap_hook`
  ADD PRIMARY KEY (`id_hook`),
  ADD UNIQUE KEY `hook_name` (`name`);

--
-- Indexes for table `ap_hook_alias`
--
ALTER TABLE `ap_hook_alias`
  ADD PRIMARY KEY (`id_hook_alias`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `ap_hook_module`
--
ALTER TABLE `ap_hook_module`
  ADD PRIMARY KEY (`id_module`,`id_hook`,`id_shop`),
  ADD KEY `id_hook` (`id_hook`),
  ADD KEY `id_module` (`id_module`),
  ADD KEY `position` (`id_shop`,`position`);

--
-- Indexes for table `ap_hook_module_exceptions`
--
ALTER TABLE `ap_hook_module_exceptions`
  ADD PRIMARY KEY (`id_hook_module_exceptions`),
  ADD KEY `id_module` (`id_module`),
  ADD KEY `id_hook` (`id_hook`);

--
-- Indexes for table `ap_image`
--
ALTER TABLE `ap_image`
  ADD PRIMARY KEY (`id_image`),
  ADD UNIQUE KEY `id_product_cover` (`id_product`,`cover`),
  ADD UNIQUE KEY `idx_product_image` (`id_image`,`id_product`,`cover`),
  ADD KEY `image_product` (`id_product`);

--
-- Indexes for table `ap_image_lang`
--
ALTER TABLE `ap_image_lang`
  ADD PRIMARY KEY (`id_image`,`id_lang`),
  ADD KEY `id_image` (`id_image`);

--
-- Indexes for table `ap_image_shop`
--
ALTER TABLE `ap_image_shop`
  ADD PRIMARY KEY (`id_image`,`id_shop`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_shop`,`cover`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_image_type`
--
ALTER TABLE `ap_image_type`
  ADD PRIMARY KEY (`id_image_type`),
  ADD KEY `image_type_name` (`name`);

--
-- Indexes for table `ap_import_match`
--
ALTER TABLE `ap_import_match`
  ADD PRIMARY KEY (`id_import_match`);

--
-- Indexes for table `ap_info`
--
ALTER TABLE `ap_info`
  ADD PRIMARY KEY (`id_info`);

--
-- Indexes for table `ap_info_lang`
--
ALTER TABLE `ap_info_lang`
  ADD PRIMARY KEY (`id_info`,`id_lang`);

--
-- Indexes for table `ap_lang`
--
ALTER TABLE `ap_lang`
  ADD PRIMARY KEY (`id_lang`),
  ADD KEY `lang_iso_code` (`iso_code`);

--
-- Indexes for table `ap_lang_shop`
--
ALTER TABLE `ap_lang_shop`
  ADD PRIMARY KEY (`id_lang`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_layered_category`
--
ALTER TABLE `ap_layered_category`
  ADD PRIMARY KEY (`id_layered_category`),
  ADD KEY `id_category` (`id_category`,`type`);

--
-- Indexes for table `ap_layered_filter`
--
ALTER TABLE `ap_layered_filter`
  ADD PRIMARY KEY (`id_layered_filter`);

--
-- Indexes for table `ap_layered_filter_shop`
--
ALTER TABLE `ap_layered_filter_shop`
  ADD PRIMARY KEY (`id_layered_filter`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_layered_friendly_url`
--
ALTER TABLE `ap_layered_friendly_url`
  ADD PRIMARY KEY (`id_layered_friendly_url`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `url_key` (`url_key`(5));

--
-- Indexes for table `ap_layered_indexable_attribute_group`
--
ALTER TABLE `ap_layered_indexable_attribute_group`
  ADD PRIMARY KEY (`id_attribute_group`);

--
-- Indexes for table `ap_layered_indexable_attribute_group_lang_value`
--
ALTER TABLE `ap_layered_indexable_attribute_group_lang_value`
  ADD PRIMARY KEY (`id_attribute_group`,`id_lang`);

--
-- Indexes for table `ap_layered_indexable_attribute_lang_value`
--
ALTER TABLE `ap_layered_indexable_attribute_lang_value`
  ADD PRIMARY KEY (`id_attribute`,`id_lang`);

--
-- Indexes for table `ap_layered_indexable_feature`
--
ALTER TABLE `ap_layered_indexable_feature`
  ADD PRIMARY KEY (`id_feature`);

--
-- Indexes for table `ap_layered_indexable_feature_lang_value`
--
ALTER TABLE `ap_layered_indexable_feature_lang_value`
  ADD PRIMARY KEY (`id_feature`,`id_lang`);

--
-- Indexes for table `ap_layered_indexable_feature_value_lang_value`
--
ALTER TABLE `ap_layered_indexable_feature_value_lang_value`
  ADD PRIMARY KEY (`id_feature_value`,`id_lang`);

--
-- Indexes for table `ap_layered_price_index`
--
ALTER TABLE `ap_layered_price_index`
  ADD PRIMARY KEY (`id_product`,`id_currency`,`id_shop`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `price_min` (`price_min`),
  ADD KEY `price_max` (`price_max`);

--
-- Indexes for table `ap_layered_product_attribute`
--
ALTER TABLE `ap_layered_product_attribute`
  ADD PRIMARY KEY (`id_attribute`,`id_product`,`id_shop`),
  ADD UNIQUE KEY `id_attribute_group` (`id_attribute_group`,`id_attribute`,`id_product`,`id_shop`);

--
-- Indexes for table `ap_linksmenutop`
--
ALTER TABLE `ap_linksmenutop`
  ADD PRIMARY KEY (`id_linksmenutop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_linksmenutop_lang`
--
ALTER TABLE `ap_linksmenutop_lang`
  ADD KEY `id_linksmenutop` (`id_linksmenutop`,`id_lang`,`id_shop`);

--
-- Indexes for table `ap_log`
--
ALTER TABLE `ap_log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `ap_mail`
--
ALTER TABLE `ap_mail`
  ADD PRIMARY KEY (`id_mail`),
  ADD KEY `recipient` (`recipient`(10));

--
-- Indexes for table `ap_manufacturer`
--
ALTER TABLE `ap_manufacturer`
  ADD PRIMARY KEY (`id_manufacturer`);

--
-- Indexes for table `ap_manufacturer_lang`
--
ALTER TABLE `ap_manufacturer_lang`
  ADD PRIMARY KEY (`id_manufacturer`,`id_lang`);

--
-- Indexes for table `ap_manufacturer_shop`
--
ALTER TABLE `ap_manufacturer_shop`
  ADD PRIMARY KEY (`id_manufacturer`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_memcached_servers`
--
ALTER TABLE `ap_memcached_servers`
  ADD PRIMARY KEY (`id_memcached_server`);

--
-- Indexes for table `ap_message`
--
ALTER TABLE `ap_message`
  ADD PRIMARY KEY (`id_message`),
  ADD KEY `message_order` (`id_order`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_employee` (`id_employee`);

--
-- Indexes for table `ap_message_readed`
--
ALTER TABLE `ap_message_readed`
  ADD PRIMARY KEY (`id_message`,`id_employee`);

--
-- Indexes for table `ap_meta`
--
ALTER TABLE `ap_meta`
  ADD PRIMARY KEY (`id_meta`),
  ADD UNIQUE KEY `page` (`page`);

--
-- Indexes for table `ap_meta_lang`
--
ALTER TABLE `ap_meta_lang`
  ADD PRIMARY KEY (`id_meta`,`id_shop`,`id_lang`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_lang` (`id_lang`);

--
-- Indexes for table `ap_module`
--
ALTER TABLE `ap_module`
  ADD PRIMARY KEY (`id_module`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `ap_modules_perfs`
--
ALTER TABLE `ap_modules_perfs`
  ADD PRIMARY KEY (`id_modules_perfs`),
  ADD KEY `session` (`session`);

--
-- Indexes for table `ap_module_access`
--
ALTER TABLE `ap_module_access`
  ADD PRIMARY KEY (`id_profile`,`id_module`);

--
-- Indexes for table `ap_module_country`
--
ALTER TABLE `ap_module_country`
  ADD PRIMARY KEY (`id_module`,`id_shop`,`id_country`);

--
-- Indexes for table `ap_module_currency`
--
ALTER TABLE `ap_module_currency`
  ADD PRIMARY KEY (`id_module`,`id_shop`,`id_currency`),
  ADD KEY `id_module` (`id_module`);

--
-- Indexes for table `ap_module_group`
--
ALTER TABLE `ap_module_group`
  ADD PRIMARY KEY (`id_module`,`id_shop`,`id_group`);

--
-- Indexes for table `ap_module_preference`
--
ALTER TABLE `ap_module_preference`
  ADD PRIMARY KEY (`id_module_preference`),
  ADD UNIQUE KEY `employee_module` (`id_employee`,`module`);

--
-- Indexes for table `ap_module_shop`
--
ALTER TABLE `ap_module_shop`
  ADD PRIMARY KEY (`id_module`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_newsletter`
--
ALTER TABLE `ap_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_np_calendar_slot`
--
ALTER TABLE `ap_np_calendar_slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_np_customer`
--
ALTER TABLE `ap_np_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_np_customer_file`
--
ALTER TABLE `ap_np_customer_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_np_customer_file_to_upload`
--
ALTER TABLE `ap_np_customer_file_to_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_np_customer_point`
--
ALTER TABLE `ap_np_customer_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_np_customer_type`
--
ALTER TABLE `ap_np_customer_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_np_product_point`
--
ALTER TABLE `ap_np_product_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ap_operating_system`
--
ALTER TABLE `ap_operating_system`
  ADD PRIMARY KEY (`id_operating_system`);

--
-- Indexes for table `ap_orders`
--
ALTER TABLE `ap_orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `reference` (`reference`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `invoice_number` (`invoice_number`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `id_address_delivery` (`id_address_delivery`),
  ADD KEY `id_address_invoice` (`id_address_invoice`),
  ADD KEY `id_shop_group` (`id_shop_group`),
  ADD KEY `current_state` (`current_state`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `date_add` (`date_add`);

--
-- Indexes for table `ap_order_carrier`
--
ALTER TABLE `ap_order_carrier`
  ADD PRIMARY KEY (`id_order_carrier`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_order_invoice` (`id_order_invoice`);

--
-- Indexes for table `ap_order_cart_rule`
--
ALTER TABLE `ap_order_cart_rule`
  ADD PRIMARY KEY (`id_order_cart_rule`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_cart_rule` (`id_cart_rule`);

--
-- Indexes for table `ap_order_detail`
--
ALTER TABLE `ap_order_detail`
  ADD PRIMARY KEY (`id_order_detail`),
  ADD KEY `order_detail_order` (`id_order`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_attribute_id` (`product_attribute_id`),
  ADD KEY `id_tax_rules_group` (`id_tax_rules_group`),
  ADD KEY `id_order_id_order_detail` (`id_order`,`id_order_detail`);

--
-- Indexes for table `ap_order_detail_tax`
--
ALTER TABLE `ap_order_detail_tax`
  ADD KEY `id_order_detail` (`id_order_detail`),
  ADD KEY `id_tax` (`id_tax`);

--
-- Indexes for table `ap_order_history`
--
ALTER TABLE `ap_order_history`
  ADD PRIMARY KEY (`id_order_history`),
  ADD KEY `order_history_order` (`id_order`),
  ADD KEY `id_employee` (`id_employee`),
  ADD KEY `id_order_state` (`id_order_state`);

--
-- Indexes for table `ap_order_invoice`
--
ALTER TABLE `ap_order_invoice`
  ADD PRIMARY KEY (`id_order_invoice`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `ap_order_invoice_payment`
--
ALTER TABLE `ap_order_invoice_payment`
  ADD PRIMARY KEY (`id_order_invoice`,`id_order_payment`),
  ADD KEY `order_payment` (`id_order_payment`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `ap_order_invoice_tax`
--
ALTER TABLE `ap_order_invoice_tax`
  ADD KEY `id_tax` (`id_tax`);

--
-- Indexes for table `ap_order_message`
--
ALTER TABLE `ap_order_message`
  ADD PRIMARY KEY (`id_order_message`);

--
-- Indexes for table `ap_order_message_lang`
--
ALTER TABLE `ap_order_message_lang`
  ADD PRIMARY KEY (`id_order_message`,`id_lang`);

--
-- Indexes for table `ap_order_payment`
--
ALTER TABLE `ap_order_payment`
  ADD PRIMARY KEY (`id_order_payment`),
  ADD KEY `order_reference` (`order_reference`);

--
-- Indexes for table `ap_order_return`
--
ALTER TABLE `ap_order_return`
  ADD PRIMARY KEY (`id_order_return`),
  ADD KEY `order_return_customer` (`id_customer`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `ap_order_return_detail`
--
ALTER TABLE `ap_order_return_detail`
  ADD PRIMARY KEY (`id_order_return`,`id_order_detail`,`id_customization`);

--
-- Indexes for table `ap_order_return_state`
--
ALTER TABLE `ap_order_return_state`
  ADD PRIMARY KEY (`id_order_return_state`);

--
-- Indexes for table `ap_order_return_state_lang`
--
ALTER TABLE `ap_order_return_state_lang`
  ADD PRIMARY KEY (`id_order_return_state`,`id_lang`);

--
-- Indexes for table `ap_order_slip`
--
ALTER TABLE `ap_order_slip`
  ADD PRIMARY KEY (`id_order_slip`),
  ADD KEY `order_slip_customer` (`id_customer`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `ap_order_slip_detail`
--
ALTER TABLE `ap_order_slip_detail`
  ADD PRIMARY KEY (`id_order_slip`,`id_order_detail`);

--
-- Indexes for table `ap_order_slip_detail_tax`
--
ALTER TABLE `ap_order_slip_detail_tax`
  ADD KEY `id_order_slip_detail` (`id_order_slip_detail`),
  ADD KEY `id_tax` (`id_tax`);

--
-- Indexes for table `ap_order_state`
--
ALTER TABLE `ap_order_state`
  ADD PRIMARY KEY (`id_order_state`),
  ADD KEY `module_name` (`module_name`);

--
-- Indexes for table `ap_order_state_lang`
--
ALTER TABLE `ap_order_state_lang`
  ADD PRIMARY KEY (`id_order_state`,`id_lang`);

--
-- Indexes for table `ap_pack`
--
ALTER TABLE `ap_pack`
  ADD PRIMARY KEY (`id_product_pack`,`id_product_item`,`id_product_attribute_item`),
  ADD KEY `product_item` (`id_product_item`,`id_product_attribute_item`);

--
-- Indexes for table `ap_page`
--
ALTER TABLE `ap_page`
  ADD PRIMARY KEY (`id_page`),
  ADD KEY `id_page_type` (`id_page_type`),
  ADD KEY `id_object` (`id_object`);

--
-- Indexes for table `ap_pagenotfound`
--
ALTER TABLE `ap_pagenotfound`
  ADD PRIMARY KEY (`id_pagenotfound`),
  ADD KEY `date_add` (`date_add`);

--
-- Indexes for table `ap_page_type`
--
ALTER TABLE `ap_page_type`
  ADD PRIMARY KEY (`id_page_type`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `ap_page_viewed`
--
ALTER TABLE `ap_page_viewed`
  ADD PRIMARY KEY (`id_page`,`id_date_range`,`id_shop`);

--
-- Indexes for table `ap_paypal_braintree`
--
ALTER TABLE `ap_paypal_braintree`
  ADD PRIMARY KEY (`id_paypal_braintree`);

--
-- Indexes for table `ap_paypal_capture`
--
ALTER TABLE `ap_paypal_capture`
  ADD PRIMARY KEY (`id_paypal_capture`);

--
-- Indexes for table `ap_paypal_customer`
--
ALTER TABLE `ap_paypal_customer`
  ADD PRIMARY KEY (`id_paypal_customer`);

--
-- Indexes for table `ap_paypal_hss_email_error`
--
ALTER TABLE `ap_paypal_hss_email_error`
  ADD PRIMARY KEY (`id_paypal_hss_email_error`);

--
-- Indexes for table `ap_paypal_login_user`
--
ALTER TABLE `ap_paypal_login_user`
  ADD PRIMARY KEY (`id_paypal_login_user`);

--
-- Indexes for table `ap_paypal_order`
--
ALTER TABLE `ap_paypal_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `ap_paypal_plus_pui`
--
ALTER TABLE `ap_paypal_plus_pui`
  ADD PRIMARY KEY (`id_paypal_plus_pui`);

--
-- Indexes for table `ap_product`
--
ALTER TABLE `ap_product`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `product_supplier` (`id_supplier`),
  ADD KEY `product_manufacturer` (`id_manufacturer`,`id_product`),
  ADD KEY `id_category_default` (`id_category_default`),
  ADD KEY `indexed` (`indexed`),
  ADD KEY `date_add` (`date_add`);

--
-- Indexes for table `ap_product_attachment`
--
ALTER TABLE `ap_product_attachment`
  ADD PRIMARY KEY (`id_product`,`id_attachment`);

--
-- Indexes for table `ap_product_attribute`
--
ALTER TABLE `ap_product_attribute`
  ADD PRIMARY KEY (`id_product_attribute`),
  ADD UNIQUE KEY `product_default` (`id_product`,`default_on`),
  ADD KEY `product_attribute_product` (`id_product`),
  ADD KEY `reference` (`reference`),
  ADD KEY `supplier_reference` (`supplier_reference`),
  ADD KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`);

--
-- Indexes for table `ap_product_attribute_combination`
--
ALTER TABLE `ap_product_attribute_combination`
  ADD PRIMARY KEY (`id_attribute`,`id_product_attribute`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

--
-- Indexes for table `ap_product_attribute_image`
--
ALTER TABLE `ap_product_attribute_image`
  ADD PRIMARY KEY (`id_product_attribute`,`id_image`),
  ADD KEY `id_image` (`id_image`);

--
-- Indexes for table `ap_product_attribute_shop`
--
ALTER TABLE `ap_product_attribute_shop`
  ADD PRIMARY KEY (`id_product_attribute`,`id_shop`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_shop`,`default_on`);

--
-- Indexes for table `ap_product_carrier`
--
ALTER TABLE `ap_product_carrier`
  ADD PRIMARY KEY (`id_product`,`id_carrier_reference`,`id_shop`);

--
-- Indexes for table `ap_product_country_tax`
--
ALTER TABLE `ap_product_country_tax`
  ADD PRIMARY KEY (`id_product`,`id_country`);

--
-- Indexes for table `ap_product_download`
--
ALTER TABLE `ap_product_download`
  ADD PRIMARY KEY (`id_product_download`),
  ADD UNIQUE KEY `id_product` (`id_product`),
  ADD KEY `product_active` (`id_product`,`active`);

--
-- Indexes for table `ap_product_group_reduction_cache`
--
ALTER TABLE `ap_product_group_reduction_cache`
  ADD PRIMARY KEY (`id_product`,`id_group`);

--
-- Indexes for table `ap_product_lang`
--
ALTER TABLE `ap_product_lang`
  ADD PRIMARY KEY (`id_product`,`id_shop`,`id_lang`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `ap_product_sale`
--
ALTER TABLE `ap_product_sale`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `quantity` (`quantity`);

--
-- Indexes for table `ap_product_shop`
--
ALTER TABLE `ap_product_shop`
  ADD PRIMARY KEY (`id_product`,`id_shop`),
  ADD KEY `id_category_default` (`id_category_default`),
  ADD KEY `date_add` (`date_add`,`active`,`visibility`),
  ADD KEY `indexed` (`indexed`,`active`,`id_product`);

--
-- Indexes for table `ap_product_supplier`
--
ALTER TABLE `ap_product_supplier`
  ADD PRIMARY KEY (`id_product_supplier`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_supplier`),
  ADD KEY `id_supplier` (`id_supplier`,`id_product`);

--
-- Indexes for table `ap_product_tag`
--
ALTER TABLE `ap_product_tag`
  ADD PRIMARY KEY (`id_product`,`id_tag`),
  ADD KEY `id_tag` (`id_tag`),
  ADD KEY `id_lang` (`id_lang`,`id_tag`);

--
-- Indexes for table `ap_profile`
--
ALTER TABLE `ap_profile`
  ADD PRIMARY KEY (`id_profile`);

--
-- Indexes for table `ap_profile_lang`
--
ALTER TABLE `ap_profile_lang`
  ADD PRIMARY KEY (`id_profile`,`id_lang`);

--
-- Indexes for table `ap_quick_access`
--
ALTER TABLE `ap_quick_access`
  ADD PRIMARY KEY (`id_quick_access`);

--
-- Indexes for table `ap_quick_access_lang`
--
ALTER TABLE `ap_quick_access_lang`
  ADD PRIMARY KEY (`id_quick_access`,`id_lang`);

--
-- Indexes for table `ap_range_price`
--
ALTER TABLE `ap_range_price`
  ADD PRIMARY KEY (`id_range_price`),
  ADD UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`);

--
-- Indexes for table `ap_range_weight`
--
ALTER TABLE `ap_range_weight`
  ADD PRIMARY KEY (`id_range_weight`),
  ADD UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`);

--
-- Indexes for table `ap_referrer`
--
ALTER TABLE `ap_referrer`
  ADD PRIMARY KEY (`id_referrer`);

--
-- Indexes for table `ap_referrer_cache`
--
ALTER TABLE `ap_referrer_cache`
  ADD PRIMARY KEY (`id_connections_source`,`id_referrer`);

--
-- Indexes for table `ap_referrer_shop`
--
ALTER TABLE `ap_referrer_shop`
  ADD PRIMARY KEY (`id_referrer`,`id_shop`);

--
-- Indexes for table `ap_request_sql`
--
ALTER TABLE `ap_request_sql`
  ADD PRIMARY KEY (`id_request_sql`);

--
-- Indexes for table `ap_required_field`
--
ALTER TABLE `ap_required_field`
  ADD PRIMARY KEY (`id_required_field`),
  ADD KEY `object_name` (`object_name`);

--
-- Indexes for table `ap_risk`
--
ALTER TABLE `ap_risk`
  ADD PRIMARY KEY (`id_risk`);

--
-- Indexes for table `ap_risk_lang`
--
ALTER TABLE `ap_risk_lang`
  ADD PRIMARY KEY (`id_risk`,`id_lang`),
  ADD KEY `id_risk` (`id_risk`);

--
-- Indexes for table `ap_scene`
--
ALTER TABLE `ap_scene`
  ADD PRIMARY KEY (`id_scene`);

--
-- Indexes for table `ap_scene_category`
--
ALTER TABLE `ap_scene_category`
  ADD PRIMARY KEY (`id_scene`,`id_category`);

--
-- Indexes for table `ap_scene_lang`
--
ALTER TABLE `ap_scene_lang`
  ADD PRIMARY KEY (`id_scene`,`id_lang`);

--
-- Indexes for table `ap_scene_products`
--
ALTER TABLE `ap_scene_products`
  ADD PRIMARY KEY (`id_scene`,`id_product`,`x_axis`,`y_axis`);

--
-- Indexes for table `ap_scene_shop`
--
ALTER TABLE `ap_scene_shop`
  ADD PRIMARY KEY (`id_scene`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_search_engine`
--
ALTER TABLE `ap_search_engine`
  ADD PRIMARY KEY (`id_search_engine`);

--
-- Indexes for table `ap_search_index`
--
ALTER TABLE `ap_search_index`
  ADD PRIMARY KEY (`id_word`,`id_product`),
  ADD KEY `id_product` (`id_product`,`weight`);

--
-- Indexes for table `ap_search_word`
--
ALTER TABLE `ap_search_word`
  ADD PRIMARY KEY (`id_word`),
  ADD UNIQUE KEY `id_lang` (`id_lang`,`id_shop`,`word`);

--
-- Indexes for table `ap_sekeyword`
--
ALTER TABLE `ap_sekeyword`
  ADD PRIMARY KEY (`id_sekeyword`);

--
-- Indexes for table `ap_shop`
--
ALTER TABLE `ap_shop`
  ADD PRIMARY KEY (`id_shop`),
  ADD KEY `id_shop_group` (`id_shop_group`,`deleted`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_theme` (`id_theme`);

--
-- Indexes for table `ap_shop_group`
--
ALTER TABLE `ap_shop_group`
  ADD PRIMARY KEY (`id_shop_group`),
  ADD KEY `deleted` (`deleted`,`name`);

--
-- Indexes for table `ap_shop_url`
--
ALTER TABLE `ap_shop_url`
  ADD PRIMARY KEY (`id_shop_url`),
  ADD UNIQUE KEY `full_shop_url` (`domain`,`physical_uri`,`virtual_uri`),
  ADD UNIQUE KEY `full_shop_url_ssl` (`domain_ssl`,`physical_uri`,`virtual_uri`),
  ADD KEY `id_shop` (`id_shop`,`main`);

--
-- Indexes for table `ap_smarty_cache`
--
ALTER TABLE `ap_smarty_cache`
  ADD PRIMARY KEY (`id_smarty_cache`),
  ADD KEY `name` (`name`),
  ADD KEY `cache_id` (`cache_id`),
  ADD KEY `modified` (`modified`);

--
-- Indexes for table `ap_smarty_last_flush`
--
ALTER TABLE `ap_smarty_last_flush`
  ADD PRIMARY KEY (`type`);

--
-- Indexes for table `ap_smarty_lazy_cache`
--
ALTER TABLE `ap_smarty_lazy_cache`
  ADD PRIMARY KEY (`template_hash`,`cache_id`,`compile_id`);

--
-- Indexes for table `ap_specific_price`
--
ALTER TABLE `ap_specific_price`
  ADD PRIMARY KEY (`id_specific_price`),
  ADD UNIQUE KEY `id_product_2` (`id_product`,`id_product_attribute`,`id_customer`,`id_cart`,`from`,`to`,`id_shop`,`id_shop_group`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`id_specific_price_rule`),
  ADD KEY `id_product` (`id_product`,`id_shop`,`id_currency`,`id_country`,`id_group`,`id_customer`,`from_quantity`,`from`,`to`),
  ADD KEY `from_quantity` (`from_quantity`),
  ADD KEY `id_specific_price_rule` (`id_specific_price_rule`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `from` (`from`),
  ADD KEY `to` (`to`);

--
-- Indexes for table `ap_specific_price_priority`
--
ALTER TABLE `ap_specific_price_priority`
  ADD PRIMARY KEY (`id_specific_price_priority`,`id_product`),
  ADD UNIQUE KEY `id_product` (`id_product`);

--
-- Indexes for table `ap_specific_price_rule`
--
ALTER TABLE `ap_specific_price_rule`
  ADD PRIMARY KEY (`id_specific_price_rule`),
  ADD KEY `id_product` (`id_shop`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`from`,`to`);

--
-- Indexes for table `ap_specific_price_rule_condition`
--
ALTER TABLE `ap_specific_price_rule_condition`
  ADD PRIMARY KEY (`id_specific_price_rule_condition`),
  ADD KEY `id_specific_price_rule_condition_group` (`id_specific_price_rule_condition_group`);

--
-- Indexes for table `ap_specific_price_rule_condition_group`
--
ALTER TABLE `ap_specific_price_rule_condition_group`
  ADD PRIMARY KEY (`id_specific_price_rule_condition_group`,`id_specific_price_rule`);

--
-- Indexes for table `ap_state`
--
ALTER TABLE `ap_state`
  ADD PRIMARY KEY (`id_state`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `name` (`name`),
  ADD KEY `id_zone` (`id_zone`);

--
-- Indexes for table `ap_statssearch`
--
ALTER TABLE `ap_statssearch`
  ADD PRIMARY KEY (`id_statssearch`);

--
-- Indexes for table `ap_stock`
--
ALTER TABLE `ap_stock`
  ADD PRIMARY KEY (`id_stock`),
  ADD KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

--
-- Indexes for table `ap_stock_available`
--
ALTER TABLE `ap_stock_available`
  ADD PRIMARY KEY (`id_stock_available`),
  ADD UNIQUE KEY `product_sqlstock` (`id_product`,`id_product_attribute`,`id_shop`,`id_shop_group`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_shop_group` (`id_shop_group`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

--
-- Indexes for table `ap_stock_mvt`
--
ALTER TABLE `ap_stock_mvt`
  ADD PRIMARY KEY (`id_stock_mvt`),
  ADD KEY `id_stock` (`id_stock`),
  ADD KEY `id_stock_mvt_reason` (`id_stock_mvt_reason`);

--
-- Indexes for table `ap_stock_mvt_reason`
--
ALTER TABLE `ap_stock_mvt_reason`
  ADD PRIMARY KEY (`id_stock_mvt_reason`);

--
-- Indexes for table `ap_stock_mvt_reason_lang`
--
ALTER TABLE `ap_stock_mvt_reason_lang`
  ADD PRIMARY KEY (`id_stock_mvt_reason`,`id_lang`);

--
-- Indexes for table `ap_store`
--
ALTER TABLE `ap_store`
  ADD PRIMARY KEY (`id_store`);

--
-- Indexes for table `ap_store_shop`
--
ALTER TABLE `ap_store_shop`
  ADD PRIMARY KEY (`id_store`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_supplier`
--
ALTER TABLE `ap_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `ap_supplier_lang`
--
ALTER TABLE `ap_supplier_lang`
  ADD PRIMARY KEY (`id_supplier`,`id_lang`);

--
-- Indexes for table `ap_supplier_shop`
--
ALTER TABLE `ap_supplier_shop`
  ADD PRIMARY KEY (`id_supplier`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_supply_order`
--
ALTER TABLE `ap_supply_order`
  ADD PRIMARY KEY (`id_supply_order`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `reference` (`reference`);

--
-- Indexes for table `ap_supply_order_detail`
--
ALTER TABLE `ap_supply_order_detail`
  ADD PRIMARY KEY (`id_supply_order_detail`),
  ADD KEY `id_supply_order` (`id_supply_order`,`id_product`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_product_product_attribute` (`id_product`,`id_product_attribute`);

--
-- Indexes for table `ap_supply_order_history`
--
ALTER TABLE `ap_supply_order_history`
  ADD PRIMARY KEY (`id_supply_order_history`),
  ADD KEY `id_supply_order` (`id_supply_order`),
  ADD KEY `id_employee` (`id_employee`),
  ADD KEY `id_state` (`id_state`);

--
-- Indexes for table `ap_supply_order_receipt_history`
--
ALTER TABLE `ap_supply_order_receipt_history`
  ADD PRIMARY KEY (`id_supply_order_receipt_history`),
  ADD KEY `id_supply_order_detail` (`id_supply_order_detail`),
  ADD KEY `id_supply_order_state` (`id_supply_order_state`);

--
-- Indexes for table `ap_supply_order_state`
--
ALTER TABLE `ap_supply_order_state`
  ADD PRIMARY KEY (`id_supply_order_state`);

--
-- Indexes for table `ap_supply_order_state_lang`
--
ALTER TABLE `ap_supply_order_state_lang`
  ADD PRIMARY KEY (`id_supply_order_state`,`id_lang`);

--
-- Indexes for table `ap_tab`
--
ALTER TABLE `ap_tab`
  ADD PRIMARY KEY (`id_tab`),
  ADD KEY `class_name` (`class_name`),
  ADD KEY `id_parent` (`id_parent`);

--
-- Indexes for table `ap_tab_advice`
--
ALTER TABLE `ap_tab_advice`
  ADD PRIMARY KEY (`id_tab`,`id_advice`);

--
-- Indexes for table `ap_tab_lang`
--
ALTER TABLE `ap_tab_lang`
  ADD PRIMARY KEY (`id_tab`,`id_lang`);

--
-- Indexes for table `ap_tab_module_preference`
--
ALTER TABLE `ap_tab_module_preference`
  ADD PRIMARY KEY (`id_tab_module_preference`),
  ADD UNIQUE KEY `employee_module` (`id_employee`,`id_tab`,`module`);

--
-- Indexes for table `ap_tag`
--
ALTER TABLE `ap_tag`
  ADD PRIMARY KEY (`id_tag`),
  ADD KEY `tag_name` (`name`),
  ADD KEY `id_lang` (`id_lang`);

--
-- Indexes for table `ap_tag_count`
--
ALTER TABLE `ap_tag_count`
  ADD PRIMARY KEY (`id_group`,`id_tag`),
  ADD KEY `id_group` (`id_group`,`id_lang`,`id_shop`,`counter`);

--
-- Indexes for table `ap_tax`
--
ALTER TABLE `ap_tax`
  ADD PRIMARY KEY (`id_tax`);

--
-- Indexes for table `ap_tax_lang`
--
ALTER TABLE `ap_tax_lang`
  ADD PRIMARY KEY (`id_tax`,`id_lang`);

--
-- Indexes for table `ap_tax_rule`
--
ALTER TABLE `ap_tax_rule`
  ADD PRIMARY KEY (`id_tax_rule`),
  ADD KEY `id_tax_rules_group` (`id_tax_rules_group`),
  ADD KEY `id_tax` (`id_tax`),
  ADD KEY `category_getproducts` (`id_tax_rules_group`,`id_country`,`id_state`,`zipcode_from`);

--
-- Indexes for table `ap_tax_rules_group`
--
ALTER TABLE `ap_tax_rules_group`
  ADD PRIMARY KEY (`id_tax_rules_group`);

--
-- Indexes for table `ap_tax_rules_group_shop`
--
ALTER TABLE `ap_tax_rules_group_shop`
  ADD PRIMARY KEY (`id_tax_rules_group`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_theme`
--
ALTER TABLE `ap_theme`
  ADD PRIMARY KEY (`id_theme`);

--
-- Indexes for table `ap_themeconfigurator`
--
ALTER TABLE `ap_themeconfigurator`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `ap_theme_meta`
--
ALTER TABLE `ap_theme_meta`
  ADD PRIMARY KEY (`id_theme_meta`),
  ADD UNIQUE KEY `id_theme_2` (`id_theme`,`id_meta`),
  ADD KEY `id_theme` (`id_theme`),
  ADD KEY `id_meta` (`id_meta`);

--
-- Indexes for table `ap_theme_specific`
--
ALTER TABLE `ap_theme_specific`
  ADD PRIMARY KEY (`id_theme`,`id_shop`,`entity`,`id_object`);

--
-- Indexes for table `ap_timezone`
--
ALTER TABLE `ap_timezone`
  ADD PRIMARY KEY (`id_timezone`);

--
-- Indexes for table `ap_warehouse`
--
ALTER TABLE `ap_warehouse`
  ADD PRIMARY KEY (`id_warehouse`);

--
-- Indexes for table `ap_warehouse_carrier`
--
ALTER TABLE `ap_warehouse_carrier`
  ADD PRIMARY KEY (`id_warehouse`,`id_carrier`),
  ADD KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `id_carrier` (`id_carrier`);

--
-- Indexes for table `ap_warehouse_product_location`
--
ALTER TABLE `ap_warehouse_product_location`
  ADD PRIMARY KEY (`id_warehouse_product_location`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_warehouse`);

--
-- Indexes for table `ap_warehouse_shop`
--
ALTER TABLE `ap_warehouse_shop`
  ADD PRIMARY KEY (`id_warehouse`,`id_shop`),
  ADD KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_webservice_account`
--
ALTER TABLE `ap_webservice_account`
  ADD PRIMARY KEY (`id_webservice_account`),
  ADD KEY `key` (`key`);

--
-- Indexes for table `ap_webservice_account_shop`
--
ALTER TABLE `ap_webservice_account_shop`
  ADD PRIMARY KEY (`id_webservice_account`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indexes for table `ap_webservice_permission`
--
ALTER TABLE `ap_webservice_permission`
  ADD PRIMARY KEY (`id_webservice_permission`),
  ADD UNIQUE KEY `resource_2` (`resource`,`method`,`id_webservice_account`),
  ADD KEY `resource` (`resource`),
  ADD KEY `method` (`method`),
  ADD KEY `id_webservice_account` (`id_webservice_account`);

--
-- Indexes for table `ap_web_browser`
--
ALTER TABLE `ap_web_browser`
  ADD PRIMARY KEY (`id_web_browser`);

--
-- Indexes for table `ap_zone`
--
ALTER TABLE `ap_zone`
  ADD PRIMARY KEY (`id_zone`);

--
-- Indexes for table `ap_zone_shop`
--
ALTER TABLE `ap_zone_shop`
  ADD PRIMARY KEY (`id_zone`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ap_address`
--
ALTER TABLE `ap_address`
  MODIFY `id_address` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ap_advice`
--
ALTER TABLE `ap_advice`
  MODIFY `id_advice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `ap_alias`
--
ALTER TABLE `ap_alias`
  MODIFY `id_alias` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_attachment`
--
ALTER TABLE `ap_attachment`
  MODIFY `id_attachment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_attachment_lang`
--
ALTER TABLE `ap_attachment_lang`
  MODIFY `id_attachment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_attribute`
--
ALTER TABLE `ap_attribute`
  MODIFY `id_attribute` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `ap_attribute_group`
--
ALTER TABLE `ap_attribute_group`
  MODIFY `id_attribute_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ap_attribute_impact`
--
ALTER TABLE `ap_attribute_impact`
  MODIFY `id_attribute_impact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ap_badge`
--
ALTER TABLE `ap_badge`
  MODIFY `id_badge` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;
--
-- AUTO_INCREMENT for table `ap_bonslick`
--
ALTER TABLE `ap_bonslick`
  MODIFY `id_tab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_carrier`
--
ALTER TABLE `ap_carrier`
  MODIFY `id_carrier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ap_cart`
--
ALTER TABLE `ap_cart`
  MODIFY `id_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `ap_cart_rule`
--
ALTER TABLE `ap_cart_rule`
  MODIFY `id_cart_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_cart_rule_product_rule`
--
ALTER TABLE `ap_cart_rule_product_rule`
  MODIFY `id_product_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_cart_rule_product_rule_group`
--
ALTER TABLE `ap_cart_rule_product_rule_group`
  MODIFY `id_product_rule_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_category`
--
ALTER TABLE `ap_category`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ap_cms`
--
ALTER TABLE `ap_cms`
  MODIFY `id_cms` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `ap_cms_block`
--
ALTER TABLE `ap_cms_block`
  MODIFY `id_cms_block` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_cms_block_page`
--
ALTER TABLE `ap_cms_block_page`
  MODIFY `id_cms_block_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ap_cms_block_shop`
--
ALTER TABLE `ap_cms_block_shop`
  MODIFY `id_cms_block` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_cms_category`
--
ALTER TABLE `ap_cms_category`
  MODIFY `id_cms_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_cms_category_shop`
--
ALTER TABLE `ap_cms_category_shop`
  MODIFY `id_cms_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_cms_role`
--
ALTER TABLE `ap_cms_role`
  MODIFY `id_cms_role` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_compare`
--
ALTER TABLE `ap_compare`
  MODIFY `id_compare` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_condition`
--
ALTER TABLE `ap_condition`
  MODIFY `id_condition` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;
--
-- AUTO_INCREMENT for table `ap_configuration`
--
ALTER TABLE `ap_configuration`
  MODIFY `id_configuration` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=445;
--
-- AUTO_INCREMENT for table `ap_configuration_kpi`
--
ALTER TABLE `ap_configuration_kpi`
  MODIFY `id_configuration_kpi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `ap_connections`
--
ALTER TABLE `ap_connections`
  MODIFY `id_connections` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;
--
-- AUTO_INCREMENT for table `ap_connections_source`
--
ALTER TABLE `ap_connections_source`
  MODIFY `id_connections_source` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `ap_contact`
--
ALTER TABLE `ap_contact`
  MODIFY `id_contact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_country`
--
ALTER TABLE `ap_country`
  MODIFY `id_country` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;
--
-- AUTO_INCREMENT for table `ap_cronjobs`
--
ALTER TABLE `ap_cronjobs`
  MODIFY `id_cronjob` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_currency`
--
ALTER TABLE `ap_currency`
  MODIFY `id_currency` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_customer`
--
ALTER TABLE `ap_customer`
  MODIFY `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ap_customer_message`
--
ALTER TABLE `ap_customer_message`
  MODIFY `id_customer_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_customer_thread`
--
ALTER TABLE `ap_customer_thread`
  MODIFY `id_customer_thread` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_customization`
--
ALTER TABLE `ap_customization`
  MODIFY `id_customization` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_customization_field`
--
ALTER TABLE `ap_customization_field`
  MODIFY `id_customization_field` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_date_range`
--
ALTER TABLE `ap_date_range`
  MODIFY `id_date_range` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_delivery`
--
ALTER TABLE `ap_delivery`
  MODIFY `id_delivery` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ap_employee`
--
ALTER TABLE `ap_employee`
  MODIFY `id_employee` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_feature`
--
ALTER TABLE `ap_feature`
  MODIFY `id_feature` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ap_feature_value`
--
ALTER TABLE `ap_feature_value`
  MODIFY `id_feature_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `ap_gender`
--
ALTER TABLE `ap_gender`
  MODIFY `id_gender` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_group`
--
ALTER TABLE `ap_group`
  MODIFY `id_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ap_group_reduction`
--
ALTER TABLE `ap_group_reduction`
  MODIFY `id_group_reduction` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_guest`
--
ALTER TABLE `ap_guest`
  MODIFY `id_guest` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `ap_homeslider`
--
ALTER TABLE `ap_homeslider`
  MODIFY `id_homeslider_slides` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ap_homeslider_slides`
--
ALTER TABLE `ap_homeslider_slides`
  MODIFY `id_homeslider_slides` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ap_hook`
--
ALTER TABLE `ap_hook`
  MODIFY `id_hook` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;
--
-- AUTO_INCREMENT for table `ap_hook_alias`
--
ALTER TABLE `ap_hook_alias`
  MODIFY `id_hook_alias` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `ap_hook_module_exceptions`
--
ALTER TABLE `ap_hook_module_exceptions`
  MODIFY `id_hook_module_exceptions` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ap_image`
--
ALTER TABLE `ap_image`
  MODIFY `id_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `ap_image_type`
--
ALTER TABLE `ap_image_type`
  MODIFY `id_image_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ap_import_match`
--
ALTER TABLE `ap_import_match`
  MODIFY `id_import_match` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_info`
--
ALTER TABLE `ap_info`
  MODIFY `id_info` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_lang`
--
ALTER TABLE `ap_lang`
  MODIFY `id_lang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_layered_category`
--
ALTER TABLE `ap_layered_category`
  MODIFY `id_layered_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `ap_layered_filter`
--
ALTER TABLE `ap_layered_filter`
  MODIFY `id_layered_filter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_layered_friendly_url`
--
ALTER TABLE `ap_layered_friendly_url`
  MODIFY `id_layered_friendly_url` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `ap_linksmenutop`
--
ALTER TABLE `ap_linksmenutop`
  MODIFY `id_linksmenutop` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_log`
--
ALTER TABLE `ap_log`
  MODIFY `id_log` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=509;
--
-- AUTO_INCREMENT for table `ap_mail`
--
ALTER TABLE `ap_mail`
  MODIFY `id_mail` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_manufacturer`
--
ALTER TABLE `ap_manufacturer`
  MODIFY `id_manufacturer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_memcached_servers`
--
ALTER TABLE `ap_memcached_servers`
  MODIFY `id_memcached_server` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_message`
--
ALTER TABLE `ap_message`
  MODIFY `id_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_meta`
--
ALTER TABLE `ap_meta`
  MODIFY `id_meta` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `ap_module`
--
ALTER TABLE `ap_module`
  MODIFY `id_module` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `ap_modules_perfs`
--
ALTER TABLE `ap_modules_perfs`
  MODIFY `id_modules_perfs` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_module_preference`
--
ALTER TABLE `ap_module_preference`
  MODIFY `id_module_preference` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_newsletter`
--
ALTER TABLE `ap_newsletter`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_np_calendar_slot`
--
ALTER TABLE `ap_np_calendar_slot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `ap_np_customer`
--
ALTER TABLE `ap_np_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ap_np_customer_file`
--
ALTER TABLE `ap_np_customer_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `ap_np_customer_file_to_upload`
--
ALTER TABLE `ap_np_customer_file_to_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ap_np_customer_point`
--
ALTER TABLE `ap_np_customer_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `ap_np_customer_type`
--
ALTER TABLE `ap_np_customer_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ap_np_product_point`
--
ALTER TABLE `ap_np_product_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `ap_operating_system`
--
ALTER TABLE `ap_operating_system`
  MODIFY `id_operating_system` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ap_orders`
--
ALTER TABLE `ap_orders`
  MODIFY `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `ap_order_carrier`
--
ALTER TABLE `ap_order_carrier`
  MODIFY `id_order_carrier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ap_order_cart_rule`
--
ALTER TABLE `ap_order_cart_rule`
  MODIFY `id_order_cart_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_order_detail`
--
ALTER TABLE `ap_order_detail`
  MODIFY `id_order_detail` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `ap_order_history`
--
ALTER TABLE `ap_order_history`
  MODIFY `id_order_history` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;
--
-- AUTO_INCREMENT for table `ap_order_invoice`
--
ALTER TABLE `ap_order_invoice`
  MODIFY `id_order_invoice` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ap_order_message`
--
ALTER TABLE `ap_order_message`
  MODIFY `id_order_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_order_payment`
--
ALTER TABLE `ap_order_payment`
  MODIFY `id_order_payment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `ap_order_return`
--
ALTER TABLE `ap_order_return`
  MODIFY `id_order_return` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_order_return_state`
--
ALTER TABLE `ap_order_return_state`
  MODIFY `id_order_return_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ap_order_slip`
--
ALTER TABLE `ap_order_slip`
  MODIFY `id_order_slip` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_order_state`
--
ALTER TABLE `ap_order_state`
  MODIFY `id_order_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `ap_page`
--
ALTER TABLE `ap_page`
  MODIFY `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ap_pagenotfound`
--
ALTER TABLE `ap_pagenotfound`
  MODIFY `id_pagenotfound` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_page_type`
--
ALTER TABLE `ap_page_type`
  MODIFY `id_page_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ap_paypal_braintree`
--
ALTER TABLE `ap_paypal_braintree`
  MODIFY `id_paypal_braintree` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_paypal_capture`
--
ALTER TABLE `ap_paypal_capture`
  MODIFY `id_paypal_capture` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_paypal_customer`
--
ALTER TABLE `ap_paypal_customer`
  MODIFY `id_paypal_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_paypal_hss_email_error`
--
ALTER TABLE `ap_paypal_hss_email_error`
  MODIFY `id_paypal_hss_email_error` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_paypal_login_user`
--
ALTER TABLE `ap_paypal_login_user`
  MODIFY `id_paypal_login_user` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_paypal_plus_pui`
--
ALTER TABLE `ap_paypal_plus_pui`
  MODIFY `id_paypal_plus_pui` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_product`
--
ALTER TABLE `ap_product`
  MODIFY `id_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `ap_product_attribute`
--
ALTER TABLE `ap_product_attribute`
  MODIFY `id_product_attribute` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `ap_product_download`
--
ALTER TABLE `ap_product_download`
  MODIFY `id_product_download` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_product_supplier`
--
ALTER TABLE `ap_product_supplier`
  MODIFY `id_product_supplier` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ap_profile`
--
ALTER TABLE `ap_profile`
  MODIFY `id_profile` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ap_quick_access`
--
ALTER TABLE `ap_quick_access`
  MODIFY `id_quick_access` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `ap_range_price`
--
ALTER TABLE `ap_range_price`
  MODIFY `id_range_price` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_range_weight`
--
ALTER TABLE `ap_range_weight`
  MODIFY `id_range_weight` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_referrer`
--
ALTER TABLE `ap_referrer`
  MODIFY `id_referrer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_referrer_shop`
--
ALTER TABLE `ap_referrer_shop`
  MODIFY `id_referrer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_request_sql`
--
ALTER TABLE `ap_request_sql`
  MODIFY `id_request_sql` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_required_field`
--
ALTER TABLE `ap_required_field`
  MODIFY `id_required_field` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_risk`
--
ALTER TABLE `ap_risk`
  MODIFY `id_risk` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ap_scene`
--
ALTER TABLE `ap_scene`
  MODIFY `id_scene` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_search_engine`
--
ALTER TABLE `ap_search_engine`
  MODIFY `id_search_engine` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `ap_search_word`
--
ALTER TABLE `ap_search_word`
  MODIFY `id_word` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5618;
--
-- AUTO_INCREMENT for table `ap_sekeyword`
--
ALTER TABLE `ap_sekeyword`
  MODIFY `id_sekeyword` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_shop`
--
ALTER TABLE `ap_shop`
  MODIFY `id_shop` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_shop_group`
--
ALTER TABLE `ap_shop_group`
  MODIFY `id_shop_group` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_shop_url`
--
ALTER TABLE `ap_shop_url`
  MODIFY `id_shop_url` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_specific_price`
--
ALTER TABLE `ap_specific_price`
  MODIFY `id_specific_price` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ap_specific_price_priority`
--
ALTER TABLE `ap_specific_price_priority`
  MODIFY `id_specific_price_priority` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;
--
-- AUTO_INCREMENT for table `ap_specific_price_rule`
--
ALTER TABLE `ap_specific_price_rule`
  MODIFY `id_specific_price_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_specific_price_rule_condition`
--
ALTER TABLE `ap_specific_price_rule_condition`
  MODIFY `id_specific_price_rule_condition` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_specific_price_rule_condition_group`
--
ALTER TABLE `ap_specific_price_rule_condition_group`
  MODIFY `id_specific_price_rule_condition_group` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_state`
--
ALTER TABLE `ap_state`
  MODIFY `id_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;
--
-- AUTO_INCREMENT for table `ap_statssearch`
--
ALTER TABLE `ap_statssearch`
  MODIFY `id_statssearch` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `ap_stock`
--
ALTER TABLE `ap_stock`
  MODIFY `id_stock` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_stock_available`
--
ALTER TABLE `ap_stock_available`
  MODIFY `id_stock_available` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `ap_stock_mvt`
--
ALTER TABLE `ap_stock_mvt`
  MODIFY `id_stock_mvt` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_stock_mvt_reason`
--
ALTER TABLE `ap_stock_mvt_reason`
  MODIFY `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ap_store`
--
ALTER TABLE `ap_store`
  MODIFY `id_store` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ap_supplier`
--
ALTER TABLE `ap_supplier`
  MODIFY `id_supplier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_supply_order`
--
ALTER TABLE `ap_supply_order`
  MODIFY `id_supply_order` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_supply_order_detail`
--
ALTER TABLE `ap_supply_order_detail`
  MODIFY `id_supply_order_detail` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_supply_order_history`
--
ALTER TABLE `ap_supply_order_history`
  MODIFY `id_supply_order_history` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_supply_order_receipt_history`
--
ALTER TABLE `ap_supply_order_receipt_history`
  MODIFY `id_supply_order_receipt_history` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_supply_order_state`
--
ALTER TABLE `ap_supply_order_state`
  MODIFY `id_supply_order_state` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ap_tab`
--
ALTER TABLE `ap_tab`
  MODIFY `id_tab` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `ap_tab_module_preference`
--
ALTER TABLE `ap_tab_module_preference`
  MODIFY `id_tab_module_preference` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_tag`
--
ALTER TABLE `ap_tag`
  MODIFY `id_tag` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;
--
-- AUTO_INCREMENT for table `ap_tax`
--
ALTER TABLE `ap_tax`
  MODIFY `id_tax` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_tax_rule`
--
ALTER TABLE `ap_tax_rule`
  MODIFY `id_tax_rule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=590;
--
-- AUTO_INCREMENT for table `ap_tax_rules_group`
--
ALTER TABLE `ap_tax_rules_group`
  MODIFY `id_tax_rules_group` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_theme`
--
ALTER TABLE `ap_theme`
  MODIFY `id_theme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ap_themeconfigurator`
--
ALTER TABLE `ap_themeconfigurator`
  MODIFY `id_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ap_theme_meta`
--
ALTER TABLE `ap_theme_meta`
  MODIFY `id_theme_meta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `ap_timezone`
--
ALTER TABLE `ap_timezone`
  MODIFY `id_timezone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=561;
--
-- AUTO_INCREMENT for table `ap_warehouse`
--
ALTER TABLE `ap_warehouse`
  MODIFY `id_warehouse` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_warehouse_product_location`
--
ALTER TABLE `ap_warehouse_product_location`
  MODIFY `id_warehouse_product_location` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_webservice_account`
--
ALTER TABLE `ap_webservice_account`
  MODIFY `id_webservice_account` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_webservice_permission`
--
ALTER TABLE `ap_webservice_permission`
  MODIFY `id_webservice_permission` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ap_web_browser`
--
ALTER TABLE `ap_web_browser`
  MODIFY `id_web_browser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ap_zone`
--
ALTER TABLE `ap_zone`
  MODIFY `id_zone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
