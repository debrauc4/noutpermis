<style media="screen">
li{
  list-style-type: none;
}
ul{
  padding: 0;
}
.one-category{
  width: 100%;
  display: flex !important;
}
.one-category .img-container{
  width: 20%;
  display: flex;
  align-items: center;
  justify-content: space-around;
  overflow: hidden;
  height: 75px;
}
.one-category .img-container img{
  width: 100%;
  height: auto;
}

.one-category .name{
  width: 80%;
  padding-left: 10px;
  text-align: left;
}
.separator{
  width: 100%;
  border-bottom: 1px solid grey;
  margin: 5px 0px;
}
</style>
<form class="" enctype="multipart/form-data" action="{$link->getAdminLink('AdminNpappcms')}" method="post">
  {if isset($current_app_cms)}
  <div class="row">
    <div class="panel col-md-6">
      <h3>Page : "{$current_app_cms->title}"</h3>
      <input type="hidden" name="id_app_cms" value="{$current_app_cms->id_app_cms}">
      <div class="form-group">
        <label class="control-label col-lg-3">Titre :</label>
        <div class="col-lg-9">
          <input type="text" value="{$current_app_cms->title}" name="nTitleCms">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-lg-3">Sous-titre :</label>
        <div class="col-lg-9">
          <input type="text" value="{$current_app_cms->subtitle}" name="nSubtitleCms">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-lg-3">Image :</label>
        <div class="col-lg-9">
          <input type="file" value="" name="nImageCms"><br />
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-lg-3">ID CMS :</label>
        <div class="col-lg-9">
          <input type="text" value="{$current_app_cms->id_cms}" name="nIdCms">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          {if $current_app_cms->id_app_cms}
          <button onclick="setInput({$current_app_cms->id_app_cms_category}); setInputCms({$current_app_cms->id_app_cms})" type="submit" name="deleteCms" class="btn btn-default"><i class="icon-trash"></i> Supprimer</button>
          {/if}
        </div>
      </div>
      <div class="panel-footer">
        <button onclick="setInput({$current_app_cms->id_app_cms_category}); setInputCms({$current_app_cms->id_app_cms})" type="submit" name="modifyCms" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer</button>
      </div>
    </div>
  </div>
  {/if}
  <div class="row">
    <div class="panel col-md-6">
      <h3>Catégories</h3>
      <input type="hidden" name="id_app_cms_category" id="id_app_cms_category" value="">
      <ul>
        <li>
          {foreach from=$cms_categories item=onecategory}
          <button onclick="setInput({$onecategory->id_app_cms_category})" class="btn btn-default one-category" type="submit" name="viewCategory">
            <div class="img-container">
              <img src="{$onecategory->img_link}" alt="">
            </div>
            <div class="name">
              <h4>{$onecategory->title}</h4>
              <h6>{$onecategory->subtitle}</h6>
            </div>
          </button>
          {/foreach}
        </li>
      </ul>
    </div>
    <div class="panel col-md-6">
      {if isset($current_category)}
      <h3>Catégorie "{$current_category->title}"</h3>
      <h4>Modifier :</h4>
      <div class="">
        <div class="form-group">
          <label class="control-label col-lg-3">Titre :</label>
          <div class="col-lg-9">
            <input type="text" value="{$current_category->title}" name="nTitleCategory">
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">Sous-titre :</label>
          <div class="col-lg-9">
            <input type="text" value="{$current_category->subtitle}" name="nSubtitleCategory">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-3">Image :</label>
          <div class="col-lg-9">
            <input type="file" value="" name="nImageCategory"><br />
          </div>
        </div>
        <div class="">
          <button onclick="setInput({$current_category->id_app_cms_category})" type="submit" name="modifyCategory" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer</button>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 separator">
        </div>
      </div>
      <h4>Articles :</h4>
      <div class="">
        <button class="btn btn-default" type="submit" name="addCms" onclick="setInput({$current_category->id_app_cms_category})">Ajouter</button>
      </div>
      <input type="hidden" name="id_app_cms" id="id_app_cms" value="">
      <div class="">
        <ul>
          <li>
            {foreach item=app_cms from=$current_category->app_cmss}
            <button type="submit" name="viewCms" class="btn btn-default one-category" onclick="setInputCms({$app_cms->id_app_cms})">
              <div class="img-container">
                <img src="{$app_cms->img_link}" alt="">
              </div>
              <div class="name">
                <h4>{$app_cms->title}</h4>
                <h6>{$app_cms->subtitle}</h6>
              </div>
            </button>
            {/foreach}
          </li>
        </ul>
      </div>
      {/if}
    </div>
  </div>
</form>

<script type="text/javascript">
  function setInput(x){
    document.getElementById('id_app_cms_category').value = x;
  }
  function setInputCms(x){
    document.getElementById('id_app_cms').value = x;
  }
</script>
