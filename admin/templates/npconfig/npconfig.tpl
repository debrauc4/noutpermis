<form class="" action="{$link->getAdminLink('AdminNpconfig')}" method="post">
  <div class="row">
    <div class="panel col-md-6">
      <div class="panel-heading">
        Configuration
      </div>
      <div class="panel-body">
        <div >
          <label class="control-label col-lg-3">
            <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Définissez la taille limite des fichiers téléchargeables (en mégaoctets). Cette valeur doit être inférieure ou égale à la taille maximale allouée pour votre serveur (actuellement: 2 MB)." data-html="true">
              Heure de début
            </span>
          </label>
          <div class="col-lg-9">
            <div class="input-group">
              <input class="form-control " type="text" size="5" name="NP_CALENDAR_HOUR_START" value="{$values['NP_CALENDAR_HOUR_START']}"/>
              <span class="input-group-addon">
                heure
              </span>
            </div>
          </div>
        </div>
        <br />
        <br />
        <div >
          <label class="control-label col-lg-3">
            <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Permet de définir l'heure de fin du calendrier." data-html="true">
              Heure de fin
            </span>
          </label>
          <div class="col-lg-9">
            <div class="input-group">
              <input class="form-control " type="text" size="5" name="NP_CALENDAR_HOUR_END" value="{$values['NP_CALENDAR_HOUR_END']}"/>
              <span class="input-group-addon">
                heure
              </span>
            </div>
          </div>
        </div>
        <br />
        <div >
          <label class="control-label col-lg-3">
            <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Permet de définir à quel moment un créneau peut etre annulé." data-html="true">
              Annulation d'un créneau
            </span>
          </label>
          <div class="col-lg-9">
            <div class="input-group">
              <input class="form-control " type="text" size="5" name="NP_CALENDAR_TIME_CANCEL" value="{$values['NP_CALENDAR_TIME_CANCEL']}"/>
              <span class="input-group-addon">
                heures avant
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <button type="submit" class="btn btn-default pull-right" name="setConfiguration"><i class="process-icon-save"></i> Enregistrer</button>
      </div>
    </div>
    <div class="panel col-md-6">
      <div class="panel-heading">
        Jours férié
      </div>
      <div class="panel-body">
        <div class="well form-horizontal">
          <div class="row">
            <div class="form-group col-lg-6 col-sm-12">
              <label class="control-label col-lg-4" style="text-align: left">Montrer</label>
              <div class="col-lg-7">
                <input type="date" name="date_holiday" value="">
              </div>
            </div>
            <div class="form-group col-lg-6 col-sm-12">
              <label class="control-label col-lg-4" style="text-align: left">Récurrence</label>
              <div class="col-lg-7">
                <label for="every_year_true">
                  <input type="radio" id="every_year_true" name="every_year" value="1"> Tous les ans
                </label><br />
                <label for="every_year_false">
                  <input type="radio" id="every_year_false" name="every_year" value="0"> Une seul fois
                </label>
              </div>
            </div>
            <button class="btn btn-default pull-right" type="submit" name="addHoliday"><i class="icon-plus"></i> Ajouter</button>
          </div>
        </div>
        <div id="holidays-list">
          <div class="">
            <input type="text" name="" value="" placeholder="Rechercher ..." onkeyup="myShowDate(event)">
          </div>
          <table class="table">
            <thead>
              <tr>
                <td>ID</td>
                <td>Date</td>
                <td>Actions</td>
              </tr>
            </thead>
            <tbody  id="holiday_list">
              {foreach from=$holidays item=hd}
              <tr id="hd-{$hd->date_holiday|date_format : '%d/%m/%Y'}">
                <td>{$hd->id}</td>
                <td>{$hd->date_holiday|date_format : "%d/%m/%Y"}</td>
                <td>
                  <button type="button" class="btn button btn-default" name="button"><i class="icon-trash"></i></button>
                </td>
              </tr>
              {/foreach}
            </tbody>
          </table>
        </div>
      </div>
      <div class="panel-footer">
        <button type="submit" class="btn btn-default pull-right" name="addHoliday"><i class="process-icon-save"></i> Enregistrer</button>
      </div>
    </div>
  </div>

</form>

<script type="text/javascript">
  function myShowDate(d){
    var cn = document.getElementById('holiday_list').childNodes
    var mdate = d.target.value
    for (tr of cn) {
      if (tr.id){
        var l = 'hd-' + mdate;
        console.log('La : [' + l + '] -- [' + tr.id + '] == ' + tr.id.indexOf(l))

        if (tr.id.indexOf(l) > -1){
          tr.style.display = '';
        } else {
          tr.style.display = 'none';
        }
      }
    }
  }

</script>
