<style media="screen">
  .oneslot{
    display: inline-block;
    width: 100%;
    padding: 3px;
  }

</style>
{if isset($error)}
<article class="alert alert-danger">
  <p>{$error}</p>
</article>
{/if}
<div class="panel">
  <div class="panel-heading">
    <h1 class="panel-title">Depot Vente</h1>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-1">
        <form class=""  action="{$link->getAdminLink('AdminNpcalendar')|escape:'htmlall':'utf-8'}" method="post">
          <input type="hidden" name="year" value="{$datepicker.before.year}" readonly>
          <input type="hidden" name="week" value="{$datepicker.before.week}" readonly>
          <button type="submit" name="button"><</button>
        </form>
      </div>
      <div class="col-md-2">
        <div class="current-date">
          Semaine du {$days[0].day} {$days[0].Month} au {$days[6].day} {$days[6].Month}
        </div>
      </div>
      <div class="col-md-1" >
        <form class="" action="{$link->getAdminLink('AdminNpcalendar')|escape:'htmlall':'utf-8'}" method="post">
          <input type="hidden" name="year" value="{$datepicker.after.year}" readonly>
          <input type="hidden" name="week" value="{$datepicker.after.week}" readonly>
          <button type="submit" name="button">></button>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <table class="table ">
          <thead>
            <tr>
              <th></th>
              {foreach from=$days item=day}
              <th>
                {$day.day_in_week}<br />
                {$day.day} {$day.Month} {$day.year}
              </th>
              {/foreach}
            </tr>
          </thead>
          <tbody>
            {for $i=$hours.start to $hours.end -1}
            <tr>
              <td>{$i}h - {$i+1}h</td>
              {foreach from=$days item=day}
              <td id="{$i}-{$day.day}-{$day.month}-{$day.year}" onclick="agendaHandleClick(event)">

                {if isset($slot_in_agenda) && isset($slot_in_agenda[$day.day]) && isset($slot_in_agenda[$day.day][$i])}
                <span class="oneslot">
                  {$slot_in_agenda[$day.day][$i]->customer_obj->firstname} {$slot_in_agenda[$day.day][$i]->customer_obj->lastname} <a target="_blank" href="{$link->getAdminLink('AdminCustomers')}&amp;viewcustomer&amp;id_customer={$slot_in_agenda[$day.day][$i]->id_customer}"> <i class="icon-link"></i></a>
                </span>
                {/if}
              </td>
              {/foreach}
            </tr>
            {/for}
          </tbody>
        </table>
      </div>
      <div class="col-md-4">
        <h3>Ajouter un creneau manuellement</h3>
        <form class="action-form" action="{$link->getAdminLink('AdminNpcalendar')|escape:'htmlall':'utf-8'}" method="post">
          <div class="form-group">
            <label class="control-label col-lg-3">
              <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="Date du créneau">
                Date
              </span>
            </label>
            <div class="col-lg-9">
              <input type="date" name="date_slot" value="" class="" id="date-picker" onchange="viewAgendaSlot()">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-lg-3">
              <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="Date du créneau">
                Heure :
              </span>
            </label>
            <div class="col-lg-9">
              <select class="" name="hour" id="hour-picker" onchange="viewAgendaSlot()">
                {for $i=$hours.start to $hours.end -1}
                <option value="{$i}">{$i}h à {$i+1}h</option>
                {/for}
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-lg-3">
              <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="Date du créneau">
                Client :
              </span>
            </label>
            <div class="col-lg-9">
              <select class="" id="customer-selector" name="id_customer" onchange="viewCustomerPoints()">
                <option value="0">Choississez un client</option>
                {foreach from=$customer_list item=c}
                  {if $c->verified == 1}
                    <option value="{$c->id}" >{$c->customer_obj->firstname} {$c->customer_obj->lastname}</option>
                  {/if}
                {/foreach}
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-lg-3">
              <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="Date du créneau">
                Retirer forfait :
              </span>
            </label>
            <div class="col-lg-9" id="point-selector">
            </div>
          </div>
          <br>
          <br>
          <br>
          <button type="submit" value="1" name="addSlot" class="btn btn-default pull-right">
            <i class="process-icon-save"></i> Enregistrer
          </button>
        </form>

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var np_customers = {$customer_list|@json_encode}
  var save_td_colored = undefined;
  function viewCustomerPoints(){
    var id_np_customer = document.getElementById('customer-selector').value;
    var ps = document.getElementById('point-selector');
    ps.innerHTML = ""
    for (cust of np_customers) {
      if (cust.id == id_np_customer){
        var myselect = document.createElement("select")
        for (point of cust.np_customer_point) {
          var onepoint = document.createElement("option");
          onepoint.innerHTML = point.product_obj.name;
          onepoint.value = point.id
          myselect.appendChild(onepoint);
        }
        //gratuit
        var onepoint = document.createElement("option");
        onepoint.innerHTML = "Gratuit";
        onepoint.value = 0;
        myselect.appendChild(onepoint);
        //!gratuit
        ps.appendChild(myselect);
        break;
      }
    }
  }

  function viewAgendaSlot(){
    if (save_td_colored)save_td_colored.style.background = 'inherit';
    var h = document.getElementById('hour-picker').value
    var d = document.getElementById('hour-picker').value
    d = new Date(d);
    m = (d.getMonth() +1).toString().length == 1 ? '0' + (d.getMonth() +1) : (d.getMonth() +1);
    day = d.getDate().toString().length == 1 ? '0' + d.getDate() : d.getDate();
    var html_id = h + '-' + day + '-' + m + '-' + d.getFullYear()
    var e = document.getElementById(html_id)
    if (e){
      e.style.background = 'red';
      save_td_colored = e
    }
  }

  function agendaHandleClick(evt){
    var html_id = evt.target.id;
    var tab = html_id.split('-');
    if (tab.length != 4) return;
    if (save_td_colored)save_td_colored.style.background = 'inherit';
    document.getElementById('hour-picker').value = tab[0]
    document.getElementById('date-picker').value = tab[3] + '-' + tab[2] + '-' + tab[1];
    var e = document.getElementById(html_id)
    if (e){
      e.style.background = 'red';
      save_td_colored = e
    }
  }
</script>
