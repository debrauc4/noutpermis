<?php
/**
* 2007-2015 PrestaShop
*
*  @author    Guterwein
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminNpappcmsControllerCore extends AdminModulesController
{

  public function init(){
    parent::init();

  }

  public function initContent(){
    parent::initContent();
    $cms_categories = NPAppCmsCategory::getAll();
    $this->context->smarty->assign(array('cms_categories' => $cms_categories));
    $this->setTemplate('../../../../override/controllers/admin/templates/npappcms/npappcms.tpl');
  }

  public function postProcess(){
    if (Tools::isSubmit('viewCategory')){
      $this->_initCurrentCategory(Tools::getValue('id_app_cms_category'));
    }
    else if (Tools::isSubmit('viewCms')){
      $n = new NPAppCms(Tools::getValue('id_app_cms'));
      $this->_initCurrentCategory($n->id_app_cms_category);
      $this->context->smarty->assign(array('current_app_cms' => $n));
    }
    else if (Tools::isSubmit('addCms')){
      $n = new NPAppCms();
      $cc = $this->_initCurrentCategory(Tools::getValue('id_app_cms_category'));
      $n->id_app_cms_category = $cc->id_app_cms_category;
      $this->context->smarty->assign(array('current_app_cms' => $n));
    }
    else if (Tools::isSubmit('modifyCms')){
      $this->_modifyCms();
    }
    else if (Tools::isSubmit('deleteCms')){
      $id = Tools::getValue('id_app_cms');
      $id_cat = Tools::getValue('id_app_cms_category');
      $n = new NPAppCms($id);
      $n->delete();
      $this->_initCurrentCategory($id_cat);
    }
    else if (Tools::isSubmit('modifyCategory')){
      $this->_modifyCategory();
    }
  }

  private function _modifyCategory(){
    $id_cat = Tools::getValue('id_app_cms_category');
    $c = new NPAppCmsCategory($id_cat);
    $c->title = Tools::getValue('nTitleCategory');
    $c->subtitle = Tools::getValue('nSubtitleCategory');
    if (isset($_FILES['nImageCategory']) && isset($_FILES['nImageCategory']['name'])){
      $l = $this->_uploadImage($_FILES['nImageCategory']);
      if ($l)
        $c->img_link = $l;
    }
    $c->save();
    $this->_initCurrentCategory($c->id_app_cms_category);
  }

  private function _modifyCms(){
    $id = Tools::getValue('id_app_cms');
    $id_cat = Tools::getValue('id_app_cms_category');
    $n = new NPAppCms($id);
    $n->id_app_cms_category = $id_cat;
    $n->title = Tools::getValue('nTitleCms');
    $n->subtitle = Tools::getValue('nSubtitleCms');
    $n->id_cms = Tools::getValue('nIdCms');
    if (isset($_FILES['nImageCms'])){
      $l = $this->_uploadImage($_FILES['nImageCms']);
      if ($l)
        $n->img_link = $l;
    }
    $n->save();
    $this->context->smarty->assign(array('current_app_cms' => $n));
    $cc = $this->_initCurrentCategory($id_cat);
  }

  private function _initCurrentCategory($id_app_cms_category){
    $cc = new NPAppCmsCategory($id_app_cms_category);
    $this->context->smarty->assign(array('current_category' => $cc));
    return $cc;
  }

  private function _uploadImage($file){
    $ret = '';
    if (isset($file['name'])
        && !empty($file['tmp_name']))
    {
      $extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.png', '.jpeg', '.jpg');
      $filename = uniqid(). '.' .pathinfo($file['name'], PATHINFO_EXTENSION);
      $filename = str_replace(' ', '-', $filename);
      $filename = strtolower($filename);
      $filename = filter_var($filename, FILTER_SANITIZE_STRING);
      $file['name'] = $filename;
      $uploader = new UploaderCore();
      $uploader->upload($file);
      $ret = _PS_BASE_URL_ . __PS_BASE_URI__. 'upload/' .$filename;
    }
    return $ret;
  }

}
