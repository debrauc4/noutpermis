<?php
/**
* 2007-2015 PrestaShop
*
*  @author    Guterwein
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminNpcalendarControllerCore extends AdminModulesController
{

  public function init(){
    parent::init();
  }

  public function initContent(){
    parent::initContent();
    $this->setTemplate('../../../../override/controllers/admin/templates/npcalendar/npcalendar.tpl');
    $this->initCalendar();
    $this->context->smarty->assign(array('customer_list' => NPCustomer::getCustomers()));
  }

  private function _createError($x){
    $this->context->smarty->assign(array('error' => $x));

  }

  public function postProcess(){
    if (Tools::isSubmit('addSlot')){
      $nDate = Tools::getValue('date_slot');
      $id_customer = Tools::getValue('id_customer');
      $hour = Tools::getValue('hour');
      if (DateTime::createFromFormat('Y-m-d', $nDate) === FALSE){
        $this->_createError("Merci de sélectionner une date valide");
      }
      else if ($id_customer == ""){
        $this->_createError("Merci de sélectionner un client.");
      }
      else if ($hour == ""){
        $this->_createError("Merci de sélectionner une heure.");
      }
      else {
        $v = new NPCalendarSlot();
        $v->hour = $hour;
        $v->date_slot = $nDate;
        if ($v->alreadyExist()){
          $this->_createError("Le creneau est déjà reservé.");
        }
        else{
          $v->id_customer = $id_customer;
          $v->save();
        }
      }
    }
  }

  public function initCalendar(){
    //init
    $days = $this->_genDays(Tools::getValue('year'), Tools::getValue('week'));
    $date_start = $days[0]['year'] . '-' . $days[0]['month'] . '-' . $days[0]['day'];
    $date_end = $days[6]['year'] . '-' . $days[6]['month'] . '-' . $days[6]['day'];
    $slots = NPCalendarSlot::getByInterval($date_start, $date_end);
    $slot_in_agenda = $this->_genSlotAgenda($slots, $days);
    $this->context->smarty->assign(array('slots' => $slots));
    $this->context->smarty->assign(array('slot_in_agenda' => $slot_in_agenda));
    $this->context->smarty->assign(array('days' => $days));
    $this->context->smarty->assign(array('datepicker' => $this->_genDatePicker(Tools::getValue('year'), Tools::getValue('week'))));
    $this->context->smarty->assign(array('hours' => array('start' => 7, 'end' => 21)));

  }
  private function _genSlotAgenda($slots, $days){
    $ret = array(array());
    foreach($slots as $slot){
      $dt = new DateTime($slot->date_slot);
      $i = $dt->format('d');
      $j = $slot->hour . "";
      $ret[$i][$j] = $slot;
    }
    return $ret;
  }
  private function _getDayName($x){return array('Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim')[$x];}
  private function _getMonthName($x){return array('', 'Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil.', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre')[$x];}
  private function _genDay($datetime){
    return array(
      'day_in_week' => $this->_getDayName($datetime->format('w')),
      'day' => $datetime->format('d'),
      'month' => $datetime->format('m'),
      'Month' => $this->_getMonthName(intval($datetime->format('m'))),
      'year' => $datetime->format('Y')
    );
  }
  private function _genDays($year, $week){
    $date_formatter = new IntlDateFormatter('fr_FR', IntlDateFormatter::FULL, IntlDateFormatter::NONE);
    $dt = new DateTime;
    if ($year != "" && $week != "") {
      $dt->setISODate($year, $week);
    } else {
      $dt->setISODate($dt->format('o'), $dt->format('W'));
    }
    $year = $dt->format('o');
    $week = $dt->format('W');
    $ret = array();
    do{
      array_push($ret, $this->_genDay($dt));
      $dt->modify('+1 day');
    }while ($week == $dt->format('W'));
    return $ret;
  }


  private function _genDatePicker($year, $week){
    $dt = new DateTime;
    if ($year != "" && $week != "") {
      $dt->setISODate($year, $week);
    } else {
      $dt->setISODate($dt->format('o'), $dt->format('W'));
    }
    $dt->modify('-1 day');
    $date_before = array('week' => $dt->format('W'), 'year' => $dt->format('o'));
    $dt->modify('+1 day');
    $year = $dt->format('o');
    $week = $dt->format('W');
    do{
      $dt->modify('+1 day');
    }while ($week == $dt->format('W'));
    $date_after = array('week' => $dt->format('W'), 'year' => $dt->format('o'));
    return array('before' => $date_before, 'after' => $date_after);
  }
}
