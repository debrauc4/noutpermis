<?php
/**
* 2007-2015 PrestaShop
*
*  @author    Guterwein
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminNpconfigControllerCore extends AdminModulesController
{
  private $keys = array('NP_CALENDAR_TIME_CANCEL', 'NP_CALENDAR_HOUR_END', 'NP_CALENDAR_HOUR_START');

  public function init(){
    parent::init();
  }

  public function initContent(){
    parent::initContent();
    $this->initValues();
    $this->setTemplate('../../../../override/controllers/admin/templates/npconfig/npconfig.tpl');
  }

  public function postProcess(){
    ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
    if (Tools::isSubmit('setConfiguration')){
      foreach($this->keys as &$key){
        $value = Tools::getValue($key);
        Configuration::updateValue($key,$value);
      }
    }
    else if (Tools::isSubmit('addHoliday')){
      $ey = Tools::getValue('every_year');
      $d = Tools::getValue('date_holiday');
      $this->context->smarty->assign(array('datesubmit' => $d));
      if ($ey == '1'){
        NPCalendarHoliday::addHundredYear($d);
      }
      else {
        $n = new NPCalendarHoliday();
        $n->date_holiday = $d;
        $n->add();
      }
    }
  }

  public function initValues(){
    //Config
    $this->context->smarty->assign(array('values' => Configuration::getMultiple($this->keys)));
    //holidays
    $this->context->smarty->assign(array('holidays' => NPCalendarHoliday::getAll()));

  }

}
