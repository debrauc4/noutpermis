#!/bin/bash
rm android-signed.apk
apkdir="platforms/android/app/build/outputs/apk/release"
appname="app-release-unsigned.apk"
echo -e "\033[0;32m Generate apk \033[0m"
ionic cordova build android --release &> /tmp/null
rm ${apkdir}/tr.apk
echo -e "\033[0;32m Signing Apk \033[0m"
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore autopermis.keystore ${apkdir}/${appname} autopermis
echo -e "\033[0;32m Zipalign Apk \033[0m"
zipalign -v 4 ${apkdir}/${appname} ${apkdir}/tr.apk  &> /tmp/null
mv ${apkdir}/tr.apk android-signed.apk
echo -e "\033[0;32m Signed apk created './android-signed.apk' \033[0m"
