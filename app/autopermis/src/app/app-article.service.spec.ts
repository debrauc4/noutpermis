import { TestBed } from '@angular/core/testing';

import { AppArticleService } from './app-article.service';

describe('AppArticleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppArticleService = TestBed.get(AppArticleService);
    expect(service).toBeTruthy();
  });
});
