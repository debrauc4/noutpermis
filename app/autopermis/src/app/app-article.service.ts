import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppArticleService {
  private url = 'https://www.autopermis.re/app/cms.php'
  constructor(private http: HttpClient) { }

  public getAll(){
    return this.http.get(`https://www.autopermis.re/app/cms.php`).pipe(map(results => results));
  }
}
