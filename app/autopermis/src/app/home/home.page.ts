import { Component } from '@angular/core';
import { AppArticleService } from '../app-article.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public tab ;
  public current_category;
  public current_article;
  public categories;
  constructor(public appArticleService : AppArticleService) {
    this.tab = 0;
    this.current_category = 0;
    this.appArticleService.getAll().subscribe((data) => {
      this.categories = data;
      console.log('Coucou', data)
    });
  }

  openCategory(c){
    this.current_category = c;
  }

  openArticle(a){
    this.current_article = a;
  }

}
