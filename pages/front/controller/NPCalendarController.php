<?php
class NPCalendarControllerCore extends ModuleFrontController
{
  public $php_self = 'npcalendar';

  public function __construct()
  {
    parent::__construct();

  }
  public function init(){ parent::init(); }
  public function initContent()
  {
    $this->context->smarty->assign(
      array(
        'variableSmarty1' => 'Prueba 1',
        'variableSmarty2' => 'Prueba 2',
      )
    );

    parent::initContent();
    $this->setTemplate('npcalendar');
  }
}
